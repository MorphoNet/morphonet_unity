#!/usr/bin/env python3
# -*- coding: latin-1 -*-

##################################################################
### THIS SCRIPT IS AUTOMATICALLY LAUNCH BY GITLAB RUNNER
##################################################################


import os,sys
from os.path import join, isdir, isfile
import argparse
from functions import checkBuild, get_versions, execute, change_scriptingBackend, \
	set_bundleVersion, set_credentials, read_secrets, build_MN, get_players, get_Unity_Executable, get_Build_Path, rmrf, \
	rm

parser = argparse.ArgumentParser()
parser.add_argument('-o',"--os", default="", help="Which OS (OSX,LINUX,WINDOWS,ANDROID,IOS)", required=True)
args = parser.parse_args()
if args.os=="":
	print(" --> miss operating system...")
	sys.exit(1)
osmn=args.os


Secrets=read_secrets() #Read Password and Users
buildPlayer=get_players() #Read the corresponding players
Unity=get_Unity_Executable()  #Unity Executable


#Set PATHs
main_path=os.getcwd() #IN morphonet_unity (not build)
unity_build=get_Build_Path()

projectPath=join(main_path,"MorphoNet")

StreamingAssets_path=join(projectPath,"Assets","StreamingAssets")
Assets_path=join(projectPath,"Assets")

unitylog=join(unity_build,"UnityLog-"+osmn+".txt")

#Get the  Version
version,gitversion=get_versions(Assets_path)

######## CLEAN PREVIOUS VULD
rmrf(join(unity_build,osmn))
rm(unitylog)

#########  PRE BUILD
change_scriptingBackend(main_path,osmn)
set_bundleVersion(main_path,version) #Change Bundle Version

####### SET CREDENTIALS
set_credentials("/Users/ci/Certificates",osmn,StreamingAssets_path)

########  BUILD  UNITY
build_MN(Unity,osmn,projectPath,unitylog,Secrets,buildPlayer[osmn])

######## SHOW LOG
if osmn!="WINDOWS": execute("cat " + unitylog)
else:
	#os.system('tasklist | findstr /i Unity.Licensing.Client && taskkill /im Unity.Licensing.Client.exe /F || echo process "Unity.Licensing.Client.exe" not running.')
	os.system('Start-Process cmd.exe -ArgumentList "/c tasklist | findstr /i Unity.Licensing.Client && taskkill /im Unity.Licensing.Client.exe /F || echo process Unity.Licensing.Client.exe not running" -NoNewWindow -Wait -PassThru')
	#rm(unitylog)

######### CHECK BUILDS
if not checkBuild(join(unity_build,osmn),osmn):
	print(" ERROR Building failed for " + osmn)
	sys.exit(1)
