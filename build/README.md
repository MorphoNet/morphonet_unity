# How To Build the MorphoNet Unity Project  ?
Notice : The MorphoNet project is  build automaticaly wit gitlab runner

This project requires multiples builds :
- API
- MorphoNet Unity  (this git)
- Lineage
- MorphoNet Standalone (this git)

### 2 differents builds in this git
- To build MorphoNet Unity: python build.py -o <OS>
- To build MorphoNet Standalone: python build-with-python.py

## PREREQUISITES
### Prepare a machine to build in unity
- In Continous Integration tools (https://ci.inria.fr/project/morphonet/slaves)
- Create a new slave  (ex : OSX Catalina   with 8Gb and 4 Cores, with 60GB and 40GB for external drive only for xcode .. )
- log with console cloud stack
-
### Prepare the runner (log on the ssh ci@ of your machine)
 - Open Browser , download and Install Unity Hub
 - Log with unity id
 - Install Unity Editor (actual 2021.3.18f1)
 - Install Xcode v 12.4 (for catalina install from develop apple website, it does not work from apple store)


### Install a new gitlab Runner
- Connect to Slave (ssh ci@...)
  - Download the binary for your system sudo : ```sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64  ```
  - Give it permission to execute : ``` sudo chmod +x /usr/local/bin/gitlab-runner ```
  - Run the runner :
    - ```cd ~```
    - ```gitlab-runner install```
    - ```gitlab-runner start```
  - Register the tokein : ```gitlab-runner register --url https://gitlab.inria.fr/ --registration-token GR134894179b_u1YAxY-GvwV5tSSv```
    - tags : unity
    - executor : shell


## Copy the secret file inside the runner to this exact location /Users/ci/secret.txt
- ``` scp secret.txt  ci@unitybuilds:/Users/ci/ ```
- ``` scp developerID_application.cer ci@unitybuilds:/Users/ci/ ```
- open xcode -> accounts -> add a new account -> apple id (enter login+passwd) -> manage certificats -> create a new one

## MophoNet Lineage Project  CI_PROJECT_ID="40367"

# Pyinstaller build setup temporary documentation

#### PRE Setup
```
git clone git@gitlab.inria.fr:MorphoNet/morphonet_unity.git
git clone git@gitlab.inria.fr:MorphoNet/morphonet_api.git
git clone git@gitlab.inria.fr:MorphoNet/morphonet_admintools.git
git clone git@gitlab.inria.fr:MorphoNet/morphonet_lineage.git
```
### CREATE A VIRTUAL ENVIRONNEMENT
```
python3 -m venv morphonet_env
source morphonet_env/bin/activate
pip install pyinstaller
pip install psutil
pip install morphonet
cd morphonet_api
pip install -e .  # TO INSTALL THE LAST VERSION on API to make sure you have the last version of API on git
```


### PREPARE A LINUX VM FOR PYINSTALLER
- In Continous Integration tools (https://ci.inria.fr/project/morphonet/slaves)
- Create a new slave  (ex : Ubuntu 20 with 8Gb and 4 Cores, with 60GB and 40GB for external drive only for xcode .. )
- log with ssh (add in config first)
- install the runner (installation inside gitlab->Settings->CI/CD->Install a Runner)

### FOR ALL Runners you have to add the ssh key and test the log (with the runner account) to the storage runner (silicon at LIRMM)
### For Windows,
1. Tapez "services" dans l'invite de commande du menu démarrer et lancer l'application.
2. Cliquez sur le service gitlab-runner et allez dans l'onglet "Log On". Là  vous entrez le mot de passe du compte ci et validez.
3. Relancez le service avec la flèche verte en haut.

# Important: storage and Artifacts.

Build/Pipeline artifacts are supposed to be automatically deleted as per project setup. The duration of conservation is specified per pipeline, with the *expires_in* parameter. In case a mass deletion of artifacts is needed, please refer to https://gitlab.inria.fr/explore/catalog/sdt-pfo/tools/gitlab-quotas-helper-component. It explains the component used after pipelines. Note that it requires a CI variable that is a gitlab api token, and can be created by the owner only. It is to be updated regularly using the tutorial on the link above. 
