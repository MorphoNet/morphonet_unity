
import os,sys
from os.path import join, isdir
import argparse
from functions import addToPlist, execute, get_versions, checkBuild, setBuildVersion, \
	sync_to_MN_server, zip, sign_OSX, read_secrets, ditto, xcodebuild, notarize, get_Unity_Version, get_Unity_Path, \
	get_Build_Path, get_Deploy_MN_Path, get_Zip, get_build_name

parser = argparse.ArgumentParser()
parser.add_argument('-o',"--os", default="", help="Which OS (OSX,LINUX,WINDOWS,ANDROID,IOS)", required=True)
parser.add_argument('-b',"--branch", default="", help="Which MorphoNet branch (sable,dev,beta)", required=True)
args = parser.parse_args()
if args.os=="":
	print(" --> MISS OS")
	sys.exit(1)
if args.branch=="":
	print(" --> MISS BRANCH")
	sys.exit(1)
osmn=args.os
branch=args.branch
if branch=="main": branch="beta"

print(" ->> DEPLOY MORPHONET from "+branch+" git branch ")

Secrets=read_secrets() #Read Password and Users


#### SET PATHS
builds_path_mn=get_Deploy_MN_Path()
main_path=os.getcwd()
unity_build=get_Build_Path()
projectPath=join(main_path,"MorphoNet")
StreamingAssets_path=join(projectPath,"Assets","StreamingAssets")
Assets_path=join(projectPath,"Assets")
entitlements=join(main_path,"build","MorphoNet.entitlements")
morphonet_zip=join(main_path,get_Zip())


######### CHECK PRE-BUILDS
if not checkBuild(join(unity_build,osmn),osmn):
	print(" --> ERROR please build first for "+ osmn)
	sys.exit(1)

#Get the  Version
version,gitversion=get_versions(Assets_path)
UnityVersion=get_Unity_Version()


######### POST BUILD
if osmn=="OSX": #https://docs.unity3d.com/2021.2/Documentation/Manual/macos-building-notarization.html#compressing-the-application
	addToPlist(join(unity_build,osmn,"MorphoNet","MorphoNet","Info.plist"))

	execute("cp -f "+entitlements+" "+join(unity_build,osmn,"MorphoNet","MorphoNet"))

	xcodebuild(join(unity_build,osmn,"MorphoNet",'MorphoNet.xcodeproj'),"MorphoNet")
	if not os.path.isdir(join(unity_build,osmn, "MorphoNet","build","Release","MorphoNet.app")):
		print(" Xcode compile morphonet failed for "+osmn)
		sys.exit(1)

	sign_OSX(entitlements,join(unity_build,osmn,"MorphoNet","build","Release","MorphoNet.app"),Secrets)

	ditto(join(unity_build,osmn,"MorphoNet","build","Release","MorphoNet.app"),morphonet_zip)

	#notarize(Secrets,morphonet_zip) #No need to notarize at this step


elif osmn == "IOS":
	addToPlist(join(unity_build,osmn,"MorphoNet","Info.plist"))

	setBuildVersion(unity_build,osmn,version) #Change BuildNumber (The VERSION SHOULD BE INCREMENT BY HAND)

	execute('xcodebuild archive -project '+join(unity_build,osmn,"MorphoNet",'Unity-iPhone.xcodeproj') +" -scheme Unity-iPhone -archivePath "+join(unity_build,osmn,"MorphoNet.xcarchive"))
	if not os.path.isdir(join(unity_build,osmn, "MorphoNet.xcarchive")):
		print(" Xcode archive build Failed for "+osmn)
		print(" --> if error is 'error: No profiles for 'com.cnrs.MorphoNet'' -> go in Xcode and relog  ")
		sys.exit(1)

	# Generate the ipa
	execute("xcodebuild -exportArchive -archivePath "+join(unity_build,osmn, "MorphoNet.xcarchive")+" -exportPath "+join(unity_build,osmn, "MorphoNet.ipa")+" -exportOptionsPlist "+join(unity_build,"AppStore_ExportOptions.plist")+"  -allowProvisioningUpdates")
	if not os.path.isdir(join(unity_build,osmn, "MorphoNet.ipa")):
		print(" Xcode ipa build Failed for "+osmn)
		sys.exit(1)

	#Upload the ipa (the passwaord is generated for the application in https://appleid.apple.com/account/manage -> Generate Password )
	execute('xcrun altool --upload-app  --file "'+join(unity_build,osmn, "MorphoNet.ipa", "MorphoNet.ipa")+'"  --type ios --username "mac.manu@gmail.com" --password "oxkw-ojci-srbx-rbvw"')

	print(" Go ons https://appstoreconnect.apple.com/apps/1542407313/testflight/ios")  #and validate few things...

elif osmn != "WebGL":
	zip(osmn, join(unity_build,osmn),morphonet_zip)

elif osmn == "WebGL":
	#WEBGL DEPLOY ON THE WEBSITE
	if branch == "dev" or branch == "stable" or branch == "beta":
		sync_to_MN_server(version, gitversion, builds_path_mn, osmn, join(unity_build,osmn,"MorphoNet"), branch, UnityVersion,architecture="amd64")


