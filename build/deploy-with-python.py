import os,sys
from os.path import dirname, basename, isdir, join, isfile
from functions import read_secrets, mkdir, execute, get_Zip, unzip, sign_OSX, zip, ditto, notarize, recurisve_sign_OSX, \
    sync_to_MN_server, get_versions, get_Unity_Version, get_Deploy_MN_Path, rmrf, ls, rm, move, sync_to_MN_server_p, \
    get_build, push_build
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-o',"--os", default="", help="Which OS (OSX,LINUX,WINDOWS,ANDROID,IOS)", required=True)
parser.add_argument('-b',"--branch", default="", help="Which MorphoNet branch (sable,dev,beta)", required=True)
parser.add_argument('-i',"--apibranch", default="main", help="Which API branch (sable,dev,beta)", required=True)
parser.add_argument('-l',"--lineagebranch", default="main", help="Which Lineage branch (sable,dev,beta)", required=True)
parser.add_argument('-t',"--cijobtoken", default="main", help="$CI_JOB_TOKEN", required=True)
parser.add_argument('-a',"--architecture", default="amd64", help="Which architecture (silicon or other...)", required=False)
args = parser.parse_args()
if args.os=="": sys.exit(1)
if args.branch=="": sys.exit(1)
osmn=args.os

Secrets=read_secrets() #Read Password and Users

# SET PATH STANDALONE
main_path = os.getcwd()
standalone_zip = get_Zip()
build_path=join(main_path,"MorphoNet_Build")

morphonet_app=None
if osmn=="OSX":
    morphonet_app = "MorphoNet.app"
    lineage_app = "MorphoNetLineage.app"
    standalone_app = "MorphoNet_Standalone.app"
    lib_path=join(morphonet_app,"Contents")
    entitlements = join(main_path, "build", "MorphoNet.entitlements")
elif osmn == "LINUX":
    morphonet_app = "MorphoNet"
    lineage_app = "MorphoNetLineage"
    standalone_app = "MorphoNet_Standalone"
    lib_path =  join("MorphoNet","libs")
elif osmn == "WINDOWS":
    morphonet_app = "MorphoNet.exe"
    lineage_app = "MorphoNetLineage"
    standalone_app = "MorphoNet_Standalone"
    lib_path = join("MorphoNet","libs")

# Get the  Version
projectPath = join(main_path, "MorphoNet")
Assets_path = join(projectPath, "Assets")

version, gitversion = get_versions(Assets_path)


UnityVersion = get_Unity_Version()
builds_path_mn = get_Deploy_MN_Path()

rmrf("MorphoNet") #Remove code folder we don't need it here
ls(osmn)

architecture=args.architecture
############### UNITY BRANCH
branch=args.branch
if branch=="main": branch="beta"

#Get MorphoNet Unity App
morphonet_zip=get_Zip()
unzip(morphonet_zip,osmn)
ls(osmn)
if osmn == "WINDOWS" :
    if not isfile(join("MorphoNet",morphonet_app)):
        print(" --> ERROR Miss MorphoNet Unity App "+morphonet_app)
        sys.exit(1)
    else:  rm(morphonet_zip)
else:
    if not isdir(morphonet_app):
        print(" --> ERROR Miss MorphoNet Unity App "+morphonet_app)
        sys.exit(1)
    else:  rm(morphonet_zip)

ls(osmn)



############### PYTHON API BUILD
def get_api_build(api_branch):
    #Get MORPHONET PYTHON FROM SHARED REPOSITORY
    standalone_zip="Py-MorphoNet-"+osmn.upper()+".tar.gz"

    get_build(api_branch, osmn, standalone_zip, architecture=architecture)
    miss_it=False
    if (osmn=="OSX" and not isdir(standalone_app)): miss_it=True
    if (osmn=="LINUX" or osmn=="WINDOWS") and not isdir(standalone_app):miss_it=True
    if miss_it:
        print(" --> warning Miss Standalone API for branch" +api_branch)
        return False
    else:
        rm(standalone_zip)
        return True

apibranch=args.apibranch
if apibranch=="main": apibranch="beta"
if not get_api_build(apibranch):
    apibranch="beta"
    if not get_api_build(apibranch):
        print(" --> ERROR Miss Standalone API ")
        sys.exit(1)
ls(osmn)

#MORPHONET LINEAGE APP
def get_lineage_app(lineage_branch):
    lineage_zip="MorphoNetLineage_"+osmn.upper()+".zip"
    if not execute('curl --location --output '+lineage_zip+' --header "JOB-TOKEN: '+str(args.cijobtoken)+'" "https://gitlab.inria.fr/api/v4/projects/40367/jobs/artifacts/'+lineage_branch+'/download?job=deploy_'+osmn.lower()+'"'):
        print(" --> warning can not download the lineage app artifacts from lineage branch "+lineage_branch)
        return False

    if os.stat(lineage_zip).st_size<100:
        print(" --> warning donwloadling the lineage app  "+lineage_zip)
        return False

    unzip(lineage_zip,osmn)
    if not isdir(lineage_app):
        print(" --> warning Miss Lineage Unity build "+lineage_app)
        return False
    else:
        rm(lineage_zip)
        return True

lineagebranch=args.lineagebranch
if not get_lineage_app(lineagebranch):
    lineagebranch="main"
    if not get_lineage_app(lineagebranch):
        print(" --> ERROR Miss Lineage Unity build ")
        sys.exit(1)
ls(osmn)


print(" ->> DEPLOY MORPHONET WITH PYTHON from "+branch+" git branch on api "+apibranch+ " and lineage branch "+lineagebranch)


##### START TO MERGE
mkdir(lib_path)
mkdir(join(lib_path,"LINEAGE"))
move(lineage_app,join(lib_path,"LINEAGE"),osmn)
mkdir(join(lib_path,"STANDALONE"))
move(standalone_app,join(lib_path,"STANDALONE"),osmn)
execute("find "+morphonet_app+" -name .DS_Store -delete") #REMOVE .DS_Store files
ls(osmn)


if osmn=="OSX":
    if  branch=="beta" or branch == "dev" or branch == "stable": ####SIGN ALL NECESSARY APPLICATION
        #recurisve_sign_OSX(entitlements,morphonet_app,Secrets)
        for line in open("build/to_sign", "r"):
            if isfile(line.strip()):
                sign_OSX(entitlements, line.strip(), Secrets, deep=False, stdout=False)
            else:
                print("--> ERROR miss " + line.strip())
        sign_OSX(entitlements,join(lib_path,"STANDALONE",standalone_app),Secrets)
        sign_OSX(entitlements,join(lib_path,"LINEAGE",lineage_app),Secrets)
        sign_OSX(entitlements,morphonet_app,Secrets)

        #####  NOTARISATION FOR THE FULL APPLICATION
        ditto(morphonet_app,morphonet_zip)
        notarize(Secrets,morphonet_zip)
    else:
        zip(osmn, ".", morphonet_zip)
else:
    zip(osmn,".",morphonet_zip)
rmrf("MorphoNet")
ls(osmn)

################### DEPLOY ON MORPHONET ORG WEB SITE FOR PRINCIPAL BRANCHES
if  branch=="beta" or branch == "dev" or branch == "stable":
    if osmn == "LINUX" or osmn == "OSX":
        sync_to_MN_server(version,gitversion,builds_path_mn,osmn,morphonet_zip,branch,UnityVersion,architecture)
    elif osmn == "WINDOWS" :
        sync_to_MN_server_p(version,gitversion,builds_path_mn,osmn,morphonet_zip,branch,UnityVersion,architecture)



################### COPY BUILD TO SHARED REPOSITORY
push_build(branch,osmn,morphonet_zip,architecture=architecture)

################### CLEAN SPACE
rmrf(morphonet_zip)

ls(osmn)
