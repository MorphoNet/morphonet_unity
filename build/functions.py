import os,sys
from os.path import join, isdir, isfile, basename
import json
from datetime import datetime
import platform
import shutil

def rmrf(path):
    import glob
    folders = glob.glob(path)
    for fold in folders:
        if os.path.exists(fold):
            if os.path.isfile(fold) or os.path.islink(fold):
                os.unlink(fold)
            else:
                res = shutil.rmtree(fold)


def rm(file):
    if os.path.exists(file):
        if os.path.isfile(file):
            os.unlink(file)

def move(file,dest,osm):
    if osm == "WINDOWS":
        execute("move " + file + " " +dest)
    else:
        execute("mv " + file + " " +dest)


def get_Unity_Path():
    UnityPath = "/Applications/Unity/Hub/Editor/"  # OSX
    if not isdir(UnityPath): UnityPath="/Applications/Unity/"  # OSX ALTERNATIVE PATH
    if not isdir(UnityPath): UnityPath = "/builds/Unity/Hub/Editor/"  # LINUX
    if not isdir(UnityPath): UnityPath = "C:\\Program Files\\Unity\\Hub\\Editor"  # WINDOWS
    if not isdir(UnityPath):
        print(" --> didn't find any unity path ....")
        sys.exit(1)
    print(" --> Unity Path " + UnityPath)
    return UnityPath

def get_Unity_Version():
    UnityPath=get_Unity_Path()
    UnityVersion = os.listdir(UnityPath)
    UnityVersion = [i for i in UnityVersion if ".ulf" not in i]
    UnityVersion.sort()
    UnityVersion = UnityVersion[-1]
    print(" --> Unity Version " + UnityVersion)
    return UnityVersion


def get_Unity_Executable():
    UnityPath=get_Unity_Path()
    UnityVersion = get_Unity_Version()

    Unity = join(UnityPath, UnityVersion, "Unity.app/Contents/MacOS/Unity")  # OSX
    if not isfile(Unity): Unity =  join(UnityPath, "Unity.app/Contents/MacOS/Unity")    # OSX
    if not isfile(Unity): Unity = join(UnityPath, UnityVersion, "Editor/Unity")  # LINUX
    if not isfile(Unity): Unity = join(UnityPath, UnityVersion, "Editor\\Unity.exe")  # LINUX
    if not isfile(Unity):
        print(" --> didn't find any unity install....")
        sys.exit(1)

    return Unity

def get_Build_Path():
    main_path = os.getcwd()  # IN morphonet_unity (not build)
    unity_build = join(main_path, "Unity_Build")
    mkdir(unity_build)
    return unity_build


def get_Deploy_MN_Path():
    return "/var/www/Builds" #PATH IN MN . ORG

def get_Zip():
    return "MorphoNet.zip"

def get_players():
    buildPlayer = {}
    buildPlayer['OSX'] = "OSXUniversal"
    buildPlayer['LINUX'] = "Linux64"
    buildPlayer['WINDOWS'] = "Win64"
    buildPlayer['ANDROID'] = "Android"
    buildPlayer['IOS'] = "iOS"
    buildPlayer['WebGL'] = "WebGL"
    return buildPlayer


def mkdir(path):
    if path is not None and path != "" and not isdir(path):
        try:
            os.mkdir(path)
            return True
        except:
            return False
            # path is already created ...
    return True


def execute(cmd,v=True):
    if v:print(cmd)
    return os.system(cmd)==0


def get_build_name(path,osmn):
    isafile = False
    isapath = False
    name = ""
    if osmn == "OSX":
        name = join(path, "MorphoNet", 'MorphoNet.xcodeproj')
        isapath = True
    elif osmn == "LINUX":
        name = join(path, "MorphoNet")
        isapath = True
    elif osmn == "IOS":
        name = join(path, "MorphoNet")
        isapath = True
    elif osmn == "ANDROID":
        name = join(path, "MorphoNet.aab")
        isafile = True
    elif osmn == "WINDOWS":
        name = join(path, "MorphoNet")
        isapath = True
    elif osmn == "WebGL":
        name = join(path, "MorphoNet", "MorphoNet", "build")
        isapath = True
    else:
        print(" --> Do not know this OS " + osmn)
        sys.exit(1)
    return name,isapath,isafile


def checkBuild(path,osmn):
    name, isapath, isafile=get_build_name(path,osmn)
    if isafile:
        if isfile(name):
            print(" --> found file "+name)
            return True
        else:
            print(" --> ERROR not found file" + name)
            return False

    if isapath:
        if isdir(name):
            print(" --> found path "+name)
            return True
        else:
            print(" --> ERROR not found path" + name)
            return False


def swith_branch(py_path,git,branch):
    execute("cd "+join(py_path,git)+"; git checkout "+branch)

def change_scriptingBackend(main_path,osmn):
    sb="il2CC"
    if osmn == "WINDOWS" or osmn == "WINDOWS_XR" or osmn == "OSX" or osmn == "LINUX":
        sb="Mono"  # WINDOWS CANNOT BUILD WITH I2LCC
        # OSX : Error building Player: IL2CPP scripting backend requires macOS 11 SDK (Xcode 12.2) or newer to be installed when targeting Apple silicon devices. Currently installed macOS SDK version is 10.15.4.
    s=""
    scriptingBackend=False
    ProjectSettings=join(main_path, 'MorphoNet', 'ProjectSettings', 'ProjectSettings.asset')
    for line in open(ProjectSettings,'r'):
        if line.find("scriptingBackend")>=0: scriptingBackend=True
        if line.find("Standalone")>=0 and scriptingBackend:
            scriptingBackend=False
            if sb.strip().lower().find("mono")>=0: s+="     Standalone: 0\n"
            elif sb.strip().lower().find("il2")>=0: s+="     Standalone: 1\n"
            else :
                print("--> ERROR unknown scriptingBackend "+str(sb))
                sys.exit(1)
        else:
            s+=line
    f=open(ProjectSettings,"w")
    f.write(s)
    f.close()

def set_bundleVersion(main_path,ver):
    s = ""
    ProjectSettings = join(main_path, 'MorphoNet', 'ProjectSettings', 'ProjectSettings.asset')
    for line in open(ProjectSettings, 'r'):
        if line.find("bundleVersion") >= 0:s += "  bundleVersion: "+ver+"\n"
        else:
            s += line
    f = open(ProjectSettings, "w")
    f.write(s)
    f.close()

def increment_AndroidbundleVersion(filename):
    v=None
    for line in open(filename, 'r'):
        v= int(line.strip())
    v+=1
    print(" --> NOW ANDROID BUNDLE VERSION "+str(v))
    f=open(filename, 'w')
    f.write(str(v))
    f.close()

def set_credentials(Certificates_path,osmn,StreamingAssets_path):
    if osmn == "ANDROID":
        # Copy Credential to unity project
        execute("cp -f " + join(Certificates_path,  "GooglePlayKeys.txt") + " " + StreamingAssets_path)
        execute("cp -f " + join(Certificates_path, "MorphoNet.keystore") + " " + StreamingAssets_path)

        android_bundlefile = join(Certificates_path,"bundleVersionCode.txt")
        increment_AndroidbundleVersion(android_bundlefile)
        execute("cp -f " + android_bundlefile + " " + StreamingAssets_path)

        # API Key : AIzaSyDZ9sMpoRmDGVLs9ubrktVlcoGIrhVlaLo
        # https://stasheq.medium.com/upload-apk-to-google-play-via-command-line-script-d93b0d6a28c5
        if not isfile(join(StreamingAssets_path, "GooglePlayKeys.txt")):
            print(" --> ERROR Miss credential " + join(StreamingAssets_path, "GooglePlayKeys.txt"))
            sys.exit(1)

def get_versions(Assets_path):
    # Get the  Version and git version
    # Read the Build Version
    version = ""
    versionfile = join(Assets_path, 'version.txt')
    if not isfile(versionfile):
        print(" --> miss version file " + versionfile)
        sys.exit(1)
    for line in open(versionfile): version = line.strip()
    if version == "":
        print(" --> did not find the current version ")
        sys.exit(1)

    # Set The Git Version
    gitversion = ""
    versionfile = join(Assets_path, 'gitversion.txt')
    os.system('git rev-parse --short HEAD >' + versionfile)
    if not isfile(versionfile):
        print(" --> creating Git version Failed")
        sys.exit(1)
    for f in open(versionfile, "r"): gitversion = f.strip()
    if gitversion == "":
        print(" --> did not find the current version ")
        sys.exit(1)
    print(" BUILD  Version  " + version + " with git " + gitversion)
    return version,gitversion

def git_branch():	 #Get The Git Branch
    execute('git branch > branch.txt')
    branch=open("branch.txt","r").read()
    print(" --> git branch is "+branch)
    return branch


def addToPlist(pfile):
    print(" --> add info.plist to "+pfile)
    # CHANGE CNRS by cnrs
    f = open(pfile, "r")
    Infoplist = []
    for line in f:
        Infoplist.append(line)
    f.close()

    fw = open(pfile, "w")
    for line in Infoplist:
        fw.write(line.replace("CNRS", "cnrs"))
    fw.close()

    f=open(pfile,"r")
    Infoplist=[]
    isCFBundleURLTypes=False
    for line in f:
        Infoplist.append(line)
        if line.find("CFBundleURLTypes")>=0:
            isCFBundleURLTypes=True
    f.close()

    if not isCFBundleURLTypes:
        print(" --> add CFBundleURLTypes to "+pfile)
        fw=open(pfile,"w")
        for line in Infoplist:
            fw.write(line)
            if line.find("  <dict>")==0:
                fw.write('<key>CFBundleURLTypes</key> \n <array> \n <dict> \n <key>CFBundleURLSchemes</key> \n <array> \n <string>morphonet</string> \n </array>\n <key>CFBundleURLName</key> \n <string>com.cnrs.morphonet</string> \n </dict>\n</array>\n')
        fw.close()


def setBuildVersion(unity_build,osmn,version):
    f = open(join(unity_build,osmn, "MorphoNet", "Info.plist"), 'r')
    fw = open(join(unity_build,osmn, "MorphoNet", "Info.save"), 'w')
    CFBundleVersion = False
    for line in f:
        if CFBundleVersion:
            fw.write("	<string>" + str(version) + "</string>\n")
            CFBundleVersion = False
        else:
            fw.write(line)
        if line.find("CFBundleVersion") > 0:
            CFBundleVersion = True
    f.close()
    fw.close()
    os.system('mv -v ' + join(unity_build,osmn, "MorphoNet", "Info.save") + " " + join(unity_build,osmn, "MorphoNet", "Info.plist"))


def zip(osmn,original_path,filename):
    if isfile(filename): rm(filename) #DELETE PREVIOUS ZIP

    if osmn == "OSX":
        execute("cd "+original_path+"; zip -r "+filename+" "+"MorphoNet.app  >/dev/null")

    if osmn == "LINUX":
        execute("cd " + original_path + "; zip -r " + filename + " " + "MorphoNet  >/dev/null")

    if osmn == "IOS":
        execute("cd " +original_path + "; zip -r " + filename + " " + join("MorphoNet.ipa", "MorphoNet.ipa")+ "  >/dev/null")

    if osmn == "ANDROID" :
        execute("cd " + original_path+ "; zip -r " + filename + " " + "MorphoNet.aab  >/dev/null")

    if osmn == "WINDOWS":
        shutil.make_archive(filename.replace(".zip",""), 'zip', original_path, "MorphoNet")

    if not isfile(filename):
        print(" --> Miss zip file "+filename)
        sys.exit(1)

    return True

def unzip(filename,osm):
    if not isfile(filename):
        print(" --> Miss filename to unzip " + filename)
        sys.exit(1)
    if osm=="WINDOWS":
        #execute("tar -xf "+filename)
        shutil.unpack_archive(filename)
    else:
        execute("unzip " + filename+ "  >/dev/null")

def ls(osm):
    if osm!="WINDOWS":
        execute("ls -laF ")
    else:
        execute("dir ")

def sync_to_MN_server_p(version,gitversion,builds_path_mn,osmn,filename,branch,UnityVersion,architecture="amd64"):
    print(" --> push the application to the website")
    if branch!="beta" and branch!="stable" and branch!="dev":
        print(" --> NOT ALLOW TO UPLOAD ON SERVER MORPHONET ORG from branch "+str(branch))
        sys.exit(1)
    build_version = version + "-" + gitversion
    dist_path=join(builds_path_mn,build_version,osmn)
    if osmn == "WINDOWS": dist_path = builds_path_mn + "/" + build_version + "/" + osmn  # TO BE IN LINUX MODE
    if architecture == "GPU": dist_path = join(builds_path_mn, build_version, osmn + "_" + architecture)
    if osmn == "WINDOWS" and architecture == "GPU": dist_path = builds_path_mn + "/" + build_version + "/" + osmn + "_"+architecture  # for windows GP otherwise path formatted incorrectly
    if architecture=="silicon": dist_path=join(builds_path_mn,build_version,architecture)

    import paramiko
    k = paramiko.RSAKey.from_private_key_file("C:\\Users\\ci\\.ssh\\id_rsa")
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect("morphonet.org", username="morphonet", pkey=k)
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("mkdir -p " + dist_path + "")

    if not isfile(filename):
        print("WARNING : file "+filename+" for paramiko scp put is missing")

    print("DIST : "+dist_path+"/"+filename)
    scp = ssh.open_sftp()
    scp.put(filename, dist_path+"/"+filename)


    #UPDATE JSON VERSIONS FILES
    json_file="versions.json"
    if isfile(json_file): rm(json_file)

    print(" SCP "+builds_path_mn+"/"+json_file+ " to "+os.getcwd()+"\\"+json_file)
    scp.get(builds_path_mn+"/"+json_file,os.getcwd()+"\\"+json_file)

    if isfile(json_file):  # Read JSON file
        with open(json_file) as data_file:
            jsonversion = json.load(data_file)
    else: jsonversion = {}  # Initialize

    newversion = {}
    newversion["name"] = version + '-' + gitversion
    newversion["build"] = branch
    newversion["version"] = version
    newversion["git"] = gitversion
    newversion["os"] = osmn
    newversion["unity"]=UnityVersion
    if osmn=="OSX": newversion["architecture"] = architecture
    if architecture == "GPU": newversion["architecture"] = architecture
    newversion["path"] = dist_path + '/' + basename(filename)
    newversion["date"] = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    newversion["author"] = 'CI-' + platform.platform()

    idx=version + '-' + gitversion + '-' + osmn
    if architecture=="silicon":idx=version + '-' + gitversion + '-' + architecture
    if architecture == "GPU": idx = version + '-' + gitversion + '-' + osmn + "_" + architecture
    jsonversion[idx] = newversion #Add New Version

    # Write JSON file
    with open(json_file, 'w') as outfile:
        json.dump(jsonversion, outfile, indent=10, separators=(',', ': '))

    scp.put(json_file,builds_path_mn+"/"+json_file)



def sync_to_MN_server(version,gitversion,builds_path_mn,osmn,filename,branch,UnityVersion,architecture="amd64"):
    print(" --> push the application to the website")
    if branch!="beta" and branch!="stable" and branch!="dev":
        print(" --> NOT ALLOW TO UPLOAD ON SERVER MORPHONET ORG from branch "+str(branch))
        sys.exit(1)
    build_version = version + "-" + gitversion
    dist_path=join(builds_path_mn,build_version,osmn)
    if architecture=="silicon": dist_path=join(builds_path_mn,build_version,architecture)
    if architecture == "GPU": dist_path = join(builds_path_mn, build_version,osmn+"_"+architecture)
    if osmn == "WINDOWS": dist_path = builds_path_mn + "/" + build_version + "/" + osmn  # TO BE IN LINUX MODE

    if not execute("ssh  morphonet@morphonet.org 'mkdir -p " + dist_path + "'"):
        print(" --> failed to create path on distant server")
        sys.exit(1)
    if not execute('scp -r ' + filename+ " morphonet@morphonet.org:" + dist_path + "/"):
        print(" --> failed to copy build ")
        sys.exit(1)

    #UPDATE JSON VERSIONS FILES
    json_file="versions.json"
    if isfile(json_file): rm(json_file)
    execute('scp morphonet@morphonet.org:'+join(builds_path_mn,json_file)+' .')
    if isfile(json_file):  # Read JSON file
        with open(json_file) as data_file:
            jsonversion = json.load(data_file)
    else: jsonversion = {}  # Initialize

    newversion = {}
    newversion["name"] = version + '-' + gitversion
    newversion["build"] = branch
    newversion["version"] = version
    newversion["git"] = gitversion
    newversion["os"] = osmn
    newversion["unity"]=UnityVersion
    if osmn=="OSX": newversion["architecture"] = architecture
    if architecture == "GPU": newversion["architecture"] = architecture
    newversion["path"] = dist_path + '/' + basename(filename)
    newversion["date"] = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    newversion["author"] = 'CI-' + platform.platform()

    idx=version + '-' + gitversion + '-' + osmn
    if architecture=="silicon":idx=version + '-' + gitversion + '-' + architecture
    if architecture == "GPU": idx = version + '-' + gitversion + '-' + osmn+"_"+architecture
    jsonversion[idx] = newversion #Add New Version

    # Write JSON file
    with open(json_file, 'w') as outfile:
        json.dump(jsonversion, outfile, indent=10, separators=(',', ': '))

    if not execute('scp versions.json morphonet@morphonet.org:/var/www/Builds/'):
        print(" --> failed to copy json version ")
        sys.exit(1)



def build_MN(Unity,osmn,projectPath,unitylog,Secrets,buildTarget):
    print(" Build MorphoNet for "+osmn)
    cmd=Unity
    if osmn=="WINDOWS": #Valide lience Manually first
        cmd = os.path.basename(Unity)
        os.chdir(os.path.dirname(Unity))
        cmd += " -projectPath " + projectPath
        cmd += " -batchmode"
        cmd += " -logFile  " + unitylog
        cmd += " -quit"
        cmd += " -nographics"
        cmd += " -username " + Secrets['unity-user'] + " -password " + Secrets['unity-passwd']
        cmd += " -executemethod BuildMorphoNet.Build_" + osmn
        cmd += " -buildReporter MorphoNet "
        cmd += ' -manualLicenseFile "' + join(os.path.dirname(Unity), 'Unity_v2020.x.ulf') + '"'
        cmd += " -buildTarget " + buildTarget
        #execute(cmd)

        os.chdir(os.path.dirname(Unity))
        cmd=os.path.basename(Unity)

    cmd+=" -projectPath "+projectPath
    cmd+=" -batchmode"
    cmd+=" -logFile  "+unitylog
    cmd+=" -quit"
    cmd+=" -nographics"
    cmd+=" -username " +Secrets['unity-user']+" -password "+Secrets['unity-passwd']
    cmd+=" -executemethod BuildMorphoNet.Build_"+osmn
    if osmn=="WINDOWS": cmd+="_XR"
    cmd+=" -buildReporter MorphoNet "
    cmd+=" -buildTarget "+buildTarget
    execute(cmd)



def read_secrets(filename="/Users/ci/secrets.txt"):
    if not isfile(filename): filename = "/builds/secrets.txt"  # LINUX
    if not isfile(filename): filename = join("C:\\", "Users", "ci", "secrets.txt")  # WINDOWS
    if not isfile(filename): filename = "/Users/morphoci/secrets.txt"  # M1
    if not isfile(filename): filename = "build/secrets.txt"  # Local
    #Read Password and Users
    Secrets={}
    for line in open(filename,"r"):
        Secrets[line.strip().split(";")[0]]=line.strip().split(";")[1]
    return Secrets


#OSX Sign and notratize
def recurisve_sign_OSX(entitlements,path,Secrets,stdout=True):
    for f in os.listdir(path):
        sign_OSX(entitlements,join(path,f),Secrets,deep=False,stdout=stdout)
        if isdir(join(path, f)):
            recurisve_sign_OSX(entitlements,join(path, f),Secrets,stdout=stdout)

def sign_OSX(entitlements,filename,Secrets,deep=True,stdout=True):
    deeps=" --deep " if deep else ""
    add="" if stdout else " > /dev/null 2>&1 "
    execute('codesign '+deeps+' --force --verify --verbose --timestamp --options runtime --entitlements  ' + entitlements+ ' --sign "'+Secrets['developer']+'" ' + filename+ add,v=False)

def ditto(app,filename):
    execute('ditto -c -k --keepParent "' +app+ '" '+filename)
    if not os.path.isfile(filename):
        print(" ZIP app morphonet failed , not exist " + filename)
        sys.exit(1)

def xcodebuild(project,MorphoNet):
    execute('xcodebuild -project ' + project + " -target  "+MorphoNet)

def notarize(Secrets,filename):
    execute('xcrun notarytool submit  --wait --apple-id  '+Secrets['apple-login']+' --password '+Secrets['apple-passwd']+' --team-id '+Secrets['asc-provider']+'  ' + filename)



def push_build(branch,osmn,morphonet_zip,architecture="",option=' -o ProxyCommand="ssh -o StrictHostKeychecking=no UserKnownHostsFile=/dev/null  -W %h:%p efaure@jumpbox.lirmm.fr" -o StrictHostKeychecking=no UserKnownHostsFile=/dev/null ', server='ci@193.49.107.243'):
    if architecture == "silicon":osmn=architecture
    if architecture == "GPU": osmn = osmn + "_" + architecture
    deploy_path = "/Users/ci/Desktop/builds/" + str(branch.lower()) + "/" + str(osmn) + "/"
    execute('ssh '+option+server+' "mkdir -p ' + deploy_path+'"')
    execute("scp "+option+" " + morphonet_zip + "  "+server+":" + deploy_path)



def get_build(branch,osmn,pyname,architecture="",option=' -o ProxyCommand="ssh -o StrictHostKeychecking=no -W %h:%p efaure@jumpbox.lirmm.fr" -o StrictHostKeychecking=no ', server='ci@193.49.107.243'):
    if architecture == "silicon":osmn=architecture
    if architecture == "GPU": osmn =osmn+"_" +architecture
    deploy_path = "/Users/ci/Desktop/builds/" + str(branch.lower()) + "/" + str(osmn) + "/"
    execute('scp '+option+server+':' + join(deploy_path,pyname)+ " "+pyname)
    execute("tar -xf "+pyname)
