# How To Build the MorphoNet Unity Project  ?
Notice : The API is now build automaticaly wit gitlab runner


### Create a group runner on : LINUX (https://gitlab.inria.fr/groups/MorphoNet/-/runners) 
- In Continous Integration tools (https://ci.inria.fr/project/morphonet/slaves)
- create a VM with 8Gb, 4 Cores, 100Gb, unbutn 18
- Connect to Slave (ssh ci@...)

# FOR Unity Build
- sudo apt-get update
- sudo apt install ubuntu-desktop #Install deskopt needed to configure unity 
- sudo service gdm3 start
- donwload unity hub

# FOR API build
- sudo apt-get install pandoc # For building the API documentation 
- install miniconda for python 3.9 (https://docs.conda.io/en/latest/miniconda.html) 
- conda create -n morphonet_env python==3.9
- conda activate morphonet_env
- pip install pyinstaller==5.12.0
- pip install psutil==5.9.5
- pip install scikit-image==0.20.0
- pip install tensorflow==2.11.0
- pip install stardist==0.8.3
- pip install vtk==9.2.6
- pip install nibabel==4.0.2

#### FOR OSX  M1
- conda env create -f python-environment-silicon.yml

### CONFIGURE GROUP RUNNER 
- sudo apt-get install curl
- Download the binary for your system sudo : ```sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64  ```
- Give it permission to execute : ``` sudo chmod +x /usr/local/bin/gitlab-runner ```
- Create a GitLab Runner user : ```sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash ``` 
- Install as a service : ```sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner ```
- Run the service : ``` sudo gitlab-runner start ```
- Register the tokein : ``` sudo gitlab-runner register --url https://gitlab.inria.fr/ --registration-token $REGISTRATION_TOKEN ```
- add tag linux ! 
- Choose shell as the runner 


## Prepare the runner (log on the ssh ci@ of your machine) 
### LINUX
- sudo rm -r /home/gitlab-runner/.bash_logout # To avoid issue of failing environnment prepration  )
- sudo mkdir /home/gitlab-runner/.ssh/
- sudo chown ci:ci /home/gitlab-runner/.ssh
- scp id_rsa <runnerapi>:/home/gitlab-runner/.ssh/ # FROM your comptuter The key correspond to the one on MorphoNet.org)
- echo "morphonet.org ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILkfXNQw7qNCe0+FjvySGMmaAxIw5wMXX1CEauAJ17hl" > /home/gitlab-runner/.ssh/known_hosts  #(To avoid asking if server is ok )
- scp /Users/efaure/Projects/160606-MorphoNet/GIT/build/secrets.txt <runnerapi>:/builds/ 
- pico /builds/conda_init.sh
    __conda_setup="$('/builds/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
    eval "$__conda_setup"
- mkdir /builds/builds #PREPARE TEMPORARY DATA TO KEEP BULDS BETWENN PROJECT 
- chmod 777 /builds/builds


#### OSX 
- Create a ci user 
- scp id_rsa <runnerapi>:/Users/ci/.ssh/ # FROM your comptuter The key correspond to the one on MorphoNet.org)
- echo "morphonet.org ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILkfXNQw7qNCe0+FjvySGMmaAxIw5wMXX1CEauAJ17hl" > /Users/ci/.ssh/known_hosts  #(To avoid asking if server is ok )
- scp /Users/efaure/Projects/160606-MorphoNet/GIT/build/secrets.txt <runnerapi>:/Users/ci/ 
- scp /Users/efaure/Projects/160606-MorphoNet/GIT/build/DeveloperIDApplicationEmmanuelFaure(2589P67X47).cer <runnerapi>:/Users/ci/  # AND LOAD IT IN THE TROUSSEAU AS SYSTEM KEY
- scp /Users/efaure/Projects/160606-MorphoNet/GIT/build/Certificat.p12 <runnerapi>:/Users/ci/  # AND LOAD IT IN THE TROUSSEAU AS SYSTEM KEY




#### ACTIVE UNITY LICENCE FOR gitlab-runner (which we cannot log, in Linux, Unity has some trouble to have 2 users) 
- active unity with ci user
- sudo mkdir -p /home/gitlab-runner/.local/share/unity3d/Unity
- sudo cp /builds/.local/share/unity3d/Unity/Unity_lic.ulf  /home/gitlab-runner/.local/share/unity3d/Unity
- sudo chown -R gitlab-runner:gitlab-runner /home/gitlab-runner/.local/share/unity3d
- rm -rf /tmp/GiCache 


##### FOR OSX NOTARIZATION
- add in certificat in the keychain 
