using UnityEngine;
using MorphoNet;

public class XRButtonTestScript : MonoBehaviour
{
#if UNITY_EDITOR

    public void OnLongPressIn() => MorphoDebug.LogWarning("OnLongPressIn");

    public void OnLongPressOut() => MorphoDebug.LogWarning("OnLongPressOut");

    public void OnLongPressFinish() => MorphoDebug.LogWarning("OnLongPressFinish");

    public void OnPressIn() => MorphoDebug.LogWarning("OnPressIn");

    public void OnPressOut() => MorphoDebug.LogWarning("OnPressOut");

    public void OnToggleOn() => MorphoDebug.LogWarning("OnToggleOn");

    public void OnToggleOff() => MorphoDebug.LogWarning("OnToggleOff");

#endif
}