using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{

    public Transform anchor;
    public MorphoNet.XR.XRController ctrl;
    void Update()
    {
        transform.position = anchor.position;
        transform.LookAt(transform.position + ctrl.GetControllerForwardDirection());
    }
}
