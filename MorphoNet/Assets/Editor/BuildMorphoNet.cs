using System;
using System.IO;
using System.Text;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Build.Reporting;
using UnityEditor.XR.Management;
using UnityEngine;

/// <summary>
/// Editor class handling build generation and options.
/// </summary>
public class BuildMorphoNet
{
    #region Nested Types

    private static class OSs
    {
        public const string Ios = "IOS";
        public const string Osx = "OSX";
        public const string Linux = "LINUX";
        public const string Windows = "WINDOWS";
        public const string OculusStandalone = "OCULUS_STANDALONE";
        public const string WindowsXR = "WINDOWS_XR";
        public const string Android = "ANDROID";
        public const string WebGL = "WebGL";
    }

    #endregion Nested Types

    #region Scene path

    /// <summary>
    /// Base path for all scenes. Relative to the Unity project root directory.
    /// </summary>
    private const string ScenesBasePath = "Assets/Scenes/";

    /// <summary>
    /// Extension of Unity Scene.
    /// </summary>
    private const string ScenesExtension = ".unity";

    // Buildable scene names.
    private const string LoadParameterScene = ScenesBasePath + "LoadParameters" + ScenesExtension;

    private const string EmbryoScene = ScenesBasePath + "SceneEmbryo" + ScenesExtension;

    private const string MorphonetVRScene = ScenesBasePath + "MorphonetVR" + ScenesExtension;

    #endregion Scene path

    #region Constant parameters

    private const string GOOGLE_PLAY_KEYS_PATH = "GooglePlayKeys.txt";

    #endregion Constant parameters

    #region Build path

    /// <summary>
    /// Folder in wich store the builds. Relative to the Unity project root directory.
    /// </summary>
    private const string BuildRootPath = "../Unity_Build/";

    #endregion Build path

    #region Build methods

    /// <summary>
    /// Build Morphonet for the given OS.
    /// </summary>
    /// <param name="whichOS">
    /// The OS to target :
    /// <list type="bullet">
    /// <item>IOS</item>
    /// <item>OSX</item>
    /// <item>WINDOWS : include a VR Scene to the build.</item>
    /// <item>ANDROID</item>
    /// <item>WebGL</item>
    /// </list>
    /// </param>
    public static void Build_OS(string whichOS)
    {
        BuildPlayerOptions buildPlayerOptions = SetCustomBuildOptions(whichOS);

#if UNITY_EDITOR
        EditorUserBuildSettings.SwitchActiveBuildTarget(buildPlayerOptions.targetGroup, buildPlayerOptions.target);
#endif

        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        BuildSummary summary = report.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
        }

        if (summary.result == BuildResult.Failed)
        {
            Debug.Log("Build failed");
        }
    }

    private static BuildPlayerOptions SetCustomBuildOptions(string whichOS)
    {
        StringBuilder logText = new StringBuilder();
        logText.AppendLine("----------");
        logText.AppendLine($"Build configuration for {whichOS} at {DateTime.UtcNow.ToString("HH:mm:ss, yyyy-MM-dd")}");

        PlayerSettings.colorSpace = ColorSpace.Gamma;

        SetXROptions(whichOS, logText);

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
        {
            scenes = new[] { LoadParameterScene, EmbryoScene },
            locationPathName = $"{BuildRootPath}{whichOS}/MorphoNet",
            targetGroup = BuildTargetGroup.Standalone,
            options = BuildOptions.None
        };

        switch (whichOS)
        {
            case OSs.Ios:
                EditorUserBuildSettings.SetPlatformSettings("OSXUniversal", "CreateXcodeProject", "true");
                buildPlayerOptions.targetGroup = BuildTargetGroup.iOS;
                buildPlayerOptions.target = BuildTarget.iOS;
                break;

            case OSs.Osx:
                EditorUserBuildSettings.SetPlatformSettings("OSXUniversal", "CreateXcodeProject", "true");
                buildPlayerOptions.target = BuildTarget.StandaloneOSX;
                break;

            case OSs.Linux:
                buildPlayerOptions.locationPathName = $"{BuildRootPath}{whichOS}/MorphoNet/MorphoNet";
                buildPlayerOptions.target = BuildTarget.StandaloneLinux64;
                break;

            case OSs.Windows:

                buildPlayerOptions.locationPathName = $"{BuildRootPath}{whichOS}/MorphoNet/MorphoNet.exe";
                buildPlayerOptions.target = BuildTarget.StandaloneWindows64;
                break;

            case OSs.OculusStandalone:
                PlayerSettings.colorSpace = ColorSpace.Linear;

                buildPlayerOptions.locationPathName = $"{BuildRootPath}{whichOS}/MorphoNetVR/MorphoNetVR_Oculus.apk";
                buildPlayerOptions.scenes = new[] { LoadParameterScene, EmbryoScene, MorphonetVRScene };
                buildPlayerOptions.targetGroup = BuildTargetGroup.Android;
                buildPlayerOptions.target = BuildTarget.Android;
                break;

            case OSs.WindowsXR:
                PlayerSettings.colorSpace = ColorSpace.Gamma;

                buildPlayerOptions.locationPathName = $"{BuildRootPath}WINDOWS/MorphoNet/MorphoNet.exe";
                buildPlayerOptions.scenes = new[] { LoadParameterScene, EmbryoScene, MorphonetVRScene };
                buildPlayerOptions.target = BuildTarget.StandaloneWindows64;
                break;

            case OSs.Android:
                PlayerSettings.colorSpace = ColorSpace.Linear;
                EditorUserBuildSettings.buildAppBundle = true;
                buildPlayerOptions.locationPathName = $"{BuildRootPath}{whichOS}/MorphoNet.aab";
                buildPlayerOptions.targetGroup = BuildTargetGroup.Android;
                buildPlayerOptions.target = BuildTarget.Android;

                if (!LoadGoogleAPIKeys())
                {
                    Debug.LogWarning("Warning : could not Load Google API Keys in build options setup");
                }


                break;

            case OSs.WebGL:
                buildPlayerOptions.locationPathName = $"{BuildRootPath}{whichOS}/MorphoNet/MorphoNet";
                buildPlayerOptions.targetGroup = BuildTargetGroup.WebGL;
                buildPlayerOptions.target = BuildTarget.WebGL;
                break;
        }

        logText.AppendLine($"Color Space: {PlayerSettings.colorSpace}");

        File.AppendAllText(Path.Combine(BuildRootPath, "buildConfigurationLog.txt"), logText.ToString());

        return buildPlayerOptions;
    }

    private static bool LoadGoogleAPIKeys()
    {
        string credentials = Path.Combine(Application.streamingAssetsPath, GOOGLE_PLAY_KEYS_PATH);

        if (System.IO.File.Exists(credentials))
        {
            Dictionary<string, string> options = readCredentials(credentials);
            if (options.TryGetValue("keystoreName", out string keystoreName) && !string.IsNullOrEmpty(keystoreName))
            {
                PlayerSettings.Android.keystoreName = Path.Combine(Application.streamingAssetsPath, keystoreName);
                if (System.IO.File.Exists(PlayerSettings.Android.keystoreName))
                {
                    Debug.Log(" Found keystore " + PlayerSettings.Android.keystoreName);
                }
                else Debug.LogWarning(" Miss keystore " + PlayerSettings.Android.keystoreName);
            }
            if (options.TryGetValue("keystorePass", out string keystorePass) && !string.IsNullOrEmpty(keystorePass))
                PlayerSettings.Android.keystorePass = keystorePass;
            if (options.TryGetValue("keyaliasName", out string keyaliasName) && !string.IsNullOrEmpty(keyaliasName))
                PlayerSettings.Android.keyaliasName = keyaliasName;
            if (options.TryGetValue("keyaliasPass", out string keyaliasPass) && !string.IsNullOrEmpty(keyaliasPass))
                PlayerSettings.Android.keyaliasPass = keyaliasPass;

            PlayerSettings.Android.useCustomKeystore = true;
        }
        else
        {
            Debug.LogWarning(" Miss file credentials " + credentials);
            return false;
        }

        // LOAD BUNDLE VERSION CODE (NEED TO BE INCREMENTE EACH TIME)
        string bundleVersionCode = Path.Combine(Application.streamingAssetsPath, "bundleVersionCode.txt");
        if (System.IO.File.Exists(bundleVersionCode))
        {
            StreamReader reader = new StreamReader(bundleVersionCode);
            if (int.TryParse(reader.ReadToEnd().Replace("\n", ""), out var end_value))
                PlayerSettings.Android.bundleVersionCode = end_value;
            reader.Close();
            return true;
        }
        else
        {
            Debug.LogWarning(" Miss file credentials " + credentials);
        }
            
        return false;

    }

    static Dictionary<string, string>  readCredentials(string path)
    {
        Dictionary<string, string> options=new Dictionary<string, string> ();
        FileInfo theSourceFile = new FileInfo (path);
        StreamReader reader = theSourceFile.OpenText();
        string text;
        do
        {
            text = reader.ReadLine();
            if (text != null){
                string []tab=text.Split(char.Parse("="));
                if(tab.Length==2)  options[tab[0]]=tab[1];
            }
        } while (text != null);          
        return options;
    }

    private static void SetXROptions(string whichOS, StringBuilder logText)
    {
        var manager = XRGeneralSettingsPerBuildTarget
                    .XRGeneralSettingsForBuildTarget(BuildTargetGroup.Standalone)
                    .Manager;

        bool openXRLoaderActive = false;
        switch (whichOS)
        {
            case OSs.WindowsXR:

                foreach (var loader in manager.activeLoaders)
                    if (loader is UnityEngine.XR.OpenXR.OpenXRLoader)
                        openXRLoaderActive = true;

                if (!openXRLoaderActive)
                {
                    var openXRLoader = new UnityEngine.XR.OpenXR.OpenXRLoader();

                    var success = manager.TryAddLoader(openXRLoader);
                    logText.AppendLine($"{openXRLoader.name} addition : {(success ? "success" : "failure")}");
                }
                break;

            case OSs.OculusStandalone:

                foreach (var loader in manager.activeLoaders)
                {
                    if (loader is UnityEngine.XR.OpenXR.OpenXRLoader)
                        openXRLoaderActive = true;
                }

                if (!openXRLoaderActive)
                {
                    var openXRLoader = new UnityEngine.XR.OpenXR.OpenXRLoader();

                    var success = manager.TryAddLoader(openXRLoader);
                    logText.AppendLine($"{openXRLoader.name} addition : {(success ? "success" : "failure")}");
                }
                break;

            default:
                int loaderCount = manager.activeLoaders.Count;
                for (int i = loaderCount - 1; i >= 0; i--)
                {
                    var loader = manager.activeLoaders[i];

                    if (loader is UnityEngine.XR.OpenXR.OpenXRLoader)
                    {
                        var success = manager.TryRemoveLoader(loader);
                        logText.AppendLine($"{loader.name} removal : {(success ? "success" : "failure")}");
                    }
                }
                break;
        }

        if (manager != null)
        {
            logText.AppendLine($"Active Loaders: {manager.activeLoaders.Count}");

            foreach (var loader in manager.activeLoaders)
                logText.AppendLine($"    {(loader == null ? "null" : loader.name)}");
        }
    }

    #endregion Build methods

    #region Editor methods

    [MenuItem("Build/Build IOS")]
    public static void Build_IOS() => Build_OS(OSs.Ios);

    [MenuItem("Build/Build OSX")]
    public static void Build_OSX() => Build_OS(OSs.Osx);

    [MenuItem("Build/Build LINUX")]
    public static void Build_LINUX() => Build_OS(OSs.Linux);

    [MenuItem("Build/Build WINDOWS")]
    public static void Build_WINDOWS() => Build_OS(OSs.Windows);

    [MenuItem("Build/Build WINDOWS (with VR)")]
    public static void Build_WINDOWS_XR() => Build_OS(OSs.WindowsXR);

    [MenuItem("Build/Build OCULUS")]
    public static void Build_OCULUS() => Build_OS(OSs.OculusStandalone);

    [MenuItem("Build/Build ANDROID")]
    public static void Build_ANDROID() => Build_OS(OSs.Android);

    [MenuItem("Build/Build WebGL")]
    public static void Build_WebGL() => Build_OS(OSs.WebGL);

    [MenuItem("Build/Setup/IOS")]
    public static void Setup_IOS() => SetCustomBuildOptions(OSs.Ios);

    [MenuItem("Build/Setup/OSX")]
    public static void Setup_OSX() => SetCustomBuildOptions(OSs.Osx);

    [MenuItem("Build/Setup/LINUX")]
    public static void Setup_LINUX() => SetCustomBuildOptions(OSs.Linux);

    [MenuItem("Build/Setup/WINDOWS")]
    public static void Setup_WINDOWS() => SetCustomBuildOptions(OSs.Windows);

    [MenuItem("Build/Setup/WINDOWS (with VR)")]
    public static void Setup_WINDOWS_XR() => SetCustomBuildOptions(OSs.WindowsXR);

    [MenuItem("Build/Setup/ANDROID")]
    public static void Setup_ANDROID() => SetCustomBuildOptions(OSs.Android);

    [MenuItem("Build/Setup/WebGL")]
    public static void Setup_WebGL() => SetCustomBuildOptions(OSs.WebGL);

    #endregion Editor methods
}
