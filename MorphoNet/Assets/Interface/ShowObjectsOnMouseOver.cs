﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ShowObjectsOnMouseOver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject[] to_open;
    public bool active = true;
    public Button the_button;
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!active) return;
        foreach (GameObject g in to_open)
        {
            g.SetActive(true);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!active) return;
        foreach (GameObject g in to_open)
        {
            g.SetActive(false);
        }
    }
}
