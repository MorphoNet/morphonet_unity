﻿using AssemblyCSharp;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorphoNet.Extensions.Unity;
using UnityEngine;
using UnityEngine.UI;

namespace MorphoNet
{
    public class SelectionManager : MonoBehaviour
    {
        #region Inner type

        private class StepColorComparer : IComparer<Color>
        {
            public int Compare(Color a, Color b)
            {
                Color.RGBToHSV(a, out float a_h, out float a_s, out float a_v);
                Color.RGBToHSV(b, out float b_h, out float b_s, out float b_v);

                int aquot = (int)(a_h * 360) / 45; //8 steps
                int bquot = (int)(b_h * 360) / 45;

                if (aquot < bquot)
                    return 1;
                else if (aquot == bquot)
                {
                    if (a_v < b_v)
                    {
                        return 1;
                    }
                    else
                    {
                        if (a_v == b_v)
                        {
                            if (a_s < b_s)
                            {
                                return 1;
                            }
                            else
                            {
                                if (a_s == b_s)
                                {
                                    return 0;
                                }
                                else
                                {
                                    return -1;
                                }
                            }
                        }
                        else
                        {
                            return -1;
                        }
                    }
                }
                else
                    return -1;
            }
        }

        #endregion Inner type

        public static string[] indexcolors = new string[] { "#B88183", "#922329", "#5A0007", "#D7BFC2", "#D86A78", "#FF8A9A", "#3B000A", "#E20027", "#943A4D", "#5B4E51", "#B05B6F", "#D83D66", "#895563", "#FF1A59", "#FFDBE5", "#CC0744", "#CB7E98", "#997D87", "#6A3A4C", "#FF2F80", "#6B002C", "#A74571", "#C6005A", "#FF5DA7", "#300018", "#B894A6", "#FF90C9", "#7C6571", "#A30059", "#DA007C", "#5B113C", "#402334", "#D157A0", "#DDB6D0", "#885578", "#962B75", "#A97399", "#D20096", "#E773CE", "#AA5199", "#E704C4", "#6B3A64", "#FFA0F2", "#6F0062", "#B903AA", "#C895C5", "#FF34FF", "#320033", "#DBD5DD", "#EEC3FF", "#BC23FF", "#671190", "#201625", "#F5E1FF", "#BC65E9", "#D790FF", "#72418F", "#4A3B53", "#9556BD", "#B4A8BD", "#7900D7", "#A079BF", "#958A9F", "#837393", "#64547B", "#3A2465", "#353339", "#BCB1E5", "#9F94F0", "#9695C5", "#0000A6", "#000035", "#636375", "#00005F", "#97979E", "#7A7BFF", "#3C3E6E", "#6367A9", "#494B5A", "#3B5DFF", "#C8D0F6", "#6D80BA", "#8FB0FF", "#0045D2", "#7A87A1", "#324E72", "#00489C", "#0060CD", "#789EC9", "#012C58", "#99ADC0", "#001325", "#DDEFFF", "#59738A", "#0086ED", "#75797C", "#BDC9D2", "#3E89BE", "#8CD0FF", "#0AA3F7", "#6B94AA", "#29607C", "#404E55", "#006FA6", "#013349", "#0AA6D8", "#658188", "#5EBCD1", "#456D75", "#0089A3", "#B5F4FF", "#02525F", "#1CE6FF", "#001C1E", "#203B3C", "#A3C8C9", "#00A6AA", "#00C6C8", "#006A66", "#518A87", "#E4FFFC", "#66E1D3", "#004D43", "#809693", "#15A08A", "#00846F", "#00C2A0", "#00FECF", "#78AFA1", "#02684E", "#C2FFED", "#47675D", "#00D891", "#004B28", "#8ADBB4", "#0CBD66", "#549E79", "#1A3A2A", "#6C8F7D", "#008941", "#63FFAC", "#1BE177", "#006C31", "#B5D6C3", "#3D4F44", "#4B8160", "#66796D", "#71BB8C", "#04F757", "#001E09", "#FFFF00", "#00B433", "#9FB2A4", "#003109", "#A3F3AB", "#456648", "#51A058", "#83A485", "#7ED379", "#D1F7CE", "#A1C299", "#061203", "#1E6E00", "#5EFF03", "#55813B", "#3B9700", "#4FC601", "#1B4400", "#C2FF99", "#788D66", "#868E7E", "#83AB58", "#374527", "#98D058", "#C6DC99", "#A4E804", "#76912F", "#8BB400", "#34362D", "#4C6001", "#DFFB71", "#6A714A", "#222800", "#6B7900", "#3A3F00", "#BEC459", "#FEFFE6", "#A3A489", "#9FA064", "#FFFF00", "#61615A", "#FFFFFE", "#9B9700", "#CFCDAC", "#797868", "#575329", "#FFF69F", "#8D8546", "#F4D749", "#7E6405", "#1D1702", "#CCAA35", "#CCB87C", "#453C23", "#513A01", "#FFB500", "#A77500", "#D68E01", "#B79762", "#7A4900", "#372101", "#886F4C", "#A45B02", "#E7AB63", "#FAD09F", "#C0B9B2", "#938A81", "#A38469", "#D16100", "#A76F42", "#5B4534", "#5B3213", "#CA834E", "#FF913F", "#953F00", "#D0AC94", "#7D5A44", "#BE4700", "#FDE8DC", "#772600", "#A05837", "#EA8B66", "#391406", "#FF6832", "#C86240", "#29201D", "#B77B68", "#806C66", "#FFAA92", "#89412E", "#E83000", "#A88C85", "#F7C9BF", "#643127", "#E98176", "#7B4F4B", "#1E0200", "#9C6966", "#BF5650", "#BA0900", "#FF4A46", "#F4ABAA", "#000000", "#452C2C", "#C8A1A1" };
        public static int[] colororder = new int[] { 76, 27, 189, 244, 5, 20, 151, 37, 180, 80, 101, 215, 206, 160, 113, 213, 25, 221, 61, 190, 22, 203, 100, 96, 111, 38, 94, 201, 195, 128, 71, 243, 120, 168, 34, 170, 237, 199, 90, 105, 36, 95, 182, 98, 216, 107, 70, 35, 84, 17, 155, 193, 89, 163, 50, 15, 197, 196, 85, 232, 14, 154, 166, 224, 249, 51, 246, 214, 236, 72, 104, 135, 147, 139, 198, 48, 106, 207, 126, 66, 1, 200, 161, 177, 41, 64, 152, 141, 186, 239, 172, 81, 10, 3, 121, 112, 132, 205, 194, 42, 54, 69, 143, 115, 229, 178, 162, 16, 68, 230, 238, 87, 118, 175, 97, 144, 114, 174, 234, 108, 60, 53, 13, 30, 21, 225, 210, 138, 58, 136, 102, 245, 231, 12, 220, 57, 59, 184, 171, 158, 252, 55, 39, 11, 125, 140, 187, 88, 157, 33, 4, 110, 130, 235, 192, 159, 2, 49, 153, 183, 242, 86, 185, 45, 52, 28, 211, 75, 145, 82, 156, 124, 164, 227, 62, 217, 218, 0, 73, 83, 129, 167, 208, 149, 8, 233, 133, 56, 142, 47, 92, 79, 103, 241, 74, 29, 240, 209, 223, 7, 250, 23, 18, 173, 188, 117, 9, 91, 131, 134, 169, 148, 228, 77, 165, 176, 219, 253, 202, 31, 67, 212, 119, 99, 251, 32, 109, 6, 123, 78, 222, 122, 204, 93, 127, 26, 247, 40, 254, 44, 150, 179, 65, 146, 181, 191, 137, 248, 43, 226, 19, 46, 24, 63, 116 };

        public static Material[] materials;
        public int SelectionValue = 1;
        public static int Static_selectionValue = 1;
        public GameObject SelectedColor;
        public GameObject SelectedColorRender;
        public InputField SelectionNumber;
        public InputField remap;
        public Slider SliderSelectionNumber;
        public static Dictionary<int, Vector3> centerBySelection;
        public static Dictionary<int, int> countBySelection;
        public GameObject MenuColor;

        public GameObject ThisSelection;
        public GameObject AllSelections;
        public GameObject MultiSelection;
        public GameObject NonSelected;
        public GameObject CurrentSelectionCount;
        public GameObject CurrentPickedCount;

        public GameObject buttonColormap;

        public GameObject MenuLoadSelection;

        public Toggle SelectedCellAllTimes;

        public GameObject This_selection_panel;
        public GameObject All_selection_panel;
        public GameObject Multi_selection_panel;
        public GameObject No_selection_panel;
        public GameObject Picked_selection_panel;

        public Button This_selection_background;
        public Button All_selection_background;
        public Button Multi_selection_background;
        public Button No_selection_background;
        public Button Picked_selection_background;

        public Color normalColor;
        public Color selectedColor;
        public Color highlightedColor;
        public Color pressedColor;
        public Color disabledColor;

        public Button ThisSelectionButton;

        public GameObject backgroundMultiColors;
        public GameObject menuMultiColors;
        public List<GameObject> MultiSelectRenderers;
        public List<int> MultiSelectIndex;
        public bool menu_state = false;
        public Dictionary<int, int> selection_count;

        private Dictionary<int, Image> _GridElements;

        public void PickThisSelection(int index)
        {
            UpdateSelectionColor(MultiSelectIndex[index]);
            menu_state = !menu_state;
            menuMultiColors.SetActive(menu_state);
            backgroundMultiColors.SetActive(menu_state);
        }

        public void LoadMultiSelection()
        {
            MultiSelectIndex = new List<int>();
            for (int i = 0; i < 10; i++)
            {
                int new_index = Random.Range(0, colororder.Length);
                while (MultiSelectIndex.Contains(new_index))
                {
                    new_index = Random.Range(0, colororder.Length);
                }

                MultiSelectIndex.Add(new_index);
                Material m = new Material(SetsManager.instance.Default);
                Color c = SelectionManager.getSelectedMaterial(new_index).color;
                m.UpdateColorProperty("_Color", c);
                MultiSelectRenderers[i].transform.GetChild(0).GetComponent<MeshRenderer>().material = m;
            }
        }

        public void ManageMultiSelectState()
        {
            menu_state = !menu_state;
            GenerateMultiLabelPanel();
            InterfaceManager.instance.multi_label_container.SetActive(menu_state);

            MenuLoadSelection.SetActive(false);
            MenuColor.SetActive(false);
        }

        public GameObject background;
        public GameObject selection_panel;

        public void DisableButtonPanels(GameObject panelActif, Button bttnActif)
        {
            This_selection_panel.SetActive(This_selection_panel == panelActif);
            All_selection_panel.SetActive(All_selection_panel == panelActif);
            Multi_selection_panel.SetActive(Multi_selection_panel == panelActif);
            No_selection_panel.SetActive(No_selection_panel == panelActif);
            Picked_selection_panel.SetActive(Picked_selection_panel == panelActif);

            Picked_selection_background.interactable = Picked_selection_background != bttnActif;
            This_selection_background.interactable = This_selection_background != bttnActif;
            All_selection_background.interactable = All_selection_background != bttnActif;
            ;
            Multi_selection_background.interactable = Multi_selection_background != bttnActif;
            ;
            No_selection_background.interactable = No_selection_background != bttnActif;

            if (panelActif == No_selection_panel)
            {
                InterfaceManager.instance.Menu_time_propagate.SetActive(false);
            }
            else
            {
                InterfaceManager.instance.Menu_time_propagate.SetActive(true);
            }
            background.SetActive(true);
            selection_panel.SetActive(true);
        }

        public void UpdateSpheresToColor()
        { 
            Color colorcurr = SelectionManager.getSelectedMaterial(SelectionValue).color;
            for (int i = 1; i <= 5; i++)
            {
                Color current = getSelectedMaterial(i).color;
                if (i < InterfaceManager.instance.all_selections_spheres.Count)
                    InterfaceManager.instance.all_selections_spheres[i].material.color = current;
            }
            InterfaceManager.instance.this_selection_sphere.material.color = colorcurr;
        }

        public void showThisSelectionPanel()
        {
            DisableButtonPanels(This_selection_panel, This_selection_background);
            InterfaceManager.instance.time_windows.SetActive(true);
            UpdateSpheresToColor();
        }

        public void showPickedSelectionPanel()
        {
            DisableButtonPanels(Picked_selection_panel, Picked_selection_background);
            InterfaceManager.instance.time_windows.SetActive(true);
            UpdateSpheresToColor();
        }

        public void showAllSelectionPanel()
        {
            DisableButtonPanels(All_selection_panel, All_selection_background);
            InterfaceManager.instance.time_windows.SetActive(true);
            UpdateSpheresToColor();
        }

        public void showMultiSelectionPanel()
        {
            DisableButtonPanels(Multi_selection_panel, Multi_selection_background);
            InterfaceManager.instance.time_windows.SetActive(true);
            UpdateSpheresToColor();
        }

        public void showNoSelectionPanel()
        {
            DisableButtonPanels(No_selection_panel, No_selection_background);
            InterfaceManager.instance.time_windows.SetActive(true);
            UpdateSpheresToColor();
        }

        private void Start()
        {

            InterfaceManager.setButtonColors(This_selection_background);
            InterfaceManager.setButtonColors(All_selection_background);
            InterfaceManager.setButtonColors(Multi_selection_background);
            InterfaceManager.setButtonColors(No_selection_background);

            ThisSelectionButton.Select();
            centerBySelection = new Dictionary<int, Vector3>();
            countBySelection = new Dictionary<int, int>();
            if (materials == null)
                materials = new Material[indexcolors.Length];
            if (materials[0] == null)
                createMaterial(0);
            MenuColor.SetActive(false);
            UpdateSelectionColor(1);

            if (MorphoTools.GetDataset().id_dataset==0)
            {
                buttonColormap.SetActive(false);
            }
            else
            {
                buttonColormap.SetActive(true);
            }
        }

        public void ChangeSelectionTo(int v)
        {
            UpdateSelectionColor(v);
            menu_state = false;
            InterfaceManager.instance.multi_label_container.SetActive(menu_state);
        }

        public void GenerateMultiLabelPanel()
        {
            if (_GridElements == null)
                _GridElements = new Dictionary<int, Image>();

            Dictionary<int, Color> select_colors = new Dictionary<int, Color>();
            for (int i = 1; i < 256; i++)
            {
                select_colors.Add(i, getSelectedMaterial(i).color);
            }

            List<KeyValuePair<int, Color>> select_colors_ordered = select_colors.OrderBy(key => key.Value, new StepColorComparer()).ToList();
            foreach (KeyValuePair<int, Color> pair in select_colors_ordered)
            {
                if (!_GridElements.ContainsKey(pair.Key))
                {
                    GameObject select_object = GameObject.Instantiate(InterfaceManager.instance.example_object, InterfaceManager.instance.multi_label_grid.transform);
                    Image im = select_object.GetComponentsInChildren<Image>()[0];
                    im.color = pair.Value;
                    int b = pair.Key;
                    select_object.GetComponentsInChildren<Button>()[0].onClick.AddListener(() => { ChangeSelectionTo(b); });
                    select_object.SetActive(true);
                    _GridElements.Add(b, im);
                }
                else
                {
                    _GridElements[pair.Key].color = pair.Value;
                }
            }
        }

        public static Vector3 getCentersByGroup(int selection)
        {
            return centerBySelection[selection];
        }

        public static int getCountByGroup(int selection)
        {
            return countBySelection[selection];
        }

        public void OnEnabled()
        {
            UpdateNumberCells();
        }

        public static void createMaterial(int i)
        {
            string hexcol = indexcolors[colororder[i]];
            Color newColor = new Color();
            ColorUtility.TryParseHtmlString(hexcol, out newColor);
            materials[i] = new Material(SetsManager.instance.ColorDefault);
            materials[i].UpdateColorProperty("_Color", newColor);
        }

        public void ChangeColorValue()
        { //From Input Field
            float vv;
            if (float.TryParse(SelectionNumber.text, out vv))
            {
                int v = (int)Mathf.Round(vv);
                UpdateSelectionColor(v);
            }
        }

        public void onReMapValue()
        {
            onReMapValue_Coroutine();
        }

        public void onReMapValue_Coroutine()
        { //From Input Field remap
            float vv;
            if (float.TryParse(remap.text, out vv))
            {
                int v = (int)Mathf.Round(vv);
                int Previous = SelectionValue;
                UpdateSelectionColor(v);
                if (SelectionValue != Previous)
                    for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
                    {
                        foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
                        {
                            if (cell.selection != null && cell.selection.Contains(Previous))
                            {
                                cell.removeSelection(Previous, false);
                                cell.addSelection(SelectionValue);
                            }
                        }
                    }
#if UNITY_WEBGL
            if (Lineage.isLineage) Lineage.UpdateSelectionCells(Previous, SelectionValue);
#else
                MorphoTools.SendCellsColorToLineage();
#endif

            }

            UpdateThisNumberCells();
        }

        public void NextSelectionClick()
        { //From Slider
            int v = SelectionValue + 1;
            if (v <= 255 && v >= 1)
                UpdateSelectionColor(v);
            UpdateSpheresToColor();
        }

        public void PreviousSelectionClick()
        { //From Slider
            
            int v = SelectionValue - 1;
            if (v <= 255 && v >= 1)
                UpdateSelectionColor(v);
            UpdateSpheresToColor();
        }

        public void ChangeSliderValue()
        { //From Slider
            int v = (int)Mathf.Round(SliderSelectionNumber.GetComponent<UnityEngine.UI.Slider>().value);
            UpdateSelectionColor(v);
        }

        //When changing the selection value (from slider or input field or as selected)
        public void UpdateSelectionColor(int v)
        {
            if (v < 1)
                v = 1;
            SliderSelectionNumber.GetComponent<UnityEngine.UI.Slider>().value = (float)v;
            SelectionNumber.text = v.ToString();
            SelectionValue = v;
            Static_selectionValue = v;
            if (materials[v - 1] == null)
                createMaterial(v - 1);
            SelectedColorRender.GetComponent<Renderer>().material = materials[v - 1];
            SelectedColor.GetComponent<Renderer>().material = materials[v - 1];
            remap.text = v.ToString();
            UpdateNumberCells();
        }

        //After closing the colormenu
        //After validating a new color from Menu colors
        public void changeMaterialSet(Material m)
        {
            for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
                {
                    if (cell.selection != null && cell.selection.Contains(SelectionValue))
                        cell.updateShader();
                }
            }
            materials[SelectionValue - 1] = m;
            UpdateSelectionColor(SelectionValue);
            MorphoTools.SendCellsColorToLineage();
        }

        //After changing materials from else where (load selection) reaffect all materials
        public void resetAllMaterials()
        {
            for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
                {
                    if (cell.selection != null && cell.selection.Count > 0)
                        cell.updateShader();
                }
            }
            UpdateSelectionColor(SelectionValue);
        }

        //Return the material assign to a cell
        public static Material getSelectedMaterial(int v)
        {
            while (v > indexcolors.Length)
                v -= indexcolors.Length;
            if (v <= 0)
                return SetsManager.instance.Default;
            if (materials == null)
                materials = new Material[indexcolors.Length];

            if (materials[v - 1] == null)
                createMaterial(v - 1);
            return materials[v - 1];
        }

        public void selectAsSelected()
        {
            int v = MorphoTools.GetPickedManager().GetSelection();
            if (v >= 1 && v <= 255)
                UpdateSelectionColor(v);
        }

        public void ChangeColorSelection()
        { //Open the menu color
            if (MenuColor.activeSelf)
                MenuColor.SetActive(false);
            else
            {
                MenuColor.GetComponent<ChangeColorSelection>().InitColor(SelectedColor.GetComponent<Renderer>().material);
                MenuColor.SetActive(true);
                InterfaceManager.instance.multi_label_container.SetActive(false);
                InterfaceManager.instance.ShaderOptionsMenu.SetActive(false);
                InterfaceManager.instance.ShaderAdvancedOptionsMenu.SetActive(false);
                InterfaceManager.instance.keep_selected_labels.gameObject.SetActive(false);
                InterfaceManager.instance.ObjectsColorManager.RemovePresets();
                menu_state = false;
                MenuLoadSelection.SetActive(false);
                InterfaceManager.instance.ObjectsColorManager.SetPrevisMaterial(SelectedColor.GetComponent<Renderer>().material);
                InterfaceManager.instance.ObjectsColorManager.UpdatePickerColor();
                InterfaceManager.instance.ObjectsColorManager.UpdateAllMats();
                MenuColor.GetComponent<ChangeColorSelection>().SetCurrentShaderSelection(SelectedColor.GetComponent<MeshRenderer>().material);
            }
        }

        private Dictionary<int, List<Cell>> cells_for_selection = new Dictionary<int, List<Cell>>();
        private List<Cell> cells = new List<Cell>();

        public void StopFuturePropagation()
        {
            int current_time = MorphoTools.GetDataset().CurrentTime;
            int current_selection = SelectionValue;
            cells_for_selection = new Dictionary<int, List<Cell>>();

            for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
            { //For all cells selected
                if (MorphoTools.GetPickedManager().clickedCells[i] != null && MorphoTools.GetPickedManager().clickedCells[i].Daughters != null)
                    foreach (Cell b in MorphoTools.GetPickedManager().clickedCells[i].Daughters)
                    {
                        if ((InterfaceManager.instance.isUntilDivision(b, b, true) || InterfaceManager.instance.isInCustomTimeRange(b.t) || !InterfaceManager.instance.isTimeModificationActive()))
                        {
                            if (b.selection != null && b.selection.Count > 0)
                            {
                                b.RemoveAllSelection(false);
                            }
                        }
                        CancelSelectionOnChild(b, b, current_selection);
                    }
            }
#if UNITY_WEBGL
            if (Lineage.isLineage)
            {
                Lineage.CancelPropagationMultiNew(true);
            }
#else
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        public void StopFuturePropagationThisSelect()
        {
            int current_time = MorphoTools.GetDataset().CurrentTime;
            int current_selection = SelectionValue;

            cells_for_selection = new Dictionary<int, List<Cell>>();

            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[current_time])
            {
                //For all cells selected
                if (cell != null && cell.Daughters != null)
                    foreach (Cell b in cell.Daughters)
                    {
                        if ((InterfaceManager.instance.isUntilDivision(b, b, true) || InterfaceManager.instance.isInCustomTimeRange(b.t) || !InterfaceManager.instance.isTimeModificationActive()) && b.t >= current_time && b.selection != null && b.selection.Count >= 0 && b.selection.Contains(current_selection))
                        {
                            b.removeSelection(current_selection, false);
                        }
                        CancelSelectionOnChild(b, b, current_selection);
                    }
            }
#if UNITY_WEBGL
            if (Lineage.isLineage)
            {
                Lineage.CancelPropagationThis(current_selection, cells, "select", true);
            }
#else
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        public int getSelectionFromColor(Color c)
        {
            for (int i = 1; i < 256; i++)
            {
                Material m = getSelectedMaterial(i);
                if (m.color == c)
                {
                    return i;
                }
            }
            return -1;
        }

        public void StopFuturePropagationMultiSelect()
        {
            int current_time = MorphoTools.GetDataset().CurrentTime;
            int current_selection = SelectionValue;

            cells_for_selection = new Dictionary<int, List<Cell>>();

            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[current_time])
            {
                //For all cells selected
                if (cell != null && cell.Daughters != null)
                {
                    foreach (Cell b in cell.Daughters)
                    {
                        if ((InterfaceManager.instance.isUntilDivision(b, b, true) || InterfaceManager.instance.isInCustomTimeRange(b.t) || !InterfaceManager.instance.isTimeModificationActive()) && b.t >= current_time && b.selection != null && b.selection.Count >= 2)
                        {
                            b.RemoveAllSelection(false);
                        }
                        CancelMultiSelectionOnChild(b, b);
                    }
                }
            }
#if UNITY_WEBGL
            if (Lineage.isLineage)
            {
                Lineage.CancelPropagationMultiNew(true);
            }
#else
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        public void StopFuturePropagationAll()
        {
            cells_for_selection = new Dictionary<int, List<Cell>>();

            int current_time = MorphoTools.GetDataset().CurrentTime;

            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[current_time])
            {
                //For all cells selected
                if (cell != null && cell.Daughters != null)
                    foreach (Cell b in cell.Daughters)
                    {
                        if (InterfaceManager.instance.isUntilDivision(b, b, true) || (InterfaceManager.instance.isInCustomTimeRange(b.t) || !InterfaceManager.instance.isTimeModificationActive()) && b.t >= current_time && b.selection != null && b.selection.Count > 0)
                        {
                            b.RemoveAllSelection(false);
                        }
                        CancelAllSelectionOnChild(b, b);
                    }
            }
#if UNITY_WEBGL
            if (Lineage.isLineage)
            {
                Lineage.CancelPropagationAll(true);
            }
#else
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        public void CancelAllSelectionOnChild(Cell cell, Cell source)
        {
            if (cell != null && cell.Daughters != null && cell.Daughters.Count > 0)
            {
                foreach (Cell b in cell.Daughters)
                {
                    if (InterfaceManager.instance.isUntilDivision(source, cell, true) || InterfaceManager.instance.isInCustomTimeRange(b.t) || !InterfaceManager.instance.isTimeModificationActive())
                    {
                        if (b.selection != null && b.selection.Count > 0)
                        {
                            b.RemoveAllSelection(false);
                        }
                    }

                    CancelAllSelectionOnChild(b, source);
                }
            }
        }

        public void CancelMultiSelectionOnChild(Cell cell, Cell source)
        {
            if (cell != null && cell.Daughters != null && cell.Daughters.Count > 0)
            {
                foreach (Cell b in cell.Daughters)
                {
                    if (InterfaceManager.instance.isUntilDivision(source, cell, true) || InterfaceManager.instance.isInCustomTimeRange(b.t) || !InterfaceManager.instance.isTimeModificationActive())
                    {
                        if (b.selection != null && b.selection.Count > 1)
                        {
                            b.RemoveAllSelection(false);
                        }
                    }

                    CancelMultiSelectionOnChild(b, source);
                }
            }
        }

        public void CancelSelectionOnChild(Cell cell, Cell source, int current_selection)
        {
            if (cell != null && cell.Daughters != null && cell.Daughters.Count > 0)
            {
                foreach (Cell b in cell.Daughters)
                {
                    if (InterfaceManager.instance.isUntilDivision(source, cell, true) || InterfaceManager.instance.isInCustomTimeRange(b.t) || !InterfaceManager.instance.isTimeModificationActive())
                    {
                        if (b.selection != null && b.selection.Count > 0 && b.selection.Contains(current_selection))
                        {
                            b.removeSelection(current_selection, false);
                        }
                    }
                    CancelSelectionOnChild(b, source, current_selection);
                }
            }
        }

        public void StopPastPropagation()
        {
            cells_for_selection = new Dictionary<int, List<Cell>>();

            int current_time = MorphoTools.GetDataset().CurrentTime;
            int current_selection = SelectionValue;

            for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
            { //For all cells selected
                if (MorphoTools.GetPickedManager().clickedCells[i] != null && MorphoTools.GetPickedManager().clickedCells[i].Mothers != null)
                {
                    foreach (Cell b in MorphoTools.GetPickedManager().clickedCells[i].Mothers)
                    {
                        if ((InterfaceManager.instance.isUntilDivision(b, b, false) || InterfaceManager.instance.isInCustomTimeRange(b.t) || !InterfaceManager.instance.isTimeModificationActive()))
                        {
                            if (b.selection != null && b.selection.Count > 0)
                            {
                                b.RemoveAllSelection(false);
                            }
                        }
                        CancelPastSelectionOnChild(b, b, current_selection);
                    }
                }

            }
#if UNITY_WEBGL
            if (Lineage.isLineage)
            {
               Lineage.cancelPropagationPicked(false);
            }
#else
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        public void StopPastPropagationThisSelect()
        {
            int current_time = MorphoTools.GetDataset().CurrentTime;
            int current_selection = SelectionValue;

            cells_for_selection = new Dictionary<int, List<Cell>>();

            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[current_time])
            {
                //For all cells selected
                if (cell != null && cell.Mothers != null)
                {
                    foreach (Cell b in cell.Mothers)
                    {
                        if ((InterfaceManager.instance.isUntilDivision(b, b, false) || InterfaceManager.instance.isInCustomTimeRange(b.t) || !InterfaceManager.instance.isTimeModificationActive()) && b.t <= current_time && b.selection != null && b.selection.Count >= 0 && b.selection.Contains(current_selection))
                        {
                            b.removeSelection(current_selection, false);
                        }
                        CancelPastSelectionOnChild(b, b, current_selection);
                    }
                }
            }
#if UNITY_WEBGL
            if (Lineage.isLineage)
            {
                Lineage.CancelPropagationThis(current_selection, cells, "select", false);
            }
#else
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        public void StopPastPropagationMultiSelect()
        {
            int current_time = MorphoTools.GetDataset().CurrentTime;

            cells_for_selection = new Dictionary<int, List<Cell>>();

            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[current_time])
            {
                //For all cells selected
                if (cell != null && cell.Mothers != null)
                {
                    foreach (Cell b in cell.Mothers)
                    {
                        if ((InterfaceManager.instance.isUntilDivision(b, b, false) || InterfaceManager.instance.isInCustomTimeRange(b.t) || !InterfaceManager.instance.isTimeModificationActive()) && b.t <= current_time && b.selection != null && b.selection.Count >= 2)
                        {
                            b.RemoveAllSelection(false);
                        }
                        CancelPastMultiSelectionOnChild(b, b);
                    }
                }
            }
#if UNITY_WEBGL
            if (Lineage.isLineage)
            {
                Lineage.CancelPropagationMultiNew(false);
            }
#else
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        public void StopPastPropagationAll()
        {
            int current_time = MorphoTools.GetDataset().CurrentTime;

            cells_for_selection = new Dictionary<int, List<Cell>>();

            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[current_time])
            {
                //For all cells selected
                if (cell != null && cell.Mothers != null)
                {
                    foreach (Cell b in cell.Mothers)
                    {
                        if ((InterfaceManager.instance.isUntilDivision(b, b, false) || InterfaceManager.instance.isInCustomTimeRange(b.t) || !InterfaceManager.instance.isTimeModificationActive()) && b.t <= current_time && b.selection != null && b.selection.Count > 0)
                        {
                            b.RemoveAllSelection(false);
                        }
                        CancelPastAllSelectionOnChild(b, b);
                    }
                }
            }

#if UNITY_WEBGL
            if (Lineage.isLineage)
            {
                Lineage.CancelPropagationAll(false);
            }
#else
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        public void CancelPastAllSelectionOnChild(Cell cell, Cell source)
        {
            if (cell != null && cell.Mothers != null && cell.Mothers.Count > 0)
            {
                foreach (Cell b in cell.Mothers)
                {
                    if (InterfaceManager.instance.isUntilDivision(source, cell, false) || InterfaceManager.instance.isInCustomTimeRange(b.t) || !InterfaceManager.instance.isTimeModificationActive())
                    {
                        if (b.selection != null && b.selection.Count > 0)
                        {
                            b.RemoveAllSelection(false);
                            //MorphoTools.AddCellToLineageCells(b);
                        }
                    }
                    CancelPastAllSelectionOnChild(b, source);
                }
            }
        }

        public void CancelPastMultiSelectionOnChild(Cell cell, Cell source)
        {
            if (cell != null && cell.Mothers != null && cell.Mothers.Count > 0)
            {
                foreach (Cell b in cell.Mothers)
                {
                    if (InterfaceManager.instance.isUntilDivision(source, cell, false) || InterfaceManager.instance.isInCustomTimeRange(b.t) || !InterfaceManager.instance.isTimeModificationActive())
                    {
                        if (b.selection != null && b.selection.Count > 1)
                        {
                            b.RemoveAllSelection(false);
                            //MorphoTools.AddCellToLineageCells(b);
                        }
                    }
                    CancelPastMultiSelectionOnChild(b, source);
                }
            }
        }

        public void CancelPastSelectionOnChild(Cell cell, Cell source, int current_selection)
        {
            if (cell != null && cell.Mothers != null && cell.Mothers.Count > 0)
            {
                foreach (Cell b in cell.Mothers)
                {
                    if (InterfaceManager.instance.isUntilDivision(source, cell, false) || InterfaceManager.instance.isInCustomTimeRange(b.t) || !InterfaceManager.instance.isTimeModificationActive())
                    {
                        if (b.selection != null && b.selection.Contains(current_selection))
                        {
                            b.removeSelection(current_selection, false);
                            //MorphoTools.AddCellToLineageCells(b);
                        }
                    }
                    CancelPastSelectionOnChild(b, source, current_selection);
                }
            }
        }

        //When we click on ok to applly this selection to the selected cell
        public void applySelection()
        {
            UpdateSpheresToColor();
            if (SelectionValue == 0)
                SelectionValue = 1;

            for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
            {
                if (MorphoTools.GetPickedManager().clickedCells[i].selection == null)
                    MorphoTools.GetPickedManager().clickedCells[i].selection = new List<int>();
                if (!MorphoTools.GetPickedManager().clickedCells[i].selection.Contains(SelectionValue))
                {
                    MorphoTools.GetPickedManager().clickedCells[i].selection.Add(SelectionValue);

                    MorphoTools.GetPickedManager().clickedCells[i].updateShader();
                }
                            //MorphoTools.AddCellToLineageCellsMorphoTools.GetPickedManager().clickedCells[i]);
#if UNITY_WEBGL

                if (Lineage.isLineage)
                    Lineage.AddToColor(MorphoTools.GetPickedManager().clickedCells[i], Lineage.GetCellColor_No_Selection(MorphoTools.GetPickedManager().clickedCells[i]));
#endif
            }
#if UNITY_WEBGL
            if (Lineage.isLineage)
                Lineage.UpdateWebsiteLineage();
#else
            MorphoTools.SendCellsColorToLineage();
#endif

            UpdateNumberCells();

            //finally clear picked , if user want to clear them (toggle)
            if (!InterfaceManager.instance.keep_selected_labels.isOn)
            {
                MorphoTools.GetPickedManager().ResetSelection();
            }
            
        }

        public void SelectionUsed()
        {
        }

        public void clearThis()
        {
            clearThis_Coroutine();
        }

        //Clear selection for all cells with this selection number at this time step
        public void clearThis_Coroutine()
        {

#if UNITY_WEBGL
        if (Lineage.isLineage) { Lineage.clearThisSelected(SelectionValue); }
#endif
            //MorphoTools.ResetLineageUpdatingCells();


            if (!InterfaceManager.instance.until_next_division)
            {
                for (int t = SetsManager.instance.DataSet.MinTime; t <= SetsManager.instance.DataSet.MaxTime; t++)
                {
                    if (InterfaceManager.instance.isInCustomTimeRange(t) || (t == SetsManager.instance.DataSet.CurrentTime && !InterfaceManager.instance.isTimeModificationActive()))
                    {
                        clearThisAt(t);
                    }
                }
            }
            else if (InterfaceManager.instance.until_next_division)
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[SetsManager.instance.DataSet.CurrentTime])
                {
                    clearInChild(cell, cell);
                    clearInMothers(cell, cell);
                }
            }
#if !UNITY_WEBGL
            MorphoTools.SendCellsColorToLineage();
#endif

            UpdateNumberCells();
        }

        public void clearThisAt(int t)
        {
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
            {
                if (cell.selection != null && cell.selection.Contains(SelectionValue))
                {
                    cell.removeSelection(SelectionValue, add_to_lineage: false);
                    //MorphoTools.AddCellToLineageCells(c);
                }
            }
        }

        public void clearInChild(Cell source, Cell cell)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, true) && cell.selection != null && cell.selection.Contains(SelectionValue))
            {
                cell.removeSelection(SelectionValue, add_to_lineage: false);
                //MorphoTools.AddCellToLineageCells(c);
            }
            if (cell.Daughters != null)
            {
                foreach (Cell b in cell.Daughters)
                {
                    clearInChild(source, b);
                }
            }
        }

        public void clearInMothers(Cell source, Cell cell)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, false) && cell.selection != null && cell.selection.Contains(SelectionValue))
            {
                cell.removeSelection(SelectionValue, add_to_lineage: false);
                //MorphoTools.AddCellToLineageCells(c);
            }
            if (cell.Mothers != null)
            {
                foreach (Cell b in cell.Mothers)
                {
                    clearInMothers(source, b);
                }
            }
        }

        public void clearAll()
        {
            Clearall_Start();
        }

        //Clear selection for all cells with all selection number at this time step
        public void Clearall_Start()
        {

#if UNITY_WEBGL
        if (Lineage.isLineage) { Lineage.clearAll(); }
#endif
            //MorphoTools.ResetLineageUpdatingCells();

            if (!InterfaceManager.instance.until_next_division)
            {
                for (int t = SetsManager.instance.DataSet.MinTime; t <= SetsManager.instance.DataSet.MaxTime; t++)
                {
                    if (InterfaceManager.instance.isInCustomTimeRange(t) || (t == SetsManager.instance.DataSet.CurrentTime && !InterfaceManager.instance.isTimeModificationActive()))
                    {
                        clearAllAt(t);
                    }
                }
            }
            else
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[SetsManager.instance.DataSet.CurrentTime])
                {
                    clearAllInChild(cell, cell);
                    clearAllInMothers(cell, cell);
                }
            }
#if !UNITY_WEBGL
            MorphoTools.SendCellsColorToLineage();
#endif


            UpdateNumberCells();
        }

        public void clearAllInChild(Cell source, Cell cell)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, true) && cell.selection != null && cell.selection.Count > 0)
            {
                cell.RemoveAllSelection(add_to_lineage: false);
                ////MorphoTools.AddCellToLineageCells(c);
            }
            if (cell.Daughters != null)
            {
                foreach (Cell b in cell.Daughters)
                {
                    clearAllInChild(source, b);
                }
            }
        }

        public void clearAllInMothers(Cell source, Cell cell)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, false) && cell.selection != null && cell.selection.Count > 0)
            {
                cell.RemoveAllSelection(add_to_lineage: false);
                ////MorphoTools.AddCellToLineageCells(c);
            }
            if (cell.Mothers != null)
            {
                foreach (Cell b in cell.Mothers)
                {
                    clearAllInMothers(source, b);
                }
            }
        }

        public void clearAllAt(int t)
        {
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
            {
                cell.RemoveAllSelection(add_to_lineage: false);
                ////MorphoTools.AddCellToLineageCells(c);
            }
        }

        public void Start_clearAllColors()
        {
            clearAllColors();
        }

        public void clearAllColors()
        { //Cear Seectopn and colors !-all times step

            //MorphoTools.ResetLineageUpdatingCells();
#if UNITY_WEBGL
        if (Lineage.isLineage) { Lineage.clearAll(); }
#endif

            InterfaceManager.instance.ColorMapScale.gameObject.SetActive(false);
            for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
                {
                    cell.RemoveUploadColor();
                    cell.RemoveAllSelection(add_to_lineage: false);
                }
            }
#if !UNITY_WEBGL
            MorphoTools.SendCellsColorToLineage();
#endif


            UpdateNumberCells();
        }

        public void clearPicked()
        {
            clearPicked_Coroutine();
        }

        public void clearPicked_Coroutine()
        {
#if UNITY_WEBGL
        if (Lineage.isLineage) { Lineage.clearPicked(); }
#endif
            //MorphoTools.ResetLineageUpdatingCells();

            if (!InterfaceManager.instance.until_next_division)
            {
                foreach (Cell cell in MorphoTools.GetPickedManager().clickedCells)
                {
                    clearPickedInTimeDaughters(cell, cell);
                    clearPickedInTimeMothers(cell, cell);
                }
            }
            else
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[SetsManager.instance.DataSet.CurrentTime])
                {
                    if (MorphoTools.GetPickedManager().clickedCells.Contains(cell))
                    {
                        clearPickedInChild(cell, cell);
                        clearPickedInMothers(cell, cell);
                    }
                }
            }

            UpdateNumberCells();
        }

        public void clearPickedInChild(Cell source, Cell cell)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, true) && cell.selection != null && cell.selection.Count > 0)
            {
                cell.RemoveAllSelection(add_to_lineage: false);
            }
            if (cell.Daughters != null)
            {
                foreach (Cell b in cell.Daughters)
                {
                    clearPickedInChild(source, b);
                }
            }
        }

        public void clearPickedInMothers(Cell source, Cell cell)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, false) && cell.selection != null && cell.selection.Count > 0)
            {
                cell.RemoveAllSelection(add_to_lineage: false);
            }
            if (cell.Mothers != null)
            {
                foreach (Cell b in cell.Mothers)
                {
                    clearPickedInMothers(source, b);
                }
            }
        }

        public void clearPickedInTimeMothers(Cell source, Cell cell)
        {
            if ((InterfaceManager.instance.isInCustomTimeRange(cell.t) || (cell.t == SetsManager.instance.DataSet.CurrentTime && !InterfaceManager.instance.isTimeModificationActive())) && cell.selection != null && cell.selection.Count > 0)
            {
                cell.RemoveAllSelection(add_to_lineage: false);
            }
            if (cell.Mothers != null)
            {
                foreach (Cell b in cell.Mothers)
                {
                    clearPickedInTimeMothers(source, b);
                }
            }
        }

        public void clearPickedInTimeDaughters(Cell source, Cell cell)
        {
            if ((InterfaceManager.instance.isInCustomTimeRange(cell.t) || (cell.t == SetsManager.instance.DataSet.CurrentTime && !InterfaceManager.instance.isTimeModificationActive())) && cell.selection != null && cell.selection.Count > 0)
            {
                cell.RemoveAllSelection(add_to_lineage: false);
            }
            if (cell.Daughters != null)
            {
                foreach (Cell b in cell.Daughters)
                {
                    clearPickedInTimeDaughters(source, b);
                }
            }
        }

        public void clearMulti()
        {
            clearMulti_Coroutine();
        }

        //Clear selection for all cells with all selection number at this time step
        public void clearMulti_Coroutine()
        {
#if UNITY_WEBGL
        if (Lineage.isLineage) { Lineage.clearMultiNew(); }
#endif
            //MorphoTools.ResetLineageUpdatingCells();

            if (!InterfaceManager.instance.until_next_division)
            {
                for (int t = SetsManager.instance.DataSet.MinTime; t <= SetsManager.instance.DataSet.MaxTime; t++)
                {
                    if (InterfaceManager.instance.isInCustomTimeRange(t) || (t == SetsManager.instance.DataSet.CurrentTime && !InterfaceManager.instance.isTimeModificationActive()))
                    {
                        clearMultiAt(t);
                    }
                }
            }
            else
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[SetsManager.instance.DataSet.CurrentTime])
                {
                    clearMultiInChild(cell, cell);
                    clearMultiInMothers(cell, cell);
                }
            }

#if !UNITY_WEBGL
            MorphoTools.SendCellsColorToLineage();
#endif

            UpdateNumberCells();
        }

        public void clearMultiAt(int t)
        {
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
            {
                if (cell.selection != null && cell.selection.Count >= 2)
                {
                    cell.RemoveAllSelection(add_to_lineage: false);
                }
            }
        }

        public void clearMultiInChild(Cell source, Cell cell)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, true) && cell.selection != null && cell.selection.Count > 1)
            {
                cell.RemoveAllSelection(add_to_lineage: false);
            }
            if (cell.Daughters != null)
            {
                foreach (Cell b in cell.Daughters)
                {
                    clearMultiInChild(source, b);
                }
            }
        }

        public void clearMultiInMothers(Cell source, Cell cell)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, false) && cell.selection != null && cell.selection.Count > 1)
            {
                cell.RemoveAllSelection(add_to_lineage: false);
            }
            if (cell.Mothers != null)
            {
                foreach (Cell b in cell.Mothers)
                {
                    clearMultiInMothers(source, b);
                }
            }
        }

        //Propagation selection
        public void propagateThisPast()
        {
            //MorphoTools.ResetLineageUpdatingCells();
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
            {
                if (cell.selection != null && cell.selection.Contains(SelectionValue))
                {
                    cell.PropagateMother(SelectionValue, cell);
                }
            }
#if UNITY_WEBGL
        if (Lineage.isLineage) { Lineage.propagateThis(false, SelectionValue, cells, "select"); }
#else
            MorphoTools.SendCellsColorToLineage();
#endif

            UpdateNumberCells();
        }

        public void showPickedCells()
        {
            showPickedCells_Coroutine();
        }

        public void showPickedCells_Coroutine()
        {

#if UNITY_WEBGL
        if (Lineage.isLineage) { Lineage.showCellList(MorphoTools.GetPickedManager().clickedCells, "picked"); }
#endif
            //MorphoTools.ResetLineageUpdatingCells();

            List<Cell> backup_clicked = new List<Cell>(MorphoTools.GetPickedManager().clickedCells);
            foreach (Cell cell in backup_clicked)
            {
                ShowDaughters(cell, false);
                ShowMothers(cell, false);
            }
#if !UNITY_WEBGL
            MorphoTools.SendCellsVisibilityToLineage();
#endif

        }

        public void hidePickedCells()
        {
            hidePickedCells_Coroutine();
        }

        public void hidePickedCells_Coroutine()
        {
#if UNITY_WEBGL
        if (Lineage.isLineage) { Lineage.hideCellList(MorphoTools.GetPickedManager().clickedCells, "picked"); }
#endif

            List<Cell> backup_clicked = new List<Cell>(MorphoTools.GetPickedManager().clickedCells);
            foreach (Cell cell in backup_clicked)
            {
                ShowDaughters(cell, true);
                ShowMothers(cell, true);
            }
#if !UNITY_WEBGL
            MorphoTools.SendCellsVisibilityToLineage();
#endif

        }

        public void ShowDaughters(Cell cell, bool hide)
        {
            if (InterfaceManager.instance.isInCustomTimeRange(cell.t) || (!InterfaceManager.instance.isTimeModificationActive() && cell.t == MorphoTools.GetDataset().CurrentTime))
            {
                cell.hide(hide);
            }

            if (cell != null && cell.Daughters != null && cell.Daughters.Count > 0)
            {
                foreach (Cell b in cell.Daughters)
                {
                    ShowDaughters(b, hide);
                }
            }
        }

        public void ShowMothers(Cell cell, bool hide)
        {
            if (InterfaceManager.instance.isInCustomTimeRange(cell.t) || (!InterfaceManager.instance.isTimeModificationActive() && cell.t == MorphoTools.GetDataset().CurrentTime))
            {
                cell.hide(hide);
            }

            if (cell != null && cell.Mothers != null && cell.Mothers.Count > 0)
            {
                foreach (Cell b in cell.Mothers)
                {
                    ShowMothers(b, hide);
                }
            }
        }

        public void pickFreeSelection()
        {
            int free_select = -1;
            bool[] selection_by_cell = new bool[256];
            for (int i = 1; i < 256; i++)
            {
                selection_by_cell[i] = false;
            }

            for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
                {
                    if (cell != null && cell.selection != null)
                    {
                        foreach (int s in cell.selection)
                        {
                            if (s < 256 && s > 0 && selection_by_cell[s] == false)
                            {
                                selection_by_cell[s] = true;
                            }
                        }
                    }
                }
            }

            for (int i = 1; i < 256; i++)
            {
                if (selection_by_cell[i] == false && i >= SelectionValue)
                {
                    free_select = i;
                    break;
                }
            }

            if (free_select != -1)
                UpdateSelectionColor(free_select);
        }

        public void propagateFutur()
        {
            if (This_selection_panel.activeSelf)
            {
                propagateThisFutur();
            }
            else if (All_selection_panel.activeSelf)
            {
                propagateAllFutur();
            }
            else if (Multi_selection_panel.activeSelf)
            {
                propagateMultiFutur();
            }
            else if (Picked_selection_panel)
            {
                propagateAllPickedFutur();
            }
        }

        public void stopPropagateFutur()
        {
            if (This_selection_panel.activeSelf)
            {
                StopFuturePropagationThisSelect();
            }
            else if (All_selection_panel.activeSelf)
            {
                StopFuturePropagationAll();
            }
            else if (Multi_selection_panel.activeSelf)
            {
                StopFuturePropagationMultiSelect();
            }
            else if (Picked_selection_panel)
            {
                StopFuturePropagation();
            }
        }

        public void stopPropagatePast()
        {
            if (This_selection_panel.activeSelf)
            {
                StopPastPropagationThisSelect();
            }
            else if (All_selection_panel.activeSelf)
            {
                StopPastPropagationAll();
            }
            else if (Multi_selection_panel.activeSelf)
            {
                StopPastPropagationMultiSelect();
            }
            else if (Picked_selection_panel)
            {
                StopPastPropagation();
            }
        }

        public void propagatePast()
        {
            if (This_selection_panel.activeSelf)
            {
                propagateThisPast();
            }
            else if (All_selection_panel.activeSelf)
            {
                propagateAllPast();
            }
            else if (Multi_selection_panel.activeSelf)
            {
                propagateMultiPast();
            }
            else if (Picked_selection_panel)
            {
                propagateAllPickedPast();
            }
        }

        public void propagateThisFutur()
        {
            List<Cell> cell_lineage = new List<Cell>();
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
            {
                if (cell.selection != null && cell.selection.Contains(SelectionValue))
                {
                    cell.PropagateDaughters(SelectionValue, cell);
                    cell_lineage.Add(cell);
                }
            }

#if UNITY_WEBGL
        if (Lineage.isLineage) { Lineage.propagateThis(true, SelectionValue, cell_lineage, "select"); }
#else
            MorphoTools.SendCellsColorToLineage();
#endif


            UpdateNumberCells();
        }

        public void propagateAllPast()
        {
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
            {
                if (cell.selection != null && cell.selection.Count > 0)
                    foreach (int s in cell.selection)
                    {
                        cell.PropagateMother(s, cell);
                    }
            }
#if UNITY_WEBGL
            if (Lineage.isLineage) Lineage.propagateAll(false);
#else
            MorphoTools.SendCellsColorToLineage();
#endif

            UpdateNumberCells();
        }

        public void propagateAllPickedPast()
        {
            foreach (Cell cell in MorphoTools.GetPickedManager().clickedCells)
            {
                if (cell.selection != null && cell.selection.Count > 0)
                {
                    foreach (int selectionId in cell.selection)
                    {
                        cell.PropagateMother(selectionId, cell);
                    }
                }
            }

#if UNITY_WEBGL
            if (Lineage.isLineage) Lineage.propagatePicked(false);
#else
            MorphoTools.SendCellsColorToLineage();
#endif


            UpdateNumberCells();
        }

        public void propagateAllFutur()
        {
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
            {
                if (cell.selection != null && cell.selection.Count > 0)
                    foreach (int s in cell.selection)
                    {
                        cell.PropagateDaughters(s, cell);
                    }
            }
#if UNITY_WEBGL
            if (Lineage.isLineage) Lineage.propagateAll(true);
#else
            MorphoTools.SendCellsColorToLineage();
#endif

            UpdateNumberCells();
        }

        public void propagateAllPickedFutur()
        {
            foreach (var itemc in MorphoTools.GetPickedManager().clickedCells)
            {
                Cell c = itemc;
                if (c.selection != null && c.selection.Count > 0)
                    foreach (int s in c.selection)
                    {
                        c.PropagateDaughters(s, c);
                    }
            }

#if UNITY_WEBGL
            if (Lineage.isLineage) Lineage.propagatePicked(true);
#else
            MorphoTools.SendCellsColorToLineage();
#endif


            UpdateNumberCells();
        }

        public void propagateMultiPast()
        {
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
            {
                if (cell.selection != null && cell.selection.Count > 1)
                    foreach (int s in cell.selection)
                    {
                        cell.PropagateMother(s, cell);
                    }
            }
#if UNITY_WEBGL
        if (Lineage.isLineage) Lineage.propagateMulti(false);
#else
            MorphoTools.SendCellsColorToLineage();
#endif

            UpdateNumberCells();
        }

        public void propagateMultiFutur()
        {
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
            {
                if (cell.selection != null && cell.selection.Count > 1)
                    foreach (int s in cell.selection)
                    {
                        cell.PropagateDaughters(s, cell);
                    }
            }

#if UNITY_WEBGL
        if (Lineage.isLineage) Lineage.propagateMulti(true);
#else
            MorphoTools.SendCellsColorToLineage();
#endif

            UpdateNumberCells();
        }

        //Hide or Show Cells
        public void hideThis()
        {
            applyhideThis(true);
        }

        public void showThis()
        {
            applyhideThis(false);
        }

        public void applyhideThis(bool v)
        {
            if (!InterfaceManager.instance.until_next_division)
            {
                for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
                {
                    if (InterfaceManager.instance.isInCustomTimeRange(t) || (!InterfaceManager.instance.isTimeModificationActive() && t == SetsManager.instance.DataSet.CurrentTime))
                    {
                        applyhideThisAt(v, t);
                    }
                }
            }
            else
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
                {
                    hideThisInChild(cell, cell, v);
                    hideThisInMothers(cell, cell, v);
                }
            }

#if UNITY_WEBGL
            if (Lineage.isLineage) { Lineage.hideSelection(SelectionValue, v); }
#else
            MorphoTools.SendCellsVisibilityToLineage();
#endif

        }

        public void hideThisInChild(Cell source, Cell cell, bool hide)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, true) && cell.selection != null && cell.selection.Contains(SelectionValue))
            {
                cell.hide(hide, false);
            }
            if (cell.Daughters != null)
            {
                foreach (Cell b in cell.Daughters)
                {
                    hideThisInChild(source, b, hide);
                }
            }
        }

        public void hideThisInMothers(Cell source, Cell cell, bool hide)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, false) && cell.selection != null && cell.selection.Contains(SelectionValue))
            {
                cell.hide(hide, false);
            }
            if (cell.Mothers != null)
            {
                foreach (Cell b in cell.Mothers)
                {
                    hideThisInMothers(source, b, hide);
                }
            }
        }

        public void applyhideThisAt(bool v, int t)
        {
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
            {
                if (cell.selection != null && cell.selection.Contains(SelectionValue))
                {
                    cell.hide(v, false);
                }
            }
        }

        public void pickBySelection()
        {
            pickSelections();
        }

        public void hideBySelections()
        {
            applyhideSelections(true);
        }

        public void showBySelections()
        {
            applyhideSelections(false);
        }

        public void pickSelections()
        {
            List<int> selection = new List<int>();
            List<Cell> to_pick = new List<Cell>();

            foreach (Cell cell in MorphoTools.GetPickedManager().clickedCells)
            {
                if (cell.selection != null)
                {
                    foreach (int i in cell.selection)
                    {
                        if (!selection.Contains(i))
                        {
                            selection.Add(i);
                        }
                    }
                }
            }

            for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
            {
                if (InterfaceManager.instance.isInCustomTimeRange(t) || (!InterfaceManager.instance.isTimeModificationActive() && t == SetsManager.instance.DataSet.CurrentTime))
                {
                    foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
                    {
                        if (cell.selection != null
                            && cell.selection.Intersect(selection).Any()
                            && !MorphoTools.GetPickedManager().clickedCells.Contains(cell)
                            && !to_pick.Contains(cell))
                        {
                            to_pick.Add(cell);
                        }
                    }
                }
            }

            foreach (Cell b in to_pick)
            {
                MorphoTools.GetPickedManager().AddCellSelected(b);
            }
#if !UNITY_WEBGL
            MorphoTools.SendCellsPickedToLineage();
#endif
        }

        public void applyhideSelections(bool v)
        {
            List<int> selection = new List<int>();
            foreach (Cell c in MorphoTools.GetPickedManager().clickedCells)
            {
                if (c.selection != null)
                {
                    foreach (int i in c.selection)
                    {
                        if (!selection.Contains(i))
                        {
                            selection.Add(i);
                        }
                    }
                }
            }
            for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
            {
                if (InterfaceManager.instance.isInCustomTimeRange(t) || (!InterfaceManager.instance.isTimeModificationActive() && t == SetsManager.instance.DataSet.CurrentTime))
                {
                    applyhideSelectionsAt(v, t, selection);
                }
            }
#if !UNITY_WEBGL
            MorphoTools.SendCellsVisibilityToLineage();
#endif

        }

        public void applyhideSelectionsAt(bool v, int t, List<int> selects)
        {
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
            {
                if (cell.selection != null && cell.selection.Intersect(selects).Any())
                    cell.hide(v);
            }
        }

        public void hideAll()
        {
            applyhideAll(true);
        }

        public void showAll()
        {
            applyhideAll(false);
        }

        public void applyhideAll(bool v)
        {
            if (!InterfaceManager.instance.until_next_division)
            {
                for (int t = SetsManager.instance.DataSet.MinTime; t <= SetsManager.instance.DataSet.MaxTime; t++)
                {
                    if (InterfaceManager.instance.isInCustomTimeRange(t) || (!InterfaceManager.instance.isTimeModificationActive() && t == SetsManager.instance.DataSet.CurrentTime))
                    {
                        applyhideAllAt(v, t);
                    }
                }
            }
            else
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
                {
                    hideAllInChild(cell, cell, v);
                    hideAllInMothers(cell, cell, v);
                }
            }
#if UNITY_WEBGL
            if (Lineage.isLineage) Lineage.applyhideAll(v);
#else
            MorphoTools.SendCellsVisibilityToLineage();
#endif

        }

        public void applyhideAllAt(bool v, int t)
        {
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
            {
                if (cell.selection != null && cell.selection.Count > 0)
                {
                    cell.hide(v, false);
                }
            }
        }

        public void hideAllInChild(Cell source, Cell cell, bool hide)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, true) && cell.selection != null && cell.selection.Count > 0)
            {
                cell.hide(hide, false);
            }
            if (cell.Daughters != null)
            {
                foreach (Cell b in cell.Daughters)
                {
                    hideAllInChild(source, b, hide);
                }
            }
        }

        public void hideAllInMothers(Cell source, Cell cell, bool hide)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, false) && cell.selection != null && cell.selection.Count > 0)
            {
                cell.hide(hide, false);
            }
            if (cell.Mothers != null)
            {
                foreach (Cell b in cell.Mothers)
                {
                    hideAllInMothers(source, b, hide);
                }
            }
        }

        public void hideMulti()
        {
            applyhideMulti(true);
        }

        public void showMulti()
        {
            applyhideMulti(false);
        }

        public void applyhideMulti(bool v)
        {
            if (!InterfaceManager.instance.until_next_division)
            {
                for (int t = SetsManager.instance.DataSet.MinTime; t <= SetsManager.instance.DataSet.MaxTime; t++)
                {
                    if (InterfaceManager.instance.isInCustomTimeRange(t) || (!InterfaceManager.instance.isTimeModificationActive() && t == SetsManager.instance.DataSet.CurrentTime))
                    {
                        applyhideMultiAt(v, t);
                    }
                }
            }
            else
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
                {
                    hideMultiInChild(cell, cell, v);
                    hideMultiInMothers(cell, cell, v);
                }
            }


#if UNITY_WEBGL
            if (Lineage.isLineage) Lineage.hideMultiple(v);
#else
            MorphoTools.SendCellsVisibilityToLineage();
#endif

        }

        public void applyhideMultiAt(bool v, int t)
        {
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
            {
                if (cell.selection != null && cell.selection.Count >= 2)
                {
                    cell.hide(v, false);
                }
            }
        }

        public void hideMultiInChild(Cell source, Cell cell, bool hide)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, true) && cell.selection != null && cell.selection.Count > 1)
            {
                cell.hide(hide, false);
            }
            if (cell.Daughters != null)
            {
                foreach (Cell b in cell.Daughters)
                {
                    hideMultiInChild(source, b, hide);
                }
            }
        }

        public void hideMultiInMothers(Cell source, Cell cell, bool hide)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, false) && cell.selection != null && cell.selection.Count > 1)
            {
                cell.hide(hide, false);
            }
            if (cell.Mothers != null)
            {
                foreach (Cell b in cell.Mothers)
                {
                    hideMultiInMothers(source, b, hide);
                }
            }
        }

        //Clear selection for all cells with all selection number at all time step
        public void hideNonSelected()
        {
            applyHideNonSelected(true);
        }

        public void showNonSelected()
        {
            applyHideNonSelected(false);
        }

        public void applyHideNonSelected(bool v)
        {
            if (!InterfaceManager.instance.until_next_division)
            {
                for (int t = SetsManager.instance.DataSet.MinTime; t <= SetsManager.instance.DataSet.MaxTime; t++)
                {
                    if (InterfaceManager.instance.isInCustomTimeRange(t) || (!InterfaceManager.instance.isTimeModificationActive() && t == SetsManager.instance.DataSet.CurrentTime))
                    {
                        applyHideNonSelectedAt(v, t);
                    }
                }
            }
            else
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
                {
                    hideFreeInChild(cell, cell, v);
                    hideFreeInMothers(cell, cell, v);
                }
            }
#if UNITY_WEBGL
          if (Lineage.isLineage) Lineage.hideFree(v);
#else
            MorphoTools.SendCellsVisibilityToLineage();
#endif

        }

        public void applyHideNonSelectedAt(bool v, int t)
        {
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
            {
                if (cell.selection == null || cell.selection.Count == 0)
                {
                    cell.hide(v, false);
                }
            }
        }

        public void hideFreeInChild(Cell source, Cell cell, bool hide)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, true) && (cell.selection == null || cell.selection.Count < 1))
            {
                cell.hide(hide, false);
            }
            if (cell.Daughters != null)
            {
                foreach (Cell b in cell.Daughters)
                {
                    hideFreeInChild(source, b, hide);
                }
            }
        }

        public void hideFreeInMothers(Cell source, Cell cell, bool hide)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, false) && (cell.selection == null || cell.selection.Count < 1))
            {
                cell.hide(hide, false);
            }
            if (cell.Mothers != null)
            {
                foreach (Cell b in cell.Mothers)
                {
                    hideFreeInMothers(source, b, hide);
                }
            }
        }

        /*public void removeFromThisSelection()
        {
            removedCells = new List<Cell>();
            for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
            {
                MorphoTools.GetPickedManager().clickedCells[i].removeSelection(SelectionValue);
                if (ThisAllTimes.isOn)
                {
                    RemoveSelectionFutur(MorphoTools.GetPickedManager().clickedCells[i], MorphoTools.GetPickedManager().clickedCells[i], SelectionValue, false);
                    RemoveSelectionPast(MorphoTools.GetPickedManager().clickedCells[i], MorphoTools.GetPickedManager().clickedCells[i], SelectionValue, false);
                }
                UpdateNumberCells();
            }
#if UNITY_WEBGL
            if (Lineage.isLineage) Lineage.UpdateWebsiteLineage();
#else
            MorphoTools.SendCellsColorToLineage();
#endif

        }*/

        public List<Cell> removedCells;

        public void RemoveSelectionFutur(Cell source, Cell cell, int selection_value, bool all_select)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, true) || InterfaceManager.instance.isInCustomTimeRange(cell) || !InterfaceManager.instance.isTimeModificationActive())
            {
                if (all_select)
                {
                    if (!removedCells.Contains(cell))
                    {
                        cell.RemoveAllSelection();
                    }
                }
                else
                {
                    if (!removedCells.Contains(cell))
                    {
                        cell.removeSelection(selection_value);
                    }
                }
            }
            if (cell.Daughters != null && cell.Daughters.Count > 0)
            {
                for (int i = 0; i < cell.Daughters.Count; i++)
                {
                    RemoveSelectionFutur(source, cell.Daughters[i], selection_value, all_select);
                }
            }
        }

        public void RemoveSelectionPast(Cell source, Cell cell, int selection_value, bool all_select)
        {
            if (InterfaceManager.instance.isUntilDivision(source, cell, false) || InterfaceManager.instance.isInCustomTimeRange(cell, false) || !InterfaceManager.instance.isTimeModificationActive())
            {
                if (all_select)
                {
                    if (!removedCells.Contains(cell))
                    {
                        cell.RemoveAllSelection();
                    }
                }
                else
                {
                    if (!removedCells.Contains(cell))
                    {
                        cell.removeSelection(selection_value);
                    }
                }
            }
            if (cell.Mothers != null && cell.Mothers.Count > 0)
            {
                for (int i = 0; i < cell.Mothers.Count; i++)
                {
                    RemoveSelectionPast(source, cell.Mothers[i], selection_value, all_select);
                }
            }
        }

        public void removeFromAllSelectionAllTimes()
        {
            List<Cell> local_removedCells = new List<Cell>(MorphoTools.GetPickedManager().clickedCells.Count);
            removedCells = new List<Cell>();
            for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
            {
                MorphoTools.GetPickedManager().clickedCells[i].RemoveAllSelection();
                RemoveSelectionFutur(MorphoTools.GetPickedManager().clickedCells[i], MorphoTools.GetPickedManager().clickedCells[i], SelectionValue, true);
                RemoveSelectionPast(MorphoTools.GetPickedManager().clickedCells[i], MorphoTools.GetPickedManager().clickedCells[i], SelectionValue, true);
                UpdateNumberCells();
            }
            if (Lineage.isLineage)
                Lineage.UpdateWebsiteLineage();
        }

        public void removeFromAllSelection()
        {
            removedCells = new List<Cell>();
            for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
            {
                MorphoTools.GetPickedManager().clickedCells[i].RemoveAllSelection();
                if (InterfaceManager.instance.isTimeModificationActive())
                {
                    RemoveSelectionFutur(MorphoTools.GetPickedManager().clickedCells[i], MorphoTools.GetPickedManager().clickedCells[i], SelectionValue, true);
                    RemoveSelectionPast(MorphoTools.GetPickedManager().clickedCells[i], MorphoTools.GetPickedManager().clickedCells[i], SelectionValue, true);
                }

                UpdateNumberCells();
            }
#if UNITY_WEBGL
            if (Lineage.isLineage) Lineage.UpdateWebsiteLineage();
#else
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        /*public void removeFromMultiSelection()
        {
            removedCells = new List<Cell>();
            for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
            {
                if (MorphoTools.GetPickedManager().clickedCells[i].selection != null && MorphoTools.GetPickedManager().clickedCells[i].selection.Count > 1)
                {
                    MorphoTools.GetPickedManager().clickedCells[i].RemoveAllSelection();
                    if (MultiAllTimes.isOn)
                    {
                        RemoveSelectionFutur(MorphoTools.GetPickedManager().clickedCells[i], MorphoTools.GetPickedManager().clickedCells[i], SelectionValue, true);
                        RemoveSelectionPast(MorphoTools.GetPickedManager().clickedCells[i], MorphoTools.GetPickedManager().clickedCells[i], SelectionValue, true);
                    }
                    UpdateNumberCells();
                }
            }

#if UNITY_WEBGL
    if (Lineage.isLineage) Lineage.UpdateWebsiteLineage();
#else
            MorphoTools.SendCellsColorToLineage();
#endif

        }*/

        //To Select all cell corresponding to this selection at this time point
        public void onSelect()
        {
            List<Cell> cells = new List<Cell>();
            if (MorphoTools.GetPickedManager().clickedCells == null)
                MorphoTools.GetPickedManager().clickedCells = new List<Cell>();
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
            {
                if (cell.selection != null && cell.selection.Count > 0 && cell.selection.Contains(SelectionValue) && !MorphoTools.GetPickedManager().clickedCells.Contains(cell))
                {
                    cell.choose(true, false);
                    cells.Add(cell);
                    MorphoTools.GetPickedManager().clickedCells.Add(cell);
                }
            }
#if UNITY_WEBGL
            if (Lineage.isLineage) Lineage.ManagePickedCells(cells, true);
#endif

            if (MorphoTools.GetDataset().Infos != null)
                MorphoTools.GetDataset().Infos.describe();
        }

        public void onSelectAll()
        {
            List<Cell> cells = new List<Cell>();
            if (MorphoTools.GetPickedManager().clickedCells == null)
                MorphoTools.GetPickedManager().clickedCells = new List<Cell>();
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
            {
                if (cell.selection != null && cell.selection.Count > 0 && !MorphoTools.GetPickedManager().clickedCells.Contains(cell))
                {
                    cell.choose(true, false);
                    cells.Add(cell);
                    MorphoTools.GetPickedManager().clickedCells.Add(cell);
                }
            }

#if UNITY_WEBGL
            if (Lineage.isLineage) Lineage.ManagePickedCells(cells, true);
#endif

            if (MorphoTools.GetDataset().Infos != null)
                MorphoTools.GetDataset().Infos.describe();
        }

        public void onSelectMulti()
        {
            List<Cell> cells = new List<Cell>();
            if (MorphoTools.GetPickedManager().clickedCells == null)
                MorphoTools.GetPickedManager().clickedCells = new List<Cell>();
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
            {
                if (cell.selection != null && cell.selection.Count >= 2 && !MorphoTools.GetPickedManager().clickedCells.Contains(cell))
                {
                    cell.choose(true, false);
                    cells.Add(cell);
                    MorphoTools.GetPickedManager().clickedCells.Add(cell);
                }
            }
#if UNITY_WEBGL
            if (Lineage.isLineage) Lineage.ManagePickedCells(cells, true);
#endif

            if (MorphoTools.GetDataset().Infos != null)
                MorphoTools.GetDataset().Infos.describe();
        }

        public void onSelectNone()
        {
            List<Cell> cells = new List<Cell>();
            if (MorphoTools.GetPickedManager().clickedCells == null)
                MorphoTools.GetPickedManager().clickedCells = new List<Cell>();
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
            {
                if ((cell.selection == null || cell.selection.Count == 0) && !MorphoTools.GetPickedManager().clickedCells.Contains(cell))
                {
                    cell.choose(true, false);
                    cells.Add(cell);
                    MorphoTools.GetPickedManager().clickedCells.Add(cell);
                }
            }

#if UNITY_WEBGL
            if (Lineage.isLineage) Lineage.ManagePickedCells(cells, true);
#endif

            if (MorphoTools.GetDataset().Infos != null)
                MorphoTools.GetDataset().Infos.describe();
        }

        public static void ComputeSelectionCenters()
        {
            //computer cell groups center by selection for scatter view
            centerBySelection = new Dictionary<int, Vector3>();
            for (int i = 0; i < 255; i++)
            {
                centerBySelection[i] = new Vector3();
            }

            countBySelection = new Dictionary<int, int>();
            for (int i = 0; i < 255; i++)
            {
                countBySelection[i] = 0;
            }
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
            {
                if (cell.selection != null && cell.selection.Count > 0 && cell.getFirstChannel() != null)
                {
                    centerBySelection[cell.selection[0]] += cell.getFirstChannel().Gravity;
                    countBySelection[cell.selection[0]] += 1;
                }
            }
        }

        //Update All Fields
        public void UpdateNumberCells()
        {
            UpdateCurrentNumberCells();
            //ComputeSelectionCenters();
            UpdateThisNumberCells();
            UpdateAllNumberCells();
            UpdateMultiNumberCells();
            UpdateNonNumberCells();
            UpdatePickedNumberCells();
        }

        public void UpdatePickedNumberCells()
        {
            if (MorphoTools.GetPickedManager() != null && MorphoTools.GetPickedManager().clickedCells != null && MorphoTools.GetPickedManager().clickedCells.Count > 0)
            {
                CurrentPickedCount.gameObject.GetComponent<Text>().text = "Selected objects (" + MorphoTools.GetPickedManager().clickedCells.Count + ")";
            }
            else
            {
                CurrentPickedCount.gameObject.GetComponent<Text>().text = "Selected objects (0)";
            }
        }

        public void UpdateCurrentNumberCells()
        {
            int nb = CountCellsWithCurrentSelection();
            if (nb > 0)
            {
                CurrentSelectionCount.gameObject.GetComponent<Text>().text = "Label : " + nb + " objects";
            }
            else
            {
                CurrentSelectionCount.gameObject.GetComponent<Text>().text = "Label : No objects";
            }
        }

        //FOR THIS SELECTED CELLS
        public void UpdateThisNumberCells()
        {
            int nb = 0;
            /*if (ThisAllTimes.isOn)
                nb = CountCellsThisSelectionAll();
            else*/
                nb = CountCellsThisSelection(MorphoTools.GetDataset().CurrentTime);
            if (nb > 0)
            {
                ThisSelection.gameObject.GetComponent<Text>().text = "Objects with current label (" + nb + ")";
            }
            else
                ThisSelection.gameObject.GetComponent<Text>().text = "Objects with current label (0)";
        }

        public int CountCellsThisSelectionAll()
        {
            int nb = 0;
            for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
                nb += CountCellsThisSelection(t);
            return nb;
        }

        public int CountCellsWithThisSelectionAtCurrentTime()
            => CountCellsThisSelection(MorphoTools.GetDataset().CurrentTime);

        public int CountCellsWithCurrentSelection()
        {
            /*if (ThisAllTimes.isOn)
                return CountCellsThisSelectionAll();
            else*/
                return CountCellsWithThisSelectionAtCurrentTime();
        }

        //Just Countthe number of cells with this selection value
        public int CountCellsThisSelection(int t)
        {
            int nb = 0;
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
            {
                if (cell.selection != null && cell.selection.Contains(SelectionValue))
                    nb++;
            }
            return nb;
        }

        //FOR ALLL SELECTED CELLS
        public void UpdateAllNumberCells()
        {
            UpdateCurrentNumberCells();
            int nb = CountCellsWithAnySelection();
            if (nb > 0)
            {
                AllSelections.gameObject.GetComponent<Text>().text = "Objects with a label (" + nb + ")";
            }
            else
                AllSelections.gameObject.GetComponent<Text>().text = "Objects with a label (0)";
        }

        public int CountCellsAllSelectionAll()
        {
            int nb = 0;
            for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
                nb += CountCellsAllSelection(t);
            return nb;
        }

        public int CountCellsWithAnySelectionAtCurrentTime()
            => CountCellsAllSelection(MorphoTools.GetDataset().CurrentTime);

        public int CountCellsWithAnySelection()
        {
            /*if (AllAllTimes.isOn)
                return CountCellsAllSelectionAll();
            else*/
                return CountCellsWithAnySelectionAtCurrentTime();
        }

        //Just Countthe number of cells with this selection value
        public int CountCellsAllSelection(int t)
        {
            int nb = 0;
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
            {
                if (cell.selection != null && cell.selection.Count > 0)
                    nb++;
            }
            return nb;
        }

        //FOR THIS MULTI CELLS
        public void UpdateMultiNumberCells()
        {
            int nb = CountCellsWithMultipleSelections();
            if (nb > 0)
            {
                MultiSelection.GetComponent<Text>().text = "Objects with multiple labels (" + nb + ")";
                MultiSelection.SetActive(true);
            }
            else
                MultiSelection.GetComponent<Text>().text = "Objects with multiple labels (0)";
        }

        public int CountCellsMultipleSelectionAll()
        {
            int nb = 0;
            for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
                nb += CountCellsMultiSelection(t);
            return nb;
        }

        public int CountCellsWithMultiSelectionAtCurrentTime()
            => CountCellsMultiSelection(MorphoTools.GetDataset().CurrentTime);

        public int CountCellsWithMultipleSelections()
        {
            /*if (MultiAllTimes.isOn)
                return CountCellsMultipleSelectionAll();
            else*/
                return CountCellsMultiSelection(MorphoTools.GetDataset().CurrentTime);
        }

        //Just Countthe number of cells with this selection value
        public int CountCellsMultiSelection(int t)
        {
            int nb = 0;
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
            {
                if (cell.selection != null && cell.selection.Count >= 2)
                    nb++;
            }
            return nb;
        }

        //FOR NON SELECTED CELLS
        public void UpdateNonNumberCells()
        {
            int nb = CountCellsWithoutSelection();
            if (nb > 0)
            {
                NonSelected.GetComponent<Text>().text = "Objects with no label (" + nb + ")";
            }
            else
                NonSelected.GetComponent<Text>().text = "Objects with no label (0)";
        }

        public int CountCellsWithoutSelection()
        {
            /*if (NonAllTimes.isOn)
                return CountCellsNonSelectionAll();
            else*/
                return CountCellsNonSelectionAtCurrentTime();
        }

        public int CountCellsNonSelectionAtCurrentTime()
            => CountCellsNonSelection(MorphoTools.GetDataset().CurrentTime);

        public int CountCellsNonSelectionAll()
        {
            int nb = 0;
            for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
                nb += CountCellsNonSelection(t);
            return nb;
        }

        //Just Countthe number of cells with this selection value
        public int CountCellsNonSelection(int t)
        {
            int nb = 0;
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
            {
                if (cell.selection == null || cell.selection.Count == 0)
                    nb++;
            }
            return nb;
        }

        public void openLoadSelection()
        {
            if (MenuLoadSelection.activeSelf)
            {
                MenuLoadSelection.transform.Find("bottomPart").Find("download").gameObject.SetActive(false);
                InterfaceManager.instance.MenuObjects.transform.Find("MenuLoadSelection").Find("Upload Panel").gameObject.SetActive(false);
                MenuLoadSelection.SetActive(false);
            }
            else
            {
                MenuColor.SetActive(false);
                menu_state = false;
                MenuLoadSelection.SetActive(true);
                MenuLoadSelection.GetComponent<ColorMapManager>().listAllColormap();
            }
        }

        public void ApplyFreeSelectionToEachSelected()
        {
            PickedManager pickedManager = MorphoTools.GetPickedManager();
            bool[] selection_by_cell = new bool[256];
            for (int i = 1; i < 256; i++)
            {
                selection_by_cell[i] = false;
            }

            for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
                {
                    if (cell != null && cell.selection != null)
                    {
                        foreach (int s in cell.selection)
                        {
                            selection_by_cell[s] = true;
                        }
                    }
                }
            }

            for (int i = 0; i < pickedManager.clickedCells.Count; i++)
            {
                int free_select = -1;
                for (int j = 1; j < 256; j++)
                {
                    if (selection_by_cell[j] == false)
                    {
                        free_select = j;
                        selection_by_cell[j] = true;
                        break;
                    }
                }
                if (free_select == -1)
                {
                    free_select = Random.Range(0, 255);
                }
                if (pickedManager.clickedCells[i].selection == null)
                {
                    pickedManager.clickedCells[i].selection = new List<int>();
                }

                pickedManager.clickedCells[i].addSelection(free_select);
            }

            pickedManager.ClearSelectedCells();


#if UNITY_WEBGL
        if (Lineage.isLineage) Lineage.UpdateWebsiteLineage();
#else
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        //FOR SELECTED CELLS
        public void resetSelectionOnSelectedCells()
        {
            List<Cell> cells = new List<Cell>(MorphoTools.GetPickedManager().clickedCells);
            if (SelectedCellAllTimes.isOn)
            { //ALL TIMES
                for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
                {
                    List<Cell> cells2 = MorphoTools.GetPickedManager().clickedCells[i].getAllCellLife();
                    foreach (Cell c in cells2)
                    {
                        c.RemoveAllSelection();
                    }
                }

            }
            else
            { //JUST THIS TIME STEP
                for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
                {
                    MorphoTools.GetPickedManager().clickedCells[i].RemoveAllSelection();
                }
            }

#if UNITY_WEBGL
        if (Lineage.isLineage) Lineage.UpdateWebsiteLineage();
#else
            MorphoTools.SendCellsColorToLineage();
#endif


            if (MorphoTools.GetDataset().Infos != null)
                MorphoTools.GetDataset().Infos.describe();
        }
    }
}