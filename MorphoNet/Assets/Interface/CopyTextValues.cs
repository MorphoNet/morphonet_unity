﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CopyTextValues : MonoBehaviour
{
    public Text source;
    public Text destination;

    // Update is called once per frame
    void Update()
    {
        if (source != null && destination != null)
        {
            destination.text = source.text;
        }
    }
}
