using MorphoNet.Tools;
using System.Collections.Generic;
using UnityEngine;

namespace MorphoNet.XR
{
    public class GazeCollisionListener : MonoBehaviour, IGazable
    {
        [SerializeField]
        private List<XRTooltip> _Tooltips = new List<XRTooltip>();

        private void Start()
        {
            gameObject.layer = LayerMask.NameToLayer(Tools.Layers.CentralGaze);
        }

        public void OnEnter()
        {
            foreach (XRTooltip tooltip in _Tooltips)
                tooltip.Show();
        }

        public void OnLeave()
        {
            foreach (XRTooltip tooltip in _Tooltips)
                tooltip.Hide();
        }
    }
}