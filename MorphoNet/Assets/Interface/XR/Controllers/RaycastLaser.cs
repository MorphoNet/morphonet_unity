using UnityEngine;

namespace MorphoNet
{
    [RequireComponent(typeof(LineRenderer))]
    public class RaycastLaser : MonoBehaviour
    {
        private LineRenderer _Line;

        ///<summary>
        /// The Start of the cable will be the transform of the Gameobject that has this component.
        /// The Transform of the Gameobject where the End of the cable is. This needs to be assigned in the inspector.
        ///</summary>
        [SerializeField]
        private Transform _EndPointTransform;

        ///<summary>
        /// Number of points per meter
        ///</summary>
        [SerializeField]
        [Tooltip("Number of points per unit length, using the straight line from the start to the end transform.")]
        private int _PointDensity = 10;

        // These are used later for calculations
        private Vector3 _VectorFromStartToEnd;

        private void Start()
        {
            _Line = GetComponent<LineRenderer>();

            transform.forward = _VectorFromStartToEnd.normalized;
            _Line.positionCount = _PointDensity;
        }

        public void Draw(Vector3? endPoint)
        {
            if (endPoint == null)
            {
                Undraw();
            }
            else
            {
                _VectorFromStartToEnd = endPoint.Value - transform.position;

                for (int i = 0; i < _PointDensity; i++)
                {
                    // This is the fraction of where we are in the cable and it accounts for arrays starting at zero.
                    float pointForCalcs = (float)i / (_PointDensity - 1);

                    // Calculate the position of the current point i
                    Vector3 pointPosition = _VectorFromStartToEnd * pointForCalcs;
                    // Calculate the sag vector for the current point i

                    // Calculate the postion with Sag.
                    Vector3 currentPointsPosition = transform.position + pointPosition;

                    // Set point
                    _Line.SetPosition(i, currentPointsPosition);
                }
            }
        }

        public void Undraw()
        {
            for (int i = 0; i < _PointDensity; i++)
                _Line.SetPosition(i, transform.position);
        }
    }
}