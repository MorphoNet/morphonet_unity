namespace MorphoNet.Tools
{
    public interface IGazable
    {
        public void OnEnter();

        public void OnLeave();
    }
}