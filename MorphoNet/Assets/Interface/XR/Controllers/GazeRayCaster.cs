using UnityEngine;

namespace MorphoNet.Tools
{
    public class GazeRayCaster : MonoBehaviour
    {
        private IGazable _GazedObject = null;

        private void Update()
        {
            if (Physics.Raycast(transform.position,
                                transform.forward,
                                out RaycastHit hit,
                                100f, 1 << LayerMask.NameToLayer(Layers.CentralGaze)))
            {
                IGazable newGazedObject = hit.collider.GetComponent<IGazable>();

                if (_GazedObject != null && newGazedObject != _GazedObject)
                    _GazedObject.OnLeave();

                if (newGazedObject != null)
                {
                    _GazedObject = newGazedObject;
                    _GazedObject.OnEnter();
                }
            }
            else if (_GazedObject != null)
            {
                _GazedObject.OnLeave();
                _GazedObject = null;
            }
        }
    }
}