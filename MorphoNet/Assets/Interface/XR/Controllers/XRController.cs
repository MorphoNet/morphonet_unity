using MorphoNet.Extensions.Unity;
using MorphoNet.Interaction.Physic;
using System.Collections.Generic;
using UnityEngine;

namespace MorphoNet.XR
{
    public class XRController : MonoBehaviour
    {
        public enum ControllerRole
        {
            Main,
            Secondary
        }

        public enum ControllerDevice
        {
            ViveController,
            Quest2Controller
        }

        [SerializeField]
        private RaycastLaser _RaycastLaser;

        [SerializeField]
        private PhysicCollisionListener _PhysicCollisionListener;

        [SerializeField]
        private Transform _MenuContainer;

        [SerializeField]
        private GameObject _CuttingPlane;

        private ControllerRole _Role;
        private ControllerDevice _Device;

        [Header("Devices"), SerializeField]
        private GameObject _DefaultDevice;

        [SerializeField]
        private GameObject _ViveDevice;

        public RaycastLaser RaycastLaser => _RaycastLaser;
        public PhysicCollisionListener PhysicCollisionListener => _PhysicCollisionListener;
        public Transform MenuContainer => _MenuContainer;

        public GameObject CuttingPlane => _CuttingPlane;

        public Vector3 GetControllerForwardDirection()
            => _Device switch
            {
                ControllerDevice.ViveController => _ViveDevice.transform.forward,
                _ => _DefaultDevice.transform.forward,
            };

        public Vector3 GetTriggerPosition()
            => PhysicCollisionListener.transform.position;

        public ControllerRole Role
        {
            get { return _Role; }
            set
            {
                _Role = value;

                switch (_Role)
                {
                    case ControllerRole.Main:
                        PhysicCollisionListener.gameObject.Activate();
                        RaycastLaser.gameObject.Activate();
                        break;

                    case ControllerRole.Secondary:
                        PhysicCollisionListener.gameObject.Deactivate();
                        RaycastLaser.gameObject.Deactivate();
                        break;
                }
            }
        }

        public ControllerDevice Device
        {
            get => _Device;

            internal set
            {
                _Device = value;

                _DefaultDevice.Deactivate();
                _ViveDevice.Deactivate();

                switch (_Device)
                {
                    case ControllerDevice.ViveController:
                        _ViveDevice.Activate();
                        break;

                    case ControllerDevice.Quest2Controller:
                    default:
                        _DefaultDevice.Activate();
                        break;
                }
            }
        }
    }
}