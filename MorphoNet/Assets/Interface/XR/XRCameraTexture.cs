using MorphoNet;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class is used to manage the VR view in the regular MorphoNet Scene
/// </summary>
public class XRCameraTexture : MonoBehaviour
{
    private RenderTexture _CameraTexture;
    private int _State = 0;//camera states : 0 = default (small), 1 = big, 2 = minimized
    private Camera _XRCamera;
    private RectTransform _TexTransform;
    private RectTransform _ParentCanvas;

    private static float DEFAULT_X_SIZE = 355.5f;
    private static int DEFAULT_Y_SIZE = 200;
    private static int MINIMIZED_XY_SIZE = 30;
    private static float TEX_ASPECT_RATIO = (16f / 9f);
    private static float PADDING = 100f;

    /// <summary>
    /// initialize values from scene and other elements
    /// </summary>
    public void InitValues()
    {
        _ParentCanvas = transform.parent.parent.GetComponent<RectTransform>();
        _TexTransform = transform.parent.gameObject.GetComponent<RectTransform>();
        _CameraTexture = (RenderTexture)gameObject.GetComponent<RawImage>().texture;
        if (_CameraTexture == null)
        {
            MorphoDebug.LogError("ERROR : camera texture could not be found for XRCameraTexture");
        }
        _XRCamera = MorphoNet.XRCameraObject.Instance.GetComponent<Camera>();
        if (_XRCamera == null)
        {
            MorphoDebug.LogError("ERROR :XR camera could not be accessed");
        }
    }


    /// <summary>
    /// resize the UI element size with 3 modes : 0 = small, 1 = big, 2 = minimized
    /// </summary>
    public void ResizeCameraTexture()
    {
        if (_CameraTexture == null || _XRCamera == null)
            InitValues();


        _State++;
        if (_State > 2)
            _State = 0;
        switch (_State)
        {
            case 0:
                _TexTransform.sizeDelta = new Vector2(DEFAULT_X_SIZE, DEFAULT_Y_SIZE);
                break;
            case 1:
                float screen_aspectratio = (_ParentCanvas.sizeDelta.x - PADDING) / (_ParentCanvas.sizeDelta.y - PADDING);
                if (screen_aspectratio > TEX_ASPECT_RATIO)
                {
                    _TexTransform.sizeDelta = new Vector2((_ParentCanvas.sizeDelta.y - PADDING) * TEX_ASPECT_RATIO, _ParentCanvas.sizeDelta.y - PADDING);
                }
                else
                {
                    _TexTransform.sizeDelta = new Vector2((_ParentCanvas.sizeDelta.x - PADDING), (_ParentCanvas.sizeDelta.x - PADDING) / TEX_ASPECT_RATIO);
                }
                    
                break;
            case 2:
                _TexTransform.sizeDelta = new Vector2(MINIMIZED_XY_SIZE, MINIMIZED_XY_SIZE);
                break;
            default:
                break;
        }
    }
}
