using System.Collections.Generic;
using UnityEngine;

namespace MorphoNet.Config
{
    [CreateAssetMenu(fileName = "Options", menuName = "ScriptableObjects/MorphoNetOptions", order = 1)]
    public class MorphoNetOptions : ScriptableObject
    {
        [Header("User Options")]
        public string Id;

        public string Password;

        [Header("XR Options")]
        public OptionValue<bool> UseHandModel = new OptionValue<bool>();

        public OptionValue<bool> LeftHandedMenu = new OptionValue<bool>();

        public OptionValue<bool> ModelRotationWithOneHand = new OptionValue<bool>();
        
        public OptionValue<List<DataSetDisplayStrategy>> VRDisplayType = new OptionValue<List<DataSetDisplayStrategy>>();

        public OptionValue<int> CurrentStrategyIndex = new OptionValue<int>();
    }

    [System.Serializable]
    public class OptionValue<T>
    {
        public bool Active = false;

        public T Value = default;

        public OptionValue()
        {
            Value = default;
            Active = default;
        }

        public OptionValue(T value, bool active)
        {
            Value = value;
            Active = active;
        }
    }
}