using MorphoNet.Config;
using System;
using System.Collections.Generic;
using MorphoNet;
using UnityEngine;

public class OptionController : MonoBehaviour
{
    public static OptionController Instance;

    private void Awake()
    {
        if (Instance is null)
            Instance = this;
        else
            Destroy(gameObject);

        DontDestroyOnLoad(this);
    }


    [Header("Options")]
    [SerializeField]
    private MorphoNetOptions _Options;
    public event Action<bool> OnLeftHandedMenuOptionChange;

    public OptionValue<bool> GetLeftHandedMenuOption() => _Options.LeftHandedMenu;

    public void SetLeftHandedMenu(bool on)
    {
        _Options.LeftHandedMenu.Value = on;
        OnLeftHandedMenuOptionChange?.Invoke(on);
    }

    public event Action<bool> OnRotationWithOneHandOptionChange;

    public OptionValue<bool> GetModelRotationWithOneHandOption() => _Options.ModelRotationWithOneHand;
    
    public OptionValue<int> CurrentStrategyIndex() => _Options.CurrentStrategyIndex;
    
    
    public DataSetDisplayStrategy GetCurrentStrategy()
    {
        return GetDisplayStrategyOption().Value[CurrentStrategyIndex().Value];
    }

    public OptionValue<List<DataSetDisplayStrategy>> GetDisplayStrategyOption() => _Options.VRDisplayType;
    public void SetModelRotationWithOneHandOption(bool on)
    {
        _Options.ModelRotationWithOneHand.Value = on;
        OnRotationWithOneHandOptionChange?.Invoke(on);
    }

    public void NextDisplayStrategy()
    {
        _Options.CurrentStrategyIndex.Value += 1;
        if (_Options.CurrentStrategyIndex.Value >= _Options.VRDisplayType.Value.Count)
            _Options.CurrentStrategyIndex.Value = 0;
        
    }

    public void PreviousDisplayStrategy()
    {
        _Options.CurrentStrategyIndex.Value -= 1;
        if (_Options.CurrentStrategyIndex.Value < 0)
            _Options.CurrentStrategyIndex.Value = _Options.VRDisplayType.Value.Count-1;
    }
    public event Action<bool> OnUseHandModelChange;

    public OptionValue<bool> GetUseHandModelOption() => _Options.UseHandModel;

    public void SetUseHandModelOption(bool on)
    {
        _Options.UseHandModel.Value = on;
        OnUseHandModelChange?.Invoke(on);
    }
}