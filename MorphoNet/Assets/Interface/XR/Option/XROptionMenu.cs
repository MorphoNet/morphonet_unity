using MorphoNet.Config;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MorphoNet.UI.XR
{
    public class XROptionMenu : XRMainMenuPart
    {
        [SerializeField]
        private OptionController _OptionController;

        [Header("UI References")]
        [SerializeField]
        private XRToggleButton _ToggleLeftHandedMenu;

        [SerializeField]
        private XRToggleButton _ToggleModelRotationWithOneHand;

        [SerializeField]
        private XRToggleButton _ToggleUseHandModel;
        
        [SerializeField]
        private XRButton _PreviousDisplayStrategy;
        
        [SerializeField]
        private XRButton _NextDisplayStrategy;
        
        [SerializeField]
        private XRLabel _XRStrategyName;

        private void Start()
        {

            _ToggleLeftHandedMenu.OnToggleOn.AddListener(
                () => _OptionController.SetLeftHandedMenu(true));
            _ToggleLeftHandedMenu.OnToggleOff.AddListener(
                () => _OptionController.SetLeftHandedMenu(false));

            _ToggleModelRotationWithOneHand.OnToggleOn.AddListener(
                () => _OptionController.SetModelRotationWithOneHandOption(true));
            _ToggleModelRotationWithOneHand.OnToggleOff.AddListener(
                () => _OptionController.SetModelRotationWithOneHandOption(false));

            _ToggleUseHandModel.OnToggleOn.AddListener(
                () => _OptionController.SetUseHandModelOption(true));
            _ToggleUseHandModel.OnToggleOff.AddListener(
                () => _OptionController.SetUseHandModelOption(false));
            
            _PreviousDisplayStrategy.OnCollision.AddListener(
                () => {
                    _OptionController.PreviousDisplayStrategy();
                    _LoadDatasetStrategy();
                });
            _NextDisplayStrategy.OnCollision.AddListener(
                () =>
                {
                    _OptionController.NextDisplayStrategy();
                    _LoadDatasetStrategy();
                });
            _LoadDatasetStrategy();
        }

        private void _LoadDatasetStrategy()
        {
            _XRStrategyName.SetText(_OptionController.GetCurrentStrategy().StrategyName);
            MorphoTools.GetDataset().DataSetDisplayStrategy = _OptionController.GetCurrentStrategy();
            MorphoTools.GetDataset().ApplyDisplayStrategyAndUpdateAll();
        }
        public override void Show()
        {
            _OptionController = OptionController.Instance;
            DisplayOptionButton(_ToggleLeftHandedMenu, _OptionController.GetLeftHandedMenuOption());
            DisplayOptionButton(_ToggleModelRotationWithOneHand, _OptionController.GetModelRotationWithOneHandOption());
            DisplayOptionButton(_ToggleUseHandModel, _OptionController.GetUseHandModelOption());
            _PreviousDisplayStrategy.Interactable = _OptionController.GetDisplayStrategyOption().Active;
            _NextDisplayStrategy.Interactable = _OptionController.GetDisplayStrategyOption().Active;
            _XRStrategyName.gameObject.SetActive(_OptionController.GetDisplayStrategyOption().Active);
            base.Show();
        }

        private void DisplayOptionButton(XRToggleButton button, OptionValue<bool> optionValue)
        {
            if (optionValue.Value)
                button.ToggleOnWithoutNotify();
            else
                button.ToggleOffWithoutNotify();

            if (optionValue.Active)
                button.Interactable = true;
            else
                button.Interactable = false;
        }
    }
}