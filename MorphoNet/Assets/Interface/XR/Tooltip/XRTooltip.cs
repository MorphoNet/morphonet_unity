using MorphoNet.Extensions.Unity;
using UnityEngine;

namespace MorphoNet.XR
{
    public class XRTooltip : MonoBehaviour
    {
        [SerializeField]
        private LineRenderer _LineRenderer;

        [SerializeField]
        private Canvas _Canvas;

        [SerializeField]
        private TMPro.TextMeshProUGUI _Text;

        [SerializeField]
        private Transform _TargetObject;

        public Transform TargetObject
        {
            get { return _TargetObject; }
            set { _TargetObject = value; }
        }

        [ContextMenu("DrawLine")]
        public void DrawLine()
        {
            if (_TargetObject == null)
                return;

            _LineRenderer.SetPosition(0, _LineRenderer.transform.position);
            _LineRenderer.SetPosition(1, _TargetObject.position);
        }

        internal void SetText(string text)
        {
            _Text.text = text;
        }

        public void DrawLineTo(GameObject go)
        {
            TargetObject = go.transform;
        }

        public void DrawLineTo(Transform t)
        {
            TargetObject = t;
        }

        public void Show() => gameObject.Activate();
        public void Hide() => gameObject.Deactivate();

        private void Start()
        {
            Transform target = _LineRenderer.transform;

            if (_TargetObject != null)
                target = _TargetObject;

            _LineRenderer.SetPositions(new Vector3[]
                {
                    _LineRenderer.transform.position,
                    target.position
                });
        }

        private void LateUpdate()
        {
            XRCameraObject lookAtTarget = XRCameraObject.Instance;

            if (lookAtTarget != null && lookAtTarget.transform != null)
                transform.LookAt(lookAtTarget.transform);

            DrawLine();
        }
    }
}