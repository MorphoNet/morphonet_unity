using System.Collections.Generic;
using UnityEngine;

namespace MorphoNet.XR
{
    public class XRTooltipManager : UnitySingleton<XRTooltipManager>
    {
        [SerializeField]
        private XRTooltip _TooltipPrefab;

        public List<XRTooltip> Tooltips { get; private set; } = new List<XRTooltip>();

        public void CreateTooltip(Transform target, string text, Transform parent = null, Vector3 position = default)
        {
            XRTooltip tooltip = Instantiate(_TooltipPrefab, parent);
            tooltip.transform.position = position;
            tooltip.TargetObject = target;
            tooltip.SetText(text);

            Tooltips.Add(tooltip);
        }

        public void DeleteTooltip(XRTooltip tooltip)
        {
            Tooltips.Remove(tooltip);
            Destroy(tooltip);
        }
    }
}