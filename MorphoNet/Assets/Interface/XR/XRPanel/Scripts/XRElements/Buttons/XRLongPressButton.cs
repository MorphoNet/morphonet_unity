using UnityEngine;

namespace MorphoNet.UI.XR
{
    public class XRLongPressButton : XRButton
    {
        [SerializeField]
        [Header("Long Press Options")]
        [Tooltip("Time required (in seconds) for the button to trigger")]
        private float _ActivationTime = 2f;

        private float _CollidingTime = 0f;

        private Collider _CollidingObject;
        private Collider _LastFrameCollidingObjet;

        private void HandlePressState()
        {
            if (_CollidingObject != null && _CollidingObject == _LastFrameCollidingObjet)
            {
                _CollidingTime += Time.deltaTime;

                _BackgroundPressedColorFilling = _CollidingTime / _ActivationTime;

                if (_CollidingTime >= _ActivationTime)
                {
                    OnCollision.Invoke();
                    ResetCollidingTime();
                }
            }

            _LastFrameCollidingObjet = _CollidingObject;
        }

        private void ResetCollidingTime()
        {
            _CollidingObject = null;
            _LastFrameCollidingObjet = null;
            _CollidingTime = 0f;
            _BackgroundPressedColorFilling = 0f;
            UpdateBackgroundFilling();
        }

        protected override void ButtonFrameUpdate()
        {
            HandlePressState();
            UpdateBackgroundFilling();
        }

        public override void ButtonTriggerCollision(Collider externalTrigger)
        {
            if (_TriggeringColliders.Contains(externalTrigger))
            {
                if (_CollidingObject is null)
                    _CollidingObject = externalTrigger;
                else
                    OnCollision.Invoke();
            }
        }

        public override void ButtonTriggerExitCollision(Collider externalTrigger)
        {
            if (_TriggeringColliders.Contains(externalTrigger))
            {
                OnExitCollision.Invoke();

                if (_CollidingObject != null && externalTrigger == _CollidingObject)
                    ResetCollidingTime();
            }
        }
    }
}