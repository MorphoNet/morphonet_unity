using System;
using UnityEngine;
using UnityEngine.Events;

namespace MorphoNet.UI.XR
{
    public class XRToggleButton : XRButton
    {
        [Header("Toggle Button Options")]
        [SerializeField]
        private bool _ToggledOn = false;

        [SerializeField]
        private bool _NotifyOnStart = false;

        [SerializeField]
        private string _ToggleOnText = "On";

        [SerializeField]
        private string _ToggleOffText = "Off";

        public bool Value { get => _ToggledOn; }

        public UnityEvent OnToggleOn;
        public UnityEvent OnToggleOff;

        protected override void ButtonStart()
        {
            base.ButtonStart();
            InitialisationTriggers();
        }

        private void InitialisationTriggers()
        {
            OnToggleOn.AddListener(() =>
            {
                ShowButtonOn();
            });

            OnToggleOff.AddListener(() =>
            {
                ShowButtonOff();
            });

            OnCollision.AddListener(() =>
            {
                if (Interactable && _ToggledOn)
                    OnToggleOff.Invoke();
                else if (Interactable)
                    OnToggleOn.Invoke();
            });

            if (_ToggledOn)
            {
                SetText(_ToggleOnText);
                if (_NotifyOnStart)
                    OnToggleOn.Invoke();
            }
            else
            {
                SetText(_ToggleOffText);
                if (_NotifyOnStart)
                    OnToggleOff.Invoke();
            }
        }

        private void ShowButtonOff()
        {
            SetText(_ToggleOffText);
            _BackgroundPressedColorFilling = 0f;
            _ToggledOn = false;
        }

        private void ShowButtonOn()
        {
            SetText(_ToggleOnText);
            _BackgroundPressedColorFilling = 1f;
            _ToggledOn = true;
        }

        /// <summary>
        /// To use to config when the button is Instantiated at runtime. Use editor properties otherwise.
        /// </summary>
        public void Init(string toggleOnText,
                         string toggleOffText,
                         Action onToggleOn,
                         Action onToggleOff,
                         bool toggleOnByDefault = false,
                         bool notifyOnInit = false)
        {
            OnToggleOn.RemoveAllListeners();
            OnToggleOff.RemoveAllListeners();

            OnToggleOn.AddListener(() => onToggleOn?.Invoke());
            OnToggleOff.AddListener(() => onToggleOff?.Invoke());

            _ToggleOnText = toggleOnText;
            _ToggleOffText = toggleOffText;

            _ToggledOn = toggleOnByDefault;
            _NotifyOnStart = notifyOnInit;

            InitialisationTriggers();
        }

        [ContextMenu("ToggleOn")]
        public void ToggleOn() => OnToggleOn.Invoke();

        [ContextMenu("ToggleOff")]
        public void ToggleOff() => OnToggleOff.Invoke();

        [ContextMenu("ToggleOnWithoutNotify")]
        public void ToggleOnWithoutNotify() => ShowButtonOn();

        [ContextMenu("ToggleOffWithoutNotify")]
        public void ToggleOffWithoutNotify() => ShowButtonOff();

        public void ToggleWithoutNotify(bool value)
        {
            if (value)
                ToggleOnWithoutNotify();
            else
                ToggleOffWithoutNotify();
        }

        public override void ButtonTriggerCollision(Collider externalTrigger)
        {
            if (Interactable && _TriggeringColliders.Contains(externalTrigger))
            {
                OnCollision.Invoke();
            }
        }

        public override void ButtonTriggerExitCollision(Collider externalTrigger)
        {
            if (Interactable && _TriggeringColliders.Contains(externalTrigger))
            {
                OnExitCollision.Invoke();
            }
        }
    }
}