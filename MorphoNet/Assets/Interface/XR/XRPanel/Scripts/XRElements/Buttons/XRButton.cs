using MorphoNet.Extensions.Unity;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace MorphoNet.UI.XR
{
    public class XRButton : XRElement
    {
        [SerializeField]
        protected List<Collider> _TriggeringColliders = new List<Collider>();

        public UnityEvent OnCollision;
        public UnityEvent OnExitCollision;

        [SerializeField]
        protected Color _Color = Color.white;

        [SerializeField]
        protected Color _InactiveColor = new Color(.1f, .1f, .1f);

        [SerializeField]
        protected XRButtonTrigger _Trigger;

        [SerializeField]
        protected Renderer _TriggerRenderer;

        [Header("Backgroung / Indicator Options")]
        [SerializeField]
        private bool _IndicatorActivated = true;

        [SerializeField]
        protected Image _DefaultBackgroundImage;

        [SerializeField]
        protected Image _ActiveBackgroundImage;

        [SerializeField]
        protected Color _BackgroundDefaultColor;

        [SerializeField]
        protected Color _BackgroundActiveColor;

        [SerializeField]
        [Range(0f, 1f)]
        protected float _BackgroundPressedColorFilling;

        [Header("Texture Parameters (if any)")]
        [SerializeField]
        protected Material _MaterialWithCustomTexture;

        [SerializeField]
        protected Texture _Texture;

        public bool Interactable { get; set; } = true;

        private void Start()
        {
            ButtonStart();
        }

        protected virtual void ButtonStart()
        {
            _Trigger.XRButton = this;

            _DefaultBackgroundImage.color = _BackgroundDefaultColor;
            _ActiveBackgroundImage.color = _BackgroundActiveColor;

            if (_Texture)
            {
                _TriggerRenderer.material = _MaterialWithCustomTexture;
                _TriggerRenderer.material.SetTexture("_MainTex", _Texture);
            }

            _TriggerRenderer.material.color = _Color;

            if (!_IndicatorActivated)
            {
                _ActiveBackgroundImage.gameObject.Deactivate();
                _DefaultBackgroundImage.gameObject.Deactivate();
            }
        }

        private void Update()
        {
            ButtonFrameUpdate();
        }

        protected virtual void ButtonFrameUpdate()
        {
            UpdateBackgroundFilling();
        }

        protected void UpdateBackgroundFilling()
        {
            if (!Interactable)
            {
                _BackgroundPressedColorFilling = 0f;
                _TriggerRenderer.material.color = _InactiveColor;

                if (_IndicatorActivated)
                    _DefaultBackgroundImage.color = _InactiveColor;
            }
            else
            {
                _TriggerRenderer.material.color = _Color;

                if (_IndicatorActivated)
                    _DefaultBackgroundImage.color = _BackgroundDefaultColor;
            }

            if (_IndicatorActivated)
            {
                _DefaultBackgroundImage.fillAmount = 1f - _BackgroundPressedColorFilling;
                _ActiveBackgroundImage.fillAmount = _BackgroundPressedColorFilling;
            }
        }

        public void SetColliders(List<Collider> colliders) => _TriggeringColliders = colliders;

        public virtual void ButtonTriggerCollision(Collider externalTrigger)
        {
            if (Interactable && _TriggeringColliders.Contains(externalTrigger))
            {
                OnCollision.Invoke();

                _BackgroundPressedColorFilling = 1f;
            }
        }

        public virtual void ButtonTriggerExitCollision(Collider externalTrigger)
        {
            if (Interactable && _TriggeringColliders.Contains(externalTrigger))
            {
                OnExitCollision.Invoke();

                _BackgroundPressedColorFilling = 0f;
            }
        }
    }
}