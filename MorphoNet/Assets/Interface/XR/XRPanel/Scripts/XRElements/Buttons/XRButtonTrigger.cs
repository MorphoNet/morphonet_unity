using UnityEngine;

namespace MorphoNet.UI.XR
{
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(Renderer))]
    public class XRButtonTrigger : MonoBehaviour
    {
        public XRButton XRButton { get; set; }


        private void OnTriggerEnter(Collider other)
        {
            XRButton.ButtonTriggerCollision(other);
        }

        private void OnTriggerExit(Collider other)
        {
            XRButton.ButtonTriggerExitCollision(other);
        }
    }
}