using TMPro;

using UnityEngine;

namespace MorphoNet.UI.XR
{
    public abstract class XRElement : MonoBehaviour
    {
        [SerializeField]
        protected TextMeshPro _Text;

        public void SetText(string newText)
        {
            _Text.text = newText;
        }
    }
}