using MorphoNet.UI.XR;
using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MorphoNet.Extensions.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

namespace MorphoNet
{
    public class XRColormapList : XRMainMenuPart
    {
        [SerializeField]
        private GameObject _ColormapItemDummy;

        [SerializeField]
        private List<GameObject> _ColormapItems;

        public TextMeshPro CurrentMapFeedback;
        public List<TextMeshPro> ColormapText;

        [SerializeField]
        private Transform _ColormapList;

        [SerializeField]
        private List<string> ColormapList;

        [SerializeField]
        private List<int> ColormapIDList;

        private List<Collider> _ListItemColliders = new List<Collider>();

        public SelectionDisplay SelectionDisplay;

        [SerializeField]
        private XRButton _Apply;

        [SerializeField]
        private XRButton _Reset;

        [SerializeField]
        private XRButton _Up;

        [SerializeField]
        private XRButton _Down;

        private const int _ItemPerPages = 2;
        public int CurrentPage { get; private set; } = 0;

        public int NumberOfPages => _ColormapItems.Count / _ItemPerPages;

        public XRColormapItem selectedMap;

        private void ListAllColormap()
        {
            StartCoroutine(QueryColormapList());
        }

        private void BindCurrentFeedback()
        {
            if (selectedMap == null)
            {
                CurrentMapFeedback.text = "Current : None";
            }
            else
            {
                CurrentMapFeedback.text = "Current : " + selectedMap.ColorName;
            }
        }

        public void Display()
        {
            HideItems();

            int startIndex = CurrentPage * _ItemPerPages;
            int maxIndex = Mathf.Min(_ColormapItems.Count + 1, startIndex + _ItemPerPages);

            for (int i = startIndex; i < maxIndex; i++)
            {
                var item = _ColormapList.GetChild(i).gameObject;
                item.SetActive(true);

                item.transform.localPosition = new Vector3(0, 0, 0);
            }
        }

        public void HideItems()
        {
            for (int i = _ColormapList.childCount - 1; i >= 0; i--)
                _ColormapList.GetChild(i).gameObject.SetActive(false);
        }

        public void NextPage()
        {
            if (CurrentPage == NumberOfPages)
                return;

            CurrentPage = Mathf.Min(NumberOfPages, CurrentPage + 1);
            Display();
        }

        public void PreviousPage()

        {
            if (CurrentPage == 0)
                return;

            CurrentPage = Mathf.Max(0, CurrentPage - 1);
            Display();
        }

        private void AddColormapToList(GameObject map)
        {
            if (_ColormapList != null)
            {
                map.transform.SetParent(_ColormapList.transform);
            }
        }

        private void ClearColormapsItem()
        {
            foreach (GameObject map in _ColormapItems)
            {
                if (map != null)
                    Destroy(map);
            }
            _ColormapItems.Clear();
            ColormapList.Clear();
            ColormapText.Clear();
            ColormapIDList.Clear();
        }

        private IEnumerator QueryColormapList()
        {
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/partialcolormap/?id_dataset=" + SetsManager.instance.DataSet.id_dataset + "&id_people=" + SetsManager.instance.IDUser);
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                ClearColormapsItem();

                //Return id,name,date
                var N = JSONNode.Parse(www.downloadHandler.text);
                ColormapList = new List<string>();
                ColormapList.Add("None");
                ColormapIDList.Add(-1);
                ColormapText.Add(null);
                int it = 1;
                if (N != null && N.Count > 0)
                {
                    for (int i = 0; i < N.Count; i++)
                    { //Number of Uploaded Data
                        int id_colormap = N[i][0].AsInt;
                        string name = N[i][1].ToString().Replace('"', ' ').Trim();
                        int id_people = N[i][3].AsInt;
                        int id_dataset = N[i][4].AsInt;
                        bool shared = N[i][5].AsBool;

                        string colormapnname = name;
                        if ((id_people == SetsManager.instance.IDUser || shared) && id_dataset == SetsManager.instance.DataSet.id_dataset)
                        {
                            ColormapList.Add(colormapnname);
                            ColormapIDList.Add(id_colormap);
                            GameObject mapobj = Instantiate(_ColormapItemDummy, _ColormapList);
                            mapobj.name = colormapnname;
                            mapobj.transform.localPosition = _ColormapItemDummy.transform.localPosition;
                            TextMeshPro mapobjtext = mapobj.transform.GetComponentInChildren<TextMeshPro>();
                            ColormapText.Add(mapobjtext);
                            if (mapobjtext != null)
                            {
                                mapobjtext.text = colormapnname;
                            }
                            XRColormapItem coloritem = mapobj.transform.GetComponentInChildren<XRColormapItem>();
                            if (coloritem != null)
                            {
                                coloritem.Init(id_colormap, colormapnname, _ListItemColliders, SelectColormap);
                            }
                            _ColormapItems.Add(mapobj);
                            it++;
                        }
                    }
                }

                Display();
                //Instantiate colormap name objects
            }
            www.Dispose();
        }

        private void ApplyColor(TextMeshPro t, Color c)
        {
            if (t != null)
                t.color = c;
        }

        private void SelectColormap(XRColormapItem infoItem)
        {
            selectedMap = infoItem;
            MorphoDebug.Log("Selected : " + selectedMap.ColorName);
            for (int i = 0; i < ColormapList.Count; i++)
            {
                if (ColormapList[i] == selectedMap.name)
                {
                    ApplyColor(ColormapText[i], Color.cyan);
                }
                else if (ColormapList[i] != "None")
                {
                    ApplyColor(ColormapText[i], Color.white);
                }
            }
        }

        public void ApplyColormap()
        {
            if (selectedMap != null && selectedMap.ColorName != "None")
            {
                StartCoroutine(queryLoadColormap(selectedMap.ColorID));
                BindCurrentFeedback();
            }
            else
            {
                ResetColormap();
            }
        }

        public void ResetColormap()
        {
            for (int i = 0; i < 255; i++)
            {
                SelectionManager.createMaterial(i);
            }
            for (int i = 0; i < ColormapList.Count; i++)
            {
                if (ColormapList[i] != "None")
                {
                    ApplyColor(ColormapText[i], Color.white);
                }
            }
            selectedMap = null;
            BindCurrentFeedback();
            SelectionDisplay.UpdateAllSpheres();
        }

        public override void SetButtonsColliders(List<Collider> colliders)
        {
            _Apply.SetColliders(colliders);
            _Reset.SetColliders(colliders);
            _Up.SetColliders(colliders);
            _Down.SetColliders(colliders);

            _ListItemColliders = colliders;
        }

        private void SetButtonsListeners()
        {
            _Apply.OnCollision.RemoveAllListeners();
            _Reset.OnCollision.RemoveAllListeners();
            _Up.OnCollision.RemoveAllListeners();
            _Down.OnCollision.RemoveAllListeners();
            _Apply.OnCollision.AddListener(ApplyColormap);
            _Reset.OnCollision.AddListener(ResetColormap);
            _Up.OnCollision.AddListener(PreviousPage);
            _Down.OnCollision.AddListener(NextPage);
        }

        public IEnumerator queryLoadColormap(int id_colormap)
        {
            WWWForm form = new WWWForm();
            form.AddField("colormap", "2");
            form.AddField("id_colormap", id_colormap.ToString());
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "/api/colormapinfos/?hash=" + SetsManager.instance.hash + "&id_colormap=" + id_colormap.ToString());
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                //split the returned text for memory managment in webGL

                //Return colors ....
                string[] sep = { "b\'{" };
                string[] sep2 = { "}," };
                string[] colors = www.downloadHandler.text.Split(sep, System.StringSplitOptions.None)[1].Split(sep2, System.StringSplitOptions.None);
                yield return new WaitForEndOfFrame();

                if (colors.Length > 0)
                {
                    for (int i = 0; i < colors.Length; i++)
                    { //Number of Uploaded Data
                        var Map = new JSONNode();
                        if (i == colors.Length - 1)
                        {
                            string text = "{" + colors[i].Substring(0, colors[i].Length - 4);
                            Map = JSONNode.Parse(text);
                        }
                        else
                        {
                            Map = JSONNode.Parse("{" + colors[i] + "}}");
                        }
                        string shader = Map[0]["shader"].ToString().Replace("\"", "");
                        shader = shader.Replace(" (Instance)", "");
                        Material m = ChangeColorSelection.getMaterialByName(shader);

                        int R = Map[0]["R"].AsInt;
                        int G = Map[0]["G"].AsInt;
                        int B = Map[0]["B"].AsInt;
                        int A = Map[0]["A"].AsInt;
                        m.color = new Color((float)R / 255f, (float)G / 255f, (float)B / 255f, (float)A / 255f);
                        //then set special parameters
                        foreach (string k in Map[0]["params"].Keys.ToList())
                        {
                            if (m.HasProperty(k))
                            {
                                if (Map[0]["params"][k].AsArray != null && Map[0]["params"][k].AsArray.Count > 1)
                                {//if color
                                    var ArrRBGA = Map[0]["params"][k].AsArray;
                                    string[] rgba = new string[4];
                                    for (int j = 0; j < ArrRBGA.Count; j++)
                                    {
                                        rgba[j] = ArrRBGA[j].ToString().Replace("\"", "");
                                    }
                                    m.UpdateColorProperty(k, new Color(float.Parse(rgba[0], System.Globalization.CultureInfo.InvariantCulture), float.Parse(rgba[1], System.Globalization.CultureInfo.InvariantCulture), float.Parse(rgba[2], System.Globalization.CultureInfo.InvariantCulture), float.Parse(rgba[3], System.Globalization.CultureInfo.InvariantCulture)));
                                }
                                else
                                {
                                    m.UpdateFloatProperty(k, float.Parse(Map[0]["params"][k].Value, System.Globalization.CultureInfo.InvariantCulture));
                                }
                            }
                        }

                        SelectionManager.materials[i] = m;
                        yield return new WaitForEndOfFrame();
                    }
                }
                InterfaceManager.instance.MenuObjects.gameObject.transform.Find("Selection").gameObject.GetComponent<SelectionManager>().resetAllMaterials();
                SelectionDisplay.UpdateAllSpheres();
            }
            www.Dispose();
            InterfaceManager.instance.MenuObjects.transform.Find("MenuLoadSelection").Find("Upload Panel").gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            SetButtonsListeners();
            ClearColormapsItem();
            ColormapList = new List<string>();
            ColormapText = new List<TextMeshPro>();
            ColormapIDList = new List<int>();
            ListAllColormap();
        }
    }
}