using System;
using System.Collections.Generic;
using UnityEngine;

namespace MorphoNet.UI.XR
{
    public class XRColormapItem : MonoBehaviour
    {
        [SerializeField]
        private XRButton _OnCollisionButton;

        public string ColorName;
        public int ColorID;

        public void SetCollisionAction(Action onCollision)
        {
            _OnCollisionButton.OnCollision.RemoveAllListeners();
            _OnCollisionButton.OnCollision.AddListener(() => onCollision());
        }

        public void SetButtonColliders(List<Collider> colliders) => _OnCollisionButton.SetColliders(colliders);

        
        internal void Init(int mapId,string name, List<Collider> colliders, Action<XRColormapItem> onCollision = null)
        {
            _OnCollisionButton.SetText(name);
            ColorName = name;
            ColorID = mapId;
            SetButtonColliders(colliders);

            if (onCollision != null)
                SetCollisionAction(() => onCollision(this));
        }
    }
}