using System.Collections.Generic;
using TMPro;

using UnityEngine;

namespace MorphoNet.UI.XR
{

    public class XRMainMenuPart : XRMenuPart
    {
        [Header("Main Menu Part")]
        [SerializeField]
        protected TextMeshPro _Title;

        [SerializeField]
        protected XRToolBar _ToolBar;

        [SerializeField]
        protected XRButton _PinButton;

        [SerializeField]
        protected XRButton _CloseButton;

        protected XRPanel _Container;

        protected Transform _PinContainer;

        protected Pose _DefaultLocalPose;

        protected bool _Pinned;

        [SerializeField]
        private Transform _XRElementsContainer;

        protected override void Init()
        {
            base.Init();
            _Pinned = false;
            SaveLocalPose();

            _PinButton.OnCollision.AddListener(Pin);
            _CloseButton.OnCollision.AddListener(Hide);
        }

        public void SetContainerPanel(XRPanel panel)
        {
            _Container = panel;
            _Container.ReplaceMenuInDefaultParent(this);
        }

        public void SetPinContainer(Transform t)
        {
            _PinContainer = t;
        }

        private void SaveLocalPose()
        {
            _DefaultLocalPose = new Pose(transform.localPosition, transform.localRotation);
        }

        private void ResetLocalPose()
        {
            transform.localPosition = _DefaultLocalPose.position;
            transform.localRotation = _DefaultLocalPose.rotation;
        }

        public void Pin()
        {
            if (!_Pinned)
            {
                //transform.SetParent(_PinContainer);
                transform.SetParent(null);
                _PinOffset = _PinContainer.position - transform.position;

                _PinButton.OnCollision.RemoveListener(Pin);
                _PinButton.OnCollision.AddListener(UnPin);

                _Pinned = true;
            }
        }

        private Vector3 _PinOffset;

        private void Update()
        {
            if (_Pinned)
            {
                transform.position = _PinContainer.position - _PinOffset;
            }
        }

        public void UnPin()
        {
            if (_Pinned)
            {
                _Container.ReplaceMenuInDefaultParent(this);
                ResetLocalPose();

                _PinButton.OnCollision.RemoveListener(UnPin);
                _PinButton.OnCollision.AddListener(Pin);

                _Pinned = false;
            }
        }

        public void ShowInfoBar()
        {
            if (_ToolBar != null)
                _ToolBar.Show();
        }

        public void HideInfoBar()

        {
            if (_ToolBar != null)
                _ToolBar.Hide();
        }

        protected override Transform GetXRElementsContainer() => _XRElementsContainer;

        public override void Show()
        {
            UnPin();

            ShowInfoBar();

            base.Show();

            SwitchMenuActive();
        }

        public override void Hide()
        {
            if (_Pinned)
                return;

            HideInfoBar();

            base.Hide();

            SwitchMenuInactive();
        }

        public override void SetButtonsColliders(List<Collider> colliders)
        {
            base.SetButtonsColliders(colliders);

            if (_PinButton != null)
                _PinButton.SetColliders(colliders);

            if (_CloseButton != null)
                _CloseButton.SetColliders(colliders);

            if (_ToolBar != null)
                _ToolBar.SetButtonsColliders(colliders);
        }

        public override void SetMediator(XRMediator xrMediator)
        {
            base.SetMediator(xrMediator);

            if (_ToolBar != null)
                _ToolBar.SetMediator(xrMediator);
        }

        public virtual void SwitchMenuActive()
        {
            // TODO: visual indicator on the menu.
        }

        public virtual void SwitchMenuInactive()
        {
            // TODO: visual indicator on the menu.
        }
    }
}