using UnityEngine;


[RequireComponent(typeof(Renderer))]
public class SelectionVisualisationSphere : MonoBehaviour
{
    public void SetMaterial(Material material)
    {
        GetComponent<Renderer>().material = material;
    }
}

