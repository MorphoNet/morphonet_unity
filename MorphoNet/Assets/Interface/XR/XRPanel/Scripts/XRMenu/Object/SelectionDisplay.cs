using MorphoNet.Extensions.Unity;
using System.Collections.Generic;
using UnityEngine;

namespace MorphoNet
{
    public class SelectionDisplay : MonoBehaviour
    {
        [Header("Configuration")]
        [SerializeField]
        private float _DisplayRadius;

        [SerializeField]
        private SelectionVisualisationSphere SpherePrefab;

        [SerializeField]
        private int _NumberOfSphereDisplayed = 7;

        private SelectionVisualisationSphere _FocusedSphere;
        private List<SelectionVisualisationSphere> _PreviousSpheres;
        private List<SelectionVisualisationSphere> _NextSpheres;

        public float _RotationAngleBetweenSpheres;
        public int _FocusedSphereNumber = 0;
        public int TargetSphereNumber = 0;

        private int _NumberOfPreviousSphere;
        private int _NumberOfNextSphere;

        public float _CurrentAngle = 0f;

        public float DesiredAngle;
        private float TargetAngle => _RotationAngleBetweenSpheres * (TargetSphereNumber - 1);

        private const float _BaseRotationSpeed = 72;
        private float RotationSpeed => _BaseRotationSpeed * Mathf.Abs(TargetSphereNumber - _FocusedSphereNumber);

        public void Init(int initialSelection)
        {
            initialSelection = Mathf.Max(Mathf.Min(initialSelection, SelectionManager.materials.Length - 1), 0);

            if (_FocusedSphere != null)
                Destroy(_FocusedSphere.gameObject);

            if (_PreviousSpheres != null)
                for (int i = _PreviousSpheres.Count - 1; i >= 0; i--)
                    Destroy(_PreviousSpheres[i].gameObject);

            if (_NextSpheres != null)
                for (int i = _NextSpheres.Count - 1; i >= 0; i--)
                    Destroy(_NextSpheres[i].gameObject);

            _RotationAngleBetweenSpheres = 360f / 2f / _NumberOfSphereDisplayed;
            _NumberOfPreviousSphere = (_NumberOfSphereDisplayed - 1) / 2;
            _NumberOfNextSphere = (_NumberOfSphereDisplayed - 1) / 2;

            _PreviousSpheres = new List<SelectionVisualisationSphere>(_NumberOfPreviousSphere);
            _NextSpheres = new List<SelectionVisualisationSphere>(_NumberOfNextSphere);

            _FocusedSphere = Instantiate(SpherePrefab, transform);

            for (int i = 0; i < _NumberOfPreviousSphere; i++)
                _PreviousSpheres.Add(Instantiate(SpherePrefab, transform));

            for (int i = 0; i < _NumberOfNextSphere; i++)
                _NextSpheres.Add(Instantiate(SpherePrefab, transform));

            TargetSphereNumber = initialSelection;
            _CurrentAngle = TargetAngle;
            UpdateSpheres(initialSelection);
        }

        public void UpdateAllSpheres()
        {
            Init(_FocusedSphereNumber);
        }

        public void UpdateSpheres(int selectionValue)
        {
            _FocusedSphereNumber = Mathf.Max(Mathf.Min(selectionValue, SelectionManager.materials.Length - 1), 0);

            UpdateSpheresPose();
            UpdateSpheresDisplay(_FocusedSphereNumber);
        }

        public void UpdateSpheresPose()
        {
            _FocusedSphere.transform.localPosition = new Vector3(0, 0, -_DisplayRadius);

            for (int i = 0; i < _PreviousSpheres.Count; i++)
            {
                _PreviousSpheres[i].transform.localPosition = new Vector3(0, 0, -_DisplayRadius);
                _PreviousSpheres[i].transform.RotateAround(transform.position, transform.up, _RotationAngleBetweenSpheres * (i + 1));
            }

            for (int i = 0; i < _NextSpheres.Count; i++)
            {
                _NextSpheres[i].transform.localPosition = new Vector3(0, 0, -_DisplayRadius);
                _NextSpheres[i].transform.RotateAround(transform.position, transform.up, -_RotationAngleBetweenSpheres * (i + 1));
            }
        }

        public void UpdateSpheresDisplay(int focusedSphereNumber)
        {
            focusedSphereNumber = Mathf.Max(0, focusedSphereNumber);
            _FocusedSphere.SetMaterial(SelectionManager.materials[focusedSphereNumber]);

            for (int i = 0; i < _PreviousSpheres.Count; i++)
            {
                int sphereNumber = focusedSphereNumber - (i + 1);
                if (sphereNumber >= 0)
                {
                    _PreviousSpheres[i].gameObject.Activate();
                    _PreviousSpheres[i].SetMaterial(SelectionManager.materials[sphereNumber]);
                }
                else
                {
                    _PreviousSpheres[i].gameObject.Deactivate();
                }
            }

            for (int i = 0; i < _NextSpheres.Count; i++)
            {
                int sphereNumber = focusedSphereNumber + i + 1;
                if (sphereNumber < SelectionManager.materials.Length)
                {
                    _NextSpheres[i].gameObject.Activate();
                    _NextSpheres[i].SetMaterial(SelectionManager.materials[sphereNumber]);
                }
                else
                {
                    _NextSpheres[i].gameObject.Deactivate();
                }
            }
        }

        public void GotToNext()
        {
            TargetSphereNumber = Mathf.Min(SelectionManager.materials.Length - 1, TargetSphereNumber + 1);
        }

        public void GotToPrevious()
        {
            TargetSphereNumber = Mathf.Max(0, TargetSphereNumber - 1);
        }

        #region Rotation methods

        private int GetSphereNumberByAngle(float angle)
            => Mathf.FloorToInt(angle) / Mathf.FloorToInt(_RotationAngleBetweenSpheres);

        private float _AccumulatedAngle = 0f;

        private void RotateTowardPreviousSphere()
        {
            float frameAngleRotation = Time.deltaTime * -RotationSpeed;
            _CurrentAngle += frameAngleRotation;
            _AccumulatedAngle += frameAngleRotation;

            if (Mathf.Approximately(_CurrentAngle, TargetAngle) || _CurrentAngle <= TargetAngle)
            {
                _AccumulatedAngle = 0f;

                UpdateSpheres(TargetSphereNumber);

                return;
            }
            else if (Mathf.Approximately(_AccumulatedAngle, _RotationAngleBetweenSpheres)
                     || _AccumulatedAngle <= _RotationAngleBetweenSpheres)
            {
                _AccumulatedAngle = 0f;
            }

            RotateShpereBy(frameAngleRotation);
        }

        private void RotateTowardNextSphere()
        {
            float frameAngleRotation = Time.deltaTime * RotationSpeed;
            _CurrentAngle += frameAngleRotation;
            _AccumulatedAngle += frameAngleRotation;

            if (Mathf.Approximately(_CurrentAngle, TargetAngle) || _CurrentAngle >= TargetAngle)
            {
                _AccumulatedAngle = 0f;

                UpdateSpheres(TargetSphereNumber);

                return;
            }
            else if (Mathf.Approximately(_AccumulatedAngle, -_RotationAngleBetweenSpheres)
                     || _AccumulatedAngle <= -_RotationAngleBetweenSpheres)
            {
                _AccumulatedAngle = 0f;
            }

            RotateShpereBy(frameAngleRotation);
        }

        private void RotateShpereBy(float frameAngleRotation)
        {
            for (int i = 0; i < _NextSpheres.Count; i++)
                _NextSpheres[i].transform.RotateAround(transform.position, transform.up, frameAngleRotation);

            for (int i = 0; i < _PreviousSpheres.Count; i++)
                _PreviousSpheres[i].transform.RotateAround(transform.position, transform.up, frameAngleRotation);

            _FocusedSphere.transform.RotateAround(transform.position, transform.up, frameAngleRotation);
        }

        #endregion Rotation methods

        #region Unity methods

        private void Update()
        {
            if (_CurrentAngle < TargetAngle)
                RotateTowardNextSphere();
            else if (_CurrentAngle > TargetAngle)
                RotateTowardPreviousSphere();

            DesiredAngle = TargetAngle;
        }

        #endregion Unity methods
    }
}