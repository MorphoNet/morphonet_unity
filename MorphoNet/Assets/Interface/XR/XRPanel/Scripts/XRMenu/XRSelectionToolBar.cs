using UnityEngine;

namespace MorphoNet.UI.XR
{
    public class XRSelectionToolBar : XRToolBar
    {
        [SerializeField]
        private XRElement SelectedCellsInfoLabel;

        [SerializeField]
        private XRButton InvertCellsSelectionBoutton;

        [SerializeField]
        private XRButton ClearCellsSelectionButton;

        [SerializeField]
        private XRButton ShowSelectedCellsButton;

        [SerializeField]
        private XRButton HideSelectedCellsBoutton;

        [SerializeField]
        private XRButton SelectCellsSiblingsBoutton;

        [SerializeField]
        private XRButton GiveCellsRandomColorsBoutton;

        [Header("Advanced selection")]
        [SerializeField]
        private XRToggleButton _SelectionMethodButton;


        #region inner-types

        public enum SelectionMethod
        {
            Collision,
            Ray
        }

        #endregion

        protected override void Init()
        {
            base.Init();
            XRMediator.OnCellSelectionInfoLabeUpdate += SelectedCellsInfoLabel.SetText;
            InvertCellsSelectionBoutton.OnCollision.AddListener(XRMediator.InvertCellsSelection);
            ClearCellsSelectionButton.OnCollision.AddListener(XRMediator.ClearCellsSelection);
            ShowSelectedCellsButton.OnCollision.AddListener(XRMediator.ShowSelectedCells);
            HideSelectedCellsBoutton.OnCollision.AddListener(XRMediator.HideSelectedCells);
            SelectCellsSiblingsBoutton.OnCollision.AddListener(XRMediator.SelectCellsSiblings);
            GiveCellsRandomColorsBoutton.OnCollision.AddListener(XRMediator.GiveCellsRandomColors);
            _SelectionMethodButton.OnToggleOn.AddListener(() => SetSelectionMethod(SelectionMethod.Ray));
            _SelectionMethodButton.OnToggleOff.AddListener(() => SetSelectionMethod(SelectionMethod.Collision));

        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        void Update()
        {
            SelectedCellsInfoLabel.SetText(InterfaceManager.instance.MenuClicked.transform.Find("selectedcells").GetComponent<UnityEngine.UI.Text>().text);
        }

        #region selection-methods


        private void SetSelectionMethod(SelectionMethod method)
        {
            switch (method)
            {
                case SelectionMethod.Ray:
                    XRMediator.TriggerSelectionWithRay();
                    break;

                case SelectionMethod.Collision:
                default:
                    XRMediator.TriggerSelectionWithCollision();
                    break;
            }
        }

        #endregion

    }
}