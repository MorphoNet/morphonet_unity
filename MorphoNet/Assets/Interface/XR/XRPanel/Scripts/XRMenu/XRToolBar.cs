using UnityEngine;

namespace MorphoNet.UI.XR
{
    public class XRToolBar : XRMenuPart
    {
        protected override Transform GetXRElementsContainer() => transform;

        protected override void Init()
        {
            base.Init();
        }
    }
}