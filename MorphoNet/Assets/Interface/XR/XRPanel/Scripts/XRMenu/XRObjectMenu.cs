using MorphoNet.Extensions.Unity;
using System.Collections.Generic;
using UnityEngine;

namespace MorphoNet.UI.XR
{
    public class XRObjectMenu : XRMainMenuPart
    {
        #region Inner types

        private enum BulkSelectionType
        {
            None,
            CurrentSelection,
            AnySelection,
            WithoutSelection,
            MultipleSelection,
            Picked
        }

        #endregion Inner types

        #region UI References

        [Header("Material/Id Selection")]
        [SerializeField]
        private XRLabel _ItemWhithSelection;

        [SerializeField]
        private XRLabel _SelectionId;

        [SerializeField]
        private XRButton _GoToPreviousSelection;

        [SerializeField]
        private XRButton _GoToNextSelection;

        [SerializeField]
        private XRButton _GoToNextFreeSelection;

        [SerializeField]
        private XRButton _ApplySelection;

        [SerializeField]
        private XRButton _GetPickedSelection;

        [SerializeField]
        private SelectionDisplay _SelectionDisplay;

        [Header("Bulk selection Group")]
        [SerializeField]
        private XRToggleButton _SetActionOnItemsInCurrentSelection;

        [SerializeField]
        private XRToggleButton _SetActionItemsWithSelection;

        [SerializeField]
        private XRToggleButton _SetActionOnItemsWithoutSelection;

        [SerializeField]
        private XRToggleButton _SetActionOnItemsWithMultipleSelection;

        [SerializeField]
        private XRToggleButton _SetActionOnItemManualyPicked;

        [Header("Selection Actions")]
        [SerializeField]
        private GameObject _ItemActionsContainer;

        [SerializeField]
        private XRLabel _ItemActionsLabel;

        [SerializeField]
        private XRButton _ClearItems;

        [SerializeField]
        private XRButton _PickItems;

        [SerializeField]
        private XRButton _ShowItems;

        [SerializeField]
        private XRButton _HideItems;

        [SerializeField]
        private XRButton _ToggleMap;

        [SerializeField]
        public GameObject _ColormapMenu;

        #endregion UI References

        private BulkSelectionType _CurrentBulkSelection;

        private void Update()
        {
            UpdateAdvanceSelectionInfo();
        }

        public override void SwitchMenuActive()
        {
            base.SwitchMenuActive();
            _SetActionOnItemsInCurrentSelection.ToggleOn();
        }

        public override void SwitchMenuInactive()
        {
            base.SwitchMenuInactive();
            XRMediator.ResetXRCommandModifiers();
        }

        public void ToggleColormapMenu()
        {
            if (_ColormapMenu != null)
                _ColormapMenu.SetActive(!_ColormapMenu.activeSelf);
        }

        protected override void Init()
        {
            base.Init();

            SetButtonsListeners();

            _CurrentBulkSelection = BulkSelectionType.CurrentSelection;
            _SelectionDisplay.Init(XRMediator.GetSelectionValue() - 1);
            _SetActionOnItemsInCurrentSelection.ToggleOn();
        }

        public override void SetButtonsColliders(List<Collider> colliders)
        {
            _GoToPreviousSelection.SetColliders(colliders);
            _GoToNextSelection.SetColliders(colliders);
            _GoToNextFreeSelection.SetColliders(colliders);
            _ApplySelection.SetColliders(colliders);

            _SetActionOnItemsInCurrentSelection.SetColliders(colliders);
            _SetActionItemsWithSelection.SetColliders(colliders);
            _SetActionOnItemsWithoutSelection.SetColliders(colliders);
            _SetActionOnItemsWithMultipleSelection.SetColliders(colliders);
            _SetActionOnItemManualyPicked.SetColliders(colliders);

            _ClearItems.SetColliders(colliders);
            _PickItems.SetColliders(colliders);
            _ShowItems.SetColliders(colliders);
            _HideItems.SetColliders(colliders);
            _ToggleMap.SetColliders(colliders);
            _GetPickedSelection.SetColliders(colliders);
            _ColormapMenu.GetComponent<XRColormapList>().SetButtonsColliders(colliders);
            ToggleColormapMenu();
            base.SetButtonsColliders(colliders);
        }

        private void SetButtonsListeners()
        {
            _ApplySelection.OnCollision.AddListener(() => XRMediator.ApplySelectionToPickedObjects());

            _GoToPreviousSelection.OnCollision.AddListener(PreviousSelection);
            _GoToNextSelection.OnCollision.AddListener(NextSelection);
            _GoToNextFreeSelection.OnCollision.AddListener(NextFreeSelection);

            _SetActionOnItemsInCurrentSelection.OnToggleOn.AddListener(
                () => ChangeCurrentBulkSelection(BulkSelectionType.CurrentSelection));
            _SetActionItemsWithSelection.OnToggleOn.AddListener(
                () => ChangeCurrentBulkSelection(BulkSelectionType.AnySelection));
            _SetActionOnItemsWithoutSelection.OnToggleOn.AddListener(
                () => ChangeCurrentBulkSelection(BulkSelectionType.WithoutSelection));
            _SetActionOnItemsWithMultipleSelection.OnToggleOn.AddListener(
                () => ChangeCurrentBulkSelection(BulkSelectionType.MultipleSelection));
            _SetActionOnItemManualyPicked.OnToggleOn.AddListener(
                () => ChangeCurrentBulkSelection(BulkSelectionType.Picked));

            _ClearItems.OnCollision.AddListener(ClearItems);
            _PickItems.OnCollision.AddListener(PickItems);
            _ShowItems.OnCollision.AddListener(ShowItems);
            _HideItems.OnCollision.AddListener(HideItems);
            _GetPickedSelection.OnCollision.AddListener(GetPickedSelection);
            _ToggleMap.OnCollision.AddListener(ToggleColormapMenu);
        }

        private void UpdateActionButtonDisplay()
        {
            _HideItems.gameObject.Activate();
            _ShowItems.gameObject.Activate();

            if (_CurrentBulkSelection == BulkSelectionType.Picked)
                _PickItems.Interactable = false;
            else
                _PickItems.Interactable = true;

            if (_CurrentBulkSelection == BulkSelectionType.WithoutSelection)
                _ClearItems.Interactable = false;
            else
                _ClearItems.Interactable = true;

            if (_CurrentBulkSelection == BulkSelectionType.None)
                _ItemActionsContainer.Deactivate();
            else
                _ItemActionsContainer.Activate();
        }

        private void ChangeCurrentBulkSelection(BulkSelectionType newSelection)
        {
            _SetActionItemsWithSelection.ToggleOff();
            _SetActionOnItemsWithoutSelection.ToggleOff();
            _SetActionOnItemsWithMultipleSelection.ToggleOff();
            _SetActionOnItemManualyPicked.ToggleOff();
            _SetActionOnItemsInCurrentSelection.ToggleOff();

            if (_CurrentBulkSelection == newSelection)
                _CurrentBulkSelection = BulkSelectionType.None;
            else
                _CurrentBulkSelection = newSelection;

            UpdateActionButtonDisplay();
        }

        private void ClearItems()
        {
            var selectionManager = MorphoTools.GetDataset().selection_manager;

            switch (_CurrentBulkSelection)
            {
                case BulkSelectionType.CurrentSelection:
                    selectionManager.clearThis();
                    break;

                case BulkSelectionType.AnySelection:
                    selectionManager.clearAll();
                    break;

                case BulkSelectionType.WithoutSelection:
                    break;

                case BulkSelectionType.MultipleSelection:
                    selectionManager.clearMulti();
                    break;

                case BulkSelectionType.Picked:
                    selectionManager.clearPicked();
                    break;

                default:
                    break;
            }
        }

        private void GetPickedSelection()
        {
            var selectionManager = MorphoTools.GetDataset().selection_manager;
            selectionManager.selectAsSelected();

            _SelectionDisplay.TargetSphereNumber = selectionManager.SelectionValue - 1;
        }

        private void PickItems()
        {
            var selectionManager = MorphoTools.GetDataset().selection_manager;

            switch (_CurrentBulkSelection)
            {
                case BulkSelectionType.CurrentSelection:
                    selectionManager.onSelect();
                    break;

                case BulkSelectionType.AnySelection:
                    selectionManager.onSelectAll();
                    break;

                case BulkSelectionType.WithoutSelection:
                    selectionManager.onSelectNone();
                    break;

                case BulkSelectionType.MultipleSelection:
                    selectionManager.onSelectMulti();
                    break;

                case BulkSelectionType.Picked:
                    break;

                default:
                    break;
            }
        }

        private void ShowItems()
        {
            var selectionManager = MorphoTools.GetDataset().selection_manager;

            switch (_CurrentBulkSelection)
            {
                case BulkSelectionType.CurrentSelection:
                    selectionManager.showThis();
                    break;

                case BulkSelectionType.AnySelection:
                    selectionManager.showAll();
                    break;

                case BulkSelectionType.WithoutSelection:
                    selectionManager.showNonSelected();
                    break;

                case BulkSelectionType.MultipleSelection:
                    selectionManager.showMulti();
                    break;

                case BulkSelectionType.Picked:
                    selectionManager.showPickedCells();
                    break;

                default:
                    break;
            }
        }

        private void HideItems()
        {
            var selectionManager = MorphoTools.GetDataset().selection_manager;

            switch (_CurrentBulkSelection)
            {
                case BulkSelectionType.CurrentSelection:
                    selectionManager.hideThis();
                    break;

                case BulkSelectionType.AnySelection:
                    selectionManager.hideAll();
                    break;

                case BulkSelectionType.WithoutSelection:
                    selectionManager.hideNonSelected();
                    break;

                case BulkSelectionType.MultipleSelection:
                    selectionManager.hideMulti();
                    break;

                case BulkSelectionType.Picked:
                    selectionManager.hidePickedCells();
                    break;

                default:
                    break;
            }
        }

        private void PreviousSelection()
        {
            XRMediator.PreviousSelection();
            _SelectionDisplay.GotToPrevious();
        }

        private void NextSelection()
        {
            XRMediator.NextSelection();
            _SelectionDisplay.GotToNext();
        }

        private void NextFreeSelection()
        {
            var selectionManager = MorphoTools.GetDataset().selection_manager;

            XRMediator.NextFreeSelection();
            _SelectionDisplay.TargetSphereNumber = selectionManager.SelectionValue - 1;
        }

        private void UpdateAdvanceSelectionInfo()
        {
            if (XRMediator is null)
                return;

            _ItemWhithSelection.SetText($"{XRMediator.CountObjectsWithCurrentSelection()} objects with this selection.");
            _SelectionId.SetText($"{_SelectionDisplay.TargetSphereNumber + 1}");
            _ItemActionsLabel.SetText(GetItemActionLabelText());
        }

        private string GetItemActionLabelText()
        {
            switch (_CurrentBulkSelection)
            {
                case BulkSelectionType.AnySelection:
                    return $"Selected; Objects with selection: {XRMediator.CountObjectsWithAnySelection()}.";

                case BulkSelectionType.WithoutSelection:
                    return $"Unselected; Objects without selection: {XRMediator.CountObjectsWithoutSelection()}.";

                case BulkSelectionType.MultipleSelection:
                    return $"Multi-selected; Objects with multiple selecitons: {XRMediator.CountObjectsWithMultipleSelection()}.";

                case BulkSelectionType.Picked:
                    return $"Picked; Objects picked: {MorphoTools.GetPickedManager().clickedCells.Count}.";

                case BulkSelectionType.CurrentSelection:
                default:
                    return $"Current; Object with this label: {XRMediator.CountObjectsWithCurrentSelection()}.";
            }
        }
    }
}