using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MorphoNet.UI.XR
{
    public class XRRawDataMenu : XRMainMenuPart, IRawDataUI
    {
        #region UI References

        [SerializeField]
        private XRToggleButton _VisibilityButton;

        [SerializeField]
        private XRToggleButton _CutMeshButton;

        [SerializeField]
        private XRToggleButton _CutMeshOnXAxisButton;

        [SerializeField]
        private XRToggleButton _CutMeshOnYAxisButton;

        [SerializeField]
        private XRToggleButton _CutMeshOnZAxisButton;

        [SerializeField]
        private Slider _XAxisSlider;

        [SerializeField]
        private Slider _YAxisSlider;

        [SerializeField]
        private Slider _ZAxisSlider;

        [SerializeField]
        private Slider _ThicknessSlider;

        [SerializeField]
        private Slider _ThresholdSlider;

        [SerializeField]
        private Slider _TransparencySlider;

        [SerializeField]
        private Slider _IntensitySlider;

        [SerializeField]
        private XRToggleButton _FreeHandPlanCutButton;

        #endregion UI References

        private void SetUIListeners()
        {
            _VisibilityButton.OnToggleOn.AddListener(() => ToggleVisibility(true));
            _VisibilityButton.OnToggleOff.AddListener(() => ToggleVisibility(false));

            _CutMeshButton.OnToggleOn.AddListener(() => ToggleCutMesh(true));
            _CutMeshButton.OnToggleOff.AddListener(() => ToggleCutMesh(false));

            _CutMeshOnXAxisButton.OnToggleOn.AddListener(() => ToggleCutMeshX(true));
            _CutMeshOnXAxisButton.OnToggleOff.AddListener(() => ToggleCutMeshX(false));

            _CutMeshOnYAxisButton.OnToggleOn.AddListener(() => ToggleCutMeshY(true));
            _CutMeshOnYAxisButton.OnToggleOff.AddListener(() => ToggleCutMeshY(false));

            _CutMeshOnZAxisButton.OnToggleOn.AddListener(() => ToggleCutMeshZ(true));
            _CutMeshOnZAxisButton.OnToggleOff.AddListener(() => ToggleCutMeshZ(false));

            _FreeHandPlanCutButton.OnToggleOn.AddListener(() => ToggleFreeHandCutMesh(true));
            _FreeHandPlanCutButton.OnToggleOff.AddListener(() => ToggleFreeHandCutMesh(false));

            _IntensitySlider.onValueChanged.AddListener(_ => RawDataUpdate());
            _ThicknessSlider.onValueChanged.AddListener(_ => RawDataUpdate());
            _ThresholdSlider.onValueChanged.AddListener(_ => RawDataUpdate());
            _TransparencySlider.onValueChanged.AddListener(_ => RawDataUpdate());

            _XAxisSlider.onValueChanged.AddListener(_ => RawDataUpdate());
            _YAxisSlider.onValueChanged.AddListener(_ => RawDataUpdate());
            _ZAxisSlider.onValueChanged.AddListener(_ => RawDataUpdate());
        }

        private IMenuController<IRawDataUI> MenuController;

        public override void Hide()
        {
            base.Hide();

            XRMediator.DeactivateCuttingPlaneControl();
            XRMediator.UnattachCuttingPlaneToHand();
        }

        private void ToggleVisibility(bool visible)
        {
            _VisibilityActivated = visible;
            RawDataUpdate();
        }

        private void ToggleCutMesh(bool cut)
        {
            _CutMeshActivated = cut;

            XRMediator.ToggleMeshCutting(_CutMeshActivated);

            if (_CutMeshActivated && _FreeHandCutActivated)
                XRMediator.ActivateCuttingPlane();
            else
                XRMediator.DeactivateCuttingPlane();

            RawDataUpdate();
        }

        private void ToggleFreeHandCutMesh(bool freeHandCut)
        {
            _FreeHandCutActivated = freeHandCut;
            _XAxisSlider.interactable = !freeHandCut;
            _YAxisSlider.interactable = !freeHandCut;
            _ZAxisSlider.interactable = !freeHandCut;

            // Attaching to hand works but need UI activation instead of automatic.
            // On hand plan may need a better physical representation.
            if (freeHandCut && _VisibilityActivated)
            {
                XRMediator.ActivateCuttingPlaneControl();
                
                //XRMediator.AttachCuttingPlaneToHand();
            }
            else
            {
                XRMediator.DeactivateCuttingPlaneControl();
                //XRMediator.UnattachCuttingPlaneToHand();
                
            }
            if (freeHandCut)
            {
                XRMediator.ActivateCuttingPlane();
            }
            else 
            {
                XRMediator.DeactivateCuttingPlane();
            }

            RawDataUpdate();
        }

        private void ToggleCutMeshX(bool cut)
        {
            _CutMeshXActivated = cut;
            RawDataUpdate();
        }

        private void ToggleCutMeshY(bool cut)
        {
            _CutMeshYActivated = cut;
            RawDataUpdate();
        }

        private void ToggleCutMeshZ(bool cut)
        {
            _CutMeshZActivated = cut;
            RawDataUpdate();
        }

        #region MainMenuPart Override

        protected override void Init()
        {
            base.Init();

            SetUIListeners();

            MenuController = MorphoTools.GetDataset().RawImages;

            MenuController.RegisterUI(this);
        }

        #endregion MainMenuPart Override

        private void OnDestroy()
        {
            MenuController.UnRegisterUI(this);
        }

        #region IRawDataUI implementation

        public void RawDataUpdate()
        {
            MenuController.UpdateUIFrom(this);
        }

        private bool _VisibilityActivated = false;
        private bool _CutMeshActivated = false;
        private bool _FreeHandCutActivated = false;
        private bool _CutMeshXActivated = false;
        private bool _CutMeshYActivated = false;
        private bool _CutMeshZActivated = false;

        public bool GetVisibilityButtonValue() => _VisibilityActivated;

        public bool GetCutMeshButtonValue() => _CutMeshActivated;

        public bool GetFreeHandCutButtonValue() => _FreeHandCutActivated;

        public bool GetCutMeshOnXAxisButtonValue() => _CutMeshXActivated;

        public bool GetCutMeshOnYAxisButtonValue() => _CutMeshYActivated;

        public bool GetCutMeshOnZAxisButtonValue() => _CutMeshZActivated;

        public float GetIntensitySliderValue() => _IntensitySlider.value;

        public float GetThicknessSliderValue() => _ThicknessSlider.value;

        public float GetThresholdSliderValue() => _ThresholdSlider.value;

        public float GetTransparencySliderValue() => _TransparencySlider.value;

        public float GetXAxisSliderValue() => _XAxisSlider.value;

        public float GetYAxisSliderValue() => _YAxisSlider.value;

        public float GetZAxisSliderValue() => _ZAxisSlider.value;

        public void UpdateUIWithoutNotify(IRawDataUI source)
        {
            //_VisibilityButton.ToggleWithoutNotify(source.GetVisibilityButtonValue());
            //_VisibilityActivated = source.GetVisibilityButtonValue();

            _CutMeshButton.ToggleWithoutNotify(source.GetCutMeshButtonValue());
            _CutMeshActivated = source.GetCutMeshButtonValue();
            /*_CutMeshOnXAxisButton.ToggleWithoutNotify(source.GetCutMeshOnXAxisButtonValue());
            _CutMeshXActivated = source.GetCutMeshOnXAxisButtonValue();
            _CutMeshOnYAxisButton.ToggleWithoutNotify(source.GetCutMeshOnYAxisButtonValue());
            _CutMeshYActivated = source.GetCutMeshOnYAxisButtonValue();
            _CutMeshOnZAxisButton.ToggleWithoutNotify(source.GetCutMeshOnZAxisButtonValue());
            _CutMeshZActivated = source.GetCutMeshOnZAxisButtonValue();*/

            /*_XAxisSlider.SetValueWithoutNotify(source.GetXAxisSliderValue());
            _YAxisSlider.SetValueWithoutNotify(source.GetYAxisSliderValue());
            _ZAxisSlider.SetValueWithoutNotify(source.GetZAxisSliderValue());

            _IntensitySlider.SetValueWithoutNotify(source.GetIntensitySliderValue());
            _ThicknessSlider.SetValueWithoutNotify(source.GetThicknessSliderValue());
            _ThresholdSlider.SetValueWithoutNotify(source.GetThresholdSliderValue());
            _TransparencySlider.SetValueWithoutNotify(source.GetTransparencySliderValue());*/
        }

        public void InitSliders(
            (float min, float max, float value) xSlider,
            (float min, float max, float value) ySlider,
            (float min, float max, float value) zSlider)
        {
            _XAxisSlider.SetValueWithoutNotify(xSlider.value);
            _XAxisSlider.minValue = xSlider.min;
            _XAxisSlider.maxValue = xSlider.max;

            _YAxisSlider.SetValueWithoutNotify(ySlider.value);
            _YAxisSlider.minValue = ySlider.min;
            _YAxisSlider.maxValue = ySlider.max;

            _ZAxisSlider.SetValueWithoutNotify(zSlider.value);
            _ZAxisSlider.minValue = zSlider.min;
            _ZAxisSlider.maxValue = zSlider.max;
        }

        public bool GetChannelVisibilityButtonValue(int channel)
        {
            throw new NotImplementedException();
        }

        public float GetPlaneSliderValue()
        {
            throw new NotImplementedException();
        }


        public float GetThresholdFieldValue()
        {
            throw new NotImplementedException();
        }

        public float GetMaxThresholdFieldValue()
        {
            throw new NotImplementedException();
        }

        public float GetTransparencyFieldValue()
        {
            throw new NotImplementedException();
        }

        public float GetIntensityFieldValue()
        {
            throw new NotImplementedException();
        }

        public int GetColorBarDropDownValue()
        {
            throw new NotImplementedException();
        }

        public bool GetCuttingPlaneButtonValue()
        {
            throw new NotImplementedException();
        }

        public bool GetCutMeshValue()
        {
            throw new NotImplementedException();
        }

        public int GetPlaneAxisValue()
        {
            throw new NotImplementedException();
        }

        public bool GetCuttingPlaneInverseValue()
        {
            throw new NotImplementedException();
        }

        public bool GetThinModeValue()
        {
            throw new NotImplementedException();
        }

        public Vector2 GetAlphaMinValue()
        {
            throw new NotImplementedException();
        }

        public Vector2 GetAlphaMaxValue()
        {
            throw new NotImplementedException();
        }

        public (float, float) GetColorBarModSliderValue()
        {
            throw new NotImplementedException();
        }

        public (float, float) GetHistoMinMaxSliderValue()
        {
            throw new NotImplementedException();
        }

        public int GetMinValue()
        {
            throw new NotImplementedException();
        }

        public int GetMaxValue()
        {
            throw new NotImplementedException();
        }

        public float GetMinHistoFieldValue()
        {
            throw new NotImplementedException();
        }

        public float GetMaxHistoFieldValue()
        {
            throw new NotImplementedException();
        }

        public float GetMinCmapFieldValue()
        {
            throw new NotImplementedException();
        }

        public float GetMaxCmapFieldValue()
        {
            throw new NotImplementedException();
        }

        #endregion IRawDataUI implementation
    }
}