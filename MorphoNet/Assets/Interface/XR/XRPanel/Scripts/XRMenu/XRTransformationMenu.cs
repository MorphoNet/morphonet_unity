using UnityEngine;

namespace MorphoNet.UI.XR
{
    public class XRTransformationMenu : XRMainMenuPart
    {
        [Header("Transformation Menu")]
        [SerializeField]
        private XRButton _ActivateScaling;

        [SerializeField]
        private XRButton _ResetScaling;

        [SerializeField]
        private XRButton _ActivateScattering;

        [SerializeField]
        private XRButton _ResetScattering;

        public override void Show()
        {
            XRMediator.ResetXRCommandModifiers();
            base.Show();
        }

        public override void Hide()
        {
            XRMediator.ResetXRCommandModifiers();
            base.Hide();
        }

        public void ResetDatasetScaling() => XRMediator.ResetDatasetScaling();

        public void ResetDatasetScattering() => XRMediator.ResetDatasetScattering();

        public void ActivateScaling() => XRMediator.ActivateScaling();

        public void ActivateScattering() => XRMediator.ActivateScattering();

        public void DeactivateScattering() => XRMediator.DeactivateScattering();

        public void ShowPlane()
        {
            XRMediator.ActivateCuttingPlaneControl();
        }

        public void HidePlane()
        {
            XRMediator.ResetXRCommandModifiers();
        }

        private bool _PlaneActive;

        public void ActivateCuttingPlane()
        {
            _PlaneActive = true;
            XRMediator.ToggleMeshCutting(_PlaneActive);
            XRMediator.ActivateCuttingPlane();
            ShowPlane();
        }

        public void DeactivateCuttingPlane()
        {
            _PlaneActive = false;
            XRMediator.ToggleMeshCutting(_PlaneActive);
            XRMediator.DeactivateCuttingPlane();
            HidePlane();
        }

        public void ResetCuttingPlanePose()
        {
            XRMediator.ResetCuttingPlanePose(_PlaneActive);
            if (_PlaneActive)
            {
                DeactivateCuttingPlane();
                ActivateCuttingPlane();
            }
        }
    }
}