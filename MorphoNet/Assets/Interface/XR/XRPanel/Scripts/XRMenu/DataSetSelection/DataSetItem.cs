using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace MorphoNet.UI.XR
{
    public class DataSetItem : MonoBehaviour
    {
        public RangeSlider TimestampRangeSlider;

        [SerializeField]
        private InputField _MinInputField;

        [SerializeField]
        private InputField _MaxInputField;

        [SerializeField]
        private XRButton _OnCollisionButton;

        [SerializeField]
        private TMPro.TextMeshPro _Label;

        [SerializeField]
        private Canvas _LocalCanvas;

        public string Label
        {
            get { return _Label.text; }
            set { _Label.text = value; }
        }

        public void SetCollisionAction(Action onCollision)
        {
            _OnCollisionButton.OnCollision.RemoveAllListeners();
            _OnCollisionButton.OnCollision.AddListener(() => onCollision());
        }

        public void SetButtonColliders(List<Collider> colliders) => _OnCollisionButton.SetColliders(colliders);

        internal void Init(List<Collider> colliders, string buttonLabel, DatasetParameterItem boundDatasetParameterItem, Canvas parentCanvas, Action onCollision = null)
        {
            _LocalCanvas.worldCamera = parentCanvas.worldCamera;
            BoundXrTimestampSliderToDesktopParameter(boundDatasetParameterItem);

            Label = buttonLabel;
            SetButtonColliders(colliders);

            if (onCollision != null)
                SetCollisionAction(onCollision);
        }

        #region Timestamp selection UI update

        private void BoundXrTimestampSliderToDesktopParameter(DatasetParameterItem boundDatasetParameterItem)
        {
            TimestampRangeSlider.MinValue = boundDatasetParameterItem.TimestampRangeSlider.MinValue;
            TimestampRangeSlider.MaxValue = boundDatasetParameterItem.TimestampRangeSlider.MaxValue;
            TimestampRangeSlider.LowValue = boundDatasetParameterItem.TimestampRangeSlider.LowValue;
            TimestampRangeSlider.HighValue = boundDatasetParameterItem.TimestampRangeSlider.HighValue;
            SyncInputFieldsWithoutNotify(TimestampRangeSlider.LowValue, TimestampRangeSlider.HighValue);

            // XR fields --> XR Slider
            _MinInputField.onValueChanged.AddListener(OnFieldLowValueChange);
            _MaxInputField.onValueChanged.AddListener(OnFieldHighValueChange);

            // XR Slider --> XR fields
            TimestampRangeSlider.OnValueChanged.AddListener((low, high) =>
            {
                SyncInputFieldsWithoutNotify(low, high);
                boundDatasetParameterItem.TimestampRangeSlider.SetValueWithoutNotify(low, high);
                boundDatasetParameterItem.SyncInputFieldsWithoutNotify(low, high);
            });

            // Desktop Fields --> XR values
            boundDatasetParameterItem.MinInputField.onValueChanged.AddListener(value =>
            {
                OnFieldLowValueChange(value);
                SyncInputFieldsWithoutNotify(TimestampRangeSlider.LowValue, TimestampRangeSlider.HighValue);
            });
            boundDatasetParameterItem.MaxInputField.onValueChanged.AddListener(value =>
            {
                OnFieldHighValueChange(value);
                SyncInputFieldsWithoutNotify(TimestampRangeSlider.LowValue, TimestampRangeSlider.HighValue);
            });

            // Desktop Slider --> XR values
            boundDatasetParameterItem.TimestampRangeSlider.OnValueChanged.AddListener((low, high) =>
            {
                TimestampRangeSlider.SetValueWithoutNotify(low, high);
                SyncInputFieldsWithoutNotify(low, high);
            });
        }

        private void SyncInputFieldsWithoutNotify(float min, float max)
        {
            _MinInputField.SetTextWithoutNotify(((int)min).ToString());
            _MaxInputField.SetTextWithoutNotify(((int)max).ToString());
        }

        private void OnFieldLowValueChange(string value)
        {
            if (float.TryParse(value, out float low))
                SyncSliderWithoutNotify(Mathf.Max(low, TimestampRangeSlider.LowValue), TimestampRangeSlider.HighValue);
            else
                SyncSliderWithoutNotify(TimestampRangeSlider.LowValue, TimestampRangeSlider.HighValue);
        }

        private void OnFieldHighValueChange(string value)
        {
            if (float.TryParse(value, out float high))
                SyncSliderWithoutNotify(TimestampRangeSlider.LowValue, Mathf.Min(high, TimestampRangeSlider.MaxValue));
            else
                SyncSliderWithoutNotify(TimestampRangeSlider.LowValue, TimestampRangeSlider.HighValue);
        }

        private void SyncSliderWithoutNotify(float low, float high) => TimestampRangeSlider.SetValueWithoutNotify(low, high);

        #endregion Timestamp selection UI update
    }
}