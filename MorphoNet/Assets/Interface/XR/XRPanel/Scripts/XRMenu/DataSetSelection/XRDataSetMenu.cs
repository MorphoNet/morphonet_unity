using MorphoNet.Extensions.Unity;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;

namespace MorphoNet.UI.XR
{
    public class XRDataSetMenu : XRMainMenuPart
    {
        [SerializeField]
        private Canvas _Canvas;

        [SerializeField]
        private int _ItemPerPages = 5;

        [SerializeField]
        private float _ItemsLocalHeight = 5f;

        public int CurrentPage { get; private set; } = 0;

        public int NumberOfPages => Items.Count / _ItemPerPages;

        public List<DataSetItem> Items { get; set; } = new List<DataSetItem>();

        [SerializeField]
        private DataSetItem _ItemPrefab;

        [SerializeField]
        private Transform _ListContainer;

        [Header("List Control")]
        [SerializeField]
        private XRButton _NextListPage;

        [SerializeField]
        private XRButton _PreviousListPage;

        private List<Collider> _ListItemColliders = new List<Collider>();

        public void InitButtons()
        {
            RegisterXRElements();
            _NextListPage.OnCollision.AddListener(NextPage);
            _PreviousListPage.OnCollision.AddListener(PreviousPage);
        }

        protected override Transform GetXRElementsContainer()
        {
            return transform;
        }

        protected override void RegisterXRElements()
        {
            base.RegisterXRElements();
        }

        public override void SetButtonsColliders(List<Collider> colliders)
        {
            base.SetButtonsColliders(colliders);
            _ListItemColliders = colliders;
            UpdateListItemColliders();
        }

        private void UpdateListItemColliders()
        {
            if (_ListItemColliders.Count == 0)
                return;

            foreach (var item in Items)
                item.SetButtonColliders(_ListItemColliders);
        }

        public void AddItem(string buttonLabel, DatasetParameterItem boundDatasetParemeterItem, Action onCollision)
        {
            DataSetItem item = Instantiate(_ItemPrefab, _ListContainer);
            item.Init(_ListItemColliders, buttonLabel, boundDatasetParemeterItem, _Canvas, onCollision);

            Items.Add(item);

            item.gameObject.Deactivate();
            Display();
        }

        public void Display()
        {
            HideItems();

            int startIndex = CurrentPage * _ItemPerPages;
            int maxIndex = Mathf.Min(Items.Count, startIndex + _ItemPerPages);

            for (int i = startIndex; i < maxIndex; i++)
            {
                var item = _ListContainer.GetChild(i).gameObject;
                item.Activate();

                float zPosition = -_ItemsLocalHeight / (_ItemPerPages - 1) * (i % _ItemPerPages);
                item.transform.localPosition = new Vector3(0, 0, zPosition);
            }
        }

        public void HideItems()
        {
            for (int i = _ListContainer.childCount - 1; i >= 0; i--)
                _ListContainer.GetChild(i).gameObject.Deactivate();
        }

        public void NextPage()
        {
            if (CurrentPage == NumberOfPages)
                return;

            CurrentPage = Mathf.Min(NumberOfPages, CurrentPage + 1);
            Display();
        }

        public void PreviousPage()
        {
            if (CurrentPage == 0)
                return;

            CurrentPage = Mathf.Max(0, CurrentPage - 1);
            Display();
        }
    }
}