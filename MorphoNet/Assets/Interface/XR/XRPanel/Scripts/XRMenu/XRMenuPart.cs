using System.Collections.Generic;

using UnityEngine;

namespace MorphoNet.UI.XR
{
    public abstract class XRMenuPart : MonoBehaviour
    {
        protected XRMediator XRMediator;

        protected List<XRElement> XRElements = new List<XRElement>();

        protected abstract Transform GetXRElementsContainer();

        protected virtual void Init()
        {
            RegisterXRElements();
            Hide();
        }

        protected virtual void RegisterXRElements()
        {
            Transform elementsContainer = GetXRElementsContainer();
            XRElements.Clear();

            foreach (XRElement element in elementsContainer.GetComponentsInChildren<XRElement>())
                if (element != null)
                    XRElements.Add(element);
        }

        public virtual void SetMediator(XRMediator xrMediator)
        {
            XRMediator = xrMediator;
            Init();
        }

        public virtual void Show() => gameObject.SetActive(true);

        public virtual void Hide() => gameObject.SetActive(false);

        public virtual void SetButtonsColliders(List<Collider> colliders)
        {
            foreach (XRElement element in XRElements)
            {
                if (element is XRButton button)
                    button.SetColliders(colliders);
            }
        }
    }
}