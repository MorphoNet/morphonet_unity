using AssemblyCSharp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MorphoNet
{
    public class TimelineSelection : MonoBehaviour
    {
        public Button HighlightSelectedCellsLifeButton;
        public Button ClearSelectedCellsHighlightButton;
        public Button ClearAllHighlightButton;
        public Button ClearAllSelectionButton;
        public Button SelectSelectedCellsLifeButton;

        private DataSet _Dataset => SetsManager.instance.DataSet;
        private PickedManager _Picker => _Dataset.PickedManager;

        public List<List<Cell>> HighlightedLifeTime = new List<List<Cell>>();

        private void Start()
        {
            HighlightSelectedCellsLifeButton.onClick.AddListener(HighlightSelectedCellsLife);
            ClearSelectedCellsHighlightButton.onClick.AddListener(ClearSelectedCellsHighlight);
            ClearAllHighlightButton.onClick.AddListener(ClearAllHighlight);
            ClearAllSelectionButton.onClick.AddListener(ClearAllSelection);
            SelectSelectedCellsLifeButton.onClick.AddListener(SelectSelectedCellsLife);
        }

        public void HighlightSelectedCellsLife()
        {
            foreach (Cell cell in _Picker.clickedCells)
            {
                var cellLife = cell.GetCellLife();
                if (!HighlightedLifeTime.Contains(cellLife))
                {
                    foreach (Cell cellTime in cellLife)
                    {
                        cellTime.Highlighted = true;
                    }
                    HighlightedLifeTime.Add(cellLife);
                }
            }
            ShowHlt();
        }

        private void ShowHlt()
        {
            string s = $"List number: {HighlightedLifeTime.Count}";
            foreach (var list in HighlightedLifeTime)
            {
                s += "    (";
                foreach (var item in list)
                {
                    s += $"{item.getName()} ; ";
                }
                s += ")\n";
            }
            MorphoDebug.LogWarning(s);
        }

        public void ClearSelectedCellsHighlight()
        {
            foreach (Cell cell in _Picker.clickedCells)
            {
                var cellLife = cell.GetCellLife();
                if (HighlightedLifeTime.Contains(cellLife))
                {
                    foreach (Cell cellTime in cellLife)
                    {
                        cellTime.Highlighted = false;
                    }
                }
                HighlightedLifeTime.Remove(cellLife);
            }
            ShowHlt();
        }

        public void ClearAllHighlight()
        {
            foreach (var lifetime in HighlightedLifeTime)
            {
                foreach (var cell in lifetime)
                {
                    cell.Highlighted = false;
                }
            }
            HighlightedLifeTime.Clear();
            ShowHlt();
        }

        public void ClearAllSelection() => _Picker.ClearClickedCell();

        public void SelectSelectedCellsLife()
        {
            foreach (Cell cell in _Picker.clickedCells)
            {
                var cellLife = cell.GetCellLife();
                foreach (Cell cellTime in cellLife)
                    _Picker.AddCellToSelection(cellTime);
            }
        }
    }
}