
using UnityEngine;

namespace MorphoNet.UI.XR
{
    public class XRCuttingPlaneMenu : XRMainMenuPart
    {

        public void ShowPlane()
        {
            if (_KeepPlaneActive)
                KeepPlaneActiveOnHide();
            else
                KeepPlaneUnactiveOnHide();

            ActivateCuttingPlane();
            XRMediator.ActivateCuttingPlaneControl();

            base.Show();
        }

        public void HidePlane()
        {
            if (!_KeepPlaneActive)
                DeactivateCuttingPlane();

            XRMediator.ResetXRCommandModifiers();

            base.Hide();
        }

        [Header("Cutting Plane Menu")]
        [SerializeField]
        private XRButton _SwitchKeepPlaneActiveButton;

        private bool _KeepPlaneActive = false;
        private bool _PlaneActive;


        public void ActivateCuttingPlane()
        {
            _PlaneActive = true;
            XRMediator.ActivateCuttingPlane();
        }

        public void DeactivateCuttingPlane()
        {
            _PlaneActive = false;
        }

        public void ResetCuttingPlanePose()
        {
            XRMediator.ResetCuttingPlanePose(_PlaneActive);
        }

        private void KeepPlaneActiveOnHide()
        {
            _KeepPlaneActive = true;

            _SwitchKeepPlaneActiveButton.SetText("Deactive on close");
            _SwitchKeepPlaneActiveButton.OnCollision.RemoveAllListeners();
            _SwitchKeepPlaneActiveButton.OnCollision.AddListener(KeepPlaneUnactiveOnHide);
        }

        private void KeepPlaneUnactiveOnHide()
        {
            _KeepPlaneActive = false;

            _SwitchKeepPlaneActiveButton.SetText("Keep active on close");
            _SwitchKeepPlaneActiveButton.OnCollision.RemoveAllListeners();
            _SwitchKeepPlaneActiveButton.OnCollision.AddListener(KeepPlaneActiveOnHide);
        }
    }

}