using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace MorphoNet.UI.XR
{
    public class QuantitativeInfoItem : InfoItem
    {
        public override void SetButtonColliders(List<Collider> colliders)
        {
            _SelectionButton.SetColliders(colliders);
        }

        protected override void SetAction(Action onCollision)
        {
            _SelectionButton.OnCollision.RemoveAllListeners();
            _SelectionButton.OnCollision.AddListener(() =>
                {
                    onCollision();
                    Select();
                });
        }

        protected override void SetSelectionAction(Action onCollision)
        {
            SetAction(onCollision);
        }

        public override void UnSelect()
        {
            _SelectionButton.ToggleOffWithoutNotify();
        }

        internal override void Init(
            Correspondence correspondence,
            List<Collider> colliders,
            Action<InfoItem> onSelection = null,
            Action onActionPressed = null)
        {
            _Label.SetText(correspondence.name);
            Correspondance = correspondence;
            SetButtonColliders(colliders);

            if (onSelection != null)
                SetSelectionAction(() => onSelection(this));
        }
    }
}