using MorphoNet.Extensions.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MorphoNet.UI.XR
{
    public class XRInfoMenu : XRMainMenuPart
    {
        private enum CorrespondenceType
        {
            Label,
            Number,
            String
        }

        private class InfoList
        {
            private const int _ItemPerPages = 5;
            public string Name { get; private set; }
            public int CurrentPage { get; private set; } = 0;

            public int NumberOfPages => Items.Count / _ItemPerPages;

            public List<InfoItem> Items { get; set; }
            private readonly InfoItem _SelectionItemPrefab;
            private readonly Transform _ListContainer;

            public InfoList(string name, InfoItem itemPrefab, Transform listContainer)
            {
                Name = name;
                Items = new List<InfoItem>();
                _SelectionItemPrefab = itemPrefab;

                GameObject emptyGameObject = new GameObject($"{Name}_container");
                _ListContainer = Instantiate(emptyGameObject.transform, listContainer);
                Destroy(emptyGameObject);
            }

            public void AddItem(Correspondence correspondence, List<Collider> buttonTriggeringCollider, Action<InfoItem> onSelection = null, Action onActionPressed = null)
            {
                InfoItem item = Instantiate(_SelectionItemPrefab, _ListContainer);
                item.Init(correspondence, buttonTriggeringCollider, onSelection, onActionPressed);

                Items.Add(item);

                item.gameObject.Deactivate();
            }

            public void Display()
            {
                HideItems();

                int startIndex = CurrentPage * _ItemPerPages;
                int maxIndex = Mathf.Min(Items.Count, startIndex + _ItemPerPages);

                for (int i = startIndex; i < maxIndex; i++)
                {
                    var item = _ListContainer.GetChild(i).gameObject;
                    item.Activate();

                    float zPosition = -5f / (_ItemPerPages - 1) * (i % _ItemPerPages);
                    item.transform.localPosition = new Vector3(0, 0, zPosition);
                }
            }

            public void HideItems()
            {
                for (int i = _ListContainer.childCount - 1; i >= 0; i--)
                    _ListContainer.GetChild(i).gameObject.Deactivate();
            }

            public void NextPage()
            {
                if (CurrentPage == NumberOfPages)
                    return;

                CurrentPage = Mathf.Min(NumberOfPages, CurrentPage + 1);
                Display();
            }

            public void PreviousPage()
            {
                if (CurrentPage == 0)
                    return;

                CurrentPage = Mathf.Max(0, CurrentPage - 1);
                Display();
            }
        }

        [SerializeField]
        private Transform _InfoListContainer;

        [SerializeField]
        private InfoItem _InfoListItemPrefab;

        [SerializeField]
        private SelectionInfoItem _SelectionInfoListItemPrefab;

        [SerializeField]
        private QuantitativeInfoItem _QuantitativeInfoListItemPrefab;

        [Header("Info Category Control")]
        [SerializeField]
        private XRLabel _InfoCategoryTittle;

        [SerializeField]
        private XRButton _NextCategory;

        [SerializeField]
        private XRButton _PreviousCategory;

        [Header("Info List Control")]
        [SerializeField]
        private XRButton _NextListPage;

        [SerializeField]
        private XRButton _PreviousListPage;

        [Header("Color Map Menu")]
        [SerializeField]
        private XRColorMapSelectionMenu _ColorMapMenu;

        private Correspondence _CurrentCorrespondence;

        private Dictionary<CorrespondenceType, InfoList> _InfoLists;
        private CorrespondenceType _SelectedListType = CorrespondenceType.Label;
        private InfoItem _SelectedInfo = null;

        private List<Collider> _ListItemColliders = new List<Collider>();

        /// <summary>
        /// Last <see cref="Correspondence"/> added to download. <c>Null</c> if no <see cref="Correspondence"/> is loading.
        /// </summary>
        private Correspondence _LastLoadingCorrespondance = null;

        /// <summary>
        /// Set of all <see cref="Correspondence"/> loading at once. Used to prevent the download of the same <see cref="Correspondence"/> multiple time.
        /// </summary>
        private readonly HashSet<Correspondence> _LoadingCorrespondances = new HashSet<Correspondence>();

        #region Initialisation

        protected override void Init()
        {
            base.Init();

            InitInfoLists();

            InitInfoButtons();

            InitColorMapButtons();

            DisplaySelectedList();
        }

        private void InitInfoLists()
        {
            _CurrentCorrespondence = null;

            _InfoLists = new Dictionary<CorrespondenceType, InfoList>
            {
                [CorrespondenceType.Label] = new InfoList(CorrespondenceType.Label.ToString(), _SelectionInfoListItemPrefab, _InfoListContainer),
                [CorrespondenceType.Number] = new InfoList(CorrespondenceType.Number.ToString(), _QuantitativeInfoListItemPrefab, _InfoListContainer)
            };

            foreach (Correspondence correspondence in XRMediator.InformationsCorrespondences)
            {
                string typeName = correspondence.ct.name;
                if (typeName == CorrespondenceType.Label.ToString())
                    _InfoLists[CorrespondenceType.Label].AddItem(
                        correspondence,
                        _ListItemColliders,
                        onSelection: SetSelectedInfoItem,
                        onActionPressed: () => XRMediator.StartApplyingSelectionCorrespondance(_SelectedInfo.Correspondance));

                if (typeName == CorrespondenceType.Number.ToString())
                    _InfoLists[CorrespondenceType.Number].AddItem(
                        correspondence,
                        _ListItemColliders,
                        onSelection: OnQuantitativeItemSelected,
                        onActionPressed: null);
            }

            _SelectedListType = CorrespondenceType.Label;
        }

        private void OnQuantitativeItemSelected(InfoItem item)
        {
            SetSelectedInfoItem(item);

            if (item == null)
            {
                MorphoDebug.LogWarning("null");
                return;
            }

            if (_LastLoadingCorrespondance == item.Correspondance || _LoadingCorrespondances.Contains(item.Correspondance))
            {
                MorphoDebug.LogWarning("already loading");
                return;
            }

            if (item.Correspondance.IsActive)
            {
                MorphoDebug.LogWarning("just oppening");
                ShowColormapSubMenu(item.Correspondance);
            }
            else
            {
                MorphoDebug.LogWarning("new correspondance loading!");
                _LastLoadingCorrespondance = item.Correspondance;
                _LoadingCorrespondances.Add(item.Correspondance);
                _LastLoadingCorrespondance.OnCorrespondenceActivation += OnCorrespondenceAcitvation;

                XRMediator.DownloadCorrespondence(item.Correspondance);
            }
        }

        private void OnCorrespondenceAcitvation(Correspondence cor)
        {
            if (cor == _LastLoadingCorrespondance)
            {
                ShowColormapSubMenu(cor);
                _LastLoadingCorrespondance = null;
            }

            _LoadingCorrespondances.Remove(cor);

            cor.OnCorrespondenceActivation -= OnCorrespondenceAcitvation;
        }

        private void InitInfoButtons()
        {
            _NextCategory.OnCollision.AddListener(DisplayNextCategory);
            _PreviousCategory.OnCollision.AddListener(DisplayPreviousCategory);
            _NextListPage.OnCollision.AddListener(() => _InfoLists[_SelectedListType].NextPage());
            _PreviousListPage.OnCollision.AddListener(() => _InfoLists[_SelectedListType].PreviousPage());
        }

        private void InitColorMapButtons()
        {
            _ColorMapMenu.PreviousColorMabButton.OnCollision.AddListener(() =>
            {
                UpdateIndexColormaps(-1);
                UpdateXRInterfaceFromDesktop();
            });
            _ColorMapMenu.NextColorMabButton.OnCollision.AddListener(() =>
            {
                UpdateIndexColormaps(1);
                UpdateXRInterfaceFromDesktop();
            });

            _ColorMapMenu.ThresholdSlider.OnValueChanged.AddListener(SetColorBarThreshold);
            _ColorMapMenu.ColorMapRangeSlider.OnValueChanged.AddListener(AffineColorMap);

            _ColorMapMenu.ValidateColorMap.OnCollision.AddListener(ApplyColorMapModifications);

            _ColorMapMenu.CancelModification.OnCollision.AddListener(ResetColorMapSubMenu);
        }

        private void UpdateXRInterfaceFromDesktop()
        {
            (float minv, float maxv) = XRMediator.GetColorBarThresholdValues();
            SetColorBarThreshold(minv, maxv);
            (int minc, int maxc) = XRMediator.GetAffineColorMapValues();
            AffineColorMap(minc, maxc);
        }

        private void ApplyColorMapModifications()
        {
            XRMediator.SetColorMapIndex(_CurrentColorMapIndex);

            float min = _ColorMapMenu.ThresholdSlider.LowValue;
            float max = _ColorMapMenu.ThresholdSlider.HighValue;

            XRMediator.SetColorBarThreshold(min, max);

            min = _ColorMapMenu.ColorMapRangeSlider.LowValue;
            max = _ColorMapMenu.ColorMapRangeSlider.HighValue;
            XRMediator.AffineColorMap(min, max);

            XRMediator.UpdateColorMap();
        }

        #endregion Initialisation

        #region Category and Item Navigation

        private void SetSelectedInfoItem(InfoItem infoItem)
        {
            if (_SelectedInfo != null)
                _SelectedInfo.UnSelect();

            _SelectedInfo = infoItem;
        }

        private void DisplaySelectedList()
        {
            if (_SelectedInfo != null)
                _SelectedInfo.UnSelect();

            HideColormapSubMenu();

            foreach (var list in _InfoLists)
                list.Value.HideItems();

            _InfoCategoryTittle.SetText(_InfoLists[_SelectedListType].Name);
            _InfoLists[_SelectedListType].Display();
        }

        private void DisplayNextCategory()
        {
            _SelectedListType = _SelectedListType switch
            {
                CorrespondenceType.Label => CorrespondenceType.Number,
                CorrespondenceType.Number => CorrespondenceType.Label,
                _ => _SelectedListType,
            };
            DisplaySelectedList();
        }

        private void DisplayPreviousCategory()
        {
            _SelectedListType = _SelectedListType switch
            {
                CorrespondenceType.Label => CorrespondenceType.Number,
                CorrespondenceType.Number => CorrespondenceType.Label,
                _ => _SelectedListType,
            };
            DisplaySelectedList();
        }

        #endregion Category and Item Navigation

        #region Menu Methods

        protected override Transform GetXRElementsContainer()
        {
            return transform;
        }

        protected override void RegisterXRElements()
        {
            base.RegisterXRElements();
            XRElements.Add(_NextCategory);
            XRElements.Add(_PreviousCategory);
            XRElements.Add(_NextListPage);
            XRElements.Add(_PreviousListPage);
            XRElements.Add(_ColorMapMenu.NextColorMabButton);
            XRElements.Add(_ColorMapMenu.PreviousColorMabButton);
        }

        public override void SetButtonsColliders(List<Collider> colliders)
        {
            base.SetButtonsColliders(colliders);
            _ListItemColliders = colliders;
            UpdateListItemColliders();
        }

        private void UpdateListItemColliders()
        {
            if (_ListItemColliders.Count == 0)
                return;

            foreach (InfoList list in _InfoLists.Values)
            {
                foreach (var item in list.Items)
                    item.SetButtonColliders(_ListItemColliders);
            }
        }

        #endregion Menu Methods

        #region ColorMap

        public void SetColorBarThreshold(float min, float max)
        {
            _ColorMapMenu.SetThresholdText(min, max);
        }

        public void AffineColorMap(float min, float max)
        {
            _ColorMapMenu.SetAffinedColorMapSprite(MenuColorBar.instance.AssignSprite(_CurrentColorMapIndex, min, max));
        }

        public void HideColormapSubMenu()
        {
            _ColorMapMenu.Hide();
            _CurrentCorrespondence = null;
        }

        private void ShowColormapSubMenu(Correspondence choosenCorrespondence)
        {
            if (choosenCorrespondence == null)
                return;

            _CurrentCorrespondence = choosenCorrespondence;
            _CurrentCorrespondence.changeColorBar();

            ResetColorMapSubMenu();

            _ColorMapMenu.Show();
        }

        /// <summary>
        /// Resets menu value with the Color Map internal values.
        /// </summary>
        private void ResetColorMapSubMenu()
        {
            var (low, high) = XRMediator.GetColorBarThresholdValues();
            var (min, max) = XRMediator.GetColorBarThresholdBounds();

            _ColorMapMenu.ThresholdSlider.MaxValue = max;
            _ColorMapMenu.ThresholdSlider.MinValue = min;

            _ColorMapMenu.ThresholdSlider.LowValue = low;
            _ColorMapMenu.ThresholdSlider.HighValue = high;

            (low, high) = XRMediator.GetAffineColorMapValues();
            (min, max) = XRMediator.GetAffineColorMapBounds();

            _ColorMapMenu.ColorMapRangeSlider.MaxValue = max;
            _ColorMapMenu.ColorMapRangeSlider.MinValue = min;

            _ColorMapMenu.ColorMapRangeSlider.LowValue = low;
            _ColorMapMenu.ColorMapRangeSlider.HighValue = high;

            _CurrentColorMapIndex = MenuColorBar.instance.MapColorV;
            SetColormapInterface(_CurrentColorMapIndex);
            UpdateXRInterfaceFromDesktop();
        }

        private int _CurrentColorMapIndex = 0;

        private void UpdateIndexColormaps(int shift)
        {
            _CurrentColorMapIndex += shift;

            int maxNumber = XRMediator.GetNumberOfColormap();

            if (_CurrentColorMapIndex < 0)
                _CurrentColorMapIndex = maxNumber - 1;
            else if (_CurrentColorMapIndex >= maxNumber)
                _CurrentColorMapIndex = 0;

            SetColormapInterface(_CurrentColorMapIndex);
        }

        private void SetColormapInterface(int index)
        {
            Sprite sprite = XRMediator.GetColormapUsingIndex(index);
            string txt = XRMediator.GetColormapTextUsingIndex(index);

            _ColorMapMenu.SetColorMapSprite(sprite);
            _ColorMapMenu.SetText(txt);
        }

        #endregion ColorMap
    }
}