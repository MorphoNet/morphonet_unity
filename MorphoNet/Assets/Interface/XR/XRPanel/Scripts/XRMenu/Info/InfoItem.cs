using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace MorphoNet.UI.XR
{
    public class InfoItem : MonoBehaviour
    {
        [SerializeField]
        protected TextMeshPro _Label;

        [SerializeField]
        protected XRToggleButton _SelectionButton;

        [SerializeField]
        protected XRButton _ActionButton;

        public Correspondence Correspondance { get; protected set; }

        protected virtual void SetSelectionAction(Action onCollision)
        {
            _SelectionButton.OnCollision.RemoveAllListeners();
            _SelectionButton.OnCollision.AddListener(() =>
                {
                    onCollision();
                    Select();
                    _ActionButton.Interactable = true;
                });
        }

        public void Select()
        {
            _SelectionButton.ToggleOnWithoutNotify();
        }

        public virtual void UnSelect()
        {
            _SelectionButton.ToggleOffWithoutNotify();
            _ActionButton.Interactable = false;
        }

        protected virtual void SetAction(Action onCollision)
        {
            _ActionButton.OnCollision.RemoveAllListeners();
            _ActionButton.OnCollision.AddListener(() => onCollision());
        }

        public virtual void SetButtonColliders(List<Collider> colliders)
        {
            _SelectionButton.SetColliders(colliders);
            _ActionButton.SetColliders(colliders);
        }

        internal virtual void Init(Correspondence correspondence, List<Collider> colliders, Action<InfoItem> onSelection = null, Action onActionPressed = null)
        {
            _Label.SetText(correspondence.name);
            Correspondance = correspondence;
            SetButtonColliders(colliders);

            if (onSelection != null)
                SetSelectionAction(() => onSelection(this));

            if (onActionPressed != null)
                SetAction(onActionPressed);

            _ActionButton.Interactable = false;
        }
    }
}