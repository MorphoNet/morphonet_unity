using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MorphoNet.UI.XR
{
    public class XRPanel : MonoBehaviour
    {
        private XRMainMenuPart _CurrentlyDisplayedMenu = null;

        private XRMainMenuPart _PreviouslyDisplayedMenu;

        [SerializeField]
        private XRToolBar _QuickToolBar;

        [SerializeField]
        private List<Collider> _XRButtonTriggeringColliders;

        [SerializeField]
        private List<XRMenuPart> _Menus = new List<XRMenuPart>();

        [SerializeField]
        private Transform _MenusContainer;

        [SerializeField]
        private Transform _PinnedMenusContainer;

        [SerializeField]
        private TextMeshProUGUI _TimestampLabel;

        [SerializeField]
        private Slider _TimestampSlider;

        [SerializeField]
        private XRSelectionToolBar _PickedMenu;

        [SerializeField]
        private TextMeshPro _XRFeedbackText;

        private XRMediator _XRMediator;

        private void Start()
        {
            RegisterMenus();

            UpdateMediator();
            UpdateButtonTriggeringColliders();

            SetMenusContainer();
            SetPinnedMenusContainer();

            ShowToolBar();
            ShowPickedMenu();
        }

        public void Init(XRMediator mediator, Transform pinnedMenuContainer, List<Collider> menuTriggeringColliders)
        {
            SetXRMediator(mediator);
            _PinnedMenusContainer = pinnedMenuContainer;
            _XRButtonTriggeringColliders = menuTriggeringColliders;
            _TimestampSlider.maxValue = SetsManager.instance.DataSet.MaxTime;
            _TimestampSlider.minValue = SetsManager.instance.DataSet.MinTime;
            UpdateTimestampUI(SetsManager.instance.DataSet.CurrentTime);
            _TimestampSlider.onValueChanged.AddListener(value => GoToTimeStamp((int)value));
        }

        private void SetXRMediator(XRMediator mediator)
        {
            _XRMediator = mediator;
            _XRMediator.OnTimestampChange += UpdateTimestampUI;
            _XRMediator.OnXRFeedbackChange += SetXRFeedback;
        }

        private void UpdateTimestampUI(int newTimestamp)
        {
            _TimestampLabel.text = $"{newTimestamp}/{SetsManager.instance.DataSet.MaxTime}";
            _TimestampSlider.SetValueWithoutNotify(newTimestamp);
        }

        public void SetXRFeedback(string text)

        {
            if (_XRFeedbackText == null)
            {
                return;
            }

            if (text == "")
            {
                _XRFeedbackText.text = "Selecting";
                return;
            }

            if (text != _XRFeedbackText.text)
            {
                _XRFeedbackText.text = text;
            }
        }

        private void RegisterMenus()
        {
            _Menus.Add(_QuickToolBar);
            _Menus.Add(_PickedMenu);

            for (int i = 0; i < _MenusContainer.childCount; i++)
            {
                XRMainMenuPart part = _MenusContainer.GetChild(i).GetComponent<XRMainMenuPart>();
                if (part != null)
                    _Menus.Add(part);
            }
        }

        private void UpdateButtonTriggeringColliders()
        {
            foreach (XRMenuPart menu in _Menus)
                menu.SetButtonsColliders(_XRButtonTriggeringColliders);
        }

        private void UpdateMediator()
        {
            foreach (XRMenuPart menu in _Menus)
                menu.SetMediator(_XRMediator);
        }

        private void SetMenusContainer()
        {
            _Menus.ForEach(menu =>
            {
                if (menu is XRMainMenuPart m)
                    m.SetContainerPanel(this);
            });
        }

        private void SetPinnedMenusContainer()
        {
            _Menus.ForEach(menu =>
            {
                if (menu is XRMainMenuPart m)
                    m.SetPinContainer(_PinnedMenusContainer);
            });
        }

        public void DisplayMenu(XRMainMenuPart menu)
        {
            if (menu is null)
                return;

            _PreviouslyDisplayedMenu = _CurrentlyDisplayedMenu;

            _CurrentlyDisplayedMenu = menu;

            if (_PreviouslyDisplayedMenu != null) // is null at the start of the application.
                _PreviouslyDisplayedMenu.Hide();
            _CurrentlyDisplayedMenu.Show();
        }

        public void ShowToolBar() => _QuickToolBar.Show();

        public void HideToolBar() => _QuickToolBar.Hide();

        public void ShowPickedMenu() => _PickedMenu.Show();

        public void ReplaceMenuInDefaultParent(XRMainMenuPart menu) => menu.transform.SetParent(_MenusContainer);

        public void GoToPreviousTimeStamp() => _XRMediator.GoToPreviousTimestamp();

        public void GoToNextTimeStamp() => _XRMediator.GoToNextTimestamp();

        public void GoToTimeStamp(int timestamp) => _XRMediator.GoToTimeStamp(timestamp);
    }
}