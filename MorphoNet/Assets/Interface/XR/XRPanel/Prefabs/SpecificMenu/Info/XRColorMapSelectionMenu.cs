using MorphoNet.Extensions.Unity;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace MorphoNet.UI.XR
{
    public class XRColorMapSelectionMenu : MonoBehaviour
    {
        [SerializeField]
        private Image _ColorMapSprite;

        [SerializeField]
        private Image _AffinedColorMapSprite;

        [SerializeField]
        private Text _ColorMapName;

        [SerializeField]
        private InputField _MinThresholdLabel;

        [SerializeField]
        private InputField _MaxThresholdLabel;

        public RangeSlider ThresholdSlider;
        public RangeSlider ColorMapRangeSlider;
        public XRButton PreviousColorMabButton;
        public XRButton NextColorMabButton;

        public XRButton ValidateColorMap;
        public XRButton CancelModification;

        public void SetColorMapSprite(Sprite sprite) => _ColorMapSprite.sprite = sprite;

        public void SetAffinedColorMapSprite(Sprite sprite) => _AffinedColorMapSprite.sprite = sprite;

        public void SetText(string text) => _ColorMapName.text = text;

        public void Show() => gameObject.Activate();

        public void Hide() => gameObject.Deactivate();

        internal void SetThresholdText(float min, float max)
        {
            _MinThresholdLabel.text = min.ToString();
            _MaxThresholdLabel.text = max.ToString();
        }
    }
}