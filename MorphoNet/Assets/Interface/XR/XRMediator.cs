using System;
using System.Collections;
using System.Collections.Generic;
using MorphoNet.Interaction;
using UnityEngine;

namespace MorphoNet.UI.XR
{
    public class XRMediator : MonoBehaviour, ISubMediator
    {
        private UIMediator _Parent;

        /// <inheritdoc/>
        public void RegisterParentMediator(UIMediator mediator)
        {
            _Parent = mediator;

            _Parent.OnCellSelectionInfoLabeUpdate += TriggerOnCellSelectionInfoLabelUpdate;
        }

        public Quaternion CurrentDatasetRotation() => _Parent.FetchCurrentDatasetRotation();

        public float CurrentDatasetScale() => _Parent.FetchCurrentDatasetScale();

        /// <inheritdoc/>
        public void ChangeCurrentDataSetName(string newName)
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc/>
        public void ChangeCurrentHoursPostFertilization(string value)
        {
            throw new System.NotImplementedException();
        }

        public void ResetDatasetRotation() => _Parent.ResetDatasetRotation();

        public void ResetDatasetScaling() => _Parent.ResetDatasetScaling();

        public void ResetDatasetScattering() => _Parent.ResetDatasetScattering();

        #region XR Interaction

        public void ActivateScaling()
        {
            DeactivateAllCommandModifiers();
            _Parent.AddCommandModifier(CommandModifier.ScalingActivated);
        }

        public void ActivateScattering()
        {
            _Parent.AddCommandModifier(CommandModifier.ScatteringActivated);
        }

        public void DeactivateScattering()
        {
            _Parent.RemoveCommandModifier(CommandModifier.ScatteringActivated);
        }

        public void DeactivateAllCommandModifiers() => _Parent.DeactivateAllModifiers();

        public void ResetXRCommandModifiers() => _Parent.ResetXRCommandModifiers();

        public void ActivateCuttingPlane() => _Parent.ActivateClippingPlane();

        public void DeactivateCuttingPlane() => _Parent.DeactivateClippingPlane();

        public void ResetCuttingPlanePose(bool updateClip = false) => _Parent.ResetClippingPlanePose(updateClip);

        public void ActivateCuttingPlaneControl()
        {
            // DeactivateAllCommandModifiers();
            _Parent.AddCommandModifier(CommandModifier.CuttingPlaneControl);
        }

        public void DeactivateCuttingPlaneControl()
        {
            _Parent.RemoveCommandModifier(CommandModifier.CuttingPlaneControl);
        }

        public void AttachCuttingPlaneToHand(bool attach = true)
        {
            if (!attach)
                UnattachCuttingPlaneToHand();

            _Parent.AddCommandModifier(CommandModifier.CuttingPlaneOnMainHand);
        }

        public void UnattachCuttingPlaneToHand()
            => _Parent.RemoveCommandModifier(CommandModifier.CuttingPlaneOnMainHand);

        #endregion XR Interaction

        #region Selection Menu

        internal void DeactivateXRSelection()
        {
            _Parent.RemoveCommandModifier(CommandModifier.XRCollisionSelection);
            _Parent.RemoveCommandModifier(CommandModifier.XRRaySelection);
        }

        internal void SetSelectionToMultipleCell()
        {
            _Parent.RemoveCommandModifier(CommandModifier.SelectionSquare);
            _Parent.RemoveCommandModifier(CommandModifier.SelectionOver);

            _Parent.AddCommandModifier(CommandModifier.SelectionMultiple);
        }

        internal void ToggleMeshCutting(bool cut)
        {
            if (cut)
                _Parent.AddCommandModifier(CommandModifier.MeshCuttingActivated);
            else
                _Parent.RemoveCommandModifier(CommandModifier.MeshCuttingActivated);
        }

        internal void SetSelectionToSingleCell()
        {
            _Parent.RemoveCommandModifier(CommandModifier.SelectionOver);
            _Parent.RemoveCommandModifier(CommandModifier.SelectionMultiple);
            _Parent.RemoveCommandModifier(CommandModifier.SelectionSquare);
        }

        internal void TriggerSelectionWithCollision()
        {
            _Parent.TriggerSelectionWithCollision();
        }

        internal void TriggerSelectionWithRay()
        {
            _Parent.TriggerSelectionWithRay();
        }

        #endregion Selection Menu

        #region XR ToolBar

        internal event Action<string> OnCellSelectionInfoLabeUpdate;

        private void TriggerOnCellSelectionInfoLabelUpdate(string value)
            => OnCellSelectionInfoLabeUpdate?.Invoke(value);

        internal void InvertCellsSelection() => _Parent.InvertCellsSelection();

        internal void ClearCellsSelection() => _Parent.ClearCellsSelection();

        internal void ShowSelectedCells() => _Parent.ShowSelectedCells();

        internal void HideSelectedCells() => _Parent.HideSelectedCells();

        internal void SelectCellsSiblings() => _Parent.SelectCellsSiblings();

        internal void GiveCellsRandomColors() => _Parent.GiveCellsRandomColors();

        internal Cell GetLastSelectedCell() => _Parent.GetLastSelectedCell();

        #endregion XR ToolBar

        #region XRFeedbacks

        /// <summary>
        /// Event triggered when the feedback text should be changed.
        /// Should be used by XR UI elements.
        /// </summary>
        internal event Action<string> OnXRFeedbackChange;

        /// <summary>
        /// Trigger feedback text changes.
        /// </summary>
        public void ChangeFeedbackText(string text) => OnXRFeedbackChange?.Invoke(text);

        #endregion XRFeedbacks

        #region Timestamp changes

        /// <summary>
        /// Event triggered when the Dataset timestamp is changed.
        /// Should be used by XR UI elements.
        /// </summary>
        internal event Action<int> OnTimestampChange;

        /// <summary>
        /// Trigger the Timestamp change event. Should be trigger by the Parent mediator.
        /// </summary>
        public void ChangeTimestamp(int text) => OnTimestampChange?.Invoke(text);

        /// <summary>
        /// Ask the Parent mediator to go the given timestamp.
        /// </summary>
        internal void GoToTimeStamp(int timestamp) => _Parent.GoToTimestamp(timestamp);

        /// <summary>
        /// Ask the Parent mediator to go the previous timestamp.
        /// </summary>
        internal void GoToPreviousTimestamp() => _Parent.GoToPreviousTimestamp();

        /// <summary>
        /// Ask the Parent mediator to go the next timestamp.
        /// </summary>
        internal void GoToNextTimestamp() => _Parent.GoToNextTimestamp();

        #endregion Timestamp changes

        #region Advance Selection

        public void ApplySelectionToPickedObjects() => _Parent.ApplySelectionToPickedObjects();

        public void PreviousSelection() => _Parent.PreviousSelection();

        public void NextSelection() => _Parent.NextSelection();

        public void NextFreeSelection() => _Parent.NextFreeSelection();

        public int GetSelectionValue() => _Parent.GetSelectionValue();

        public int CountObjectsWithCurrentSelection() => _Parent.CountObjectsWithCurrentSelection();

        public int CountObjectsWithAnySelection() => _Parent.CountObjectsWithAnySelection();

        public int CountObjectsWithoutSelection() => _Parent.CountObjectsWithoutSelection();

        public int CountObjectsWithMultipleSelection() => _Parent.CountObjectsWithMultipleSelection();

        #endregion Advance Selection

        #region Info Menu

        public List<Correspondence> InformationsCorrespondences
            => MorphoTools.GetDataset().Infos.Correspondences;

        public void StartApplyingSelectionCorrespondance(Correspondence correspondence)
        {
            MorphoTools.GetDataset().Infos.DownloadAndApplyCorrespondance(correspondence);
        }

        public void DownloadCorrespondence(Correspondence correspondence)
        {
            MorphoTools.GetDataset().Infos.DownloadCorrespondence(correspondence);
        }

        #region Info-ColorMap

        public int GetNumberOfColormap()
        {
            return InterfaceManager.instance.ColormapDropdown.options.Count;
        }

        public Sprite GetColormapUsingIndex(int indexmap)
        {
            return InterfaceManager.instance.ColormapDropdown.options[indexmap].image;
        }

        public string GetColormapTextUsingIndex(int indexmap)
        {
            return InterfaceManager.instance.ColormapDropdown.options[indexmap].text;
        }

        public void SetColorMapIndex(int index)
        {
            MorphoTools.GetDataset().Infos.ClassColorBar.changeColorMap(index);
        }

        public void SetColorBarThreshold(float min, float max)
        {
            MorphoTools.GetDataset().Infos.ClassColorBar.UpdateMinV(min, true);
            MorphoTools.GetDataset().Infos.ClassColorBar.UpdateMaxV(max, true);
        }

        public (float, float) GetColorBarThresholdValues()
            =>
            (MorphoTools.GetDataset().Infos.ClassColorBar.GetMinVal(),
            MorphoTools.GetDataset().Infos.ClassColorBar.GetMaxVal());

        public (float, float) GetColorBarThresholdBounds()
            =>
            (MorphoTools.GetDataset().Infos.ClassColorBar.GetMinThresholdBounds(),
            MorphoTools.GetDataset().Infos.ClassColorBar.GetMaxThresholdBounds());

        public void AffineColorMap(float min, float max)
        {
            MorphoTools.GetDataset().Infos.ClassColorBar.UpdateColorMapRange(min, max);
        }

        public (int, int) GetAffineColorMapValues()
            =>
            (MorphoTools.GetDataset().Infos.ClassColorBar.GetMinC(),
            MorphoTools.GetDataset().Infos.ClassColorBar.GetMaxC());

        public (int, int) GetAffineColorMapBounds()
            =>
            (MorphoTools.GetDataset().Infos.ClassColorBar.GetMinCBounds(),
            MorphoTools.GetDataset().Infos.ClassColorBar.GetMaxCBounds());

        public void UpdateColorMap()
        {
            InterfaceManager.instance.GetComponent<MenuColorBar>().apply();
        }

        #endregion Info-ColorMap

        #endregion Info Menu
    }
}