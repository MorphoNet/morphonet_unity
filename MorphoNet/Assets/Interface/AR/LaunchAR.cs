using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchAR : MonoBehaviour
{
    public GameObject ButtonAR;
    public GameObject ARSessionOrigin;
    public GameObject ARSession;
    public GameObject ARCamera;
    public GameObject MainCamera;
    public GameObject MazeBg;
    public GameObject Background;
    public Canvas cL;
    public bool isActive=false;

    void Start()
    {
        isActive=false;
        #if UNITY_IOS || UNITY_ANDROID
            inactivateAR();
        #else //DESTROY AR 
            ButtonAR.SetActive(false);
            Destroy(ARSessionOrigin);
            Destroy(ARSession);
            Destroy(ButtonAR);
        #endif
    }

    
    public void changeActivation(){
        if(isActive)inactivateAR();
        else activateAR();
        isActive=!isActive;
    }
    public void activateAR(){
        if(!ARSessionOrigin.activeSelf) ARSessionOrigin.SetActive(true);
        if(!ARSession.activeSelf) ARSession.SetActive(true);
        ARCamera.SetActive(true);
        MainCamera.SetActive(false);
        MazeBg.SetActive(false);
        Background.SetActive(false);
        cL.GetComponent<Canvas>().worldCamera=ARCamera.transform.GetComponent<Camera>();
        
    }
    
    public void inactivateAR(){
        ARSessionOrigin.SetActive(false);
        ARSession.SetActive(false);
        MainCamera.SetActive(true);
        ARCamera.SetActive(false);
        MazeBg.SetActive(true);
        Background.SetActive(true);
        cL.GetComponent<Canvas>().worldCamera=MainCamera.transform.GetComponent<Camera>();
    }
}
