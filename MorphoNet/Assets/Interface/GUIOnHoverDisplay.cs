using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GUIOnHoverDisplay : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private GameObject _Comment;

    public void Start()
    {
        Transform tooltip = gameObject.transform.Find("comment");
        if (tooltip != null)
            _Comment = tooltip.gameObject;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (_Comment != null)
        {
            _Comment.SetActive(true);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (_Comment != null)
        {
            _Comment.SetActive(false);
        }
    }
}
