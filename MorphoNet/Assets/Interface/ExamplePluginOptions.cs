using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MorphoNet;

public class ExamplePluginOptions : MonoBehaviour
{
    private Button _ParentButton;

    [SerializeField]
    private GameObject _ExampleInput;
    [SerializeField]
    private GameObject _PlayBtnObject;
    [SerializeField]
    private Text _Name;
    [SerializeField]
    private Button _PlayButton;

    [SerializeField]
    private GameObject _HelpBtn;

    [SerializeField]
    private Image _PluginImage;

    public Button PlayButton { get => _PlayButton; }

    private ExampleHelpWindow _HelpWindow;

    private bool _keephelpopen = false;


    // Start is called before the first frame update
    void Start()
    {
        _ExampleInput.SetActive(false);
    }

    public void SetName(string name)
    {
        _Name.text = name;
    }

    public GameObject AddInput(string name, string type)
    {
        GameObject go = Instantiate(_ExampleInput, this.transform);
        go.name = name;
        go.transform.Find("Text").GetComponent<Text>().text = name.Replace("IF_","").Replace("DD_", "").Replace("CD_", "").Replace("CB_", "").Replace("FP_", "").Replace("FS_", "");
        GameObject rGO = null;
        switch (type)
        {
            case "IF":
                rGO = go.transform.Find("IF").gameObject;
                rGO.gameObject.SetActive(true);
                break;
            case "DD":
                rGO = go.transform.Find("DD").gameObject;
                rGO.gameObject.SetActive(true);
                break;
            case "CD":
                rGO = go.transform.Find("CD").gameObject;
                rGO.gameObject.SetActive(true);
                break;
            case "MI":
                rGO = go.transform.Find("MI").gameObject;
                rGO.gameObject.SetActive(true);
                break;
            case "TI":
                rGO = go.transform.Find("TI").gameObject;
                rGO.gameObject.SetActive(true);
                break;
            case "CB":
                rGO = go.transform.Find("CB").gameObject;
                rGO.gameObject.SetActive(true);
                break;
            case "FP":
                rGO = go.transform.Find("FP").gameObject;
                rGO.gameObject.SetActive(true);
                break;
            case "FS":
                rGO = go.transform.Find("FS").gameObject;
                rGO.gameObject.SetActive(true);
                break;
            default:
                go.transform.Find("Text").GetComponent<Text>().text = name+"(unsupported)";
                MorphoDebug.LogError("could not parse input named :" + name + " of type " + type);
                break;
        }
        rGO.name = name;
        _PlayBtnObject.transform.SetAsLastSibling();
        return rGO;

    }


    public void ToggleDisplay()
    {
        gameObject.SetActive(!gameObject.activeSelf);
        if (!gameObject.activeSelf && _HelpWindow!=null)
            _HelpWindow.gameObject.SetActive(false);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void SetHelpWindowVisibility(bool value)
    {
        if (!_keephelpopen)
        {
            if (_HelpWindow != null)
                _HelpWindow.gameObject.SetActive(value);
        }
        
    }

    public void ToggleHelpWindowVisibility()
    {
        if (_HelpWindow != null)
        {
            _HelpWindow.gameObject.SetActive(!_HelpWindow.gameObject.activeSelf);
            if (_HelpWindow.gameObject.activeSelf)
            {
                _HelpWindow.transform.SetAsFirstSibling();
                _keephelpopen = true;
            }
            else
            {
                _keephelpopen = false;
            }
                
        }
            
    }

    public ExampleHelpWindow GetHelpWindow()
    {
        return _HelpWindow;
    }

    public void SetHelpWindow(ExampleHelpWindow win)
    {
        _HelpWindow = win;
        if (win != null)
        {
            _HelpBtn.SetActive(true);
        }
    }

    public void SetParentButton(Button b)
    {
        _ParentButton = b;
    }

    public Button GetParentButton()
    {
        return _ParentButton;
    }

    public void SetImages(Texture2D tex)
    {
        _PluginImage.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
        if (_HelpWindow != null)
        {
            _HelpWindow.SetIcon(Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f)));
        }
    }

}