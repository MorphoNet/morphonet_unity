using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExampleHelpWindow : MonoBehaviour
{
    [SerializeField]
    private Text _PluginName;

    [SerializeField]
    private Text _PluginDesc;

    [SerializeField]
    private Image _PluginIcon;

    [SerializeField]
    private RawImage _PluginImage;

    [SerializeField]
    private Button _DocumentationHelp;

    [SerializeField] public HelpLink HelpLink;

    public Button GetDocumentationButton()
    {
        return _DocumentationHelp;
    }

    public void SetName(string name)
    {
        _PluginName.text = name;
    }

    public void SetDescription(string desc)
    {
        _PluginDesc.text = desc;
    }

    public void SetLinkURL(string url)
    {
        if (HelpLink != null)
        {
            HelpLink.url = url;
        }
    }
    public void SetImage(Texture img)
    {
        _PluginImage.texture = img;
    }

    public void SetIcon(Sprite img)
    {
        _PluginIcon.sprite = img;
    }

    public RectTransform GetImage()
    {
        return _PluginImage.GetComponent<RectTransform>();
    }

}
