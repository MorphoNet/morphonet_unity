using UnityEngine;
using UnityEngine.UI;

namespace MorphoNet.UI.Desktop
{
    /// <summary>
    /// The Mediator handling Desktop UI. It get commands from the <see cref="UIMediator"/> through the <see cref="ISubMediator"/> interface
    /// and dispatch command itself to UI sub-parts.
    /// </summary>
    public class DesktopMediator : MonoBehaviour, ISubMediator
    {
        #region Editor references

        /// <summary>
        /// The Menu handling operation on the dataset.
        /// </summary>
        [SerializeField]
        private DataSetMenu _DataSetMenu;

        /// <summary>
        /// The current displayed Dataset name to display
        /// </summary>
        [SerializeField]
        private Text _CurrentDataSetName;

        /// <summary>
        /// The current Hours Post Fertilisation (HPF) to display.
        /// </summary>
        [SerializeField]
        private Text _CurrentHoursPostFertilisation;

        #endregion Editor references

        private UIMediator _Parent;

        /// <inheritdoc/>
        public void RegisterParentMediator(UIMediator mediator)
        {
            _Parent = mediator;
        }

        /// <inheritdoc/>
        public void ChangeCurrentDataSetName(string newName)
        {
            _CurrentDataSetName.text = newName;
        }

        /// <inheritdoc/>
        public void ChangeCurrentHoursPostFertilization(string value)
        {
            _CurrentHoursPostFertilisation.text = value;
        }

        // TODO: link this
        public void ChangeTimestamp(int value)
        { }

        internal void SetMeshCutting(bool on)
        {
            if (on)
                _Parent.AddCommandModifier(Interaction.CommandModifier.MeshCuttingActivated);
            else
                _Parent.RemoveCommandModifier(Interaction.CommandModifier.MeshCuttingActivated);
        }
    }
}