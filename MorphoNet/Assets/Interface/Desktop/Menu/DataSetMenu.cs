using MorphoNet.Extensions.Unity;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace MorphoNet.UI.Desktop
{
    public class DataSetMenu : MonoBehaviour
    {
        #region UI Mediator

        [SerializeField]
        private DesktopMediator _UIMediator;

        #endregion UI Mediator

        [SerializeField]
        private GameObject _Menu;

        [Header("Folding")]
        [SerializeField]
        private Button _FoldButton;

        [Header("Rotation")]
        [SerializeField]
        private GameObject _SaveRotationButton;

        [SerializeField]
        private GameObject _SaveAllRotationButton;

        [SerializeField]
        private GameObject _DeleteRotationButton;

        [Header("Download")]
        [SerializeField]
        private GameObject _DownloadButton;

        [SerializeField]
        private GameObject _DownloadLineageButton;

        [SerializeField]
        private GameObject _DownloadMesh;

        [Header("Crop")]
        [SerializeField]
        private GameObject _CropGameObject;

        [SerializeField]
        private Slider _XMinSlider;

        [SerializeField]
        private Slider _XMaxSlider;

        [SerializeField]
        private Slider _YMinSlider;

        [SerializeField]
        private Slider _YMaxSlider;

        [SerializeField]
        private Slider _ZMinSlider;

        [SerializeField]
        private Slider _ZMaxSlider;

        [SerializeField]
        private Slider _FixedPlanSlider;

        [SerializeField]
        private Toggle _FixedPlanToggle;

        [Header("Layout")]
        [SerializeField]
        private VerticalLayoutGroup _SubMenuLayout;

        public GameObject Menu { get => _Menu; }

        public Slider XMinSlider { get => _XMinSlider; }
        public Slider XMaxSlider { get => _XMaxSlider; }
        public Slider YMinSlider { get => _YMinSlider; }
        public Slider YMaxSlider { get => _YMaxSlider; }
        public Slider ZMinSlider { get => _ZMinSlider; }
        public Slider ZMaxSlider { get => _ZMaxSlider; }
        public Slider FixedPlanSlider { get => _FixedPlanSlider; }
        public Toggle FixedPlanToggle { get => _FixedPlanToggle; }

        public VerticalLayoutGroup SubMenuLayout { get => _SubMenuLayout; }

        public void InitButtons(bool activateRotationControl)
        {
            _SaveRotationButton.SetActive(activateRotationControl);
            _SaveAllRotationButton.SetActive(activateRotationControl);
            _DeleteRotationButton.SetActive(activateRotationControl);

            _DownloadButton.Deactivate();
            _DownloadLineageButton.Deactivate();
        }

        public void UnFold()
        {
            _Menu.Activate();
        }

        public void Fold()
        {
            _Menu.Deactivate();
        }

        public void ShowDownloadMesh()
            => _DownloadMesh.Activate();

        public void HideDownloadMesh()
            => _DownloadMesh.Deactivate();


    }
}