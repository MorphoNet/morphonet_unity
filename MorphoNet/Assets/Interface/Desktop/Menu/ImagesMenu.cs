using MorphoNet;
using MorphoNet.UI;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace MorphoNet.UI.Desktop
{
    public class ImagesMenu : MonoBehaviour, IRawDataUI
    {
        private bool _RawActive = false;

        #region UI references

        [SerializeField]
        private Text _PropertyDimensionsText;

        [SerializeField]
        private Text _PropertyVoxelSizeText;

        [SerializeField]
        private Text _PropertyDownsamplingText;

        [SerializeField]
        private UILineRenderer _HistogramLineRenderer;

        [SerializeField]
        private UILineRenderer _HistogramOpacityRenderer;

        [SerializeField]
        private Text _MinValueText;

        [SerializeField]
        private Text _MaxValueText;

        [SerializeField]
        private RectTransform _ChannelsLayout;

        [SerializeField]
        private GameObject _ChannelEntryPrefab;

        private Toggle[] _ChannelActiveToggles;

        private Button[] _ChannelActiveButtons;

        [SerializeField]
        private InputField _MinThresholdField;

        [SerializeField]
        private InputField _MaxThresholdField;

        [SerializeField]
        private InputField _MinHistoField;

        [SerializeField]
        private InputField _MaxHistoField;

        /*[SerializeField]
        private InputField _MinTransparencyField;

        [SerializeField]
        private InputField _MaxTransparencyField;*/

        [SerializeField]
        private InputField _MinCmapField;

        [SerializeField]
        private InputField _MaxCmapField;

        [SerializeField]
        private Slider _TransparencySlider;

        [SerializeField]
        private Dropdown _ColorBarSelect;

        [SerializeField]
        private InputField _IntensityField;

        [SerializeField]
        private Slider _IntensitySlider;


        [SerializeField]
        private Toggle _CuttingPlaneActive;

        [SerializeField]
        private Toggle _CuttingPlaneInverse;

        [SerializeField]
        private Toggle _CuttingMeshesActive;

        [SerializeField]
        private Toggle _CuttingX;

        [SerializeField]
        private Toggle _CuttingY;

        [SerializeField]
        private Toggle _CuttingZ;

        [SerializeField]
        private Slider _PlanePositionSlider;

        [SerializeField]
        private Toggle _SliceMode;

        [SerializeField]
        private Slider _ThicknessSlider;

        [SerializeField]
        private RangeSlider _AlphaXSlider;

        [SerializeField]
        private RangeSlider _AlphaYSlider;

        [SerializeField]
        private RangeSlider _ColorbarModSlider;

        [SerializeField]
        private Image _ColorBarPreview;

        [SerializeField]
        private RangeSlider _HistoMinMaxScaling;

        private int _CurrentChannel = 0;

        #endregion

        private void SetHistogram(long[] histo, float max)
        {
            if (histo.Length > 256)
            {
                MorphoDebug.LogError("ERROR: histogram has more than 256 values.");
                return;
            }
            _HistogramLineRenderer.Points.Clear();
            //pow(0.2) to be able to see the values for low numbers better
            float index = 0f;
            float step = 255f / (float)histo.Length;
            for (int i=0;i<histo.Length;i++)
            {
                _HistogramLineRenderer.Points.Add(new Vector2(index,(float)Mathf.Pow((float)((double)histo[i] / (double)max),0.2f)));
                index += step;
            }
            _HistogramLineRenderer.ForceRedraw();
            
        }

        private void SyncThreshFieldsWithSlider()
        {
            RawImages raw = MorphoTools.GetRawImages();
            _MinThresholdField.SetTextWithoutNotify((GetAlphaMinValue().x * (GetMaxValue() - GetMinValue()) + GetMinValue()).ToString());
            _MaxThresholdField.SetTextWithoutNotify((GetAlphaMaxValue().x * (GetMaxValue() - GetMinValue()) + GetMinValue()).ToString());
        }

        private void SyncThreshSliderWithFields()
        {
            _AlphaXSlider.SetValueWithoutNotify(GetThresholdFieldValue() / (GetMaxValue() - GetMinValue()) + GetMinValue(), GetMaxThresholdFieldValue()/ (GetMaxValue() - GetMinValue()) + GetMinValue());
        }

        private void SyncHistoFieldsWithSlider()
        {
            _MinHistoField.SetTextWithoutNotify(GetHistoMinMaxSliderValue().Item1.ToString());
            _MaxHistoField.SetTextWithoutNotify(GetHistoMinMaxSliderValue().Item2.ToString());
        }

        private void SyncHistoSliderWithFields()
        {
            _HistoMinMaxScaling.SetValueWithoutNotify(GetMinHistoFieldValue(), GetMaxHistoFieldValue());
        }

        private void SyncCmapFieldsWithSlider()
        {
            _MinCmapField.SetTextWithoutNotify(GetColorBarModSliderValue().Item1.ToString());
            _MaxCmapField.SetTextWithoutNotify(GetColorBarModSliderValue().Item2.ToString());
        }

        private void SyncCmapSliderWithFields()
        {
            _ColorbarModSlider.SetValueWithoutNotify(GetMinCmapFieldValue(), GetMaxCmapFieldValue());
        }

        private void ModifyHistogram()
        {
            RawImages raw = MorphoTools.GetRawImages();
            long[] histo_base = raw.GetHistogram(_CurrentChannel);
            int min = (int)(255f * GetHistoMinMaxSliderValue().Item1);
            int max = (int)(255f * GetHistoMinMaxSliderValue().Item2);
            if(min == max)
            {
                _HistogramLineRenderer.Points.Clear();
                _HistogramLineRenderer.ForceRedraw();
                return;
            }
            long[] histo = new long[max - min];
            for (int i = 0; i< histo.Length; i++)
                histo[i] = histo_base[i + min];

            long maximum = histo_base[0];
            foreach (long l in histo_base)
                if (l > maximum)
                    maximum = l;

            SetHistogram(histo, maximum);
        }

        private void UpdateAutoTransparencyCurve()
        {
            Vector2 min = GetAlphaMinValue();
            Vector2 max = GetAlphaMaxValue();
            SetTransparencyCurve(min, max);
        }

        private void SetTransparencyCurve(Vector2 min, Vector3 max)
        {
            if (_HistogramOpacityRenderer.Points.Count < 5)
            {
                Debug.LogError("ERROR: trying to set transparency curve with less than 5 points");
                return;
            }
            min.x *= 255;
            min.y = Mathf.Pow(min.y,0.2f);
            max.x *= 255;
            max.y = Mathf.Pow(max.y, 0.2f);
            _HistogramOpacityRenderer.Points[1] = new Vector2(min.x,0);
            _HistogramOpacityRenderer.Points[2] = min;
            _HistogramOpacityRenderer.Points[3] = max;
            _HistogramOpacityRenderer.Points[4] = new Vector2(255,max.y);


            _HistogramOpacityRenderer.ForceRedraw();
        }


        public Sprite ColorBarSprite(int value)
        {
            SetColorBarPreview(value);
            return _ColorBarPreview.sprite;
        }

        public static Sprite ExtractSprite(Sprite sprite, float minValue, float maxValue)
        {
            return Sprite.Create(sprite.texture, new Rect(minValue, 0, Mathf.Max(1,maxValue - minValue), 30), new Vector2(0.5f, 0.5f));
        }

        public void SetColorBarPreview(int value)
        {
            _ColorBarPreview.sprite = ExtractSprite(_ColorBarSelect.options[value].image,_ColorbarModSlider.LowValue, _ColorbarModSlider.HighValue);
        }

        public bool RawActive() => _RawActive;


        private IMenuController<IRawDataUI> _MenuController;

        public void SetController(IMenuController<IRawDataUI> controller)
            => _MenuController = controller;

        private void Start()
        {
            SetUIListeners();
            SetColorBarPreview(_ColorBarSelect.value);
        }

        private void SetUIListeners()
        {
            _MinThresholdField.onValueChanged.AddListener(_ => SyncThreshSliderWithFields());
            _MinThresholdField.onValueChanged.AddListener(_ => RawDataUpdate());

            _MaxThresholdField.onValueChanged.AddListener(_ => SyncThreshSliderWithFields());
            _MaxThresholdField.onValueChanged.AddListener(_ => RawDataUpdate());
            

            _MinHistoField.onValueChanged.AddListener(_ => SyncHistoSliderWithFields());
            _MinHistoField.onValueChanged.AddListener(_ => RawDataUpdate());

            _MaxHistoField.onValueChanged.AddListener(_ => SyncHistoSliderWithFields());
            _MaxHistoField.onValueChanged.AddListener(_ => RawDataUpdate());

            _MinCmapField.onValueChanged.AddListener(_ => SyncCmapSliderWithFields());
            _MinCmapField.onValueChanged.AddListener(delegate { SetColorBarPreview(_ColorBarSelect.value); });
            _MinCmapField.onValueChanged.AddListener(delegate { RawDataUpdate(); });

            _MaxCmapField.onValueChanged.AddListener(_ => SyncCmapSliderWithFields());
            _MaxCmapField.onValueChanged.AddListener(delegate { SetColorBarPreview(_ColorBarSelect.value); });
            _MaxCmapField.onValueChanged.AddListener(delegate { RawDataUpdate(); });


            _TransparencySlider.onValueChanged.AddListener(_ => RawDataUpdate());
            _IntensityField.onValueChanged.AddListener(_ => TrySetIntensitySliderValue());
            _IntensityField.onValueChanged.AddListener(_ => RawDataUpdate());
            _IntensitySlider.onValueChanged.AddListener(_ => _IntensityField.SetTextWithoutNotify(_IntensitySlider.value.ToString()));
            _IntensitySlider.onValueChanged.AddListener(_ => RawDataUpdate());
            _ColorBarSelect.onValueChanged.AddListener(_ => SetColorBarPreview(_ColorBarSelect.value));
            _ColorBarSelect.onValueChanged.AddListener(_ => RawDataUpdate());

            _AlphaXSlider.OnValueChanged.AddListener(delegate { SyncThreshFieldsWithSlider(); });
            _AlphaXSlider.OnValueChanged.AddListener(delegate { RawDataUpdate(); });
            _AlphaXSlider.OnValueChanged.AddListener(delegate { UpdateAutoTransparencyCurve(); });
            
            _AlphaYSlider.OnValueChanged.AddListener(delegate { RawDataUpdate(); });
            _AlphaYSlider.OnValueChanged.AddListener(delegate { UpdateAutoTransparencyCurve(); });
            

            _ColorbarModSlider.OnValueChanged.AddListener(delegate { SyncCmapFieldsWithSlider(); });
            _ColorbarModSlider.OnValueChanged.AddListener(delegate { SetColorBarPreview(_ColorBarSelect.value); });
            _ColorbarModSlider.OnValueChanged.AddListener(delegate { RawDataUpdate(); });

            _HistoMinMaxScaling.OnValueChanged.AddListener(delegate { SyncHistoFieldsWithSlider(); });
            _HistoMinMaxScaling.OnValueChanged.AddListener(delegate { ModifyHistogram(); });
            _HistoMinMaxScaling.OnValueChanged.AddListener(delegate { RawDataUpdate(); });


            _CuttingPlaneActive.onValueChanged.AddListener(_ => RawDataUpdate());
            _CuttingPlaneInverse.onValueChanged.AddListener(_ => RawDataUpdate());
            _CuttingMeshesActive.onValueChanged.AddListener(_ => RawDataUpdate());
            _CuttingX.onValueChanged.AddListener(_ => RawDataUpdate());
            _CuttingY.onValueChanged.AddListener(_ => RawDataUpdate());
            _CuttingZ.onValueChanged.AddListener(_ => RawDataUpdate());
            _PlanePositionSlider.onValueChanged.AddListener(_ => RawDataUpdate());
            _SliceMode.onValueChanged.AddListener(_ => RawDataUpdate());
            _ThicknessSlider.onValueChanged.AddListener(_ => RawDataUpdate());

        }

        private void TrySetIntensitySliderValue()
        {
            float ret;
            if (float.TryParse(_IntensityField.text.Replace(',','.'), NumberStyles.Any, CultureInfo.InvariantCulture, out ret))
                _IntensitySlider.SetValueWithoutNotify(ret);
        }


        public void SetChannels(int channels)
        {

            //destroy previous if exists
            if (_ChannelActiveToggles != null && _ChannelActiveToggles.Length > 0)
            {
                foreach (Toggle t in _ChannelActiveToggles)
                {
                    Destroy(t.transform.parent);
                }
            }
            //+1 and <= because 0 is included
            _ChannelActiveToggles = new Toggle[channels + 1];
            _ChannelActiveButtons = new Button[channels + 1];

            for (int i = 0; i <= channels; i++)
            {
                GameObject go = Instantiate(_ChannelEntryPrefab, _ChannelsLayout);
                Text label = go.GetComponentsInChildren<Text>()[0];
                label.text = "Channel " + i;
                Toggle t = go.GetComponentsInChildren<Toggle>()[0];
                Button b = go.GetComponentsInChildren<Button>()[0];
                int channelvalue = i;
                //actually need a toggle and a button... to select one in particular as the active
                t.onValueChanged.AddListener(_ => MorphoTools.GetDataset().RawImages.UpdateChannelActive(t,channelvalue));
                b.onClick.AddListener(() => MorphoTools.GetDataset().RawImages.SetCurrentChannel(channelvalue));
                b.onClick.AddListener(() => SetChannelButtonActive(channelvalue));
                _ChannelActiveToggles[i] = t;
                _ChannelActiveButtons[i] = b;
            }

            SetChannelButtonActive(0,false);

        }

        private void SetChannelButtonActive(int i, bool histogram=true)
        {
            _CurrentChannel = i;
            foreach (Button b in _ChannelActiveButtons)
            {
                Image im = b.GetComponent<Image>();
                Color c = im.color;
                c.a = 0f;
                im.color = c;
            }
            Image img = _ChannelActiveButtons[i].GetComponent<Image>();
            Color clr = img.color;
            clr.a = 1f;
            img.color = clr;
            RawImages raw = MorphoTools.GetRawImages();
            raw.SetCurrentChannel(i);
            if(histogram && raw.GetHistogramMax(i) > 0)
            {
                SetCurrentHistogram(i);
                //SetHistogram(raw.GetHistogram(i), raw.GetHistogramMax(i));
                //SetTransparencyCurve(raw.GetHistoMinAlphaPoint(i), raw.GetHistoMaxAlphaPoint(i));
                //ModifyHistogram();

            }
            SetColorBarPreview(_ColorBarSelect.value);
            
        }

        /// <summary>
        /// used to set visible histogram on image load
        /// </summary>
        /// <param name="c">histogram channel channel</param>
        public void SetCurrentHistogram(int c)
        {
            RawImages raw = MorphoTools.GetRawImages();
            SetHistogram(raw.GetHistogram(c), raw.GetHistogramMax(c));
            ModifyHistogram();
            SetTransparencyCurve(raw.GetHistoMinAlphaPoint(c), raw.GetHistoMaxAlphaPoint(c));
        }

        public void AutoSetCurrentHistogram()
        {
            SetCurrentHistogram(_CurrentChannel);
        }

        /// <summary>
        /// set properties of image in ui
        /// </summary>
        /// <param name="x">width</param>
        /// <param name="y">height</param>
        /// <param name="z">depth</param>
        /// <param name="vx">voxel size width</param>
        /// <param name="vy">voxel size height</param>
        /// <param name="vz">voxel size depth</param>
        /// <param name="downsampling">downsampling applied</param>
        /// <param name="z_sampling">downsamplied applied to depth</param>
        public void SetImagePropertyLabels(int x, int y, int z, float vx, float vy, float vz, int downsampling, int z_sampling)
        {
            _PropertyDimensionsText.text = $"{x},{y},{z}";
            _PropertyVoxelSizeText.text = $"[{vx},{vy},{vz}]";
            _PropertyDownsamplingText.text = $"[{downsampling}, {downsampling}, {z_sampling}]";
        }

        /// <summary>
        /// set properties of image in ui from database data strings
        /// </summary>
        /// <param name="dims">string with the dimensions</param>
        /// <param name="voxel">string with the voxel size</param>
        /// <param name="downsampling">string with the downsampling</param>
        public void SetImagePropertyLabels(string dims,string voxel,string downsampling)
        {
            _PropertyDimensionsText.text = $"{dims}";
            _PropertyVoxelSizeText.text = $"[{voxel}]";
            _PropertyDownsamplingText.text = $"[{downsampling}, {downsampling}, {downsampling}]";
        }

        public float GetPlaneSliderValue() => _PlanePositionSlider.value;

        public bool GetCutMeshButtonValue() => _CuttingMeshesActive.isOn;

        

        public float GetIntensityFieldValue()
        {
            float ret;
            if (float.TryParse(_IntensityField.text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out ret))
                return ret;
            return 1.0f;
        }

        public float GetMaxThresholdFieldValue()
        {
            float ret;
            if (float.TryParse(_MaxThresholdField.text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out ret))
                return ret;
            return 1.0f;
        }

        public float GetMinHistoFieldValue()
        {
            float ret;
            if (float.TryParse(_MinHistoField.text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out ret))
                return ret;
            return 1.0f;
        }

        public float GetMaxHistoFieldValue()
        {
            float ret;
            if (float.TryParse(_MaxHistoField.text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out ret))
                return ret;
            return 1.0f;
        }

        public float GetThicknessSliderValue() => _ThicknessSlider.value;

        public float GetThresholdFieldValue()
        {
            float ret;
            if (float.TryParse(_MinThresholdField.text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out ret))
                return ret;
            return 0.0f;
        }


        public void UpdateUIWithoutNotify(IRawDataUI source)
        {
            //_TransparencyField.SetTextWithoutNotify(source.GetTransparencyFieldValue().ToString());
            _TransparencySlider.SetValueWithoutNotify(source.GetTransparencySliderValue());

            _MinThresholdField.SetTextWithoutNotify((source.GetAlphaMinValue().x * (GetMaxValue() - GetMinValue()) + GetMinValue()).ToString());
            _MaxThresholdField.SetTextWithoutNotify((source.GetAlphaMaxValue().x * (GetMaxValue() - GetMinValue()) + GetMinValue()).ToString());

            _MinHistoField.SetTextWithoutNotify(source.GetHistoMinMaxSliderValue().Item1.ToString());
            _MaxHistoField.SetTextWithoutNotify(source.GetHistoMinMaxSliderValue().Item2.ToString());

            _IntensityField.SetTextWithoutNotify(source.GetIntensityFieldValue().ToString());
            _IntensitySlider.SetValueWithoutNotify(source.GetIntensitySliderValue());
            _AlphaXSlider.SetValueWithoutNotify(source.GetAlphaMinValue().x, source.GetAlphaMaxValue().x);
            _AlphaYSlider.SetValueWithoutNotify(Mathf.Pow(source.GetAlphaMinValue().y,0.2f), Mathf.Pow(source.GetAlphaMaxValue().y,0.2f));
            _ColorbarModSlider.SetValueWithoutNotify(source.GetColorBarModSliderValue().Item1, source.GetColorBarModSliderValue().Item2);
            _HistoMinMaxScaling.SetValueWithoutNotify(source.GetHistoMinMaxSliderValue().Item1, source.GetHistoMinMaxSliderValue().Item2);

            _ColorBarSelect.SetValueWithoutNotify(source.GetColorBarDropDownValue());

            _MinCmapField.SetTextWithoutNotify(source.GetColorBarModSliderValue().Item1.ToString());
            _MaxCmapField.SetTextWithoutNotify(source.GetColorBarModSliderValue().Item2.ToString());

            _MinValueText.text = source.GetMinValue().ToString();
            _MaxValueText.text = source.GetMaxValue().ToString();
        }

        public void RawDataUpdate()
        {
            _MenuController.UpdateUIFrom(this);
        }

        public float GetTransparencySliderValue()
        {
            return _TransparencySlider.value;
        }

        public float GetIntensitySliderValue()
        {
            return _IntensitySlider.value;
        }

        public int GetColorBarDropDownValue()
        {
            return _ColorBarSelect.value;
        }

        public bool GetCuttingPlaneButtonValue() => _CuttingPlaneActive.isOn;

        public int GetPlaneAxisValue()
        {
            if (_CuttingX.isOn)
                return 0;
            if (_CuttingY.isOn)
                return 1;
            if (_CuttingZ.isOn)
                return 2;
            return 0;
        }

        public bool GetCuttingPlaneInverseValue() => _CuttingPlaneInverse.isOn;

        public bool GetThinModeValue() => _SliceMode.isOn;

        public Vector2 GetAlphaMinValue()
        {
            return new Vector2(_AlphaXSlider.LowValue, Mathf.Pow(_AlphaYSlider.LowValue,5));
        }

        public Vector2 GetAlphaMaxValue()
        {
            return new Vector2(_AlphaXSlider.HighValue, Mathf.Pow(_AlphaYSlider.HighValue, 5));
        }

        public (float, float) GetColorBarModSliderValue()
        {
            return (_ColorbarModSlider.LowValue, _ColorbarModSlider.HighValue);
        }

        public (float, float) GetHistoMinMaxSliderValue()
        {
            return (_HistoMinMaxScaling.LowValue, _HistoMinMaxScaling.HighValue);
        }

        public int GetMinValue()
        {
            return int.Parse(_MinValueText.text);
        }

        public int GetMaxValue()
        {
            return int.Parse(_MaxValueText.text);
        }

        public float GetMinCmapFieldValue()
        {
            float ret;
            if (float.TryParse(_MinCmapField.text.Replace(",", "."), NumberStyles.Any, CultureInfo.InvariantCulture, out ret))
                return ret;
            return 0.0f;
        }

        public float GetMaxCmapFieldValue()
        {
            float ret;
            if (float.TryParse(_MaxCmapField.text.Replace(",", "."), NumberStyles.Any, CultureInfo.InvariantCulture, out ret))
                return ret;
            return 0.0f;
        }
    }
}