﻿using UnityEngine;

namespace MorphoNet
{
    public class BackgroundColorPicker : MonoBehaviour
    {
        public GameObject Background;
        public GameObject ColorPickedPrefab;
        private ColorPickerTriangle CP;
        private bool isPaint = false;
        public GameObject go;
        private Material mat;
        private Color last_color;
        public string type;
        [SerializeField]
        private Material _Target;

        private void Awake()
        {
            mat = gameObject.GetComponent<MeshRenderer>().material;
            if(_Target!=null)
                _Target.SetColor("_Tint", mat.color);
        }

        private void Update()
        {
            if (isPaint)
            {
                if (type == "normal")
                {
                    mat.color = CP.TheColor;
                    if (_Target != null)
                        _Target.SetColor("_Tint", CP.TheColor);

                }
                else if (type == "lineage")
                {
                    if (Lineage.isLineage)
                    {
                        InterfaceManager.instance.lineage_color_holder.color = CP.TheColor;
                        mat.color = CP.TheColor;
                        Lineage.sendBackgroundColor(CP.TheColor);
                    }
                    //implement lineage standalone here
                }
                else if (type == "infotext")
                {
                    if (last_color != CP.TheColor)
                    {
                        mat.color = CP.TheColor;
                        InterfaceManager.instance.text_color_holder.color = CP.TheColor;
                        MorphoTools.GetFigureManager().TextColor = CP.TheColor;
                        MorphoTools.GetFigureManager().UpdateInfoCellsText();
                        last_color = CP.TheColor;
                    }
                    //implement lineage standalone here
                }
                //MorphoDebug.Log("Update zco");
            }
        }

        public void DestroyColorPicker()
        {
            if (go != null)
            {
                go.SetActive(false);
                isPaint = false;
            }
        }

        private void OnMouseDown()
        {
            if (isPaint)
            {
                StopPaint();
            }
            else
            {
                StartPaint();
            }
        }

        private void StartPaint()
        {
            go.SetActive(true);

            CP = go.GetComponent<ColorPickerTriangle>();
            if (type == "normal")
            {
                CP.SetNewColor(mat.color);
                //mat.SetColor("_Tint", c);
            }
                
            else if (type == "lineage")
            {
                CP.SetNewColor(InterfaceManager.instance.lineage_color_holder.color);
                //implement lineage standalone here
            }
            else if (type == "infotext")
            {
                CP.SetNewColor(InterfaceManager.instance.text_color_holder.color);
                //implement lineage standalone here
            }

            isPaint = true;
        }

        private void StopPaint()
        {
            go.SetActive(false);
            isPaint = false;
        }
    }
}