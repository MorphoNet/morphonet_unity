using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using MorphoNet.Extensions.Unity;

namespace MorphoNet
{
    public class ChangeColorGlobal : MonoBehaviour
    {
        public int R = 0;
        public Slider SliderR;
        public InputField InputFieldR;

        public int G = 0;
        public Slider SliderG;
        public InputField InputFieldG;

        public int B = 0;
        public Slider SliderB;
        public InputField InputFieldB;

        public int A = 255;
        public Slider SliderA;
        public InputField InputFieldA;

        public GameObject NewColor;

        public Button ValidNewColor;
        public Button CancelNewColor;

        public GameObject MenuColor;
        public GameObject Selection;

        public Dropdown ShadersSelection;

        public int currentChoosen = 0;

        public List<GameObject> sliders;

        public GameObject Color_sliders;

        public GameObject multi_color_parameters;

        public void SendParameterToCurrent(string parameter, float value)
        {
            if (currentChoosen == 9)
            {
                SetsManager.instance.Selected.UpdateFloatProperty(parameter, value);
            }
            else if (currentChoosen == 10)
            {
                for (int i = 0; i < SetsManager.instance.StainSelection.Count; i++)
                {
                    SetsManager.instance.StainSelection[i].UpdateFloatProperty(parameter, value);
                }

                for (int i = 0; i < SetsManager.instance.BarSelection.Count; i++)
                {
                    SetsManager.instance.BarSelection[i].UpdateFloatProperty(parameter, value);
                }
            }
            NewColor.GetComponent<Renderer>().material.UpdateFloatProperty(parameter, value);
        }

        public void SendColorToCurrent(string colorName, Color value)
        {
            NewColor.GetComponent<Renderer>().material.UpdateColorProperty(colorName, value);
        }

        public void changeShaderValue(GameObject slider)
        {
            SendParameterToCurrent(slider.name, slider.GetComponent<Slider>().value);
        }

        public void changeShaderValue(string param, float value)
        {
            SendParameterToCurrent(param, value);
        }

        public void changeShaderColor(string param, Color value)
        {
            SendColorToCurrent(param, value);
        }

        public void SendGlobalParameterToCurrent(string parameter, float value, bool selectedOrMulti)
        {
            if (selectedOrMulti)
            {
                SetsManager.instance.Selected.UpdateFloatProperty(parameter, value);
            }
            else
            {
                for (int i = 0; i < SetsManager.instance.StainSelection.Count; i++)
                {
                    SetsManager.instance.StainSelection[i].UpdateFloatProperty(parameter, value);
                }

                for (int i = 0; i < SetsManager.instance.BarSelection.Count; i++)
                {
                    SetsManager.instance.BarSelection[i].UpdateFloatProperty(parameter, value);
                }
            }
        }

        public void changeGlobalShaderValue(GameObject slider, bool selectedOrMulti)
        {
            SendGlobalParameterToCurrent(slider.name, slider.GetComponent<Slider>().value, selectedOrMulti);
        }

        public void InitColor(Material m)
        { //Initialise the color set on menu opening
            replaceMaterial(m);
            chooseShaders(ShadersSelection.value);
            Color c = m.color;
            changeR((int)Mathf.Round(c.r * 255f));
            changeG((int)Mathf.Round(c.g * 255f));
            changeB((int)Mathf.Round(c.b * 255f));
            changeA((int)Mathf.Round(c.a * 255f));
            UpdateColor();
            //shader rework : update emission, fresnell, power, smoothness, metallic
            if (m.HasProperty("_Emission"))
                UpdateParameter("_Emission", m.GetFloat("_Emission"));
            if (m.HasProperty("_Fresnel"))
                UpdateParameter("_Fresnel", m.GetFloat("_Fresnel"));
            if (m.HasProperty("_Power"))
                UpdateParameter("_Power", m.GetFloat("_Power"));
            if (m.HasProperty("_Smoothness"))
                UpdateParameter("_Smoothness", m.GetFloat("_Smoothness"));
            if (m.HasProperty("_Metallic"))
                UpdateParameter("_Metallic", m.GetFloat("_Metallic"));
        }

        public Material GetMaterialFromCurrent()
        {
            Material chooseMaterial = null;
            switch (currentChoosen)
            {
                case 8:
                    chooseMaterial = new Material(SetsManager.instance.Selected);
                    break;

                case 9:
                    if (InterfaceManager.instance.shaderType == "stain")
                    {
                        chooseMaterial = SetsManager.instance.StainSelection[0];
                    }
                    else
                    {
                        chooseMaterial = SetsManager.instance.BarSelection[0];
                    }
                    break;
            }

            return chooseMaterial;
        }

        public void chooseShaders(int v)
        {
            currentChoosen = v;
            Material chooseMaterial = GetMaterialFromCurrent();

            if (currentChoosen == 9)
            {
                multi_color_parameters.SetActive(true);
            }
            else
            {
                multi_color_parameters.SetActive(false);
            }

            foreach (GameObject g in sliders)
            {
                if (chooseMaterial.HasProperty(g.name))
                {
                    g.SetActive(true);
                    if (g.transform.childCount > 1 && g.transform.GetChild(1).GetComponent<Slider>() != null)
                    {
                        float value = -1;

                        if (currentChoosen == 8)
                        {
                            value = SetsManager.instance.Selected.HasProperty(g.name) ? SetsManager.instance.Selected.GetFloat(g.name) : 0f;
                        }
                        else if (currentChoosen == 9)
                        {
                            if (InterfaceManager.instance.shaderType == "stain")
                            {
                                value = SetsManager.instance.StainSelection[0].HasProperty(g.name) ? SetsManager.instance.StainSelection[0].GetFloat(g.name) : 0f;
                            }
                            else
                            {
                                value = SetsManager.instance.BarSelection[0].HasProperty(g.name) ? SetsManager.instance.BarSelection[0].GetFloat(g.name) : 0f;
                            }
                        }
                        else
                        {
                            value = SelectionManager.materials != null && SelectionManager.materials[SelectionManager.Static_selectionValue] != null && SelectionManager.materials[SelectionManager.Static_selectionValue].HasProperty(g.name) ? SelectionManager.materials[SelectionManager.Static_selectionValue].GetFloat(g.name) : 0f;
                        }
                        g.transform.GetChild(1).GetComponent<Slider>().value = value;
                    }
                }
                else
                {
                    g.SetActive(false);
                }
            }

            if (chooseMaterial.HasProperty("_Color") && v != 9 && v != 10)
            {
                Color_sliders.SetActive(true);
                ValidNewColor.gameObject.SetActive(true);
                CancelNewColor.gameObject.SetActive(true);
            }
            else
            {
                ValidNewColor.gameObject.SetActive(false);
                CancelNewColor.gameObject.SetActive(false);
                Color_sliders.SetActive(false);
            }

            NewColor.GetComponent<Renderer>().material = chooseMaterial;

            UpdateColor();
        }

        //Select the material in the dropdown button
        public void replaceMaterial(Material m)
        {
            if (m.name.IndexOf("default") == 0)
                ShadersSelection.value = 0;
            else if (m.name.IndexOf("TransparentCell") == 0)
                ShadersSelection.value = 1;
            else if (m.name.IndexOf("WhiteGem") == 0)
                ShadersSelection.value = 2;
            else if (m.name.IndexOf("Rough Textured Metal") == 0)
                ShadersSelection.value = 3;
            else if (m.name.IndexOf("Robot MatCap Plain Additive") == 0)
                ShadersSelection.value = 4;
            else if (m.name.IndexOf("Lavabrick") == 0)
                ShadersSelection.value = 5;
            else if (m.name.IndexOf("Stars") == 0)
                ShadersSelection.value = 6;
            else if (m.name.IndexOf("Plasticine_08_8K_Small Red") == 0)
                ShadersSelection.value = 7;
            else if (m.name.IndexOf("SelectedCell") == 0)
                ShadersSelection.value = 8;
        }

        public static Material getMaterialByName(string name)
        {
            Material chooseMaterial = null;
            if (name.IndexOf("StandardCell") == 0 || name.IndexOf("default") == 0 || name.IndexOf("FresnellCell") == 0)
                chooseMaterial = new Material(SetsManager.instance.Default);
            else if (name.IndexOf("TransparentCell") == 0)
                chooseMaterial = new Material(SetsManager.instance.Transparent);
            else if (name.IndexOf("WhiteGem") == 0)
                chooseMaterial = new Material(SetsManager.instance.Diamond);
            else if (name.IndexOf("Rough Textured Metal") == 0)
                chooseMaterial = new Material(SetsManager.instance.Dark);
            else if (name.IndexOf("Robot MatCap Plain Additive") == 0)
                chooseMaterial = new Material(SetsManager.instance.Vertex);
            else if (name.IndexOf("Bubble wrap pattern") == 0)
                chooseMaterial = new Material(SetsManager.instance.Bumped);
            else if (name.IndexOf("Lavabrick") == 0)
                chooseMaterial = new Material(SetsManager.instance.Brick);
            else if (name.IndexOf("Stars") == 0)
                chooseMaterial = new Material(SetsManager.instance.Stars);
            else if (name.IndexOf("Wireframe - ShadedUnlit") == 0)
                chooseMaterial = new Material(SetsManager.instance.Wireframe);
            else if (name.IndexOf("WPlasticine_08_8K_Small Red") == 0)
                chooseMaterial = new Material(SetsManager.instance.bloodcell);
            else if (name.IndexOf("SelectedCell") == 0)
                chooseMaterial = new Material(SetsManager.instance.Selected);
            return chooseMaterial;
        }

        public void UpdateColor()
        {
            NewColor.GetComponent<Renderer>().material.UpdateColorProperty("_Color", new Color((float)R / 255f, (float)G / 255f, (float)B / 255f, (float)A / 255f));
        }

        public void UpdateParameter(string param, float value)
        {
            if (NewColor.GetComponent<Renderer>().material.HasProperty(param))
                NewColor.GetComponent<Renderer>().material.UpdateFloatProperty(param, value);
        }

        public void changeR(int v)
        {
            if (v < 0)
                v = 0;
            if (v > 255)
                v = 255;
            R = v;
            SliderR.value = (float)v;
            InputFieldR.text = v.ToString();
        }

        public void changeB(int v)
        {
            if (v < 0)
                v = 0;
            if (v > 255)
                v = 255;
            B = v;
            SliderB.value = (float)v;
            InputFieldB.text = v.ToString();
        }

        public void changeG(int v)
        {
            if (v < 0)
                v = 0;
            if (v > 255)
                v = 255;
            G = v;
            SliderG.value = (float)v;
            InputFieldG.text = v.ToString();
        }

        public void changeA(int v)
        {
            if (v < 0)
                v = 0;
            if (v > 255)
                v = 255;
            A = v;
            SliderA.value = (float)v;
            InputFieldA.text = v.ToString();
        }

        //When Input field changed
        public void changeFieldR()
        {
            int v = 0;
            int.TryParse(InputFieldR.text, out v);
            changeR(v);
            UpdateColor();
        }

        public void changeFieldB()
        {
            int v = 0;
            int.TryParse(InputFieldB.text, out v);
            changeB(v);
            UpdateColor();
        }

        public void changeFieldG()
        {
            int v = 0;
            int.TryParse(InputFieldG.text, out v);
            changeG(v);
            UpdateColor();
        }

        public void changeFieldA()
        {
            int v = 0;
            int.TryParse(InputFieldA.text, out v);
            changeA(v);
            UpdateColor();
        }

        //When Slider changed
        public void changeSliderR()
        {
            int v = (int)Mathf.Round(SliderR.GetComponent<UnityEngine.UI.Slider>().value);
            changeR(v);
            UpdateColor();
        }

        public void changeSliderB()
        {
            int v = (int)Mathf.Round(SliderB.GetComponent<UnityEngine.UI.Slider>().value);
            changeB(v);
            UpdateColor();
        }

        public void changeSliderG()
        {
            int v = (int)Mathf.Round(SliderG.GetComponent<UnityEngine.UI.Slider>().value);
            changeG(v);
            UpdateColor();
        }

        public void changeSliderA()
        {
            int v = (int)Mathf.Round(SliderA.GetComponent<UnityEngine.UI.Slider>().value);
            changeA(v);
            UpdateColor();
        }

        public void Valid()
        {
            MenuColor.SetActive(false);
        }

        public void Cancel()
        {
            MenuColor.SetActive(false);
        }
    }
}