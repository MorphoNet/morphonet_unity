﻿using UnityEngine;
using UnityEngine.UI;

namespace MorphoNet
{
    public class ButtonUserTracking : MonoBehaviour
    {
        private Button AttachedButton;

        // Start is called before the first frame update
        private void Start()
        {
            AttachedButton = gameObject.GetComponent<Button>();
            if (AttachedButton != null)
            {
                /*AttachedButton.onClick.AddListener(delegate ()
                {
                    StoreInteraction();
                });*/
            }
        }

        // Update is called once per frame
        private void StoreInteraction()
        {
            string path = "";
            GameObject to_treat = gameObject;
            while (to_treat != null && to_treat.name != "Canvas" && to_treat.transform.parent != null)
            {
                path = to_treat.name + "," + path;
                if (to_treat.transform.parent != null)
                    to_treat = to_treat.transform.parent.gameObject;
                else
                    to_treat = null;
            }
            TrackerManagement.instance.SendTrackingInfo(path + "Click");
        }
    }
}