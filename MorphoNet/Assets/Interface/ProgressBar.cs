﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using UnityEngine.UI;

namespace MorphoNet
{
    public class Progress
    {
        private Correspondence cor;
        public GameObject go;
        private Image foregroundImage;
        public float value;
        public int progress_type;
        private UnityWebRequest wbrImages;

        public void init(string name, int progress_type, int count)
        {
            this.progress_type = progress_type;
            go = SetsManager.instance.InstantiateGO(ProgressBar.defaultPB, ProgressBar.defaultPB.transform.parent);
            go.name = name;
            go.transform.Find("Name").gameObject.GetComponent<Text>().text = name;
            go.transform.Translate(new Vector3(0, count * 20f * InterfaceManager.instance.canvasScale, 0));
            foregroundImage = go.transform.Find("Foreground").gameObject.GetComponent<Image>();
            foregroundImage.fillAmount = 0f;
            value = 0f;
            go.SetActive(true);
            update();
        }

        public Progress(int count)
        {//Download Meshes or Images
            init("Meshes", 0, count);
        }

        public Progress(bool reload, int count)
        {//Download Meshes or Images
            init("Reload meshes", 3, count);
        }

        public Progress(Correspondence cor, int count)
        { //Download Infos
            this.cor = cor;
            init(cor.name, 1, count);
        }

        public Progress(UnityWebRequest wbrImages, int count)
        {
            this.wbrImages = wbrImages;
            init("Raw Image", 2, count);
        }

        public void shift(int s)
        {
            if (go != null)
            { go.transform.Translate(new Vector3(0, -s * 20f * InterfaceManager.instance.canvasScale, 0)); }
        }

        public void update()
        {
            switch (progress_type)
            {
                case 0: //MESHES
                    if (SetsManager.instance == null || SetsManager.instance.nbMeshesDownloaded == -1 || SetsManager.instance.TotalNumberOfMesh == -1)
                    { value = 1f; break; }
                    value = 1f * SetsManager.instance.nbMeshesDownloaded / SetsManager.instance.TotalNumberOfMesh;
                    break;

                case 1: //CORRESPONDENCE
                    if (cor.lines == null)
                        value = 1f;
                    else
                        value = 1f * cor.startIndex / cor.lines.Length;
                    break;

                case 2: //IMAGES
                    if (wbrImages == null)
                    { value = 1f; break; }
                    value = wbrImages.downloadProgress;
                    if (wbrImages.isNetworkError || wbrImages.isHttpError || wbrImages.isDone)
                        value = 1f;
                    break;

                case 3: //RELOAD MESHES
                    if (SetsManager.instance == null || SetsManager.instance.CurrentTimeReload == -1 || SetsManager.instance.TimeReloadCount == -1 || !MorphoTools.GetDataset().is_reloading)
                    { value = 1f; break; }
                    value = 1f * SetsManager.instance.CurrentTimeReload / SetsManager.instance.TimeReloadCount;
                    break;
            }

            //MorphoDebug.Log (this.go.name + "->" + this.value .ToString ());
            if (foregroundImage != null && value != null)
            {
                foregroundImage.fillAmount = value;
            }
        }
    }

    public class ProgressBar : MonoBehaviour
    {
        public static GameObject defaultPB;
        public static List<Progress> Progresss;

        //Main initialisation
        private void Start()
        {
            init();
        }

        public static void init()
        {
            if (defaultPB == null)
            {
                defaultPB = InterfaceManager.instance.canvas.transform.Find("ProgressBar").Find("default").gameObject;
                defaultPB.SetActive(false);
            }
            if (Progresss == null)
                Progresss = new List<Progress>();
        }

        public static void addMeshes()
        {//Meshes
            init();
            Progress p = new Progress(Progresss.Count);
            Progresss.Add(p);
        }

        public static void addReloadMesh()
        {//Meshes
            init();
            Progress p = new Progress(true, Progresss.Count);
            Progresss.Add(p);
        }

        public static void addCor(Correspondence cor)
        {
            init();
            Progress p = new Progress(cor, Progresss.Count);
            Progresss.Add(p);
        }

        public static void addImages(UnityWebRequest uwr)
        {
            init();
            Progress p = new Progress(uwr, Progresss.Count);
            Progresss.Add(p);
        }

        public void Update()
        {
            foreach (Progress p in Progresss)
                p.update();

            List<Progress> NewProgresss = new List<Progress>();
            int shift = 0;
            foreach (Progress p in Progresss)
            {
                //MorphoDebug.Log("Evaluate " + p.go.name+ " with value "+p.value);
                if (p.value >= 1f)
                {
                    shift += 1;
                    DestroyImmediate(p.go);
                }
                else
                {
                    p.shift(shift);
                    NewProgresss.Add(p);
                }
            }
            Progresss = NewProgresss;
        }
    }
}