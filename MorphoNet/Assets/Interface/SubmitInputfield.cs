﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SubmitInputfield : MonoBehaviour
{
    public Button attached_button;
    private InputField self;

    void Start()
    {
        self = gameObject.GetComponent<InputField>();
    }
    // Update is called once per frame
    void Update()
    {
        if (self != null && Input.GetKeyUp(KeyCode.Return)) { attached_button.OnPointerClick(new PointerEventData(EventSystem.current)); }
    }
}
