using System;
using System.Collections.Generic;
using AssemblyCSharp;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MorphoNet
{
    public class ScatterView : MonoBehaviour
    {
        public Dictionary<int, Vector3> CenterT;
        public Dictionary<int, int> NbCenterT;
        public Vector3 embryoCenterScatter;
        public DataSet dataset;
        public bool canScatter = true;
        public int viewMode;
        public bool scatterReady = false;

        //scatter View
        public float explodeFactor = 0.0f;

        public Dictionary<int, float> previous_explodeFactor;

        //since translate : factor for all types of scattering
        public Dictionary<int, float> previous_explodeFactor_all;

        public Dictionary<int, float> previous_explodeFactor_selected;
        public Dictionary<int, float> previous_explodeFactor_color;

        //scatterdisplay
        public Text scatterFactor;

        public void AttachDataSet(DataSet d)
        {
            dataset = d;
        }

        public void FillCenterTab()
        {
            for (int t = dataset.MinTime; t <= dataset.MaxTime; t++)
                if (!CenterT.ContainsKey(t))
                    CenterT[t] = new Vector3();
            for (int t = dataset.MinTime; t <= dataset.MaxTime; t++)
                if (!NbCenterT.ContainsKey(t))
                    NbCenterT[t] = 0;
            for (int t = dataset.MinTime; t <= dataset.MaxTime; t++)
                if (!previous_explodeFactor.ContainsKey(t))
                    previous_explodeFactor[t] = 0f;
            for (int t = dataset.MinTime; t <= dataset.MaxTime; t++)
                if (!previous_explodeFactor_all.ContainsKey(t))
                    previous_explodeFactor_all[t] = 0f;
            for (int t = dataset.MinTime; t <= dataset.MaxTime; t++)
                if (!previous_explodeFactor_selected.ContainsKey(t))
                    previous_explodeFactor_selected[t] = 0f;
            for (int t = dataset.MinTime; t <= dataset.MaxTime; t++)
                if (!previous_explodeFactor_color.ContainsKey(t))
                    previous_explodeFactor_color[t] = 0f;
        }

        public void init()
        {
            scatterFactor = InterfaceManager.instance.ScatterFactor;
            if (canScatter)
            {
                //Center by time
                CenterT = new Dictionary<int, Vector3>();
                NbCenterT = new Dictionary<int, int>();
                previous_explodeFactor = new Dictionary<int, float>();
                previous_explodeFactor_all = new Dictionary<int, float>();
                previous_explodeFactor_selected = new Dictionary<int, float>();
                previous_explodeFactor_color = new Dictionary<int, float>();

                for (int t = dataset.MinTime; t <= dataset.MaxTime; t++)
                {
                    CenterT[t] = new Vector3();
                    NbCenterT[t] = 0;
                    previous_explodeFactor[t] = 0f;
                    previous_explodeFactor_all[t] = 0f;
                    previous_explodeFactor_selected[t] = 0f;
                    previous_explodeFactor_color[t] = 0f;
                }

                viewMode = 0; //Default (nothing selected)
                scatterReady = true;
            }
        }

        //We update the boundaries of the sliders accord the different gravity center
        public void updateCenter(int t, Vector3 Grav)
        {
            //countermeasure to avoid bugs on scatter
            if (!Double.IsNaN(Grav.x) && !Double.IsNaN(Grav.y) && !Double.IsNaN(Grav.z))
            {
                if (canScatter)
                {
                    if (CenterT == null)
                        CenterT = new Dictionary<int, Vector3>();

                    if (NbCenterT == null)
                        NbCenterT = new Dictionary<int, int>();

                    if (CenterT.ContainsKey(t))
                        CenterT[t] += Grav;
                }

                if (NbCenterT.ContainsKey(t))
                    NbCenterT[t] += 1;

                if (explodeFactor > 0 && t == dataset.CurrentTime)
                    UpdateView(t); //During Loading
            }
        }

        public void UpdateExplodeFactor(float value)
        {
            value = Mathf.Clamp(explodeFactor + value, 0f, 5f);
            if (canScatter)
            {
                scatterFactor.text = (1 + value).ToString("F2") + "x";
                explodeFactor = value;
                UpdateView(dataset.CurrentTime);
            }
        }

        //When Slider move
        public void onChangeExplodeFactor(float value)
        {
            if (canScatter)
            {
                scatterFactor.text = (1 + value).ToString("F2") + "x";
                explodeFactor = value;
                UpdateView(dataset.CurrentTime);
            }
        }

        public void UpdateSliderToTime(int timepoint)
        {
            if (canScatter)
            {
                if (previous_explodeFactor != null && previous_explodeFactor.ContainsKey(timepoint))
                {
                    scatterFactor.text =
                        (1 + previous_explodeFactor[timepoint]).ToString("F2") + "x";
                }
            }
        }

        //When mode change
        public void OnScatterTypeChange(int scatterType)
        {
            viewMode = scatterType;
            if (scatterType == 0)
                OnScatterReset();
        }

        public void UpdateView(int t,bool force=false)
        {
            if (canScatter && dataset.CellsByTimePoint.ContainsKey(t))
            {
                if (!previous_explodeFactor.ContainsKey(t))
                    previous_explodeFactor[t] = explodeFactor - 1;
                if (!previous_explodeFactor_all.ContainsKey(t))
                    previous_explodeFactor_all[t] = explodeFactor - 1;
                if (!previous_explodeFactor_selected.ContainsKey(t))
                    previous_explodeFactor_selected[t] = explodeFactor - 1;
                if (!previous_explodeFactor_color.ContainsKey(t))
                    previous_explodeFactor_color[t] = explodeFactor - 1;
                if (!CenterT.ContainsKey(t))
                    CenterT[t] = new Vector3();
                if (!NbCenterT.ContainsKey(t))
                    NbCenterT[t] = 0;
                if (
                    (dataset.CellsByTimePoint.ContainsKey(t)
                    && explodeFactor != previous_explodeFactor[t]) || force
                )
                {
                    embryoCenterScatter = CenterT[t] / NbCenterT[t];
                    if (viewMode == 0)
                    { // CAS 0: WITHOUT SELECTION
                        foreach (Cell cell in dataset.CellsByTimePoint[t])
                        {
                            if (cell.Channels != null)
                            {
                                foreach (CellChannelRenderer c in cell.Channels.Values)
                                {
                                    if (c != null && c.AssociatedCellObject != null)
                                    {
                                        float zoom_fact = MorphoTools
                                            .GetTransformationsManager()
                                            .CurrentScale.x;
                                        Vector3 direction =
                                            cell.getFirstChannel().Gravity - embryoCenterScatter;
                                        if (cell.primi != null)
                                        {
                                            c.AssociatedCellObject.transform.localPosition =
                                                (direction * explodeFactor)
                                                + cell.getFirstChannel().Gravity;
                                        }
                                        else
                                        {
                                            c.AssociatedCellObject.transform.localPosition =
                                                direction * explodeFactor;
                                        }
                                    }
                                }
                            }
                        }

                        previous_explodeFactor_all[t] = explodeFactor;
                    }

                    if (viewMode == 1)
                    { //CAS 1 : ON SELECTED CELL
                        int ctr = 0;
                        foreach (Cell cell in dataset.CellsByTimePoint[t])
                            if (
                                cell.getFirstChannel() != null
                                && dataset.PickedManager.clickedCells.Contains(cell)
                            )
                            {
                                if (cell.Channels != null)
                                {
                                    foreach (CellChannelRenderer c in cell.Channels.Values)
                                    {
                                        if (c != null)
                                        {
                                            float zoom_fact = MorphoTools
                                                .GetTransformationsManager()
                                                .CurrentScale.x;
                                            ctr++;
                                            Vector3 direction =
                                                cell.getFirstChannel().Gravity
                                                - embryoCenterScatter;
                                            if (cell.primi != null)
                                            {
                                                c.AssociatedCellObject.transform.localPosition =
                                                    (direction * explodeFactor)
                                                    + cell.getFirstChannel().Gravity;
                                            }
                                            else
                                            {
                                                c.AssociatedCellObject.transform.localPosition =
                                                    direction * explodeFactor;
                                            }
                                        }
                                    }
                                }
                            }
                        previous_explodeFactor_selected[t] = explodeFactor;
                    }

                    if (viewMode == 2)
                    { //CAS 2 : ON COLORZED CELL
                        int ctr = 0;
                        SelectionManager.ComputeSelectionCenters();
                        foreach (Cell cell in dataset.CellsByTimePoint[t])
                            if (
                                cell.getFirstChannel() != null
                                && cell.selection != null
                                && cell.selection.Count > 0
                            )
                            {
                                if (cell.Channels != null)
                                {
                                    foreach (CellChannelRenderer c in cell.Channels.Values)
                                    {
                                        if (c != null)
                                        {
                                            float zoom_fact = MorphoTools
                                                .GetTransformationsManager()
                                                .CurrentScale.x;
                                            ctr++;
                                            //to compute direction, use the good embryo center, but as cell center, use the centerBySelection stored in Selection color, and divide it by countBySelection from Selection color
                                            Vector3 groupSum = SelectionManager.getCentersByGroup(
                                                cell.selection[0]
                                            );
                                            int count = SelectionManager.getCountByGroup(
                                                cell.selection[0]
                                            );
                                            Vector3 direction =
                                                (groupSum / count) - embryoCenterScatter;
                                            if (cell.primi != null)
                                            {
                                                c.AssociatedCellObject.transform.localPosition =
                                                    (direction * explodeFactor)
                                                    + cell.getFirstChannel().Gravity;
                                            }
                                            else
                                            {
                                                c.AssociatedCellObject.transform.localPosition =
                                                    direction * explodeFactor;
                                            }
                                        }
                                    }
                                }
                            }
                        if (ctr > 0)
                            previous_explodeFactor_color[t] = explodeFactor;
                    }
                }
                previous_explodeFactor[t] = explodeFactor;
                UpdateInfoConnectionPosition();
            }
        }

        public void ResetScattering()
        {
            onChangeExplodeFactor(0);
        }

        //reset all cells to center position to avoid differed scattering
        public void OnScatterReset()
        {
            for (
                int t = MorphoTools.GetDataset().MinTime;
                t <= MorphoTools.GetDataset().MaxTime;
                t++
            )
            {
                if (!previous_explodeFactor.ContainsKey(t))
                    previous_explodeFactor[t] = 0;
                if (!previous_explodeFactor_all.ContainsKey(t))
                    previous_explodeFactor_all[t] = 0;
                if (!previous_explodeFactor_selected.ContainsKey(t))
                    previous_explodeFactor_selected[t] = 0;
                if (!previous_explodeFactor_color.ContainsKey(t))
                    previous_explodeFactor_color[t] = 0;
                if (!CenterT.ContainsKey(t))
                    CenterT[t] = new Vector3();
                if (!NbCenterT.ContainsKey(t))
                    NbCenterT[t] = 0;

                embryoCenterScatter = CenterT[t] / NbCenterT[t];
                if (viewMode == 0)
                {
                    foreach (Cell cell in dataset.CellsByTimePoint[t])
                    {
                        if (cell.Channels != null)
                        {
                            foreach (CellChannelRenderer c in cell.Channels.Values)
                            {
                                if (c != null)
                                {
                                    Vector3 dir = c.Gravity - c.GetCurrentGravityCenter();
                                    if(c.MeshFilter.mesh.vertexCount>0)
                                        c.AssociatedCellObject.transform.localPosition =
                                            c.AssociatedCellObject.transform.localPosition + dir;
                                }
                            }
                        }
                    }

                    previous_explodeFactor_all[t] = 0;
                    previous_explodeFactor_selected[t] = 0;
                    previous_explodeFactor_color[t] = 0;
                    previous_explodeFactor[t] = 0;
                }
            }
            UpdateInfoConnectionPosition();
        }

        public void UpdateInfoConnectionPosition()
        {
            if (MorphoTools.GetDataset().InfoConnectionParent != null)
            {
                if (MorphoTools.GetDataset().InfoConnectionParent.transform.childCount > 0)
                {
                    GameObject anc = MorphoTools.GetDataset().mesh_by_time[MorphoTools.GetDataset().CurrentTime];
                    foreach (Transform child in MorphoTools.GetDataset().InfoConnectionParent.transform)
                    {
                        ConnectionInfo ci = child.gameObject.GetComponent<ConnectionInfo>();
                        if (ci != null)
                        {
                            LineRenderer lr = child.gameObject.GetComponent<LineRenderer>();
                            Transform parentTransform = MorphoTools.GetDataset().InfoConnectionParent.transform;

                            Vector3 originpos = parentTransform.InverseTransformPoint(ci.Source.transform.TransformPoint(ci.SCh.MeshGravity));
                            Vector3 destpos = parentTransform.InverseTransformPoint(ci.Destination.transform.TransformPoint(ci.DCh.MeshGravity));

                            lr.SetPosition(0, originpos);
                            lr.SetPosition(1, destpos);
                        }
                    }
                }
            }
        }
    }
}