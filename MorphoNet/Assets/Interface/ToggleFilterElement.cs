using MorphoNet.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum FilterType
{
    Source,
    Group,
    Tag,
    Species,
    Favorite
}

public class ToggleFilterElement : MonoBehaviour
{
    public Text Label;
    public int Id;
    public Toggle Toggle;

    public FilterType Type;
    public MorphoSource Source;
    public string Name { get => Label.text; }

    public void SetLabel(string name)
    {
        Label.text = name;
    }
}
