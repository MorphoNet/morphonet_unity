﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class onMouseOver : MonoBehaviour
{
        private Vector3 _PrevMousePosition;
        private float _Wait = 0f;
        private GameObject _Current;
        public GameObject BackupLast;
        public GameObject MenuDataset;
        public GameObject MenuImages;
        public GameObject MenuInfos;
        public GameObject MenuObjects;
        public GameObject MenuGroups;
        public GameObject MenuGenetic;
        public GameObject MenuMovie;

        private bool _OnUI = true;
        private bool _Hitted;

        private bool IsMainMenu()
        {
            return MenuDataset.activeSelf
               || MenuInfos.activeSelf
               || MenuImages.activeSelf
               || MenuObjects.activeSelf
               || MenuGroups.activeSelf
               || MenuGenetic.activeSelf
               || MenuMovie.activeSelf;
        }

        private void Update()
        {
            if (Vector3.Distance(Input.mousePosition, _PrevMousePosition) < 0.05f && !_OnUI)
            {
                _Wait += Time.unscaledDeltaTime;
            }
            else if (!_OnUI)
            {
                if (BackupLast != null)
                    BackupLast.SetActive(false);

                if (_Current != null)
                    _Current.SetActive(false);

                _Wait = 0f;
                _Hitted = false;
            }
            if ((_Wait >= 0.5f && !_Hitted) || _OnUI)
            {
                var pointerEventData = new PointerEventData(EventSystem.current);
                var hits = new List<RaycastResult>();

                pointerEventData.position = Input.mousePosition;
                _Current = null;
                _Hitted = true;

                EventSystem.current.RaycastAll(pointerEventData, hits);
                foreach (RaycastResult h in hits)
                {
                    bool toCheck = true;

                    if (h.gameObject.name == "BoutonDataset" && IsMainMenu())
                        toCheck = false;
                    if (h.gameObject.name == "BoutonProperties" && IsMainMenu())
                        toCheck = false;
                    if (h.gameObject.name == "BoutonLabels" && IsMainMenu())
                        toCheck = false;
                    if (h.gameObject.name == "BoutonGroups" && IsMainMenu())
                        toCheck = false;
                    if (h.gameObject.name == "BoutonGenetic" && IsMainMenu())
                        toCheck = false;
                    if (h.gameObject.name == "BoutonMoovie" && IsMainMenu())
                        toCheck = false;

                    if (toCheck && h.gameObject.transform.Find("comment") != null)
                        _Current = h.gameObject.transform.Find("comment").gameObject;
                }

                if (_Current != null)
                {
                    _OnUI = true;
                    _Current.SetActive(true);
                }
            }
            else if (_Current != null && !_Hitted)
            {
                _OnUI = false;
                _Current.SetActive(false);
                _Current = null;
            }

            if (BackupLast != null && BackupLast != _Current)
                BackupLast.SetActive(false);

            BackupLast = _Current;
            _PrevMousePosition = Input.mousePosition;
        }
    }