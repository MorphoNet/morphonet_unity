using MorphoNet.Core;
using MorphoNet.Interaction;
using MorphoNet.UI.XR;
using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace MorphoNet
{
    public class InteractionManager : MonoBehaviour
    {
        public static InteractionManager instance = null;
        private InteractionMediator _InteractionMediator;

        //store the download infos coroutine to be sure to not start a download if a download is running
        public Coroutine infos_dl = null;

        //instance static allow to use them anywhere in the code using InteractionManager.instance.<function or variable>
        private void Awake()
        {
            if (instance == null)
                instance = this;
            else
                Destroy(gameObject);
        }

        private void Start()
        {
            FetchInteractionMediator();
        }

        /*
         * All functions are callback used to transfer scene calls to corresponding function in other scripts
         */

        public void ChangeMenuBackgrounds(bool v)
        {
            foreach (Image i in InterfaceManager.instance.menus_background)
            {
                i.color = new Color(v ? 191 : 255, v ? 191 : 255, v ? 191 : 255, v ? 100 : 80);
            }
        }

        public void UpdateTimeDisplay()
        {
            InterfaceManager.instance.time_display.text = InterfaceManager.instance.use_all_times ? "All time points" : InterfaceManager.instance.until_next_division ? "For cell's life" : InterfaceManager.instance.use_time_window ? "Times : " + InterfaceManager.instance.min_custom_time_selection + " => " + InterfaceManager.instance.max_custom_time_selection : "Time point : " + MorphoTools.GetDataset().CurrentTime;
            
        }

        public void AddRecorderAction()
        {
            MorphoTools.GetRecorder().addNewAction();
        }

        public void DisplayScenarioMenu()
        {
            InterfaceManager.instance.ShowScenarioMenu();
        }

        public void DisplayRenderingMenu()
        {
            InterfaceManager.instance.ShowRenderingMenu();
        }

        public void ForAllTimes(bool b)
        {
            InterfaceManager.instance.use_all_times = b;

            if (b)
            {
                InterfaceManager.instance.until_next_division = false;
                InterfaceManager.instance.use_time_window = false;
            }
            UpdateTimeDisplay();
        }

        public void DisplaySisterLinks()
        {
            MorphoTools.GetFigureManager().DisplaySisterLinks();
        }

        public void ClearSisterLinks()
        {
            MorphoTools.GetFigureManager().ClearSisterLinks();
        }

        public void UpdateScaleLines(float scale)
        {
            MorphoTools.GetFigureManager().UpdateLineThickness(scale);
        }

        public void UpdateSisterLinks()
        {
            MorphoTools.GetFigureManager().UpdateSisterLinks();
        }

        public void ForTimeRange(bool b)
        {
            InterfaceManager.instance.use_time_window = b;
            InterfaceManager.instance.time_range.SetActive(b);

            if (b)
            {
                InterfaceManager.instance.until_next_division = false;
                InterfaceManager.instance.use_all_times = false;
            }
            UpdateTimeDisplay();
        }

        public void ToNextDisvision(bool b)
        {
            InterfaceManager.instance.until_next_division = b;

            if (b)
            {
                InterfaceManager.instance.use_time_window = false;
                InterfaceManager.instance.use_all_times = false;
            }
            UpdateTimeDisplay();
        }

        private bool time_window_active = false;

        public void ShowTimeWindow()
        {
            time_window_active = !time_window_active;
            InterfaceManager.instance.time_windows.SetActive(time_window_active);
        }

        public void UpdateMinTimePoint(string value)
        {
            int final_value = -1;
            if (int.TryParse(value, out final_value))
            {
                if (final_value < SetsManager.instance.DataSet.MinTime || final_value > SetsManager.instance.DataSet.MaxTime)
                {
                    InterfaceManager.instance.min_input.text = InterfaceManager.instance.min_custom_time_selection + "";
                    return;
                }
                InterfaceManager.instance.range_slider.LowValue = final_value;
                InterfaceManager.instance.min_custom_time_selection = final_value;
            }
            UpdateTimeDisplay();
        }

        public void UpdateMaxTimePoint(string value)
        {
            int final_value = -1;
            if (int.TryParse(value, out final_value))
            {
                if (final_value < SetsManager.instance.DataSet.MinTime || final_value > SetsManager.instance.DataSet.MaxTime)
                {
                    InterfaceManager.instance.max_input.text = InterfaceManager.instance.max_custom_time_selection + "";
                    return;
                }
                InterfaceManager.instance.range_slider.HighValue = final_value;
                InterfaceManager.instance.max_custom_time_selection = final_value;
            }
            UpdateTimeDisplay();
        }

        public void UpdateMinMaxTime(Single a, Single b)
        {
            int low = (int)a;
            int high = (int)b;
            InterfaceManager.instance.max_input.text = high + "";
            InterfaceManager.instance.min_input.text = low + "";
            InterfaceManager.instance.min_custom_time_selection = low;
            InterfaceManager.instance.max_custom_time_selection = high;
            UpdateTimeDisplay();
        }

        public void useCustomTime()
        {
            InterfaceManager.instance.custom_time_is_active = !InterfaceManager.instance.custom_time_is_active;
            InterfaceManager.instance.time_range.SetActive(InterfaceManager.instance.custom_time_is_active);
            InterfaceManager.instance.custom_time_button_text.text = InterfaceManager.instance.custom_time_is_active ? "on" : "off";
        }

        //on or off data streaming flag
        public void activateWindowLoading(bool status)
        {
            MorphoTools.GetDataset().range_time_loading = status;
        }

        //Slider control size of data streaming range
        public void UpdateWindowSize(float size)
        {
            MorphoTools.GetDataset().range_size = (int)size;
            if (InterfaceManager.instance.range_window_size != null)
            {
                InterfaceManager.instance.range_window_size.text = (int)size + "";
            }
            MorphoTools.GetDataset().UpdateLoadedMeshCache();
        }

        //slider control size of data streaming cached data
        public void UpdateWindowCache(float size)
        {
            MorphoTools.GetDataset().range_cache = (int)size;
            if (InterfaceManager.instance.range_window_cache != null)
            {
                InterfaceManager.instance.range_window_cache.text = (int)size + "";
            }
            MorphoTools.GetDataset().UpdateLoadedMeshCache();
        }

        //Change the settings menu state will ALWAYS close the MenuShortcuts
        public void changeMenuParamsState()
        {
            InterfaceManager.instance.MenuShortcuts.SetActive(false);
            InterfaceManager.instance.Params.SetActive(!InterfaceManager.instance.Params.activeSelf);
        }

        private static bool _XRStarted = false;

        public void SwitchVrMode()
        {
            if (_InteractionMediator == null)
                FetchInteractionMediator();

            if (_XRStarted)
            {
                MorphoNet.SceneManager.Instance.DisableVr();
                _InteractionMediator.ResetSelectionModifiers();
            }
            else
            {
                _InteractionMediator.ResetXRModifiersToDefault();
                MorphoNet.SceneManager.Instance.EnableVr();
                
            }

            _XRStarted = !_XRStarted;
        }

        private void FetchInteractionMediator()
        {
            var startup = FindObjectOfType<StartUp>();
            if (startup != null)
                _InteractionMediator = startup.GetInteractionMediator();
        }

        public Transform GetMainControllerCuttingPlaneTransform()
        {
            if (!_XRStarted)
                return (null);

            if (_InteractionMediator == null)
                FetchInteractionMediator();

            if (_InteractionMediator == null)
                return null;

            return _InteractionMediator.GetMainXRControllerCuttingPlaneTransform();
        }


        public void MenuDatasetActive()
        {
            InterfaceManager.instance.MainMenuActive(InterfaceManager.instance.MenuDataSet.Menu);
        }

        public void MenuImagesActive()
        {
            InterfaceManager.instance.MainMenuActive(InterfaceManager.instance.MenuImages.gameObject);
        }

        public void MenuObjectsActive()
        {
            if (!InterfaceManager.instance.MenuObjects.activeSelf)
                InterfaceManager.instance.MenuObjects.transform.Find("Selection").gameObject.GetComponent<SelectionManager>().OnEnabled();

            InterfaceManager.instance.MainMenuActive(InterfaceManager.instance.MenuObjects);
        }

        public void MenuPickedActive()
        {
            InterfaceManager.instance.MenuPicked.SetActive(!InterfaceManager.instance.MenuPicked.activeSelf);
        }

        public void MenuInfosActive()
        {
            InterfaceManager.instance.MainMenuActive(InterfaceManager.instance.MenuInfos);
        }

        public void MenuShortcutsActive()
        {
            InterfaceManager.instance.Params.SetActive(false);
            InterfaceManager.instance.MainMenuActive(InterfaceManager.instance.MenuShortcuts);
        }

        public void MenuGeneticActive()
        {
            InterfaceManager.instance.MainMenuActive(InterfaceManager.instance.MenuGenetic);
        }

        public void UpdateInfoDisplayCells()
        {
            MorphoTools.GetFigureManager().UpdateInfoCells();
        }

        public void ManageInfoDisplayCells()
        {
            MorphoTools.GetFigureManager().DisplayInfoCells(MorphoTools.GetPickedManager().clickedCells);
        }

        public void ClearInfoDisplayCells()
        {
            MorphoTools.GetFigureManager().ClearInfoCells();
        }

        public void MenuScenarioActive()
        {
            //	if (MorphoTools.getCurrentDataSet().scenario != null) MorphoTools.getCurrentDataSet().scenario.CheckScenarios();
            InterfaceManager.instance.MainMenuActive(InterfaceManager.instance.MenuMovie);
        }

        public void MenuGroupsActive()
        {
            InterfaceManager.instance.MainMenuActive(InterfaceManager.instance.MenuGroup);
            if (InterfaceManager.instance.MenuGroup.activeSelf)
                MorphoTools.GetDataset().tissue.updateHeight();
        }

        public void MenuSegmentationActive()
        {
            InterfaceManager.instance.MainMenuActive(InterfaceManager.instance.MenuSegmentation);
        }

        public void MenuSimulationActive()
        {
            if (InterfaceManager.instance.MenuSimulation.activeSelf)
            {
                MorphoTools.GetPickedManager().ClearSelectedCellsMenu();
            }
            else
            {
                MorphoTools.GetPickedManager().AddSelectedCellsMenu();
            }
            InterfaceManager.instance.MainMenuActive(InterfaceManager.instance.MenuSimulation);
        }
        
        public void resetRotation()
        {
            MorphoTools.GetTransformationsManager().ResetTransformation();
        }

        public void downloadAllMeshes()
        { DataLoader.instance.onDownloadAllMeshes(); }

        public void downloadAllMeshesCurrentChannel()
        { DataLoader.instance.onDownloadAllMeshesCurrentChannel(); }

        public void saveRotationAt()
        { MorphoTools.GetTransformationsManager().saveRotationAt(); }

        public void saveRotation()
        { MorphoTools.GetTransformationsManager().saveRotation(); }

        public void clearRotation()
        { MorphoTools.GetTransformationsManager().ClearRotation(); }

        public void NextChannel()
        {
            MorphoTools.GetDataset().NextChannel();
        }

        public void PreviousChannel()
        {
            MorphoTools.GetDataset().PreviousChannel();
        }

        //Show or hide the colormap scale
        public void DisplayColorMapScale()
        {
            if (InterfaceManager.instance.ColorMapScale.gameObject.activeSelf)
                InterfaceManager.instance.ColorMapScale2.gameObject.SetActive(true);
            else
                InterfaceManager.instance.ColorMapScale.gameObject.SetActive(true);
        }

        public void refreshDatasetInfos()
        {
            MorphoTools.GetInformations().Refresh();
        }

        public void UploadDatasetInfos()
        {
            MorphoTools.GetInformations().Upload();
        }

        public void UploadNewInfos()
        {
            MorphoTools.GetInformations().uploadNewEmptyInfos();
        }

        public void UploadNewSelection()
        {
            MorphoTools.GetInformations().uploadNewSelectionInfos();
        }

        public void CloseNewInfos()
        {
            MorphoTools.GetInformations().closeNewInfos();
        }

        public void ShowX(bool show)
        {
            MorphoTools.GetFigureManager().ShowX(show);
        }

        public void ShowY(bool show)
        {
            MorphoTools.GetFigureManager().ShowY(show);
        }

        public void ShowZ(bool show)
        {
            MorphoTools.GetFigureManager().ShowZ(show);
        }

        public void UpdateXTextFigureAxis(string text)
        {
            MorphoTools.GetFigureManager().UpdateXTextFigureAxis(text);
        }

        public void UpdateXReverseTextFigureAxis(string text)
        {
            MorphoTools.GetFigureManager().UpdateXReverseTextFigureAxis(text);
        }

        public void UpdateYTextFigureAxis(string text)
        {
            MorphoTools.GetFigureManager().UpdateYTextFigureAxis(text);
        }

        public void UpdateYReverseTextFigureAxis(string text)
        {
            MorphoTools.GetFigureManager().UpdateYReverseTextFigureAxis(text);
        }

        public void UpdateZTextFigureAxis(string text)
        {
            MorphoTools.GetFigureManager().UpdateZTextFigureAxis(text);
        }

        public void UpdateZReverseFigureAxis(string text)
        {
            MorphoTools.GetFigureManager().UpdateZReverseFigureAxis(text);
        }

        public void ShowLegendInfoText()
        {
            MorphoTools.GetFigureManager().ShowLegendInfoText();
        }

        public void ScaleFigureAxis(float scale)
        {
            MorphoTools.GetFigureManager().ScaleFigureAxis(scale);
        }

        public void PickCellsWithText()
        {
            MorphoTools.GetFigureManager().PickTextedCells();
        }

        public void ResetFigureAxis()
        {
            MorphoTools.GetFigureManager().ResetAxis();
        }

        public void ScaleFigureLegend(float scale)
        {
            MorphoTools.GetFigureManager().ScaleFigureLegend(scale);
        }

        public void ShowRotationAxis()
        {
            MorphoTools.GetFigureManager().ShowRotationAxis();
        }

        public void ColorAxisUsingField(int choice)
        {
            MorphoTools.GetFigureManager().ColorAxisUsingField(choice);
        }

        public void UseShortcutNameFigure()
        {
            MorphoTools.GetFigureManager().HandleShortcutName();
        }

        public void createNewDatasetInfos()
        {
            MorphoTools.GetInformations().createNewInfos();
        }

        public void createNewDatasetInfosSelection()
        {
            MorphoTools.GetInformations().createNewInfosSelection();
        }

        public void downloadCommonDataset()
        {
            MorphoTools.GetInformations().downloadCommonDataset();
        }

        public void SelectDatasetBridge(int v)
        {
            MorphoTools.GetInformations().onSelectThisBridge(v);
        }

        public void onSelectDataset(int p)
        {
            MorphoTools.GetInformations().onSelectCommonDataset(p);
        }

        public void onSelectOtherBridge(int v)
        {
            MorphoTools.GetInformations().onSelectOtherBridge(v);
        }

        public void openInfosMenuCalcul()
        {
            MorphoTools.GetInformations().openMenuCalcul();
        }

        public void onSelectOtherInfos(int v)
        {
            MorphoTools.GetInformations().onSelectOtherInfos(v);
        }

        public void addInfosFromDataset()
        {
            MorphoTools.GetInformations().addInfosFromDataset();
        }

        public void cleanInfosToCompare()
        {
            MorphoTools.GetInformations().cleanInfosToCompare();
        }

        public void AddQualitativeInfos()
        {
            MorphoTools.GetInformations().addQualitativeInfos();
        }

        public void OnSelectThisBridge(int v)
        {
            MorphoTools.GetInformations().onSelectThisBridge(v);
        }

        public void onChangeFonctionUnique(int v)
        {
            MorphoTools.GetInformations().onChangeFunctionUnique(v);
        }

        public void onChangeFonction(int v)
        {
            MorphoTools.GetInformations().onChangeFunction(v);
        }

        public void calculCompare()
        {
            MorphoTools.GetInformations().calculCompare();
        }

        public void onSroll(float x)
        {
            MorphoTools.GetDataset().tissue.onSroll(x);
        }

        public void resetAndClear()
        {
            MorphoTools.GetGeneticManager().resetAndClear();
        }

        public void massiveCuration()
        {
            MorphoTools.GetGeneticManager().massiveCurration();
        }

        public void onActiveAll(bool v)
        {
            MorphoTools.GetGeneticManager().onActiveAll(v);
        }

        public void aniSearch(string name)
        {
            MorphoTools.GetGeneticManager().search(name);
        }

        public void onSrollAniseed(float x)
        {
            MorphoTools.GetGeneticManager().onSroll(x);
        }

        public void ShowOrHideChannel()
        {
            MorphoTools.GetDataset().ShowOrHideCurrentChannel();
        }

        public void TransferPicked()
        {
            MorphoTools.GetDataset().TransferPicked();
        }

        public void changeSubMenuByGenes()
        {
            MorphoTools.GetGeneticManager().changeSubMenuBy("Genes");
        }

        public void changeSubMenuByCells()
        {
            MorphoTools.GetGeneticManager().changeSubMenuBy("Cells");
        }

        public void changeSubMenuByStages()
        {
            MorphoTools.GetGeneticManager().changeSubMenuBy("Stages");
        }

        public void OpenMorphoHelp()
        {
#if UNITY_WEBGL
        Application.ExternalEval("window.open(\"https://morphonet.org/help_app \")");
#endif
        }

        public void canelAniseed()
        {
            MorphoTools.GetGeneticManager().cancel();
        }

        public void resetExpressionMutation()
        {
            MorphoTools.GetMutations().resetExpression();
        }

        public void onSrollMutation(float x)
        {
            MorphoTools.GetMutations().onSroll(x);
        }

        public void forceLineageClose(int i)
        {
            InterfaceManager.instance.lineage.forceLineageClose(i);
        }

        public void onCancelScenario()
        {
            Confirm.init();
            Confirm.setMessage("delete this scenario.");
            Confirm.yes.onClick.AddListener(() => { if (MorphoTools.GetRecorder() != null) MorphoTools.GetRecorder().onDeleteScenario(); });
            Confirm.confirm();
        }

        public void onShareScenario()
        {
            MorphoTools.GetRecorder().onShare();
        }

        public void onPlayScenario()
        {
            MorphoTools.GetRecorder().onPlay();
        }

        public void onRecordScenario()
        {
            MorphoTools.GetRecorder().onRecord();
        }

        public void onSaveScenario()
        {
            MorphoTools.GetRecorder().onSaveScenario();
        }

        public void onFindSisters()
        {
            MorphoTools.GetPickedManager().ShowSisters();
        }

        public void ScaleTextInfo(float scale)
        {
            MorphoTools.GetFigureManager().scaleTextInfo(scale);
        }

        public void SetFontSize(float size)
        {
            MorphoTools.GetFigureManager().SetFontSize(size);
        }

        public void onBackScenario()
        {
            MorphoTools.GetRecorder().backToScenario();
        }

        public void onScenarioScrollBack()
        {
            MorphoTools.GetRecorder().scrollBarAction();
        }

        public void onScenarioScrenshot()
        {
            MorphoTools.GetRecorder().screenshot();
        }

        public void onScenarioPanel()
        {
            MorphoTools.GetRecorder().ShowPanel();
        }

        public void onEndToggle(bool value)
        {
            MorphoTools.GetRecorder().onToggleText(value);
        }

        public void onEndToggleTimeScreenshot(bool value)
        {
            MorphoTools.GetRecorder().onToggleTime(value);
        }

        public void onEndEditText(string text)
        {
            MorphoTools.GetRecorder().StoreCustomScreenshotText(text);
        }

        public void onScenarioCreate()
        {
            MorphoTools.GetRecorder().createScenario();
        }

        public void GeneticMenuClicked()
        {
            MorphoTools.GetGeneticManager().onClickMenuGenetic();
        }

        public void ClearAllColorsInfo()
        {
            MorphoTools.GetInformations().Start_clearAllColors();
        }

        public void GroupMenuClicked()
        {
            MorphoTools.GetDataset().tissue.onMenuClick();
        }

        public void InverseSetPicking()
        {
            MorphoTools.GetPickedManager().InverseSelection();
        }

        public void DownLoadInfosOnClick()
        {
            if (infos_dl == null && InterfaceManager.instance.MenuInfos.activeSelf && MorphoTools.GetDataset().id_dataset != 0)
                StartCoroutine(MorphoTools.GetInformations().start_Download_Correspondences());
        }

        public void ResetSetPicking()
        {
            MorphoTools.GetPickedManager().ResetSelection();
        }

        public void ShowSetPicking(bool all_times)
        {
            MorphoTools.GetPickedManager().ShowAllCell(all_times);
        }

        public void HideSetPicking(bool all_times)
        {
            MorphoTools.GetPickedManager().HideClickedCell(all_times);
        }

        public void ShowNeighboursPicking()
        {
            MorphoTools.GetPickedManager().ShowNeigbhors();
        }

        public void ShowSistersPicking()
        {
            MorphoTools.GetPickedManager().ShowSisters();
        }

        public void onScatterTypeChange(int type)
        {
            MorphoTools.GetScatter().OnScatterTypeChange(type);
        }

        public void onScatterChangeExplodeFactor(float value)
        {
            MorphoTools.GetScatter().onChangeExplodeFactor(value);
        }

        //Callbacks from scene to update cut
        public void Xmin()
        { MorphoTools.GetTransformationsManager().Xmin(); }

        public void Xmax()
        { MorphoTools.GetTransformationsManager().Xmax(); }

        public void Ymin()
        { MorphoTools.GetTransformationsManager().Ymin(); }

        public void Ymax()
        { MorphoTools.GetTransformationsManager().Ymax(); }

        public void Zmin()
        { MorphoTools.GetTransformationsManager().Zmin(); }

        public void Zmax()
        { MorphoTools.GetTransformationsManager().Zmax(); }

        public void onFixedPlan(bool v)
        {
            MorphoTools.GetTransformationsManager().OnFixedPlan(v);
        }

        public void search()
        {
            string searchtext = InterfaceManager.instance.SearchField.text.ToString();
            if (searchtext != "")
            {
                List<Cell> cellsSearched = new List<Cell>();
                Cell cell = MorphoTools.GetDataset().getCell(searchtext, false);

                if (cell == null)
                    cell = MorphoTools.GetDataset().getCell(searchtext + ",0", false);

                if (cell == null)
                    cell = MorphoTools.GetDataset().getCell(MorphoTools.GetDataset().CurrentTime + "," + searchtext + ",0", false);

                //Search By String informatiopn described in Infos
                if (cell == null && MorphoTools.GetInformations() != null)
                {
                    string[] searchTexts = searchtext.Split(',');

                    foreach (Correspondence cor in MorphoTools.GetInformations().Correspondences)
                    {
                        if (cor.datatype == "string" || cor.datatype == "text")
                        {
                            for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
                            {
                                foreach (Cell cellAtT in MorphoTools.GetDataset().CellsByTimePoint[t])
                                {
                                    string cellInfo = cellAtT.getInfos(cor.id_infos);
                                    foreach (string s in searchTexts)
                                    {
                                        string trimedText = s.Trim();
                                        string trimedTextCopy = trimedText;

                                        //probably a better way to do this, but for now it will be ok
                                        if (trimedText.EndsWith("_") || trimedText.EndsWith("*"))
                                        {
                                            trimedTextCopy = trimedText.Remove(trimedText.Length - 1, 1);
                                        }

                                        string[] cell_values = trimedTextCopy.Split('.');

                                        if (cell_values.Length > 1)
                                        {
                                            while (cell_values[1].Length < 4)
                                            {
                                                cell_values[1] = $"0{cell_values[1]}";
                                            }

                                            trimedTextCopy = $"{cell_values[0]}.{cell_values[1]}";
                                        }

                                        if (!trimedText.EndsWith("_") && !trimedText.EndsWith("*"))
                                        {
                                            trimedTextCopy += "_";
                                        }

                                        if (trimedText.EndsWith("*"))
                                        {
                                            trimedTextCopy += "*";
                                        }

                                        if (trimedText.EndsWith("_"))
                                        {
                                            trimedTextCopy += "_";
                                        }

                                        if (trimedText != "" && trimedTextCopy != "_" && ((cellInfo.Length >= trimedText.Length && cellInfo.Substring(0, trimedText.Length) == trimedText) || (cellInfo.Length >= trimedTextCopy.Length && cellInfo.Substring(0, trimedTextCopy.Length) == trimedTextCopy)))
                                        {
                                            cellsSearched.Add(cellAtT);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    cellsSearched.Add(cell);
                }
                if (cellsSearched.Count > 0)
                {
                    //First We Look if there is a cell a this current time
                    bool isACellAtT = false;
                    foreach (Cell ce in cellsSearched)
                        if (ce != null && ce.t == MorphoTools.GetDataset().CurrentTime)
                            isACellAtT = true;
                    if (isACellAtT)
                    {//We only show cell at this current time
                        foreach (Cell ce in cellsSearched)
                            if (ce.t == MorphoTools.GetDataset().CurrentTime)
                                MorphoTools.GetPickedManager().AddCellSelected(ce, false);
#if !UNITY_WEBGL
                        MorphoTools.SendCellsPickedToLineage();
#endif
                    }
                    else
                    { //We Look for the min time founded
                        int minTimeFounded = MorphoTools.GetDataset().MaxTime;
                        foreach (Cell ce in cellsSearched)
                            if (ce != null)
                                minTimeFounded = Mathf.Min(minTimeFounded, ce.t);
                        InterfaceManager.instance.setTime(minTimeFounded); //We MOve Time point
                        foreach (Cell ce in cellsSearched)
                            if (ce != null && ce.t == minTimeFounded)
                                MorphoTools.GetPickedManager().AddCellSelected(ce, false);
                        //cellsearched = true;//WHAT FOR ??
#if !UNITY_WEBGL
                        MorphoTools.SendCellsPickedToLineage();
#endif
                    }
                    if (MorphoTools.GetInformations() != null)
                        MorphoTools.GetInformations().describe();
                }
            }
        }

        public void ShowShaderOptions()
        {
            if (!InterfaceManager.instance.ShaderOptionsMenu.activeSelf)
            {
                InterfaceManager.instance.ShaderOptionsMenu.SetActive(true);
            }
        }

        public void ToggleAdvancedShaderMenu()
        {
            if (!InterfaceManager.instance.ShaderAdvancedOptionsMenu.activeSelf)
            {
                InterfaceManager.instance.ShaderAdvancedOptionsMenu.SetActive(true);
            }
            else
            {
                InterfaceManager.instance.ShaderAdvancedOptionsMenu.SetActive(false);
            }
        }

        public void DisplayAdvancedShaderMenu(bool value)
        {
            InterfaceManager.instance.ShaderAdvancedOptionsMenu.SetActive(value);
        }

        public void DisplayShaderOptionsMenu()
        {
            InterfaceManager.instance.ShaderOptionsMenu.SetActive(true);
        }

        public void HideShaderOptionsMenu()
        {
            InterfaceManager.instance.ShaderOptionsMenu.SetActive(false);
        }

        
    }
}