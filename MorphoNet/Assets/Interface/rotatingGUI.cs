using MorphoNet;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotatingGUI : MonoBehaviour
{
    [SerializeField]
    private float _RotateSpeed = -90;

    private RectTransform parentTransform;
    // Start is called before the first frame update
    void Start()
    {
        try
        {
            parentTransform = gameObject.GetComponent<RectTransform>();
        }catch(Exception e)
        {
            MorphoDebug.LogError("ERROR : could not get rotating GUI element rect transform  : " + e);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (parentTransform != null)
        {
            Vector3 newRotation = parentTransform.rotation.eulerAngles;
            newRotation.z = Time.deltaTime * _RotateSpeed;
            parentTransform.Rotate(newRotation);
        }
    }
}
