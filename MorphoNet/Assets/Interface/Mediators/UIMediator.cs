using AssemblyCSharp;
using MorphoNet.Interaction;
using MorphoNet.UI.XR;
using System;
using System.Collections.Generic;

using UnityEngine;

namespace MorphoNet.UI
{
    /// <summary>
    /// Handle commands to and from the UI. Mostly receiving commands from the <see cref="InteractionMediator"/>.
    /// Mostly sending commands to and reading from the datasets.
    /// </summary>
    public class UIMediator
    {
        /// <summary>
        /// <see cref="ISubMediator"/> that received commands from this <see cref="UIMediator"/> and notify it of its actions.
        /// </summary>
        private List<ISubMediator> _SubMediators;

        /// <summary>
        /// The <see cref="SetsManager"/> used to get informations from data set and to which send data set related commands.
        /// </summary>
        private SetsManager _DataSetManager;

        /// <summary>
        /// The <see cref="InteractionMediator"/> this <see cref="UIMediator"/> exanges commands and informations with.
        /// </summary>
        private InteractionMediator _InteractionMediator;

        public UIMediator(List<ISubMediator> subMediators, SetsManager dataSetManager)
        {
            _SubMediators = subMediators;
            _SubMediators.ForEach(subMediator => subMediator.RegisterParentMediator(this));

            _DataSetManager = dataSetManager;
        }

        public Quaternion FetchCurrentDatasetRotation()
        {
            return _DataSetManager.DataSet.transform.rotation;
        }

        public float FetchCurrentDatasetScale()
        {
            return _DataSetManager.DataSet.transform.localScale.x; // All axes should have the same scale.
        }

        public void ResetDatasetRotation()
        {
            _DataSetManager.DataSet.TransformationsManager.ResetRotation();
        }

        public void ResetDatasetScaling()
        {
            _DataSetManager.DataSet.TransformationsManager.ResetScale();
        }

        public void ResetDatasetScattering()
        {
            MorphoTools.GetScatter().ResetScattering();
        }

        public void SetInteractionMediator(InteractionMediator mediator)
            => _InteractionMediator = mediator;

        public bool CanInteractWithViewer()
            => InterfaceManager.instance.lineage_viewer is null
                || InterfaceManager.instance.CanInteractWithViewer(0);

        internal void AddCommandModifier(CommandModifier modifier)
            => _InteractionMediator.AddModifier(modifier);

        internal void RemoveCommandModifier(CommandModifier modifier)
            => _InteractionMediator.RemoveModifier(modifier);

        internal void DeactivateAllModifiers()
            => _InteractionMediator.DeactivateAllModifiers();

        internal void ResetXRCommandModifiers()
            => _InteractionMediator.ResetXRModifiersToDefault();

        internal void TriggerSelectionWithCollision()
        {
            RemoveCommandModifier(CommandModifier.XRRaySelection);
            AddCommandModifier(CommandModifier.XRCollisionSelection);
            _InteractionMediator.SelectionMethod = CommandModifier.XRCollisionSelection;
        }

        internal void TriggerSelectionWithRay()
        {
            RemoveCommandModifier(CommandModifier.XRCollisionSelection);
            AddCommandModifier(CommandModifier.XRRaySelection);
            _InteractionMediator.SelectionMethod = CommandModifier.XRRaySelection;
        }

        internal CommandModifier GetCurrentSelectionMethod()
        {
            return _InteractionMediator.SelectionMethod;
        }

        #region Cutting Plane

        internal void ActivateClippingPlane() => _InteractionMediator.ActivateOrUpdateClippingPlane();

        internal void DeactivateClippingPlane() => _InteractionMediator.DeactivateClippingPlane();

        internal void ResetClippingPlanePose(bool updateClip = false) => _InteractionMediator.ResetClippingPlanePose(updateClip);

        #endregion Cutting Plane

        #region Side ToolBar

        // TODO: restructur interaction manager to avoid singleton.

        public event Action<String> OnCellSelectionInfoLabeUpdate;

        internal void InvertCellsSelection()
        {
            if (InteractionManager.instance != null)
                InteractionManager.instance.InverseSetPicking();
        }

        internal void ClearCellsSelection()
        {
            if (InteractionManager.instance != null)
                InteractionManager.instance.ResetSetPicking();
        }

        internal void ShowSelectedCells()
        {
            if (InteractionManager.instance != null)
                InteractionManager.instance.ShowSetPicking(false);
        }

        internal void HideSelectedCells()
        {
            if (InteractionManager.instance != null)
                InteractionManager.instance.HideSetPicking(false);
        }

        /// <summary>
        /// Ask the <see cref="InterfaceManager"/> to go to the given timestamp. Then update the submediator UI.
        /// </summary>
        internal void GoToTimestamp(int timestamp)
        {
            InterfaceManager.instance.ScrollTo(timestamp);
            _SubMediators.ForEach(sub => sub.ChangeTimestamp(timestamp));
        }

        /// <summary>
        /// Ask the <see cref="InterfaceManager"/> to go to the given timestamp. Then update the submediator UI.
        /// </summary>
        internal void ChangeFeedbackText(string text)
        {
            _SubMediators.ForEach(sub => {
                if (sub is XR.XRMediator mediator)
                    mediator.ChangeFeedbackText(text);
            });
        }

        /// <summary>
        /// Ask the <see cref="InterfaceManager"/> to go to the previous timestamp. Then update the submediator UI.
        /// </summary>
        internal void GoToPreviousTimestamp()
        {
            InterfaceManager.instance.scrollPrevious();
            _SubMediators.ForEach(sub => sub.ChangeTimestamp(InterfaceManager.instance.scrolledVal));
        }

        /// <summary>
        /// Ask the <see cref="InterfaceManager"/> to go to the next timestamp. Then update the submediator UI.
        /// </summary>
        internal void GoToNextTimestamp()
        {
            InterfaceManager.instance.scrollNext();
            _SubMediators.ForEach(sub => sub.ChangeTimestamp(InterfaceManager.instance.scrolledVal));
        }

        internal void SelectCellsSiblings()
        {
            if (InteractionManager.instance != null)
                InteractionManager.instance.onFindSisters();
        }

        internal Cell GetLastSelectedCell() => _InteractionMediator.LastSelectedCell;

        internal void GiveCellsRandomColors()
        {
            // TODO: Selection manager is in the top MENU. Change this when a menu refactoring will be done.
            if (_DataSetManager.DataSet!= null)
                _DataSetManager.DataSet.selection_manager.ApplyFreeSelectionToEachSelected();
        }

        #endregion Side ToolBar

        #region Advance Selection

        public void ApplySelectionToPickedObjects()
        {
            SelectionManager selectionManager = _DataSetManager.DataSet.selection_manager;
            selectionManager.applySelection();
        }

        public void PreviousSelection()
        {
            SelectionManager selectionManager = _DataSetManager.DataSet.selection_manager;
            selectionManager.PreviousSelectionClick();
        }

        public void NextSelection()
        {
            SelectionManager selectionManager = _DataSetManager.DataSet.selection_manager;
            selectionManager.NextSelectionClick();
        }

        public void NextFreeSelection()
        {
            SelectionManager selectionManager = _DataSetManager.DataSet.selection_manager;
            selectionManager.pickFreeSelection();
        }

        public int GetSelectionValue()
        {
            SelectionManager selectionManager = _DataSetManager.DataSet.selection_manager;
            return selectionManager.SelectionValue;
        }

        public int CountObjectsWithCurrentSelection()
        {
            SelectionManager selectionManager = _DataSetManager.DataSet.selection_manager;
            return selectionManager.CountCellsWithCurrentSelection();
        }

        public int CountObjectsWithAnySelection()
        {
            SelectionManager selectionManager = _DataSetManager.DataSet.selection_manager;
            return selectionManager.CountCellsWithAnySelection();
        }

        public int CountObjectsWithoutSelection()
        {
            SelectionManager selectionManager = _DataSetManager.DataSet.selection_manager;
            return selectionManager.CountCellsWithoutSelection();
        }

        public int CountObjectsWithMultipleSelection()
        {
            SelectionManager selectionManager = _DataSetManager.DataSet.selection_manager;
            return selectionManager.CountCellsWithMultipleSelections();
        }

        #endregion Advance Selection
    }
}