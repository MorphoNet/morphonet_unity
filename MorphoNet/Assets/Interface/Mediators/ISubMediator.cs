namespace MorphoNet.UI
{
    /// <summary>
    /// Common interface that all UI Mediator should implement.
    /// Should be used by the <see cref="UIMediator"/> to trigger actions in sub-mediators.
    /// </summary>
    public interface ISubMediator
    {
        /// <summary>
        /// Register the given <paramref name="mediator"/> as parent of this <see cref="ISubMediator"/>.
        /// </summary>
        /// <param name="mediator">The mediator to use as parent.</param>
        public void RegisterParentMediator(UIMediator mediator);

        /// <summary>
        /// Change the currently display dataset name for <paramref name="newName"/>.
        /// </summary>
        /// <param name="newName">The new name to display.</param>
        public void ChangeCurrentDataSetName(string newName);

        /// <summary>
        /// Change the currently displayed HPF for <paramref name="value"/>.
        /// </summary>
        /// <param name="value">The new value to display.</param>
        public void ChangeCurrentHoursPostFertilization(string value);
        
        /// <summary>
        /// Change the currently displayed Dataset Timestamp for <paramref name="value"/>.
        /// </summary>
        /// <param name="value">The new value to display.</param>
        public void ChangeTimestamp(int value);
    }
}