using UnityEngine;

namespace MorphoNet.UI
{
    public interface IRawDataUI
    {
        void RawDataUpdate();

        void UpdateUIWithoutNotify(IRawDataUI source);

        bool GetCutMeshButtonValue();

        bool GetCuttingPlaneButtonValue();

        bool GetCuttingPlaneInverseValue();

        float GetPlaneSliderValue();

        int GetPlaneAxisValue();

        bool GetThinModeValue();

        float GetThicknessSliderValue();

        float GetThresholdFieldValue();

        float GetMaxThresholdFieldValue();

        float GetMinHistoFieldValue();

        float GetMaxHistoFieldValue();

        float GetMinCmapFieldValue();

        float GetMaxCmapFieldValue();

        //float GetTransparencyFieldValue();

        float GetTransparencySliderValue();

        float GetIntensityFieldValue();

        float GetIntensitySliderValue();

        Vector2 GetAlphaMinValue();

        Vector2 GetAlphaMaxValue();

        int GetColorBarDropDownValue();

        (float, float) GetColorBarModSliderValue();

        (float, float) GetHistoMinMaxSliderValue();

        int GetMinValue();

        int GetMaxValue();

        /*void InitSliders(
            (float min, float max, float value) xSlider,
            (float min, float max, float value) ySlider,
            (float min, float max, float value) zSlider
            );*/
    }
}