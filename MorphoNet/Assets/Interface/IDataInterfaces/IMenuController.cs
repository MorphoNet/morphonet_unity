using System;

namespace MorphoNet.UI
{
    public interface IMenuController<T>
    {
        public void RegisterUI(T rawDataUI);

        public void UnRegisterUI(T rawDataUI);

        public event Action<T> OnUIParametersChange;

        public void UpdateUIFrom(T source);
    }
}