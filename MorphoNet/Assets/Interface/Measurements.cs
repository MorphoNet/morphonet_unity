using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MorphoNet;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class LineMeasurement
{
    public GameObject position1;
    public GameObject position2;
    public Vector3 old_pos1;
    public Vector3 old_pos2;
    public LineRenderer LineRenderer;
    public Text distance_text;

    
    public LineMeasurement(GameObject pos1 , GameObject pos2, LineRenderer lineRender,Text text)
    {
        position1 = pos1;
        position2 = pos2;
        distance_text = text;
        LineRenderer = lineRender;
        old_pos1 = Vector3.zero;
        old_pos2 = Vector3.zero;
    }

    public Vector2 WorldToCanvasPosition(Canvas canvas, RectTransform canvasRect, Camera camera, Vector3 position)
    {
        Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(camera, position);
        Vector2 result;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRect, screenPoint, canvas.renderMode == RenderMode.ScreenSpaceOverlay ? null : camera, out result);

        return canvas.transform.TransformPoint(result);
    }
    public void DisplayText()
    {
        Camera main_cam = Camera.main;
        InterfaceManager interfaceManager = InterfaceManager.instance;
         Vector3 initialPosition = (position1.transform.position+position2.transform.position)/2f;
         Vector2 screenPosition = new Vector2();

        distance_text.gameObject.SetActive(true);
        
        Canvas canvas = InterfaceManager.instance.canvas.GetComponent<Canvas>();
        RectTransform tCanvas = canvas.GetComponent<RectTransform>();
        
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            tCanvas,
            RectTransformUtility.WorldToScreenPoint(main_cam, initialPosition), main_cam, out screenPosition);
        distance_text.transform.localPosition = screenPosition; // Shift text down
        distance_text.fontSize = 14;
        distance_text.font = interfaceManager.default_font;

        RawImages raw = MorphoTools.GetRawImages();
        float[] voxel_size = raw.VoxelSize;
        Vector3 measurement_1_image_pos =
            position1.transform.position / InterfaceManager.instance.initcanvasScale;
        Vector3 measurement_2_image_pos =
            position2.transform.position / InterfaceManager.instance.initcanvasScale;
        
        int distance = (int)(Vector3.Distance(measurement_1_image_pos, measurement_2_image_pos)/MorphoTools.GetTransformationsManager().CurrentScale.x);

        string text = distance + " voxels";
        if (voxel_size.Length > 2)
        {
            if (voxel_size[0] > 0 && voxel_size[1] > 0 && voxel_size[2] > 0)
            {
                Vector3 final_point_1 = new Vector3(measurement_1_image_pos.x * voxel_size[0],
                    measurement_1_image_pos.y * voxel_size[1], measurement_1_image_pos.z * voxel_size[2]);
                Vector3 final_point_2 = new Vector3(measurement_2_image_pos.x * voxel_size[0],
                    measurement_2_image_pos.y * voxel_size[1], measurement_2_image_pos.z * voxel_size[2]);
                int physical_distance = (int)(Vector3.Distance(final_point_1, final_point_2)/MorphoTools.GetTransformationsManager().CurrentScale.x);
                text += "\n(= " + physical_distance + " physical unit)"; // change physical unit to real unity if we know it
            }
        }
        
        distance_text.text = text;
        distance_text.lineSpacing = 1.5f;
        distance_text.horizontalOverflow = HorizontalWrapMode.Overflow;
        distance_text.verticalOverflow = VerticalWrapMode.Overflow;
        distance_text.color = Color.white;
        distance_text.alignment = TextAnchor.MiddleCenter;
        RectTransform distance_text_rtransform = distance_text.GetComponent<RectTransform>();
        distance_text_rtransform.sizeDelta = new Vector2(100, 200);
        distance_text.material = interfaceManager.font_material;
            
    }
    
    public void UpdateLine()
    {
        if (old_pos1 != position1.transform.position || old_pos2 != position2.transform.position)
        {
            LineRenderer.SetPosition(0,
                position1.transform.position); //x,y and z position of the starting point of the line
            LineRenderer.SetPosition(1, position2.transform.position); //x,y and z position of the end point of the line
            DisplayText();
        }
        
        old_pos1 = position1.transform.position;
        old_pos2 = position2.transform.position;
    }
}
public class Measurements : MonoBehaviour
{

    private Color measure_color = new Color(1f, 0.1451f,0f);
    public GameObject empty_object;
    public List<LineMeasurement> lines;
    private KeyCode MesureKey = KeyCode.M;
    private KeyCode DeleteKey = KeyCode.D;
    public List<GameObject> MeasurementPoints;
    public GameObject MeasurementsContainer;
    public GameObject sphere_example;
    public GameObject MeasurementPanels;
    public bool show_measurement;
    public Material LineMat;
    public void showHideMeasurement()
    {
        if (show_measurement)
        {
            MeasurementsContainer.SetActive(false);
            MeasurementPanels.SetActive(false);
            show_measurement = false;
            foreach (LineMeasurement line in lines)
            {
                line.LineRenderer.gameObject.SetActive(false);
                line.distance_text.gameObject.SetActive(false);
            }

            InterfaceManager.instance.ButtonMeasurement.text = "Show";
        }
        else
        {
            if (lines == null)
            {
                lines = new List<LineMeasurement>();
            }
            MeasurementsContainer.SetActive(true);
            show_measurement = true;
            MeasurementPanels.SetActive(true);
            foreach (LineMeasurement line in lines)
            {
                line.LineRenderer.gameObject.SetActive(true);
                line.distance_text.gameObject.SetActive(true);
            }
            InterfaceManager.instance.ButtonMeasurement.text = "Hide";
        }
    }
    public void SyncMeasurementContainer()
    {
        MeasurementsContainer.transform.rotation = MorphoTools.GetTransformationsManager().CurrentRotation;
        MeasurementsContainer.transform.localScale = MorphoTools.GetTransformationsManager().CurrentScale;
        MeasurementsContainer.transform.position = MorphoTools.GetTransformationsManager().CurrentPosition;
        if (lines.Count > 0){
            foreach (LineMeasurement line in lines)
            {
                line.UpdateLine();
            }
        }
            
    }
    public void ClearMeasurements()
    {
        foreach (GameObject measure in MeasurementPoints)
        {
            DestroyImmediate(measure);
        }
        MeasurementPoints.Clear();
        foreach (LineMeasurement line in lines)
        {
            DestroyImmediate(line.LineRenderer.gameObject);
            DestroyImmediate(line.distance_text.gameObject);
        }
        lines.Clear();
    }

    public void DeletePoint()
    {
        RaycastHit hitInfo = new RaycastHit();
        bool hiting = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
        if (hiting)
        {
            GameObject clicked = hitInfo.transform.gameObject;
            List<int> to_remove = new List<int>();
            for (int i=0;
            i < lines.Count; i++)
            {
                LineMeasurement line = lines[i];
                if (line.LineRenderer.gameObject == clicked || line.distance_text.gameObject == clicked ||
                    line.position1 == clicked || line.position2 == clicked)
                {
                    DestroyImmediate(line.LineRenderer.gameObject);
                    DestroyImmediate(line.distance_text.gameObject);
                    to_remove.Add(i);
                }
            }

            foreach (int i in to_remove)
            {
                lines.RemoveAt(i);
            }
            to_remove.Clear();
            for (int i = 0; i < MeasurementPoints.Count;  i++)
            {
                GameObject point = MeasurementPoints[i];
                if (clicked == point)
                { 
                    to_remove.Add(i);
                    DestroyImmediate(point);
                    
                }
            }
            foreach (int i in to_remove)
            {
                MeasurementPoints.RemoveAt(i);
            }
        }
    }
    
    public void DeleteLastPoint()
    {
        if (MeasurementPoints.Count > 0){
            GameObject last_point = MeasurementPoints.Last();
            List<int> to_remove = new List<int>();
            for (int i=0;
                 i < lines.Count; i++)
            {
                LineMeasurement line = lines[i];
                if (line.LineRenderer.gameObject == last_point || line.distance_text.gameObject == last_point ||
                    line.position1 == last_point || line.position2 == last_point)
                {
                    DestroyImmediate(line.LineRenderer.gameObject);
                    DestroyImmediate(line.distance_text.gameObject);
                    to_remove.Add(i);
                }
            }
            foreach (int i in to_remove)
            {
                lines.RemoveAt(i);
            }
            to_remove.Clear();
            foreach (GameObject point in MeasurementPoints)
            {
                if (last_point == point)
                {
                    MeasurementPoints.Remove(point);
                    DestroyImmediate(point);
                    break;
                }
            }
        }
    }
    
    
    public void CreateSphereAtMousePosition()
    {
        Camera main_cam = Camera.main;
        Vector3 input_mouse_pos = Input.mousePosition;
        Ray ray = main_cam.ScreenPointToRay(input_mouse_pos);
        RaycastHit outHit;
        if (Physics.Raycast(ray, out outHit))
        {
            Vector3 world_pos = outHit.point;
            GameObject point = Instantiate(sphere_example, MeasurementsContainer.transform);
            point.transform.position = world_pos;
            point.SetActive(true);
            MeasurementPoints.Add(point);
        }

        if (MeasurementPoints.Count > 1)
        {
            for (int i = 0; i < MeasurementPoints.Count; i++)
            {
                GameObject p1 = MeasurementPoints[i];
                if (i + 1 < MeasurementPoints.Count)
                {
                    GameObject p2 = MeasurementPoints[i + 1];
                    bool to_add = true;
                    foreach (LineMeasurement line in lines)
                    {
                        if ((line.position1 == p1 && line.position2 == p2) ||
                            (line.position1 == p2 && line.position2 == p1))
                        {
                            to_add = false;
                        }
                    }

                    if (to_add)
                    {
                        LineRenderer current_line = Instantiate(empty_object,MeasurementsContainer.transform).AddComponent<LineRenderer>();
                        Text distance_text = Instantiate(empty_object,InterfaceManager.instance.canvas.transform).AddComponent<Text>();
                        distance_text.gameObject.AddComponent<Old_MoveMenu>();

                        current_line.material = LineMat;
                        current_line.startColor = measure_color;
                        current_line.endColor = measure_color;
                        current_line.startWidth = 0.03f;
                        current_line.endWidth = 0.03f;
                        current_line.positionCount = 2;
                        current_line.useWorldSpace = true;
                        lines.Add(new LineMeasurement(MeasurementPoints[i], MeasurementPoints[i + 1], current_line,
                            distance_text));
                    }
                }
            }
  
            
//For drawing line in the world space, provide the x,y,z values


            
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (show_measurement)
        {
            SyncMeasurementContainer();
            if (!Input.GetMouseButton(0) && !Input.GetMouseButton(1))
            {
                if (Input.GetKeyDown(MesureKey))
                {
                    CreateSphereAtMousePosition();

                }
                else
                {
                    if (Input.GetKeyDown(DeleteKey))
                    {
                        DeletePoint();
                    }
                }
            }
        } 
    }
}
