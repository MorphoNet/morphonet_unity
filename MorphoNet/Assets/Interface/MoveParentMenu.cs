﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace MorphoNet
{
    public class MoveParentMenu : EventTrigger, IPointerEnterHandler, IPointerExitHandler
    {
        private bool dragging;
        private Vector3 backup_start;
        private Vector3 start_mousePosition;
        private Vector3 start_dragPosition;
        public bool is_fixed;
        public bool can_move = true;

        public bool hovered = false;
        private Button SelfButton;

        private static float CLICK_MOVE_DEADZONE = 6f;

        public void FixInSpace()
        {
            is_fixed = true;
        }

        public void UnfixInSpace()
        {
            is_fixed = false;
        }

        private void Start()
        {
            dragging = false;
            Canvas.willRenderCanvases += canvasUpdate;
            backup_start = transform.parent.localPosition;
            is_fixed = false;
            SelfButton = gameObject.GetComponent<Button>();
        }

        // Update is called once per frame
        public void canvasUpdate()
        {
            if (dragging)
            {
                Vector3 localPoint = Input.mousePosition - start_mousePosition;
                localPoint.x /= Screen.width / InterfaceManager.instance.canvasWidth;
                localPoint.y /= Screen.height / InterfaceManager.instance.canvasHeight;
                transform.parent.localPosition = start_dragPosition + localPoint;

                if (transform.parent.localPosition != start_dragPosition
                    && (Input.mousePosition - start_mousePosition).magnitude > CLICK_MOVE_DEADZONE)
                {
                    if (!InterfaceManager.instance.MovingMenus.Contains(transform.parent.GetChild(1).gameObject))
                        InterfaceManager.instance.MovingMenus.Add(transform.parent.GetChild(1).gameObject);
                }
            }
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            dragging = true;
            start_mousePosition = Input.mousePosition;
            start_dragPosition = transform.parent.localPosition;
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            dragging = false;
            StartCoroutine(delayUnlockMenu());
        }

        public IEnumerator delayUnlockMenu()
        {
            yield return new WaitForSeconds(0.3f);
            InterfaceManager.instance.MovingMenus.Remove(transform.parent.GetChild(1).gameObject);
        }

        public void StartDragging()
        {
            if (can_move)
            {
                dragging = true;
                start_mousePosition = Input.mousePosition;
                start_dragPosition = transform.parent.localPosition;
            }
        }

        public void StopDragging()
        {
            dragging = false;
        }

        public void getBack()
        {
            //    is_fixed = false;
            //transform.localPosition = backup_start;
        }
    }
}