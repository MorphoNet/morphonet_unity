﻿using MorphoNet.Extensions.Unity;
using MorphoNet.UI.Desktop;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Button = UnityEngine.UI.Button;
using Screen = UnityEngine.Screen;

namespace MorphoNet
{
    public class InterfaceManager : MonoBehaviour
    {
        public static InterfaceManager instance = null;

        private void Awake()
        {
            if (instance == null)
                instance = this;
            else
                Destroy(gameObject);
        }

        public List<GameObject> HelpButtons;
        public Text ButtonMeasurement;
        public GameObject recordingPanel;

        public GameObject directPlot;
        public DirectPlot DirectPlot;

        public LayerMask IgnoredLayer;

        public Material SkyBoxMaterial;

        [Header("Version number")]
        public TextAsset VersionNumber;

        public TextAsset GitVersionNumber;

        [Header("Script references")]
        public Lineage lineage;

        public LineageViewer lineage_viewer;
        public BackgroundColorPicker background_picker;
        public BackgroundColorPicker lineage_background_picker;

        [Space(10)]
        [Header("Canvas values")]
        public Font default_font;

        public Material font_material;

        //Canvas Scale
        public float canvasScale = 1F;

        //public float initcanvasScale = 0.01392758F;//Standard Scale for the Canvas
        public float initcanvasScale = 0.01367409f;//Standard Scale for the Canvas
        public float initCanvasDatasetScale = 0.01367409f;//Standard Scale for the Canvas
        public float canvasHeight = 950;
        public float initCanvasHeight = 950;
        public float canvasWidth = 1688;//Default Exprort Value
        public float initCanvasWidth = 1688;//Default Exprort Value
        public GameObject canvas;

        [Space(10)]
        [Header("Time bar")]
        public GameObject TimeBar; //Main Time Bar

        public GameObject BoutonLeft;
        public GameObject BoutonRight;
        public int scrolledVal; //Valeur de la scrollbar temporel
        public GameObject scrollbarTime;
        public GameObject textTime;
        public GameObject PreviousStep;
        public GameObject NexStep;

        [Space(10)]
        [Header("Embryo geometry modification")]
        public Text ScatterFactor;

        public Vector3 ZommCutPlan;
        public Slider SliderFixedPlan;
        public Slider SliderXMin;
        public Slider SliderXMax;
        public Slider SliderYMin;
        public Slider SliderYMax;
        public Slider SliderZMin;
        public Slider SliderZMax;

        public InputField Manual_X_Translate_Field;
        public InputField Manual_Y_Translate_Field;
        public InputField Manual_Z_Translate_Field;
        public InputField Manual_X_Rotate_Field;
        public InputField Manual_Y_Rotate_Field;
        public InputField Manual_Z_Rotate_Field;
        public InputField Manual_Zoom_Field;
        //Embryo cut

        [Space(10)]
        [Header("Menus")]
        public GameObject MenuDescriptDuringLoad;

        public GameObject MenuShortcuts;
        public GameObject MenuCells;
        public GameObject MenuGenetic;
        public ImagesMenu MenuImages;
        public GameObject MenuObjects;
        public GameObject MenuLoadSelection;
        public GameObject MenuShare;
        public GameObject MenuInfos;
        public GameObject MenuCommon;
        public GameObject MenuCalcul;
        public GameObject MenuColorBar;

        public DataSetMenu MenuDataSet;

        public GameObject MenuGroup;
        public GameObject scrollbar_menugroup;
        public GameObject MenuMutant;
        public GameObject scrollbar_menumutant;
        public GameObject MenuDeregulations;
        public GameObject default_group;
        public GameObject MenuRender;
        public GameObject MenuChannel;
        public GameObject NextChannelArrow;
        public GameObject PrevChannelArrow;
        public GameObject buttonTransferChannel;
        public GameObject MenuColor;
        public GameObject MenuAction;
        public GameObject MenuPicked;
        public GameObject MenuCurration;
        public GameObject MenuMutants;
        public GameObject MenuSegmentation;
        public GameObject MenuSimulation;
        public List<GameObject> MovingMenus;
        public GameObject MenuMovie;
        public GameObject MenuScenario;
        public GameObject MenuActions;
        public GameObject MenuClicked;
        public GameObject Params;

        public GameObject AxisFigure;
        public GameObject FigureParams;
        public GameObject LegendFigure;
        public GameObject InfoPicked;
        public GameObject template_comment;
        public List<GameObject> snap_back;

        [Space(10)]
        private bool _BackupGenetic = false;

        private bool _BackupGroups = false;
        private bool _BackupMovie = false;
        private bool _BackupDataset = false;
        private bool _BackupImages = false;
        private bool _BackupInfos = false;
        private bool _BackupObjects = false;
        private bool _BackupSegmentation = false;
        private bool _BackupSimulation = false;
        private bool _BackupLineage = false;

        public bool backupDone = false;
        public bool minimized = false;

        [Header("Buttons")]
        public GameObject buttonGenetic;

        public GameObject buttonGroups;
        public GameObject buttonMovie;
        public GameObject buttonDataset;
        public GameObject buttonImages;
        public GameObject buttonInfos;
        public GameObject buttonObjects;
        public GameObject buttonLineage;
        public GameObject buttonClicked;

        public GameObject BoutonDataset;
        public GameObject BoutonImages;
        public GameObject BoutonInfos;
        public GameObject BoutonObjects;
        public GameObject BoutonShortcuts;
        public GameObject BoutonHelp;
        public GameObject BoutonAniseed;
        public GameObject BoutonMovie;
        public GameObject BoutonGroups;
        public GameObject lineage_button;
        public GameObject BoutonLineageViewer;
        public GameObject BoutonSettings;
        public GameObject rawImageChannelNext;
        public GameObject rawImageChannelPrevious;
        public GameObject buttonSegmentation;
        public GameObject buttonSimulation;

        [Space(10)]
        [Header("Buttons and menus containers")]
        public GameObject containerCurrations;

        public GameObject containerGenetic;
        public GameObject containerGroups;
        public GameObject containerMovie;
        public GameObject containerDataset;
        public GameObject containerImages;
        public GameObject containerInfos;
        public GameObject containerObjects;
        public GameObject containerSegmentation;
        public GameObject containerSimulation;
        public GameObject containerLineage;

        [Space(10)]
        [Header("Framerate")]
        public Text Framerate;

        private int _FrameCounter = 0;
        private float _TimeCounter = 0.0f;
        private float _LastFramerate = 0.0f;
        private readonly float _RefreshTime = 0.5f;
        private int _Last_fps = 0;

        [Space(10)]
        [Header("Recording")]
        public Text Description;

        public GameObject PanelRecorder;

        public GameObject default_scenar;
        public GameObject default_action;
        public GameObject scrollbar_actionO;
        public Scrollbar scrollbar_action;
        public GameObject loading_text;
        public GameObject hologramO;
        public Camera ShotCamera;
        public Toggle hologram;

        [Space(10)]
        [Header("Zoom")]
        public float init_data_scale = -1f;

        public Text zoom_display;
        public GameObject zoom_display_window;
        private bool _Modifying_zoom = false;
        private Coroutine _Zooming_stop;

        [Space(10)]
        [Header("Group")]
        public GameObject isGO;

        public GameObject deployGO;
        public GameObject colorizeGO;
        public GameObject reployGO;
        public GameObject nameGO;

        [Space(10)]
        [Header("Data streaming")]
        public Slider loadingTime_Range;

        public Slider loadingTime_Cache;
        public Text range_window_size;
        public Text range_window_cache;



        [Space(10)]
        [Header("Shader rendering")]
        public Text moreHelp;

        public float shaderScale = 0.2f;
        public string shaderType = "stain";
        public List<string> shaderTypes;
        public float shaderSmoothness = 0.0f;
        public Text shaderSmoothness_display;

        public float defaultShaderSmoothness = 0.0f;
        public float defaultShaderSmoothness_backup = 0.0f;
        public bool defaultShaderUseFresnel = false;
        public bool defaultShaderUseFresnel_backup = false;
        public float defaultShaderFresnelPower = 2f;
        public float defaultShaderFresnelPower_backup = 2f;
        public float defaultShaderEmission = 0f;
        public float defaultShaderEmission_backup = 0f;
        public float defaultShaderRoughness = 0f;
        public float defaultShaderRoughness_backup = 0f;
        public float defaultShaderMetallic = 0f;
        public float defaultShaderTransparency = 1f;
        public float defaultShaderMetallic_backup = 0f;
        public float r_fresnel_color = 0.2479083f;
        public float r_fresnel_color_backup = 0.2479083f;
        public float g_fresnel_color = 1f;
        public float g_fresnel_color_backup = 1f;
        public float b_fresnel_color = 0.04245281f;
        public float b_fresnel_color_backup = 0.04245281f;
        public float a_fresnel_color = 1.0f;
        public float a_fresnel_color_backup = 1f;

        public float r_native_color = 1.0f;
        public float r_native_color_backup = 1.0f;
        public float g_native_color = 1.0f;
        public float g_native_color_backup = 1.0f;
        public float b_native_color = 1.0f;
        public float b_native_color_backup = 1.0f;
        public float a_native_color = 1.0f;
        public float a_native_color_backup = 1.0f;

        //shader rework
        private bool default_picked = true;

        public Text ColorMenuShaderName;

        public GameObject ShaderAdvancedOptionsMenu;
        public GameObject ShaderOptionsMenu;

        public Renderer preview_material;
        public Renderer preview_material_small;
        public Renderer preview_material_picked_small;
        public Renderer preview_material_multiselect_small;
        public InputField a_color_inf;
        public Slider a_color_sli;
        public InputField b_color_inf;
        public Slider b_color_sli;
        public InputField g_color_inf;
        public Slider g_color_sli;
        public InputField r_color_inf;
        public Slider r_color_sli;

        public InputField a_color_native_inf;
        public Slider a_native_color_sli;
        public InputField b_color_native_inf;
        public Slider b_native_color_sli;
        public InputField g_color_native_inf;
        public Slider g_native_color_sli;
        public InputField r_color_native_inf;
        public Slider r_native_color_sli;

        public Slider smoothness;
        public Toggle useFresnel;
        public Slider FPOwer;
        public Slider Emission;
        public Slider Metallic;

        public GameObject UpdateDefaultPanel;
        public Text ScaleTextInfoT;
        public Text ScaleFontT;
        public Button ShowTextInfoButton;

        public Material lineage_color_holder;
        public Material text_color_holder;

        public GameObject axis_figure;
        public GameObject figure_params;
        public TextMeshPro x_axis_text;
        public TextMeshPro y_axis_text;
        public TextMeshPro z_axis_text;
        public TextMeshPro x_axis_text_reverse;
        public TextMeshPro y_axis_text_reverse;
        public TextMeshPro z_axis_text_reverse;
        public Button ShortCutNamesButton;
        public Text scale_text;
        public GameObject x_axis;
        public GameObject x_axis_reverse;
        public GameObject y_axis;
        public GameObject y_axis_reverse;
        public GameObject z_axis;
        public GameObject z_axis_reverse;
        public InputField x_text;
        public InputField x_text_r;
        public InputField y_text;
        public InputField y_text_r;
        public InputField z_text;
        public InputField z_text_r;
        public Dropdown color_axis;
        public Slider scale_slid;
        public Text scale_legend;
        public Slider scale_text_slid;
        public Toggle showX;
        public Toggle showY;
        public Toggle showZ;

        public Material axisMat;
        public Material defaultXMat;
        public Material defaultYMat;
        public Material defaultZMat;

        public GameObject display_info_template;
        public GameObject display_transfo_container;
        public Text DisplayLineButtonText;
        public Text FloatFeedback;
        public Button show_legend_button;
        public Button ShowMovieButton;
        public Button ShowRenderingButton;
        public Button ShowFigureInfoButton;
        public Button show_axis_button;
        public GameObject legend_panel;

        public GameObject ButtonBackToMenu;

        public GameObject RenderButtonPanel;
        public GameObject RecordButtonPanel;
        public bool ShowScenario = false;
        public bool ShowRendering = false;
        public GameObject MenuRendering;

        //shader rework : moved from changeColorSelection
        public GameObject MultiColorParameters;

        [Space(10)]
        [Header("Shaders - Objects menu")]
        public ChangeColorSelection ObjectsColorManager;

        public Slider ObjEmission;
        public Slider ObjFresnel;
        public Slider ObjPower;
        public Slider ObjSmoothness;
        public Slider ObjMetallic;

        //play and pause button for time loading
        public Button play_loading;

        public Button pause_loading;

        public Button ButtonVR;
        public Button ExpandVRViewButton;
        public GameObject VRViewTexture;

        [Space(10)]
        [Header("Infos Menu UI")]
        public GameObject MenuInfosConnections;

        public Slider ConnectionMinThreshold;
        public Slider ConnectionMaxThreshold;
        public InputField ConnectionMinV;
        public InputField ConnectionMaxV;
        public Button ApplyConnection;
        public Button CancelConnection;
        public Dropdown ColormapDropdown;
        public Button RefreshPropertiesButton;

        [Space(10)]
        [Header("Console")]
        public GameObject Console;
        public Text ConsoleContents;
        public Button ConsoleOpenButton;

        [Space(10)]
        [Header("Plot Interface")]
        public GameObject PluginsMenu;
        public GameObject PluginsHelpMenu;
        public ExamplePluginCategory ExamplePluginCategory;
        public ExamplePluginOptions ExamplePluginOptions;
        public ExampleHelpWindow ExamplePluginHelp;

        public GameObject MorphingSliderPanel;

        public GameObject MeasurementPanel;
        public GameObject MeasurementMenu;

        //Start or pause the loading  , and update UI
        public void PlayLoading(bool val)
        {
            if (MorphoTools.GetDataset() != null)
            {
                if (!val)
                    MorphoDebug.Log("Loading paused at time : " + MorphoTools.GetDataset().LoadingTime,1);
                MorphoTools.GetDataset().allow_time_download = val;
            }
            play_loading.gameObject.SetActive(!val);
            pause_loading.gameObject.SetActive(val);
        }

    public void AskBackToMenu()
    {
        StartCoroutine(BackToMenu());
    }
    /// <summary>
    /// Load the main menu scene
    /// </summary>
    public IEnumerator BackToMenu()
    {
        if (InterfaceManager.instance.lineage_viewer != null)
        {
            StartCoroutine(MorphoTools.sendPythonCommandPyinstaller("end_lineage", ""));
            if(instance.lineage_viewer.gameObject.activeSelf)
                instance.lineage_viewer.StopStandaloneLineage();
        }

        yield return new WaitUntil(() => !lineage_viewer.isAvaible || !lineage_viewer.isActive);
        if (LoadParameters.instance.use_plot)
            if (directPlot != null)
                if (directPlot.GetComponent<DirectPlot>() != null)
                    StartCoroutine(directPlot.GetComponent<DirectPlot>().SendRestartRequest());
            SceneManager.Instance.LoadParameterScene();
        }

        public void ShowScenarioMenu()
        {
            ShowScenario = !ShowScenario;
            ShowMovieButton.GetComponentInChildren<Text>().text = ShowScenario ? "Hide" : "Show";
            MenuScenario.SetActive(ShowScenario);
        }

        public void ShowRenderingMenu()
        {
            ShowRendering = !ShowRendering;
            ShowRenderingButton.GetComponentInChildren<Text>().text = ShowRendering ? "Hide" : "Show";
            MenuRendering.SetActive(ShowRendering);
        }

        public void DisplayTransfoPanel()
        {
            display_transfo_container.SetActive(true);
            display_transfo_container.transform.Find("position").Find("position_x_input").GetComponent<InputField>().text =
                MorphoTools.GetTransformationsManager().CurrentPosition.x + "";
            display_transfo_container.transform.Find("position").Find("position_y_input").GetComponent<InputField>().text =
                MorphoTools.GetTransformationsManager().CurrentPosition.y + "";
            display_transfo_container.transform.Find("position").Find("position_z_input").GetComponent<InputField>().text =
                MorphoTools.GetTransformationsManager().CurrentPosition.z + "";
            display_transfo_container.transform.Find("rotate").Find("rotate_x_input").GetComponent<InputField>().text =
                MorphoTools.GetTransformationsManager().CurrentRotation.eulerAngles.x + "";
            display_transfo_container.transform.Find("rotate").Find("rotate_y_input").GetComponent<InputField>().text =
                MorphoTools.GetTransformationsManager().CurrentRotation.eulerAngles.y + "";
            display_transfo_container.transform.Find("rotate").Find("rotate_z_input").GetComponent<InputField>().text =
                MorphoTools.GetTransformationsManager().CurrentRotation.eulerAngles.z + "";
            display_transfo_container.transform.Find("zoom").Find("zoom_input").GetComponent<InputField>().text =
                MorphoTools.GetTransformationsManager().CurrentScale.x + "";
        }

        public void BackupShaderParameters()
        {
            defaultShaderSmoothness_backup = defaultShaderSmoothness;
            defaultShaderUseFresnel_backup = defaultShaderUseFresnel;
            defaultShaderFresnelPower_backup = defaultShaderFresnelPower;
            defaultShaderEmission_backup = defaultShaderEmission;
            defaultShaderRoughness_backup = defaultShaderRoughness;
            defaultShaderMetallic_backup = defaultShaderMetallic;
            r_fresnel_color_backup = r_fresnel_color;
            g_fresnel_color_backup = g_fresnel_color;
            b_fresnel_color_backup = b_fresnel_color;
            a_fresnel_color_backup = a_fresnel_color;
            r_native_color_backup = r_native_color;
            g_native_color_backup = g_native_color;
            b_native_color_backup = b_native_color;
            a_native_color_backup = a_native_color;
        }

        public void SynchroniseUIAndValue()
        {
            a_color_inf.text = (255 * a_fresnel_color).ToString();
            a_color_inf.CancelInvoke();
            b_color_inf.text = (255 * b_fresnel_color).ToString();
            b_color_inf.CancelInvoke();
            g_color_inf.text = (255 * g_fresnel_color).ToString();
            g_color_inf.CancelInvoke();
            r_color_inf.text = (255 * r_fresnel_color).ToString();
            r_color_inf.CancelInvoke();
            a_color_native_inf.text = (255 * a_native_color).ToString();
            a_color_native_inf.CancelInvoke();
            b_color_native_inf.text = (255 * b_native_color).ToString();
            b_color_native_inf.CancelInvoke();
            g_color_native_inf.text = (255 * g_native_color).ToString();
            g_color_native_inf.CancelInvoke();
            r_color_native_inf.text = (255 * r_native_color).ToString();
            r_color_native_inf.CancelInvoke();
            a_color_sli.value = (255 * a_fresnel_color);
            a_color_sli.CancelInvoke();
            r_color_sli.value = (255 * r_fresnel_color);
            r_color_sli.CancelInvoke();
            g_color_sli.value = (255 * g_fresnel_color);
            g_color_sli.CancelInvoke();
            b_color_sli.value = (255 * b_fresnel_color);
            b_color_sli.CancelInvoke();
            a_native_color_sli.value = (255 * a_native_color);
            a_native_color_sli.CancelInvoke();
            r_native_color_sli.value = (255 * r_native_color);
            r_native_color_sli.CancelInvoke();
            g_native_color_sli.value = (255 * g_native_color);
            g_native_color_sli.CancelInvoke();
            b_native_color_sli.value = (255 * b_native_color);
            b_native_color_sli.CancelInvoke();
            smoothness.value = defaultShaderSmoothness;
            smoothness.CancelInvoke();
            useFresnel.isOn = defaultShaderUseFresnel;
            useFresnel.CancelInvoke();
            FPOwer.value = defaultShaderFresnelPower;
            FPOwer.CancelInvoke();
            Emission.value = defaultShaderEmission;
            Emission.CancelInvoke();
            Metallic.value = defaultShaderMetallic;
            Metallic.CancelInvoke();
        }

        public void RestoreShaderParameters()
        {
            defaultShaderSmoothness = defaultShaderSmoothness_backup;
            defaultShaderUseFresnel = defaultShaderUseFresnel_backup;
            defaultShaderFresnelPower = defaultShaderFresnelPower_backup;
            defaultShaderEmission = defaultShaderEmission_backup;
            defaultShaderRoughness = defaultShaderRoughness_backup;
            defaultShaderMetallic = defaultShaderMetallic_backup;
            r_fresnel_color = r_fresnel_color_backup;
            g_fresnel_color = g_fresnel_color_backup;
            b_fresnel_color = b_fresnel_color_backup;
            a_fresnel_color = a_fresnel_color_backup;
            r_native_color = r_native_color_backup;
            g_native_color = g_native_color_backup;
            b_native_color = b_native_color_backup;
            a_native_color = a_native_color_backup;

            UpdatePreviewMaterial();
            SynchroniseUIAndValue();
            applyDefaultNewValues();
        }

        public void applyDefaultNewValues(bool close = true)
        {
            foreach (KeyValuePair<int, List<Cell>> time in MorphoTools.GetDataset().CellsByTimePoint)
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[time.Key])
                {
                    cell.updateShader();
                }
            }

            if (close)
                UpdateDefaultPanel.SetActive(false);
        }

        public void UpdatePreviewMaterial()
        {
            preview_material.materials[0].UpdateFloatProperty("_Glossiness", defaultShaderSmoothness);
            preview_material.materials[0].UpdateFloatProperty("_ActivateFrenel", defaultShaderUseFresnel ? 1f : 0f);
            preview_material.materials[0].UpdateFloatProperty("_FresnelPower", defaultShaderFresnelPower);
            preview_material.materials[0].UpdateFloatProperty("_Emission", defaultShaderEmission);
            preview_material.materials[0].UpdateFloatProperty("_Roughness", defaultShaderRoughness);
            preview_material.materials[0].UpdateFloatProperty("_Metallic", defaultShaderMetallic);
            preview_material.materials[0].UpdateColorProperty("_FresnelColor", new Color(r_fresnel_color, g_fresnel_color, b_fresnel_color, a_fresnel_color));
            preview_material.materials[0].UpdateColorProperty("_Color", new Color(r_native_color, g_native_color, b_native_color, a_native_color));

            preview_material_small.materials[0].UpdateFloatProperty("_Glossiness", defaultShaderSmoothness);
            preview_material_small.materials[0].UpdateFloatProperty("_ActivateFrenel", defaultShaderUseFresnel ? 1f : 0f);
            preview_material_small.materials[0].UpdateFloatProperty("_FresnelPower", defaultShaderFresnelPower);
            preview_material_small.materials[0].UpdateFloatProperty("_Emission", defaultShaderEmission);
            preview_material_small.materials[0].UpdateFloatProperty("_Roughness", defaultShaderRoughness);
            preview_material_small.materials[0].UpdateFloatProperty("_Metallic", defaultShaderMetallic);
            preview_material_small.materials[0].UpdateColorProperty("_FresnelColor", new Color(r_fresnel_color, g_fresnel_color, b_fresnel_color, a_fresnel_color));
            preview_material_small.materials[0].UpdateColorProperty("_Color", new Color(r_native_color, g_native_color, b_native_color, a_native_color));
        }

        public void OpenUpdateDefault()
        {
            default_picked = true;
            ColorMenuShaderName.text = "Default material";
            ActivateShaderUI(true, true);
            UpdateDefaultPanel.SetActive(true);
            MultiColorParameters.SetActive(false);
            preview_material.material = preview_material_small.material;
            BackupShaderParameters();
        }

        public void OpenUpdateSelected()
        {
            default_picked = false;
            ColorMenuShaderName.text = "Selected cell material";
            ActivateShaderUI(false, true);
            MultiColorParameters.SetActive(false);
            UpdateDefaultPanel.SetActive(true);
            preview_material.material = preview_material_picked_small.material;
            BackupShaderParameters();
        }

        public void OpenUpdateMultiSelected()
        {
            ColorMenuShaderName.text = "Multi-colored material";
            ActivateShaderUI(false, false);
            UpdateDefaultPanel.SetActive(true);
            MultiColorParameters.SetActive(true);
            preview_material.material = preview_material_multiselect_small.material;
            BackupShaderParameters();
        }

        public void UpdatePreviewMaterialSmooth()
        {
            preview_material.material = SetsManager.instance.Selected;
            preview_material_picked_small.material = SetsManager.instance.Selected;
        }

        //hide all UI material for modification
        public void ActivateShaderUI(bool toggle, bool keepSelect)
        {
            a_color_inf.transform.parent.gameObject.SetActive(toggle);
            b_color_inf.transform.parent.gameObject.SetActive(toggle);
            g_color_inf.transform.parent.gameObject.SetActive(toggle);
            r_color_inf.transform.parent.gameObject.SetActive(toggle);

            a_color_native_inf.transform.parent.gameObject.SetActive(toggle);
            b_color_native_inf.transform.parent.gameObject.SetActive(toggle);
            g_color_native_inf.transform.parent.gameObject.SetActive(toggle);
            r_color_native_inf.transform.parent.gameObject.SetActive(toggle);

            if (!toggle && keepSelect)
            {
                smoothness.transform.parent.gameObject.SetActive(true);
                Metallic.transform.parent.gameObject.SetActive(true);
            }
            else
            {
                smoothness.transform.parent.gameObject.SetActive(toggle);
                Metallic.transform.parent.gameObject.SetActive(toggle);
            }
            Emission.transform.parent.gameObject.SetActive(toggle);

            useFresnel.gameObject.SetActive(toggle);
            FPOwer.gameObject.SetActive(toggle);
        }

        public void UpdateRColor(float value)
        {
            r_color_inf.text = value + "";
            r_fresnel_color = value / 255;
            UpdatePreviewMaterial();
            applyDefaultNewValues(false);
        }

        public void UpdateNativeRColorInputfield(string value)
        {
            if (int.TryParse(value, out var end_value))
            {
                r_native_color_sli.value = end_value;
                r_native_color = end_value / 255;
                UpdatePreviewMaterial();
                applyDefaultNewValues(false);
            }
        }

        public void UpdateNativeRColor(float value)
        {
            r_color_native_inf.text = value + "";
            r_native_color = value / 255;
            UpdatePreviewMaterial();
            applyDefaultNewValues(false);
        }

        public void UpdateRColorInputfield(string value)
        {
            if (int.TryParse(value, out var end_value))
            {
                r_color_sli.value = end_value;
                r_fresnel_color = end_value / 255;
                UpdatePreviewMaterial();
                applyDefaultNewValues(false);
            }
        }

        public void UpdateGColor(float value)
        {
            g_color_inf.text = value + "";
            g_fresnel_color = value / 255;
            UpdatePreviewMaterial();
            applyDefaultNewValues(false);
        }

        public void UpdateGColorInputfield(string value)
        {
            if (int.TryParse(value, out var end_value))
            {
                g_color_sli.value = end_value;
                g_fresnel_color = end_value / 255;
                UpdatePreviewMaterial();
                applyDefaultNewValues(false);
            }
        }

        public void UpdateNativeGColor(float value)
        {
            g_color_native_inf.text = value + "";
            g_native_color = value / 255;
            UpdatePreviewMaterial();
            applyDefaultNewValues(false);
        }

        public void UpdateNativeGColorInputfield(string value)
        {
            if (int.TryParse(value, out var end_value))
            {
                g_native_color_sli.value = end_value;
                g_native_color = end_value / 255;
                UpdatePreviewMaterial();
                applyDefaultNewValues(false);
            }
        }

        public void UpdateAColor(float value)
        {
            a_color_inf.text = value + "";
            a_fresnel_color = value / 255;
            UpdatePreviewMaterial();
            applyDefaultNewValues(false);
        }

        public void UpdateAColorInputfield(string value)
        {
            if (int.TryParse(value, out var end_value))
            {
                a_color_sli.value = end_value;
                a_fresnel_color = end_value / 255;
                UpdatePreviewMaterial();
                applyDefaultNewValues(false);
            }
        }

        public void UpdateNativeAColor(float value)
        {
            a_color_native_inf.text = value + "";
            a_native_color = value / 255;
            UpdatePreviewMaterial();
            applyDefaultNewValues(false);
        }

        public void UpdateNativeAColorInputfield(string value)
        {
            if (int.TryParse(value, out var end_value))
            {
                a_native_color_sli.value = end_value;
                a_native_color = end_value / 255;
                UpdatePreviewMaterial();
                applyDefaultNewValues(false);
            }
        }

        public void UpdateBHColor(float value)
        {
            b_color_inf.text = value + "";
            b_fresnel_color = value / 255;
            UpdatePreviewMaterial();
            applyDefaultNewValues(false);
        }

        public void UpdateBColorInputfield(string value)
        {
            if (int.TryParse(value, out var end_value))
            {
                b_color_sli.value = end_value;
                b_fresnel_color = end_value / 255;
                UpdatePreviewMaterial();
                applyDefaultNewValues(false);
            }
        }

        public void UpdateNativeBHColor(float value)
        {
            b_color_native_inf.text = value + "";
            b_native_color = value / 255;
            UpdatePreviewMaterial();
            applyDefaultNewValues(false);
        }

        public void UpdateNativeBColorInputfield(string value)
        {
            if (int.TryParse(value, out var end_value))
            {
                b_native_color_sli.value = end_value;
                b_native_color = end_value / 255;
                UpdatePreviewMaterial();
                applyDefaultNewValues(false);
            }
        }

        public void CloseUpdateDefault()
        {
            UpdateDefaultPanel.SetActive(false);
            RestoreShaderParameters();
        }

        public void UpdateDefaultMetallic(float value)
        {
            if (default_picked)
            {
                defaultShaderMetallic = value;
                UpdatePreviewMaterial();
                applyDefaultNewValues(false);
            }
            else
            {
                SetsManager.instance.Selected.UpdateFloatProperty("_Metallic", value);
                UpdatePreviewMaterialSmooth();
            }
        }

        public void UpdateDefaultRoughness(float value)
        {
            defaultShaderRoughness = value;
            UpdatePreviewMaterial();
            applyDefaultNewValues(false);
        }

        public void UpdateDefaultEmission(float value)
        {
            defaultShaderEmission = value;
            UpdatePreviewMaterial();
            applyDefaultNewValues(false);
        }

        public void UpdateDefaultPower(float value)
        {
            defaultShaderFresnelPower = value;
            UpdatePreviewMaterial();
            applyDefaultNewValues(false);
        }

        public void UpdateDefaultSmoothness(float value)
        {
            if (default_picked)
            {
                defaultShaderSmoothness = value;
                UpdatePreviewMaterial();
                applyDefaultNewValues(false);
            }
            else
            {
                SetsManager.instance.Selected.UpdateFloatProperty("_Smoothness", value);
                UpdatePreviewMaterialSmooth();
            }
        }

        public void UpdateDefaultUseFresnell(bool value)
        {
            defaultShaderUseFresnel = value;
            UpdatePreviewMaterial();
            applyDefaultNewValues(false);
        }

        [Space(10)]
        [Header("Genetic")]
        public GameObject scrollbar_genetic;

        public InputField GeneName;

        [Space(10)]
        [Header("Dataset")]
        public Text datasetname;

        public Text hpf;

        [Space(10)]
        [Header("Lineage")]
        public Image loading_bar_lineage;

        public GameObject lineage_panel;
        public Text lineage_pourcentage;

        [Space(10)]
        [Header("RawImages references")]
        public Toggle meshesCut;

        public GameObject CubePlan;
        public GameObject CubeScale;
        public Renderer Cube;
        public GameObject XPlan;
        public GameObject YPlan;
        public GameObject ZPlan;
        public GameObject CurationButton;
        public Dropdown MapColor;
        public Text RawImageChannelText;

        [Space(10)]
        [Header("Custom time range")]
        public InputField min_input;

        public InputField max_input;
        public int min_custom_time_selection = 0;
        public int max_custom_time_selection = 0;
        public bool custom_time_is_active;
        public bool use_all_times;
        public Toggle All_times_slider;
        public Toggle range_times_slider;
        public Toggle cell_life_slider;
        public bool use_time_window;
        public Text time_display;
        public bool until_next_division;
        public GameObject custom_time_panel;
        public UnityEngine.UI.Extensions.RangeSlider range_slider;
        public Text custom_time_button_text;
        public GameObject time_range;
        public GameObject time_windows;

        [Space(10)]
        [Header("Opaque Backgrounds")]
        public bool are_menu_opaque = false;

        public List<Image> menus_background;

        [Header("Multi Labels Selection")]
        public GameObject example_object;

        public GameObject multi_label_grid;
        public GameObject multi_label_container;

        [Header("Others")]
        public GameObject loading;

        public InputField SearchField; //Search Input text
        public Text currenttextloaded; //Text draw for informations loading
        public Toggle allTimes;
        public RectTransform BoxSelectionArea;

        //Keyboard action
        private readonly float _KeyDelay = 0.2f;  //1 Second

        private float _TimePassed = 0f;

        public GameObject MorphoNetLogo;
        public GameObject spacebar_image;
        public GameObject leftalt_image;
        public Text transparencyText;
        public Text[] targetText;
        public GameObject bouton_layout_group;
        public static MaterialPropertyBlock individualColors;
        public Sprite NormalActive;
        public Sprite BlendActive;
        
        public GameObject background_color_picker;
        public GameObject buttonColormap;
        public Image ColorMapScale;
        public List<Text> ColorMapScalePercent;
        public Image ColorMapScale2;
        public List<Text> ColorMapScalePercent2;
        public GameObject Menu_time_propagate;

        [Space(10)]
        [Header("Sprites")]
        public Sprite LineageSprite;
        public Sprite LineageCrossedSprite;
        public Sprite SkimageSprite;

        public MeshRenderer this_selection_sphere;
        public List<MeshRenderer> all_selections_spheres;
        public Toggle keep_selected_labels;
        public void UpdateCustomTimePoints()
        {
            if (SetsManager.instance.DataSet.MinTime < SetsManager.instance.DataSet.MaxTime)
            {
                min_custom_time_selection = SetsManager.instance.DataSet.MinTime;
                min_input.text = min_custom_time_selection + "";
                max_custom_time_selection = SetsManager.instance.DataSet.MaxTime;
                max_input.text = max_custom_time_selection + "";
                range_slider.MaxValue = SetsManager.instance.DataSet.MaxTime;
                range_slider.HighValue = SetsManager.instance.DataSet.MaxTime;
                ;
                range_slider.MinValue = SetsManager.instance.DataSet.MinTime;
                range_slider.LowValue = SetsManager.instance.DataSet.MinTime;
                
            }
        }

        public void ApplyManualPosition()
        {
            if (float.TryParse(Manual_X_Translate_Field.text, out var x) &&
                float.TryParse(Manual_Y_Translate_Field.text, out var y))
            {
                MorphoTools.GetTransformationsManager().ApplyTransformationsManualPosition(new Vector3(x, y, 20));
            }
        }

        public void ApplyManualRotation()
        {
            if (float.TryParse(Manual_X_Rotate_Field.text, out var x)
                && float.TryParse(Manual_Y_Rotate_Field.text, out var y)
                && float.TryParse(Manual_Z_Rotate_Field.text, out var z))
            {
                MorphoTools
                    .GetTransformationsManager()
                    .ApplyTransformationsManualRotation(Quaternion.Euler(new Vector3(x, y, z)));
            }
        }

        public void ApplyManualZoom()
        {
            if (float.TryParse(Manual_Zoom_Field.text, out var x))
            {
                MorphoTools.GetTransformationsManager().ApplyTransformationsManualScale(x);
            }
        }

        public void UpdateManualTranslate(Vector3 translation)
        {
            Manual_X_Translate_Field.SetTextWithoutNotify(translation.x.ToString());
            Manual_Y_Translate_Field.SetTextWithoutNotify(translation.y.ToString());
            Manual_Z_Translate_Field.SetTextWithoutNotify(translation.z.ToString());
        }

        public void UpdateManualRotation(Vector3 rotation)
        {
            Manual_X_Rotate_Field.SetTextWithoutNotify(rotation.x.ToString());
            Manual_Y_Rotate_Field.SetTextWithoutNotify(rotation.y.ToString());
            Manual_Z_Rotate_Field.SetTextWithoutNotify(rotation.z.ToString());
        }

        public void ApplyDatasetTransfoToManualField()
        {
            UpdateManualTranslate(MorphoTools.GetTransformationsManager().CurrentPosition);
            UpdateManualRotation(MorphoTools.GetTransformationsManager().CurrentRotation.eulerAngles);
            UpdateManualScale(MorphoTools.GetTransformationsManager().CurrentScale);
        }

        public void UpdateManualScale(Vector3 zoom)
        {
            Manual_Zoom_Field.SetTextWithoutNotify(zoom.x.ToString());
        }

        public string GetTimeMethod()
        {
            var method = "normal";

            if (until_next_division)
                method = "life";
            else if (use_all_times)
                method = "all";
            else if (use_time_window)
                method = $"range*{min_custom_time_selection}*{max_custom_time_selection}";

            return method;
        }

        public bool isUntilDivision(Cell source_cell, Cell cell, bool direction)
        {
            var result = false;

            if (until_next_division)
            {
                if (source_cell == cell)
                {
                    return true;
                }

                Cell target = source_cell;

                if (direction)
                {
                    while (target.Daughters != null && target.Daughters.Count > 0 && target.Daughters.Count < 2)
                    {
                        target = target.Daughters[0];
                    }

                    result = (source_cell.t <= cell.t && cell.t <= target.t);
                }
                else
                {
                    while (target.Mothers != null && target.Mothers.Count > 0 && target.Mothers[0].Daughters != null && target.Mothers[0].Daughters.Count > 0 && target.Mothers[0].Daughters.Count < 2)
                    {
                        target = target.Mothers[0];
                    }

                    result = (source_cell.t >= cell.t && cell.t >= target.t);
                }
            }

            return result;
        }

        public bool isTimeModificationActive()
        {
            return
                until_next_division
                || use_all_times
                || use_time_window;
        }

        public bool isInCustomTimeRange(Cell cell, bool futur = true)
        {
            return
                (until_next_division
                    && isUntilDivision(cell, cell, futur))
                || (use_time_window
                    && cell.t <= max_custom_time_selection
                    && cell.t >= min_custom_time_selection)
                || (use_all_times
                    && cell.t <= MorphoTools.GetDataset().MaxTime
                    && cell.t >= MorphoTools.GetDataset().MinTime);
        }

        public bool isInCustomTimeRange(int timepoint)
        {
            return
                !until_next_division
                &&
                    ((use_time_window
                        && timepoint <= max_custom_time_selection
                        && timepoint >= min_custom_time_selection)
                    || (use_all_times
                        && timepoint <= MorphoTools.GetDataset().MaxTime
                        && timepoint >= MorphoTools.GetDataset().MinTime));
        }

        //apply a status to all button and menus needed
        public void hideButtonsAndMenus(bool value)
        {
            if (!backupDone)
            {
                _BackupGenetic = buttonGenetic.activeSelf;
                _BackupGroups = buttonGroups.activeSelf;
                _BackupMovie = buttonMovie.activeSelf;
                _BackupDataset = buttonDataset.activeSelf;
                _BackupImages = buttonImages.activeSelf;
                _BackupInfos = buttonInfos.activeSelf;
                _BackupObjects = buttonObjects.activeSelf;
                _BackupSegmentation = (MorphoTools.GetDataset().id_dataset == 0);
                _BackupSimulation = (MorphoTools.GetDataset().id_dataset == 0);
                _BackupLineage = buttonLineage.activeSelf;
                backupDone = true;
            }

            buttonGenetic.SetActive(value ? _BackupGenetic : value);
            buttonGroups.SetActive(value ? _BackupGroups : value);
            buttonMovie.SetActive(value ? _BackupMovie : value);
            buttonDataset.SetActive(value ? _BackupDataset : value);
            buttonImages.SetActive(value ? _BackupImages : value);
            buttonInfos.SetActive(value ? _BackupInfos : value);
            buttonObjects.SetActive(value ? _BackupObjects : value);
            buttonSegmentation.SetActive(value ? _BackupSegmentation : value);
            buttonSimulation.SetActive(value ? _BackupSimulation : value);
            buttonLineage.SetActive(value ? _BackupLineage : value);

            MenuObjects.SetActive(false);
            MenuDataSet.Fold();
            MenuInfos.SetActive(false);
            MenuImages.gameObject.SetActive(false);
            MenuShortcuts.SetActive(false);
            MenuGenetic.SetActive(false);
            MenuMovie.SetActive(false);
            MenuGroup.SetActive(false);
            MenuSegmentation.SetActive(false);
            MenuSimulation.SetActive(false);

            instance.HideUIMinimizedMode(value);
            PartialHideMinimizedMode.instance.HideUIMinimizedMode(value);
        }

        //Status on the website (bottom right miniminzed)
        public void goToMinimized()
        {
            minimized = true;
            hideButtonsAndMenus(false);
        }

        //Status on the website (whole page)
        public void goToMaximized()
        {
            minimized = false;
            hideButtonsAndMenus(true);
        }

        //Configure data streaming values
        public void updateLoadingRangeBoundaries()
        {
            DataSet dataSet = MorphoTools.GetDataset();

            if (dataSet.MaxTime - dataSet.MinTime <= 200)
            {
                dataSet.range_time_loading = false;
            }
            else
            {
                int new_max_value
                    = 8000 / (dataSet.MaxTime - 200) <= 0
                    ? 20
                    : 8000 / (dataSet.MaxTime - 200);

                loadingTime_Range.minValue = 0;
                loadingTime_Range.maxValue = dataSet.MaxTime - dataSet.MinTime;

                loadingTime_Cache.minValue = 0;
                loadingTime_Cache.maxValue = dataSet.MaxTime - dataSet.MinTime;

                loadingTime_Cache.value = 20;
                loadingTime_Range.value = new_max_value;

                dataSet.range_size = new_max_value;
                dataSet.range_cache = 20;
            }
        }

        //configure all shaders, changing smoothness
        public void UpdateShaderSmoothness(float new_value)
        {
            shaderSmoothness = new_value;
            shaderSmoothness_display.text = shaderSmoothness + "";

            SetsManager.instance.Default.UpdateFloatProperty("_Glossiness", shaderSmoothness);
            SetsManager.instance.Clippable.UpdateFloatProperty("_Glossiness", shaderSmoothness);
            foreach (KeyValuePair<int, List<Cell>> time in MorphoTools.GetDataset().CellsByTimePoint)
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[time.Key])
                    cell.updateShader();
            }
        }

        //Callback to modify the loading panel
        public void UpdateLineageLoading(bool state, int purcent)
        {
            lineage_panel.SetActive(state);
            loading_bar_lineage.fillAmount = purcent <= 0 ? 0 : purcent >= 100 ? 1 : purcent / 100;
            lineage_pourcentage.text = purcent + "%";
        }

        //Change the shaderType , then ask all cells who got this shader to update
        public void UpdateShaderType(int value)
        {
            shaderType = shaderTypes[value];
            foreach (KeyValuePair<int, List<Cell>> time in MorphoTools.GetDataset().CellsByTimePoint)
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[time.Key])
                {
                    if (cell.selection != null && cell.selection.Count > 1 && cell.selection.Count < 5)
                        cell.updateShader();
                }
            }
        }

        //Change the scale for multi select shaders , then ask all cells who got this shader to update
        public void ShaderScale(float new_scale)
        {
            shaderScale = new_scale;
            for (var i = 0; i < SetsManager.instance.BarSelection.Count; i++)
                SetsManager.instance.BarSelection[i].UpdateFloatProperty("_Scale", shaderScale);

            for (var i = 0; i < SetsManager.instance.StainSelection.Count; i++)
                SetsManager.instance.StainSelection[i].UpdateFloatProperty("_Scale", shaderScale);

            foreach (KeyValuePair<int, List<Cell>> time in MorphoTools.GetDataset().CellsByTimePoint)
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[time.Key])
                {
                    if (cell.selection != null && cell.selection.Count > 1 && cell.selection.Count < 5)
                        cell.updateShader();
                }
            }
        }

        //Start lineage loading panel hide
        public void HideLineageLoading()
        {
            StartCoroutine(DelayHide());
        }

        //Hide lineage panel, the delay is to make it smooths
        public IEnumerator DelayHide()
        {
            yield return new WaitForSecondsRealtime(1.0f);
            lineage_panel.SetActive(false);
            loading_bar_lineage.fillAmount = 0;
            lineage_pourcentage.text = "0%";
        }

        //GENERAL GUI BUTTONS
        private static ColorBlock _BlockBttnActive;

        private static ColorBlock _BlockImageActive;

        //Uniformize button style using a block of colors
        public static void setButtonColors(Button bttn)
        {
            bttn.colors = _BlockBttnActive;
        }

        //Uniformize image style using a color
        public static void setImageColors(Button bttn)
        {
            bttn.colors = _BlockImageActive;
        }

        //Show button is the same thing to go back to maximized on site
        public void ShowButtons()
        {
            goToMaximized();
        }

        //Hide button is the same thing to go back to minimized on site
        public void HideButtons()
        {
            goToMinimized();
        }

        //Open help from the menuhelp in interface

        //Update and draw famerate
        private void drawFrameRate()
        {
            if (_TimeCounter < _RefreshTime)
            {
                _TimeCounter += Time.deltaTime;
                _FrameCounter++;
            }
            else
            {
                //This code will break if you set your m_refreshTime to 0, which makes no sense.
                _LastFramerate = _FrameCounter / _TimeCounter;
                _FrameCounter = 0;
                _TimeCounter = 0.0f;
            }

            //TODO NOT REFRESH IOF PREIVOUS SAME
            var fps = (int)Math.Round(_LastFramerate);

            if (_Last_fps != fps)
            {
                Framerate.text = fps.ToString() + "fps ";

                if (fps > 50)
                    Framerate.color = Color.green;
                else if (fps < 20)
                    Framerate.color = Color.red;
                else
                    Framerate.color = Color.yellow;

                _Last_fps = fps;
            }
        }

        //Comment is the text above the timebar to display things (mainly loading status) to user
        public void setComment(string n)
        {
            if (currenttextloaded.text != n)
                currenttextloaded.text = n;

        }

        //Update the time time bar text from time point
        public void setTime(int t)
        {
            updateTextTime(t.ToString());
        }

        //Update Time Bar from Text
        public void updateTextTime(string textVal)
        {
            if (int.TryParse(textVal, out var parsedVal))
                scrolledVal = parsedVal;
            scrollTime();
        }

        //Centralize version
        public void scrollTime()
        {
            scrollbarTime.transform.GetComponent<Slider>().SetValueWithoutNotify(scrolledVal);  // -> GO TO updateScrollTime
            updateScrollTime(scrolledVal);
        }

        //Update Time Text From Scroll bar
        public void updateScrollTime(int newt)
        {
            scrolledVal = newt;
            textTime.GetComponent<InputField>().text = scrolledVal.ToString("0");

            DataSet ds = MorphoTools.GetDataset();

            if (ds != null)
            {
                ds.hpf.text = MorphoTools.ConvertTimeToHPF(scrolledVal);
                ds.CurrentTime = scrolledVal;

                if (!buttonSimulation.activeSelf)
                {
                    if (ds.RawImages != null && ds.RawImages.RawImagesAreVisible)
                        ds.RawImages.updateMeshShader();
                }
                else
                {
                    if (MorphoTools.GetPickedManager() != null)
                        MorphoTools.GetPickedManager().ClearSelectedCellsMenu();
                }
            }
        }

        public void UpdateScrollTimeSlider(Single newt)
        {
            scrolledVal = (int)newt;
            textTime.GetComponent<InputField>().text = scrolledVal.ToString("0");

            DataSet ds = MorphoTools.GetDataset();

            if (ds != null)
            {
                ds.hpf.text = MorphoTools.ConvertTimeToHPF(scrolledVal);
                ds.CurrentTime = scrolledVal;

                if (!buttonSimulation.activeSelf)
                {
                    if (ds.RawImages != null && ds.RawImages.RawImagesAreVisible)
                        ds.RawImages.updateMeshShader();
                }
                else
                {
                    if (MorphoTools.GetPickedManager() != null)
                        MorphoTools.GetPickedManager().ClearSelectedCellsMenu();
                }
            }
        }

        public void UpdateClearSelectedLabels(bool status)
        {
            keep_selected_labels.transform.Find("comment").GetChild(1).GetComponent<Text>().text =
                "Objects are " + (status?"":"un") + "selected after applying labels";
        }
        
        //FROM RIGHT BUTTON of the time bar, next time step
        public void scrollNext()
        {
            if (scrolledVal < MorphoTools.GetDataset().MaxTime)
                scrolledVal += 1;

            scrollTime();
        }

        /// <summary>
        /// If the given <paramref name="timestamp"/> is a valid timestamp,
        /// change the <see cref="scrolledVal"/> to its value. Then, update the <see cref="scrollTime(int)"/>.
        /// </summary>
        /// <param name="timestamp">The targeted timestamp.</param>
        public void ScrollTo(int timestamp)
        {
            if (timestamp >= MorphoTools.GetDataset().MinTime && timestamp <= MorphoTools.GetDataset().MaxTime)
                scrolledVal = timestamp;

            scrollTime();
        }

        //adapt time bar to match params of the dataset
        public void HideUIMinimizedMode(bool state)
        {
            if (TimeBar != null && MorphoTools.GetDataset().MinTime < MorphoTools.GetDataset().MaxTime)
                TimeBar.SetActive(state);

            if (SearchField != null)
                SearchField.gameObject.SetActive(false);

            if (BoutonLeft != null && MorphoTools.GetDataset().MinTime < MorphoTools.GetDataset().MaxTime)
                BoutonLeft.SetActive(state);

            if (BoutonRight != null && MorphoTools.GetDataset().MinTime < MorphoTools.GetDataset().MaxTime)
                BoutonRight.SetActive(state);

            if (scrollbarTime != null && MorphoTools.GetDataset().MinTime < MorphoTools.GetDataset().MaxTime)
                scrollbarTime.SetActive(state);

            if (textTime != null && MorphoTools.GetDataset().MinTime < MorphoTools.GetDataset().MaxTime)
                textTime.SetActive(state);
        }

        //FROM LEFT BUTTON of the time bar, previous time step
        public void scrollPrevious()
        {
            if (scrolledVal > MorphoTools.GetDataset().MinTime)
                scrolledVal -= 1;

            scrollTime();
        }

        //EMBRYO CUT
        //Call at initialisation
        //When received a API loading, and after storing the state of the buttons asked by api (session) , apply them
        public void API_hideButtons()
        {
            //On Start with a sepcific session
            if (SetsManager.instance.API_Buttons != null)
            {
                //MenuName -> true or false
                foreach (KeyValuePair<string, bool> pair in SetsManager.instance.API_Buttons)
                {
                    string key = pair.Key;
                    bool value = pair.Value;

                    if (key.Equals("movie"))
                    {
                        buttonMovie.SetActive(value);
                        containerMovie.SetActive(value);
                        _BackupMovie = value;
                    }

                    if (key.Equals("genetic"))
                    {
                        buttonGenetic.SetActive(value);
                        _BackupGenetic = value;
                        containerGenetic.SetActive(value);
                    }

                    if (key.Equals("dataset"))
                    {
                        buttonDataset.SetActive(value);
                        _BackupDataset = value;
                        containerDataset.SetActive(value);
                    }

                    if (key.Equals("images"))
                    {
                        buttonImages.SetActive(value);
                        _BackupImages = value;
                        containerImages.SetActive(value);
                        Debug.LogError("value: "+value);
                    }

                    if (key.Equals("group"))
                    {
                        buttonGroups.SetActive(value);
                        _BackupGroups = value;
                        containerGroups.SetActive(value);
                    }

                    if (key.Equals("infos"))
                    {
                        buttonInfos.SetActive(value);
                        _BackupInfos = value;
                        containerInfos.SetActive(value);
                    }

                    if (key.Equals("objects"))
                    {
                        buttonObjects.SetActive(value);
                        _BackupObjects = value;
                        containerObjects.SetActive(value);
                    }

                    _BackupSegmentation = (LoadParameters.instance.id_dataset == 0);
                    _BackupSimulation = (LoadParameters.instance.id_dataset == 0);
                }

                backupDone = true;
            }
        }

        public IEnumerator organizeButtons()
        {
            if (MenuDataSet != null)
            {
                yield return new WaitUntil(() => MorphoTools.GetDataset() != null);

                var activateRotationSave
                    = (LoadParameters.instance.id_owner == SetsManager.instance.IDUser || LoadParameters.instance.user_right < 1)
                    && !LoadParameters.instance.liveMode
                    && LoadParameters.instance.id_dataset > 0;

                MenuDataSet.InitButtons(activateRotationSave);
            }

            //Manage Gene DATABASE ACCESS , id_type will decide if we can access gene or no
            if (LoadParameters.instance.id_type == 2 || LoadParameters.instance.id_type == 3)
            {
                buttonGenetic.Activate();
                containerGenetic.Activate();
            }
            else
            {
                buttonGenetic.Deactivate();
                containerGenetic.Deactivate();
            }

            buttonGroups.Deactivate(); //Desactive at initialisation
            containerGroups.Deactivate();
        }

        //Menus havec a component that allow them to move : when moved they are stuck in place (so cant close them automatically)
        public void hideNotFixedMenus(GameObject MenuToHide, GameObject nothide)
        {
            // Special case for MenuShortcuts and MenuPicked -> TODO: correct this to avoid special cases.
            // TODO: this hiding system should be handled by the menu themselves. Cannot do it while all menu are not refactored.
            if (MenuToHide == MenuShortcuts || MenuToHide == MenuPicked)
            {
                Old_MoveMenu moveMenu = MenuToHide.transform.GetComponent<Old_MoveMenu>();

                if (moveMenu == null || moveMenu.is_fixed)
                    moveMenu.getBack();
                else if (MenuToHide != nothide)
                    MenuToHide.Deactivate();
                else
                    MenuToHide.SwitchActiveState();
            }
            else
            {
                if (MenuToHide != nothide)
                {
                    MoveMenu moveMenu = MenuToHide.transform.parent.GetComponent<MoveMenu>();

                    if (moveMenu != null && !moveMenu.is_fixed)
                        MenuToHide.Deactivate();
                }
                else
                {
                    MenuToHide.SwitchActiveState();
                }
            }
        }

        //Generic function for main menu , so it will choose "main menu" as the new opened menu, and close all of others if able to close (if not moved and fixed)
        public void MainMenuActive(GameObject mainmenu)
        {
            if (MovingMenus.Contains(mainmenu))
                return;

            hideSubMenus();

            if (mainmenu.activeSelf)
            {
                if (mainmenu == MenuShortcuts || mainmenu == MenuPicked)
                {
                    if (!mainmenu.transform.GetComponent<Old_MoveMenu>().is_fixed)
                    {
                        //Menu Is Moved -> Let him back
                        hideMenus(mainmenu);
                        mainmenu.SetActive(false); //Close Menu
                    }
                    else
                    {
                        mainmenu.SetActive(false); //Close Menu
                    }
                }
                else
                {
                    if (!mainmenu.transform.parent.GetComponent<MoveMenu>().is_fixed)
                    {
                        //Menu Is Moved -> Let him back
                        hideMenus(mainmenu);
                        mainmenu.SetActive(false); //Close Menu
                    }
                    else
                    {
                        mainmenu.SetActive(false); //Close Menu
                    }
                }
            }
            else
            {
                hideMenus(mainmenu);
            }
        }

        //Callbacks to stuck every specific menu
        //If a button is open, callback to restart the transition to minimized mode on the site
        private void checkEveryButtons()
        {
            if (buttonGenetic.activeSelf)
                minimized = false;

            if (buttonGroups.activeSelf)
                minimized = false;

            if (buttonMovie.activeSelf)
                minimized = false;

            if (buttonDataset.activeSelf)
                minimized = false;

            if (buttonInfos.activeSelf)
                minimized = false;

            if (buttonObjects.activeSelf)
                minimized = false;

            if (buttonSegmentation.activeSelf)
                minimized = false;

            if (buttonSimulation.activeSelf)
                minimized = false;

            if (buttonLineage.activeSelf)
                minimized = false;
        }

        //hide all menus except the given one
        public void hideMenus(GameObject nothide)
        {
            hideNotFixedMenus(MenuObjects, nothide);
            hideNotFixedMenus(MenuDataSet.Menu, nothide);
            hideNotFixedMenus(MenuInfos, nothide);
            hideNotFixedMenus(MenuImages.gameObject, nothide);
            hideNotFixedMenus(MenuShortcuts, nothide);
            hideNotFixedMenus(MenuGenetic, nothide);
            hideNotFixedMenus(MenuMovie, nothide);
            hideNotFixedMenus(MenuGroup, nothide);
            hideNotFixedMenus(MenuSegmentation, nothide);
            hideNotFixedMenus(MenuSimulation, nothide);
        }

        //when closing or opening menus, we make sure to close all submenus
        public void hideSubMenus()
        {
            MenuColor.SetActive(false);
            MenuLoadSelection.SetActive(false);
            MenuColorBar.SetActive(false);
            MenuCurration.SetActive(false);
            MenuCommon.SetActive(false);
            MenuCalcul.SetActive(false);
            MenuMutants.SetActive(false);
            MenuDeregulations.SetActive(false);
        }

        // Start is called before the first frame update
        private IEnumerator Start()
        {

            SkyBoxMaterial.SetColor("_Tint", new Color(0.251f, 0.757f, 0.906f));

            MenuShortcuts.transform.Find("VersionPanel").Find("version").gameObject.transform.GetComponent<Text>().text = MorphoTools.GetFullVersionNumber();
            DirectPlot = directPlot.GetComponent<DirectPlot>();
            ButtonVR.gameObject.Deactivate();
#if UNITY_STANDALONE_WIN
            ButtonVR.gameObject.Activate();
#endif

            ButtonBackToMenu = GameObject.Find("BackToMenu");
#if UNITY_WEBGL
         if (ButtonBackToMenu != null)
        {ButtonBackToMenu.SetActive(false);
         }
#else
            //recordingPanel.SetActive(false);
        if (ButtonBackToMenu != null)
        {
            ButtonBackToMenu.GetComponent<Button>().onClick.AddListener(() => { AskBackToMenu(); });
        }

#endif
        MovingMenus = new List<GameObject>();

            if (LoadParameters.instance.liveMode == true)
            {
                buttonMovie.SetActive(false);
                buttonInfos.SetActive(false);
            }

            //hideMenus(MenuShortcuts);
            hideSubMenus();
            MenuScenario.SetActive(false);
            MenuAction.SetActive(false);

            //	buttonDataset.GetComponent<Button>().onMouseOver
            API_hideButtons();

            if (RightManager.Instance.canAccessCuration && LoadParameters.instance.type != "lpy")
                buttonSegmentation.SetActive(true);
            else
            {
                buttonSegmentation.SetActive(false);
                buttonSegmentation.transform.parent.gameObject.SetActive(false);
            }
            
            if (LoadParameters.instance.id_dataset == 0 && LoadParameters.instance.type == "lpy")
                buttonSimulation.SetActive(true);
            else
                buttonSimulation.SetActive(false);

            //always hide lineage menu by default : show it in webGL with the little icon on lineage detect, and show the menu in standalone on lieange detect
            containerLineage.SetActive(false);
            buttonLineage.SetActive(false);

            loading.SetActive(true);
            buttonColormap.SetActive(RightManager.Instance.canLoadColormap);
            spacebar_image.SetActive(SystemInfo.operatingSystem.Contains("Windows"));
            leftalt_image.SetActive(!SystemInfo.operatingSystem.Contains("Windows"));
            canvasScale = canvas.transform.localScale.x;
            canvasWidth = canvas.GetComponent<RectTransform>().sizeDelta.x;
            canvasHeight = canvas.GetComponent<RectTransform>().sizeDelta.y;

            CubePlan.SetActive(false); //We hide at the begining the cube for raw data view
                                       //We hide all unecessarry button according the file infos downlaoded
                                       //SPACE
            canvas.transform.Find("MenuPicked").gameObject.transform.Find("Bouton layout").transform.Find("NeighborsSelect").gameObject.SetActive(false);

            //RESCALE BUTTONS AND MENU
            StartCoroutine(instance.organizeButtons()); //RESCALE BUTTONS AND MENU
            for (var i = 0; i < SetsManager.instance.BarSelection.Count; i++)
                SetsManager.instance.BarSelection[i].UpdateFloatProperty("_Scale", shaderScale);

            for (var i = 0; i < SetsManager.instance.StainSelection.Count; i++)
                SetsManager.instance.StainSelection[i].UpdateFloatProperty("_Scale", shaderScale);

            //GENERATE INVIDIDUAL COLOR
            individualColors = new MaterialPropertyBlock();

#if !UNITY_WEBGL
            //moreHelp.text = "More help on : morphonet.org/help_app";
#endif

#if !UNITY_EDITOR && (UNITY_IOS || UNITY_ANDROID)
         background_color_picker.SetActive(false);
#endif

            //GUI BUTTON COLORS
            _BlockBttnActive = new ColorBlock
            {
                normalColor = new Color(68f / 255f, 212f / 255f, 1f, 1f),//MORPHONET
                highlightedColor = new Color(68f / 255f, 212f / 255f, 1f, 0.5f),  //CYAN LIGHT
                pressedColor = Color.white,
                selectedColor = Color.black,
                disabledColor = Color.black,
                colorMultiplier = 1
            };

            _BlockImageActive = new ColorBlock
            {
                normalColor = Color.white,
                highlightedColor = new Color(68f / 255f, 212f / 255f, 1f, 0.5f),  //CYAN LIGHT
                pressedColor = Color.gray,
                selectedColor = new Color(68f / 255f, 212f / 255f, 1f, 1f),//MORPHONET
                disabledColor = new Color(68f / 255f, 212f / 255f, 1f, 1f),
                colorMultiplier = 1
            };

            setButtonColors(BoutonDataset.GetComponent<Button>());
            setButtonColors(BoutonInfos.GetComponent<Button>());
            setButtonColors(BoutonObjects.GetComponent<Button>());
            setButtonColors(BoutonGroups.GetComponent<Button>());
            setButtonColors(BoutonAniseed.GetComponent<Button>());
            setButtonColors(BoutonMovie.GetComponent<Button>());

            //init listeners for Info connections menu
            ConnectionMaxThreshold.onValueChanged.AddListener(UpdateInfosConnectionMaxValue);
            ConnectionMinThreshold.onValueChanged.AddListener(UpdateInfosConnectionMinValue);
            ConnectionMaxV.onValueChanged.AddListener(UpdateInfosConnectionMaxSlider);
            ConnectionMinV.onValueChanged.AddListener(UpdateInfosConnectionMinSlider);
            CancelConnection.onClick.AddListener(() => { DisplayInfosConnectionMenu(false); });
            ApplyConnection.onClick.AddListener(() => { DisplayInfosConnectionMenu(false); });

            //console button add listener
            ConsoleOpenButton.onClick.AddListener(() => { DebugConsoleManager.instance.ToggleConsole(); });

            yield return new WaitForEndOfFrame();
            containerImages.SetActive(false);//by default deactivate raw

            yield return null;
        }

        //When on a standalone, check if the focus is on the lineage or the dataset (for now dataset_index is useless cause we only have one dataset)
        public bool CanInteractWithViewer(int dataset_index)
        {
            return (!lineage_viewer.isActive || (lineage_viewer.isActive && !lineage_viewer.hasFocus));
        }

        // Update is called once per frame
        private void Update()
        {
            //Detect if we need to minimize
            if (Screen.width < 310 && Screen.height < 310)
                checkEveryButtons();

            //swap Sliders min and max values if they cross each others
            if (SliderXMin.value > SliderXMax.value)
            {
                float backup = SliderXMax.value;
                SliderXMax.value = SliderXMin.value;
                SliderXMin.value = backup;
            }

            if (SliderYMin.value > SliderYMax.value)
            {
                float backup = SliderYMax.value;
                SliderYMax.value = SliderYMin.value;
                SliderYMin.value = backup;
            }

            if (SliderZMin.value > SliderZMax.value)
            {
                float backup = SliderZMax.value;
                SliderZMax.value = SliderZMin.value;
                SliderZMin.value = backup;
            }

            //compute and draw framerate
            drawFrameRate();

            //update stored canvas scale value, because these values are used elsewhere
            if (canvasScale != canvas.transform.localScale.x)
            {
                
                if (transparencyText != null && transparencyText.cachedTextGenerator != null)
                {
                    int targetSize = transparencyText.cachedTextGenerator.fontSizeUsedForBestFit;
                    foreach (Text text in targetText)
                        text.fontSize = targetSize;
                }
                    

                canvasScale = canvas.transform.localScale.x;  //If screen change ....
                canvasWidth = canvas.GetComponent<RectTransform>().sizeDelta.x;
                canvasHeight = canvas.GetComponent<RectTransform>().sizeDelta.y;
            }

            _TimePassed += Time.deltaTime;

            if (!SearchField.isFocused && Input.GetKey(KeyCode.RightArrow) && _TimePassed >= _KeyDelay && CanInteractWithViewer(0))
            {
                _TimePassed = 0f;
                scrollNext();
            }

            if (!SearchField.isFocused && Input.GetKey(KeyCode.LeftArrow) && _TimePassed >= _KeyDelay && CanInteractWithViewer(0))
            {
                _TimePassed = 0f;
                scrollPrevious();
            }

            if (!SearchField.isFocused && (MorphoTools.GetDataset() != null && MorphoTools.GetDataset().Curations != null && !MorphoTools.GetDataset().Curations.newvalue.isFocused) && Input.GetKey(KeyCode.S) && _TimePassed >= _KeyDelay)
            {
                _TimePassed = 0f;
                MenuObjects.transform.Find("Selection").GetComponent<SelectionManager>().applySelection();
            }

            if (!MenuRendering.activeSelf && background_picker.go != null)
            {
                background_picker.DestroyColorPicker();
            }

            if (!MenuRendering.activeSelf && lineage_background_picker.go != null)
            {
                lineage_background_picker.DestroyColorPicker();
            }

            if ((Input.GetAxis("Mouse ScrollWheel") < 0 || Input.GetAxis("Mouse ScrollWheel") > 0) && CanInteractWithViewer(0)) // Zoom forward
            {
                zoom_display_window.SetActive(true);
                _Modifying_zoom = true;

                if (init_data_scale == -1f && MorphoTools.GetDataset() != null)
                {
                    init_data_scale = MorphoTools.GetTransformationsManager().CurrentScale.x;
                    if (init_data_scale == 0f)
                        init_data_scale = 1f;
                }
                zoom_display.text = "Zoom : " + (int)((MorphoTools.GetTransformationsManager().CurrentScale.x / init_data_scale)*100f)/100f;
            }
            else
            {
                if (_Zooming_stop == null)
                    _Zooming_stop = StartCoroutine(DelayedZoomHide());
            }
        }

        //Show Loading Icone
        public static void setLoading()
        {
            instance.loading.SetActive(true);
        }

        //Stop Loading Icone
        public static void stopLoading()
        {
            instance.loading.SetActive(false);
        }

        //Handle a zoom scroll (is delayed to smoothen things)
        public IEnumerator DelayedZoomHide()
        {
            _Modifying_zoom = false;

            yield return new WaitForSecondsRealtime(2.0f);

            while (_Modifying_zoom)
            {
                _Modifying_zoom = false;
                yield return new WaitForSecondsRealtime(2.0f);
            }

            zoom_display_window.SetActive(false);
            _Zooming_stop = null;
        }

        //Initialize ui by updating min and mx time
        public void initializeTime(int minTime, int maxTime)
        {
            scrolledVal = minTime;
            if ((minTime != maxTime))
            {
                scrollbarTime.transform.GetComponent<Slider>().maxValue = maxTime;
                scrollbarTime.transform.GetComponent<Slider>().minValue = minTime;
                scrollbarTime.transform.GetComponent<Slider>().value = minTime;
                TimeBar.SetActive(true);
            }
            else
            {
                //prevent all time menu to open
                MenuClicked.transform.Find("Bouton layout").Find("ShowSelected").GetComponent<ShowObjectsOnMouseOver>().active = false;
                MenuClicked.transform.Find("Bouton layout").Find("HideSelected").GetComponent<ShowObjectsOnMouseOver>().active = false;
                MenuClicked.transform.Find("Bouton layout").Find("Sisters").gameObject.SetActive(false);
                TimeBar.SetActive(false);


                GameObject MenuInfos = canvas.transform.Find("bouton layout group").Find("Properties").Find("MenuProperties").gameObject;
                MenuInfos.transform.Find("CellMotherID").gameObject.SetActive(false);
                MenuInfos.transform.Find("LineMotherCell").gameObject.SetActive(false);
                MenuInfos.transform.Find("LineCellDaughters").gameObject.SetActive(false);
                MenuInfos.transform.Find("CellDaughtersID").gameObject.SetActive(false);
            }
        }

        //Sync sliders and TextField for Infos Connection menu
        private void UpdateInfosConnectionMinValue(float v)
        {
            ConnectionMinV.SetTextWithoutNotify(v.ToString());
        }

        private void UpdateInfosConnectionMaxValue(float v)
        {
            ConnectionMaxV.SetTextWithoutNotify(v.ToString());
        }

        private void UpdateInfosConnectionMinSlider(string s)
        {
            float v;
            if (float.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out v))
            {
                ConnectionMinThreshold.SetValueWithoutNotify(v);
            }
        }

        private void UpdateInfosConnectionMaxSlider(string s)
        {
            float v;
            if (float.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out v))
            {
                ConnectionMaxThreshold.SetValueWithoutNotify(v);
            }
        }

        public void SetCurrentConnectionMinMaxValues(float min, float max)
        {
            ConnectionMaxThreshold.maxValue = max;
            ConnectionMaxThreshold.minValue = min;
            ConnectionMaxThreshold.value = max;
            ConnectionMinThreshold.maxValue = max;
            ConnectionMinThreshold.minValue = min;
            ConnectionMinThreshold.value = min;
        }

        public void DisplayInfosConnectionMenu(bool value)
        {
            MenuInfosConnections.SetActive(value);
        }
    }
}