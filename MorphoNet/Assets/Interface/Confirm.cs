﻿using UnityEngine;
using UnityEngine.UI;

namespace MorphoNet
{
    public class Confirm : MonoBehaviour
    {
        public static GameObject ConfirmBox;
        public static Button yes;
        public static Text action;

        public static void init()
        {
            if (ConfirmBox == null)
                ConfirmBox = InterfaceManager.instance.canvas.transform.Find("ConfirmBox").gameObject;
            if (action == null)
            {
                action = ConfirmBox.transform.Find("action").gameObject.GetComponent<Text>();
            }
            if (yes == null)
            {
                yes = ConfirmBox.transform.Find("yes").gameObject.GetComponent<Button>();
                ConfirmBox.transform.Find("no").gameObject.GetComponent<Button>().onClick.AddListener(() => Confirm.close());
                
            }
            yes.onClick.RemoveAllListeners();
            yes.onClick.AddListener(() => Confirm.close());
        }

        public static void setMessage(string message)
        {
            if (action != null)
            {
                action.text = message;
            }
        }

        public static void confirm()
        {
            ConfirmBox.SetActive(true);
        }

        public static void close()
        {
            action.text = "";
            ConfirmBox.SetActive(false);
        }
    }
}