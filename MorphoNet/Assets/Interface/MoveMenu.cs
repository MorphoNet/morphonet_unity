﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace MorphoNet
{
    public class MoveMenu : EventTrigger, IPointerEnterHandler, IPointerExitHandler
    {
        private bool dragging;
        private Vector3 backup_start;
        private Vector3 start_mousePosition;
        private Vector3 start_dragPosition;
        public bool is_fixed;
        public bool can_move = false;

        public bool hovered = false;

        public void FixInSpace()
        {
            is_fixed = true;
        }

        public void UnfixInSpace()
        {
            is_fixed = false;
        }

        private void Start()
        {
            dragging = false;
            Canvas.willRenderCanvases += canvasUpdate;
            backup_start = transform.localPosition;
            is_fixed = false;
        }

        // Update is called once per frame
        public void canvasUpdate()
        {
            if (dragging)
            {
                //if (Vector3.Distance(transform.localPosition, backup_start) > 1f) is_fixed = true;
                Vector3 localPoint = Input.mousePosition - start_mousePosition;
                localPoint.x /= Screen.width / InterfaceManager.instance.canvasWidth;
                localPoint.y /= Screen.height / InterfaceManager.instance.canvasHeight;
                transform.localPosition = start_dragPosition + localPoint;
            }
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            if (can_move)
            {
                dragging = true;
                start_mousePosition = Input.mousePosition;
                start_dragPosition = transform.localPosition;
            }
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            dragging = false;
            //   if(Vector3.Distance(transform.localPosition,backup_start) < 0.1f) is_fixed = false;
        }

        public void StartDragging()
        {
            if (can_move)
            {
                dragging = true;
                start_mousePosition = Input.mousePosition;
                start_dragPosition = transform.localPosition;
            }
        }

        public void StopDragging()
        {
            dragging = false;
        }

        public void getBack()
        {
            //    is_fixed = false;
            //transform.localPosition = backup_start;
        }
    }
}