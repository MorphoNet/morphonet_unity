using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILineRenderer : Graphic
{
    public Vector2Int GridSize;
    public float Thickness = 10f;
    public List<Vector2> Points;

    private float _Width;
    private float _Height;
    private float _UnitWidth;
    private float _UnitHeight;
    private Vector2 _Offset;

    public void ForceRedraw()
    {
        this.SetAllDirty();
    }

    protected override void OnPopulateMesh(VertexHelper vh)
    {
        vh.Clear();

        _Width = rectTransform.rect.width;
        _Height = rectTransform.rect.height;

        _UnitWidth = _Width / (float)GridSize.x;
        _UnitHeight = _Height / (float)GridSize.y;

        _Offset = new Vector2(-((float)GridSize.x / 2.0f), -((float)GridSize.y / 2.0f));        

        if (Points.Count < 2)
        {
            return;
        }

        for (int i = 0; i < Points.Count-1; i++)
        {
            DrawForPoints(Points[i]+_Offset, Points[i+1]+ _Offset, vh,i*4);
        }
    }

    private float GetAngle(Vector2 start,Vector2 target)
    {
        return (float)(Mathf.Atan2(target.y - start.y, target.x - start.x) * (180.0f / Mathf.PI));
    }

    private void DrawForPoints(Vector2 start, Vector2 end, VertexHelper vh, int index)
    {
        float angle = GetAngle(start, end)+90f;

        UIVertex vertex = UIVertex.simpleVert;
        vertex.color = color;

        vertex.position = Quaternion.Euler(0, 0, angle) * new Vector3(-Thickness / 2.0f, 0);
        vertex.position += new Vector3(_UnitWidth * start.x, _UnitHeight * start.y);
        vh.AddVert(vertex);
        vertex.position = Quaternion.Euler(0, 0, angle) * new Vector3(Thickness / 2.0f, 0);
        vertex.position += new Vector3(_UnitWidth * start.x, _UnitHeight * start.y);
        vh.AddVert(vertex);

        vertex.position = Quaternion.Euler(0, 0, angle) * new Vector3(-Thickness / 2.0f, 0);
        vertex.position += new Vector3(_UnitWidth * end.x, _UnitHeight * end.y);
        vh.AddVert(vertex);
        vertex.position = Quaternion.Euler(0, 0, angle) * new Vector3(Thickness / 2.0f, 0);
        vertex.position += new Vector3(_UnitWidth * end.x, _UnitHeight * end.y);
        vh.AddVert(vertex);

        //add triangles
        vh.AddTriangle(index, index + 1, index + 3);
        vh.AddTriangle(index + 3, index + 2, index + 0);
    }
}
