﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Menus : MonoBehaviour {
	public GameObject MenuObjects;
	public GameObject MenuDataset;
	public GameObject MenuInfos;
	public GameObject MenuLoadSelection;
	public GameObject MenuShortcuts;
	public GameObject MenuGenetic;
	public GameObject MenuScenario;
	public GameObject MenuMovie;
	public GameObject MenuGroups;
    public GameObject MenuSegmentation;
	public GameObject MenuLineage;


	public GameObject MenuColor;
	public GameObject MenuColorBar;
	public GameObject MenuShare;
	public GameObject MenuChannel;
	public GameObject MenuAction;
	public GameObject MenuPicked;
	public GameObject MenuCommon;
	public GameObject MenuCalcul;
	public GameObject MenuCurration;
    public GameObject MenuMutants;
    public GameObject MenuDeregulations;
	public Text Description;

	public GameObject MenuColorChoose;
	public GameObject MenuColorChooseBackground;

	//Button
    public GameObject buttonGenetic;
	public GameObject buttonGroups;
	public GameObject buttonMovie;
    public GameObject buttonDataset;
    public GameObject buttonInfos;
    public GameObject buttonObjects;
    public GameObject buttonSegmentation;
	public GameObject buttonLineage;
	public GameObject buttonClicked;

	public GameObject containerGenetic;
	public GameObject containerGroups;
	public GameObject containerMovie;
	public GameObject containerDataset;
	public GameObject containerInfos;
	public GameObject containerObjects;
	public GameObject containerSegmentation;
	public GameObject containerLineage;

	public bool backupDone = false;
	public bool minimized = false;

	public List<GameObject> MovingMenus;
}
