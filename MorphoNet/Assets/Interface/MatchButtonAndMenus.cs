﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MorphoNet
{
    public class MatchButtonAndMenus : MonoBehaviour
    {
        // Start is called before the first frame update
        public List<GameObject> buttons;

        public List<GameObject> menus;
        public List<GameObject> buttons_snap;
        public Dictionary<int, bool> moved;
        public List<Vector3> backup_pos;
        public GameObject HorizontalGroupObject;
        public GameObject Canvas;
        private bool backup_done = false;

        private void Start()
        {
            moved = new Dictionary<int, bool>();
        }

        // Update is called once per frame
        private void Update()
        {
#if !UNITY_IOS //TEMPORARY BUG BECAUSE ALL MENUS OPENS IN AR IOS
            if (backup_done)
            {
                for (int i = 0; i < buttons.Count; i++)
                {
                    //  MorphoDebug.Log(buttons[i].transform.position);
                    if (buttons[i].activeSelf)
                    {
                        if ((HorizontalGroupObject.transform.position.y - buttons[i].transform.position.y) > 0.02f && buttons[i].transform.parent != Canvas.transform && ((moved.ContainsKey(i) && moved[i] == false) || !moved.ContainsKey(i)))
                        {
                            buttons[i].GetComponent<LayoutElement>().ignoreLayout = true;
                            //  buttons[i].transform.parent = Canvas.transform;
                            buttons[i].GetComponent<MoveMenu>().FixInSpace();
                            menus[i].SetActive(true);
                            if (moved.ContainsKey(i))
                            {
                                moved[i] = true;
                            }
                            else
                            {
                                moved.Add(i, true);
                            }
                            buttons_snap[i].SetActive(true);
                        }
                    }
                }
            }

            if (!backup_done && SetsManager.instance.loading_finished)
            {
                StartCoroutine(DelayStore());
            }
#endif
        }

        public IEnumerator DelayStore()
        {
            yield return new WaitForSeconds(2.0f);
            for (int i = 0; i < buttons.Count; i++)
            {
                Vector3 the_position = buttons[i].transform.position;
                buttons[i].transform.SetParent(HorizontalGroupObject.transform);
            }
            for (int i = 0; i < buttons.Count; i++)
            {
                buttons[i].GetComponent<MoveMenu>().can_move = true;
            }
            backup_done = true;
        }

        public void SendBackToLayout(GameObject button)
        {
#if !UNITY_IOS
            StartCoroutine(SendDelayed(button));
#endif
        }

        public IEnumerator SendDelayed(GameObject button)
        {
            int index = -1;
            for (int i = 0; i < buttons.Count; i++)
            {
                if (buttons[i] == button)
                {
                    index = i;
                }
            }
            if (index != -1)
            {
                buttons[index].GetComponent<MoveMenu>().is_fixed = false;
                // buttons[index].transform.parent = HorizontalGroupObject.transform;
                menus[index].SetActive(false);
                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();
                buttons[index].GetComponent<LayoutElement>().ignoreLayout = false;
                buttons[index].SetActive(false);
                yield return new WaitForEndOfFrame();
                buttons[index].SetActive(true);
                yield return new WaitForEndOfFrame();
                if (moved.ContainsKey(index))
                {
                    moved[index] = false;
                }
                else
                {
                    moved.Add(index, false);
                }

                buttons[index].GetComponent<MoveMenu>().UnfixInSpace();
                buttons_snap[index].SetActive(false);
            }
        }
    }
}