﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MenuMouseOver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject menu_to_open;
    public void OnPointerEnter(PointerEventData eventData)
    {
        menu_to_open.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        menu_to_open.SetActive(false);
    }
}
