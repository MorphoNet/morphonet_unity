using UnityEngine;
using UnityEngine.EventSystems;

namespace MorphoNet
{
    public class RotateTextOnMouseOver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public bool hover = false;
        public bool rotate = false;

        void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
        {
            hover = true;
        }

        void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
        {
            rotate = false;
            hover = false;
        }

        // Update is called once per frame
        private void Update()
        {
            if (hover)
            {
                if (Input.GetKeyDown(KeyCode.R))
                {
                    rotate = true;
                }
                else if (Input.GetKeyUp(KeyCode.R))
                {
                    rotate = false;
                }

                if (rotate)
                {
                    transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles - new Vector3(0, 0, MorphoTools.GetFigureManager().TextRotationSpeed));
                }
            }
        }
    }
}