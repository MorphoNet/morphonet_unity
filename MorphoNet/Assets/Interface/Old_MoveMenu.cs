﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace MorphoNet
{
    public class Old_MoveMenu : EventTrigger
    {
        private bool dragging;
        private Vector3 backup_start;
        private Vector3 start_mousePosition;
        private Vector3 start_dragPosition;
        public bool is_fixed;

        private void Start()
        {
            dragging = false;
            Canvas.willRenderCanvases += canvasUpdate;
            backup_start = transform.localPosition;
            is_fixed = false;
        }

        // Update is called once per frame
        public void canvasUpdate()
        {
            if (dragging)
            {
                Vector3 localPoint = Input.mousePosition - start_mousePosition;
                localPoint.x /= Screen.width / InterfaceManager.instance.canvasWidth;
                localPoint.y /= Screen.height / InterfaceManager.instance.canvasHeight;
                transform.localPosition = start_dragPosition + localPoint;
            }
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            dragging = true;
            is_fixed = true;
            start_mousePosition = Input.mousePosition;
            start_dragPosition = transform.localPosition;
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            dragging = false;
            if (transform.localPosition == backup_start)
                is_fixed = false;
        }

        public void getBack()
        {
            is_fixed = false;
            transform.localPosition = backup_start;
        }
    }
}