﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveObject : MonoBehaviour
{
    private bool state;
    // Start is called before the first frame update
    void Start()
    {
        state = gameObject.activeSelf;
    }

    // Update is called once per frame
    public void onClick()
    {
        state = !state;
        gameObject.SetActive(state);
    }
}
