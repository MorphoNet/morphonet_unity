﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartialHideMinimizedMode : MonoBehaviour
{
    public static PartialHideMinimizedMode instance = null;
    public GameObject HideButton;
    public GameObject ShowButton;
    public GameObject ResetButton;
    public GameObject InverseButton;
    public GameObject NeighborsButton;
    public GameObject TimePointsText;
    public GameObject TimePointSwitch;
    public GameObject NavigationButton;

    public void HideUIMinimizedMode(bool state)
    {
        if (HideButton != null)
            HideButton.SetActive(state);
        if (ShowButton != null)
            ShowButton.SetActive(state);
        if (ResetButton != null)
            ResetButton.SetActive(state);
        if (InverseButton != null)
            InverseButton.SetActive(state);
       /* if (NeighborsButton != null) { 
            NeighborsButton.SetActive(state);
        }*/
        if (TimePointsText != null)
            TimePointsText.SetActive(state);
        if (TimePointSwitch != null)
            TimePointSwitch.SetActive(state);
        if (NavigationButton != null)
            NavigationButton.SetActive(state);
    }

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

}
