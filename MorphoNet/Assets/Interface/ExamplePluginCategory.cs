using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExamplePluginCategory : MonoBehaviour
{
    [SerializeField]
    private Text _Name;

    [SerializeField]
    private Button _ExamplePluginBtn;


    // Start is called before the first frame update
    void Start()
    {
        _ExamplePluginBtn.gameObject.SetActive(false);
    }

    public void SetName(string name)
    {
        _Name.text = name;
    }

    public Button AddButton(string name,Sprite img=null)
    {
        Button b = Instantiate(_ExamplePluginBtn.gameObject, this.transform).GetComponent<Button>();
        if (img != null)
        {
            b.gameObject.GetComponent<Image>().sprite = img;
        }
        b.gameObject.name = name;
        return b;
    }
}
