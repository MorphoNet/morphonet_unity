﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using AssemblyCSharp;
using MorphoNet.Extensions.Unity;

namespace MorphoNet
{
    public class ChangeColorSelection : MonoBehaviour
    {
        [SerializeField]
        private int _R = 0;

        [SerializeField]
        private InputField _InputFieldR;

        [SerializeField]
        private int _G = 0;

        [SerializeField]
        private InputField _InputFieldG;

        [SerializeField]
        private int _B = 0;

        [SerializeField]
        private InputField _InputFieldB;

        [SerializeField]
        private int _A = 255;

        [SerializeField]
        private Renderer _NewColor;

        [SerializeField]
        private Button _ValidNewColor;

        [SerializeField]
        private Button _CancelNewColor;

        [SerializeField]
        private GameObject _MenuColor;

        [SerializeField]
        private GameObject _Selection;

        [SerializeField]
        private int _CurrentChoosen = 0;

        [SerializeField]
        private ShaderPresetsManager _Presets;

        [SerializeField]
        private List<MaterialPreset> _Materials;

        [SerializeField]
        private ColorPickerTriangle _ColorPicker;

        private Color _PreviousColor;

        [SerializeField]
        private GameObject _AdvancedMenuPrefab;

        [SerializeField]
        private RectTransform _ShaderParamsPanel;

        [SerializeField]
        private RectTransform _ShaderColorPrefab;

        private Dictionary<string, ShaderSliderParam> _AdvancedParameters;
        private Vector2 _InitAdvancedYPos;

        [SerializeField]
        public ColorPickerTriangle AdditionnalParameterPicker;

        private Dictionary<string, ShaderColorParam> _AdvancedColorParameters;

        [SerializeField]
        public ShaderColorParam CurrentAdvancedColor;

        private Color _PreviousAdvancedColor;

        public List<GameObject> SelectionMenuMainUIElements;

        private void Update()
        {
            if (_ColorPicker != null && AdditionnalParameterPicker != null)
            {
                if (_PreviousColor != _ColorPicker.TheColor)
                {
                    Color nc = _ColorPicker.TheColor;
                    nc.a = (float)_A / 255f;
                    _PreviousColor = _ColorPicker.TheColor;
                    _Presets.ChangeAllPresetsColor(nc);
                    foreach (MaterialPreset p in _Materials)
                        p.SetAndUpdateColor(nc);

                    _R = (int)(nc.r * 255);
                    _G = (int)(nc.g * 255);
                    _B = (int)(nc.b * 255);
                    _InputFieldR.SetTextWithoutNotify("" + _R);
                    _InputFieldG.SetTextWithoutNotify("" + _G);
                    _InputFieldB.SetTextWithoutNotify("" + _B);
                    SendColorToCurrent("_Color", nc);
                }

                if (_PreviousAdvancedColor != AdditionnalParameterPicker.TheColor)
                {
                    if (CurrentAdvancedColor != null)
                    {
                        _PreviousAdvancedColor = AdditionnalParameterPicker.TheColor;
                        Color nc = AdditionnalParameterPicker.TheColor;

                        CurrentAdvancedColor.UpdateFields(nc);
                        SendColorToCurrent(CurrentAdvancedColor.name, nc);
                    }
                }
            }
        }

        public void UpdateAllMats()
        {
            Color nc = _ColorPicker.TheColor;
            foreach (MaterialPreset p in _Materials)
                p.SetAndUpdateColor(nc);
            SendColorToCurrent("_Color", nc);
        }

        public void UpdatePickerColor()
        {
            Color nc = new Color(int.Parse(_InputFieldR.text) / 255f, int.Parse(_InputFieldG.text) / 255f, int.Parse(_InputFieldB.text) / 255f);
            _R = int.Parse(_InputFieldR.text);
            _G = int.Parse(_InputFieldG.text);
            _B = int.Parse(_InputFieldB.text);
            _ColorPicker.SetNewColor(nc);
        }

        private void Start()
        {
            Color actual = SelectionManager.materials[SelectionManager.Static_selectionValue - 1].color;
            _ColorPicker.SetNewColor(actual);

            _PreviousColor = _ColorPicker.TheColor;
            Color nc = _ColorPicker.TheColor;
            nc.a = (float)_A / 255f;
            _PreviousColor = _ColorPicker.TheColor;
            _Presets.ChangeAllPresetsColor(nc);
            foreach (MaterialPreset p in _Materials)
                p.SetAndUpdateColor(nc);

            _InputFieldR.SetTextWithoutNotify("" + (int)(nc.r * 255));
            _InputFieldG.SetTextWithoutNotify("" + (int)(nc.g * 255));
            _InputFieldB.SetTextWithoutNotify("" + (int)(nc.b * 255));

            if (_AdvancedParameters == null)
                _AdvancedParameters = new Dictionary<string, ShaderSliderParam>();
            if (_AdvancedColorParameters == null)
                _AdvancedColorParameters = new Dictionary<string, ShaderColorParam>();
            if (_InitAdvancedYPos == null)
                _InitAdvancedYPos = _ShaderParamsPanel.anchoredPosition;

            _PreviousAdvancedColor = AdditionnalParameterPicker.TheColor;
        }

        public void SetPrevisMaterial(Material m)
        {
            _NewColor.material = m;
        }

        public void SendParameterToCurrent(string parameter, float value)
        {
            if (_CurrentChoosen == 9)
            {
                SetsManager.instance.Selected.UpdateFloatProperty(parameter, value);
            }
            else if (_CurrentChoosen == 10)
            {
                for (int i = 0; i < SetsManager.instance.StainSelection.Count; i++)
                {
                    SetsManager.instance.StainSelection[i].UpdateFloatProperty(parameter, value);
                }

                for (int i = 0; i < SetsManager.instance.BarSelection.Count; i++)
                {
                    SetsManager.instance.BarSelection[i].UpdateFloatProperty(parameter, value);
                }
            }
            _NewColor.material.UpdateFloatProperty(parameter, value);
        }

        public void SendColorToCurrent(string colorName, Color value)
        {
            _NewColor.material.UpdateColorProperty(colorName, value);
        }

        public void changeShaderValue(GameObject slider)
        {
            SendParameterToCurrent(slider.name, slider.GetComponent<Slider>().value);
        }

        public void changeShaderValue(string param, float value)
        {
            SendParameterToCurrent(param, value);
        }

        public void changeShaderValueField(GameObject field)
        {
            SendParameterToCurrent(field.name, float.Parse(field.GetComponent<InputField>().text));
        }

        public void changeShaderColor(string param, Color value)
        {
            SendColorToCurrent(param, value);
        }

        public void SendGlobalParameterToCurrent(string parameter, float value, bool selectedOrMulti)
        {
            if (selectedOrMulti)
            {
                SetsManager.instance.Selected.UpdateFloatProperty(parameter, value);
            }
            else
            {
                for (int i = 0; i < SetsManager.instance.StainSelection.Count; i++)
                {
                    SetsManager.instance.StainSelection[i].UpdateFloatProperty(parameter, value);
                }

                for (int i = 0; i < SetsManager.instance.BarSelection.Count; i++)
                {
                    SetsManager.instance.BarSelection[i].UpdateFloatProperty(parameter, value);
                }
            }
        }

        public void changeGlobalShaderValue(GameObject slider, bool selectedOrMulti)
        {
            SendGlobalParameterToCurrent(slider.name, slider.GetComponent<Slider>().value, selectedOrMulti);
        }

        public void InitColor(Material m)
        { //Initialise the color set on menu opening
            Color c = m.color;
            changeR((int)Mathf.Round(c.r * 255f));
            changeG((int)Mathf.Round(c.g * 255f));
            changeB((int)Mathf.Round(c.b * 255f));
            changeA((int)Mathf.Round(c.a * 255f));
        }

        public Material GetMaterialFromCurrent()
        {
            Material chooseMaterial = null;
            switch (_CurrentChoosen)
            {
                case 0:
                    chooseMaterial = new Material(SetsManager.instance.Default);
                    break;

                case 1:
                    chooseMaterial = new Material(SetsManager.instance.Transparent);
                    break;

                case 2:
                    chooseMaterial = new Material(SetsManager.instance.Diamond);
                    break;

                case 3:
                    chooseMaterial = new Material(SetsManager.instance.Vertex);
                    break;

                case 4:
                    chooseMaterial = new Material(SetsManager.instance.Stars);
                    break;

                case 5:
                    chooseMaterial = new Material(SetsManager.instance.bloodcell);
                    break;

                case 6:
                    chooseMaterial = new Material(SetsManager.instance.Dark);
                    break;

                case 7:
                    chooseMaterial = new Material(SetsManager.instance.Brick);
                    break;

                case 8:
                    chooseMaterial = new Material(SetsManager.instance.Selected);
                    break;

                case 9:
                    if (InterfaceManager.instance.shaderType == "stain")
                    {
                        chooseMaterial = SetsManager.instance.StainSelection[0];
                    }
                    else
                    {
                        chooseMaterial = SetsManager.instance.BarSelection[0];
                    }
                    break;
            }

            return chooseMaterial;
        }

        public void chooseShaders(int v)
        {
            _CurrentChoosen = v;
            Material chooseMaterial = GetMaterialFromCurrent();

            if (chooseMaterial.HasProperty("_Color") && v != 9 && v != 10)
            {
                _ValidNewColor.gameObject.SetActive(true);
                _CancelNewColor.gameObject.SetActive(true);
            }
            else
            {
                _ValidNewColor.gameObject.SetActive(false);
                _CancelNewColor.gameObject.SetActive(false);
            }

            _NewColor.material = new Material(chooseMaterial);

            UpdateColor();

            AdditionnalParameterPicker.gameObject.SetActive(false);
        }

        public static Material getMaterialByName(string name)
        {
            Material chooseMaterial = null;
            if (name.IndexOf("StandardCell") == 0 || name.IndexOf("default") == 0 || name.IndexOf("FresnellCell") == 0)
                chooseMaterial = new Material(SetsManager.instance.Default);
            else if (name.IndexOf("TransparentCell") == 0)
                chooseMaterial = new Material(SetsManager.instance.Transparent);
            else if (name.IndexOf("WhiteGem") == 0)
                chooseMaterial = new Material(SetsManager.instance.Diamond);
            else if (name.IndexOf("Rough Textured Metal") == 0 || name.IndexOf("RoughTexturedMetal") == 0)
                chooseMaterial = new Material(SetsManager.instance.Dark);
            else if (name.IndexOf("Robot MatCap Plain Additive") == 0 || name.IndexOf("RobotMatCapPlainAdditive") == 0)
                chooseMaterial = new Material(SetsManager.instance.Vertex);
            else if (name.IndexOf("Bubble wrap pattern") == 0)
                chooseMaterial = new Material(SetsManager.instance.Bumped);
            else if (name.IndexOf("Lavabrick") == 0)
                chooseMaterial = new Material(SetsManager.instance.Brick);
            else if (name.IndexOf("Stars") == 0)
                chooseMaterial = new Material(SetsManager.instance.Stars);
            else if (name.IndexOf("Wireframe - ShadedUnlit") == 0)
                chooseMaterial = new Material(SetsManager.instance.Wireframe);
            else if (name.IndexOf("WPlasticine_08_8K_Small Red") == 0 || name.IndexOf("bloodcell") == 0)
                chooseMaterial = new Material(SetsManager.instance.bloodcell);
            else if (name.IndexOf("SelectedCell") == 0)
                chooseMaterial = new Material(SetsManager.instance.Selected);
            return chooseMaterial;
        }

        public static MaterialPreset GetPresetByMaterialName(string name)
        {
            MaterialPreset preset = SetsManager.instance.DefaultPreset;
            if (name.IndexOf("StandardCell") == 0 || name.IndexOf("default") == 0 || name.IndexOf("FresnellCell") == 0)
                preset = SetsManager.instance.DefaultPreset;
            else if (name.IndexOf("TransparentCell") == 0)
                preset = SetsManager.instance.TransparentPreset;
            else if (name.IndexOf("WhiteGem") == 0)
                preset = SetsManager.instance.DiamondPreset;
            else if (name.IndexOf("Rough Textured Metal") == 0)
                preset = SetsManager.instance.DarkPreset;
            else if (name.IndexOf("Robot MatCap Plain Additive") == 0)
                preset = SetsManager.instance.HaloPreset;
            else if (name.IndexOf("Lavabrick") == 0)
                preset = SetsManager.instance.BrickPreset;
            else if (name.IndexOf("Stars") == 0)
                preset = SetsManager.instance.ConcretePreset;
            else if (name.IndexOf("WPlasticine_08_8K_Small Red") == 0 || name.IndexOf("bloodcell") == 0)
                preset = SetsManager.instance.BloodcellPreset;
            return preset;
        }

        public static int GetPresetIdByName(string name)
        {
            if (name.IndexOf("StandardCell") == 0 || name.IndexOf("default") == 0 || name.IndexOf("FresnellCell") == 0)
                return 0;
            else if (name.IndexOf("TransparentCell") == 0)
                return 1;
            else if (name.IndexOf("WhiteGem") == 0)
                return 2;
            else if (name.IndexOf("Robot MatCap Plain Additive") == 0)
                return 3;
            else if (name.IndexOf("Stars") == 0)
                return 4;
            else if (name.IndexOf("WPlasticine_08_8K_Small Red") == 0 || name.IndexOf("bloodcell") == 0)
                return 5;
            else if (name.IndexOf("Rough Textured Metal") == 0)
                return 6;
            else if (name.IndexOf("Lavabrick") == 0)
                return 7;
            return 0;
        }

        public void UpdateColor()
        {
            _NewColor.material.UpdateColorProperty("_Color", new Color((float)_R / 255f, (float)_G / 255f, (float)_B / 255f, (float)_A / 255f));
        }

        public void UpdateParameter(string param, float value)
        {
            if (_NewColor.material.HasProperty(param))
                _NewColor.material.UpdateFloatProperty(param, value);
        }

        public void changeR(int v)
        {
            if (v < 0)
                v = 0;
            if (v > 255)
                v = 255;
            _R = v;
            _InputFieldR.SetTextWithoutNotify(v.ToString());
        }

        public void changeB(int v)
        {
            if (v < 0)
                v = 0;
            if (v > 255)
                v = 255;
            _B = v;
            _InputFieldB.SetTextWithoutNotify(v.ToString());
        }

        public void changeG(int v)
        {
            if (v < 0)
                v = 0;
            if (v > 255)
                v = 255;
            _G = v;
            _InputFieldG.SetTextWithoutNotify(v.ToString());
        }

        public void changeA(int v)
        {
            if (v < 0)
                v = 0;
            if (v > 255)
                v = 255;
            _A = v;
        }

        //When Input field changed
        public void changeFieldR()
        {
            int v = 0;
            int.TryParse(_InputFieldR.text, out v);
            changeR(v);
            UpdateColor();
        }

        public void changeFieldB()
        {
            int v = 0;
            int.TryParse(_InputFieldB.text, out v);
            changeB(v);
            UpdateColor();
        }

        public void changeFieldG()
        {
            int v = 0;
            int.TryParse(_InputFieldG.text, out v);
            changeG(v);
            UpdateColor();
        }

        public void changeFieldA()
        {
            UpdateColor();
        }

        public void Valid()
        {
            _Selection.GetComponent<SelectionManager>().changeMaterialSet(_NewColor.material);
            _MenuColor.SetActive(false);
        }

        public void Cancel()
        {
            _MenuColor.SetActive(false);
        }

        public void GenerateAdvancedMenu(MaterialPreset p)
        {
            if (_AdvancedParameters == null)
                _AdvancedParameters = new Dictionary<string, ShaderSliderParam>();
            if (_AdvancedColorParameters == null)
                _AdvancedColorParameters = new Dictionary<string, ShaderColorParam>();
            if (_InitAdvancedYPos == null)
                _InitAdvancedYPos = _ShaderParamsPanel.anchoredPosition;

            //first clear all previous
            foreach (KeyValuePair<string, ShaderSliderParam> para in _AdvancedParameters)
            {
                DestroyImmediate(para.Value.gameObject);
            }
            _AdvancedParameters.Clear();

            foreach (KeyValuePair<string, ShaderColorParam> para in _AdvancedColorParameters)
            {
                DestroyImmediate(para.Value.gameObject);
            }
            _AdvancedColorParameters.Clear();

            _ShaderParamsPanel.anchoredPosition = _InitAdvancedYPos;

            //then generate new menu
            foreach (MaterialPreset.ShaderProperty prop in p.Properties)
            {
                if (!prop.isColor)
                {
                    GameObject newMenu = Instantiate(_AdvancedMenuPrefab, _ShaderParamsPanel.transform);
                    Vector3 newPos = _ShaderParamsPanel.anchoredPosition;
                    newPos.y -= 17.5f;//half of height
                    _ShaderParamsPanel.anchoredPosition = newPos;

                    ShaderSliderParam param = newMenu.GetComponent<ShaderSliderParam>();

                    //then change params according to shader:
                    param.ParameterName.text = prop.name.Substring(1);
                    param.ParameterName.name = prop.name;
                    param.Slider.minValue = prop.min;
                    param.Slider.maxValue = prop.max;
                    param.Slider.wholeNumbers = prop.whole;
                    param.Slider.name = prop.name;
                    param.Slider.SetValueWithoutNotify(prop.value);
                    param.Value.text = prop.value.ToString();
                    param.Value.name = prop.name;
                    param.Value.onValueChanged.AddListener(delegate
                    { changeShaderValueField(param.Value.gameObject); });

                    param.Slider.onValueChanged.AddListener(delegate
                    { changeShaderValue(param.Slider.gameObject); });

                    if (param != null)
                        _AdvancedParameters.Add(prop.name, param);
                }
                else
                {
                    GameObject newMenu = Instantiate(_ShaderColorPrefab.gameObject, _ShaderParamsPanel.transform);
                    Vector3 newPos = _ShaderParamsPanel.anchoredPosition;
                    newPos.y -= 17.5f;//half of height
                    _ShaderParamsPanel.anchoredPosition = newPos;

                    ShaderColorParam param = newMenu.GetComponent<ShaderColorParam>();
                    param.RValue.SetTextWithoutNotify(((int)(255f * prop.color.r)).ToString());
                    param.GValue.SetTextWithoutNotify(((int)(255f * prop.color.g)).ToString());
                    param.BValue.SetTextWithoutNotify(((int)(255f * prop.color.b)).ToString());

                    param.ParameterName.text = prop.name.Substring(1);
                    param.ParameterName.gameObject.name = prop.name;
                    param.gameObject.name = prop.name;
                    param.UpdateFields(prop.color);

                    if (param != null)
                        _AdvancedColorParameters.Add(prop.name, param);
                }
            }
        }

        public void ClearAdvancedMenu()
        {
            if (_AdvancedParameters != null)
            {
                //first clear all previous
                foreach (KeyValuePair<string, ShaderSliderParam> para in _AdvancedParameters)
                {
                    DestroyImmediate(para.Value.gameObject);
                }
                _AdvancedParameters.Clear();
            }
            _ShaderParamsPanel.GetComponent<RectTransform>().anchoredPosition = _InitAdvancedYPos;
        }

        public void RemovePresets()
        {
            foreach (GameObject g in _Presets.highlighters)
                g.SetActive(false);
        }

        //sets current selected shader and parameters from shader : used on opening shaders menu to be able to modify actual shader
        public void SetCurrentShaderSelection(Material material)
        {
            int id = GetPresetIdByName(material.name);
            _Presets.DisplayPresetsId(id);
            _Presets.SetShaderTypeDefaultAsSelected(id);

            foreach (KeyValuePair<string, ShaderSliderParam> kv in _AdvancedParameters)
            {
                string propName = "_" + kv.Value.ParameterName.text;
                if (material.HasProperty(propName))
                {
                    kv.Value.Slider.value = material.GetFloat(propName);
                    kv.Value.Value.text = material.GetFloat(propName).ToString();
                }
            }
            foreach (KeyValuePair<string, ShaderColorParam> kv in _AdvancedColorParameters)
            {
                string propName = "_" + kv.Value.ParameterName.text;
                if (material.HasProperty(propName))
                {
                    kv.Value.UpdateFields(material.GetColor(propName));
                    _NewColor.material.UpdateColorProperty(propName, material.GetColor(propName));
                }
            }
        }

        public void ActivateSelectionMainMenu(bool value)
        {
            foreach (GameObject go in SelectionMenuMainUIElements)
            {
                go.SetActive(value);
            }
        }

        private void OnEnable()
        {
            ActivateSelectionMainMenu(false);
        }

        private void OnDisable()
        {
            ActivateSelectionMainMenu(true);
        }
    }
}