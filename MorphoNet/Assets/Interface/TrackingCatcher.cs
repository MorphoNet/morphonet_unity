﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MorphoNet
{
    public class TrackingCatcher : MonoBehaviour
    {
        // Update is called once per frame
        /*
        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                //Game Objects
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform.gameObject != MorphoTools.GetBackground())
                    {
                        string path = "";
                        GameObject to_treat = hit.transform.gameObject;
                        while (to_treat != null && to_treat != MorphoTools.GetBackground() && to_treat.transform.parent != null)
                        {
                            path = to_treat.name + "," + path;
                            if (to_treat.transform.parent != null)
                                to_treat = to_treat.transform.parent.gameObject;
                            else
                                to_treat = null;
                        }

                        TrackerManagement.instance.SendTrackingInfo(path + "Click");
                        //MorphoDebug.Log("Engine : You clicked on : " + path);
                    }
                }

                //UI
                PointerEventData pointerData = new PointerEventData(EventSystem.current);

                pointerData.position = Input.mousePosition;

                List<RaycastResult> results = new List<RaycastResult>();
                EventSystem.current.RaycastAll(pointerData, results);

                if (results.Count > 0)
                {
                    if (results[0].gameObject.name == "Text")
                    {
                        string path = "";
                        GameObject to_treat = results[0].gameObject.transform.parent.gameObject;
                        while (to_treat != null && to_treat.name != "Canvas" && to_treat.transform.parent != null)
                        {
                            path = to_treat.name + "," + path;
                            if (to_treat.transform.parent != null)
                                to_treat = to_treat.transform.parent.gameObject;
                            else
                                to_treat = null;
                        }
                        TrackerManagement.instance.SendTrackingInfo(path + "Click");
                        //MorphoDebug.Log("UI : You clicked on : " + path);
                        results.Clear();
                    }
                    else
                    {
                        string path = "";
                        GameObject to_treat = results[0].gameObject;
                        while (to_treat != null && to_treat.name != "Canvas" && to_treat.transform.parent != null)
                        {
                            path = to_treat.name + "," + path;
                            if (to_treat.transform.parent != null)
                                to_treat = to_treat.transform.parent.gameObject;
                            else
                                to_treat = null;
                        }
                        TrackerManagement.instance.SendTrackingInfo(path + "Click");
                        results.Clear();
                    }
                }
            }
        }*/
    }
}