using SimpleJSON;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace MorphoNet.UI
{
    public enum MorphoSource
    {
        MorphoNet,
        MorphoPlot,
        DistantPlot
    }

    public class DatasetParameterItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public int ID;
        public int IDUser;

        public Text Name;
        public Button Button;
        public RangeSlider TimestampRangeSlider;

        public InputField MinInputField;

        public InputField MaxInputField;

        public RawImage FavIcon;

        public bool Favorite=false;

        public MorphoSource DataSource;
        public RawImage SourceIcon;

        public DateTime UploadDate;

        public Button OptionsDatasetButton;
        public Button RecomputeDataButton;
        

        public List<int> Groups = new List<int>();
        public int Species;
        public List<int> Tags = new List<int>();

        public LocalDatasetItem LocalDatasetInfo;
        public bool LocalDataset = false;

        

        private void Start()
        {
            TimestampRangeSlider.OnValueChanged.AddListener(SyncInputFieldsWithoutNotify);

            MinInputField.onValueChanged.AddListener(OnFieldLowValueChange);

            MaxInputField.onValueChanged.AddListener(OnFieldHighValueChange);

            SyncInputFieldsWithoutNotify(TimestampRangeSlider.LowValue, TimestampRangeSlider.HighValue);

            switch (DataSource)
            {
                case MorphoSource.MorphoNet:
                    SourceIcon.texture = StandaloneMainMenuInteractions.instance.MorphoNetSourceSprite.texture;
                    break;
                case MorphoSource.MorphoPlot:
                    SourceIcon.texture = StandaloneMainMenuInteractions.instance.MorphoPlotSourceSprite.texture;
                    OptionsDatasetButton.gameObject.SetActive(true);
                    //RecomputeDataButton.gameObject.SetActive(true);
                    break;
                case MorphoSource.DistantPlot:
                    SourceIcon.texture = StandaloneMainMenuInteractions.instance.DistantPlotSourceSprite.texture;
                    break;
            }
        }

        #region Timestamp selection UI update

        public void SyncInputFieldsWithoutNotify(float min, float max)
        {
            MinInputField.SetTextWithoutNotify(((int)min).ToString());
            MaxInputField.SetTextWithoutNotify(((int)max).ToString());
        }

        private void OnFieldLowValueChange(string value)
        {
            if (float.TryParse(value, out float low))
                SyncSliderWithoutNotify(Mathf.Max(low, TimestampRangeSlider.MinValue), TimestampRangeSlider.HighValue);
            else
                SyncSliderWithoutNotify(TimestampRangeSlider.LowValue, TimestampRangeSlider.HighValue);
        }

        private void OnFieldHighValueChange(string value)
        {
            if (float.TryParse(value, out float high))
                SyncSliderWithoutNotify(TimestampRangeSlider.LowValue, Mathf.Min(high, TimestampRangeSlider.MaxValue));
            else
                SyncSliderWithoutNotify(TimestampRangeSlider.LowValue, TimestampRangeSlider.HighValue);
        }

        private void SyncSliderWithoutNotify(float low, float high) => TimestampRangeSlider.SetValueWithoutNotify(low, high);


        #endregion Timestamp selection UI update

        public (int, int) TimestampRange => ((int)TimestampRangeSlider.LowValue, (int)TimestampRangeSlider.HighValue);

        public void SetActive(bool active) => gameObject.SetActive(active);

        internal void SetTimestampBounds(int minValue, int maxValue)
        {
            TimestampRangeSlider.LowValue = TimestampRangeSlider.MinValue = minValue;
            TimestampRangeSlider.HighValue = TimestampRangeSlider.MaxValue = maxValue;
            if(minValue == maxValue)
            {
                TimestampRangeSlider.gameObject.SetActive(false);
                MinInputField.gameObject.SetActive(false);
                MaxInputField.gameObject.SetActive(false);
            }
        }

        public void AddGroup(int id)
        {
            if (!HasGroup(id))
                Groups.Add(id);
        }

        public bool HasGroup(int id) => Groups.Contains(id);

        public void AddTag(int id)
        {
            if (!HasTag(id))
                Tags.Add(id);
        }

        public bool HasTag(int id) => Tags.Contains(id);

        public void SetFavorite(bool fav)
        {
            Favorite = fav;
            FavIcon.gameObject.SetActive(fav);

        }

        public void SetDate(string date)
        {
            UploadDate = DateTime.Parse(date);
        }


        public void OnPointerEnter(PointerEventData eventData)
        {
            Name.fontSize = 18;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            Name.fontSize = 16;
        }


        public void UpdateDataset(LocalDatasetItem item)
        {
            Name.text = item.Name;
            TimestampRangeSlider.MinValue = item.MinTime;
            TimestampRangeSlider.LowValue = item.MinTime;
            TimestampRangeSlider.MaxValue = item.MaxTime;
            TimestampRangeSlider.HighValue = item.MaxTime;
            MinInputField.text = (item.MinTime.ToString());
            MaxInputField.text = (item.MaxTime.ToString());
            LocalDatasetInfo = item;
        }

    }
}
