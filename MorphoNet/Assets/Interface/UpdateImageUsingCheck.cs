﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateImageUsingCheck : MonoBehaviour
{
    public Toggle source;
    public Image background;
    private Color c;

    // Start is called before the first frame update
    void Start()
    {
        if (source != null)
        {
            source.onValueChanged.AddListener(delegate (bool value) { ChangeImageColor(value); });
        }

        if (background != null)
        {
            c = background.color;
        }
    }

    // Update is called once per frame
    void ChangeImageColor(bool value)
    {
        if (value)
        {
            background.color = new Color(c.r, c.g, c.b, c.a * 2 % 255);
        }
        else
        {
            background.color = new Color(c.r, c.g, c.b, c.a);
        }
    }
}
