using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HelpLink : MonoBehaviour
{
    private string MORPHONET_URL_START = "https://morphonet.org";

    public string url;

    private Button url_button;
    // Start is called before the first frame update
    void Start()
    {
        url_button = gameObject.GetComponent<Button>();
        if (url_button != null)
        {
            url_button.onClick.RemoveAllListeners();
            url_button.onClick.AddListener(OpenURL);
        }
    }

    // Update is called once per frame
    void OpenURL()
    {
        if (!string.IsNullOrEmpty(url))
        {
#if UNITY_WEBGL
                Application.ExternalEval("window.open(\"" + url + "\")");
#else
            if (url.StartsWith(MORPHONET_URL_START))
            {
                Application.OpenURL(url);
            }
#endif
        }
    }
}
