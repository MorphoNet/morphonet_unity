using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MorphoNet
{
    public class FigureManager : MonoBehaviour
    {
        public DataSet Dataset;
        public float DisplayFactor = 1f;
        public List<Cell> CellsWithText = new List<Cell>();
        public bool display_infos_cells = false;
        public float ScaleTextInfo = 1f;
        public int FontSize = 16;
        public Text ScaleTextInfoT;
        public Text ScaleFontT;
        public bool show_axis_figure = false;
        public bool axius_figure_placed = false;
        public bool use_shortcut_names = false;
        public float scale_figure_axis = 0.03f;
        public Color TextColor = Color.white;
        public Button ShowTextInfoButton;
        public float TextRotationSpeed = 2f;
        public Dictionary<Cell, List<Cell>> SisterCells;
        public bool ShowSisterLinks = false;
        public Text DisplayLineButtonText;
        public float LineThickness = 1.5f;
        public Text FloatFeedback;
        public Button ShortCutNamesButton;
        public GameObject axis_figure;
        public Material axisMat;
        public Material defaultXMat;
        public Material defaultYMat;
        public Material defaultZMat;
        public bool show_legend = false;
        public Button show_legend_button;
        public bool show_axis = false;
        public Button show_axis_button;

        // Start is called before the first frame update
        private void Start()
        {
            show_axis_button = InterfaceManager.instance.show_axis_button;
            show_legend_button = InterfaceManager.instance.show_legend_button;
            axisMat = InterfaceManager.instance.axisMat;
            axis_figure = InterfaceManager.instance.axis_figure;
            defaultXMat = InterfaceManager.instance.defaultXMat;
            defaultYMat = InterfaceManager.instance.defaultYMat;
            defaultZMat = InterfaceManager.instance.defaultZMat;
            ScaleTextInfoT = InterfaceManager.instance.ScaleTextInfoT;
            FloatFeedback = InterfaceManager.instance.FloatFeedback;
            ScaleFontT = InterfaceManager.instance.ScaleFontT;
            ShowTextInfoButton = InterfaceManager.instance.ShowTextInfoButton;
            SisterCells = new Dictionary<Cell, List<Cell>>();
            DisplayLineButtonText = InterfaceManager.instance.DisplayLineButtonText;
            ShortCutNamesButton = InterfaceManager.instance.ShortCutNamesButton;
        }

        /// <summary>
        /// Show legend info text on axis
        /// </summary>
        public void ShowLegendInfoText()
        {
            show_legend = !show_legend;
            InterfaceManager.instance.show_legend_button.GetComponentInChildren<Text>().text = show_legend ? "Hide" : "Show";
            InterfaceManager.instance.legend_panel.SetActive(show_legend);
        }

        /// <summary>
        /// Show or hide axis for figure mode
        /// </summary>
        public void ShowRotationAxis()
        {
            show_axis = !show_axis;
            show_axis_figure = show_axis;
            show_axis_button.GetComponentInChildren<Text>().text = show_axis ? "Hide" : "Show";
            if (axis_figure.transform.parent != Dataset.embryo_container.transform)
            {
                axis_figure.transform.parent = Dataset.embryo_container.transform;
            }
            InterfaceManager.instance.figure_params.SetActive(show_axis);
            if (show_axis)
            {
                axis_figure.transform.localScale = new Vector3(scale_figure_axis, scale_figure_axis, scale_figure_axis);
                if (!axius_figure_placed)
                {
                    axius_figure_placed = true;
                    axis_figure.transform.localPosition = Vector3.zero;
                }
                axis_figure.SetActive(true);
            }
            else
            {
                axis_figure.SetActive(false);
            }
        }

        /// <summary>
        /// Color the field using a choice corresponding to dropdown UI state
        /// </summary>
        /// <param name="choice">Dropdown choice in UI</param>
        public void ColorAxisUsingField(int choice)
        {
            if (choice == 0)
            {
                InterfaceManager.instance.x_axis.transform.Find("pCone1").GetComponent<Renderer>().material = defaultXMat;
                InterfaceManager.instance.x_axis.transform.Find("Cylinder").GetComponent<Renderer>().material = defaultXMat;
                InterfaceManager.instance.x_axis_reverse.transform.Find("pCone1").GetComponent<Renderer>().material = defaultXMat;
                InterfaceManager.instance.x_axis_reverse.transform.Find("Cylinder").GetComponent<Renderer>().material = defaultXMat;
                InterfaceManager.instance.y_axis.transform.Find("pCone1").GetComponent<Renderer>().material = defaultYMat;
                InterfaceManager.instance.y_axis.transform.Find("Cylinder").GetComponent<Renderer>().material = defaultYMat;
                InterfaceManager.instance.y_axis_reverse.transform.Find("pCone1").GetComponent<Renderer>().material = defaultYMat;
                InterfaceManager.instance.y_axis_reverse.transform.Find("Cylinder").GetComponent<Renderer>().material = defaultYMat;
                InterfaceManager.instance.z_axis.transform.transform.Find("pCone1").GetComponent<Renderer>().material = defaultZMat;
                InterfaceManager.instance.z_axis.transform.Find("Cylinder").GetComponent<Renderer>().material = defaultZMat;
                InterfaceManager.instance.z_axis_reverse.transform.Find("pCone1").GetComponent<Renderer>().material = defaultZMat;
                InterfaceManager.instance.z_axis_reverse.transform.Find("Cylinder").GetComponent<Renderer>().material = defaultZMat;
            }
            else if (choice == 1)
            {
                axisMat.color = Color.black;
                InterfaceManager.instance.x_axis.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.x_axis.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.x_axis_reverse.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.x_axis_reverse.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.y_axis.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.y_axis.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.y_axis_reverse.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.y_axis_reverse.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.z_axis.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.z_axis.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.z_axis_reverse.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.z_axis_reverse.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
            }
            else if (choice == 2)
            {
                axisMat.color = Color.gray;
                InterfaceManager.instance.x_axis.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.x_axis.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.x_axis_reverse.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.x_axis_reverse.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.y_axis.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.y_axis.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.y_axis_reverse.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.y_axis_reverse.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.z_axis.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.z_axis.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.z_axis_reverse.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.z_axis_reverse.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
            }
            else if (choice == 3)
            {
                axisMat.color = Color.white;
                InterfaceManager.instance.x_axis.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.x_axis.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.x_axis_reverse.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.x_axis_reverse.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.y_axis.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.y_axis.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.y_axis_reverse.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.y_axis_reverse.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.z_axis.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.z_axis.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.z_axis_reverse.transform.Find("pCone1").GetComponent<Renderer>().material = axisMat;
                InterfaceManager.instance.z_axis_reverse.transform.Find("Cylinder").GetComponent<Renderer>().material = axisMat;
            }
        }

        /// <summary>
        /// Attach text axis parent to canvas to avoid rotate bug
        /// </summary>
        public void DetachAxisText()
        {
            InterfaceManager.instance.x_axis.transform.Find("x").transform.parent = InterfaceManager.instance.canvas.transform;
            InterfaceManager.instance.x_axis_reverse.transform.Find("x_r").transform.parent = InterfaceManager.instance.canvas.transform;
            InterfaceManager.instance.y_axis.transform.Find("y").transform.parent = InterfaceManager.instance.canvas.transform;
            InterfaceManager.instance.y_axis_reverse.transform.Find("y_r").transform.parent = InterfaceManager.instance.canvas.transform;
            InterfaceManager.instance.z_axis.transform.Find("z").transform.parent = InterfaceManager.instance.canvas.transform;
            InterfaceManager.instance.z_axis_reverse.transform.Find("z_r").transform.parent = InterfaceManager.instance.canvas.transform;
        }

        /// <summary>
        /// Attach text axis parent
        /// </summary>
        public void ReattachAxisText()
        {
            InterfaceManager.instance.canvas.transform.Find("x").transform.parent = InterfaceManager.instance.x_axis.transform;
            InterfaceManager.instance.canvas.transform.Find("x_r").transform.parent = InterfaceManager.instance.x_axis_reverse.transform;
            InterfaceManager.instance.canvas.transform.Find("y").transform.parent = InterfaceManager.instance.y_axis.transform;
            InterfaceManager.instance.canvas.transform.Find("y_r").transform.parent = InterfaceManager.instance.y_axis_reverse.transform;
            InterfaceManager.instance.canvas.transform.Find("z").transform.parent = InterfaceManager.instance.z_axis.transform;
            InterfaceManager.instance.canvas.transform.Find("z_r").transform.parent = InterfaceManager.instance.z_axis_reverse.transform;
        }

        /// <summary>
        /// Switch names between fully displayed or shortcut (only working on acidian names)
        /// </summary>
        public void HandleShortcutName()
        {
            use_shortcut_names = !use_shortcut_names;
            string display_text = use_shortcut_names ? "Short names" : "Full names";
            ShortCutNamesButton.transform.GetComponentInChildren<Text>().text = display_text;
            UpdateInfoCellsText();
        }

        /// <summary>
        /// Change the scale of the figure axes
        /// </summary>
        /// <param name="scale"> The new scale for each axis</param>
        public void ScaleFigureAxis(float scale)
        {
            scale_figure_axis = scale;
            //InterfaceManager.instance.x_text.transform.parent = null;
            axis_figure.transform.localScale = new Vector3(scale_figure_axis, scale_figure_axis, scale_figure_axis);
            InterfaceManager.instance.scale_text.text = scale.ToString("F3");
        }

        /// <summary>
        /// Show or hide the X axis for figure mode
        /// </summary>
        /// <param name="show"> show or hide</param>
        public void ShowX(bool show)
        {
            InterfaceManager.instance.x_axis.SetActive(show);
            InterfaceManager.instance.x_axis_reverse.SetActive(show);
        }

        /// <summary>
        /// Show or hide the Y axis for figure mode
        /// </summary>
        /// <param name="show"> show or hide</param>
        public void ShowY(bool show)
        {
            InterfaceManager.instance.y_axis.SetActive(show);
            InterfaceManager.instance.y_axis_reverse.SetActive(show);
        }

        /// <summary>
        /// Show or hide the Z axis for figure mode
        /// </summary>
        /// <param name="show"> show or hide</param>
        public void ShowZ(bool show)
        {
            InterfaceManager.instance.z_axis.SetActive(show);
            InterfaceManager.instance.z_axis_reverse.SetActive(show);
        }

        /// <summary>
        /// Update the text for forward axis in X for figure mode
        /// </summary>
        /// <param name="text"> Value of text </param>
        public void UpdateXTextFigureAxis(string text)
        {
            InterfaceManager.instance.x_axis_text.text = text;
        }

        /// <summary>
        /// Update the text for forward axis in X reversed for figure mode
        /// </summary>
        /// <param name="text"> Value of text </param>
        public void UpdateXReverseTextFigureAxis(string text)
        {
            InterfaceManager.instance.x_axis_text_reverse.text = text;
        }

        /// <summary>
        /// Update the text for forward axis in Y for figure mode
        /// </summary>
        /// <param name="text"> Value of text </param>
        public void UpdateYTextFigureAxis(string text)
        {
            InterfaceManager.instance.y_axis_text.text = text;
        }

        /// <summary>
        /// Update the text for forward axis in Y reversed for figure mode
        /// </summary>
        /// <param name="text"> Value of text </param>
        public void UpdateYReverseTextFigureAxis(string text)
        {
            InterfaceManager.instance.y_axis_text_reverse.text = text;
        }

        /// <summary>
        /// Update the text for forward axis in Z for figure mode
        /// </summary>
        /// <param name="text"> Value of text </param>
        public void UpdateZTextFigureAxis(string text)
        {
            InterfaceManager.instance.z_axis_text.text = text;
        }

        /// <summary>
        /// Update the text for forward axis in Z reversed for figure mode
        /// </summary>
        /// <param name="text"> Value of text </param>
        public void UpdateZReverseFigureAxis(string text)
        {
            InterfaceManager.instance.z_axis_text_reverse.text = text;
        }

        /// <summary>
        /// Scale the axis text for figure mode
        /// </summary>
        /// <param name="scale">The new scale </param>
        public void ScaleFigureLegend(float scale)
        {
            InterfaceManager.instance.x_axis_text.transform.localScale = new Vector3(scale, scale, scale);
            InterfaceManager.instance.x_axis_text_reverse.transform.localScale = new Vector3(scale, scale, scale);
            InterfaceManager.instance.y_axis_text.transform.localScale = new Vector3(scale, scale, scale);
            InterfaceManager.instance.y_axis_text_reverse.transform.localScale = new Vector3(scale, scale, scale);
            InterfaceManager.instance.z_axis_text.transform.localScale = new Vector3(scale, scale, scale);
            InterfaceManager.instance.z_axis_text_reverse.transform.localScale = new Vector3(scale, scale, scale);
            InterfaceManager.instance.scale_legend.text = scale.ToString("F3");
        }

        /// <summary>
        /// Reset axis parameters to default values for figure mode
        /// </summary>
        public void ResetAxis()
        {
            InterfaceManager.instance.x_text.text = "Green";
            InterfaceManager.instance.x_text_r.text = "Green";
            InterfaceManager.instance.y_text.text = "Blue";
            InterfaceManager.instance.y_text_r.text = "Blue";
            InterfaceManager.instance.z_text.text = "Red";
            InterfaceManager.instance.z_text_r.text = "Red";
            InterfaceManager.instance.scale_slid.value = 0.03f;
            InterfaceManager.instance.scale_text_slid.value = 1f;
            InterfaceManager.instance.showX.isOn = true;
            InterfaceManager.instance.showY.isOn = true;
            InterfaceManager.instance.showZ.isOn = true;
            InterfaceManager.instance.color_axis.value = 0;
            InterfaceManager.instance.axis_figure.transform.rotation = Quaternion.identity;
        }

        /// <summary>
        /// Update the thickness of the link for future and picked cells
        /// </summary>
        /// <param name="scale">The new thickness for links</param>
        public void UpdateLineThickness(float scale)
        {
            LineThickness = scale;
            FloatFeedback.text = scale.ToString();
            UpdateSisterLinks();
        }

        /// <summary>
        /// For each picked cell with a link , update the link (position + parameters)
        /// </summary>
        public void UpdateSisterLinks()
        {
            foreach (Cell cell in Dataset.PickedManager.clickedCells)
            {
                if (SisterCells.ContainsKey(cell))
                {
                    cell.getFirstChannel().DestroyAllLines();
                    List<Cell> findSitstersCells = MorphoTools.FindSisterCells(cell);

                    SisterCells.Remove(cell);

                    foreach (Cell findSisterCell in findSitstersCells)
                    {
                        if (findSisterCell != cell && SisterCells.ContainsKey(findSisterCell) && SisterCells[findSisterCell] != null && SisterCells[findSisterCell].Contains(cell))
                        {
                            CellChannelRenderer findSisterCellChannel = findSisterCell.getFirstChannel();

                            findSisterCellChannel.DestroySisterLines(cell);
                            findSisterCellChannel.DisplaySisterLines(cell);

                            SisterCells[findSisterCell].Remove(cell);
                            SisterCells[findSisterCell].Add(cell);
                        }
                    }
                    ShowSisterLinksCell(cell);
                }
            }
        }

        /// <summary>
        /// Delete existing sister links for picked cells
        /// </summary>
        public void ClearSisterLinks()
        {
            ShowSisterLinks = false;
            foreach (Cell cell in Dataset.PickedManager.clickedCells)
            {
                cell.getFirstChannel().DestroyAllLines();
                SisterCells.Remove(cell);
            }
        }

        public void ClearSisterLinks(List<Cell> cells)
        {
            ShowSisterLinks = false;
            foreach (Cell cell in Dataset.PickedManager.clickedCells)
            {
                cell.getFirstChannel().DestroyAllLines();
                SisterCells.Remove(cell);
            }
        }

        /// <summary>
        /// Create the links with sisters cells for each picked cell
        /// </summary>
        public void DisplaySisterLinks()
        {
            ShowSisterLinks = true;
            foreach (Cell cell in Dataset.PickedManager.clickedCells)
            {
                ShowSisterLinksCell(cell);
            }
        }

        /// <summary>
        /// Store the dataset for this FigureManager
        /// </summary>
        /// <param name="d">The dataset to link</param>
        public void AttachDataset(DataSet d)
        {
            Dataset = d;
        }

        #region FIGURE_REGION

        /// <summary>
        /// Modify the font size of the figure text for picked cells and future ones
        /// </summary>
        /// <param name="text"> New font size of the text</param>
        public void SetFontSize(float font)
        {
            FontSize = (int)font;
            ScaleFontT.text = ((int)font).ToString();
            Dataset.FigureManager.UpdateInfoCellsText();
        }

        /// <summary>
        /// Modify the scale of the figure text for picked cells and future ones
        /// </summary>
        /// <param name="text"> New scale of the text</param>
        public void scaleTextInfo(float text)
        {
            ScaleTextInfo = text;
            ScaleTextInfoT.text = text.ToString();
            Dataset.FigureManager.UpdateInfoCellsText();
        }

        /// <summary>
        /// Pick all cells with a figure text displayed
        /// </summary>
        public void PickTextedCells()
        {
            foreach (Cell cell in CellsWithText)
            {
                Dataset.PickedManager.AddCellSelected(cell);
            }
#if !UNITY_WEBGL
            MorphoTools.SendCellsPickedToLineage();
#endif
        }

        /// <summary>
        /// Handle timechange from Dataset, then apply it to Links and Text
        /// </summary>
        /// <param name="new_time"></param>
        public void ManageTimeChange(int new_time, int old_time)
        {
            if (CellsWithText != null && CellsWithText.Count > 0)
            {
                ChangeTimeText(new_time, old_time);
            }

            if (SisterCells != null && SisterCells.Count > 0)
            {
                ChangeTimeLink(new_time, old_time);
            }
        }

        /// <summary>
        /// Handle time change for text
        /// </summary>
        /// <param name="new_time">The new time point</param>
        public void ChangeTimeText(int new_time, int old_time)
        {
            List<Cell> daughters = new List<Cell>();
            foreach (Cell b in CellsWithText)
            {
                if (new_time > old_time)
                {
                    if (b.Daughters != null && b.Daughters.Count > 0)
                    {
                        daughters.AddRange(b.Daughters);
                    }
                }
                else
                {
                    if (b.Mothers != null && b.Mothers.Count > 0)
                    {
                        daughters.AddRange(b.Mothers);
                    }
                }
                b.getFirstChannel().DestroyInfoDisplay();
            }
            CellsWithText.Clear();
            DisplayInfoCells(daughters);
        }

        /// <summary>
        /// Handle time change for sisters link
        /// </summary>
        /// <param name="new_time">The new time point</param>
        public void ChangeTimeLink(int new_time, int old_time)
        {
            List<Cell> cells = new List<Cell>();
            List<Cell> daughters = new List<Cell>();
            foreach (KeyValuePair<Cell, List<Cell>> b in SisterCells)
            {
                cells.Add(b.Key);
                if (new_time > old_time)
                {
                    if (b.Key.Daughters != null && b.Key.Daughters.Count > 0)
                    {
                        daughters.AddRange(b.Key.Daughters);
                    }
                }
                else
                {
                    if (b.Key.Mothers != null && b.Key.Mothers.Count > 0)
                    {
                        daughters.AddRange(b.Key.Mothers);
                    }
                }
            }
            ClearSisterLinks(cells);
            SisterCells.Clear();
            foreach (Cell d in daughters)
            {
                ShowSisterLinksCell(d);
            }
        }

        /// <summary>
        /// Update the posiition (but not the text) for each figure text displayed
        /// </summary>
        public void MoveTextCells()
        {
            foreach (Cell b in CellsWithText)
            {
                b.getFirstChannel().UpdateDisplayInfoPosition();
            }
        }

        /// <summary>
        /// For each object picked and with information figure text displayed, update completely the figure text
        /// </summary>
        public void UpdateInfoCells()
        {
            foreach (Cell b in Dataset.PickedManager.clickedCells)
            {
                if (CellsWithText.Contains(b))
                {
                    b.getFirstChannel().DestroyInfoDisplay();
                    CellsWithText.Remove(b);

                    if (b.getFirstChannel().DisplayTextMeshPro() > 0)
                    {
                        CellsWithText.Add(b);
                    }
                }
            }
        }

        /// <summary>
        /// For each object picked and with information text displayed, update the text but not the position
        /// </summary>
        public void UpdateInfoCellsText()
        {
            foreach (Cell b in Dataset.PickedManager.clickedCells)
            {
                if (CellsWithText.Contains(b))
                {
                    b.getFirstChannel().UpdateDisplayInfoNoPosition();
                }
            }
        }

        /// <summary>
        /// Create link between the cell in parameter and its sister cells
        /// </summary>
        public void ShowSisterLinksCell(Cell b)
        {
            if (ShowSisterLinks)
            {
                List<Cell> sisters_cells = MorphoTools.FindSisterCells(b);
                //MorphoDebug.Log(sisters_cells.Count);
                if (sisters_cells != null && sisters_cells.Count > 0)
                {
                    if (SisterCells == null)
                    {
                        SisterCells = new Dictionary<Cell, List<Cell>>();
                    }

                    if (!SisterCells.ContainsKey(b))
                    {
                        SisterCells.Add(b, new List<Cell>());
                    }
                    //SisterCells = MorphoTools.AddKeyToDict(SisterCells, b);
                    foreach (Cell sister in sisters_cells)
                    {
                        if (!SisterCells[b].Contains(sister))
                        {
                            SisterCells[b].Add(sister);
                        }
                        if (!SisterCells.ContainsKey(sister) || (SisterCells.ContainsKey(sister) && !SisterCells[sister].Contains(b)))
                            b.getFirstChannel().DisplaySisterLines(sister);
                    }
                }
            }
        }

        /// <summary>
        /// For each object picked , create the figure text and store it. If it's already displayed , update it
        /// </summary>
        public void DisplayInfoCells(List<Cell> cells_list)
        {
            display_infos_cells = true;

            foreach (Cell b in cells_list)
            {
                if (b.show)
                {
                    if (!CellsWithText.Contains(b))
                    {
                        if (b.getFirstChannel().DisplayTextMeshPro() > 0)
                            CellsWithText.Add(b);
                    }
                    else
                    {
                        b.getFirstChannel().DestroyInfoDisplay();
                        CellsWithText.Remove(b);

                        if (b.getFirstChannel().DisplayTextMeshPro() > 0)
                        {
                            CellsWithText.Add(b);
                        }
                    }

                    ShowSisterLinksCell(b);
                }
            }
        }

        /// <summary>
        /// For each picked cell with a figure text, clear the corresponding text
        /// </summary>
        public void ClearInfoCells()
        {
            display_infos_cells = false;
            foreach (Cell b in Dataset.PickedManager.clickedCells)
            {
                if (CellsWithText.Contains(b))
                {
                    b.getFirstChannel().DestroyInfoDisplay();
                    CellsWithText.Remove(b);
                }
            }
        }

        #endregion FIGURE_REGION
    }
}