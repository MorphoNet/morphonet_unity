using UnityEngine;

namespace MorphoNet
{
    [ExecuteInEditMode]
    public class FixedScale : MonoBehaviour
    {
        private float FixeScale = 1;
        public GameObject parent;

        // Update is called once per frame
        private void Update()
        {
            FixeScale = MorphoTools.GetFigureManager().ScaleTextInfo;
            transform.localScale = new Vector3(FixeScale / parent.transform.localScale.x, FixeScale / parent.transform.localScale.y, FixeScale / parent.transform.localScale.z);
        }
    }
}