﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace MorphoNet
{
    public class TrackerManagement : MonoBehaviour
    {
        public static TrackerManagement instance = null;

        private void Awake()
        {
            if (instance == null)
                instance = this;
            else
                Destroy(gameObject);
        }

        public void SendTrackingInfo(string feature)
        {
            if (LoadParameters.instance.liveMode != true && LoadParameters.instance.rgpd)
                StartCoroutine(SendTrackingInfo_Routine(feature));
        }

        public IEnumerator SendTrackingInfo_Routine(string feature)
        {
            int user_id = 0;
            int dataset_id = 0;
            if (LoadParameters.instance != null && LoadParameters.instance.id_user != -1)
            {
                user_id = LoadParameters.instance.id_user;
            }

            if (LoadParameters.instance != null && LoadParameters.instance.id_dataset != -1)
            {
                dataset_id = LoadParameters.instance.id_dataset;
            }

            string prefix = "";

            prefix = Application.platform.ToString() + ",";
            //#if UNITY_EDITOR
            //        prefix = "UnityEditor,";
            //#else
            //	    prefix = "Viewer,";
            //#endif
            string final_feature = prefix + feature;
            //MorphoDebug.Log(final_feature);
            WWWForm form = new WWWForm();
            form.AddField("user_id", user_id);
            form.AddField("id_dataset", dataset_id);
            form.AddField("feature_list", final_feature);
            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/storeuserinteraction/", form);

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            { MorphoDebug.Log("Error during interaction storing: " + www.error); }
        }
    }
}