﻿using AssemblyCSharp;

using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MorphoNet
{
    public class PickedManager : MonoBehaviour
    {
        public Cell selectedCell; //The last selected Cell
        public List<Cell> clickedCells; //List of cells currently selected

        public List<Cell> HighlightedCells = new List<Cell>();//highlighted cells. the list represents the labels applied to the cells to be able to toggle rendering easily
        public bool HighlightMode = false;
        public bool HighlightToggle = false;

        private RectTransform _BoxSelectionArea;
        private DataSet _AttachedDataset;

        public void AttachDataset(DataSet dataset)
        {
            _AttachedDataset = dataset;
        }

        private void Start()
        {
            _BoxSelectionArea = InterfaceManager.instance.BoxSelectionArea;
            BoutonLayoutVisible(true);
            clickedCells = new List<Cell>();
            selectedCell = null;
        }

        private void BoutonLayoutVisible(bool active)
        {
            InterfaceManager.instance.MenuClicked.transform.Find("Bouton layout").Find("ResetSelected").GetComponent<Button>().interactable = active;
            InterfaceManager.instance.MenuClicked.transform.Find("Bouton layout").Find("RandomSelected").GetComponent<Button>().interactable = active;
            InterfaceManager.instance.MenuClicked.transform.Find("Bouton layout").Find("HideSelected").Find("New Button").GetComponent<Button>().interactable = active;
            InterfaceManager.instance.MenuClicked.transform.Find("Bouton layout").Find("ShowSelected").Find("ShowSelectedAll").GetComponent<Button>().interactable = active;
            InterfaceManager.instance.MenuClicked.transform.Find("Bouton layout").Find("HideSelected").Find("HideSelected All").GetComponent<Button>().interactable = active;
            InterfaceManager.instance.MenuClicked.transform.Find("Bouton layout").Find("Sisters").GetComponent<Button>().interactable = active;
            InterfaceManager.instance.MenuClicked.transform.Find("Bouton layout").Find("NeighborsSelect").GetComponent<Button>().interactable = active;
        }

        //Update Menu Aniseed Cells description
        public void UpdateGeneCells()
        {
            GeneticManager geneticManager = _AttachedDataset.GeneticManager;
            if (geneticManager != null && geneticManager.MenuGenetic != null && geneticManager.MenuGenetic.activeSelf)
                if (geneticManager.isAnnotate || geneticManager.ByWhat != "Genes")
                    geneticManager.onshowSelectedCells();
        }

        //Clear The List of selected Cells
        public void ClearSelectedCells(Cell exception = null)
        {
            List<Cell> cells = new List<Cell>();
            if (clickedCells != null)
            {
                foreach (Cell c in clickedCells)
                {
                    if(c != exception)
                    {
                        c.choose(false, false);
                        cells.Add(c);
                        if (InterfaceManager.instance.buttonSimulation.activeSelf && !c.ID.Contains("-"))
                            StartCoroutine(SetsManager.instance.directPlot.SendDeletePlugLPY(c.t, c.ID));
                    }
                    
                }

                clickedCells.Clear();

#if UNITY_WEBGL
            if (Lineage.isLineage)
                Lineage.ManagePickedCells(cells, false);
#else
                if (cells.Count > 0)
                {
                    MorphoTools.SendClearPickedToLineage(cells);
                }
                
#endif

            }
            UpdateGeneCells();
        }

        //Clear The Menu of the List of selected Cells
        public void ClearSelectedCellsMenu()
        {
            List<Cell> cells = new List<Cell>();
            if (clickedCells != null)
            {
                List<Cell> new_cells = new List<Cell>(clickedCells);
                foreach (Cell c in clickedCells)
                {
                    if (InterfaceManager.instance.buttonSimulation.activeSelf && !c.ID.Contains("-"))
                    {
                        var directPlot = SetsManager.instance.directPlot;
                        if (directPlot != null)
                            StartCoroutine(directPlot.SendDeletePlugLPY(c.t, c.ID));
                    }
                }
            }
        }

        //Clear the click cell
        public void ClearClickedCell()
        {
            Cell previous_selected = selectedCell;

            if (selectedCell != null)
            {
                if (InterfaceManager.instance.buttonSimulation.activeSelf)
                    StartCoroutine(SetsManager.instance.directPlot.SendDeletePlugLPY(selectedCell.t, selectedCell.ID));
                selectedCell = null;
            }

            if (_AttachedDataset.Infos != null)
                _AttachedDataset.Infos.describe();


            UpdateGeneCells();
        }

        //Follow selected Cells
        public void FollowSelectedCells(int t)
        {
            if (clickedCells != null)
            {
                List<Cell> NewClickedCells = new List<Cell>();
                foreach (Cell c in clickedCells)
                {
                    c.choose(false, false);

                    List<Cell> Parents = c.GetParentAt(t);

                    if (Parents != null)
                    {
                        for (int k = 0; k < Parents.Count; k++)
                        {
                            if (!NewClickedCells.Contains(Parents[k]))
                            {
                                Parents[k].choose(true, false);
                                NewClickedCells.Add(Parents[k]);
                                if (c == selectedCell)
                                    selectedCell = Parents[k]; //Update Selected Cell
                            }
                        }
                    }
                }

                

                if (Lineage.isLineage)
                {
                    Lineage.ManagePickedCells(clickedCells, false);
                    Lineage.ManagePickedCells(NewClickedCells, true);
                }

#if !UNITY_WEBGL
                if (InterfaceManager.instance.lineage_viewer != null && InterfaceManager.instance.lineage_viewer.lineage_shown && clickedCells.Count>0)
                {
                    InterfaceManager.instance.lineage_viewer.SendClearPicked(clickedCells);
                }
#endif

                clickedCells.Clear();
                clickedCells = NewClickedCells;

#if !UNITY_WEBGL
                if (InterfaceManager.instance.lineage_viewer != null && InterfaceManager.instance.lineage_viewer.lineage_shown && NewClickedCells.Count>0)
                {
                    InterfaceManager.instance.lineage_viewer.SendCellsPicked();
                }
#endif

                if (HighlightMode || HighlightToggle)
                {
                    ToggleHighlightMode(false);
                    ToggleHighlightMode(true);
                }

                UpdateGeneCells();
            }
        }

        public bool SelectCell(Cell c,bool zoom =false)
        {
            if (c != null)
            {
                //MorphoDebug.Log ("Cell  found ..." + p);
                ClearSelectedCells();
                ClearClickedCell();
                selectedCell = c;
                if (MorphoTools.GetDataset().CurrentTime != c.t)
                    InterfaceManager.instance.setTime(c.t);
                AddCellSelected(MorphoTools.GetPickedManager().selectedCell, true, false);

                
                //add send back picked cell ? probably better to do it in a place where it's constantly called
                //SendCellsPicked(zoom);
#if !UNITY_WEBGL
                MorphoTools.SendCellsPickedToLineage(zoom);
#endif

                if (HighlightMode || HighlightToggle)
                {
                    ToggleHighlightMode(false);
                    ToggleHighlightMode(true);
                }
                return true;
            }
            else
                MorphoDebug.Log("Cell not found ...");
            return false;
        }

        //Remove a selected cell
        public void RemoveCellSelected(Cell c, bool updateDescribe = true)
        {
            List<Cell> cells = new List<Cell>();

            c.choose(false, false);
            cells.Add(c);

            if (clickedCells != null && clickedCells.Contains(c))
            {
                clickedCells.Remove(c);
                if (InterfaceManager.instance.buttonSimulation.activeSelf && !c.ID.Contains("-"))
                {
                    var directPlot = SetsManager.instance.directPlot;
                    if (directPlot != null)
                        StartCoroutine(directPlot.SendDeletePlugLPY(c.t, c.ID));
                }
            }

            if (updateDescribe)
                _AttachedDataset.Infos.describe(); //Update the description menu

            UpdateGeneCells();
#if !UNITY_WEBGL
            if (InterfaceManager.instance.lineage_viewer != null && InterfaceManager.instance.lineage_viewer.lineage_shown)
            {
                InterfaceManager.instance.lineage_viewer.SendClearPicked(cells);
            }
#endif

            if (Lineage.isLineage)
                Lineage.ManagePickedCells(cells, false);
        }

        public void AddCellSelectedOnly(Cell c, bool updateDescribe = true)
        {
            List<Cell> cells = new List<Cell>();

            c.choose(true, false);
            cells.Add(c);

            if (clickedCells != null && !clickedCells.Contains(c))
            {
                clickedCells.Add(c);

                if (InterfaceManager.instance.lineage_viewer != null && InterfaceManager.instance.lineage_viewer.lineage_shown)
                {
                    //MorphoMorphoDebug.LogError("Implement this lineage communication");
                    /* InterfaceManager.instance.lineage_viewer.last_click_cell = c;
                     InterfaceManager.instance.lineage_viewer.RedrawBranch(c);
                     InterfaceManager.instance.lineage_viewer.LoadUIPanel(c);*/

                }
            }

            UpdateGeneCells();

            SetsManager.instance.DataSet.selection_manager.UpdatePickedNumberCells();

            if (_AttachedDataset.Curations != null && _AttachedDataset.Curations.MenuCurration.activeSelf)
                _AttachedDataset.Curations.showCuration();

            if (Lineage.isLineage)
                Lineage.ManagePickedCells(cells, true);
        }

        //Add a cell in the selection
        public void AddCellSelected(Cell c, bool updateDescribe = true, bool lineagezoom = true)
        {
            List<Cell> cells = new List<Cell>();
            List<Cell> sisters = new List<Cell>();

            c.choose(true, false);
            cells.Add(c);
            
            if (clickedCells != null && !clickedCells.Contains(c))
            {
                clickedCells.Add(c);
                if (InterfaceManager.instance.buttonSimulation.activeSelf)
                {
                    if (c.ID.Contains("-"))
                    {
                        string id = c.ID.Split('-')[0];
                        string id_ = id + "-";
                        if (InterfaceManager.instance.MenuSimulation.activeSelf)
                            StartCoroutine(SetsManager.instance.directPlot.SendCreatePlugLPY(c.t, id)); // il faut ajouter l'apparition quand le menu est ouvert
                        foreach (Cell cell in _AttachedDataset.CellsByTimePoint[c.t])
                        {
                            if (cell.ID == id || (cell.ID.Contains(id_) && cell.ID != c.ID))
                            {
                                sisters.Add(cell);
                            }
                        }
                    }
                    else
                    {
                        if (InterfaceManager.instance.MenuSimulation.activeSelf)
                            StartCoroutine(SetsManager.instance.directPlot.SendCreatePlugLPY(c.t, c.ID));
                        foreach (Cell cell in _AttachedDataset.CellsByTimePoint[c.t])
                        {
                            if (cell.ID.Contains("-"))
                            {
                                string id = cell.ID.Split('-')[0];
                                if (id == c.ID)
                                    sisters.Add(cell);
                            }
                        }
                    }
                }
/*#if !UNITY_WEBGL
                if (InterfaceManager.instance.lineage_viewer != null && InterfaceManager.instance.lineage_viewer.lineage_shown)
                {
                    InterfaceManager.instance.lineage_viewer.SendCellsPicked(lineagezoom);
                }
#endif*/
                SetsManager.instance.DataSet.selection_manager.UpdatePickedNumberCells();

            }

            UpdateGeneCells();


            if (_AttachedDataset.Curations != null && _AttachedDataset.Curations.MenuCurration.activeSelf)
                _AttachedDataset.Curations.showCuration();

            if (Lineage.isLineage)
                Lineage.ManagePickedCells(cells, true);

            for (int i = 0; i < sisters.Count; i++)
            {
                AddCellSelectedOnly(sisters[i], updateDescribe);
            }
            

        }

        //Add menu of a cell in the selection
        public void AddSelectedCellsMenu()
        {
            foreach (Cell c in clickedCells)
            {
                if (!c.ID.Contains("-"))
                    StartCoroutine(SetsManager.instance.directPlot.SendCreatePlugLPY(c.t, c.ID));
            }
        }

        //Return the selection number from the selected list if exist;
        public int GetSelection()
        {
            if (clickedCells.Count > 0)
                if (clickedCells[0].selection != null && clickedCells[0].selection.Count > 0)
                    return clickedCells[0].selection[0];

            return 0;
        }

        //Clear All selected cells
        public void ResetSelection()
        {
            List<Cell> new_cells = new List<Cell>(clickedCells);

            ClearSelectedCells();
            ClearClickedCell();


#if UNITY_WEBGL
            if (Lineage.isLineage)
                Lineage.UpdateWebsiteLineage();
#endif

        }

        public void ResetStaticSelection()
        {
            List<Cell> new_cells = new List<Cell>(clickedCells);

            ClearSelectedCells();
            ClearClickedCell();

#if UNITY_WEBGL
            if (Lineage.isLineage)
                Lineage.UpdateWebsiteLineage();
#endif

        }

        //Select the others cells none selected
        public void InverseSelection()
        {
            List<Cell> selected_cells = new List<Cell>(clickedCells);
            clickedCells.Clear();
            foreach (Cell cell in _AttachedDataset.CellsByTimePoint[_AttachedDataset.CurrentTime])
            {
                var directPlot = SetsManager.instance.directPlot;
                if (cell.show)
                {
                    if (!cell.selected)
                    {
                        clickedCells.Add(cell);
                        if (InterfaceManager.instance.MenuSimulation.activeSelf && directPlot != null)
                            StartCoroutine(directPlot.SendCreatePlugLPY(cell.t, cell.ID));
                    }
                    else if (InterfaceManager.instance.buttonSimulation.activeSelf)
                    {
                        if (directPlot != null)
                            StartCoroutine(directPlot.SendDeletePlugLPY(cell.t, cell.ID));
                    }

                    cell.choose(!cell.selected, false);
                }
            }
#if !UNITY_WEBGL
            if (InterfaceManager.instance.lineage_viewer != null && InterfaceManager.instance.lineage_viewer.lineage_shown)
            {
                InterfaceManager.instance.lineage_viewer.SendCellsPicked();
                InterfaceManager.instance.lineage_viewer.SendClearPicked(selected_cells);
            }
#endif


#if UNITY_WEBGL
            if (Lineage.isLineage)
            {
                Lineage.ManagePickedCells(selected_cells, false);
                Lineage.ManagePickedCells(clickedCells, true);
            }
#endif


            if (_AttachedDataset.Infos != null)
                _AttachedDataset.Infos.describe();

            if (HighlightMode || HighlightToggle)
            {
                ToggleHighlightMode(false);
                ToggleHighlightMode(true);
            }

            UpdateGeneCells();
        }

        //Add the neigbhors of the clicked cell in the list of selectd cells
        public void ShowNeigbhors()
        {
            List<Cell> temp_clickedCells = new List<Cell>();
            for (int i = 0; i < clickedCells.Count; i++)
                temp_clickedCells.Add(clickedCells[i]);

            for (int i = 0; i < temp_clickedCells.Count; i++)
            {
                if (temp_clickedCells[i].Neigbhors != null)
                    foreach (Cell n in temp_clickedCells[i].Neigbhors)
                    {
                        bool isInside = false;

                        n.choose(true, false);

                        for (int ix = 0; ix < clickedCells.Count; ix++)
                            if (clickedCells[ix] == n)
                                isInside = true;

                        if (!isInside)
                            clickedCells.Add(n);
                    }
            }


#if UNITY_WEBGL
            if (Lineage.isLineage)
                Lineage.ManagePickedCells(clickedCells, true);
#endif


            if (_AttachedDataset.Infos != null)
                _AttachedDataset.Infos.describe(); //Update the description menu

            UpdateGeneCells();
        }

        public void ShowSisters()
        {
            List<Cell> temp_clickedCells = new List<Cell>();

            for (int i = 0; i < clickedCells.Count; i++)
                temp_clickedCells.Add(clickedCells[i]);

            for (int i = 0; i < temp_clickedCells.Count; i++)
            {
                List<Cell> sisters = MorphoTools.FindSisterCells(temp_clickedCells[i]);
                if (sisters != null && sisters.Count > 0)
                    foreach (Cell s in sisters)
                        AddCellSelected(s);
            }

#if UNITY_WEBGL
            if (Lineage.isLineage)
                Lineage.UpdateWebsiteLineage();
#else
            MorphoTools.SendCellsPickedToLineage();
#endif


            if (_AttachedDataset.Infos != null)
                _AttachedDataset.Infos.describe(); //Update the description menu

            UpdateGeneCells();
        }

        public void HideClickedCell(bool all_alltimes)
        {
            HideClickedCell_Coroutine(all_alltimes);
        }

        //Hide the selected cells
        private void HideClickedCell_Coroutine(bool all_alltimes)
        {
#if UNITY_WEBGL
            if (Lineage.isLineage)
            {
                if (all_alltimes)
                    Lineage.hideCellListAllTimes(MorphoTools.GetPickedManager().clickedCells, "picked");
                else
                    Lineage.hideCellListCurrentTimes(MorphoTools.GetPickedManager().clickedCells, "picked");
            }
#endif
            if (clickedCells != null)
            {
                //We Hide
                if (all_alltimes)
                {
                    //ALL TIMES
                    for (int i = 0; i < clickedCells.Count; i++)
                        clickedCells[i].hideAll(true, false);
                }
                else
                {
                    //ONLY THIS TIME
                    for (int i = 0; i < clickedCells.Count; i++)
                        clickedCells[i].hide(true, false);
                }
            }

            ClearSelectedCells();
            ClearClickedCell();

            if (_AttachedDataset.Infos != null)
                _AttachedDataset.Infos.describe();
#if !UNITY_WEBGL
            MorphoTools.SendCellsVisibilityToLineage();
#endif

        }

        //Show all cells
        public void ShowAllCell(bool all_alltimes)
        {
            if (all_alltimes)
            {
                for (int t = _AttachedDataset.MinTime; t <= _AttachedDataset.MaxTime; t++)
                    ShowAllAt(t);
            }
            else
            {
                ShowAllAt(_AttachedDataset.CurrentTime);
            }

#if UNITY_WEBGL
            if (Lineage.isLineage)
                Lineage.UpdateWebsiteLineage();
#else
            //MorphoTools.SendCellsPickedToLineage();
            MorphoTools.SendCellsVisibilityToLineage();
#endif

        }

        //Unhide all cell
        private void ShowAllAt(int t)
        {
            foreach (Cell cell in _AttachedDataset.CellsByTimePoint[t])
            {
                cell.hide(false);
            }
        }

        //Select multiple Cell inside box area
        //TOO MANY RAYCASTS
        private void SelectObjectsInArea(Vector2 firstPoint, Vector2 secondPoint)
        {
            ClearSelectedCells();

            RaycastHit hitInfo = new RaycastHit();
            int xMin = (int)Mathf.Round(Mathf.Min(firstPoint.x, secondPoint.x));
            int xMax = (int)Mathf.Round(Mathf.Max(firstPoint.x, secondPoint.x));
            int yMin = (int)Mathf.Round(Mathf.Min(firstPoint.y, secondPoint.y));
            int yMax = (int)Mathf.Round(Mathf.Max(firstPoint.y, secondPoint.y));
            int shift = 5;

            for (int x = xMin; x <= xMax; x += shift)
            {
                for (int y = yMin; y <= yMax; y += shift)
                {
                    bool hiting = Physics.Raycast(Camera.main.ScreenPointToRay(new Vector2(x, y)), out hitInfo);

                    if (hiting)
                    {
                        string clickedname = hitInfo.transform.gameObject.name;

                        if (clickedname.Contains("_MeshPart"))
                            clickedname = hitInfo.transform.parent.gameObject.name;

                        Cell c = _AttachedDataset.getCell(clickedname, false);

                        if (c != null && !c.selected)
                            AddCellSelected(c);
                    }
                }
            }
#if !UNITY_WEBGL
            MorphoTools.SendCellsPickedToLineage();
#endif

        }

        private bool IsPointerOverUIObject() => EventSystem.current.IsPointerOverGameObject();

        private void Update()
        {
#if UNITY_IOS || UNITY_ANDROID
        //When String Visualisation is activate we have to check on where we are
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            RaycastHit hitInfo = new RaycastHit();
            bool hiting = false;

            if (Input.touchCount == 1)
            {
                Touch touch1 = Input.GetTouch(0);
                hiting = Physics.Raycast(Camera.main.ScreenPointToRay(Input.GetTouch(0).position), out hitInfo);

                if (hiting)
                {
                    if (IsPointerOverUIObject()
                        || hitInfo.transform.gameObject.layer == 5
                        || LayerMask.LayerToName(hitInfo.transform.gameObject.layer) == "UI"
                        || (InterfaceManager.instance.lineage_viewer != null && InterfaceManager.instance.lineage_viewer.lineage_shown && InterfaceManager.instance.lineage_viewer.hasFocus))
                    {
                        return;
                    }

                    string clickedname = hitInfo.transform.gameObject.name;

                    if (hitInfo.transform.gameObject == MorphoTools.GetBackground())
                    {
                        //We click outside First the Time Bar, Second the menu
                        if (MorphoTools.GetTransformationsManager() != null)
                            MorphoTools.GetTransformationsManager().ClickBackground(true);

                        if (touch1.phase == TouchPhase.Began)
                        {
                            ClearSelectedCells();
                            ClearClickedCell();
                        }
                    }
                    else
                    {
                        if (clickedname.Contains("_MeshPart"))
                            clickedname = hitInfo.transform.parent.gameObject.name;

                        Cell c = _AttachedDataset.getCell(clickedname, false);

                        if (c != null)
                        {
                            if (!c.selected)
                            {
                                if (touch1.phase == TouchPhase.Moved || touch1.phase == TouchPhase.Began)
                                {
                                    selectedCell = c;
                                    AddCellSelected(c);
                                }
                            }
                            else if (touch1.phase == TouchPhase.Began)
                            {
                                RemoveCellSelected(c);
                                ClearClickedCell();
                                selectedCell = null;
                            }
                        }
                    }
                }

                if (_AttachedDataset.Infos != null)
                    _AttachedDataset.Infos.describe(); //Update the description menu ! VERY COSTLY IN MEMORY
            }

            if (Lineage.isLineage)
                Lineage.UpdateWebsiteLineage();

            /////// TO DRAW STRING INFOS ON CELLS
            if (_AttachedDataset.goShowString != null && _AttachedDataset.Infos != null)
            {
                //Wait for initialisation
                if (_AttachedDataset.Infos.NbStringOn > 0)
                {
                    _AttachedDataset.goShowString.transform.GetComponent<Renderer>().enabled = true;

                    if (hiting)
                    {
                        if (hitInfo.transform.gameObject == MorphoTools.GetBackground())
                        {
                            string clickedname = hitInfo.transform.gameObject.name;

                            Cell c = _AttachedDataset.getCell(hitInfo.transform.gameObject, false);

                            if (c != null)
                            {
                                Vector3 screenPoint = new Vector3(Input.mousePosition.x + 20, Input.mousePosition.y + 20, 10f);

                                _AttachedDataset.goShowString.transform.position = Camera.main.ScreenToWorldPoint(screenPoint);

                                MorphoTools.GetDataset().goShowString.transform.position += new Vector3(0.0f, 0.1f, 0.0f);

                                string stringToShow = c.getShowString();

                                if (stringToShow != _AttachedDataset.goShowString.GetComponent<TextMesh>().text)
                                    _AttachedDataset.goShowString.GetComponent<TextMesh>().text = stringToShow;
                            }
                            else if (_AttachedDataset.goShowString.GetComponent<TextMesh>().text != "")
                            {
                                _AttachedDataset.goShowString.GetComponent<TextMesh>().text = "";
                            }
                        }
                        else if (_AttachedDataset.goShowString.GetComponent<TextMesh>().text != "")
                        {
                            _AttachedDataset.goShowString.GetComponent<TextMesh>().text = "";
                        }
                    }
                    else if (_AttachedDataset.goShowString.GetComponent<TextMesh>().text != "")
                    {
                        _AttachedDataset.goShowString.GetComponent<TextMesh>().text = "";
                    }
                }
                else
                {
                    _AttachedDataset.goShowString.transform.GetComponent<Renderer>().enabled = false;
                }
            }
        }
#else
            //When String Visualisation is activate we have to check on where we are
            if (!IsPointerOverUIObject() && Lineage.isLineage)
                Lineage.UpdateWebsiteLineage();
#endif
            if (!HighlightToggle)
            {
                if (Input.GetKeyDown(KeyCode.H))
                {
                    if (!HighlightMode)
                    {
                        HighlightMode = true;
                        ToggleHighlightMode(true);
                    }

                }
                if (Input.GetKeyUp(KeyCode.H))
                {
                    if (HighlightMode)
                    {
                        HighlightMode = false;
                        ToggleHighlightMode(false);
                    }

                }
            }
            



        }

        /// <summary>
        /// Switch the selection state of the given cell (<see cref="Cell"/>):
        /// Select it if it was not, unselect it if it was.
        /// </summary>
        public void SwitchCellSelectionState(Cell cell)
        {
            if (cell.selected)
            {
                RemoveCellSelected(cell);
                ClearClickedCell();
                selectedCell = null;
/*#if !UNITY_WEBGL
                List<Cell> c = new List<Cell>();
                c.Add(cell);
                MorphoTools.SendClearPickedToLineage(c);
#endif*/
            }
            else
            {
                selectedCell = cell;
                AddCellSelected(cell);
#if !UNITY_WEBGL
                MorphoTools.SendCellsPickedToLineage();
#endif
            }

            if (HighlightMode || HighlightToggle)
            {
                ToggleHighlightMode(false);
                ToggleHighlightMode(true);
            }

        }

        public void StopSquareSelection()
        {
            //On mouse Up reinit box selection area
            _BoxSelectionArea.gameObject.SetActive(false);

            if (_AttachedDataset.Infos != null)
                _AttachedDataset.Infos.describe(); //Update the description menu ! VERY COSTLY IN MEMORY
        }

        public void StartOrContinueSquareSelection(Vector2 mouseStartPosition, Vector2 currentMousePosition)
        {
            //Mouse down and a first click were selected
            if (Vector3.Distance(mouseStartPosition, currentMousePosition) > 1)
            {
                float boxX = mouseStartPosition.x;
                float boxY = mouseStartPosition.y;
                float boxW = Mathf.Abs(currentMousePosition.x - mouseStartPosition.x);
                float boxH = Mathf.Abs(currentMousePosition.y - mouseStartPosition.y);

                if (mouseStartPosition.x > currentMousePosition.x)
                    boxX = currentMousePosition.x;

                if (mouseStartPosition.y > currentMousePosition.y)
                    boxY = currentMousePosition.y;

                _BoxSelectionArea.position = new Vector3(
                    (boxX - InterfaceManager.instance.canvasWidth / 2f) * InterfaceManager.instance.canvasScale,
                    (boxY - InterfaceManager.instance.canvasHeight / 2f) * InterfaceManager.instance.canvasScale,
                    -100f);

                _BoxSelectionArea.sizeDelta = new Vector2(boxW, boxH);
                _BoxSelectionArea.gameObject.SetActive(true);

                SelectObjectsInArea(mouseStartPosition, currentMousePosition);//SELECT OBJECTS


                if (HighlightMode || HighlightToggle)
                {
                    ToggleHighlightMode(false);
                    ToggleHighlightMode(true);
                }
            }
        }

        public void AddCellToSelection(Cell cell)
        {
            if (cell != null && !cell.selected)
            {
                AddCellSelected(cell);

                if (_AttachedDataset.Infos != null)
                    _AttachedDataset.Infos.describe(); //Update the description menu ! VERY COSTLY IN MEMORY

                selectedCell = cell;
            }

#if !UNITY_WEBGL
            MorphoTools.SendCellsPickedToLineage();
#endif
            if (HighlightMode || HighlightToggle)
            {
                ToggleHighlightMode(false);
                ToggleHighlightMode(true);
            }
        }

        /// <summary>
        /// Check if the given <see cref="GameObject"/> is a <see cref="Cell"/>.
        /// If <c>true</c>, the <paramref name="cell"/> is an instance of a <see cref="Cell"/>.
        /// Otherwise, <paramref name="cell"/> end up null.
        /// TODO: Move to <see cref="SetsManager"/> or <see cref="DataSet"/>.
        /// </summary>
        /// <param name="go">The <see cref="GameObject"/> to check.</param>
        /// <param name="cell">The <see cref="Cell"/> in which store the founded instance.</param>
        /// <returns><c>True</c> if the <paramref name="go"/> is a cell, <c>false</c> otherwise.</returns>
        public bool IsGameObjectACell(GameObject go, out Cell cell)
        {
            if (go == MorphoTools.GetBackground())
            {
                cell = null;
                return false;
            }

            cell = _AttachedDataset.getCell(go, false);

            return cell != null;
        }

        /// <summary>
        /// Update the <see cref="DataSet.goShowString"/> with the given <paramref name="cell"/> data if possible.
        /// TODO: Move to UI.
        /// </summary>
        /// <param name="cellGameObject"></param>
        public void UpdateCellShowString(Cell cell, Vector2 mousePosition)
        {
            var error
                = cell is null
                || _AttachedDataset.goShowString is null
                || _AttachedDataset.Infos is null
                || _AttachedDataset.Infos.NbStringOn <= 0;

            if (error)
            {
                _AttachedDataset.goShowString.transform.GetComponent<Renderer>().enabled = false;
                return;
            }

            TextMesh datasetDisplayedText = _AttachedDataset.goShowString.GetComponent<TextMesh>();

            var screenPoint
                = new Vector3(mousePosition.x + 20, mousePosition.y + 20, 10f);

            _AttachedDataset.goShowString.transform.position
                = Camera.main.ScreenToWorldPoint(screenPoint);

#if UNITY_IOS || UNITY_ANDROID
        _AttachedDataset.goShowString.transform.position += new Vector3(0.0f,0.1f,0.0f);
#endif

            _AttachedDataset.goShowString.transform.GetComponent<Renderer>().enabled = true;

            if (cell.getShowString() != datasetDisplayedText.text)
                datasetDisplayedText.text = cell.getShowString();
        }

        #region highlight_functions

        public void ToggleHighlightMode(bool value)
        {
            DataSet dataset = MorphoTools.GetDataset();
            if (value)
            {
                foreach(Cell c in dataset.CellsByTimePoint[dataset.CurrentTime])
                {
                    if (!clickedCells.Contains(c))
                    {
                        if (c.Channels.Count > 0)
                        {
                            c.getFirstChannel().ShouldBeUpdated = true;
                            HighlightedCells.Add(c);
                        }
                        
                    }
                }
            }
            else
            {
                foreach (Cell c in HighlightedCells)
                {
                    if (c.Channels.Count > 0)
                    {
                        c.getFirstChannel().ShouldBeUpdated = true;
                    }
                }
                    
                HighlightedCells.Clear();
            }
            //then in any case call for rendering
        }


        public void ActivateHighlightToggle()
        {
            HighlightToggle = !HighlightToggle;
            ToggleHighlightMode(HighlightToggle);
        }


        #endregion
    }
}

    