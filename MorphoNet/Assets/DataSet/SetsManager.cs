﻿using MorphoNet.Core;
using SimpleJSON;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Action = System.Action;

namespace MorphoNet
{
    public class SetsManager : MonoBehaviour
    {
        public static SetsManager instance = null;

        private void Awake()
        {
            if (instance == null)
                instance = this;
            else
                Destroy(gameObject);
        }
        public GameObject embryosBase;

        public GameObject Background;

        public GameObject rotationAxis;
        public GameObject cube3DImage;
        public GameObject datasetcomment;

        [SerializeField]
        public DataSetDisplayStrategy _DataSetDisplayStrategy;

        public DataSetDisplayStrategy _HelicoidalStrategyCache;

        //User Attribute
        public Text login;

        public string nameUser;
        public string surnameUser;
        public int IDUser;
        public int userRight; //0:CREATOR; 1:USER; 2:READE
        public GameObject cell_default;

        //FOR DB
        public string hash = "34morpho:";  //For safety communication...

        public string urlSERVER;
        public string urlLog;
        public string urlUsers;
        public string urlUpload;
        public string urlDataset;
        public string urlSelection;
        public string urlColormap;
        public string urlShare;
        public string urlScenario;
        public string urlMovie;
        public string urlAdmin;
        public string urlPlayAPI;
        public string urlCurration;
        public string urlAPI;
        public string urlLineage;
        public string urlCorrespondence;
        public string urlAniseed;
        public string urlRawImages;

        // Shaders
        public Material Selected;

        public Material Default;
        public Material ColorDefault;
        public Material Transparent;
        public Material Diamond;
        public Material Dark;
        public Material Vertex;
        public Material Bumped;
        public Material Brick;
        public Material Stars;
        public Material bloodcell;
        public Material Wireframe;
        public Material Clippable;
        public Material BackgroundMaterial;
        public Material HighlightSelectedMaterial;
        public Material HighlightCopyMaterial;

        //Shader presets (for param accessing for color maps)
        public MaterialPreset DefaultPreset;

        public MaterialPreset TransparentPreset;
        public MaterialPreset DiamondPreset;
        public MaterialPreset DarkPreset;
        public MaterialPreset HaloPreset;
        public MaterialPreset BrickPreset;
        public MaterialPreset ConcretePreset;
        public MaterialPreset BloodcellPreset;

        public List<Material> BarSelection;
        public List<Material> StainSelection;

        // FOR POINTS 3D
        public GameObject sphere;

        public MeshFilter sphereLPY;
        public MeshFilter coneLPY;
        public MeshFilter cubeLPY;
        public MeshFilter cylinderLPY;
        public MeshFilter discLPY;
        public MeshFilter frustum02;
        public MeshFilter frustum04;
        public MeshFilter frustum06;
        public MeshFilter frustum08;
        public MeshFilter frustum12;
        public MeshFilter frustum14;
        public MeshFilter frustum16;
        public MeshFilter frustum18;
        public MeshFilter frustum2;

        // Font
        public Font mainFont;

        public Material Text3D;

        // Use MorphoNet in API mode
        public string API_Menu = "";

        public List<string> API_Genes;
        public int API_Time = -1;
        public string API_Territory = "";
        public string API_Fate = "";
        public string API_Stage = "";
        public Dictionary<string, bool> API_Buttons; //button name : 0 for hide, 1 for show
        public Dictionary<int, int> API_Correspondence;//0 : nothing, 1:hide for user, 2:load, 3: apply
        public Dictionary<string, string> MorphoFormat;//List Morpho Format
        public List<GameObject> embryos;
        public List<int> meshes_with_textures;
        public int TimeReloadCount = -1;
        public int CurrentTimeReload = -1;

        public static bool MeshDownloaded = false;

        public event Action OnMeshDownloaded;

        // Cut
        public bool realcut;

        public KeyCode TranslationButton;
        public DirectPlot directPlot;
        public bool loading_finished = false;
        public float lineage_update_delay = 0.01f;

        // Mesh downloading
        public int TotalNumberOfMesh = 0;

        public int nbMeshesDownloaded;
        public int nbMeshesLoaded;
        public bool primitive_downloaded = false;

        //sprites for dataset
        public Sprite lineageTreeWhite;

        public Sprite network;
        public Sprite eyeClose;

#if UNITY_WEBGL
        [DllImport("__Internal")]
        private static extern void DatasetLoadFinishedJS();
#else

        private static void DatasetLoadFinishedJS()
        { }

#endif

        public DataSet DataSet { get; private set; }

        public void TriggerOnMeshDownloaded()
        {
            if (!MeshDownloaded || DataSet == null)
                return;

            OnMeshDownloaded?.Invoke();
        }

        #region DOWNLOAD_AND_UI_CALLBACKS_REGION

        public void setURLServer(string t_url)
        {
            if (t_url.Contains("morpho.lirmm"))
            {
                urlSERVER = t_url;
                urlLog = urlSERVER + "checkUsers.php";
                urlUsers = urlSERVER + "changeUsers.php";
                urlUpload = urlSERVER + "upload.php";
                urlDataset = urlSERVER + "api/dataset/";
                urlSelection = urlSERVER + "selection.php";
                urlColormap = urlSERVER + "colormap.php";
                urlShare = urlSERVER + "share.php";
                urlScenario = urlSERVER + "scenario.php";
                urlMovie = urlSERVER + "movie.php";
                urlAdmin = urlSERVER + "admin.php";
                urlPlayAPI = urlSERVER + "playapi.php";
                urlCurration = urlSERVER + "curration.php";
                urlAPI = urlSERVER + "VirtualEmbryo.py";
                urlLineage = urlSERVER + "lineage.php";
                urlCorrespondence = urlSERVER + "correspondence.php";
                urlAniseed = urlSERVER + "askDB.php";
                urlRawImages = urlSERVER + "rawimages.php";
            }
            else
            {
                urlSERVER = t_url;
                urlLog = urlSERVER + "checkUsers.php";
                urlUsers = urlSERVER + "changeUsers.php";
                urlUpload = urlSERVER + "upload.php";
                //urlDataset = urlSERVER + "dataset.php";
                urlDataset = urlSERVER + "api/dataset/";
                urlSelection = urlSERVER + "selection.php";
                urlColormap = urlSERVER + "colormap.php";
                urlShare = urlSERVER + "share.php";
                urlScenario = urlSERVER + "scenario.php";
                urlMovie = urlSERVER + "movie.php";
                urlAdmin = urlSERVER + "admin.php";
                urlPlayAPI = urlSERVER + "playapi.php";
                urlCurration = urlSERVER + "curration.php";
                urlAPI = urlSERVER + "VirtualEmbryo.py";
                urlLineage = urlSERVER + "lineage.php";
                urlCorrespondence = urlSERVER + "correspondence.php";
                urlAniseed = urlSERVER + "askDB.php";
                urlRawImages = urlSERVER + "rawimages.php";
            }

            if (!urlSERVER.EndsWith("/"))
                urlSERVER = urlSERVER + "/";

            MorphoDebug.Log(" Set SERVER URL " + t_url);
        }

        public void CreateMorphoFormat()
        {
            MorphoFormat = new Dictionary<string, string>();
            MorphoFormat["time"] = " t,id,ch:t,id,ch";
            MorphoFormat["space"] = "t,id,ch:t,id,ch";
            MorphoFormat["float"] = "t,id,ch:float";
            MorphoFormat["number"] = "t,id,ch:float";
            MorphoFormat["string"] = "t,id,ch:string";
            MorphoFormat["text"] = "t,id,ch:string";
            MorphoFormat["group"] = "t,id,ch:string";
            MorphoFormat["selection"] = "t,id,ch:int";
            MorphoFormat["label"] = "t,id,ch:int";
            MorphoFormat["color"] = "t,id,ch:r,g,b";
            MorphoFormat["dict"] = "t,id,ch:t,id,ch:float";
            MorphoFormat["sphere"] = "t,id,ch:x,y,z,r";
            MorphoFormat["vector"] = "t,id,ch:x,y,z,r:x,y,z,r";
        }

        public IEnumerator TestRequest()
        {
            WWWForm formC = new WWWForm();
            formC.AddField("id_dataset", LoadParameters.instance.id_dataset);

            UnityWebRequest www2 = UnityWebRequests.Post(urlSERVER, "api/testpublic/", formC);

            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www2.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www2.SendWebRequest();

            if (www2.isHttpError || www2.isNetworkError)
            {
                MorphoDebug.Log("Error : " + www2.error,1);
            }
            else
            {
                // MorphoDebug.Log(www2.downloadHandler.text);
            }
        }

        //Donwload the comments
        public IEnumerator DownloadComments(GameObject linkc)
        {
            UnityWebRequest www = UnityWebRequests.Get(urlDataset, LoadParameters.instance.id_dataset + "?hash=" + hash);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                MorphoDebug.Log("Error : " + www.error);
            }
            else
            {
                GameObject dc = datasetcomment;

                linkc.SetActive(false);

                if (www.downloadHandler.data.Length > 0)
                {
                    var N = JSONNode.Parse(www.downloadHandler.text);
                    string comments = N["comments"].ToString().Replace('"', ' ').Trim();

                    if (comments.ToLower() != "null")
                    {
                        dc.SetActive(true);
                        comments.Replace('\n', ' ');
                        dc.GetComponent<Text>().text = comments;
                        dc.SetActive(true);
                    }
                }
                else
                {
                    dc.SetActive(false);
                    linkc.SetActive(false);
                    MorphoDebug.Log("Nothing to download..." + www.downloadHandler.text);
                }
            }
            www.Dispose();
        }

        public void ReLoadMesh()
        {
            if (MeshDownloaded && DataSet != null
                && DataSet.time_to_reload != null
                && DataSet.time_to_reload.Count > 0)
            {
                int time_to_reload = DataSet.time_to_reload[0];
                DataSet.time_to_reload.RemoveAt(0);
                CurrentTimeReload = 0;
                TimeReloadCount = DataSet.time_to_reload.Count;
                DataLoader.instance.download_OBJorBundle(time_to_reload, 0);  //TODO if there is multiple Bundle per Time point
            }
            else
            {
                DataSet.is_reloading = false;
            }
        }

        // After Download Asset Bundle or OBJ
        public Dictionary<string, GameObject> Primitives = new Dictionary<string, GameObject>();

        public Dictionary<int, bool> isTimePrimitive;
        private Dictionary<string, Dictionary<GameObject, bool>> InstantiatePrimitives;

        public GameObject GetNextAvailablePrimitive(string primi, Transform parent)
        {
            if (InstantiatePrimitives == null)
                InstantiatePrimitives = new Dictionary<string, Dictionary<GameObject, bool>>();

            if (!InstantiatePrimitives.ContainsKey(primi))
                InstantiatePrimitives[primi] = new Dictionary<GameObject, bool>();

            //Look for the next aviable instantiation of this primitive
            foreach (KeyValuePair<GameObject, bool> it in InstantiatePrimitives[primi])
            {
                GameObject go = it.Key;

                if (!it.Value)
                {
                    InstantiatePrimitives[primi][go] = true; //Mark as used
                    go.transform.SetParent(parent);
                    return go;
                }
            }

            //Nothing is free -> instantiate a new one
            GameObject goe = InstantiateGO(Primitives[primi], parent);
            InstantiatePrimitives[primi][goe] = true;

            return goe;
        }

        public void ActivePrimitives(int timeCurrent, int timePrevious)
        {
            //UNACTIVE UNUSED PRIMITIVES
            if (DataSet.mesh_by_time.ContainsKey(timePrevious))
            {
                GameObject emb = DataSet.mesh_by_time[timePrevious];

                for (int i = 0; i < emb.transform.childCount; i++)
                {
                    GameObject goe = emb.transform.GetChild(i).gameObject;
                    foreach (KeyValuePair<string, Dictionary<GameObject, bool>> primi in InstantiatePrimitives)
                    {
                        if (InstantiatePrimitives[primi.Key].ContainsKey(goe))
                        {
                            Cell c = DataSet.getCell(goe.name, false);
                            c.primi.Position = goe.transform.localPosition;
                            c.primi.Scale = goe.transform.localScale;
                            c.primi.Rotation = goe.transform.localRotation;
                            InstantiatePrimitives[primi.Key][goe] = false;
                        }
                    }
                }
            }

            //ACTIVE NEW ONE
            if (DataSet.mesh_by_time.ContainsKey(timeCurrent) && DataSet.CellsByTimePoint.ContainsKey(timeCurrent))
            {
                GameObject emb = DataSet.mesh_by_time[timeCurrent];

                //List Active Cells
                List<string> ActiveGo = new List<string>();

                for (int i = 0; i < emb.transform.childCount; i++)
                {
                    ActiveGo.Add(emb.transform.GetChild(i).gameObject.name);
                }

                if (emb.transform.childCount != DataSet.CellsByTimePoint[timeCurrent].Count())
                { //Only if they are not active
                    foreach (Cell cell in DataSet.CellsByTimePoint[timeCurrent])
                    {
                        if (cell.Channels != null && cell.Channels.Values != null)
                        {
                            foreach (CellChannelRenderer c in cell.Channels.Values)
                            {
                                string cname = cell.getName();
                                if (!ActiveGo.Contains(cname) && cell.primi != null && cell.primi.Name == cname)
                                {
                                    GameObject goe = GetNextAvailablePrimitive(cell.primi.Type, emb.transform);
                                    goe.name = cname;
                                    goe.transform.localPosition = cell.primi.Position;
                                    goe.transform.localScale = cell.primi.Scale;
                                    goe.transform.localRotation = cell.primi.Rotation;

                                    cell.updateShader();
                                    c.AssociatedCellObject = goe;
                                    c.UpdateChannelRenderer();
                                }
                            }
                        }
                        else
                        {
                            string cname = cell.getName();
                            if (!ActiveGo.Contains(cname) && cell.primi != null && cell.primi.Name == cname)
                            {
                                GameObject goe = GetNextAvailablePrimitive(cell.primi.Type, emb.transform);
                                goe.name = cname;
                                goe.transform.localPosition = cell.primi.Position;
                                goe.transform.localScale = cell.primi.Scale;
                                goe.transform.localRotation = cell.primi.Rotation;

                                CellChannelRenderer channel = cell.getFirstChannel();
                                cell.updateShader();
                                channel.AssociatedCellObject = goe;
                                channel.UpdateChannelRenderer();
                            }
                        }
                    }
                }
            }
        }

        public void CleanPrimitives(int t)
        {
            if (InstantiatePrimitives != null)
            {
                List<GameObject> objs = new List<GameObject>();
                foreach (KeyValuePair<string, Dictionary<GameObject, bool>> primi in InstantiatePrimitives)
                {
                    foreach (KeyValuePair<GameObject, bool> goe in primi.Value)
                    {
                        if (goe.Key.name.StartsWith(t + ","))
                            objs.Add(goe.Key);
                    }
                    for (int i = 0; i < objs.Count; i++)
                    {
                        InstantiatePrimitives[primi.Key].Remove(objs[i]);
                    }
                    objs.Clear();
                }
            }
        }

        #endregion DOWNLOAD_AND_UI_CALLBACKS_REGION

        #region UTILS_REGION

        public void onLinkClick(string link)
        {
            Application.ExternalEval("window.open(\"" + link + "\")");
        }

        public void enLoad()
        {
            //UIManager.instance.MenuShortcuts.SetActive(false);
        }

        public void lineage_showCell(string p)
        {
            Lineage.showCell(p);
        }

        public void DestroyGO(GameObject go)
            => Destroy(go);

        public GameObject InstantiateGO(GameObject original, Transform parent)
            => Instantiate(original, parent);

        public GameObject InstantiateGO(GameObject original, Transform parent, bool instantiateInWorldSpace)
            => Instantiate(original, parent, instantiateInWorldSpace);

        #endregion UTILS_REGION

        #region API_REGION

        //TEMP
        public void loadNewAPIMode()
        {
            API_Territory = "b8.32";
            //if (API_Time == 20) API_Load("?time=10&gene=Cirobu.g00002212&gene=Cirobu.g00011105&gene=Cirobu.g00012743");
            //API_Load("?menu=Genetic&time=10&gene=Cirobu.g00003733&gene=Cirobu.g00009524&gene=Cirobu.g00002320&gene=Cirobu.g00004098");
            //API_Load("?menu=Genetic&time=10&gene=Cirobu.g00003733&gene=Cirobu.g00009524&gene=Cirobu.g00002320&gene=Cirobu.g00004098");
            API_Load("?id_dataset = 1095 & mintime = 1 & maxtime = 6 & territory = a7.9");
            //API_Load("?stage=10&territory=b8.32");
        }

        //END TEMP

        public IEnumerator DelayedAPILoad(string param)
        {
            yield return new WaitUntil(() => loading_finished);
            API_Genes = new List<string>();
            // API_Button = new List<string>();
            API_Territory = "";
            API_Menu = "";
            API_Fate = "";
            API_Time = DataSet.CurrentTime;
            API_Stage = "";
            //bool resetColor = false;
            param = param.Substring(1, param.Length - 1); //REMOVE "?"
                                                          //MorphoDebug.Log("BEFORE API_Time=" + API_Time);
                                                          //MorphoDebug.Log(param);
            string[] parames = param.Split('&');
            foreach (string p in parames)
            {
                string[] pv = p.Split('=');
                if (pv[0] == "time")
                    int.TryParse(pv[1].Trim(), out API_Time);
                if (pv[0].StartsWith("gene"))
                    API_Genes.Add(pv[1].Trim());
                if (pv[0].StartsWith("territory"))
                    API_Territory = pv[1].Trim().Replace('_', ' ');
                if (pv[0].StartsWith("fate"))
                    API_Fate = pv[1].Trim().Replace('_', ' ');
                if (pv[0] == "menu")
                    API_Menu = pv[1].Trim();
                if (pv[0] == "stage")
                    API_Stage = "Stage " + pv[1].Trim();
                if (pv[0].StartsWith("reset"))
                {
                    if (pv[1] == "color")
                    { InterfaceManager.instance.MenuObjects.transform.Find("Selection").GetComponent<SelectionManager>().Start_clearAllColors(); }
                    if (pv[1] == "selection" || pv[1] == "label")
                    { MorphoTools.GetPickedManager().ResetStaticSelection(); }
                }
            }

            if (API_Time != DataSet.CurrentTime) //MOVE AT A SPECIFIC TIME STEP
                InterfaceManager.instance.setTime(API_Time);

            if (API_Genes.Count() > 0) //SHOW SPECIFIC GENE
            {
                DataSet.GeneticManager.init();

                //We first Clear Alll previous Sellection
                InterfaceManager.instance.MenuObjects.transform.Find("Selection").GetComponent<SelectionManager>().clearAll();

                if (DataSet.GeneticManager.scrollBar == null)
                {
                    //NEVER INITIALIZE
                    if (!InterfaceManager.instance.MenuGenetic.activeSelf)
                        InteractionManager.instance.MenuGeneticActive();
                }
                else
                {
                    if (!InterfaceManager.instance.MenuGenetic.activeSelf)
                        InteractionManager.instance.MenuGeneticActive();
                }

                StartCoroutine(DataLoader.instance.requestAllGenes(DataSet.GeneticManager.AttachedDataset));
            }
            //  MorphoMorphoDebug.Log("before api fate d");
            DataSet.update_Fate(API_Fate); //Selelction Fate Map
                                           // MorphoMorphoDebug.Log("after apiifate");
            DataSet.update_Territory(API_Territory, API_Stage); //Selelction Territory
                                                                //  MorphoMorphoDebug.Log("after update territory");
        }

        //ANISEED API MODE (Callled from JAvascript HTML web page)
        public void API_Load(string param)
        {
            StartCoroutine(DelayedAPILoad(param));
        }

        #endregion API_REGION

        #region ROTATION_AND_CUTS

        //Define Plan On Zomm Cut
        public void OnCutZoom(float v)
            => InterfaceManager.instance.ZommCutPlan = new Vector3(v, v, v);

        //On click on the button reset rotation

        //Download Rotation, Position, Scale at specific time point

        //To REally Cut the embryo
        public void onClickCut()
            => realcut = !realcut;

        #endregion ROTATION_AND_CUTS

        #region Unity methods

        // Start is called before the first frame update
        private IEnumerator Start()
        {
            if (Camera.main.depthTextureMode != DepthTextureMode.Depth)
                Camera.main.depthTextureMode = DepthTextureMode.Depth;
            AssetBundle.UnloadAllAssetBundles(false);
#if UNITY_EDITOR
            yield return new WaitUntil(() => LoadParameters.instance.load_standalone_plot || !DebugManager.instance.is_active || DebugManager.instance.loaded_data);
#endif
            MeshDownloaded = false;
            StartCoroutine(Init());

            yield return null;
        }

        #endregion Unity methods

        private void ReportError(string logString, string stackTrace, LogType type)
        {
            // if type is an exception, logs the error (and stackTrace) to our backend
            MorphoDebug.Log("error ! ");
            MorphoDebug.Log(logString);
            MorphoDebug.Log(stackTrace);
        }

        #region Initialisation

        // Start is called before the first frame update
        private IEnumerator Init()
        {
            embryos = new List<GameObject>();
            embryos.Add(new GameObject());
            embryos[0].transform.parent = embryosBase.transform;
            embryos[0].name = "embry0";
            embryos[0].transform.localPosition = Vector3.zero;

            Application.runInBackground = true;
            CreateMorphoFormat();
            
            DataSet = embryos[0].AddComponent<DataSet>();
            DataSet.DataSetDisplayStrategy = _DataSetDisplayStrategy;

            //pass sprite parameters
            DataSet.eyeClose = eyeClose;
            DataSet.lineageTreeWhite = lineageTreeWhite;
            DataSet.network = network;
            if (LoadParameters.instance.id_dataset == 0)
            {
                DataSet.range_time_loading = false;
            }

            //params
            setURLServer(LoadParameters.instance.urlSERVER);
            IDUser = LoadParameters.instance.id_user;
            nameUser = LoadParameters.instance.name_user;
            surnameUser = LoadParameters.instance.surname_user;
            userRight = LoadParameters.instance.user_right;

            //API
            API_Menu = LoadParameters.instance.api_menu;
            API_Genes = LoadParameters.instance.api_Genes;
            API_Buttons = LoadParameters.instance.api_Buttons;
            API_Correspondence = LoadParameters.instance.api_Correspondence;
            API_Time = LoadParameters.instance.api_Time;
            API_Territory = LoadParameters.instance.api_Territory;
            API_Fate = LoadParameters.instance.api_Fate;

            // Load Shaders

            if (ColorUtility.TryParseHtmlString("#61BAE9", out Color newCol))
                BackgroundMaterial.color = newCol;

            //For Text visualisation (cell names etc..)
            DataSet.init();  //Initialize  Dataset

            if (LoadParameters.instance.liveMode)
            {
                InterfaceManager.instance.containerImages.SetActive(false);

                MorphoDebug.Log("Building 3D Object for  live mode",1);

                yield return new WaitUntil(() => DataSet.TransformationsManager.Initialised);

                string embryoOBJ = LoadParameters.instance.OBJ;

                GameObject goe;

                if (LoadParameters.instance.is_STL)
                    goe = DataLoader.instance.ReadSTL(LoadParameters.instance.STL);
                else
                    goe = DataLoader.instance.createGameFromObj(embryoOBJ);

                if (goe != null)
                {
                    DataLoader.instance.LoadMesh(DataSet.LoadingTime, goe, true, LoadParameters.instance.embryo_name);

                    MeshDownloaded = true;

                    DestroyImmediate(goe, true);
                }

                DataSet.embryoCenter = new Vector3(0f, 0f, 0f);
                DataSet.mesh_by_time[0].SetActive(true);

                InterfaceManager.instance.BoutonAniseed.SetActive(false);
                InterfaceManager.instance.BoutonGroups.SetActive(false);

                Resources.UnloadUnusedAssets();

                GC.Collect();
                MorphoDebug.Log("",1);
            }
            else
            {
                InterfaceManager.instance.MenuShortcuts.SetActive(true);
                DataSet.Curations.init(); //Initialize Curation Menu
                DataSet.Infos.Init(); //Initialize Informations

                if (DataSet.id_dataset != 0)
                {
                    
                    //NOT FOR DIRECT PLOT
                    StartCoroutine(DataLoader.instance.MeshesWithTextures(LoadParameters.instance.id_dataset));
                    StartCoroutine(DataLoader.instance.downloadPositions(DataSet)); //DOWNLOAD POSITION AT SPECIFIC TIME FOR THE DATASET
                    StartCoroutine(DataLoader.instance.containsRawImages(LoadParameters.instance.id_dataset));
                    StartCoroutine(DataLoader.instance.Donwload_Primitives(LoadParameters.instance.id_dataset)); //DOWNLOAD PRIMITIVES
                    StartCoroutine(DataLoader.instance.downloadDevelopmentalTable(DataSet.id_type));  //DONWLOAD DEVELOPMENTAL TABLE
                    StartCoroutine(MorphoTools.GetInformations().start_Download_Correspondences());  //DONWLOAD INFORMATIONS
                    StartCoroutine(DataLoader.instance.start_Download_Mesh_Type());  //DONWLOAD MESHS
                }
                else
                {// Only in direct plot
                    InterfaceManager.setLoading();
                    directPlot = InterfaceManager.instance.directPlot.GetComponent<DirectPlot>();
                }
            }

            if (userRight >= 2 || LoadParameters.instance.liveMode == true || LoadParameters.instance.id_dataset == 0)
            {
                InterfaceManager.instance.MenuChannel.transform.Find("downloadMesh").gameObject.SetActive(false);
            }

            //ScatterView Initialisation
            if (MorphoTools.GetScatter() != null)
                MorphoTools.GetScatter().init();

            loading_finished = true;
        }

        #endregion Initialisation


        public void startShot()
        {
            DataSet.startShot();
        }

        public void sendDelay(string value)
        {
            DataSet.sendDelay(value);
        }

        public void sendNbShots(string value)
        {
            DataSet.sendNbShots(value);
        }

        public void sendFramerate(string value)
        {
            DataSet.sendFramerate(value);
        }
    }
}