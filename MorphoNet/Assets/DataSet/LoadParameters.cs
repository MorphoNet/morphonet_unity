﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;
using System.Globalization;
using System.Runtime.InteropServices;
using UnityEngine.UI;
using System;
using UnityEngine.Networking;
using System.Text;
using System.Diagnostics;
using System.ComponentModel;
using SimpleJSON;
using MorphoNet;
using UnityEditor;
using MorphoNet.UI;
using System.IO;
using Debug = UnityEngine.Debug;
using Newtonsoft.Json;

public enum BuildType
{
    WebGL, MorphoPlot
}

public class LoadParameters : MonoBehaviour
{
    public static LoadParameters instance = null;
    public BuildType buildType;

    private void Awake()
    {
#if UNITY_STANDALONE_WIN
        //PlayerSettings.forceSingleInstance = true;
        if (allow_refresh_standalone) {
            Process currentProcess = Process.GetCurrentProcess();

            Process[] localByName = Process.GetProcessesByName("MorphoNet");

            foreach (Process p in localByName)
            {
                if (p.Id != currentProcess.Id)
                {
                    Application.Quit();
                }
            }
        }
#endif

        if (instance == null)
        {
            instance = this;
        }
        else
            Destroy(gameObject);
    }

    public DebugManager debug_m;
    public string urlSERVER = "http://morphonet.org/";
    public int id_dataset;
    public string privacy = "private";
    public string embryo_name;
    public int min_time;
    public int max_time;
    public int id_owner;
    public string bundle_type;
    public int is_bundle;
    public int id_type;
    public string type = "";
    public Quaternion init_rotation;
    public Vector3 init_translation;
    public Vector3 init_scale;
    public int quality;
    public int spf;
    public int dt;
    public string name_user;
    public string surname_user;
    public int id_user;
    public int user_right = -1; //0:CREATOR; 1:USER; 2:READER
    public bool rgpd = true;

    //[HideInInspector]
    public int morphoplot_port_send = 9875;

    //[HideInInspector]
    public int morphoplot_port_recieve = 9876;

    //[HideInInspector]
    public string morphoplot_adress = "http://localhost";

    public string server_adress = "";
    public string server_path = "";

    //API Mode
    public string api_menu = "";

    public List<string> api_Genes;
    public int api_Time = -1;
    public string api_Territory = "";
    public string api_Fate = "";
    public string api_Stage = "";
    public Dictionary<string, bool> api_Buttons; //button name : 0 for hide, 1 for show
    public Dictionary<int, int> api_Correspondence;//0 : nothing, 1:hide for user, 2:load, 3: apply
    public string user_token;
    public string previousLoadedScene = "";
    public bool ready = false;
    public bool liveMode = false;
    public bool parsed_parameters = false;
    public bool objReceived = false;
    public string OBJ = "";
    public byte[] STL = null;
    public List<string> obj_parts;

    public bool is_STL = false;
    public bool fullscreen_backup = false;

    public string mobile_params = "";
    public string mobile_ip = "";
    public bool canRestart = false;

    public bool load_standalone_plot;
    public MorphoNet.User.pass pass_o;
    public bool allow_refresh_standalone = true;
    public Dictionary<int, string> set_params;

    //params for filters
    public Dictionary<int,DatasetParameterItem> SetsDisplay = new Dictionary<int, DatasetParameterItem>();
    public Dictionary<int,DatasetParameterItem> FilteredSetsDisplay = new Dictionary<int, DatasetParameterItem>();
    public Dictionary<DatasetParameterItem, int> BaseSetsOrder = new Dictionary<DatasetParameterItem, int>();
    public List<int> NCBIInAvailableSets = new List<int>();
    public int current_fav_state = 0;

    private bool _Filtering;

    public bool use_plot = false;
    public bool is_plot_deleting = false;
    public bool start_plot = false;
    public bool is_connected;

    [SerializeField]
    private MorphoNet.UI.XR.XRDataSetMenu _XRDataSetMenu;

    //standalone menu : running filter routines
    private IEnumerator _FilterRoutine;

    private bool _FinishedLoadingDatasets=false;

    private static int UI_LOAD_PER_FRAME = 10;
    private readonly static string _COMMA = ",";

    private int _PlotDatasetsIdS = -1;

    private bool _adminMode = false;
    
    [Header("Version number")]
    public TextAsset VersionNumberAsset;
        
    [Header("Version number")]
    public TextAsset GitVersionNumberAsset;

    public bool InitDone = false;

    public void SetAdmin(bool value)
    {
        _adminMode = value;
    }

#if UNITY_WEBGL
    [DllImport("__Internal")]
    private static extern void LeaveFullscreen();

    [DllImport("__Internal")]
    private static extern void RetrieveParameters();

    [DllImport("__Internal")]
    private static extern void loadingReady();
#else

    private static void RetrieveParameters()
    { }

    private static void loadingReady()
    { }

#endif

    private void DisableInputKeyboard(int state)
    {
#if UNITY_WEBGL
        WebGLInput.captureAllKeyboardInput = (state == 0)?false:true;
        if (state == 0)
        {
            if (InterfaceManager.instance != null) {
                InterfaceManager.instance.HideButtons();
            }
        }
        else
        {
            if (InterfaceManager.instance != null)
            {
                InterfaceManager.instance.ShowButtons();
            }
        }
#endif
    }

    private void DisableInputOnly(int state)
    {
#if UNITY_WEBGL
        WebGLInput.captureAllKeyboardInput = (state == 0) ? false : true;
#endif
    }

    private void ReceiveProps()
    {
        ready = true;
    }

    private void ReceiveLiveModeProps()
    {
        ready = true;
        liveMode = true;
    }

    private void ResetFromWebsite()
    {
        Resources.UnloadUnusedAssets();
        System.GC.Collect();
        bool result = Caching.ClearCache();
        UnityEngine.SceneManagement.SceneManager.LoadScene("LoadParameters");
    }

    private void ResetFromWebsiteAndCache()
    {
        bool result = Caching.ClearCache();
        UnityEngine.SceneManagement.SceneManager.LoadScene("LoadParameters");
    }

    private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "LoadParameters" && (previousLoadedScene != "" || Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor))
        {
            previousLoadedScene = scene.name;
            ready = false;
            liveMode = false;
            objReceived = false;

            urlSERVER = "https://morphonet.org";
            id_dataset = 0;
            privacy = "private";
            embryo_name = "";
            min_time = 0;
            max_time = 0;
            id_owner = 0;
            bundle_type = "";
            is_bundle = -1;
            id_type = -1;
            type = "";
            init_rotation = new Quaternion();
            init_translation = new Vector3();
            init_scale = new Vector3();
            quality = -1;
            spf = -1;
            is_STL = false;
            fullscreen_backup = false;
            mobile_params = "";
            mobile_ip = "";
            canRestart = false;
            dt = -1;
            name_user = "";
            surname_user = "";
            id_user = 0;
            user_right = 2; //0:CREATOR; 1:USER; 2:READER
            rgpd = true;
            morphoplot_port_send = 9875;
            morphoplot_port_recieve = 9876;
            morphoplot_adress = "http://localhost";

            server_adress = "";

            parsed_parameters = false;
            server_path = "";

            //API Mode
            api_menu = "";
            api_Genes = new List<string>();
            api_Time = -1;
            api_Territory = "";
            api_Fate = "";
            api_Stage = "";
            api_Buttons = new Dictionary<string, bool>(); //button name : 0 for hide, 1 for show
            api_Correspondence = new Dictionary<int, int>();//0 : nothing, 1:hide for user, 2:load, 3: apply
            user_token = "";
            ready = false;
            liveMode = false;
            objReceived = false;
            OBJ = "";
            obj_parts = new List<string>();
            is_STL = false;
            current_fav_state = 0;
            set_params = new Dictionary<int, string>();
            
            StartCoroutine(Start());
        }
        ResetMainMenuLists();
    }

    private void ResetMainMenuLists()
    {
        SetsDisplay = new Dictionary<int, DatasetParameterItem>();
        FilteredSetsDisplay = new Dictionary<int, DatasetParameterItem>();
        BaseSetsOrder = new Dictionary<DatasetParameterItem, int>();
        NCBIInAvailableSets = new List<int>();
        current_fav_state = 0;
    }


    private void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        //MorphoDebug.Log("Event Bind");
        UnityEngine.SceneManagement.SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    

    public void ResetStateSets()
    {
        foreach (DatasetParameterItem cfs in SetsDisplay.Values)
        {
            if (!FilteredSetsDisplay.ContainsValue(cfs))
                cfs.SetActive(false);
            else
                cfs.SetActive(true);
        }
    }

    public void DisplayAllSets()
    {
        foreach (DatasetParameterItem cfs in SetsDisplay.Values)
        {
            cfs.SetActive(true);
        }

    }




    public void ResetParameters(String SceneName)
    {
        previousLoadedScene = SceneName;
        ready = false;
        liveMode = false;
        objReceived = false;

        urlSERVER = "https://morphonet.org";
        id_dataset = 0;
        privacy = "private";
        embryo_name = "";
        min_time = 0;
        max_time = 0;
        id_owner = 0;
        bundle_type = "";
        is_bundle = -1;
        id_type = -1;
        type = "";
        init_rotation = new Quaternion();
        init_translation = new Vector3();
        init_scale = new Vector3();
        quality = -1;
        spf = -1;
        is_STL = false;
        fullscreen_backup = false;
        mobile_params = "";
        mobile_ip = "";
        canRestart = false;
        dt = -1;
        name_user = "";
        surname_user = "";
        id_user = 0;
        user_right = 2; //0:CREATOR; 1:USER; 2:READER
        rgpd = true;
        morphoplot_port_send = 9875;
        morphoplot_port_recieve = 9876;
        morphoplot_adress = "http://localhost";
        server_adress = "";
        parsed_parameters = false;
        server_path = "";

        //API Mode
        api_menu = "";
        api_Genes = new List<string>();
        api_Time = -1;
        api_Territory = "";
        api_Fate = "";
        api_Stage = "";
        api_Buttons = new Dictionary<string, bool>(); //button name : 0 for hide, 1 for show
        api_Correspondence = new Dictionary<int, int>();//0 : nothing, 1:hide for user, 2:load, 3: apply
        user_token = "";
        ready = false;
        liveMode = false;
        objReceived = false;
        OBJ = "";
        load_standalone_plot = false;
        STL = null;
        obj_parts = new List<string>();
        is_STL = false;
        if (SceneName == "LoadParameters")
        {
            GameObject canvas = FindObjectOfType<Canvas>().gameObject;
        }
    }
    

    public void RestartPlotLoading()
    {
        StopCoroutine(PlotLoading());
        StartCoroutine(PlotLoading());
    }

    public IEnumerator PlotLoading(string name ="Plot")
    {

        //CHECK IF MORPHOPLOT ACTIF
        //MorphoDebug.Log("MORPHOPLOT ?");
        yield return new WaitUntil(() => (use_plot && start_plot));
        //  MorphoDebug.Log("Waiting plot on :" + morphoplot_adress + ":" + morphoplot_port_send);
        WWWForm form = new WWWForm();
        form.AddField("action", "pass");
        form.AddField("time", 0);
        form.AddField("objects", "");
        UnityWebRequest www2 = UnityWebRequests.Post(morphoplot_adress + ":" + morphoplot_port_recieve, "/ send.html", form);
        //UnityWebRequest www2 = UnityWebRequests.Post(morphoplot_adress + ":"+ morphoplot_port_send, "/ get.html", form);
        www2.SetRequestHeader("Access-Control-Allow-Origin", "*");
        www2.SetRequestHeader("Content-Security-Policy", "upgrade-insecure-requests");
        www2.timeout = 2;
        yield return www2.SendWebRequest();
        if (!www2.isHttpError && !www2.isNetworkError)
        {
            MorphoDebug.Log("Detected Plot Request (warning during load)");
            parseParameters("{id:0,name:'" + name.Replace(" ", "&&") +
                            "',bundle:0,maxtime:"+max_time+ ",id_dataset_type:0,mintime:" + min_time + ",time:" + min_time + ",u_right:1,id_people:0,u_ID:0,u_name:'Plot',u_surname:'Direct',port:" +
                            morphoplot_port_send + ",port_send:" + morphoplot_port_send + ",port_receive:" +
                            morphoplot_port_recieve + "host_adress:" + morphoplot_adress + "}",load_from_plot:true);
            load_standalone_plot = true;
        }
        else
        {
            MorphoDebug.Log("Detected Plot Request ");
            parseParameters("{id:0,name:" + name.Replace(" ","&&") + ",bundle:0,maxtime:" + max_time + ",id_dataset_type:0,mintime:" + min_time + ",time:" + min_time + ",u_right:1,id_people:0,u_ID:0,u_name:'Plot',u_surname:'Direct',port:" + morphoplot_port_send + ",port_send:" + morphoplot_port_send + ",port_receive:" + morphoplot_port_recieve + "host_adress:" + morphoplot_adress + "}",load_from_plot:true);
            load_standalone_plot = true;
        }

        yield return new WaitForSeconds(4.0f); //WAS 5 SECONDS
        if (!parsed_parameters) StartCoroutine(PlotLoading()); //WE RESTART THIS COROUTNINE
    }

    public void StartBrowser()
    {
        Application.OpenURL("https://morphonet.org/dataset");
    }

    public void StartBrowserSignup()
    {
        Application.OpenURL("https://morphonet.org/login");
    }

    public void SetHostPlot(string s)
    {
        morphoplot_adress = s;
    }

    public void SetPortSend(string s)
    {
        int t = -1;
        if (int.TryParse(s, out t))
        {
            morphoplot_port_send = t;
        }
    }

    public void SetPortReceive(string s)
    {
        int t = -1;
        if (int.TryParse(s, out t))
        {
            morphoplot_port_recieve = t;
        }
    }


    public IEnumerator GetDatasetsForUser()
    {
        StandaloneMainMenuInteractions.instance.DatasetSearchField.text = "";
        StandaloneMainMenuInteractions.instance.DatasetSearchField.interactable = false;
        StandaloneMainMenuInteractions.instance.TagFilterButton.interactable = false;
        StandaloneMainMenuInteractions.instance.FavFilterButton.interactable = false;
        StandaloneMainMenuInteractions.instance.GroupFilterButton.interactable = false;
        StandaloneMainMenuInteractions.instance.NCBIFilterButton.interactable = false;

        yield return StartCoroutine(GetDatasetsForPublic(true));

        _FinishedLoadingDatasets = false;
        StandaloneMainMenuInteractions.instance.LoadingImage.gameObject.SetActive(true);
        //ClearSetsFav();


        //request for a chunk
        //MorphoDebug.Log("api/searchdatasetschunks/?sorting=" + (int)ordering + "&currentpage=" + page + "&chunksize=" + chunk_size);
        UnityWebRequest www = UnityWebRequests.Get(urlSERVER, "api/userdatasets/");
        if (instance.user_token != "" && instance.user_token != null)
        {
            www.SetRequestHeader("Authorization", "TOKEN " + instance.user_token);
        }
        yield return www.SendWebRequest();
        if (www.isHttpError || www.isNetworkError)
        {
            MorphoDebug.LogError("Unity Error access : " + www.error);
        }
        else
        if (www.downloadHandler.text != "")
        {
            var N = JSONNode.Parse(www.downloadHandler.text);
            int nb = 0;
            if (N != null && N.Count > 0)
            {
                for (int i = 0; i < N.Count; i++)
                {
                    CreateDatasetItem(N[i]);
                    nb++;
                    if (nb > UI_LOAD_PER_FRAME)
                    {
                        nb = 0;
                        yield return new WaitForEndOfFrame();
                    }
                }
                StandaloneMainMenuInteractions.instance.ApplyAllFilters();
                StandaloneMainMenuInteractions.instance.ApplyCurrentOrderingMethod();
                //DatasetSetSearchWithFilter();
            }
        }
        _FinishedLoadingDatasets = true;
        StandaloneMainMenuInteractions.instance.LoadingImage.gameObject.SetActive(false);
        StandaloneMainMenuInteractions.instance.DatasetSearchField.interactable = true;
        StandaloneMainMenuInteractions.instance.InitializeSpeciesFilterMenu();

        //call to load other important infos on datasets
        yield return StartCoroutine(LoadFavoritesForDatasets());
        yield return StartCoroutine(LoadGroupsForDatasets());
        yield return StartCoroutine(LoadTagsForDatasets());
        
        yield return null;
    }


    public IEnumerator GetDatasetsForPublic(bool usermode=false)
    {
        _FinishedLoadingDatasets = false;
        StandaloneMainMenuInteractions.instance.LoadingImage.gameObject.SetActive(true);
        StandaloneMainMenuInteractions.instance.DatasetSearchField.text = "";
        StandaloneMainMenuInteractions.instance.DatasetSearchField.interactable = false;
        StandaloneMainMenuInteractions.instance.TagFilterButton.interactable = false;
        StandaloneMainMenuInteractions.instance.NCBIFilterButton.interactable = false;
        ClearSetsFav();


        //request for a chunk
        //MorphoDebug.Log("api/searchdatasetschunks/?sorting=" + (int)ordering + "&currentpage=" + page + "&chunksize=" + chunk_size);
        UnityWebRequest www = UnityWebRequests.Get(urlSERVER, "api/publicdatasets/");
        if (instance.user_token != "" && instance.user_token != null)
        {
            www.SetRequestHeader("Authorization", "TOKEN " + instance.user_token);
        }
        yield return www.SendWebRequest();
        if (www.isHttpError || www.isNetworkError)
        {
            MorphoDebug.LogError("Unity Error access : " + www.error);
        }
        else
        if (www.downloadHandler.text != "")
        {
            var N = JSONNode.Parse(www.downloadHandler.text);
            if (N != null && N.Count > 0)
            {
                int nb = 0;
                for (int i = 0; i < N.Count; i++)
                {
                    CreateDatasetItem(N[i]);
                    nb++;
                    if (nb > UI_LOAD_PER_FRAME)
                    {
                        nb = 0;
                        yield return new WaitForEndOfFrame();
                    }
                }
                StandaloneMainMenuInteractions.instance.ApplyAllFilters();
                StandaloneMainMenuInteractions.instance.ApplyCurrentOrderingMethod();
                //DatasetSetSearchWithFilter();
            }
        }

        StandaloneMainMenuInteractions.instance.LoadingImage.gameObject.SetActive(false);
        if (!usermode)
        {
            _FinishedLoadingDatasets = true;
            StandaloneMainMenuInteractions.instance.InitializeSpeciesFilterMenu();
            StandaloneMainMenuInteractions.instance.DatasetSearchField.interactable = true;
            yield return StartCoroutine(LoadTagsForDatasets());
        }
            
        yield return null;
    }


    public void DatasetSetSearchWithFilter()
    {

        FilterDatasets(StandaloneMainMenuInteractions.instance.DatasetSearchField.text);
    }


    

    public void LoadLocalDatasets()
    {
        _PlotDatasetsIdS = -1;

        bool new_version = false;
        string path = Application.persistentDataPath + StandaloneMainMenuInteractions.LOCAL_DATASET_PATH;
        string prev_path = Application.persistentDataPath + StandaloneMainMenuInteractions.PREV_LOCAL_DATASET_PATH;

        
        if (!File.Exists(path) && File.Exists(prev_path))
        {
            new_version = true;
        }
        StandaloneMainMenuInteractions.LocalDatasetList json = null;

        if (File.Exists(path))
        {
            string rawjson = File.ReadAllText(path);

            json = JsonConvert.DeserializeObject<StandaloneMainMenuInteractions.LocalDatasetList>(rawjson);
        }
        

        //retrocompatibility check:
        //if (json != null)
        if (new_version)
        {
            //if (json.LocalDatasetItems.Length > 0 && (json.LocalDatasetItems.FirstOrDefault().SegmentedData.Count == 0 && json.LocalDatasetItems.FirstOrDefault().IntensityData.Count == 0))
            //{
                Debug.LogWarning("Old JSON dataset file detected... begin reformatting...");
                //
                StandaloneMainMenuInteractions.instance.DisplayAwaitFunction("Previous local dataset file version detected. Please wait while the system updates (do not close the application)");
                StartCoroutine(StandaloneMainMenuInteractions.instance.SendUpdateLocalDatasets());
                //DO NOT continue loading : we'll do it once the file is up-to-date
                return;
            //}
        }
        

        if (json != null && json.LocalDatasetItems.Length != 0 && json.LocalDatasetItems != null)
        {
            foreach(LocalDatasetItem item in json.LocalDatasetItems)
            {
                if (item.RawDownScale == 0 || item.RawDownScale == 0)//if voxel size is 0,0,0, must be a bug: reset as default
                    item.SetRawScalingDefault();
                CreateDatasetItem(item);
            }
        }

        //always apply filters after loading local datasets
        StandaloneMainMenuInteractions.instance.ApplyAllFilters();
        StandaloneMainMenuInteractions.instance.ApplyCurrentOrderingMethod();

    }


    public void FilterDatasets(string filter = "")
    {
        //yield return new WaitUntil(() => _FinishedLoadingDatasets);

        if (filter != "")
        {
            foreach (DatasetParameterItem d in FilteredSetsDisplay.Values)
            {
                if (!d.Name.text.ToLower().Contains(filter.ToLower()))
                {
                    d.SetActive(false);
                }
                else
                {
                    d.SetActive(true);
                }
            }
        }
        else
        {
            foreach (DatasetParameterItem d in FilteredSetsDisplay.Values)
            {
                d.SetActive(true);
            }
        }
        
    }

    
    
    //TODO : add id_people somewhere to filter by my datasets/other ppl's datasets
    private void CreateDatasetItem(JSONNode N)
    {
        DatasetParameterItem datasetParemeterItem = Instantiate(
            StandaloneMainMenuInteractions.instance.UISetExample,
            StandaloneMainMenuInteractions.instance.NetStandaloneDatasetList.transform);

        int id_set = int.Parse(N["id"]);
        datasetParemeterItem.ID = id_set;
        int id_ppl = int.Parse(N["id_people"]);
        datasetParemeterItem.ID = id_set;
        datasetParemeterItem.SetTimestampBounds(N["mintime"].AsInt, N["maxtime"].AsInt);

        string datasetName = N["name"];

        datasetParemeterItem.SetDate(N["date"]);


        int NCBIId = N["id_ncbi"].AsInt;
        if (!NCBIInAvailableSets.Contains(NCBIId))
            NCBIInAvailableSets.Add(NCBIId);
        datasetParemeterItem.Species = NCBIId;

        

        void onItemClicked(bool activateXR = false)
        {
            (N["mintime"].AsInt, N["maxtime"].AsInt) = datasetParemeterItem.TimestampRange;
            set_params.Add(id_set, N.ToString());
            StartCoroutine(GetQualityAndRights(int.Parse(N["id"]), int.Parse(N["id_people"]) ,activateXR));
            /*parseParameters(set_params[id_set], activateXR);
            load_standalone_plot = true;*/
        }

        //datasetParemeterItem.SetActive(false);
        datasetParemeterItem.Name.text = datasetName;
        datasetParemeterItem.Button.onClick.RemoveAllListeners();
        datasetParemeterItem.Button.onClick.AddListener(() => onItemClicked());

        if (_XRDataSetMenu != null)
        {
            _XRDataSetMenu.InitButtons();
            _XRDataSetMenu.AddItem(datasetName, datasetParemeterItem, () => onItemClicked(true));
        }

        if (SetsDisplay.ContainsKey(id_set))
        {
            if (SetsDisplay[id_set].gameObject != null)
            {
                if (BaseSetsOrder.ContainsKey(SetsDisplay[id_set]))
                    BaseSetsOrder.Remove(SetsDisplay[id_set]);
                Destroy(SetsDisplay[id_set].gameObject);
            }
                
            SetsDisplay.Remove(id_set);
        }
        if (FilteredSetsDisplay.ContainsKey(id_set))
        {
            if (FilteredSetsDisplay[id_set].gameObject != null)
            {
                if (BaseSetsOrder.ContainsKey(FilteredSetsDisplay[id_set]))
                    BaseSetsOrder.Remove(FilteredSetsDisplay[id_set]);

                Destroy(FilteredSetsDisplay[id_set].gameObject);
            }
                
            FilteredSetsDisplay.Remove(id_set);
        }
        SetsDisplay.Add(id_set,datasetParemeterItem);
        BaseSetsOrder.Add(datasetParemeterItem, SetsDisplay.Count - 1);//this is to get the base order ! 
        FilteredSetsDisplay.Add(id_set,datasetParemeterItem);
    }

    public void CreateDatasetItem(LocalDatasetItem item)
    {
        DatasetParameterItem datasetParemeterItem = Instantiate(
            StandaloneMainMenuInteractions.instance.UISetExample,
            StandaloneMainMenuInteractions.instance.NetStandaloneDatasetList.transform);

        datasetParemeterItem.SetTimestampBounds(item.MinTime, item.MaxTime);//times are set to 0 they are saved within the localdataset info, we don't want to touch it here ?
        datasetParemeterItem.ID = _PlotDatasetsIdS;

        datasetParemeterItem.DataSource = MorphoSource.MorphoPlot;
        datasetParemeterItem.LocalDataset = true;
        datasetParemeterItem.LocalDatasetInfo = item;

        datasetParemeterItem.UploadDate = ParseDate(item.Date);

        datasetParemeterItem.SetFavorite(false);

        datasetParemeterItem.Species = 0;

        void onItemClickedLocalDataset(LocalDatasetItem item, DatasetParameterItem guiitem, bool activateXR = false)
        {
            if (!is_plot_deleting)
            {
                StandaloneMainMenuInteractions.instance.LoadLocalDataset(item,
                    (int)guiitem.TimestampRangeSlider.LowValue, (int)guiitem.TimestampRangeSlider.HighValue);
                load_standalone_plot = true;
            }
        }

        void onPrepareRemoveDataset(LocalDatasetItem item, DatasetParameterItem guiitem)
        {
            StandaloneMainMenuInteractions.instance.SetConfirmPanelTextDeleteDataset();
            StandaloneMainMenuInteractions.instance.ConfirmPanel.SetActive(true);
            StandaloneMainMenuInteractions.instance.ConfirmApplyButton.onClick.RemoveAllListeners();
            StandaloneMainMenuInteractions.instance.ConfirmApplyButton.onClick.AddListener(delegate { onRemoveDataset(item, guiitem); });
        }

        void onRemoveDataset(LocalDatasetItem item, DatasetParameterItem guiitem)
        {
            MorphoDebug.Log("remove dataset " + item.Name);
            StandaloneMainMenuInteractions.instance.RemoveLocalDataset(item);
            SetsDisplay.Remove(guiitem.ID);
            BaseSetsOrder.Remove(guiitem);
            FilteredSetsDisplay.Remove(guiitem.ID);
            Destroy(guiitem.gameObject);
            StandaloneMainMenuInteractions.instance.SetAddDatasetPanelActive(false);
            StandaloneMainMenuInteractions.instance.ConfirmPanel.SetActive(false);
        }

        void onPrepareUpdateDataset(LocalDatasetItem item, DatasetParameterItem guiitem)
        {
            StandaloneMainMenuInteractions.instance.SetConfirmPanelTextUpdateDataset();
            StandaloneMainMenuInteractions.instance.ConfirmPanel.SetActive(true);
            StandaloneMainMenuInteractions.instance.ConfirmApplyButton.onClick.RemoveAllListeners();
            StandaloneMainMenuInteractions.instance.ConfirmApplyButton.onClick.AddListener(delegate { onApplyOptionsDataset(item, guiitem); });
        }

        void onApplyOptionsDataset(LocalDatasetItem item, DatasetParameterItem guiitem)
        {
            LocalDatasetItem l = StandaloneMainMenuInteractions.instance.UpdateLocalDataset(item);
            if(l!=null)
                guiitem.UpdateDataset(l);

            onRecomputeData(item,guiitem);
            StandaloneMainMenuInteractions.instance.SetAddDatasetPanelActive(false);
            StandaloneMainMenuInteractions.instance.ConfirmPanel.SetActive(false);

            datasetParemeterItem.OptionsDatasetButton.onClick.RemoveAllListeners();
            datasetParemeterItem.OptionsDatasetButton.onClick.AddListener(() => onOptionsDataset(l, datasetParemeterItem));
        }

        void onRecomputeData(LocalDatasetItem item, DatasetParameterItem guiitem)
        {
            StartCoroutine(StandaloneMainMenuInteractions.instance.PlotRecomputeData(item));
        }

        void onOptionsDataset(LocalDatasetItem item, DatasetParameterItem guiitem)
        {
            //CreateDatasetMenuManager cm = CreateDatasetMenuManager.Instance;
            StandaloneMainMenuInteractions.instance.SetAddDatasetPanelActive(true);
            StartCoroutine(CreateDatasetMenuManager.Instance.SetDatasetPageFromLocalDataset(item));

            CreateDatasetMenuManager.Instance.DeleteDatasetButton.onClick.RemoveAllListeners();
            CreateDatasetMenuManager.Instance.DeleteDatasetButton.onClick.AddListener(() => onPrepareRemoveDataset(item, datasetParemeterItem));

            CreateDatasetMenuManager.Instance.EditButton.onClick.RemoveAllListeners();
            CreateDatasetMenuManager.Instance.EditButton.onClick.AddListener(() => onPrepareUpdateDataset(item, datasetParemeterItem));

            StandaloneMainMenuInteractions.instance.SetExportFullPath(item.FullPath);
            StandaloneMainMenuInteractions.instance.SetCurrentLocalDatasetForExport(item);


            CreateDatasetMenuManager.Instance.PrepareUploadDatasetButton.interactable = id_user <= 0 ? false : true;
            CreateDatasetMenuManager.Instance.PrepareUploadDatasetButton.onClick.RemoveAllListeners();
            CreateDatasetMenuManager.Instance.PrepareUploadDatasetButton.onClick.AddListener(() => onPrepareUpload(item, datasetParemeterItem));
        }

        void onPrepareUpload(LocalDatasetItem item, DatasetParameterItem guiitem)
        {
            CreateDatasetMenuManager.Instance.SetUploadName(item.Name);
            CreateDatasetMenuManager.Instance.SetUploadMenuVisible(true);

            StandaloneMainMenuInteractions.instance.SetUploadDatasetFullPath(item.FullPath);

            CreateDatasetMenuManager.Instance.UploadDatasetButton.onClick.RemoveAllListeners();
            CreateDatasetMenuManager.Instance.UploadDatasetButton.onClick.AddListener(() => StandaloneMainMenuInteractions.instance.LaunchDataSetUpload());
        }

        datasetParemeterItem.Name.text = item.Name;
        datasetParemeterItem.Button.onClick.RemoveAllListeners();
        datasetParemeterItem.Button.onClick.AddListener(() => onItemClickedLocalDataset(item,datasetParemeterItem));

        datasetParemeterItem.OptionsDatasetButton.onClick.RemoveAllListeners();
        datasetParemeterItem.OptionsDatasetButton.onClick.AddListener(() => onOptionsDataset(item, datasetParemeterItem));

        


        SetsDisplay.Add(_PlotDatasetsIdS,datasetParemeterItem);
        BaseSetsOrder.Add(datasetParemeterItem, SetsDisplay.Count - 1);
        FilteredSetsDisplay.Add(_PlotDatasetsIdS,datasetParemeterItem);
        _PlotDatasetsIdS--;
    }


    private DateTime ParseDate(string date)
    {
        string[] tokens = date.Split('-');
        if (tokens.Length != 6)
        {
            MorphoDebug.LogError("could not parse date properly");
            return DateTime.MinValue;
        }
        else
        {
            string parseddate = $"{tokens[0]}/{tokens[1]}/{tokens[2]} {tokens[3]}:{tokens[4]}:{tokens[5]}";
            return DateTime.Parse(parseddate,CultureInfo.InvariantCulture);
        }
    }

    //ordering functions
    public void OrderByLatest()
    {
        int i = 0;
        foreach(var v in SetsDisplay.OrderBy(i => i.Value.UploadDate).Reverse())
        {
            v.Value.transform.SetSiblingIndex(i);
            i++;
        }
            
    }

    public void OrderByOldest()
    {
        int i = 0;
        foreach (var v in SetsDisplay.OrderBy(i => i.Value.UploadDate))
        {
            v.Value.transform.SetSiblingIndex(i);
            i++;
        }

    }

    public void OrderByAlphaAZ()
    {
        int i = 0;
        foreach (var v in SetsDisplay.OrderBy(i => i.Value.Name.text.ToLower()))
        {
            v.Value.transform.SetSiblingIndex(i);
            i++;
        }

    }

    public void OrderByAlphaZA()
    {
        int i = 0;
        foreach (var v in SetsDisplay.OrderBy(i => i.Value.Name.text.ToLower()).Reverse())
        {
            v.Value.transform.SetSiblingIndex(i);
            i++;
        }

    }

    public void ResetDefaultOrder()
    {
        foreach(var kv in BaseSetsOrder)
        {
            kv.Key.transform.SetSiblingIndex(kv.Value);
        }
    }


    public void SetDecreaseInteract(bool state)
    {
        if (GameObject.Find("decrease") != null)
        {
            GameObject.Find("decrease").GetComponent<Button>().interactable = state;
        }
    }

    public IEnumerator RoutineApplyAllFilters()
    {
        //yield return new WaitUntil(() => _FinishedLoadingDatasets);

        yield return new WaitUntil(() => !_Filtering);

        _Filtering = true;

        foreach (DatasetParameterItem p in SetsDisplay.Values)
        {
            p.SetActive(true);
        }
        FilteredSetsDisplay.Clear();
        foreach (DatasetParameterItem p in SetsDisplay.Values)
        {

            StandaloneMainMenuInteractions.instance.ApplySourceFilters(p);
            StandaloneMainMenuInteractions.instance.ApplyGroupFilters(p);
            StandaloneMainMenuInteractions.instance.ApplyTagFilters(p);
            StandaloneMainMenuInteractions.instance.ApplySpeciesFilters(p);
            StandaloneMainMenuInteractions.instance.ApplyFavFilters(p);

        }
        if(FilteredSetsDisplay.Count==0)//if nothing has been filtered, reinitialize the list
            FilteredSetsDisplay = new Dictionary<int,DatasetParameterItem>(SetsDisplay);

        if (StandaloneMainMenuInteractions.instance.DatasetSearchField.text!="")
            DatasetSetSearchWithFilter();
        _Filtering = false;
    }

    

    public void SetIncreaseInteract(bool state)
    {
        if (GameObject.Find("increase") != null)
        {
            GameObject.Find("increase").GetComponent<Button>().interactable = state;
        }
    }


    public void ClearSetsFav()
    {
        if (SetsDisplay == null)
        {
            SetsDisplay = new Dictionary<int, DatasetParameterItem>();
        }
        foreach (DatasetParameterItem g in SetsDisplay.Values)
        {
            if (g != null)
                Destroy(g.gameObject);
        }

        SetsDisplay.Clear();
        BaseSetsOrder.Clear();
        FilteredSetsDisplay.Clear();
        current_fav_state = 0;
        set_params = new Dictionary<int, string>();
    }

    /// <summary>
    /// Load favorite info for all current datsets
    /// </summary>
    /// <returns></returns>
    public IEnumerator LoadFavoritesForDatasets()
    {

        UnityWebRequest www = UnityWebRequests.Get(urlSERVER, "api/favourites/");
        if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
        {
            www.SetRequestHeader("Authorization", "Token " + LoadParameters.instance.user_token);

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                MorphoDebug.LogError("Unity Error access : " + www.error);
            }
            else
            if (www.downloadHandler.text != "")
            {
                var N = JSONNode.Parse(www.downloadHandler.text);
                if (N != null && N.Count > 0)
                {
                    for (int i = 0; i < N.Count; i++)
                    {
                        string p = N[i]["params"];
                        int id = 0;
                        if(int.TryParse(p.Split('=')[1],out id))
                        {
                            if (SetsDisplay.ContainsKey(id))
                            {
                                SetsDisplay[id].SetFavorite(true);
                            }
                            
                        }

                    }
                }
            }
        }
        StandaloneMainMenuInteractions.instance.FavFilterButton.interactable = true;
        yield return null;
    }

    /// <summary>
    /// Load groups for all current datsets
    /// </summary>
    /// <returns></returns>
    public IEnumerator LoadTagsForDatasets()
    {
        List<int> ids = new List<int>();
        StringBuilder sb = new StringBuilder();
        foreach (var s in SetsDisplay.Values)
        {
            if(s.ID > 1)
            {
                sb.Append(s.ID + _COMMA);
                ids.Add(s.ID);
            }
            
        }
        sb.Remove(sb.Length - 2, 1);

        UnityWebRequest www = UnityWebRequests.Get(urlSERVER, "api/getdatasettags/?id_datasets=" + sb.ToString());
        if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
        {
            www.SetRequestHeader("Authorization", "Token " + LoadParameters.instance.user_token);

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                MorphoDebug.LogError("Unity Error access : " + www.error);
            }
            else
            if (www.downloadHandler.text != "")
            {
                var N = JSONNode.Parse(www.downloadHandler.text);
                if (N != null && N.Count > 0)
                {
                    for(int i=0;i<N.Count;i++)
                    {
                        if (N[i]!= null && N[i].Count > 0)
                        {
                            for(int j = 0; j < N[i].Count; j++)
                            {
                                int tag;
                                if(int.TryParse(N[i][j]["id"], out tag))
                                {
                                    if(SetsDisplay.ContainsKey(ids[i]))
                                        SetsDisplay[ids[i]].AddTag(tag);
                                }
                            }
                        }
                    }
                }
            }
        }
        if(StandaloneMainMenuInteractions.instance.TagFilterButton!=null)
            StandaloneMainMenuInteractions.instance.TagFilterButton.interactable = true;
        yield return null;
    }


    /// <summary>
    /// Load groups for all current datsets
    /// </summary>
    /// <returns></returns>
    public IEnumerator LoadGroupsForDatasets()
    {
        StringBuilder sb = new StringBuilder();
        foreach (var s in SetsDisplay.Values)
        {
            if (s.ID > 1)
            {
                sb.Append(s.ID + _COMMA);
            }

        }
        sb.Remove(sb.Length - 2, 1);
        UnityWebRequest www = UnityWebRequests.Get(urlSERVER, "api/sharing/?id_datasets=" + sb.ToString());
        if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
        {
            www.SetRequestHeader("Authorization", "Token " + LoadParameters.instance.user_token);

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                MorphoDebug.LogError("Unity Error access : " + www.error);
            }
            else
            if (www.downloadHandler.text != "")
            {
                var N = JSONNode.Parse(www.downloadHandler.text);
                if (N != null && N.Count > 0)
                {
                    for (int i = 0; i < N.Count; i++)
                    {
                        if (N[i]["base"].ToString().Replace("\"","") == "dataset" && N[i]["id_group"]!=null)
                        {
                            int idset;
                            
                            if(int.TryParse(N[i]["id_base"],out idset))
                            {
                                int idgroup;
                                if (int.TryParse(N[i]["id_group"], out idgroup))
                                {
                                    if (SetsDisplay.ContainsKey(idset)){
                                        SetsDisplay[idset].AddGroup(idgroup);
                                    }
                                    
                                }
                                
                            }
                            
                        }
                    }
                }
            }
        }
        StandaloneMainMenuInteractions.instance.GroupFilterButton.interactable = true;
        yield return null;
    }

    //generators for filter menus from user parameters
    public IEnumerator LoadFiltersForGroupMenu()
    {
        
        UnityWebRequest www = UnityWebRequests.Get(urlSERVER, "api/group/");
        if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
        {
            www.SetRequestHeader("Authorization", "Token " + LoadParameters.instance.user_token);

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                MorphoDebug.LogError("Unity Error access : " + www.error);
            }
            else
            if (www.downloadHandler.text != "")
            {
               
                var N = JSONNode.Parse(www.downloadHandler.text);
                if (N != null && N.Count > 0)
                {
                    for (int i = 0; i < N.Count; i++)
                    {
                        string name = N[i]["name"];
                        int id = N[i]["id"].AsInt;
                        
                        StandaloneMainMenuInteractions.instance.GenerateGroupFilterElement(name, id);
                    }
                }
            }
        }
    }

    public IEnumerator LoadFiltersForSpeciesMenu()
    {
        yield return new WaitUntil(() => _FinishedLoadingDatasets);

        string ncbi = "[" + String.Join(",", NCBIInAvailableSets)+"]";

        UnityWebRequest www = UnityWebRequests.Get(urlSERVER, "api/phyloparenttree/?id_ncbi_list="+ncbi);


        yield return www.SendWebRequest();
        if (www.isHttpError || www.isNetworkError)
        {
           MorphoDebug.LogError("Unity Error access : " + www.error);
        }
        else
        if (www.downloadHandler.text != "")
        {
            var N = JSONNode.Parse(www.downloadHandler.text);
            if (N != null && N.Count > 0)
            {
                for (int i = 0; i < N.Count; i++)
                {
                    string name = N[i]["name"];
                    int id = N[i]["id"].AsInt;
                    if (NCBIInAvailableSets.Contains(id))
                        StandaloneMainMenuInteractions.instance.GenerateSpeciesFilterElement(name, id);
                }
            }
        }
        StandaloneMainMenuInteractions.instance.GenerateSpeciesFilterElement("Unclassified", 0);
        StandaloneMainMenuInteractions.instance.NCBIFilterButton.interactable = true;

    }

    public IEnumerator LoadFiltersForTagMenu()
    {

        UnityWebRequest www = UnityWebRequests.Get(urlSERVER, "api/usertags/");
        if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
        {
            www.SetRequestHeader("Authorization", "Token " + LoadParameters.instance.user_token);
        }
        else//the request is different for tags if you are not connected : 
        {
            www = UnityWebRequests.Get(urlSERVER, "api/publictags/");
        }
        yield return www.SendWebRequest();
        if (www.isHttpError || www.isNetworkError)
        {
           MorphoDebug.LogError("Unity Error access : " + www.error);
        }
        else
        if (www.downloadHandler.text != "")
        {
            var N = JSONNode.Parse(www.downloadHandler.text);
            if (N != null && N.Count > 0)
            {
                for (int i = 0; i < N.Count; i++)
                {
                    string name = N[i]["name"];
                    int id = N[i]["id"].AsInt;
                    StandaloneMainMenuInteractions.instance.GenerateTagFilterElement(name,id);
                }
            }
        }

    }


    public IEnumerator GetQualityAndRights(int dataset_id, int owner_id, bool activateXR)
    {
        if (id_user != 0)
        {
            
            UnityWebRequest www = UnityWebRequests.Get(urlSERVER, "api/sharingsbyuserdataset/?id_dataset=" + dataset_id+"&id_user="+id_user);
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "Token " + LoadParameters.instance.user_token);
            }
            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                MorphoDebug.LogError("Error while fetching rights for dataset : " + www.error);
            }
            else
            {                
                JSONNode N = JSONNode.Parse(www.downloadHandler.text);
                if (N.Count > 0)
                {
                    if (N.Keys.Contains("how"))
                    {
                        int right = N["how"].AsInt;
                        switch (right)
                        {
                            case 0:
                                user_right = 1;
                                break;
                            case 1:
                                user_right = 0;
                                break;
                            default:
                                user_right = 2;
                                break;
                        }
                    }
                    else
                        user_right = 2;
                }
                else if (owner_id == id_user)
                    user_right = 0;
                else
                    user_right = 2;
                UnityWebRequest www2 = UnityWebRequests.Get(urlSERVER, "api/datasetqualities/?id_datasets=" + dataset_id);
                yield return www2.SendWebRequest();
                if (www2.result == UnityWebRequest.Result.ConnectionError || www2.result == UnityWebRequest.Result.ProtocolError)
                {
                    MorphoDebug.LogError("Error while fetching quality for dataset : " + www2.error);
                }
                else
                {
                    JSONNode N2 = JSONNode.Parse(www2.downloadHandler.text);
                    if (N2.Count > 0)
                    {
                        quality = int.Parse(N2[0])-1;
                    }
                    else
                    {
                        quality = 0;
                    }
                    parseParameters(set_params[dataset_id], activateXR);
                    load_standalone_plot = true;
                }
                www.Dispose();
                www2.Dispose();
            }
            }
        else
        {
            UnityWebRequest www2 = UnityWebRequests.Get(urlSERVER, "api/datasetqualities/?id_datasets=" + dataset_id);
            yield return www2.SendWebRequest();
            if (www2.result == UnityWebRequest.Result.ConnectionError || www2.result == UnityWebRequest.Result.ProtocolError)
            {
                MorphoDebug.LogError("Error while fetching quality for dataset : " + www2.error);
            }
            else
            {
                JSONNode N2 = JSONNode.Parse(www2.downloadHandler.text);
                if (N2.Count > 0)
                {
                    quality = int.Parse(N2[0])-1;
                }
                else
                {
                    quality = 0;
                }
                parseParameters(set_params[dataset_id], activateXR);
                load_standalone_plot = true;
            }
            www2.Dispose();
            parseParameters(set_params[dataset_id], activateXR);
            load_standalone_plot = true;
        }
        if (_adminMode)
            user_right = 0;
        }


        public void UpdateStatePlot(bool state)
    {
        use_plot = state;
        start_plot = state;
        //ResetFieldPlotInteractable(!state);
        //UpdateButtonPlot();
    }

    private IEnumerator Start()
    {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "LoadParameters")
        {

#if UNITY_WEBGL
            //add hide everything here!
#else

        FindParamsList();
        use_plot = false;
        start_plot = false;
        //ResetFieldPlot();
        //UpdateButtonPlot();
        //ActivatePanelOS(InteractionLoadScene.instance.netPanel);
        //ActiveWelcomeText(true);
#endif
        }
        //if (previousLoadedScene == "") ActiveWelcomeText(true); //TO CHECK IF NECESSARY
        //MorphoDebug.Log("Start Load Paramters");
        OBJ = "";
        objReceived = false;
        obj_parts.Clear();
        obj_parts = new List<string>();

        DontDestroyOnLoad(gameObject);//Allow to keep object through the scenes
                                           //yield return new WaitForSeconds(1.0f); //TO CHECK IF NECESSARY

        //URL DEFINITION
#if UNITY_EDITOR
        urlSERVER = "https://morpho.lirmm.fr/";
        urlSERVER = "https://morphonet.org/";
#elif UNITY_WEBGL
        urlSERVER = "https://morphonet.org/";
#else
        urlSERVER = "https://morphonet.org/";
#endif
#if UNITY_WEBGL
        urlSERVER = Application.absoluteURL;
#endif
        if (urlSERVER.Contains("/")) // Url server should not contain "/"
        {
            string[] taburl = urlSERVER.Replace("https://","").Replace("http://","").Split('/');
            urlSERVER = "https://"+taburl[0];
        }

        MorphoDebug.Log("Load parameters from " + urlSERVER);
        //yield return new WaitForEndOfFrame();  //TO CHECK IF NECESSARY

#if !UNITY_EDITOR && UNITY_WEBGL
        loadingReady(); // READY FOR REACT COMMUNCATION
        yield return new WaitUntil(() => ready);
        RetrieveParameters();  //GET PARAMETERS FORM REACT

#endif

        //if (buildType == BuildType.MorphoPlot) setupDesktopMorphoplot(); //WAS FOR THE STANDANLONE DEDICATED FOR MORPHOPLOT  , NOT USED ANYMORE, BUT KEEP IT

        yield return null;
    }

    private void FindParamsList()
    {
        // InteractionLoadScene.instance.UI_standalone_parent = GameObject.Find("params_list");
        // InteractionLoadScene.instance.UI_set_example = InteractionLoadScene.instance.UI_standalone_parent.transform.GetChild(0).gameObject;
    }

    public void receiveOBJStringPart(string obj_part)
    {
        // MorphoDebug.Log("Received : " + obj_part);
        obj_parts.Add(obj_part);
    }

#if UNITY_WEBGL
    public void Update()
    {
        if (Screen.fullScreen != fullscreen_backup)
        {
            fullscreen_backup = Screen.fullScreen;
            if (!Screen.fullScreen)
            {
                LeaveFullscreen();
            }
        }
    }
#endif

    public void receiveSTL(string stl)
    {
        //  MorphoDebug.Log("receive stl");
        STL = Encoding.Unicode.GetBytes(stl);
        //  MorphoDebug.Log(STL);
        //   MorphoDebug.Log(STL.Length);
        is_STL = true;
        objReceived = true;
    }

    public void OBJ_received()
    {
        // MorphoDebug.Log("Ready to parse ");
        objReceived = true;
    }

    public IEnumerator receiveLiveModeOBJ(string parameters)
    {
        yield return new WaitUntil(() => objReceived);
        foreach (string s in obj_parts)
        {
            OBJ += s;
        }
        obj_parts.Clear();
        obj_parts = new List<string>();
        //MorphoDebug.Log("obj length unity = "+OBJ.Length);
        parseParametersLiveMode(parameters);
    }

    public void getParameters(string parameters)
    {
        //MorphoDebug.Log("Return message received");
        //OLD and unneeded ?
        /*if (StandaloneMainMenuInteractions.instance.loadingText)
        {
            StandaloneMainMenuInteractions.instance.loadingText.GetComponent<Text>().text = "Please wait until your dataset is loaded";
            //MorphoDebug.Log("Text modified");
        }*/

        if (liveMode == true)
        {
            StartCoroutine(receiveLiveModeOBJ(parameters));
        }
        else
        {
            parseParameters(parameters);
        }
    }

#if UNITY_WEBGL
    public IEnumerator DelayFocus(string p_focus)
    {
        yield return new WaitForSeconds(0.8f);
        if (p_focus == "0") {
          //  MorphoDebug.Log("Cancel input");
            WebGLInput.captureAllKeyboardInput = false;
        } else {
         //   MorphoDebug.Log("Retake input");
            WebGLInput.captureAllKeyboardInput = true;
        }
    }
#endif

    public void FocusCanvas(string p_focus)
    {
        //  MorphoDebug.Log("received focus : " + p_focus);
#if !UNITY_EDITOR && UNITY_WEBGL
    StartCoroutine(DelayFocus(p_focus));
#endif
    }

    public void parseParametersLiveMode(string parameters)
    {
        //COPY PAST OBJ IN WEB BROWSER
        JSONNode N = JSONNode.Parse(parameters);
        //MorphoDebug.Log(parameters);
        id_dataset = -2;
        privacy = "private";
        UnityEngine.Debug.LogError(N["name"].ToString());
        embryo_name = N["name"].ToString().Replace('"', ' ');

        min_time = 0;
        max_time = 0;
        id_owner = 0;
        if (N["id_user"] != null && UtilsManager.convertJSON(N["id_user"]) != "null")
        {
            id_owner = int.Parse(N["id_user"].ToString().Replace('"', ' ').Trim());
        }
        quality = 0;
        id_user = id_owner;
        name_user = "Live";
        surname_user = "Mode";
        user_right = 1;

        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "SceneEmbryo")
        {
            previousLoadedScene = "SceneEmbryo";
            UnityEngine.SceneManagement.SceneManager.LoadScene("SceneEmbryo");
        }
    }

    public void setupDesktopMorphoplot()
    {
        //MORPHOPLOT STANDALONE , NOT USED ANY MORE
        id_dataset = 0;
        privacy = "private";
        morphoplot_port_send = 9875;
        morphoplot_port_recieve = 9876;
        //change embryo name here with dataset name... if in standalone
        embryo_name = "Plot";
        min_time = 0;
        max_time = 0;
        api_Time = 0;
        id_user = 1;
        name_user = "Emmanuel";
        surname_user = "Faure";
        user_right = 3;
    }

    public void parseParameters(string parameters, bool xrActivated = false,bool load_from_plot=false)
    {

        Debug.Log(parameters);
        JSONNode N = JSONNode.Parse(parameters);
        id_dataset = int.Parse(N["id"].ToString().Replace('"', ' ').Trim());
        privacy = "private";

        if (id_dataset == 0)
        {
#if UNITY_WEBGL
WebGLInput.captureAllKeyboardInput = true;
#endif
        }
        embryo_name = N["name"].ToString().Replace('"', ' ');
        if (load_from_plot)
        {
            embryo_name = embryo_name.Replace("&&", " ");
        }
        min_time = int.Parse(N["mintime"].ToString().Replace('"', ' ').Trim());
        max_time = int.Parse(N["maxtime"].ToString().Replace('"', ' ').Trim());
        id_owner = int.Parse(N["id_people"].ToString().Replace('"', ' ').Trim()); ;

        if (N["server"]["adress"] != null && N["server"]["path"] != null)
        {
            server_adress = N["server"]["adress"].ToString().Replace('"', ' ').Trim();

            server_path = N["server"]["path"].ToString().Replace('"', ' ').Trim();

            char last = server_path.Last();
            if (last != '/')
            {
                //If people path if not ending with '/', we need to add
                server_path = server_path + '/';
            }
        }

        if (N["bundle"] != null && UtilsManager.convertJSON(N["bundle"]) != "null")
        {
            is_bundle = int.Parse(N["bundle"].ToString().Replace('"', ' ').Trim());
        }
        if (N["id_dataset_type"] != null && UtilsManager.convertJSON(N["id_dataset_type"]) != "null")
        {
            id_type = UtilsManager.convertJSONToInt(N["id_dataset_type"]);
        }

        CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
        ci.NumberFormat.CurrencyDecimalSeparator = ".";

        if (N["rotation"] != null && UtilsManager.convertJSON(N["rotation"]) != "null")
        {
            string[] rot = UtilsManager.convertJSON(N["rotation"]).Replace('(', ' ').Replace(')', ' ').Trim().Split(',');
            if (rot.Count() == 4)
            {
                init_rotation = new Quaternion(float.Parse(rot[0], NumberStyles.Any, ci), float.Parse(rot[1], NumberStyles.Any, ci), float.Parse(rot[2], NumberStyles.Any, ci), float.Parse(rot[3], NumberStyles.Any, ci));
            }
        }

        if (N["translation"] != null && UtilsManager.convertJSON(N["translation"]) != "null")
        {
            string[] trans = UtilsManager.convertJSON(N["translation"]).Replace('(', ' ').Replace(')', ' ').Trim().Split(',');
            if (trans.Count() == 3)
            {
                init_translation = new Vector3(float.Parse(trans[0], NumberStyles.Any, ci), float.Parse(trans[1], NumberStyles.Any, ci), float.Parse(trans[2], NumberStyles.Any, ci));
            }
        }

        if (N["scale"] != null && UtilsManager.convertJSON(N["scale"]) != "null")
        {
            string[] sca = UtilsManager.convertJSON(N["scale"]).Replace('(', ' ').Replace(')', ' ').Trim().Split(',');
            if (sca.Count() == 3)
            {
                init_scale = new Vector3(float.Parse(sca[0], NumberStyles.Any, ci), float.Parse(sca[1], NumberStyles.Any, ci), float.Parse(sca[2], NumberStyles.Any, ci));
            }
        }

        if (N["rgpd"] != null && UtilsManager.convertJSON(N["rgpd"]) != "null")
        {
            rgpd = UtilsManager.convertJSON(N["rgpd"]) != "false";
        }

        if (N["spf"] != null && UtilsManager.convertJSON(N["spf"]) != "null")
        {
            spf = UtilsManager.convertJSONToInt(N["spf"]);
        }

        if (N["dt"] != null && UtilsManager.convertJSON(N["dt"]) != "null")
        {
            dt = UtilsManager.convertJSONToInt(N["dt"]);
        }

#if !UNITY_STANDALONE
        if (N["quality"] != null && UtilsManager.convertJSON(N["quality"]) != "null")
        {
            quality = UtilsManager.convertJSONToInt(N["quality"]);
        }
#endif

        //USER
        if (N["u_ID"] != null)
            id_user = int.Parse(N["u_ID"].ToString().Replace('"', ' ').Trim());

        if (N["u_name"] != null && UtilsManager.convertJSON(N["u_name"]) != "null")
        {
            name_user = UtilsManager.convertJSON(N["u_name"]);
        }

        if (N["u_token"] != null && UtilsManager.convertJSON(N["u_token"]) != "null")
        {
            user_token = UtilsManager.convertJSON(N["u_token"]);
        }

        if (N["u_surname"] != null && UtilsManager.convertJSON(N["u_surname"]) != "null")
        {
            surname_user = UtilsManager.convertJSON(N["u_surname"]);
        }

#if !UNITY_STANDALONE
        if (N["u_right"] != null && UtilsManager.convertJSON(N["u_right"]) != "null")
        {
            user_right = int.Parse(N["u_right"]);
        }
        else
        {
            //If we didn't find the rights , consider public
            user_right = 2;
        }
#endif

        if (N["port_send"] != null && UtilsManager.convertJSON(N["port_send"]) != "null")
        {
            morphoplot_port_send = int.Parse(N["port_send"].ToString().Replace('"', ' ').Trim());
        }
        if (N["port_recieve"] != null && UtilsManager.convertJSON(N["port_recieve"]) != "null")
        {
            morphoplot_port_recieve = int.Parse(N["port_recieve"].ToString().Replace('"', ' ').Trim());
        }

        if (N["host_adress"] != null && UtilsManager.convertJSON(N["host_adress"]) != "null")
        {
            morphoplot_adress = N["host_adress"].ToString().Replace('"', ' ').Trim() + ":";
        }

        if (N["plot_host"] != null && UtilsManager.convertJSON(N["plot_host"]) != "null")
        {
            morphoplot_adress = N["plot_host"].ToString().Replace('"', ' ').Trim() + ":";
        }

        if (N["type"] != null && UtilsManager.convertJSON(N["type"]) != "null")
        {
            type = N["type"].ToString().Replace('"', ' ').Trim();
        }

        if (N["privacy"] != null && UtilsManager.convertJSON(N["privacy"]) != "null")
        {
            privacy = N["privacy"].ToString().Replace('"', ' ').Trim();
        }

        parseAPIMode(N);

        parsed_parameters = true;
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "SceneEmbryo")
        {
            previousLoadedScene = "SceneEmbryo";
            if (xrActivated)
            {
                var asyncOp = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("SceneEmbryo");
                asyncOp.completed += (_) => MorphoNet.SceneManager.Instance.EnableVr();
            }
            else
            {
                var asyncOp = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("SceneEmbryo");
                asyncOp.completed += (_) => MorphoNet.SceneManager.Instance.DisableVr();
            }
        }
    }

    //When a url contain specifc parameters on load
    public void parseAPIMode(JSONNode N)
    {
        if (N["menu"] != null && UtilsManager.convertJSON(N["menu"]) != "null")
        {
            //     MorphoDebug.Log("LoadParameters -  Add Menu " + UtilsManager.convertJSON(N["menu"]));
            api_menu = UtilsManager.convertJSON(N["menu"]);
        }

        foreach (string key in N.Keys)
        {
            if (key.StartsWith("gene"))
            {
                //MorphoDebug.Log(" Add GENES " +Instantiation.convertJSON(N[key]));
                if (api_Genes == null)
                {
                    api_Genes = new List<string>();
                }

                api_Genes.Add(UtilsManager.convertJSON(N[key]));
            }
            else if (key.StartsWith("bttn")) //bttn1=hide_movie
            {
                if (api_Buttons == null) api_Buttons = new Dictionary<string, bool>();
                string button_value = UtilsManager.convertJSON(N[key]); //ex : hide_movie or show_genetic
                string[] button_array = button_value.Split('_');
                string action = button_array[0];
                string menustosh = button_array[1];
                if (action.Equals("hide")) api_Buttons[menustosh] = false;
                else if (action.Equals("show")) api_Buttons[menustosh] = true;
                //MorphoDebug.Log("Button " + menustosh + "->" + action);
            }
            else if (key.StartsWith("cor"))
            {
                if (api_Correspondence == null) api_Correspondence = new Dictionary<int, int>();
                string coorespondence_value = UtilsManager.convertJSON(N[key]); //ex : hide_12 or load_34 or apply_45
                string[] coorespondence_array = coorespondence_value.Split('_');
                int id_correspondence;
                if (int.TryParse(coorespondence_array[1], out id_correspondence))
                {
                    string action = coorespondence_array[0];
                    if (action.Equals("hide")) api_Correspondence[id_correspondence] = 1;
                    else if (action.Equals("load")) api_Correspondence[id_correspondence] = 2;
                    else if (action.Equals("apply")) api_Correspondence[id_correspondence] = 3;
                    //  MorphoDebug.Log("API_Correspondence " + id_correspondence + "->" + action);
                }
            }
        }

        if (N["time"] != null && UtilsManager.convertJSON(N["time"]) != "null")
        {
            api_Time = UtilsManager.convertJSONToInt(N["time"]); //Load the Embryo at a specific time point
            //MorphoDebug.Log("LoadParameters - API_Time =" + api_Time);
        }
        if (N["stage"] != null && UtilsManager.convertJSON(N["stage"]) != "null")
        {
            api_Stage = "Stage " + UtilsManager.convertJSON(N["stage"]); //Show a specific Stage
            // MorphoDebug.Log("LoadParameters - API_Stage =" + api_Stage);
        }

        if (N["territory"] != null && UtilsManager.convertJSON(N["territory"]) != "null")
        {
            api_Territory = UtilsManager.convertJSON(N["territory"]).Replace('_', ' '); //Load a specifc territoy
                                                                                        //  MorphoDebug.Log("LoadParameters - API_Territory =" + api_Territory);
        }
        if (N["fate"] != null && UtilsManager.convertJSON(N["fate"]) != "null")
        {
            api_Fate = UtilsManager.convertJSON(N["fate"]).Replace('_', ' ');//??
            //MorphoDebug.Log("LoadParameters - API_Fate =" + api_Fate);
        }
    }
}
