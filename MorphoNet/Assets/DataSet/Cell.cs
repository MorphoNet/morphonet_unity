﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using MorphoNet.Core;
using System;

namespace MorphoNet
{
    public class Cell
    {
        public int t; //Cell Time
        public TimePoint TimePoint;
        public string ID; //Cell at this time point
        public List<int> selection; //List of Color Selction
                                    //public int currentIxSelect; //Just indice of the id selection showed (automatique iteration and loop infinite !)

        public List<Cell> Mothers; //Pointer to  Mothers
        public List<Cell> Daughters;//Pointer to  Daughters

        public bool selected; //If cell is selected true

        public bool show = true; //Do we display this cell ?

        //Cell Description
        public Dictionary<int, string> Infos; //Map of infos (fate,compacity,volume,surface...)

        public List<Cell> Neigbhors; //List of Neibhors

        //Curation
        public Dictionary<int, List<Curation>> Curations; //List of Curations for each infos

        public Color UploadColor; //Color upload by the user

        public bool UploadedColor = false; //Color upload by the user
        public Color UploadColorMain; //Color upload by the user
        public bool UploadedMainColor = false; //Color upload by the user
        public Color UploadColorHalo; //Color upload by the user
        public bool UploadedHaloColor = false; //Color2 upload by the user
        public Color UploadColorSecond; //Color upload by the user
        public bool UploadedSecondColor = false; //Color2 upload by the user
        public float UploadMetallicParameter; //Value of the metallic parameter upload by the user
        public bool UploadedMetallicParameter = false; //Metallic parameter upload by the user
        public float UploadSmoothnessParameter; //Value of the smoothness parameter upload by the user
        public bool UploadedSmoothnessParameter = false; //Smoothness parameter upload by the user
        public float UploadTransparencyParameter; //Value of the transparency parameter upload by the user
        public bool UploadedTransparencyParameter = false; //Transparency parameter upload by the user

        public int lifePast;
        public int lifeFutur;

        public Primitive primi; // if the object is a primitive, the data of the primitive

        public Dictionary<string, CellChannelRenderer> Channels = new Dictionary<string, CellChannelRenderer>(); //List of Channels

        public bool IsFirstMother => Mothers == null || Mothers.Count == 0;
        public bool IsCreatedFromDivision => Mothers != null && Mothers.Count == 1 && Mothers[0].Daughters.Count > 1;
        public bool IsLastDaughterBeforeDivision => Daughters != null && Daughters.Count > 1;
        public bool IsLastDaughter => Daughters == null || Daughters.Count == 0;

        public void SetToUpdate()
        {
            if (TimePoint != null)
                TimePoint.AddCellToRendererUpdate(this);
        }

        public void UpdateAllChannel()
        {
            foreach (CellChannelRenderer channelRenderer in Channels.Values)
            {
                if (channelRenderer.ShouldBeUpdated)
                    TimePoint.StartCoroutine(channelRenderer.UpdateChannelRenderer());
            }
        }

        public void UpdateFirstChannel()
        {
            var ch = getFirstChannel();
            if (ch.ShouldBeUpdated)
                TimePoint.StartCoroutine(ch.UpdateChannelRenderer());
        }

        public bool Highlighted
        {
            get => _ShouldBeHiglighted;
            set
            {
                updateShader();
                _ShouldBeHiglighted = value;
            }
        }

        private bool _ShouldBeHiglighted = false;

        public Cell(int t, string ID) //Constructeur
        {
            this.t = t;
            this.ID = ID;
            selected = false;
            lifePast = -1;
            lifeFutur = -1;
            UploadedColor = false;
            show = true;
            primi = null;
        }

        public string getName()
        {
            string n = "";

            if (MorphoTools.GetDataset().MaxTime != MorphoTools.GetDataset().MinTime)
                n = t.ToString() + ",";

            n += ID.ToString();

            return n;
        }

        public void Destroy()
        {
            if (Neigbhors != null)
                Neigbhors.Clear();

            Neigbhors = null;

            if (Infos != null)
                Infos.Clear();

            Infos = null;
        }

        public Color GetCellColor()
        {
            CellChannelRenderer ObjectRender = getFirstChannel();
            Color final = Color.white;
            if ((this.UploadedMainColor || this.UploadedHaloColor || this.UploadedSecondColor ||
                 this.UploadedMetallicParameter || this.UploadedSmoothnessParameter ||
                 this.UploadedTransparencyParameter) && !this.selected && ObjectRender != null && ObjectRender.Renderer != null)
            {
                if ((ObjectRender.Renderer.sharedMaterial.name != "Custom_2colors_noise (Instance)" &&
                     ObjectRender.Renderer.sharedMaterial.name != "Custom_2colors_noise") && this.UploadedSecondColor)
                {
                    if (this.UploadedMainColor) final = this.UploadColorMain;
                }

                else if ((ObjectRender.Renderer.sharedMaterial.name == "Custom_2colors_noise (Instance)" || ObjectRender.Renderer.sharedMaterial.name == "Custom_2colors_noise") && !UploadedSecondColor)
                {
                    final = ObjectRender.Renderer.material.GetColor("_Color1");
                }
                else if ((ObjectRender.Renderer.sharedMaterial.name == "Custom_2colors_noise (Instance)" || ObjectRender.Renderer.sharedMaterial.name == "Custom_2colors_noise") && UploadedSecondColor)
                {
                    final = ObjectRender.Renderer.material.GetColor("_Color1");
                }

                if (UploadedSecondColor)
                {
                    final = UploadColorSecond;
                }
                else
                {
                    if (UploadedMainColor)
                    {
                        final = UploadColorMain;
                    }

                    if (UploadedHaloColor)
                    {
                        final = UploadColorHalo;
                    }
                }
            }
            else
            {
                if (UploadedColor)
                {
                    final = UploadColor;
                }
                else if (selection != null && selection.Count > 0)
                {
                    if (selection.Count >= 1)
                    {
                        final = SelectionManager.getSelectedMaterial(selection[0]).color;
                    }
                }
                else
                {
                    if (selected)
                    {
                        final = Color.yellow;
                    }
                }
            }

            return final;
        }


        public CellChannelRenderer getFirstChannel()
        {
            if (Channels == null)
                return null;

            foreach (KeyValuePair<string, CellChannelRenderer> ite in Channels)
                return ite.Value;

            return null;
        }

        public CellChannelRenderer getChannel(string ch)
        {
            if (Channels == null)
                return null;

            if (Channels.ContainsKey(ch))
                return Channels[ch];

            return null;
        }

        public CellChannelRenderer GetChannel(int nb)
        {
            if (Channels == null)
                return null;
            int i = 0;
            foreach (KeyValuePair<string, CellChannelRenderer> ite in Channels)
            {
                if(i==nb)
                    return ite.Value;
                i++;
            }

            return null;
        }

        public CellChannelRenderer addChannel(string ch, GameObject go, Channel cho)
        {
            if (Channels == null)
                Channels = new Dictionary<string, CellChannelRenderer>();

            Channels[ch] = new CellChannelRenderer(go, this, cho);

            return Channels[ch];
        }

        public void clearInfos(int idx)
        {
            if (Infos != null && Infos.ContainsKey(idx))
                Infos.Remove(idx);
        }

        public void setInfos(int idx, string v)
        {
            if (Infos == null)
                Infos = new Dictionary<int, string>();
            Infos[idx] = v;
        }

        public void addInfos(int idx, string v)
        {
            if (Infos == null)
                Infos = new Dictionary<int, string>();
            if (Infos.ContainsKey(idx))
                Infos[idx] += ";" + v;
            else
                Infos[idx] = v;
        }

        public void clearRelations()
        {
            if (Mothers != null)
                Mothers.Clear();
            if (Daughters != null)
                Daughters.Clear();
        }

        public void addMother(Cell c)
        {
            if (Mothers == null)
                Mothers = new List<Cell>();
            if (!Mothers.Contains(c))
                Mothers.Add(c);
        }

        public void removeMother(Cell c)
        {
            if (Mothers == null)
                Mothers = new List<Cell>();
            if (Mothers.Contains(c))
                Mothers.Remove(c);
        }

        public void addDaughter(Cell c)
        {
            if (Daughters == null)
                Daughters = new List<Cell>();
            if (!Daughters.Contains(c))
                Daughters.Add(c);
            c.addMother(this);
        }

        public void RemoveDaughter(Cell c)
        {
            if (Daughters == null)
                Daughters = new List<Cell>();
            if (Daughters.Contains(c))
                Daughters.Remove(c);
            c.removeMother(this);
        }

        public void addNeigbhors(Cell c)
        {
            if (Neigbhors == null)
                Neigbhors = new List<Cell>();
            if (!Neigbhors.Contains(c))
                Neigbhors.Add(c);
        }

        public string getInfos(int idx)
        {
            //Check First if any curration exist for this cell
            string curated = getCuration(idx);

            if (curated != null)
                return curated;

            //Otherwise check normal values
            if (idx >= 0 && Infos != null && Infos.ContainsKey(idx))
            {
                string[] row = Infos[idx].Trim().Split(',');

                if (row.Length >= 2)
                    return Infos[idx];

                float v;

                if (float.TryParse(Infos[idx], out v))
                    return "" + (Mathf.Round(v * 1000) / 1000);
                else
                    return Infos[idx];
            }
            return "";
        }

        public string getDisplayInfos(int idx)
        {
            //Check First if any curration exist for this cell
            string curated = getDisplayCuration(idx);
            if (curated != null)
                return curated;
            //Otherwise check normal values
            if (idx >= 0 && Infos != null && Infos.ContainsKey(idx))
            {
                string[] row = Infos[idx].Trim().Split(',');
                if (row.Length >= 2)
                    return Infos[idx];
                float v;
                if (float.TryParse(Infos[idx], out v))
                {
                    return "" + (Mathf.Round(v * 1000) / 1000);
                }
                else
                    return Infos[idx];
            }
            return "";
        }

        public bool loadColor(int idx)
        {
            string curated = getCuration(idx);
            if (curated != null)
                return parseColor(curated);
            //Otherwise check normal values
            if (idx >= 0 && Infos != null && Infos.ContainsKey(idx))
                return parseColor(Infos[idx]);

            return false;
        }

        private bool parseColor(string col)
        {
            float R, B, G;
            string[] row = col.Trim().Split(',');
            if (row.Length >= 3 && float.TryParse(row[0], out R) && float.TryParse(row[1], out G) && float.TryParse(row[2], out B))
            {
                UploadNewColor(new Color(R / 255f, G / 255f, B / 255f, 1));
                return true;
            }
            return false;
        }

        public bool LoadColorSelection(int idx)
        {
            if (idx >= 0 && Infos != null && Infos.ContainsKey(idx))
            {
                if (selection == null)
                    selection = new List<int>();
                selection.Add(int.Parse(Infos[idx]));
                updateShader();
                return true;
            }
            return false;
        }

        public bool UnloadColorSelection(int idx)
        {
            if (idx >= 0 && Infos != null && Infos.ContainsKey(idx))
            {
                if (selection.Contains(int.Parse(Infos[idx])))
                    selection.Remove(int.Parse(Infos[idx]));
                updateShader();
                return true;
            }
            return false;
        }

        public string getShowString()
        {
            string Description = "";
            if (Infos != null)
                foreach (Correspondence cor in MorphoTools.GetDataset().Infos.Correspondences)
                    if (cor.isValueShow)
                        Description += getInfos(cor.id_infos) + "\n";
            return Description;
        }

        //Add a new curration
        public void setCuration(int idInfos, Curation cur)
        {
            if (Curations == null)
                Curations = new Dictionary<int, List<Curation>>();

            if (!Curations.ContainsKey(idInfos))
                Curations.Add(idInfos, new List<Curation>());

            if (!Curations[idInfos].Contains(cur))
                Curations[idInfos].Add(cur);
        }

        public string getCuration(int idInfos)
        {
            if (Curations == null)
                return null;

            if (Curations != null && Curations.ContainsKey(idInfos) && Curations[idInfos].Count > 0)
                return Curations[idInfos][0].value;

            return null;
        }

        public string getCurationDate(int idInfos, string value)
        {
            if (Curations == null)
                return null;
            if (Curations != null && Curations.ContainsKey(idInfos) && Curations[idInfos].Count > 0)
            {
                return Curations[idInfos].Find((Curation c) => (c.value == value)).date;
            }
            return null;
        }

        // Return string of all actie curr
        public string getDisplayCuration(int idInfos)
        {
            if (Curations == null)
                return null;
            if (Curations != null && Curations.ContainsKey(idInfos) && Curations[idInfos].Count > 0)
            {
                string displayed = "";
                foreach (Curation c in Curations[idInfos])
                {
                    displayed += c.value + ",";
                }
                if (displayed.Length > 0)
                { displayed = displayed.Remove(displayed.Length - 1, 1); }
                return displayed;
            }
            return null;
        }

        public void deleteCuration(int idInfos, Curation c)
        {
            if (Curations != null && Curations.ContainsKey(idInfos) && Curations[idInfos].Contains(c))
            {
                Curations[idInfos].Remove(c);
                if (Curations[idInfos].Count == 0)
                {
                    Curations.Remove(idInfos);
                }
            }
        }

        //Check if this cell is on the right side
        public bool isRight()
        {
            int nbName = MorphoTools.GetDataset().Infos.getIdInfosForName();
            string name = getInfos(nbName);
            if (name.Length > 1 && name[name.Length - 1] == '*')
                return true;

            return false;
        }

        //Check if this cell is on the left side
        public bool isLeft()
        { return !isRight(); }

        //Load a unpload sphere
        public bool loadSphere(int idx, string v)
        {
            CellChannelRenderer ch = getFirstChannel();

            if (ch != null)
            {
                string[] row = v.Trim().Split(',');
                float x, y, z, r;
                if (row.Length >= 4 && float.TryParse(row[0], out x) && float.TryParse(row[1], out y) && float.TryParse(row[2], out z) && float.TryParse(row[3], out r))
                {
                    MorphoTools.GetTransformationsManager().resetDataSetRotation();
                    GameObject pt = GameObject.Instantiate(SetsManager.instance.sphere);
                    pt.name = "UPLOAD_" + idx;
                    pt.transform.SetParent(ch.AssociatedCellObject.transform);
                    Vector3 o = new Vector3(x, y, z);
                    o.Set(-o.y, o.x, o.z);
                    o.Set(o.x - MorphoTools.GetDataset().embryoCenter.x, o.y - MorphoTools.GetDataset().embryoCenter.y, o.z - MorphoTools.GetDataset().embryoCenter.z);
                    o.Set(o.x * InterfaceManager.instance.initCanvasDatasetScale, o.y * InterfaceManager.instance.initCanvasDatasetScale, o.z * InterfaceManager.instance.initCanvasDatasetScale); //was TrackingVectors.scaleFactor
                    pt.transform.position = o;
                    pt.transform.localScale = new Vector3(r, r, r);

                    pt.GetComponent<Renderer>().sharedMaterial = ch.AssociatedCellObject.GetComponent<Renderer>().sharedMaterial;

                    MorphoTools.GetTransformationsManager().updateDataSetRotation();
                    return true;
                }
            }
            return false;
        }

        public void removeObject(int idx)
        {
            CellChannelRenderer ch = getFirstChannel();
            if (ch != null && ch.AssociatedCellObject != null)
                while (ch.AssociatedCellObject.transform.Find("UPLOAD_" + idx) != null)//We can have multiple things (line or sphere ...)
                    GameObject.DestroyImmediate(ch.AssociatedCellObject.transform.Find("UPLOAD_" + idx).gameObject, true);
            GizmoManager.instance.DeleteInfos(idx); // remove line from list to display
        }

        public bool loadVector(int idx, string v)
        {
            CellChannelRenderer ch = getFirstChannel();
            if (ch != null)
            {
                string[] vectors = v.Trim().Split(':');
                if (vectors.Count() == 2)
                {
                    float x1, y1, z1, r1;
                    float x2, y2, z2, r2;
                    string[] row1 = vectors[0].Trim().Split(',');
                    string[] row2 = vectors[1].Trim().Split(',');

                    //Unify float decimal separator for this parse only
                    CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    ci.NumberFormat.CurrencyDecimalSeparator = ".";
                    if (row1.Length >= 4 && float.TryParse(row1[0], NumberStyles.Any, ci, out x1) && float.TryParse(row1[1], NumberStyles.Any, ci, out y1) && float.TryParse(row1[2], NumberStyles.Any, ci, out z1) && float.TryParse(row1[3], NumberStyles.Any, ci, out r1) &&
                        row2.Length >= 4 && float.TryParse(row2[0], NumberStyles.Any, ci, out x2) && float.TryParse(row2[1], NumberStyles.Any, ci, out y2) && float.TryParse(row2[2], NumberStyles.Any, ci, out z2) && float.TryParse(row2[3], NumberStyles.Any, ci, out r2))
                    {
                        MorphoTools.GetTransformationsManager().resetDataSetRotation();
                        GameObject vector = new GameObject();
                        vector.name = "UPLOAD_" + idx;
                        vector.AddComponent<LineRenderer>();
                        vector.GetComponent<LineRenderer>().useWorldSpace = false;
                        vector.transform.SetParent(ch.AssociatedCellObject.transform);
                        vector.GetComponent<LineRenderer>().positionCount = 2;

                        Vector3 o = new Vector3(x1, y1, z1);
                        Vector3 shift_initial_position = MorphoTools.GetDataset().embryo_container.transform.position - Camera.main.transform.position;

                        o.Set(o.x - MorphoTools.GetDataset().embryoCenter.x, o.y - MorphoTools.GetDataset().embryoCenter.y, o.z - MorphoTools.GetDataset().embryoCenter.z);
                        o.Set(o.x * InterfaceManager.instance.initCanvasDatasetScale +shift_initial_position.x, o.y * InterfaceManager.instance.initCanvasDatasetScale+ shift_initial_position.y, o.z * InterfaceManager.instance.initCanvasDatasetScale + shift_initial_position.z); //was TrackingVectors.scaleFactor
                        vector.GetComponent<LineRenderer>().SetPosition(0, o);

                        Vector3 d = new Vector3(x2, y2, z2);
                        d.Set(d.x - MorphoTools.GetDataset().embryoCenter.x, d.y - MorphoTools.GetDataset().embryoCenter.y, d.z - MorphoTools.GetDataset().embryoCenter.z);
                        d.Set(d.x * InterfaceManager.instance.initCanvasDatasetScale+shift_initial_position.x, d.y * InterfaceManager.instance.initCanvasDatasetScale+shift_initial_position.y, d.z * InterfaceManager.instance.initCanvasDatasetScale+shift_initial_position.z); //was TrackingVectors.scaleFactor

                        vector.GetComponent<LineRenderer>().SetPosition(1, d);

                        //WIDTH  (from 1 to 5)
                        vector.GetComponent<LineRenderer>().startWidth = r1 / 100f;
                        vector.GetComponent<LineRenderer>().endWidth = r2 / 100f;
                        vector.GetComponent<Renderer>().sharedMaterial = ch.AssociatedCellObject.GetComponent<Renderer>().sharedMaterial;

                        MorphoTools.GetTransformationsManager().updateDataSetRotation();

                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Add a connection (visible with a linerenderer between 2 elements of the dataset)
        /// </summary>
        /// <param name="idx">id of the info</param>
        /// <param name="v">value of the connection info</param>
        /// <param name="min">Min threshold of connection info</param>
        /// <param name="max">Max threshold of connection info</param>
        /// <returns>returns true if the connection was created properly</returns>
        public bool AddConnection(int idx, string v, float min, float max)
        {
            DataSet dataSet = MorphoTools.GetDataset();
            GameObject anc = dataSet.mesh_by_time[dataSet.CurrentTime];
            
            if (dataSet.InfoConnectionParent == null)
            {
                dataSet.InfoConnectionParent = new GameObject("Connection_Parent");
                dataSet.InfoConnectionParent.transform.parent = dataSet.GetTimepointAt(dataSet.CurrentTime).Pivot;
            }
            else if (dataSet.InfoConnectionParent.transform.childCount == 0)

            {
                GameObject.DestroyImmediate(dataSet.InfoConnectionParent);
                dataSet.InfoConnectionParent = new GameObject("Connection_Parent");

                dataSet.InfoConnectionParent.transform.parent = dataSet.GetTimepointAt(dataSet.CurrentTime).Pivot;
            }

            CellChannelRenderer ch = getFirstChannel();
            if (ch != null)
            {
                string[] tokens = v.Trim().Split(':');
                float value;
                if (tokens.Length == 2 && float.TryParse(tokens[1], NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                {
                    if (value <= max && value >= min)
                    {
                        Cell destination = dataSet.getCell(tokens[0], true);
                        if (destination != null && destination.getFirstChannel() != null && destination.getFirstChannel().AssociatedCellObject != null )
                        {
                            GameObject vector = new GameObject();

                            vector.name = "UPLOAD_" + idx;
                            LineRenderer r = vector.AddComponent<LineRenderer>();
                            ConnectionInfo c = vector.AddComponent<ConnectionInfo>();
                            c.Source = ch.AssociatedCellObject;
                            c.Destination = destination.getFirstChannel().AssociatedCellObject;

                            r.useWorldSpace = false;
                            vector.transform.SetParent(dataSet.InfoConnectionParent.transform);
                            r.positionCount = 2;
                            //set origin
                            Vector3 originpos = anc.transform.TransformPoint(ch.Gravity);
                            
                            //set destination
                            Vector3 destpos = anc.transform.TransformPoint(destination.getFirstChannel().Gravity);

                            if(Double.IsNaN(originpos.x) || Double.IsNaN(originpos.y) || Double.IsNaN(originpos.z) || Double.IsNaN(destpos.x) || Double.IsNaN(destpos.y) || Double.IsNaN(destpos.z))
                            {
                                GameObject.DestroyImmediate(vector);
                                return false;
                            }

                            r.SetPosition(0, originpos);
                            r.SetPosition(1, destpos);
                            c.SourcePos = originpos;
                            c.DestPos = destpos;
                            c.SCh = ch;
                            c.DCh = destination.getFirstChannel();

                            //set thickness
                            r.startWidth = value / 50f;
                            r.endWidth = value / 50f;
                            r.sharedMaterial = new Material(Shader.Find("Sprites/Default"));

                            r.startColor = Color.green;
                            r.endColor = Color.red;

                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public void removeConnection(int idx)
        {
            GameObject connectParent = MorphoTools.GetDataset().InfoConnectionParent;
            if (connectParent != null)
                while (connectParent.transform.Find("UPLOAD_" + idx) != null)//We can have multiple things (line or sphere ...)
                    GameObject.DestroyImmediate(connectParent.transform.Find("UPLOAD_" + idx).gameObject, true);
            GizmoManager.instance.DeleteInfos(idx); // remove line from list to display
        }

        //Load a upload lines
        public bool loadLines(int idx, string v)
        {
            CellChannelRenderer ch = getFirstChannel();
            if (ch != null)
            {
                string[] vectors = v.Trim().Split(':');
                if (vectors.Count() == 2 || vectors.Count() == 3)
                {
                    float x1, y1, z1;
                    float x2, y2, z2;
                    // color rgb infos
                    float r, g, b;

                    string[] row1 = vectors[0].Trim().Split(',');
                    string[] row2 = vectors[1].Trim().Split(',');

                    //Unify float decimal separator for this parse only
                    CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    ci.NumberFormat.CurrencyDecimalSeparator = ".";

                    if (row1.Length >= 3 && float.TryParse(row1[0], NumberStyles.Any, ci, out x1) && float.TryParse(row1[1], NumberStyles.Any, ci, out y1) && float.TryParse(row1[2], NumberStyles.Any, ci, out z1) &&
                        row2.Length >= 3 && float.TryParse(row2[0], NumberStyles.Any, ci, out x2) && float.TryParse(row2[1], NumberStyles.Any, ci, out y2) && float.TryParse(row2[2], NumberStyles.Any, ci, out z2))
                    {
                        MorphoTools.GetTransformationsManager().resetDataSetRotation();
                        Vector3 o = new Vector3(x1, y1, z1);

                        o.Set(o.x - MorphoTools.GetDataset().embryoCenter.x, o.y - MorphoTools.GetDataset().embryoCenter.y, o.z - MorphoTools.GetDataset().embryoCenter.z);
                        o.Set(o.x * InterfaceManager.instance.initCanvasDatasetScale, o.y * InterfaceManager.instance.initCanvasDatasetScale, o.z * InterfaceManager.instance.initCanvasDatasetScale); //origin of the line, scaled

                        Vector3 d = new Vector3(x2, y2, z2);
                        d.Set(d.x - MorphoTools.GetDataset().embryoCenter.x, d.y - MorphoTools.GetDataset().embryoCenter.y, d.z - MorphoTools.GetDataset().embryoCenter.z);
                        d.Set(d.x * InterfaceManager.instance.initCanvasDatasetScale, d.y * InterfaceManager.instance.initCanvasDatasetScale, d.z * InterfaceManager.instance.initCanvasDatasetScale); //dest of the line, scaled

                        //default color is black
                        Color line_color = Color.black;

                        //compute color if info exist
                        if (vectors.Count() == 3)
                        {
                            string[] row3 = vectors[2].Trim().Split(',');
                            if (row3.Length >= 3 && float.TryParse(row3[0], NumberStyles.Any, ci, out r) && float.TryParse(row3[1], NumberStyles.Any, ci, out g) && float.TryParse(row3[2], NumberStyles.Any, ci, out b))
                            {
                                line_color.r = r;
                                line_color.g = g;
                                line_color.b = b;
                            }
                        }
                        //send the line to be displayed, linked to a time step and a cell id (this script is attached to the camera)
                        GizmosTest.DrawLine(o, d, idx, ID, t, line_color);
                        MorphoTools.GetTransformationsManager().updateDataSetRotation();

                        return true;
                    }
                }
            }
            return false;
        }

        //CHANGE THIS TO VECTOR
        public bool loadArrow(int idx, string v)
        {
            CellChannelRenderer ch = getFirstChannel();
            if (ch != null)
            {
                string[] vectors = v.Trim().Split(':');
                if (vectors.Count() == 2 || vectors.Count() == 3)
                {
                    float x1, y1, z1;
                    float x2, y2, z2;
                    // color rgb infos
                    float r, g, b;

                    string[] row1 = vectors[0].Trim().Split(',');
                    string[] row2 = vectors[1].Trim().Split(',');

                    //Unify float decimal separator for this parse only
                    CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    ci.NumberFormat.CurrencyDecimalSeparator = ".";

                    if (row1.Length >= 3 && float.TryParse(row1[0], NumberStyles.Any, ci, out x1) && float.TryParse(row1[1], NumberStyles.Any, ci, out y1) && float.TryParse(row1[2], NumberStyles.Any, ci, out z1) &&
                        row2.Length >= 3 && float.TryParse(row2[0], NumberStyles.Any, ci, out x2) && float.TryParse(row2[1], NumberStyles.Any, ci, out y2) && float.TryParse(row2[2], NumberStyles.Any, ci, out z2))
                    {
                        MorphoTools.GetTransformationsManager().resetDataSetRotation();
                        Vector3 o = new Vector3(x1, y1, z1);

                        o.Set(o.x - MorphoTools.GetDataset().embryoCenter.x, o.y - MorphoTools.GetDataset().embryoCenter.y, o.z - MorphoTools.GetDataset().embryoCenter.z);
                        o.Set(o.x * InterfaceManager.instance.initCanvasDatasetScale, o.y * InterfaceManager.instance.initCanvasDatasetScale, o.z * InterfaceManager.instance.initCanvasDatasetScale); //origin of the line, scaled

                        Vector3 d = new Vector3(x2, y2, z2);
                        d.Set(d.x - MorphoTools.GetDataset().embryoCenter.x, d.y - MorphoTools.GetDataset().embryoCenter.y, d.z - MorphoTools.GetDataset().embryoCenter.z);
                        d.Set(d.x * InterfaceManager.instance.initCanvasDatasetScale, d.y * InterfaceManager.instance.initCanvasDatasetScale, d.z * InterfaceManager.instance.initCanvasDatasetScale); //dest of the line, scaled

                        //default color is black
                        Color line_color = Color.black;

                        //compute color if info exist
                        if (vectors.Count() == 3)
                        {
                            string[] row3 = vectors[2].Trim().Split(',');
                            if (row3.Length >= 3 && float.TryParse(row3[0], NumberStyles.Any, ci, out r) && float.TryParse(row3[1], NumberStyles.Any, ci, out g) && float.TryParse(row3[2], NumberStyles.Any, ci, out b))
                            {
                                line_color.r = r;
                                line_color.g = g;
                                line_color.b = b;
                            }
                        }
                        //send the line to be displayed, linked to a time step and a cell id (this script is attached to the camera)
                        GizmosTest.DrawVector(o, d, idx, ID, t, line_color);
                        MorphoTools.GetTransformationsManager().updateDataSetRotation();

                        return true;
                    }
                }
            }
            return false;
        }

        public void updateShader()
        {
            if (Channels != null)
            {
                foreach (KeyValuePair<string, CellChannelRenderer> item in Channels)
                {
                    CellChannelRenderer ch = item.Value;
                    ch.ShouldBeHiglighted = Highlighted;
                    ch.ShouldBeUpdated = true;
                }
            }
        }

        //When add a selection value
        public void addSelection(int selectValue, bool add_to_lineage = true)
        {
            if (selection == null)
                selection = new List<int>();
            if (!selection.Contains(selectValue))
            {
                selection.Add(selectValue);

                updateShader();
            }
            if (Lineage.isLineage && add_to_lineage)
                Lineage.AddToColor(this, Lineage.GetCellColor(this));
        }

        //We remove a selection value
        public void removeSelection(int selectValue, bool add_to_lineage = true)
        {
            if (selection != null)
            {
                if (selection.Contains(selectValue))
                {
                    selection.Remove(selectValue);
                    if (Lineage.isLineage && add_to_lineage)
                    { Lineage.AddToColor(this, Lineage.GetCellColor(this)); }
                    updateShader();
                }
            }
        }

        public void RemoveAllSelection(bool add_to_lineage = true)
        {
            if (selection != null)
            {
                if (selection.Count > 0)
                {
                    selection.Clear();
                    if (Lineage.isLineage && add_to_lineage)
                    { Lineage.AddToColor(this, Lineage.GetCellColor(this)); }
                    updateShader();
                }
            }
        }

        //When clicked on a cell it select it
        public void choose(bool v, bool add_to_lineage = true)
        {
            if (selected != v)
            {
                selected = v;
            }
            if (Lineage.isLineage && add_to_lineage)
                Lineage.AddToColor(this, selected == true ? Lineage.SelectedColor : Lineage.GetCellColor_No_Selection(this));
            updateShader();
        }

        public void UploadNewColor(Color col)
        {
            UploadColor = col;
            UploadedColor = true;
            updateShader();
            if (Lineage.isLineage)
                Lineage.addColorCell(this, col);
        }

        public void UploadMainColor(Color col)
        {
            UploadColorMain = col;
            UploadedMainColor = true;
            updateShader();
            if (Lineage.isLineage)
                Lineage.addColorCell(this, col);
        }

        public void UploadHaloColor(Color col)
        {
            UploadColorHalo = col;
            UploadedHaloColor = true;
            updateShader();
            if (Lineage.isLineage)
                Lineage.addColorCell(this, col);
        }

        public void UploadSecondColor(Color col)
        {
            UploadColorSecond = col;
            UploadedSecondColor = true;
            updateShader();
            if (Lineage.isLineage)
                Lineage.addColorCell(this, col);
        }

        public void UploadMetallic(float val)
        {
            UploadMetallicParameter = val;
            UploadedMetallicParameter = true;
            updateShader();
        }

        public void UploadSmoothness(float val)
        {
            UploadSmoothnessParameter = val;
            UploadedSmoothnessParameter = true;
            updateShader();
        }

        public void UploadTransparency(float val)
        {
            UploadTransparencyParameter = val;
            UploadedTransparencyParameter = true;
            updateShader();
        }

        public void RemoveUploadColor()
        {
            if (UploadedColor)
            {
                UploadedColor = false;
                if (Lineage.isLineage)
                { Lineage.AddToColor(this, Lineage.GetCellColor(this)); }
                updateShader();
            }
        }

        public void RemoveUploadMainColor()
        {
            if (UploadedMainColor)
            {
                UploadedMainColor = false;
                if (Lineage.isLineage)
                { Lineage.AddToColor(this, Lineage.GetCellColor(this)); }
                updateShader();
            }
        }

        public void RemoveUploadHaloColor()
        {
            if (UploadedHaloColor)
            {
                UploadedHaloColor = false;
                if (Lineage.isLineage)
                { Lineage.AddToColor(this, Lineage.GetCellColor(this)); }
                updateShader();
            }
        }

        public void RemoveUploadSecondColor()
        {
            if (UploadedSecondColor)
            {
                UploadedSecondColor = false;
                if (Lineage.isLineage)
                { Lineage.AddToColor(this, Lineage.GetCellColor(this)); }
                updateShader();
            }
        }

        public void RemoveUploadMetallicParameter()
        {
            if (UploadedMetallicParameter)
            {
                UploadedMetallicParameter = false;
                updateShader();
            }
        }

        public void RemoveUploadTransparencyParameter()
        {
            if (UploadedTransparencyParameter)
            {
                UploadedTransparencyParameter = false;
                updateShader();
            }
        }

        public void RemoveUploadSmoothnessParameter()
        {
            if (UploadedSmoothnessParameter)
            {
                UploadedSmoothnessParameter = false;
            }
        }

        public void RemoveUploadColorInfo(int idx)
        {
            if (UploadedColor && idx >= 0 && Infos != null && Infos.ContainsKey(idx))
            {
                UploadedColor = false;
                if (Lineage.isLineage)
                { Lineage.AddToColor(this, Lineage.GetCellColor(this)); }
                updateShader();
            }
        }

        public void hide(bool v, bool add_to_lineage = true)
        {
            if (v)
            {
                choose(false, false); //If it was selected we remove it
            }
            if (show == v)
            {
                if (Lineage.isLineage && add_to_lineage)
                {
                    if (v)
                    {
                        Lineage.AddToHide(this);
                    }
                    else
                    {
                        Lineage.AddToShow(this);
                    }
                }
                show = !v;
            }
            updateShader();
        }

        public void hideAll(bool v, bool add_to_lineage = true)
        {
            hide(v, add_to_lineage);

            if (Mothers != null)
                foreach (Cell c in Mothers)
                    c.hidePast(v, add_to_lineage);
            if (Daughters != null)
                foreach (Cell c in Daughters)
                    c.hideFutur(v, add_to_lineage);
        }

        public void hidePast(bool v, bool add_to_lineage = true)
        {
            hide(v, add_to_lineage);
            if (Mothers != null)
                foreach (Cell c in Mothers)
                    c.hidePast(v, add_to_lineage);
        }

        public void hideFutur(bool v, bool add_to_lineage = true)
        {
            hide(v, add_to_lineage);
            if (Daughters != null)
                foreach (Cell c in Daughters)
                    c.hideFutur(v, add_to_lineage);
        }

        //Return the list of parents at t
        public List<Cell> GetParentAt(int nt)
        {
            if (t - 1 == nt) //PAST
                return Mothers;
            else if (t + 1 == nt) //FUTUR
                return Daughters;

            List<Cell> parent = new List<Cell>();
            if (t == nt)//SAME TIME
                parent.Add(this);
            else if (t > nt)
            { //VERY PAST
                if (Mothers != null)
                    foreach (Cell c in Mothers)
                    {
                        List<Cell> parentMother = c.GetParentAt(nt);
                        if (parentMother != null)
                            foreach (Cell m in parentMother)
                                if (!parent.Contains(m))
                                    parent.Add(m);
                    }
            }
            else if (t < nt)
            { //VERY FUTUR
                if (Daughters != null)
                    foreach (Cell c in Daughters)
                    {
                        List<Cell> parentDaughter = c.GetParentAt(nt);
                        if (parentDaughter != null)
                            foreach (Cell d in parentDaughter)
                                if (!parent.Contains(d))
                                    parent.Add(d);
                    }
            }
            return parent;
        }

        //Return the number of time step before mother get multiple childs
        public int getPastLife(int t)
        {
            if (Mothers == null)
                return t;
            if (Mothers.Count == 1 && Mothers[0].Daughters.Count == 1)
            { //Une seul Mere et une seul fille
                return Mothers[0].getPastLife(t + 1);
            }
            return t;
        }

        //Return the number of time step before division
        public int getFuturLife(int t)
        {
            if (Daughters == null)
                return t;

            if (Daughters.Count == 1 && Daughters[0].Mothers.Count == 1)
            { //Une seul Mere et une seul fille
                return Daughters[0].getFuturLife(t + 1);
            }

            return t;
        }

        public List<Cell> GetCellLife()
        {
            List<Cell> result = new List<Cell>();
            List<Cell> pastLife = GetCellLifeFromDivision();
            List<Cell> futurLife = GetCellLifUntilDivision();

            result.AddRange(pastLife);
            result.Add(this);
            result.AddRange(futurLife);

            MorphoDebug.LogError($"{pastLife.Count} -- {futurLife.Count}");

            return result;
        }

        private List<Cell> GetCellLifeFromDivision()
        {
            var pastLife = new List<Cell>();

            if (!IsFirstMother && !IsCreatedFromDivision)
            {
                Cell tempCell;
                pastLife.Add(Mothers[0]);
                tempCell = Mothers[0];

                while (!tempCell.IsFirstMother && !tempCell.IsCreatedFromDivision)
                {
                    if (!pastLife.Contains(tempCell.Mothers[0]))
                        pastLife.Add(tempCell.Mothers[0]);
                    tempCell = tempCell.Mothers[0];
                }
            }

            return pastLife;
        }

        private List<Cell> GetCellLifUntilDivision()
        {
            List<Cell> futurLife = new List<Cell>();
            if (!IsLastDaughter && !IsLastDaughterBeforeDivision)
            {
                Cell tempCell;
                futurLife.Add(Daughters[0]);
                tempCell = Daughters[0];

                while (!tempCell.IsLastDaughter && !tempCell.IsLastDaughterBeforeDivision)
                {
                    if (!futurLife.Contains(tempCell.Daughters[0]))
                        futurLife.Add(tempCell.Daughters[0]);
                    tempCell = tempCell.Daughters[0];
                }
            }
            return futurLife;
        }

        //Return all daighter and mother
        public List<Cell> getAllCellLife()
        {
            List<Cell> cells = new List<Cell>();
            List<Cell> pastCells = getAllCellLifePast();
            List<Cell> futurCells = getAllCellLifeFutur();

            if (pastCells != null)
                foreach (Cell pastCell in pastCells)
                    if (!cells.Contains(pastCell))
                        cells.Add(pastCell);

            if (futurCells != null)
                foreach (Cell futurCell in futurCells)
                    if (!cells.Contains(futurCell))
                        cells.Add(futurCell);

            if (!cells.Contains(this))
                cells.Add(this);

            return cells;
        }

        public List<Cell> getAllCellLifePast()
        {
            List<Cell> pastCells = new List<Cell>();

            if (Mothers != null)
            {
                foreach (Cell mother in Mothers)
                {
                    if (!pastCells.Contains(mother))
                        pastCells.Add(mother);

                    List<Cell> motherPastCells = mother.getAllCellLifePast();

                    if (motherPastCells != null)
                        foreach (Cell motherPastCell in motherPastCells)
                            if (!pastCells.Contains(motherPastCell))
                                pastCells.Add(motherPastCell);
                }
            }

            return pastCells;
        }

        public List<Cell> getAllCellLifeFutur()
        {
            List<Cell> futurCells = new List<Cell>();

            if (Daughters != null)
            {
                foreach (Cell daughter in Daughters)
                {
                    if (!futurCells.Contains(daughter))
                        futurCells.Add(daughter);

                    List<Cell> daughterFuturCells = daughter.getAllCellLifeFutur();

                    if (daughterFuturCells != null)
                        foreach (Cell daughterFuturCell in daughterFuturCells)
                            if (!futurCells.Contains(daughterFuturCell))
                                futurCells.Add(daughterFuturCell);
                }
            }

            return futurCells;
        }

        //Propagate the selection to mothers
        public void PropagateMother(int selectionId, Cell source)
        {
            if (Mothers != null)
            {
                foreach (Cell mother in Mothers)
                {
                    if (InterfaceManager.instance.isUntilDivision(source, mother, false)
                        || InterfaceManager.instance.isInCustomTimeRange(mother.t)
                        || !InterfaceManager.instance.isTimeModificationActive())
                    {
                        if (mother.selection == null || (mother.selection != null && !mother.selection.Contains(selectionId)))
                        {
                            {
                                mother.addSelection(selectionId, false);
                            }
                        }
                    }
                    mother.PropagateMother(selectionId, source);
                }
            }
        }

        //Propagate the selection to all daughters
        public void PropagateDaughters(int s, Cell source)
        {
            if (Daughters != null)
                foreach (Cell c in Daughters)
                {
                    if (InterfaceManager.instance.isUntilDivision(source, c, true)
                        || InterfaceManager.instance.isInCustomTimeRange(c.t)
                        || !InterfaceManager.instance.isTimeModificationActive())
                    {
                        string method = InterfaceManager.instance.GetTimeMethod();
                        if (c.selection == null || (c.selection != null && !c.selection.Contains(s)))
                        {
                            c.addSelection(s, false);
                        }
                    }
                    c.PropagateDaughters(s, source);
                }
        }

        public int getLifeLength()
        {
            return 1 + lifePast + lifeFutur;
        }

        public string Describe()
        {
            string Description = "";

            //Mother
            if (Mothers != null && Mothers.Count > 0)
                foreach (Cell m in Mothers)
                    Description += "Mother: " + m.ID + "(" + lifePast + ") \n";
            else
                Description += "No Mother " + "(" + lifePast + ") \n";

            //Daughters
            if (Daughters != null || Daughters.Count == 0)
                Description += "No Daughters " + "\n";
            else
            {
                Description += "Daughers: ";
                foreach (Cell d in Daughters)
                    Description += d.ID + ";";
                Description += "(" + lifeFutur + ") \n";
            }
            Description += "\n";
            return Description;
        }

        public string DescribeNeigbohrs()
        {
            if (Neigbhors == null || Neigbhors.Count == 0)
                return "";
            string Description = "";
            //Cell Neigbhors
            Description += "Neighbors:\n";
            if (Neigbhors.Count > 0)
            {
                foreach (Cell n in Neigbhors)
                {
                    if (n != null)
                        Description += n.ID + "\n";
                }
                Description += "\n";
            }
            Description += "\n";

            return Description;
        }
    }
}