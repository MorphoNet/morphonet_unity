using MorphoNet;
using MorphoNet.Core;
using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class DataSet : MonoBehaviour
{
    public Vector3 rDirection;
    public Dictionary<int, List<MeshType>> mesh_list; //and people_id != 0List of Mesh To Donwload
    public int id_dataset; //id of dataset -1 if is public
    public int isBundle;//is the embryo data set exist in assetbunde ?
    public string bundleType;
    public int MinTime; //Starting point
    public int MaxTime; //Last Time point
    public string embryo_name; //string contenant le nom de l'embryon courant;
    public Text datasetname; //Just to show the embryo name
    public int Quality = 0;//Value of quality of the mesh (stored in the DB)
    public int id_owner; //ID Propriétaire du dataset
    public int id_type;
    public GameObject embryo_container; //Main Embryo Game Object
    public Vector3 backupPosEmbryo;
    public SelectionManager selection_manager;
    public bool allow_time_download = true; //if on , the time will load in DataLoader, otherwise it's stopped

    public Dictionary<int, GameObject> mesh_by_time = new Dictionary<int, GameObject>(); //List of Embryons, tous les embryons MQ sont préchargés

    public Dictionary<int, List<Cell>> CellsByTimePoint = new Dictionary<int, List<Cell>>(); //List of Cells for each Embryons

    public List<TimePoint> TimePoints { get; private set; } = new List<TimePoint>();

    public TimePoint GetTimepointAt(int time)
    {
        return TimePoints.Where(t => t.Number == time).First();
    }

    //Gestion du temps
    public int CurrentTime; // on le commence à 1

    public int LoadingTime;  //Temps à chargé en MQ
    public int PreviousTime;

    //Gestion du temps chargé
    public bool range_time_loading = true;

    public List<int> time_to_reload;
    public int min_loading_time = -1;
    public int max_loading_time = -1;
    public int range_size = 50; // Multiply by 2 (before and afer)
    public int range_cache = 20;
    public int range_time_cache = -1;

    //Developmettal ...
    public int dt; //Difference between 2 stacks in seconds

    public int spf;//Hours post fertilization in seconds
    public Text hpf;
    public string start_stage;
    public string end_stage;

    //For Multiple Link Bundle Per Download
    public string[] BundleLinks;

    public int nbCurentLink = 0;

    public string alternativ_server_adress = "";
    public string alternativ_server_path = "";

    //FOR VECTOR FIELD
    public Vector3 embryoCenter = Vector3.zero;

    //For Channel CELL VISDUALISATION
    public Dictionary<string, Channel> Channels = new Dictionary<string, Channel>(); //List of all channels

    public string currentChannel;
    public string backupCurrentChannel;
    public int timeChangeShader = 10; //Number of update before changing shader for multiple material

    //When we search a cell
    public bool cellsearched;

    //Cut
    public bool realcut;

    public GameObject goShowString;//To Show String Information

    public bool update; //When update from somewhere else

    //Sprite parameters to avoid Resources.Load calls
    public Sprite network;

    public Sprite lineageTreeWhite;
    public Sprite eyeClose;

    //Cube for 3D Image Rendering
    public GameObject cube3DImage;

    public DataSetInformations Infos;
    public GroupManager tissue;
    public DevelopmentTable DevTable;
    public Recording Recorder;
    public GeneticManager GeneticManager;
    public MenuCurrated Curations;
    public Mutation Mutations;
    public PickedManager PickedManager;
    public TransformationsManager TransformationsManager;
    public FigureManager FigureManager;
    public RawImages RawImages;
    public ScatterView Scatter;
    private Coroutine describe_coroutine = null;

    public Dictionary<int, Dictionary<string, int>> multiSelectionIndexes = new Dictionary<int, Dictionary<string, int>>();

    public Bounds DatasetBounds = new Bounds(Vector3.zero, Vector3.zero);

    #region CHANNEL_REGION

    public Channel createChannel(string ch)
    {
        if (!Channels.ContainsKey(ch))
        { //Only if it was not previously created
            Channels[ch] = new Channel(ch, this);
        }

        if ((currentChannel == "" || currentChannel == null) && (ch != "" && ch != null))
        {
            currentChannel = ch;
        }

        if (Channels.Count > 1)
        {
            InterfaceManager.instance.NextChannelArrow.SetActive(true);
            InterfaceManager.instance.PrevChannelArrow.SetActive(true);
            InterfaceManager.instance.buttonTransferChannel.SetActive(true);
        }

        RefreshChannelDisplay();
        return Channels[ch];
    }

    //Return the channel from a tuple
    public string getChannelString(string cellstr)
    {
        string[] cellstrs = cellstr.Split(',');

        if (cellstrs.Length != 3)
            return "";

        if (!int.TryParse(cellstrs[0].Trim(), out _))
            return "";

        string ch = cellstrs[2].Trim();

        return ch;
    }

    #endregion CHANNEL_REGION

    #region CELL_REGION

    public bool TryParseCellName(string name, out string id, out int timestamp)
    {
        string[] splitedName = name.Split(',');
        id = default;
        timestamp = default;

        switch (splitedName.Length)
        {
            case 1: //Just name
                timestamp = MinTime;
                id = splitedName[0].Trim();
                break;

            case 2: //Time + Name
                if (!int.TryParse(splitedName[0].Trim(), out timestamp))
                    return false;
                id = splitedName[1].Trim();
                break;

            case 3: //Time,Name,Channel
                    //modified for multi-channel
                if (!int.TryParse(splitedName[0].Trim(), out timestamp))
                    return false;
                id = splitedName[1].Trim();

                break;

            default:
                return false;
        }
        return true;
    }

    public Cell getCell(string cellstr, bool create)
    {
        bool cellNameIsValid = TryParseCellName(cellstr, out string id, out int t);

        if (!cellNameIsValid)
            return null;

        Cell c = getCell(t, id);

        if (c == null && create)
            if ((t >= MinTime && t <= MaxTime) || LoadParameters.instance.id_dataset == 0)
            {
                c = createCell(t, id);
            }
        return c;
    }

    public Cell createCell(int t, string id)
    {
        if (t >= MinTime && t <= MaxTime)
        {
            Cell c = new Cell(t, id);
            if (!CellsByTimePoint.ContainsKey(t))
                CellsByTimePoint.Add(t, new List<Cell>());
            CellsByTimePoint[t].Add(c);
            return c;
        }

        return null;
    }

    public Cell getCell(int t, string id)
    {
        if (t >= MinTime && t <= MaxTime && CellsByTimePoint.ContainsKey(t))
        {
            foreach (var cell in CellsByTimePoint[t])
            {
                if (cell.ID == id)
                    return cell;
            }
        }

        return null;
    }

    public Cell getCell(GameObject go, bool create)
    {
        if (go != null)
            return getCell(go.name, create);

        return null;
    }

    #endregion CELL_REGION

    public bool lineage_created = false;

    public GameObject InfoConnectionParent;

    #region SPACE_REGION

    //Create on the menu embryo the space or time icon to show or download infos
    public void createSpaceTime(int id_infos, string datatype, int type)
    {
        // TODO: The operation on the SubMenu Layout should be transfered to the DataSetMenu class.
        Transform dataSetSubMenuLayout = InterfaceManager.instance.MenuDataSet.SubMenuLayout.transform;

        if (dataSetSubMenuLayout.Find(datatype + "_" + id_infos) == null)
        {
            GameObject downloadGameObject = dataSetSubMenuLayout.Find(datatype == "time" ? "download_lineage" : "download").gameObject;
            GameObject downloadGameObjectCopy = Instantiate(downloadGameObject);

            downloadGameObjectCopy.name = $"{datatype}_{id_infos}";
            downloadGameObjectCopy.transform.SetParent(dataSetSubMenuLayout);
            downloadGameObjectCopy.transform.SetSiblingIndex(downloadGameObjectCopy.transform.parent.childCount - 2);
            downloadGameObjectCopy.transform.localScale = new Vector3(1f, 1f, 1f);
            downloadGameObjectCopy.transform.localPosition = new Vector3(downloadGameObject.transform.localPosition.x, downloadGameObject.transform.localPosition.y, downloadGameObject.transform.localPosition.z);

            downloadGameObject.transform.localPosition = new Vector3(downloadGameObject.transform.localPosition.x, downloadGameObject.transform.localPosition.y - 40, downloadGameObject.transform.localPosition.z);

            if (datatype != "time")
                downloadGameObjectCopy.transform.Find("Text").GetComponent<Text>().text = "Spatial Connections ";

            if (datatype == "time")
            {
#if UNITY_WEBGL
                downloadGameObjectCopy.transform.SetParent(InterfaceManager.instance.bouton_layout_group.transform);
                InterfaceManager.instance.bouton_layout_group.transform.Find("FakeLineage").gameObject.SetActive(false);
                downloadGameObjectCopy.transform.SetAsFirstSibling();
                InterfaceManager.instance.lineage_button = downloadGameObjectCopy;
                downloadGameObjectCopy.transform.Find("show").GetComponent<Button>().onClick.AddListener(() => InterfaceManager.instance.lineage.showLineage(downloadGameObjectCopy, id_infos));
                downloadGameObjectCopy.transform.Find("show").GetComponent<Image>().sprite = lineageTreeWhite;
#else
                InterfaceManager.instance.containerLineage.SetActive(true);

                if (RightManager.Instance.canAccessLineage)
                    InterfaceManager.instance.buttonLineage.SetActive(true);

#endif
                // Find sisters in menuPicked !!!
                if (MinTime != MaxTime)
                {
                    InterfaceManager.instance.MenuClicked.transform.Find("Bouton layout").Find("Sisters").gameObject.SetActive(true);
                    //InterfaceManager.instance.MenuClicked.transform.Find("Bouton layout").Find("RandomSelected").Find("Separator").gameObject.SetActive(true);
                    InterfaceManager.instance.MenuClicked.transform.Find("Background").GetComponent<Image>().fillAmount += 0.099f;
                }
            }

            if (datatype == "space")
            {
                downloadGameObjectCopy.transform.Find("show").GetComponent<Button>().onClick.AddListener(() => showSpaceConnection(downloadGameObjectCopy, InterfaceManager.instance.initCanvasDatasetScale));
                downloadGameObjectCopy.transform.Find("show").GetComponent<Image>().sprite = network;
                downloadGameObjectCopy.transform.Find("show").gameObject.transform.Find("comment").gameObject.transform.Find("Text").GetComponent<Text>().text = "View objects neighbour relations";
            }

#if UNITY_WEBGL
            downloadGameObjectCopy.SetActive(true);
#endif

            if (LoadParameters.instance.id_dataset == 0 && datatype == "time" && lineage_created == true)
                Destroy(downloadGameObjectCopy);

            if (LoadParameters.instance.id_dataset == 0 && datatype == "time")
                lineage_created = true;
        }
    }

    private void showSpaceConnection(GameObject st, float initialCanvasScale)
    {
        if (isSpaceConnection)
        { //DESACTIVATE
            for (int t = MinTime; t <= MaxTime; t++)
            {
                if (MeshAtTimestampExists(t))
                {
                    if (mesh_by_time[t].transform.Find("Space") != null)
                        Destroy(mesh_by_time[t].transform.Find("Space").gameObject);
                }
            }
        }
        else
        { //ACTIVATE
            plotSpaceConnection(initialCanvasScale);
        }

        isSpaceConnection = !isSpaceConnection;

        if (!isSpaceConnection)
            st.transform.Find("show").GetComponent<Image>().sprite = network;
        else
            st.transform.Find("show").GetComponent<Image>().sprite = eyeClose;
    }

    private void plotSpaceConnection(float initialCanvasScale)
    {
        if (mesh_by_time[CurrentTime].transform.Find("Space") == null)
        {
            TransformationsManager.resetDataSetRotation();

            GameObject nv = new GameObject();
            nv.name = "Space";

            foreach (Cell cell in CellsByTimePoint[CurrentTime])
            {
                CellChannelRenderer ch = cell.getFirstChannel();

                if (ch != null && cell.Neigbhors != null && cell.Neigbhors.Count > 0)
                {
                    GameObject nvd = new GameObject();
                    nvd.name = cell.ID;
                    var nvdLineRenderer = nvd.AddComponent<LineRenderer>();
                    nvdLineRenderer.useWorldSpace = false;
                    nvdLineRenderer.startWidth = 0.1f;
                    nvdLineRenderer.endWidth = 1f;
                    nvdLineRenderer.positionCount = cell.Neigbhors.Count * 2;

                    int i = 0;
                    Vector3 grv = new Vector3((ch.Gravity[0] + embryoCenter.x), (ch.Gravity[1] - embryoCenter.y), (ch.Gravity[2] - embryoCenter.z));

                    foreach (Cell n in cell.Neigbhors)
                    {
                        nvdLineRenderer.SetPosition(i, grv * initialCanvasScale);

                        if (n.getFirstChannel() != null)
                        {
                            CellChannelRenderer chn = n.getFirstChannel();
                            if (chn.Gravity != null)
                            {
                                Vector3 grvN = new Vector3((chn.Gravity[0] + embryoCenter.x), (chn.Gravity[1] - embryoCenter.y), (chn.Gravity[2] - embryoCenter.z));
                                nvdLineRenderer.SetPosition(i + 1, grvN * initialCanvasScale);
                            }
                        }
                        else
                        {
                            nvdLineRenderer.SetPosition(i + 1, grv * initialCanvasScale);
                        }

                        i += 2;
                    }
                    // Instantiation.Default;
                    if (ch.cell.selected)
                    {
                        nvdLineRenderer.sharedMaterial = SetsManager.instance.Selected;
                    }
                    else if (ch.cell.UploadedColor)
                    {
                        nvdLineRenderer.material.color = ch.cell.UploadColor;
                    }
                    else if (ch.cell.selection != null && ch.cell.selection.Count > 0)
                    {
                        nvdLineRenderer.sharedMaterial = SelectionManager.getSelectedMaterial(ch.cell.selection[0]);
                    }
                    else
                    {
                        nvdLineRenderer.sharedMaterial = SetsManager.instance.Default;
                    }

                    nvd.transform.SetParent(nv.transform);
                }
            }

            nv.transform.SetParent(mesh_by_time[CurrentTime].transform);
            TransformationsManager.updateDataSetRotation();
        }
    }

    public bool isSpaceConnection = false;

    #endregion SPACE_REGION

    //Select cells at the current time inside a specific territory
    public void update_Fate(string fate)
    {
        if (fate.Trim() != "")
        {
            //Selelction Fate Map
            int idxInfoxFate = Infos.getInfosIndex("Fate");
            if (idxInfoxFate != -1)
            {
                foreach (Cell cell in CellsByTimePoint[CurrentTime])
                {
                    string cn = cell.getInfos(idxInfoxFate);
                    if (cn.Contains(fate))
                        MorphoTools.GetPickedManager().AddCellSelected(cell);
                }
#if !UNITY_WEBGL
                MorphoTools.SendCellsPickedToLineage();
#endif
            }
        }
    }

    //Select cells at the current time inside a specific territory
    public void update_Territory(string territory, string stage)
    {
        if (territory.Trim() != "")
        {
            StartCoroutine(DevTable.downloadCellFromTerritory(territory, stage));
        }
    }

    // Start is called before the first frame update
    public void init()
    {
        datasetname = InterfaceManager.instance.datasetname;
        hpf = InterfaceManager.instance.hpf;


        id_type = LoadParameters.instance.id_type;
        id_owner = LoadParameters.instance.id_owner;
        isBundle = LoadParameters.instance.is_bundle;
        embryo_name = LoadParameters.instance.embryo_name;
        id_dataset = LoadParameters.instance.id_dataset;
        realcut = false;
        cellsearched = false;
        CurrentTime = 1;
        LoadingTime = 1;
        PreviousTime = -1;
        bundleType = "WebGL";
#if UNITY_IOS
        bundleType = "iOS";
#elif UNITY_STANDALONE_WIN
        bundleType = "Windows";
#elif UNITY_STANDALONE_OSX
        bundleType = "OSX";
#elif UNITY_ANDROID
        bundleType = "Android";
#elif UNITY_STANDALONE_LINUX
        bundleType = "Linux";
#endif
        Quality = LoadParameters.instance.quality;
        if (Quality == -1)
            Quality = 0;

        //Create Embryos
        embryo_container = gameObject;
        MaxTime = LoadParameters.instance.max_time;
        MinTime = LoadParameters.instance.min_time;

        for (int t = MinTime; t <= MaxTime; t++)
            mesh_by_time[t] = null;

        //Create Cells
        for (int t = MinTime; t <= MaxTime; t++)
            CellsByTimePoint[t] = new List<Cell>();

        

        InterfaceManager.instance.UpdateCustomTimePoints();
        InterfaceManager.instance.updateLoadingRangeBoundaries();

        CurrentTime = MinTime;
        LoadingTime = MinTime;



        InterfaceManager.instance.initializeTime(LoadParameters.instance.min_time, LoadParameters.instance.max_time);

        //Initialize Show string on element
        goShowString = GameObject.Find("ShowString");
        goShowString.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        var showStringText = goShowString.AddComponent<TextMesh>();
        showStringText.text = "";
        showStringText.font = SetsManager.instance.mainFont;

        goShowString.GetComponent<Renderer>().material = SetsManager.instance.Text3D;
        embryo_name = LoadParameters.instance.embryo_name;

        spf = LoadParameters.instance.spf;
        dt = LoadParameters.instance.dt;

        //REMOVE HPF IF DT IS NOT PRECISE
        if (spf == -1 || dt <= 0)
            hpf.gameObject.SetActive(false);
        else
            hpf.text = MorphoTools.ConvertTimeToHPF(0);

        TransformationsManager = embryo_container.AddComponent<TransformationsManager>();
        TransformationsManager.Init(this);
        SetsManager.instance.OnMeshDownloaded += UpdateCurrentDisplayTime;
        update = false;

        alternativ_server_adress = LoadParameters.instance.server_adress;
        alternativ_server_path = LoadParameters.instance.server_path;

        cube3DImage = SetsManager.instance.cube3DImage;

        if (embryo_name.Contains("("))
        {
            string[] enames = embryo_name.Split('(');
            datasetname.text = enames[0];
        }
        else
        {
            datasetname.text = embryo_name;
        }

        if (LoadParameters.instance.liveMode != true)
        {
            if (spf > 0 && dt  > 0){ // Only activate Genetic system if we have a correct spf and dt
                GeneticManager = embryo_container.AddComponent<GeneticManager>();
                GeneticManager.AttachDataset(this);
            }
            else 
                InterfaceManager.instance.MenuGenetic.SetActive(false);

            Infos = embryo_container.AddComponent<DataSetInformations>();
            Infos.AttachDataset(this);

            tissue = embryo_container.AddComponent<GroupManager>();
            tissue.AttachDataset(this);

            DevTable = embryo_container.AddComponent<DevelopmentTable>();
            DevTable.AttachDataSet(this);

            Recorder = embryo_container.AddComponent<Recording>();
            Recorder.AttachDataset(this);

            Curations = embryo_container.AddComponent<MenuCurrated>();
            Curations.AttachDataset(this);

            Mutations = embryo_container.AddComponent<Mutation>();
            Mutations.AttachDataset(this);

            RawImages = embryo_container.AddComponent<RawImages>();
            RawImages.AttachDataset(this);
        }

        Scatter = embryo_container.AddComponent<ScatterView>();
        Scatter.AttachDataSet(this);

        FigureManager = embryo_container.AddComponent<FigureManager>();
        FigureManager.AttachDataset(this);

        PickedManager = embryo_container.AddComponent<PickedManager>();
        PickedManager.AttachDataset(this);
    }

    public void startShot()
    {
        Recorder.startShot();
    }

    public void sendDelay(string value)
    {
        Recorder.StoreDelay(value);
    }

    public void sendNbShots(string value)
    {
        Recorder.StoreNbShots(value);
    }

    public void sendFramerate(string value)
    {
        Recorder.StoreFramerate(value);
    }

    public bool TimestampIsValid(int timestamp)
        => timestamp >= MinTime && timestamp <= MaxTime;

    public bool MeshAtTimestampExists(int timestamp)
        => TimestampIsValid(timestamp) && mesh_by_time.ContainsKey(timestamp) && mesh_by_time[timestamp] != null;

    public bool TimePointExists(int timestamp)
        => TimestampIsValid(timestamp)
        && TimePoints.Where(ts => ts.Number == timestamp).FirstOrDefault() != default;

    public void deleteAt(int t)
    {
        if (mesh_by_time.ContainsKey(t) && mesh_by_time[t] != null)
        {
            DestroyImmediate(mesh_by_time[t], true);
            mesh_by_time[t] = null;
        }

        if (mesh_by_time.ContainsKey(t))
            mesh_by_time.Remove(t);

        if (CellsByTimePoint.ContainsKey(t))
        {
            CellsByTimePoint.Remove(t);
            CellsByTimePoint[t] = new List<Cell>();
        }

        if (CurrentTime == t)
        {
            PreviousTime = -1;

            if (t - 1 >= MinTime)
                InterfaceManager.instance.setTime(t - 1);
            else
                InterfaceManager.instance.setTime(t + 1);
        }

        TimePoints.RemoveAll(tsb => tsb.Number == t);

        Resources.UnloadUnusedAssets();
        System.GC.Collect();
    }

    public void deleteAt(int t, int c)
    {
        if (mesh_by_time.ContainsKey(t) && mesh_by_time[t] != null)
        {
            foreach(Transform child in mesh_by_time[t].transform)
            {
                if (child.name.EndsWith(c.ToString()))
                {
                    Destroy(child.gameObject);
                }
            }
        }

        //new
        foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
        {

            if (cell != null && cell.getChannel(c.ToString()) != null && (cell.getName().Split(',').Length > 2 && cell.getName().EndsWith(c.ToString())))
            {
                cell.Destroy();
            }

        }

        //old
        /*if (mesh_by_time.ContainsKey(t) && mesh_by_time[t] != null)
        {
            DestroyImmediate(mesh_by_time[t], true);
            mesh_by_time[t] = null;
        }

        if (mesh_by_time.ContainsKey(t))
            mesh_by_time.Remove(t);

        if (CellsByTimePoint.ContainsKey(t))
        {
            CellsByTimePoint.Remove(t);
            CellsByTimePoint[t] = new List<Cell>();
        }

        if (CurrentTime == t)
        {
            PreviousTime = -1;

            if (t - 1 >= MinTime)
                InterfaceManager.instance.setTime(t - 1);
            else
                InterfaceManager.instance.setTime(t + 1);
        }

        TimePoints.RemoveAll(tsb => tsb.Number == t);
        */
        Resources.UnloadUnusedAssets();
        System.GC.Collect();
    }

    public IEnumerator describeInfosWithDelay()
    {
        yield return new WaitForSeconds(0.35f);
        if (Infos != null)
            Infos.describe();
        describe_coroutine = null;
    }

    public void ShowOrHideCurrentChannel()
    {
        Channel channel = Channels[currentChannel];
        channel.Visible = !channel.Visible;
        channel.showChannel();
        InterfaceManager.instance.MenuChannel.transform.Find("Show Hide").GetChild(0).GetChild(0).GetComponent<Text>().text = Channels[currentChannel].Visible == true ? "Hide" : "Show";
        UpdateAllShaders();
        InterfaceManager.instance.MenuImages.RawDataUpdate();
    }

    public void TransferPicked()
    {
        string to_transfer_channel = "";
        IEnumerable<KeyValuePair<string, Channel>> query = Channels.SkipWhile(kvp => kvp.Key != currentChannel);
        query = query.Skip(1);

        if (query.Count() > 0)
        {
            //Found a next one
            to_transfer_channel = query.First().Key;
        }
        else
        {
            //Go back to first
            to_transfer_channel = Channels.First().Key;
        }

        if (to_transfer_channel == null || to_transfer_channel == "")
            return;

        List<Cell> cellsToAdd = new List<Cell>();

        foreach (Cell c in MorphoTools.GetPickedManager().clickedCells)
        {
            if (c.getChannel(currentChannel) != null)
            {
                Cell cellToAdd = CellsByTimePoint[c.t].Where(cell => cell.ID == c.ID).First();
                if (cellToAdd != null && cellToAdd.getChannel(currentChannel) != null)
                {
                    cellsToAdd.Add(cellToAdd);
                }
            }
        }

        MorphoTools.GetPickedManager().ClearSelectedCells();
        MorphoTools.GetPickedManager().ClearClickedCell();

        foreach (Cell cell in cellsToAdd)
            MorphoTools.GetPickedManager().AddCellSelected(cell);
#if !UNITY_WEBGL
        MorphoTools.SendCellsPickedToLineage();
#endif
    }

    public void NextChannel()
    {
        string backup_current = currentChannel;
        IEnumerable<KeyValuePair<string, Channel>> query = Channels.SkipWhile(kvp => kvp.Key != currentChannel);

        query = query.Skip(1);

        if (query.Count() > 0)
        {
            //Found a next one
            currentChannel = query.First().Key;
        }
        else
        {
            //Go back to first
            //Go back to first
            currentChannel = Channels.First().Key;
        }

        if (currentChannel != backupCurrentChannel && currentChannel != null && currentChannel != "")
        {
            backupCurrentChannel = currentChannel;
            InterfaceManager.instance.MenuChannel.transform.Find("ChannelName").GetComponent<Text>().text = "Channel : " + currentChannel;
            InterfaceManager.instance.MenuChannel.transform.Find("Show Hide").GetChild(0).GetChild(0).GetComponent<Text>().text = Channels[currentChannel].Visible == true ? "Hide" : "Show";
        }
    }

    public void PreviousChannel()
    {
        string backup_current = currentChannel;
        IEnumerable<KeyValuePair<string, Channel>> query = Channels.TakeWhile(kvp => kvp.Key != currentChannel);

        if (query.Count() > 0)
        {
            //Found a next one
            currentChannel = query.Last().Key;
        }
        else
        {
            //Go back to first
            currentChannel = Channels.Last().Key;
        }

        if (currentChannel != backupCurrentChannel && currentChannel != null && currentChannel != "")
        {
            backupCurrentChannel = currentChannel;
            InterfaceManager.instance.MenuChannel.transform.Find("ChannelName").GetComponent<Text>().text = "Channel : " + currentChannel;
            InterfaceManager.instance.MenuChannel.transform.Find("Show Hide").GetChild(0).GetChild(0).GetComponent<Text>().text = Channels[currentChannel].Visible == true ? "Hide" : "Show";
        }
    }

    public void RefreshChannelDisplay()
    {
        if (currentChannel != null && currentChannel != "")
            InterfaceManager.instance.MenuChannel.transform.Find("ChannelName").GetComponent<Text>().text = "Channel : " + currentChannel;

        if (currentChannel != null && currentChannel != "")
            InterfaceManager.instance.MenuChannel.transform.Find("Show Hide").GetChild(0).GetChild(0).GetComponent<Text>().text = Channels[currentChannel].Visible == true ? "Hide" : "Show";
    }

#if UNITY_IOS || UNITY_ANDROID
    private float distanceTouch = -1;
#endif
    public bool is_reloading = false;

    public void UpdateLoadedMeshCache()
    {
        if (range_time_loading)
        {
            //windows time range
            if (MaxTime - MinTime < range_size) //RANGE SIZE if Bigger than all times points
            {
                min_loading_time = MinTime;
                min_loading_time = MaxTime;
            }
            else
            {
                if (range_time_cache < CurrentTime - range_cache || range_time_cache > CurrentTime + range_cache) //We do work on the change only if we move enough
                {
                    range_time_cache = CurrentTime;

                    min_loading_time = CurrentTime - range_size;
                    max_loading_time = CurrentTime + range_size;

                    if (min_loading_time < MinTime)
                    {
                        max_loading_time -= min_loading_time;
                        min_loading_time = MinTime;
                    }
                    if (max_loading_time > MaxTime)
                    {
                        min_loading_time -= max_loading_time - MaxTime;
                        max_loading_time = MaxTime;
                    }

                    if (time_to_reload == null)
                        time_to_reload = new List<int>();
                    else
                        time_to_reload.Clear();

                    //We Start by Adding the Current time ...
                    if (!time_to_reload.Contains(CurrentTime) && mesh_by_time[CurrentTime] == null)
                        time_to_reload.Add(CurrentTime);

                    bool DestroyedEmbryo = false;
                    for (int i = MinTime; i <= MaxTime; i++)
                    {
                        if (i < min_loading_time || i > max_loading_time)
                        {
                            if (mesh_by_time[i] != null)
                            {
                                DestroyImmediate(mesh_by_time[i]);
                                mesh_by_time[i] = null;
                                DestroyedEmbryo = true;
                            }
                        }
                        else if (mesh_by_time[i] == null)
                        {
                            if (!time_to_reload.Contains(i))
                                time_to_reload.Add(i);
                        }
                    }
                    //DESTROY
                    if (DestroyedEmbryo)
                    {
                        Resources.UnloadUnusedAssets();
                        System.GC.Collect();
                    }

                    //RELOAD
                    if (SetsManager.MeshDownloaded && !is_reloading)
                        is_reloading = true;
                    SetsManager.instance.ReLoadMesh();
                    ProgressBar.addReloadMesh();
                }
            }
        }
    }

    public void forceUpdate()
    {
        Update();
    }

    // Update is called once per frame
    private void Update()
    {
#if UNITY_EDITOR
        /*if (Input.GetKeyDown(KeyCode.H))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("sceneEmbryo");
        }*/
#endif
        if (TimestampIsValid(CurrentTime)
            && !LoadParameters.instance.liveMode
            && CurrentTime != PreviousTime)
        {
            UpdateCurrentDisplayTime();
        }

        if (update) //AFTER TIME CHANGEMENT
        {
            TransformationsManager.ApplyTransformationsOnTimeChange();
            update = false;
        }
        else
        {
            TransformationsManager.ComputeUserInputs();
        }
    }

    public DataSetDisplayStrategy DataSetDisplayStrategy { get; set; }

    private void UpdateCurrentDisplayTime()
    {
        int currentTimeStep = CurrentTime;
        //Time step changes...
        InteractionManager.instance.UpdateTimeDisplay();



        if (FigureManager != null)
        {
            FigureManager.ManageTimeChange(currentTimeStep, PreviousTime);
        }

        if (selection_manager == null)
        {
            selection_manager = InterfaceManager.instance.MenuCells.transform.Find("Selection").gameObject.GetComponent<SelectionManager>();
            selection_manager.GenerateMultiLabelPanel();
        }

        if (InterfaceManager.instance.MenuCells.activeSelf)
            selection_manager.OnEnabled(); //Active Cells number if selection if open

        if (!cellsearched)
            MorphoTools.GetPickedManager().FollowSelectedCells(currentTimeStep); //We follow in time the selected cells

        if (isSpaceConnection)
            plotSpaceConnection(InterfaceManager.instance.initCanvasDatasetScale);

        if (describe_coroutine == null)
            describe_coroutine = StartCoroutine(describeInfosWithDelay());

        cellsearched = false;
        DisplayDataSet(currentTimeStep);

        //Raw Images
        if (RawImages.RawImagesAreVisible)
            RawImages.changeTimeStep();

        if (MorphoTools.GetScatter().scatterReady)
        {
            MorphoTools.GetScatter().UpdateView(currentTimeStep);
            MorphoTools.GetScatter().UpdateSliderToTime(currentTimeStep);
        }

        UpdateLoadedMeshCache();
    }

    public void DisplayDataSet(int currentTimeStep)
    {
                
        if (!MeshAtTimestampExists(currentTimeStep))
        {
            PreviousTime = currentTimeStep; //maj previoustime
            return;
        }

        //ACTIVE PRIMITIVE TRANSFERT
        if (SetsManager.instance.isTimePrimitive != null && SetsManager.instance.isTimePrimitive[currentTimeStep])
            SetsManager.instance.ActivePrimitives(currentTimeStep, PreviousTime);

        ApplyDisplayStrategy(DataSetDisplayStrategy, currentTimeStep);

        UpdateHpfText(currentTimeStep);

        foreach (KeyValuePair<string, Channel> item in Channels)
            item.Value.updateTrackingVectors(); //Update Tracking Visualisation (to update track length)

        if (Recorder != null && !Recorder.actionDone)
            Recorder.finishAction();   //It was the result of an action

        PreviousTime = currentTimeStep; //maj previoustime
    }

    public void SetLayerRecursively(GameObject obj, int newLayer)
    {
        obj.layer = newLayer;

        foreach (Transform child in obj.transform)
        {
            SetLayerRecursively(child.gameObject, newLayer);
        }
    }

    private void ApplyDisplayStrategy(DataSetDisplayStrategy dataSetDisplayStrategy, int currentTimeStep)
    {
        dataSetDisplayStrategy.ChangeTimestamp(currentTimeStep);
        TransformationsManager.updateTransformationsOnTimeChange(dataSetDisplayStrategy);

        if ((SetsManager.MeshDownloaded || currentTimeStep == MaxTime) && dataSetDisplayStrategy.MultiTimestamp)
        {
            foreach (var tp in TimePoints)
            {
                UpdateLayers(dataSetDisplayStrategy, tp, currentTimeStep);

                if (dataSetDisplayStrategy.IsTimestampVisible(tp.Number) && TimestampIsValid(tp.Number))
                    ShowTimePoint(tp.Number);
                else
                    HideTimePoint(tp.Number);
            }
        }
        else
        {
            // Hide previous
            foreach( var tp in TimePoints)
            {
                if (MeshAtTimestampExists(tp.Number))
                    HideTimePoint(tp.Number);
            }
                

            // Activate time point
            ShowTimePoint(currentTimeStep);
        }
    }

    public void UpdateLayers(DataSetDisplayStrategy strategy, TimePoint tp, int currentTimeStep)
    {
        if (strategy == SetsManager.instance._HelicoidalStrategyCache && tp.Number != currentTimeStep)
        {
            SetLayerRecursively(tp.Pivot.gameObject, 14);
        }
        else
        {
            SetLayerRecursively(tp.Pivot.gameObject, 0);
        }
    }

    public void ApplyDisplayStrategyAndUpdateAll()
    {
        DataSetDisplayStrategy.ChangeTimestamp(CurrentTime);
        TransformationsManager.updateTransformationsOnTimeChange(DataSetDisplayStrategy);

        foreach (var tp in TimePoints)
        {
            UpdateLayers(DataSetDisplayStrategy, tp, CurrentTime);

            if (DataSetDisplayStrategy.IsTimestampVisible(tp.Number) && TimestampIsValid(tp.Number))
                ShowTimePoint(tp.Number);
            else
                HideTimePoint(tp.Number);
        }

        if (!((SetsManager.MeshDownloaded || CurrentTime == MaxTime) && DataSetDisplayStrategy.MultiTimestamp))
        {
            ShowTimePoint(CurrentTime);
        }
    }
    private void ShowTimePoint(int ts)
    {
        if(mesh_by_time.ContainsKey(ts))
            if(mesh_by_time[ts]!=null)
                mesh_by_time[ts].SetActive(true);
        foreach (var tsb in TimePoints)
            if (tsb.Number == ts)
            {
                if (TransformationsManager.embryo_cropping)
                {
                    foreach (Cell cell in CellsByTimePoint[tsb.Number])
                    {
                        if (cell != null && cell.getFirstChannel() != null)
                        {
                            cell.getFirstChannel().ShouldBeUpdated = true;
                        }
                    }
                }
                tsb.Show();
            }
              
    }

    private void HideTimePoint(int ts)
    {
        if (mesh_by_time.ContainsKey(ts))
            if (mesh_by_time[ts] != null)
                mesh_by_time[ts].SetActive(false);
        foreach (var tsb in TimePoints)
            if (tsb.Number == ts)
                tsb.Hide();
    }

    private void UpdateHpfText(int currentTimeStep)
    {
        if (mesh_by_time[currentTimeStep].name.Substring(0, 1) != "_")
        {
            hpf.gameObject.SetActive(true);
            hpf.text = mesh_by_time[currentTimeStep].name;
        }
    }

    public void UpdateAllShaders()
    {
        if (CellsByTimePoint.ContainsKey(CurrentTime)){
            foreach (Cell cell in CellsByTimePoint[CurrentTime])
            {
                if (cell.Channels != null)
                    foreach (KeyValuePair<string, CellChannelRenderer> itemc in cell.Channels)
                        itemc.Value.ShouldBeUpdated = true;
            }
        }
    }
}