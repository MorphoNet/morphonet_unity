namespace MorphoNet
{
    public class MeshType
    {
        public string Link { get; }
        public uint Version { get; }
        public bool Obj { get; }
        public int Id { get; }
        public int T { get; }
        public string Name { get; }
        public string Channel { get; }

        public MeshType(string link, uint version, bool obj, int id, int t, string name, string channel = "")
        {
            Link = link;
            Version = version;
            Obj = obj;
            Id = id;
            T = t;
            Name = name;
            Channel = channel;

            if (Name.ToLower() == "null")
                Name = $"_DATASET_{T}";
        }
    }
}