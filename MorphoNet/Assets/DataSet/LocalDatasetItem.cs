using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LocalDatasetItem
{
    public string Name;
    public int MinTime;
    public int MaxTime;
    public int Background;
    //data
    public Dictionary<int, Dictionary<int, Image3D>> SegmentedData = new Dictionary<int, Dictionary<int, Image3D>>();
    public Dictionary<int, Dictionary<int, Image3D>> IntensityData = new Dictionary<int, Dictionary<int, Image3D>>();

    public string XMLFile;
    public string Date;
    public string FullPath;
    public int PortSend;
    public int PortRecieve;

    //advanced params
    public int DownScale;
    public int ZDownScale;
    public int RawDownScale;
    public int ZRawDownScale;
    //advanced vtk params
    public bool Smoothing;
    public float SmoothPassband;
    public int SmoothIterations;
    public bool QuadricClustering;
    public int QCDivisions;
    public bool Decimation;
    public float DecimateReduction;
    public int AutoDecimateThreshold;

    public LocalDatasetItem()
    {

    }

    public LocalDatasetItem(string name, Dictionary<int, Dictionary<int, Object3DImage>> seg_data, Dictionary<int, Dictionary<int, Object3DImage>> raw_data, int minTime, int maxTime, 
        int downscale, int background, string xmlfile = "", int portsend = 9875, int portreceive = 9876, string time = "", string fullPath = "", int zDownScale=-1,bool smoothing = true,
        float smoothPassband = 0.01f, int smoothIterations=1, bool quadricClustering = true, int qCDivisions = 1, bool decimation = true, float decimateReduction=0.8f, 
        int autoDecimateThreshold=30, int rawDownScale=4, int zRawDownScale=4)
    {
        Name = name;
        MinTime = minTime;
        MaxTime = maxTime;
        Background = background;

        /*SegmentedData = seg_data;
        IntensityData = raw_data;*/
        foreach(var t in seg_data)
        {
            SegmentedData.Add(t.Key, new Dictionary<int, Image3D>());
            foreach(var c in t.Value)
            {
                SegmentedData[t.Key].Add(c.Key, new Image3D(seg_data[t.Key][c.Key]));
            }
        }
        foreach (var t in raw_data)
        {
            IntensityData.Add(t.Key, new Dictionary<int, Image3D>());
            foreach (var c in t.Value)
            {
                IntensityData[t.Key].Add(c.Key, new Image3D(raw_data[t.Key][c.Key]));
            }
        }

        XMLFile = xmlfile;
        PortSend = portsend;
        PortRecieve = portreceive;
        if (time == null || time =="")
            Date = DateTime.Now.ToString();
        else
            Date = time;
        FullPath = fullPath;
        DownScale = downscale;
        if (zDownScale < 0)
            ZDownScale = downscale;
        else
            ZDownScale = zDownScale;
        Smoothing = smoothing;
        SmoothPassband = smoothPassband;
        SmoothIterations = smoothIterations;
        QuadricClustering = quadricClustering;
        QCDivisions = qCDivisions;
        Decimation = decimation;
        DecimateReduction = decimateReduction;
        AutoDecimateThreshold = autoDecimateThreshold;
        RawDownScale = rawDownScale;
        ZRawDownScale = zRawDownScale;
    }

    public void UpdateData(Dictionary<int, Dictionary<int, Object3DImage>> seg_data, Dictionary<int, Dictionary<int, Object3DImage>> raw_data)
    {
        SegmentedData.Clear();
        IntensityData.Clear();

        foreach (var t in seg_data)
        {
            SegmentedData.Add(t.Key, new Dictionary<int, Image3D>());
            foreach (var c in t.Value)
            {
                SegmentedData[t.Key].Add(c.Key, new Image3D(seg_data[t.Key][c.Key]));
            }
        }
        foreach (var t in raw_data)
        {
            IntensityData.Add(t.Key, new Dictionary<int, Image3D>());
            foreach (var c in t.Value)
            {
                IntensityData[t.Key].Add(c.Key, new Image3D(raw_data[t.Key][c.Key]));
            }
        }
    }

    public void SetAdvancedParametersDefaults()
    {
        ZDownScale = DownScale;
        Smoothing = true;
        SmoothPassband = 0.01f;
        SmoothIterations = 25;
        QuadricClustering = true;
        QCDivisions = 1;
        Decimation = true;
        DecimateReduction = 0.8f;
        AutoDecimateThreshold = 30;
        RawDownScale = 4;
        ZRawDownScale = 4;
    }

    public void SetRawScalingDefault()
    {
        RawDownScale = 4;
        ZRawDownScale = 4;
    }

    public override bool Equals(object obj) => Equals(obj as LocalDatasetItem);

    public bool Equals(LocalDatasetItem l)
    {
        if (l == null)
            return false;
        if (System.Object.ReferenceEquals(this, l))
        {
            return true;
        }
        try
        {
            return (FullPath == l.FullPath);
        }
        catch(Exception e)
        {
            return false;
        }
    }

    public void Update(LocalDatasetItem i)
    {
        Name = i.Name;
        MinTime = i.MinTime;
        MaxTime = i.MaxTime;
        SegmentedData = i.SegmentedData;
        IntensityData = i.IntensityData;

        Background = i.Background;
        XMLFile = i.XMLFile;
        PortSend = i.PortSend;
        PortRecieve = i.PortRecieve;
        DownScale = i.DownScale;
        ZDownScale = i.ZDownScale;
        Smoothing = i.Smoothing;
        SmoothPassband = i.SmoothPassband;
        SmoothIterations = i.SmoothIterations;
        QuadricClustering = i.QuadricClustering;
        QCDivisions = i.QCDivisions;
        Decimation = i.Decimation;
        DecimateReduction = i.DecimateReduction;
        AutoDecimateThreshold = i.AutoDecimateThreshold;
        RawDownScale = i.RawDownScale;
        ZRawDownScale = i.ZRawDownScale;
    }

}
