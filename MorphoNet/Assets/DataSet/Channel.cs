﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MorphoNet
{
    public class Channel
    {
        public string name; //Name of the channel
        public DataSet parent;
        public bool Visible { get; set; }

        public bool center; //Visualize or not the center
        public int centerSize;

        public bool tracking; //Visualize or not the trai,nc
        public float trackingSize;
        public int trackingLengthPath;
        public int trackingLengthFutur;
        public GameObject Tracking; //Main Tracking Object


        public Channel(string name, DataSet source)
        {
            parent = source;
            this.name = name;
            Visible = true;
            center = false;
            tracking = false;
            centerSize = 8;
            trackingSize = 0.03F;
            trackingLengthPath = 0;
            trackingLengthFutur = 0;

           
        }

        public void showChannel()
        {
            for (int t = parent.MinTime; t <= parent.MaxTime; t++)
            {
                if (parent.CellsByTimePoint.ContainsKey(t))
                {
                    foreach (Cell cell in parent.CellsByTimePoint[t])
                    {
                        if (cell.Channels != null && cell.Channels.ContainsKey(name))
                        {
                            CellChannelRenderer channel = cell.Channels[name];
                            if (channel != null)
                            {
                                channel.Activate(Visible);
                            }
                        }
                    }
                }
                
            }
        }

        public void showChannelMenu()
        {
            GameObject MenuChannel = InterfaceManager.instance.canvas.GetComponent<Menus>().MenuChannel;
            MenuChannel.transform.Find("back").GetComponent<Button>().onClick.RemoveAllListeners();
            MenuChannel.transform.Find("back").GetComponent<Button>().onClick.AddListener(() => showChannelMenu());
            if (MenuChannel.activeSelf)
                MenuChannel.SetActive(false);
            else
            {
                MenuChannel.transform.Find("ChannelName").GetComponent<Text>().text = "Channel " + name;
                //Center
                MenuChannel.transform.Find("centers").GetComponent<Toggle>().isOn = center;
                MenuChannel.transform.Find("centers").GetComponent<Toggle>().onValueChanged.RemoveAllListeners();
                MenuChannel.transform.Find("centers").GetComponent<Toggle>().onValueChanged.AddListener(delegate
                { showCenter(); });

                MenuChannel.transform.Find("centersSize").GetComponent<Slider>().value = centerSize;
                MenuChannel.transform.Find("centersSize").GetComponent<Slider>().onValueChanged.RemoveAllListeners();
                MenuChannel.transform.Find("centersSize").GetComponent<Slider>().onValueChanged.AddListener(delegate
                { changeCenterSize(); });

                //Tracking
                MenuChannel.transform.Find("Tracking").GetComponent<Toggle>().isOn = tracking;
                MenuChannel.transform.Find("Tracking").GetComponent<Toggle>().onValueChanged.RemoveAllListeners();
                MenuChannel.transform.Find("Tracking").GetComponent<Toggle>().onValueChanged.AddListener(delegate
                { showTracking(); });

                MenuChannel.transform.Find("TrackingSize").GetComponent<Slider>().value = trackingSize;
                MenuChannel.transform.Find("TrackingSize").GetComponent<Slider>().onValueChanged.RemoveAllListeners();
                MenuChannel.transform.Find("TrackingSize").GetComponent<Slider>().onValueChanged.AddListener(delegate
                { changeTrackingSize(); });

                MenuChannel.transform.Find("TrackingLengthPath").GetComponent<Slider>().maxValue = parent.MaxTime;
                MenuChannel.transform.Find("TrackingLengthPath").GetComponent<Slider>().value = trackingLengthPath;
                MenuChannel.transform.Find("TrackingLengthPath").GetComponent<Slider>().onValueChanged.RemoveAllListeners();
                MenuChannel.transform.Find("TrackingLengthPath").GetComponent<Slider>().onValueChanged.AddListener(delegate
                { changeTrackingLengthPath(); });

                MenuChannel.transform.Find("TrackingLengthFutur").GetComponent<Slider>().maxValue = parent.MaxTime;
                MenuChannel.transform.Find("TrackingLengthFutur").GetComponent<Slider>().value = trackingLengthFutur;
                MenuChannel.transform.Find("TrackingLengthFutur").GetComponent<Slider>().onValueChanged.RemoveAllListeners();
                MenuChannel.transform.Find("TrackingLengthFutur").GetComponent<Slider>().onValueChanged.AddListener(delegate
                { changeTrackingLengthFutur(); });

                MenuChannel.SetActive(true);
            }
        }

        //////////////////// CENTER
        //Active or Desactive Center Visualisation
        public void showCenter()
        {
            center = InterfaceManager.instance.canvas.GetComponent<Menus>().MenuChannel.transform.Find("centers").GetComponent<Toggle>().isOn;
            if (!center)
            { //Destroy Center
                for (int t = parent.MinTime; t <= parent.MaxTime; t++)
                {
                    foreach (Cell cell in parent.CellsByTimePoint[t])
                    {
                        if (cell.Channels.ContainsKey(name))
                            cell.Channels[name].destroyCenter();
                    }
                }
                System.GC.Collect();
            }
        }

        //Change Nuclei Size
        public void changeCenterSize()
        {
            centerSize = (int)InterfaceManager.instance.canvas.GetComponent<Menus>().MenuChannel.transform.Find("centersSize").GetComponent<Slider>().value;
            if (center)
            {
                for (int t = parent.MinTime; t <= parent.MaxTime; t++)
                {
                    List<Cell> CellAtT = parent.CellsByTimePoint[t];
                    foreach (Cell cell in CellAtT)
                    {
                        if (cell.Channels.ContainsKey(name))
                            cell.Channels[name].resizeCenter();
                    }
                }
            }
        }

        //////////////////// TRACKING
        public void showTracking()
        {
            tracking = InterfaceManager.instance.canvas.GetComponent<Menus>().MenuChannel.transform.Find("Tracking").GetComponent<Toggle>().isOn;

            if (!tracking)
            { //Destroy Tracking
                Object.DestroyImmediate(Tracking, true);
                System.GC.Collect();
            }
            else
            {
                if (Tracking == null)
                    createTracking();
            }
            //updateTrackingVectors ();
        }

        public void createTracking()
        {
            Tracking = new GameObject();
            Tracking.name = name;
            Tracking.transform.SetParent(parent.TransformationsManager.Tracking.transform);
            int lastT = (int)Mathf.Max(parent.MinTime, parent.CurrentTime - trackingLengthPath);
            int firsT = (int)Mathf.Min(parent.MaxTime, parent.CurrentTime + trackingLengthFutur);
            for (int t = lastT; t <= firsT; t++)
                createTrackingAt(t);
        }

        public void changeTrackingLengthPath()
        {
            trackingLengthPath = (int)InterfaceManager.instance.canvas.GetComponent<Menus>().MenuChannel.transform.Find("TrackingLengthPath").GetComponent<Slider>().value;
            updateTrackingVectors();
        }

        public void changeTrackingLengthFutur()
        {
            trackingLengthFutur = (int)InterfaceManager.instance.canvas.GetComponent<Menus>().MenuChannel.transform.Find("TrackingLengthFutur").GetComponent<Slider>().value;
            updateTrackingVectors();
        }

        public void changeTrackingSize()
        {
            trackingSize = InterfaceManager.instance.canvas.GetComponent<Menus>().MenuChannel.transform.Find("TrackingSize").GetComponent<Slider>().value;
            if (tracking && Tracking != null)
            {
                for (int ic = 0; ic < Tracking.transform.childCount; ic++)
                {
                    GameObject TrackingT = Tracking.transform.GetChild(ic).gameObject;
                    for (int ix = 0; ix < TrackingT.transform.childCount; ix++)
                    {
                        GameObject nv = TrackingT.transform.GetChild(ix).gameObject;
                        for (int iy = 0; iy < nv.transform.childCount; iy++)
                        {
                            GameObject nvd = nv.transform.GetChild(iy).gameObject;
                            nvd.GetComponent<LineRenderer>().startWidth = trackingSize;
                            nvd.GetComponent<LineRenderer>().endWidth = trackingSize;
                        }
                    }
                }
            }
        }

        public void updateTrackingVectors()
        {
            if (tracking && Tracking != null)
            {
                int lastT = (int)Mathf.Max(parent.MinTime, parent.CurrentTime - trackingLengthPath);
                int firsT = (int)Mathf.Min(parent.MaxTime, parent.CurrentTime + trackingLengthFutur);

                for (int t = parent.MinTime; t < lastT; t++)
                {
                    if (Tracking.transform.Find("tracking_" + t) != null)
                        Tracking.transform.Find("tracking_" + t).gameObject.SetActive(false);
                }
                for (int t = lastT; t <= firsT; t++)
                {
                    if (Tracking.transform.Find("tracking_" + t) == null)
                        createTrackingAt(t);
                    else
                        Tracking.transform.Find("tracking_" + t).gameObject.SetActive(true);
                }
                for (int t = firsT + 1; t <= parent.MaxTime; t++)
                {
                    if (Tracking.transform.Find("tracking_" + t) != null)
                        Tracking.transform.Find("tracking_" + t).gameObject.SetActive(false);
                }
            }
        }

        public GameObject createTrackingAt(int t)
        {
            parent.TransformationsManager.ResetTransformation();
            GameObject TrackingT = new GameObject();
            TrackingT.name = "tracking_" + t;
            TrackingT.transform.SetParent(Tracking.transform);
            foreach (Cell cell in parent.CellsByTimePoint[t])
            {
                if (cell.Daughters.Count > 0)
                {
                    if (cell.Channels.ContainsKey(name))
                    {
                        CellChannelRenderer ch = cell.Channels[name];
                        GameObject nv = new GameObject();
                        nv.name = ch.getTuple();
                        nv.transform.SetParent(TrackingT.transform);
                        ch.vector = nv;
                        foreach (Cell d in cell.Daughters)
                        {
                            GameObject nvd = new GameObject();
                            LineRenderer lr = nvd.AddComponent<LineRenderer>();
                            lr.useWorldSpace = false;
                            lr.startWidth = trackingSize;
                            lr.endWidth = trackingSize;
                            lr.positionCount = 2;
                            lr.SetPosition(0, (ch.Gravity - parent.embryoCenter) * InterfaceManager.instance.canvasScale);
                            lr.SetPosition(1, (d.Channels[name].Gravity - parent.embryoCenter) * InterfaceManager.instance.canvasScale);
                            lr.sharedMaterial = SetsManager.instance.Default;
                            nvd.transform.SetParent(nv.transform);
                        }
                    }
                }
            }
            parent.TransformationsManager.updateDataSetRotation();
            return TrackingT;
        }
    }
}