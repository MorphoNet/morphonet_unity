using MorphoNet.Core;
using MorphoNet.Extensions.Unity;
using MorphoNet.UI.Desktop;

using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using MorphoNet.Interaction;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace MorphoNet
{
    public class TransformationsManager : MonoBehaviour
    {
        public DataSet dataset;

        [Header("Embryo geometry modification")]
        public Vector3 ZommCutPlan;

        public Slider SliderFixedPlan;
        public Slider SliderXMin;
        public Slider SliderXMax;
        public Slider SliderYMin;
        public Slider SliderYMax;
        public Slider SliderZMin;
        public Slider SliderZMax;


        public StartUp startup;
        //Embryo cut
        public int XMin = 0;

        public int XMax = int.MaxValue;
        public int YMin = int.MinValue;
        public int YMax = int.MaxValue;
        public int ZMin = int.MinValue;
        public int ZMax = int.MaxValue;

        #region Data Set Translation & Rotation

        private Vector2 _DragPosition;
        private Vector2 _PreviousMousePosition;
        private readonly float _Radius = 50f;
        private Vector3 _StartDrag;
        private Vector3 _EndDrag;
        private Quaternion _DownR;
        private bool _Dragging;
        private bool _Moving;

        private Quaternion _InitialRotation;
        private Vector3 _InitialTranslation;
        private Vector3 _InitialScale;

        public Quaternion CurrentRotation { get; private set; }
        public Vector3 CurrentPosition { get; private set; }
        public Vector3 CurrentScale { get; private set; }

        public Dictionary<int, Quaternion> RotationAt { get; private set; } = new Dictionary<int, Quaternion>();
        public Dictionary<int, Vector3> TranslationAt { get; private set; } = new Dictionary<int, Vector3>();
        public Dictionary<int, Vector3> ScaleAt { get; private set; } = new Dictionary<int, Vector3>();

        #endregion Data Set Translation & Rotation

        #region Figure Translation & Rotation

        private Vector3 _DragPositionAxis;
        private Vector3 _PreviousMousePositionAxis;
        private readonly float _Radius_axis = 5f;
        private Vector3 _StartDrag_axis;
        private Vector3 _EndDrag_axis;
        private Quaternion _DownR_axis;
        private bool _Dragging_axis;
        private bool _MovingAxis;

        #endregion Figure Translation & Rotation

        private readonly float _MovingSpeed = 0.01f;

        //ZOOm
        public readonly float _ZoomRatio = 0.02f;

        public bool zoomning;

        //Main GO Tracking
        public GameObject Tracking;

        //To plot the Embryo Center
        public GameObject rotationAxis;

        public bool Initialised { get; private set; } = false;

        public bool user_left_clicked_background = false;

        public bool track_geometric = true;
        public static bool turnEmbryo = false;
        public static bool activeTurnEmbryo = false;
        public static bool embryo_cropping = false;

#if UNITY_IOS || UNITY_ANDROID
    private float _DistanceTouch = -1;
#endif

        private void Start()
        {
            ZommCutPlan = InterfaceManager.instance.ZommCutPlan;
            SliderFixedPlan = InterfaceManager.instance.SliderFixedPlan;
            SliderXMin = InterfaceManager.instance.SliderXMin;
            SliderXMax = InterfaceManager.instance.SliderXMax;
            SliderYMin = InterfaceManager.instance.SliderYMin;
            SliderYMax = InterfaceManager.instance.SliderYMax;
            SliderZMin = InterfaceManager.instance.SliderZMin;
            SliderZMax = InterfaceManager.instance.SliderZMin;

            //For Tracking
            Tracking = GameObject.Find("Tracking");

            _Dragging = false;
            _Moving = false;

            _Dragging_axis = false;
            _MovingAxis = false;

            zoomning = false;

            CurrentRotation = Quaternion.Euler(new Vector3(0, 0, 0));

            if (LoadParameters.instance.init_rotation != null)
                CurrentRotation = LoadParameters.instance.init_rotation;

            _InitialRotation = CurrentRotation;
            //CurrentPosition = transform.position;
            CurrentPosition = transform.position;
            if (LoadParameters.instance.init_translation != null)
            {
                CurrentPosition += LoadParameters.instance.init_translation;
                _InitialTranslation = CurrentPosition;
            }
            else
                _InitialTranslation = CurrentPosition;

            CurrentScale = new Vector3(1f, 1f, 1f);

            if (LoadParameters.instance.init_scale == new Vector3(0f, 0f, 0f))
                LoadParameters.instance.init_scale = new Vector3(1f, 1f, 1f);

            if (LoadParameters.instance.init_scale != null || _InitialScale != null)
                CurrentScale = LoadParameters.instance.init_scale;

            _InitialScale = CurrentScale;

            dataset.embryo_container.transform.rotation = CurrentRotation;
            dataset.embryo_container.transform.position = CurrentPosition;
            dataset.embryo_container.transform.localScale = CurrentScale;
            InterfaceManager.instance.ApplyDatasetTransfoToManualField();

            dataset.backupPosEmbryo = gameObject.transform.position;

            dataset.cube3DImage.transform.position = CurrentPosition;
            dataset.cube3DImage.transform.rotation = CurrentRotation;
            dataset.cube3DImage.transform.localScale = CurrentScale;
            dataset.cube3DImage.SetActive(false);


            rotationAxis = SetsManager.instance.rotationAxis;
            rotationAxis.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
            rotationAxis.transform.position = CurrentPosition;
            rotationAxis.transform.rotation = CurrentRotation;
            rotationAxis.SetActive(false);
            if (LoadParameters.instance.id_dataset != 0 && LoadParameters.instance.liveMode != true)
                StartCoroutine(GetDefaultTransformations());
            Initialised = true;

            UpdateEmbryoCutting();
            StartCoroutine(TrackGeometricTransformation());
        }

        public void resetDataSetRotation()
        {
            if (dataset != null && dataset.embryo_container != null)
            {

                dataset.embryo_container.transform.rotation = new Quaternion(0F, 0F, 0F, 0F);
                //dataset.embryo_container.transform.position = new Vector3(0F, 0F, 0F);
                dataset.embryo_container.transform.localPosition = new Vector3(0F, 0F, 0F);
                dataset.embryo_container.transform.localScale = new Vector3(1f, 1f, 1f);
            }

            if (Tracking != null)
            {
                Tracking.transform.rotation = new Quaternion(0F, 0F, 0F, 0F);
                Tracking.transform.position = new Vector3(0F, 0F, 0F);
                Tracking.transform.localScale = new Vector3(1f, 1f, 1f);
            }

            if (dataset != null && dataset.cube3DImage != null)
            {
                dataset.cube3DImage.transform.localScale = new Vector3(1f, 1f, 1f);
            }
        }

        public void ApplyManualRotation(float x, float y, float z)
        {
            CurrentRotation = new Quaternion(x, y, z, 1);
            updateDataSetRotation();
        }

        public void ApplyManualTranslation(float x, float y, float z)
        {
            CurrentPosition = new Vector3(x, y, z);
            updateDataSetRotation();
        }

        public void ApplyManualScale(float x)
        {
            CurrentScale = new Vector3(x, x, x);
            updateDataSetRotation();
        }

        public void Init(DataSet newDataset)
        {
            dataset = newDataset;

            SetsManager.instance.OnMeshDownloaded += UpdateDataSetPoses;
        }

        public void updateDataSetRotation()
        {
            if (dataset != null && dataset.embryo_container != null && dataset.mesh_by_time != null)
            {
                UpdateDataSetPoses();
                dataset.embryo_container.transform.localScale = CurrentScale;
            }

            if (Tracking != null)
                UpdateTrakingTransform();

            if (dataset != null && dataset.cube3DImage != null)
            {
                dataset.cube3DImage.transform.localScale = CurrentScale;
            }
        }

        public Vector3 GetInitialPosition() => _InitialTranslation;

        private IEnumerator GetDefaultTransformations()
        {
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/defaulttransformation/?id_dataset=" + LoadParameters.instance.id_dataset);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error default position : " + www.error);
            else
            {
                if (www.downloadHandler.text != "")
                {
                    CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    ci.NumberFormat.CurrencyDecimalSeparator = ".";

                    var N = JSONNode.Parse(www.downloadHandler.text);

                    if (N["translation"] != null && UtilsManager.convertJSON(N["translation"]) != "null")
                    {
                        string[] trans = UtilsManager.convertJSON(N["translation"]).Replace('(', ' ').Replace(')', ' ').Trim().Split(',');
                        if (trans.Count() == 3)
                        {
                            _InitialTranslation = CurrentPosition;
                            _InitialTranslation += new Vector3(float.Parse(trans[0], NumberStyles.Any, ci), float.Parse(trans[1], NumberStyles.Any, ci), float.Parse(trans[2], NumberStyles.Any, ci));
                            CurrentPosition = _InitialTranslation;

                        }
                    }

                    if (N["rotation"] != null && UtilsManager.convertJSON(N["rotation"]) != "null")
                    {
                        string[] rot = UtilsManager.convertJSON(N["rotation"]).Replace('(', ' ').Replace(')', ' ').Trim().Split(',');
                        if (rot.Count() == 4)
                        {
                            _InitialRotation = new Quaternion(float.Parse(rot[0], NumberStyles.Any, ci), float.Parse(rot[1], NumberStyles.Any, ci), float.Parse(rot[2], NumberStyles.Any, ci), float.Parse(rot[3], NumberStyles.Any, ci));
                            CurrentRotation = _InitialRotation;
                        }
                    }

                    if (N["scale"] != null && UtilsManager.convertJSON(N["scale"]) != "null")
                    {
                        string[] sca = UtilsManager.convertJSON(N["scale"]).Replace('(', ' ').Replace(')', ' ').Trim().Split(',');
                        if (sca.Count() == 3)
                        {
                            _InitialScale = new Vector3(float.Parse(sca[0], NumberStyles.Any, ci), float.Parse(sca[1], NumberStyles.Any, ci), float.Parse(sca[2], NumberStyles.Any, ci));
                            CurrentScale = _InitialScale;
                        }
                    }
                    UpdateDataSetPoses();
                    InterfaceManager.instance.ApplyDatasetTransfoToManualField();
                }
            }
        }

        //Rotate the embryo
        public void SetRotation(Quaternion rotation)
        {
            var previousRotation = CurrentRotation;
            CurrentRotation = rotation;
            //dataset.embryo_container.transform.rotation = CurrentRotation;
            UpdateDataSetPoses();

            if (CurrentRotation != previousRotation && MorphoTools.GetFigureManager().CellsWithText != null && MorphoTools.GetFigureManager().CellsWithText.Count > 0)
                MorphoTools.GetFigureManager().MoveTextCells();

            if (CurrentRotation != previousRotation)
                InterfaceManager.instance.ApplyDatasetTransfoToManualField();
            UpdateBoundedTransformsRotations();
        }

        public void RotateInverse(Vector3 axis, float angle)
        {
            var reversedAxis = dataset.embryo_container.transform.InverseTransformDirection(axis);
            Rotate(reversedAxis, angle);
        }

        public void Rotate(Vector3 axis, float angle)
        {
            Quaternion previousRotation = CurrentRotation;
            axis = axis.normalized;

            Quaternion newRotation = CurrentRotation * Quaternion.AngleAxis(angle, axis);

            transform.rotation = newRotation;
            CurrentRotation = transform.rotation;
            transform.rotation = Quaternion.identity;

            UpdateDataSetPoses();

            if (previousRotation != CurrentRotation)
                UpdateBoundedManagers();

            UpdateBoundedTransformsRotations();
        }

        private void UpdateBoundedManagers()
        {
            if (MorphoTools.GetFigureManager().CellsWithText != null && MorphoTools.GetFigureManager().CellsWithText.Count > 0)
                MorphoTools.GetFigureManager().MoveTextCells();

            InterfaceManager.instance.ApplyDatasetTransfoToManualField();
        }

        //Add a angle of riation
        public void addRotate(Quaternion rotation)
        {
            if (CurrentRotation.x == 0 && CurrentRotation.y == 0 && CurrentRotation.z == 0)
                CurrentRotation = rotation;
            else
                CurrentRotation = CurrentRotation * rotation;

            MorphoDebug.LogWarning($"[{Time.realtimeSinceStartup}] TM.addrotate");
            dataset.embryo_container.transform.rotation = CurrentRotation;
            Tracking.transform.rotation = CurrentRotation;
            InterfaceManager.instance.ApplyDatasetTransfoToManualField();
        }

        //Scale the embryo
        public void rescale(float sca)
        {
            CurrentScale = new Vector3(sca, sca, sca);
            dataset.embryo_container.transform.localScale = CurrentScale;
            Tracking.transform.localScale = CurrentScale;
            dataset.cube3DImage.transform.localScale = CurrentScale;
            InterfaceManager.instance.ApplyDatasetTransfoToManualField();
        }

        public void ClickBackground(bool background)
        {
            user_left_clicked_background = background;
        }

        public void rescale(Vector3 sca)
        {
            CurrentScale = sca;
            dataset.embryo_container.transform.localScale = CurrentScale;
            Tracking.transform.localScale = CurrentScale;
            dataset.cube3DImage.transform.localScale = CurrentScale;
        }

        //Move the embryo
        public void move(Vector3 position)
        {
            CurrentPosition = position;
            dataset.embryo_container.transform.position = CurrentPosition;
            Tracking.transform.position = CurrentPosition;
            InterfaceManager.instance.ApplyDatasetTransfoToManualField();
        }

        private void UpdateEmbryoCutting()
        {
            DataSetMenu datasetMenu = InterfaceManager.instance.MenuDataSet;
            if (datasetMenu != null)
            {
                //Define  EMBRYO CUT Sliders
                SliderXMin = datasetMenu.XMinSlider;
                SliderXMin.minValue = int.MaxValue;
                SliderXMin.value = int.MinValue;
                SliderXMin.maxValue = int.MinValue;

                SliderXMax = datasetMenu.XMaxSlider;
                SliderXMax.minValue = int.MaxValue;
                SliderXMax.value = int.MaxValue;
                SliderXMax.maxValue = int.MinValue;

                SliderYMin = datasetMenu.YMinSlider;
                SliderYMin.minValue = int.MaxValue;
                SliderYMin.value = int.MinValue;
                SliderYMin.maxValue = int.MinValue;

                SliderYMax = datasetMenu.YMaxSlider;
                SliderYMax.minValue = int.MaxValue;
                SliderYMax.value = int.MaxValue;
                SliderYMax.maxValue = int.MinValue;

                SliderZMin = datasetMenu.ZMinSlider;
                SliderZMin.minValue = int.MaxValue;
                SliderZMin.value = int.MinValue;
                SliderZMin.maxValue = int.MinValue;

                SliderZMax = datasetMenu.ZMaxSlider;
                SliderZMax.minValue = int.MaxValue;
                SliderZMax.value = int.MaxValue;
                SliderZMax.maxValue = int.MinValue;

                SliderFixedPlan = datasetMenu.FixedPlanSlider;
                SliderFixedPlan.gameObject.Deactivate();
                SliderFixedPlan.minValue = int.MaxValue;
                SliderFixedPlan.value = 0;
                SliderFixedPlan.maxValue = int.MinValue;
            }
        }

        public void updateTransformationsOnTimeChange(DataSetDisplayStrategy dataSetDisplayStrategy)
        {
            //UPDATE ROATION, TRANSLATION,SCALE
            float NewScale = CurrentScale.x;
            if (ScaleAt.ContainsKey(dataset.PreviousTime))
                NewScale = CurrentScale.x * _InitialScale.x / ScaleAt[dataset.PreviousTime].x;
            if (ScaleAt.ContainsKey(dataset.CurrentTime))
                NewScale = NewScale * ScaleAt[dataset.CurrentTime].x / _InitialScale.x;
            rescale(NewScale);

            Vector3 NewTranslation = new Vector3(CurrentPosition.x, CurrentPosition.y, CurrentPosition.z);
            if (TranslationAt.ContainsKey(dataset.PreviousTime))
                NewTranslation = NewTranslation + _InitialTranslation - TranslationAt[dataset.PreviousTime];
            if (TranslationAt.ContainsKey(dataset.CurrentTime))
                NewTranslation = NewTranslation + TranslationAt[dataset.CurrentTime] - _InitialTranslation;
            move(NewTranslation);

            Quaternion NewRotation = new Quaternion(CurrentRotation.x, CurrentRotation.y, CurrentRotation.z, CurrentRotation.w);
            if (RotationAt.ContainsKey(dataset.PreviousTime))
            {
                NewRotation = (NewRotation * Quaternion.Inverse(RotationAt[dataset.PreviousTime]).normalized * _InitialRotation).normalized;
            }
            if (RotationAt.ContainsKey(dataset.CurrentTime))
            {
                NewRotation = (NewRotation * Quaternion.Inverse(_InitialRotation).normalized * RotationAt[dataset.CurrentTime]).normalized;
            }
            SetRotation(NewRotation);
        }

        public void ApplyTransformationsManualPosition(Vector3 position)
        {
            CurrentPosition = position;
            if (dataset != null && dataset.embryo_container != null)
            {
                Vector3 v = new Vector3(position.x, position.y, dataset.embryo_container.transform.position.z);
                dataset.embryo_container.transform.position = v;
            }

            if (Tracking != null)
                Tracking.transform.position = position;

            if (rotationAxis != null)
                rotationAxis.transform.position = position;

            if (dataset != null && dataset.cube3DImage != null)
                dataset.cube3DImage.transform.position = position;

        }

        public void ApplyTransformationsManualRotation(Quaternion rotation)
        {
            CurrentRotation = rotation;
            if (dataset != null && dataset.embryo_container != null)
            {
                dataset.embryo_container.transform.rotation = rotation;
            }

            if (Tracking != null)
                Tracking.transform.rotation = rotation;

            if (rotationAxis != null)
                rotationAxis.transform.rotation = rotation;

            if (dataset != null && dataset.cube3DImage != null)
                dataset.cube3DImage.transform.rotation = rotation;
            updateDataSetRotation();
        }

        public void ApplyTransformationsManualScale(float zoom)
        {
            CurrentScale = new Vector3(zoom, zoom, zoom);
            if (dataset != null && dataset.embryo_container != null)
                dataset.embryo_container.transform.localScale = new Vector3(zoom, zoom, zoom);

            if (Tracking != null)
                Tracking.transform.localScale = new Vector3(zoom, zoom, zoom);
            dataset.cube3DImage.transform.localScale = new Vector3(zoom, zoom, zoom);
        }

        public void ApplyTransformationsOnTimeChange()
        {
            UpdateDataSetPoses();
            dataset.embryo_container.transform.localScale = CurrentScale;
            //dataset.embryo_container.transform.localPosition = CurrentPosition;
            dataset.embryo_container.transform.position = CurrentPosition;

            Tracking.transform.rotation = CurrentRotation;
            Tracking.transform.position = CurrentPosition;
            Tracking.transform.localScale = CurrentScale;

            rotationAxis.transform.rotation = CurrentRotation;
            rotationAxis.transform.position = CurrentPosition;

            dataset.cube3DImage.transform.rotation = CurrentRotation;
            dataset.cube3DImage.transform.position = CurrentPosition;
            dataset.cube3DImage.transform.localScale = CurrentScale;
            InterfaceManager.instance.ApplyDatasetTransfoToManualField();
        }

        public void ResetTransformation()
        {
            ResetRotation(false);
            ResetPosition(false);
            ResetScale(false);
            InterfaceManager.instance.ApplyDatasetTransfoToManualField();
            dataset.update = true;
            ResetRawImages();
        }

        public void ResetRawImages()
        {
            dataset.cube3DImage.transform.position = CurrentPosition;
            dataset.cube3DImage.transform.localScale = CurrentScale;
            dataset.cube3DImage.transform.rotation = CurrentRotation;

        }

        public void ResetRotation(bool update = true)
        {
            CurrentRotation = _InitialRotation;

            if (update)
            {
                InterfaceManager.instance.ApplyDatasetTransfoToManualField();
                dataset.update = true;
            }
        }

        public void ResetScale(bool update = true)
        {
            CurrentScale = _InitialScale;

            if (update)
            {
                InterfaceManager.instance.ApplyDatasetTransfoToManualField();
                dataset.update = true;
            }
        }

        public void ResetPosition(bool update = true)
        {
            CurrentPosition = _InitialTranslation;
            if (update)
            {
                InterfaceManager.instance.ApplyDatasetTransfoToManualField();
                dataset.update = true;
            }
        }

        public void saveRotationAt()
        {
            if (SetsManager.instance.userRight <= 1)
                StartCoroutine(launchSaveRotation(dataset.CurrentTime));
        }

        public void saveRotation()
        {
            if (SetsManager.instance.userRight <= 1)
                StartCoroutine(launchSaveRotation(-1));
        }

        public IEnumerator launchSaveRotation(int tToSave)
        {
            WWWForm form = new WWWForm();

            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("download", "9");
            form.AddField("id_dataset", dataset.id_dataset.ToString());

            if (tToSave > -1)
            {
                form.AddField("t", tToSave.ToString());

                RotationAt[tToSave] = CurrentRotation;
                TranslationAt[tToSave] = CurrentPosition;
                ScaleAt[tToSave] = CurrentScale;
            }

            form.AddField("rotation", CurrentRotation.ToString());
            form.AddField("translation", CurrentPosition.ToString());
            form.AddField("scale", CurrentScale.ToString());

            string request_url = "";

            if (tToSave > -1) // ASK: c�est pas juste les deux m�me requettes au final ? Seul la position de &t change dans la requete.
            {
                request_url = MorphoTools.GetServer() + "api/saverotations/?hash=" + SetsManager.instance.hash + "&id_dataset=" + dataset.id_dataset.ToString() + "&t=" + tToSave.ToString() + "&rotation=" + CurrentRotation.ToString() + "&translation=" + CurrentPosition.ToString() + "&scale=" + CurrentScale.ToString();
            }
            else
            {
                request_url = MorphoTools.GetServer() + "api/saverotations/?hash=" + SetsManager.instance.hash + "&id_dataset=" + dataset.id_dataset.ToString() + "&rotation=" + CurrentRotation.ToString() + "&translation=" + CurrentPosition.ToString() + "&scale=" + CurrentScale.ToString() + "&t=" + tToSave.ToString();
            }

            UnityWebRequest www = UnityWebRequests.Get(request_url, "");

            yield return www.SendWebRequest();

            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);

            www.Dispose();
        }

        //clear default rotation for dataset on the server
        public void ClearRotation()
        {
            if (SetsManager.instance.userRight <= 1)
                StartCoroutine(LaunchClearRotation());
        }

        private IEnumerator LaunchClearRotation()
        {
            WWWForm form = new WWWForm();

            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("download", "13");
            form.AddField("id_dataset", dataset.id_dataset.ToString());

            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/deletepositions/?hash=" + SetsManager.instance.hash + "&id_dataset=" + dataset.id_dataset.ToString());

            yield return www.SendWebRequest();

            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);

            www.Dispose();

            RotationAt.Clear();
            TranslationAt.Clear();
        }

        //We update the boundaries of the sliders accord the different gravity center
        public void updateXYZCutSliders(Vector3 Grav)
        {
            //X
            if (Grav.x < SliderXMin.minValue)
            {
                SliderXMin.minValue = (int)Mathf.Floor(Grav.x) - 1;
                SliderXMax.minValue = SliderXMin.minValue;
                SliderXMin.value = SliderXMin.minValue;  //SLIDER WHERE NOT YET MOVE
            }

            if (Grav.x > SliderXMin.maxValue)
            {
                SliderXMin.maxValue = (int)Mathf.Floor(Grav[0]) + 1;
                SliderXMax.maxValue = SliderXMin.maxValue;
                SliderXMax.value = SliderXMax.maxValue;  //SLIDER WHERE NOT YET MOVE
            }

            //Y
            if (Grav.y < SliderYMin.minValue)
            {
                SliderYMin.minValue = (int)Mathf.Floor(Grav.y) - 1;
                SliderYMax.minValue = SliderYMin.minValue;
                SliderYMin.value = SliderYMin.minValue;  //SLIDER WHERE NOT YET MOVE
            }

            if (Grav.y > SliderYMin.maxValue)
            {
                SliderYMin.maxValue = (int)Mathf.Floor(Grav[1]) + 1;
                SliderYMax.maxValue = SliderYMin.maxValue;
                SliderYMax.value = SliderYMax.maxValue;  //SLIDER WHERE NOT YET MOVE
            }

            //Z
            if (Grav.z < SliderZMin.minValue)
            {
                SliderZMin.minValue = (int)Mathf.Floor(Grav.z) - 1;
                SliderZMax.minValue = SliderZMin.minValue;
                SliderZMin.value = SliderZMin.minValue;  //SLIDER WHERE NOT YET MOVE
            }

            if (Grav.z > SliderZMin.maxValue)
            {
                SliderZMin.maxValue = (int)Mathf.Floor(Grav.z) + 1;
                SliderZMax.maxValue = SliderZMin.maxValue;
                SliderZMax.value = SliderZMax.maxValue;  //SLIDER WHERE NOT YET MOVE
            }

            //FOR FIXED PLAN
            for (int i = 0; i < 3; i++)
            {
                if (Grav[i] > SliderFixedPlan.maxValue)
                    SliderFixedPlan.maxValue = (int)Mathf.Floor(Grav[i]) + 1;

                if (Grav[i] < SliderFixedPlan.minValue)
                    SliderFixedPlan.minValue = (int)Mathf.Floor(Grav[i]) - 1;
            }
        }

        //Callbacks from scene to update cut
        public void Xmin()
        {
            XMin = (int)Mathf.Round(SliderXMin.value);
            embryo_cropping = (XMin != int.MinValue);
            dataset.UpdateAllShaders();
        }

        public void Xmax()
        {
            XMax = (int)Mathf.Round(SliderXMax.value);
            embryo_cropping = (XMax != int.MaxValue);
            dataset.UpdateAllShaders();
        }

        public void Ymin()
        {
            YMin = (int)Mathf.Round(SliderYMin.value);
            embryo_cropping = (YMin != int.MinValue);
            dataset.UpdateAllShaders();
        }

        public void Ymax()
        {
            YMax = (int)Mathf.Round(SliderYMax.value);
            embryo_cropping = (YMax != int.MaxValue);
            dataset.UpdateAllShaders();
        }

        public void Zmin()
        {
            ZMin = (int)Mathf.Round(SliderZMin.value);
            embryo_cropping = (ZMin != int.MinValue);
            dataset.UpdateAllShaders();
        }

        public void Zmax()
        {
            ZMax = (int)Mathf.Round(SliderZMax.value);
            embryo_cropping = (ZMax != int.MaxValue);
            dataset.UpdateAllShaders();
        }

        private bool _usingFixedPlan;
        //Dataset > Transformation, when switching to 3 planes cut to a fixed plane cut
        public void OnFixedPlan(bool v)
        {
            GameObject ce = InterfaceManager.instance.canvas.transform.Find("bouton layout group").Find("Dataset").Find("MenuDataset").gameObject;
            _usingFixedPlan = v;
            if (ce != null)
            {
                Transform layout = ce.transform.Find("SubMenusGroup").Find("Transformations").Find("TransfoLayout").Find("Crop");

                layout.Find("XMin").gameObject.SetActive(!v);
                layout.Find("XMax").gameObject.SetActive(!v);
                layout.Find("YMin").gameObject.SetActive(!v);
                layout.Find("YMax").gameObject.SetActive(!v);
                layout.Find("ZMin").gameObject.SetActive(!v);
                layout.Find("ZMax").gameObject.SetActive(!v);
                layout.Find("SliderXMin").gameObject.SetActive(!v);
                layout.Find("SliderXMax").gameObject.SetActive(!v);
                layout.Find("SliderYMin").gameObject.SetActive(!v);
                layout.Find("SliderYMax").gameObject.SetActive(!v);
                layout.Find("SliderZMin").gameObject.SetActive(!v);
                layout.Find("SliderZMax").gameObject.SetActive(!v);
                layout.Find("SliderFixedPlan").gameObject.SetActive(v);
            }
            MorphoTools.GetDataset().UpdateAllShaders();
        }

        public static Vector3 MapToSphere(Vector3 initPos, Vector3 position, float radius)
        {
            Ray ray = Camera.main.ScreenPointToRay(position);

            Vector3 normal = (initPos - Camera.main.transform.position).normalized;
            Plane plane = new Plane(normal, initPos);

            plane.Raycast(ray, out float dist);

            Vector3 hitPoint = ray.GetPoint(dist);
            float length = Vector3.Distance(hitPoint, initPos);

            if (length < radius)
            {
                //on arcball
                float k = (float)(Mathf.Sqrt(radius - length));
                hitPoint -= normal * k;
            }
            else
            {
                //outside.
                Vector3 dir = (hitPoint - initPos).normalized;
                hitPoint = initPos + dir * radius;
            }

            return (hitPoint - initPos).normalized;
        }

        public bool outOfPlan(Vector3 gr)
        {
            Vector3 grC = new Vector3((gr[0] + dataset.embryoCenter.x), (gr[1] - dataset.embryoCenter.y), (gr[2] - dataset.embryoCenter.z));
            Vector3 coordrotate = CurrentRotation * grC;

            if (_usingFixedPlan && coordrotate.z < InterfaceManager.instance.SliderFixedPlan.value)
                return true;

            return false;
        }

        public IEnumerator TrackGeometricTransformation()
        {
            /*
            initialRotation = CurrentRotation.eulerAngles;
            initialPosition = CurrentPosition;
            initialZoom = CurrentScale;

            while (track_geometric)
            {
                if (initialPosition != CurrentPosition)
                {
                    Vector3 pos_shift = initialPosition - CurrentPosition;
                    if (pos_shift.x != 0f || pos_shift.y != 0f || pos_shift.z != 0f)
                        TrackerManagement.instance.SendTrackingInfo("DATASET_" + MorphoTools.GetDataset().id_dataset + ",move," + pos_shift.ToString());
                }

                if (initialRotation != CurrentRotation.eulerAngles)
                {
                    Vector3 euler_shift = initialRotation - CurrentRotation.eulerAngles;
                    //force save of rotation
                    if (euler_shift.x != 0f || euler_shift.y != 0f || euler_shift.z != 0f)
                        TrackerManagement.instance.SendTrackingInfo("DATASET_" + MorphoTools.GetDataset().id_dataset + ",rotate," + euler_shift.ToString());
                }

                if (initialZoom != CurrentScale)
                {
                    Vector3 zoom_shift = initialZoom - CurrentScale;
                    //force save of rotation
                    if (zoom_shift.x != 0f || zoom_shift.y != 0f || zoom_shift.z != 0f)
                        TrackerManagement.instance.SendTrackingInfo("DATASET_" + MorphoTools.GetDataset().id_dataset + ",scale," + zoom_shift.ToString());
                }

                initialRotation = CurrentRotation.eulerAngles;
                initialPosition = CurrentPosition;
                initialZoom = CurrentScale;

                yield return new WaitForSecondsRealtime(1.0f);
            }
            */
            yield return null;
        }

        internal void RotateSlerpUnclamped(Quaternion rotationMultiplire, float factor)
        {
            Quaternion newRotation = Quaternion.SlerpUnclamped(
                CurrentRotation,
                rotationMultiplire * CurrentRotation,
                factor);

            Quaternion previousRotation = CurrentRotation;

            transform.rotation = newRotation;
            CurrentRotation = transform.rotation;
            transform.rotation = Quaternion.identity;

            UpdateDataSetPoses();

            if (previousRotation != CurrentRotation)
                UpdateBoundedManagers();

            UpdateBoundedTransformsRotations();
        }

        public void ComputeUserInputs()
        {
            if (!Initialised)
                return;

#if (UNITY_IOS || UNITY_ANDROID)
        int coef = 0;
        if (Input.touchCount == 2)
        {
            var touch1 = Input.GetTouch(0).position;
            var touch2 = Input.GetTouch(1).position;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touch1 - touch2).magnitude;

            if (prevTouchDeltaMag - _DistanceTouch < -10 || prevTouchDeltaMag - _DistanceTouch > 10)
            {
                if (prevTouchDeltaMag < _DistanceTouch)
                    coef = -1;
                else
                    coef = 1;
            }

            _DistanceTouch = prevTouchDeltaMag;
        }

        if (coef == 0 && Input.touchCount == 2)
            DataSetRotation(Input.mousePosition);
        else
            CancelDataSetRotation();

        if (Input.touchCount == 3 && InterfaceManager.instance.CanInteractWithViewer(0))
            DataSetTranslation(Input.mousePosition);
        else
            CancelDataSetTranslation();

        UpdateDatasetScaling(coef);
#endif
            UpdateRotationAxisDisplay();
        }

        public bool CanTransformFigureManager()
        {
            return MorphoTools.GetFigureManager() != null && MorphoTools.GetFigureManager().show_axis_figure;
        }

        public void UpdateDatasetScaling(int coef)
        {
            if (coef != 0)
            {
                Vector3 b = dataset.embryo_container.transform.localScale;
                dataset.embryo_container.transform.localScale += coef * new Vector3(_ZoomRatio*b.x, _ZoomRatio*b.y, _ZoomRatio*b.z);

                if (dataset.embryo_container.transform.localScale.x <= 0)
                    dataset.embryo_container.transform.localScale = new Vector3(_ZoomRatio, _ZoomRatio, _ZoomRatio);

                Vector3 previous_current = CurrentScale;
                CurrentScale = transform.localScale;

                if (CurrentScale != previous_current && MorphoTools.GetFigureManager().CellsWithText != null && MorphoTools.GetFigureManager().CellsWithText.Count > 0)
                    MorphoTools.GetFigureManager().MoveTextCells();

                InterfaceManager.instance.ApplyDatasetTransfoToManualField();
                Tracking.transform.localScale = CurrentScale;
                dataset.cube3DImage.transform.localScale = CurrentScale;
            }
        }

        public void UpdateRotationAxisDisplay()
        {
            if (_Moving || _Dragging)
                rotationAxis.SetActive(true);
            else
                rotationAxis.SetActive(false);
        }

        public void DataSetTranslation(Vector2 mousePosition)
        {
            //Move the embryo center

            if (!_Moving)
            {
                _PreviousMousePosition = mousePosition;

                _Moving = true;
            }
            else
            {
                var previousVec3Position = new Vector3(_PreviousMousePosition.x, _PreviousMousePosition.y);
                var currentVec3Position = new Vector3(mousePosition.x, mousePosition.y);
                dataset.embryo_container.transform.position += (currentVec3Position - previousVec3Position) * _MovingSpeed;
                _PreviousMousePosition = mousePosition;

                Vector3 previous_current = CurrentPosition;
                CurrentPosition = transform.position;

                if (previous_current != CurrentPosition && MorphoTools.GetFigureManager().CellsWithText != null && MorphoTools.GetFigureManager().CellsWithText.Count > 0)
                    MorphoTools.GetFigureManager().MoveTextCells();

                InterfaceManager.instance.ApplyDatasetTransfoToManualField();
                rotationAxis.transform.position = CurrentPosition;
                dataset.cube3DImage.transform.position = CurrentPosition;
                Tracking.transform.position = CurrentPosition;
            }
        }

        public void CancelDataSetTranslation()
        {
            _Moving = false;
        }

        private void OnApplicationFocus(bool hasFocus)
        {
            if (startup == null)
            {
                startup = FindObjectOfType<StartUp>();
            }

            if (startup != null)
            {
                startup.GetInteractionMediator().RemoveModifier(CommandModifier.RotationActivated);
            }
            CancelDataSetRotation();

        }

        public void DataSetRotation(Vector2 mousePosition)
        {
            //Turn the embryo (Arcball)

            if (!_Dragging) //WHEN START
            {
                _DownR = CurrentRotation;

                _PreviousMousePosition = _DragPosition = mousePosition;
                _StartDrag = MapToSphere(transform.position, _DragPosition, _Radius);
                _Dragging = true;
            }
            else //WHEN DRAG
            {
                _DragPosition += mousePosition - _PreviousMousePosition;
                _EndDrag = MapToSphere(transform.position, _DragPosition, _Radius);

                Vector3 axis = Vector3.Cross(_StartDrag, _EndDrag).normalized;
                float angle = Vector3.Angle(_StartDrag, _EndDrag);

                Quaternion dragR = Quaternion.AngleAxis(angle, axis);

                Quaternion newRotation = dragR * _DownR;
                transform.rotation = newRotation;

                _PreviousMousePosition = mousePosition;
                Quaternion previousRotation = CurrentRotation;
                CurrentRotation = transform.rotation;

                transform.rotation = Quaternion.identity;
                UpdateDataSetPoses();

                if (previousRotation != CurrentRotation)
                    UpdateBoundedManagers();

                UpdateBoundedTransformsRotations();

                if (_usingFixedPlan)
                    dataset.UpdateAllShaders();
            }
        }

        private void UpdateDataSetPoses()
        {
            foreach (TimePoint tsb in dataset.TimePoints)
            {
                if (tsb != null)
                {
                    Pose pose = dataset.DataSetDisplayStrategy.RelativePose(
                        CurrentRotation,
                        dataset.CurrentTime,
                        tsb.Number);
                    tsb.SetLocalPosition(pose.position);
                    tsb.SetRotation(pose.rotation);
                }
            }
        }

        private void UpdateTrakingTransform()
        {
            Tracking.transform.rotation = CurrentRotation;
            Tracking.transform.position = CurrentPosition;
            Tracking.transform.localScale = CurrentScale;
        }

        private void UpdateBoundedTransformsRotations()
        {
            rotationAxis.transform.rotation = CurrentRotation;
            dataset.cube3DImage.transform.rotation = CurrentRotation;
            Tracking.transform.rotation = CurrentRotation;
        }

        public void CancelDataSetRotation()
        {
            _Dragging = false;
        }

        public void FigureTranslation(Vector3 mousePosition)
        {
            if (!_MovingAxis)
                _MovingAxis = true;
            else
                InterfaceManager.instance.axis_figure.transform.position += (mousePosition - _PreviousMousePositionAxis) * _MovingSpeed;

            _PreviousMousePositionAxis = mousePosition;
        }

        public void CancelFigureTranslation()
        {
            _MovingAxis = false;
        }

        public void FigureRotation(Vector3 mousePosition)
        {
            if (!_Dragging_axis) //WHEN START
            {
                _DownR_axis = InterfaceManager.instance.axis_figure.transform.rotation;

                _PreviousMousePositionAxis = mousePosition;
                _DragPositionAxis = mousePosition;

                _StartDrag_axis = MapToSphere(InterfaceManager.instance.axis_figure.transform.position, _DragPositionAxis, _Radius_axis);
                _Dragging_axis = true;
            }
            else //WHEN DRAG
            {
                _DragPositionAxis += mousePosition - _PreviousMousePositionAxis;
                _EndDrag_axis = MapToSphere(InterfaceManager.instance.axis_figure.transform.position, _DragPositionAxis, _Radius_axis);

                Vector3 axis = Vector3.Cross(_StartDrag_axis, _EndDrag_axis).normalized;
                float angle = Vector3.Angle(_StartDrag_axis, _EndDrag_axis);

                Quaternion dragR = Quaternion.AngleAxis(angle, axis);

                InterfaceManager.instance.axis_figure.transform.rotation = dragR * _DownR_axis;

                _PreviousMousePositionAxis = mousePosition;
            }
        }

        public void CancelFigureRotation()
        {
            _Dragging_axis = false;
        }
    }
}
