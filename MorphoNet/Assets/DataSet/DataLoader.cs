using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System.IO;
using System;
using UnityEngine.UI;
using static MorphoNet.DevelopmentTable;
using MorphoNet.Core;
using UnityEditor;

namespace MorphoNet
{
    public class DataLoader : MonoBehaviour
    {
        public static DataLoader instance;

        private const int MENU_SHORTCUT_UNLOAD_TIME = 30;

        private const string PRIMITIVE_PREFIX = "p ";

        [SerializeField]
        private TimePoint _TimestampBehaviourPrefab;

        private void Awake()
        {
            if (instance == null)
                instance = this;
            else
                Destroy(gameObject);
        }

        #region Download_data

        //Download all meshes in one file
        public void onDownloadAllMeshes()
        { StartCoroutine(downloadAllMeshes()); }

        public void onDownloadAllMeshesCurrentChannel()
        { StartCoroutine(downloadAllMeshesForChannel()); }

        public IEnumerator downloadAllMeshes()
        {
            InterfaceManager.instance.MenuDataSet.HideDownloadMesh();
            //GameObject downloadMesh = InterfaceManager.instance.MenuDataset.transform.Find("SubMenusGroup").Find("Channels").Find("MenuChannels").Find("downloadMesh").gameObject;
            //downloadMesh.SetActive(false);
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("download", "11");
            form.AddField("id_dataset", MorphoTools.GetDataset().id_dataset.ToString());
            form.AddField("quality", MorphoTools.GetDataset().Quality.ToString());
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer() + "api/downloadallmeshes/?hash=", SetsManager.instance.hash + "&id_dataset=" + MorphoTools.GetDataset().id_dataset.ToString() + "&quality=" + MorphoTools.GetDataset().Quality.ToString());
            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                var N = JSONNode.Parse(www.downloadHandler.text);

#if UNITY_WEBGL
                MorphoDebug.Log("window.open(\"" + MorphoTools.GetServer() + N["filename"] + "\")");
                Application.ExternalEval("window.open(\"" + MorphoTools.GetServer() + N["filename"] + "\")");
#else
                string urlinfos = MorphoTools.GetServer() + N["filename"];
                MorphoDebug.Log("OPEN " + urlinfos);
                if (urlinfos.StartsWith(MORPHONET_URL_START))
                {
                    Application.OpenURL(urlinfos);
                }
#endif
                Application.ExternalEval("window.open(\"" + MorphoTools.GetServer() + N["filename"] + "\")");
            }
            www.Dispose();
            if (SetsManager.instance.userRight < 2)
                InterfaceManager.instance.MenuDataSet.ShowDownloadMesh();//downloadMesh.SetActive(true); //ONLY FOR CREATOR AND OWNER
        }

        private static string MORPHONET_URL_START = "https://morphonet.org";
        public IEnumerator downloadAllMeshesForChannel()
        {
            string channel_name = MorphoTools.GetDataset().currentChannel != null && MorphoTools.GetDataset().currentChannel != "" ? MorphoTools.GetDataset().currentChannel : "0";
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("download", "11");
            form.AddField("id_dataset", MorphoTools.GetDataset().id_dataset.ToString());
            form.AddField("quality", MorphoTools.GetDataset().Quality.ToString());
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer() + "api/downloadallmesheschannel/?hash=", SetsManager.instance.hash + "&channel_name=" + channel_name + "&id_dataset=" + MorphoTools.GetDataset().id_dataset.ToString() + "&quality=" + MorphoTools.GetDataset().Quality.ToString());
            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                MorphoDebug.Log(www.downloadHandler.text);
                var N = JSONNode.Parse(www.downloadHandler.text);
                MorphoDebug.Log("window.open(\"" + MorphoTools.GetServer() + N["filename"] + "\")");
#if UNITY_WEBGL
                Application.ExternalEval("window.open(\"" + MorphoTools.GetServer() + N["filename"] + "\")");
#else
                    string urlinfos = MorphoTools.GetServer() + N["filename"];
                    MorphoDebug.Log("OPEN " + urlinfos);
                    Debug.Log(urlinfos);
                    if (urlinfos.StartsWith(MORPHONET_URL_START))
                    {
                        Application.OpenURL(urlinfos);
                    }
#endif
                
            }
            www.Dispose();
        }

        #endregion Download_data

        #region Genetic

        public IEnumerator queryGeneAnissed(Gene g, DataSet AttachedDataset)
        {
            InterfaceManager.setLoading();
            if (g.Cells == null)
            {
                //MorphoDebug.Log ("Look for cells for gene " + g.name);
                WWWForm form = new WWWForm();
                form.AddField("gene_id", g.id.ToString());
                form.AddField("organism_id", AttachedDataset.GeneticManager.organism_id.ToString());
                form.AddField("start_stage", AttachedDataset.start_stage);
                form.AddField("end_stage", AttachedDataset.end_stage);
                //MorphoDebug.Log("start_stage="+ AttachedDataset.start_stage+ " end_stage=" + AttachedDataset.end_stage);
                UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/genebyid/", form);
                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                {
                    MorphoDebug.Log("Error : " + www.error);
                }
                else
                {
                    // Stopwatch sw = Stopwatch.StartNew();
                    //MorphoDebug.Log ("For " + g.name + " -> found cells " + www.downloadHandler.text);
                    if (www.downloadHandler.text != "[]")
                    {
                        g.Cells = new Dictionary<Cell, float>();
                        //MorphoDebug.Log(www.downloadHandler.text);
                        var N = JSONNode.Parse(www.downloadHandler.text);
                        //MorphoDebug.Log ("For " + g.name + " found " + N.Count + " Stages");
                        if (N.Count > 0)
                        { //ORDER BY STAGE
                            int i = 0;
                            //MorphoDebug.Log("idxInfoxName=" + idxInfoxName.ToString());
                            foreach (var key in N.Keys)
                            {
                                string stage = key.ToString().Trim();
                                int minTime = -1;
                                int maxTime = -1;
                                //MorphoDebug.Log("AttachedDataset.spf=" + AttachedDataset.spf + " AttachedDataset.dt=" + AttachedDataset.dt);

                                if (AttachedDataset.dt > 0 && AttachedDataset.spf > 0)
                                {
                                    Vector2 hpfb = AttachedDataset.GeneticManager.getHPFBoundaries(stage);
                                    //MorphoDebug.Log ("Found hpfb.x=" + hpfb.x + " hpfb.y=" + hpfb.y);
                                    minTime = AttachedDataset.DevTable.convertHPFtoTime(hpfb.x);
                                    maxTime = AttachedDataset.DevTable.convertHPFtoTime(hpfb.y);
                                    //MorphoDebug.Log (" For Stage " + stage+"-> found MinTime=" + minTime + " MaxTime=" + maxTime);
                                }
                                else
                                { //Embryo Split by Stage (for ANISEED) where gameobject embryo name correspond to stage
                                    for (int t = AttachedDataset.MinTime; t <= AttachedDataset.MaxTime; t++)
                                        if (AttachedDataset.mesh_by_time[t] != null && AttachedDataset.mesh_by_time[t].name == stage)
                                        { minTime = t; maxTime = t; }
                                }

                                if (minTime != -1 && maxTime != -1)
                                {
                                    if (minTime < AttachedDataset.MinTime)
                                        minTime = AttachedDataset.MinTime;
                                    if (maxTime >= AttachedDataset.MaxTime)
                                        maxTime = AttachedDataset.MaxTime + 1;
                                    //MorphoDebug.Log(" FNM" + stage + "-> found MinTime=" + minTime + " MaxTime=" + maxTime);
                                    Dictionary<string, float> cellnamesConverted = new Dictionary<string, float>();
                                    Dictionary<string, bool> cellFound = new Dictionary<string, bool>(); //Just to list the cells founed
                                                                                                         //MorphoDebug.Log("FNM : " + N[i].ToString() + "count :  " + N[i].Count);
                                    for (int j = 0; j < N[i].Count; j++)
                                    {
                                        string cnc = AttachedDataset.GeneticManager.convertCellName(UtilsManager.convertJSON(N[i][j][0])); //Convert the cell name format
                                        float val = UtilsManager.convertJSONToFloat(N[i][j][1]);
                                        cellnamesConverted[cnc] = val;
                                        cellFound[cnc] = false;
                                    }
                                    if (maxTime == minTime)
                                        maxTime += 1; //FOR ANISEED STAGES
                                    for (int t = minTime; t < maxTime; t++) //MaxTime below to the next Stage
                                    {
                                        if (AttachedDataset.CellsByTimePoint.ContainsKey(t))
                                        {
                                            foreach (var cell in AttachedDataset.CellsByTimePoint[t])
                                            {
                                                string cn = cell.getInfos(AttachedDataset.GeneticManager.idxInfoxName); //Get the cell name
                                                                                                                        //always false, but cn is in cellnamesConverted
                                                if (cellnamesConverted.Keys.Contains(cn))
                                                {
                                                    g.Cells[cell] = cellnamesConverted[cn]; //Recieve the expression value
                                                    cellFound[cn] = true;
                                                }
                                                else
                                                {
                                                    if (SetsManager.instance.API_Menu == "Genetic")
                                                        g.Cells[cell] = 0.00001f;
                                                }
                                            }
                                        }
                                    }
                                    if (SetsManager.instance.API_Menu == "Genetic")
                                    {
                                        //FOR ANISEED STAGE EMBRYO IT CELL IS NOT FOUND AT THIS STAGE WE  LOOK THE TIME BEFORE AND AFTER IF THE MOTHER OR DAUGHTER CELL EXIST
                                        Dictionary<string, float> cellnamesConvertedLeft = new Dictionary<string, float>();
                                        foreach (var itemc in cellFound)
                                        {
                                            if (!itemc.Value)
                                            {
                                                string cnc = itemc.Key;
                                                //MorphoDebug.Log(" FNM" + "Cell " + cnc + " not found at " + stage);
                                                cellnamesConvertedLeft[cnc] = cellnamesConverted[cnc];
                                            }
                                        }
                                        if (cellnamesConvertedLeft.Count() > 0)
                                        {
                                            AttachedDataset.GeneticManager.assignTo(false, g, minTime - 1, cellnamesConvertedLeft);
                                            AttachedDataset.GeneticManager.assignTo(true, g, maxTime + 1, cellnamesConvertedLeft);
                                        }
                                    }
                                }
                                i += 1;
                            }
                        }
                    }
                    g.download();
                    g.active();
                    // sw.Stop();

                    //MorphoDebug.Log("Time taken: " + sw.Elapsed.TotalMilliseconds + "ms");
                }
                www.Dispose();
            }
            else
            {
                g.active();
                g.active(); //???????
            }
            InterfaceManager.stopLoading();
        }

        public IEnumerator querySelectedCell(Cell c, DataSet AttachedDataset)
        {
            InterfaceManager.setLoading();
            string cn = c.getInfos(AttachedDataset.GeneticManager.idxInfoxName);
            string stage = "Stage " + AttachedDataset.DevTable.getStage(c.t);
            //MorphoDebug.Log ("Show on " + c.ID + " -> " + cn + "==" + invConvertCellName (cn) + " at " + stage);
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("getGenes", AttachedDataset.GeneticManager.invConvertCellName(cn));
            form.AddField("organism_id", AttachedDataset.GeneticManager.organism_id.ToString());
            form.AddField("stage", stage);
            AttachedDataset.GeneticManager.GeneListRequest = UnityWebRequests.Post(MorphoTools.GetServer(), "api/genesforcell/", form);
            yield return AttachedDataset.GeneticManager.GeneListRequest.SendWebRequest();
            if (AttachedDataset.GeneticManager.GeneListRequest.result == UnityWebRequest.Result.ConnectionError || AttachedDataset.GeneticManager.GeneListRequest.result == UnityWebRequest.Result.ProtocolError)
            {
                MorphoDebug.Log("Error : " + AttachedDataset.GeneticManager.GeneListRequest.error);
            }
            else
            {
                if (AttachedDataset.GeneticManager.GeneListRequest.downloadHandler.text != "")
                {
                    var N = JSONNode.Parse(AttachedDataset.GeneticManager.GeneListRequest.downloadHandler.text);
                    MorphoDebug.Log(N.ToString());
                    AttachedDataset.GeneticManager.ShowGenes = new Dictionary<int, int>();
                    if (N != null)
                        for (int i = 0; i < N.Count; i++)
                        {
                            string ns = N[i].ToString().Trim();
                            int gene_id = int.Parse(ns.Trim('"'));
                            //MorphoDebug.Log (i + "->" + N [i].ToString () + " -> " + ns.Substring (3, ns.Length - 6) + "->" + gene_id);
                            AttachedDataset.GeneticManager.ShowGenes[gene_id] = 1;
                        }
                    AttachedDataset.GeneticManager.UpdateGenesView();
                }
            }
            AttachedDataset.GeneticManager.GeneListRequest.Dispose();
            AttachedDataset.GeneticManager.GeneListRequest = null;
            InterfaceManager.stopLoading();
        }

        public IEnumerator queryStage(string stage, GeneticManager manager)
        {
            InterfaceManager.setLoading();
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("organism_id", manager.organism_id.ToString());
            form.AddField("stage", stage);
            manager.GeneListRequest = UnityWebRequests.Post(MorphoTools.GetServer(), "api/genesforstage/", form);
            yield return manager.GeneListRequest.SendWebRequest();
            if (manager.GeneListRequest.result == UnityWebRequest.Result.ConnectionError || manager.GeneListRequest.result == UnityWebRequest.Result.ProtocolError)
            {
                MorphoDebug.Log("Error : " + manager.GeneListRequest.error);
            }
            else
            {
                if (manager.GeneListRequest.downloadHandler.text != "")
                {
                    var N = JSONNode.Parse(manager.GeneListRequest.downloadHandler.text);
                    //MorphoDebug.Log(N.ToString());
                    manager.ShowGenes = new Dictionary<int, int>();
                    if (N != null)
                        for (int i = 0; i < N.Count; i++)
                        {
                            //MorphoDebug.Log(N[i].ToString().Trim());
                            string ns = N[i].ToString().Trim();
                            int gene_id = int.Parse(ns.Trim('"'));
                            //MorphoDebug.Log (i + "->" + N [i].ToString () + " -> " + ns.Substring (3, ns.Length - 6) + "->" + gene_id);
                            manager.ShowGenes[gene_id] = 1;
                        }
                    manager.UpdateGenesView();
                }
            }
            manager.GeneListRequest.Dispose();
            manager.GeneListRequest = null;
            InterfaceManager.stopLoading();
        }

        public IEnumerator querySelectedCells(List<Cell> allcells, DataSet AttachedDataset)
        {
            InterfaceManager.setLoading();
            //MorphoDebug.Log("querySelectedCells "+allcells.Count());
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            string allcellsS = "";
            string stage = "";
            foreach (Cell c in allcells)
            {
                string cn = c.getInfos(AttachedDataset.GeneticManager.idxInfoxName);
                if (stage == "")
                    stage = "Stage " + AttachedDataset.DevTable.getStage(c.t);
                //MorphoDebug.Log("Add on " + c.ID + " -> " + cn + "==" + invConvertCellName(cn) + " at " + stage);
                allcellsS += AttachedDataset.GeneticManager.invConvertCellName(cn) + ",";
            }
            form.AddField("getGenes", allcellsS);
            form.AddField("organism_id", AttachedDataset.GeneticManager.organism_id.ToString());
            form.AddField("stage", stage);
            if (AttachedDataset.GeneticManager.PickedCellUnion == AttachedDataset.GeneticManager.activePickedCell)
                form.AddField("request_type", "union");
            if (AttachedDataset.GeneticManager.PickedCellIntersection == AttachedDataset.GeneticManager.activePickedCell)
                form.AddField("request_type", "intersection");
            if (AttachedDataset.GeneticManager.PickedCellDifferent == AttachedDataset.GeneticManager.activePickedCell)
                form.AddField("request_type", "difference");
            AttachedDataset.GeneticManager.GeneListRequest = UnityWebRequests.Post(MorphoTools.GetServer(), "api/geneforcellsbytype/", form);
            yield return AttachedDataset.GeneticManager.GeneListRequest.SendWebRequest();
            if (AttachedDataset.GeneticManager.GeneListRequest.result == UnityWebRequest.Result.ConnectionError || AttachedDataset.GeneticManager.GeneListRequest.result == UnityWebRequest.Result.ProtocolError)
            {
                MorphoDebug.Log("Error : " + AttachedDataset.GeneticManager.GeneListRequest.error);
            }
            else
            {
                if (AttachedDataset.GeneticManager.GeneListRequest.downloadHandler.text != "")
                {
                    var N = JSONNode.Parse(AttachedDataset.GeneticManager.GeneListRequest.downloadHandler.text);
                    //MorphoDebug.Log(N.ToString());
                    AttachedDataset.GeneticManager.ShowGenes = new Dictionary<int, int>();
                    if (N != null)
                        for (int i = 0; i < N.Count; i++)
                        {
                            string ns = N[i].ToString().Trim();
                            int gene_id = int.Parse(ns.Trim('"'));
                            AttachedDataset.GeneticManager.ShowGenes[gene_id] = 1;
                        }
                    AttachedDataset.GeneticManager.UpdateGenesView();
                }
            }
            AttachedDataset.GeneticManager.GeneListRequest.Dispose();
            AttachedDataset.GeneticManager.GeneListRequest = null;
            InterfaceManager.stopLoading();
        }

        public IEnumerator queryNbCells(DataSet AttachedDataset)
        {
            InterfaceManager.setLoading();
            if (AttachedDataset.start_stage == null)
                AttachedDataset.DevTable.updateDatasetStages();
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("numberofcells", "ok");
            form.AddField("organism_id", AttachedDataset.GeneticManager.organism_id.ToString());
            form.AddField("start_stage", AttachedDataset.start_stage);
            form.AddField("end_stage", AttachedDataset.end_stage);
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/numberofcell/?organism_id=" + AttachedDataset.GeneticManager.organism_id.ToString() + "&start_stage=" + AttachedDataset.start_stage + "&end_stage=" + AttachedDataset.end_stage);
            //MorphoDebug.Log (Instantiation.urlAniseed + "?numberofcells=ok&organism_id=" + organism_id.ToString () + "&start_stage=" + Instantiation.start_stage + "&end_stage=" + Instantiation.end_stage);
            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                MorphoDebug.Log("Error : " + www.error);
            }
            else
            {
                //MorphoDebug.Log ("queryNbCells="+www.downloadHandler.text);
                var N = JSONNode.Parse(www.downloadHandler.text);
                if (N != null)
                {
                    //MorphoDebug.Log(N);
                    Dictionary<int, int> NbCells = new Dictionary<int, int>();
                    for (int i = 0; i < N.Count; i++)
                    {
                        NbCells[int.Parse(N[i]["gene"]["id"].ToString().Replace('"', ' ').Trim())] = int.Parse(N[i]["nb"].ToString().Replace('"', ' ').Trim());
                    }
                    foreach (var gn in AttachedDataset.GeneticManager.Genes)
                    {
                        Gene g = gn.Value;
                        if (NbCells.ContainsKey(g.id))
                            g.setNbCells(NbCells[g.id]);
                        else
                            g.setNbCells(0);
                    }
                    NbCells.Clear();
                    NbCells = null;
                }
            }
            InterfaceManager.stopLoading();
        }

        public IEnumerator requestAllGenes(DataSet AttachedDataset)
        {
            //MorphoDebug.Log("requestAllGenes ");
            InterfaceManager.setLoading();

            //if (SetsManager.instance.API_Menu == "Genetic") hideGenesBar(false);

            if (AttachedDataset.start_stage == null || AttachedDataset.start_stage == "Stage " || AttachedDataset.start_stage == "Stage" || AttachedDataset.end_stage == null || AttachedDataset.end_stage == "Stage " || AttachedDataset.end_stage == "Stage")
            {
                AttachedDataset.DevTable.updateDatasetStages();
            }
            WWWForm form = new WWWForm();
            form.AddField("organism_id", AttachedDataset.GeneticManager.organism_id.ToString());
            //MorphoDebug.Log("start_stage : " + AttachedDataset.start_stage);
            form.AddField("start_stage", AttachedDataset.start_stage);
            //MorphoDebug.Log("end_stage : " + AttachedDataset.end_stage);
            form.AddField("end_stage", AttachedDataset.end_stage);
            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/requestallgenes/", form);
            // MorphoDebug.Log("api/requestallgenes/?organism_id=" + AttachedDataset.genetic_manager.organism_id.ToString() + "&start_stage=" + AttachedDataset.start_stage + "&end_stage=" + AttachedDataset.end_stage);
            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                MorphoDebug.Log("Error RequestAllGenes : " + www.error);
            else
            {
                //MorphoDebug.Log("RequestAllGenes found : " + www.downloadHandler.text);
                var N = JSONNode.Parse(www.downloadHandler.text.Trim('"'));
                //MorphoDebug.Log(N);
                if (AttachedDataset.GeneticManager.Genes != null && AttachedDataset.GeneticManager.Genes.Count > 0)
                { //We first have to clear the previous list
                    foreach (var gn in AttachedDataset.GeneticManager.Genes)
                    {
                        Gene g = gn.Value;
                        DestroyImmediate(g.go);
                    }
                }
                AttachedDataset.GeneticManager.Genes = new Dictionary<string, Gene>();
                if (N != null && N.Count > 0)
                {
                    //MorphoDebug.Log("Found : " + N.Count+" genes");
                    for (int i = 0; i < N.Count; i++)
                    {
                        //MorphoDebug.Log("Found gene : " + N[i]);
                        int nbCells = UtilsManager.convertJSONToInt(N[i]["nb"]);
                        if (nbCells > 0)
                        {
                            //MorphoDebug.Log("Gene is related to 1+ cell");
                            string unique_id = N[i]["unique_gene_id"].ToString();
                            unique_id = unique_id.Substring(1, unique_id.Length - 2);
                            //MorphoDebug.Log("The gene id : " + unique_id);
                            string name = N[i]["gene_name"].ToString();
                            name = name.Substring(1, name.Length - 2);
                            //MorphoDebug.Log("Name : " + name);
                            Gene g = new Gene(int.Parse(N[i]["id"]), name, unique_id, AttachedDataset.GeneticManager);
                            //MorphoDebug.Log("Init the gene");
                            g.init(i);
                            AttachedDataset.GeneticManager.Genes[name] = g;
                            //MorphoDebug.Log("setNbCells");
                            g.setNbCells(nbCells);
                            if (SetsManager.instance.API_Genes != null && SetsManager.instance.API_Genes.Contains(unique_id))
                            {
                                //MorphoDebug.Log("Menu genetic is open, you can active gene");
                                //g.active();//StartCoroutine (Aniseed.queryGeneAnissed (g));
                                g.go.transform.Find("selection").gameObject.GetComponent<Toggle>().isOn = true;
                            }
                        }
                    }
                }
                AttachedDataset.GeneticManager.updateScrol(AttachedDataset.GeneticManager.Genes.Count);
            }
            www.Dispose();
            AttachedDataset.GeneticManager.showPickedOptions(false);
            AttachedDataset.GeneticManager.byCells.SetActive(true);
            InterfaceManager.stopLoading();
            // StartCoroutine(queryNbCells());
        }

        #endregion Genetic

        #region Informations

        public IEnumerator uploadInfos(string morpho, string name, string datatype, DataSetInformations source)
        {
            if (LoadParameters.instance.id_dataset == 0)
            {
                SetsManager.instance.directPlot.CreateInfo(morpho, name, datatype);
            }
            else
            {
                //MorphoDebug.Log("Uploading info : " + name + " type : " + datatype + " with text : " + morpho);
                WWWForm form = new WWWForm();
                form.AddField("hash", SetsManager.instance.hash);
                // form.AddField("id_people", SetsManager.instance.IDUser.ToString());
                form.AddField("infos", name);
                form.AddField("type", "3");
                form.AddField("datatype", datatype);
                form.AddField("id_dataset", source.AttachedDataset.id_dataset.ToString());
                form.AddBinaryData("r_field", UtilsManager.Zip(System.Text.Encoding.UTF8.GetBytes(morpho)));
                UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/createcorrespondence/", form);
                if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
                {
                    www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
                }
                /*UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer() + "api/createcorrespondence/?hash=" + SetsManager.instance.hash + "&id_people=" + SetsManager.instance.IDUser.ToString() + "&infos=" + name + "&type=3&datatype=" + datatype + "&id_dataset=" + AttachedDataset.id_dataset.ToString() + "&field=" + UtilsManager.Zip(System.Text.Encoding.UTF8.GetBytes(morpho)));*/
                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                    MorphoDebug.Log("Error : " + www.error);
                else
                {
                    int id_infos_upload;
                    if (int.TryParse(Encoding.UTF8.GetString(www.downloadHandler.data), out id_infos_upload))
                    {
                        CorrespondenceType ct = source.getCorrespondenceType(datatype);
                        string uploaddate = DateTime.Now.ToString();
                        Correspondence co = new Correspondence(source.AttachedDataset.id_dataset, ct, SetsManager.instance.IDUser, 2, id_infos_upload, name, "", 1, true, datatype, uploaddate, 0, source);
                        ct.addCorrespondence(co);
                    }
                    source.Refresh();
                    source.reOrganize();
                }
                www.Dispose();
            }
        }

        public IEnumerator downloadDataBundle(Correspondence cor, bool continu, DataSetInformations source)
        {
            //MorphoDebug.Log("Download Correspondence  from Bundle");
            while (!Caching.ready)
                yield return null;
            //string url = MorphoTools.GetServer() + cor.link;
            string url = "";
            bool distant = false;
            if (source.AttachedDataset.alternativ_server_adress != "" && source.AttachedDataset.alternativ_server_adress != null)
            {
                distant = true;
                url = source.AttachedDataset.alternativ_server_adress + "/" + cor.link;
            }
            else
            {
                url = MorphoTools.GetServer() + cor.link;
            }
            url = url.Replace("/var/www/html/", "");
            if (source.AttachedDataset.bundleType != "WebGL")
                url = url.Replace("WebGL", source.AttachedDataset.bundleType);

            UnityWebRequest www_FBX = UnityWebRequestAssetBundles.GetAssetBundle(url, "", cor.version, 0);
            if (distant == true)
            {
                MorphoDebug.Log("Access-Control-Allow-Origin : " + "*");
                www_FBX.SetRequestHeader("Access-Control-Allow-Origin", "*");
            }
            //ProgressBar.addWWW (www_FBX,cor.name);
            yield return www_FBX.SendWebRequest();
            if (www_FBX.result == UnityWebRequest.Result.ConnectionError || www_FBX.result == UnityWebRequest.Result.ProtocolError)
            {
                //If distant server, search data inside morphonet.org server
                if (distant == true)
                {
                    www_FBX.Dispose();
                    url = MorphoTools.GetServer() + cor.link;
                    url = url.Replace("/var/www/html/", "");
                    if (source.AttachedDataset.bundleType != "WebGL")
                        url = url.Replace("WebGL", source.AttachedDataset.bundleType);
                    UnityWebRequest www_FBX2 = UnityWebRequestAssetBundles.GetAssetBundle(url, "", cor.version, 0);
                    yield return www_FBX2.SendWebRequest();
                    if (www_FBX2.result == UnityWebRequest.Result.ConnectionError || www_FBX2.result == UnityWebRequest.Result.ProtocolError)
                    {
                        MorphoDebug.LogError("Erreur DL FBX2 " + www_FBX2.error + " from " + url);
                    }
                    else
                    {
                        AssetBundle bundle_cor = ((DownloadHandlerAssetBundle)www_FBX2.downloadHandler).assetBundle;
                        string[] AssetNames = bundle_cor.GetAllAssetNames();

                        //MorphoDebug.Log ("-> Load   " + AssetNames [0] + ":" + AssetNames.Count ());
                        TextAsset corT = (TextAsset)bundle_cor.LoadAsset(AssetNames[0], typeof(TextAsset));
                        cor.startIndex = 0;
                        cor.parseTuple(corT.text, cor.name);
                        Resources.UnloadUnusedAssets();
                        bundle_cor.Unload(false);
                        bundle_cor = null;
                        //cor.active();
                    }
                    www_FBX2.Dispose();
                }
                else
                {
                    MorphoDebug.LogError("Erreur DL FBX " + www_FBX.error + " from " + url);
                }

                //setComment("ERROR Download Correspondence from " + url);
            }
            else
            {
                //MorphoDebug.Log("OK Download Correspondence  from "+ url);
                AssetBundle bundle_cor = ((DownloadHandlerAssetBundle)www_FBX.downloadHandler).assetBundle;
                string[] AssetNames = bundle_cor.GetAllAssetNames();

                //MorphoDebug.Log ("-> Load   " + AssetNames [0] + ":" + AssetNames.Count ());
                TextAsset corT = (TextAsset)bundle_cor.LoadAsset(AssetNames[0], typeof(TextAsset));
                cor.startIndex = 0;
                cor.parseTuple(corT.text, cor.name);
                Resources.UnloadUnusedAssets();
                bundle_cor.Unload(false);
                bundle_cor = null;
            }
            www_FBX.Dispose();
            if (continu)
                source.startdownloadDATA();
        }

        public IEnumerator start_Download_ID_Correspondences(int id_correspondence, DataSetInformations source)
        {
            //MorphoDebug.Log ( "Download Correspondence Type ");
            WWWForm form = new WWWForm();
            form.AddField("id_correspondence", id_correspondence.ToString());
            form.AddField("id_dataset", source.AttachedDataset.id_dataset.ToString());
            //form.AddField("id_people", SetsManager.instance.IDUser.ToString());
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/partialcorrespondence/?id_correspondence=" + id_correspondence.ToString() + "&id_dataset=" + source.AttachedDataset.id_dataset.ToString());
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }
            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                //MorphoDebug.Log ("www.downloadHandler.text=" + www.downloadHandler.text);
                if (www.downloadHandler.text != "")
                {
                    var N = JSONNode.Parse(www.downloadHandler.text);
                    if (N != null && N.Count > 0)
                    {
                        MorphoDebug.Log("Found " + N.Count + " infos");
                        for (int i = 0; i < N.Count; i++)
                        { //Number of Infos(as id,infos,field
                          //MorphoDebug.Log("N->"+N[i].ToString());
                            int id_infos = int.Parse(N[i]["id"].ToString().Replace('"', ' ').Trim());
                            int id_owner = int.Parse(N[i]["id_people"].ToString().Replace('"', ' ').Trim());
                            string infoname = N[i]["infos"].ToString().Replace('"', ' ').Trim();
                            string link = N[i]["link"].ToString().Replace('"', ' ').Trim();
                            uint version = uint.Parse(N[i]["version"].ToString().Replace('"', ' ').Trim());
                            int type = int.Parse(N[i]["type"].ToString().Replace('"', ' ').Trim());
                            int common = int.Parse(N[i]["type"].ToString().Replace('"', ' ').Trim());
                            string datatype = N[i]["datatype"].ToString().Replace('"', ' ').Trim();
                            string uploaddate = N[i]["date"].ToString().Replace('"', ' ').Trim();
                            CorrespondenceType ct = source.getCorrespondenceType(datatype);
                            Correspondence co = new Correspondence(source.AttachedDataset.id_dataset, ct, id_owner, type, id_infos, infoname, link, version, link == "null", datatype, uploaddate, common, source);
                            ct.addCorrespondence(co);
                        }
                    }
                }
                else
                    MorphoDebug.Log("Nothing ...");
            }
            www.Dispose();
            source.reOrganize();
        }

        public IEnumerator start_downloadCommonDataset(DataSetInformations infos)
        {
            //MorphoDebug.Log ("start_downloadCommonDataset");
            WWWForm form = new WWWForm();
            form.AddField("id_dataset", infos.AttachedDataset.id_dataset);
            form.AddField("id_dataset_type", infos.AttachedDataset.id_type.ToString());
            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/commondataset/", form);
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
                MorphoDebug.Log("ERROR :" + www.error);
            else
            {
                if (www.downloadHandler.text != "")
                {
                    var N = JSONNode.Parse(www.downloadHandler.text);
                    if (N != null && N.Count > 0)
                    {
                        //MorphoDebug.Log("Found " + N.Count+ " dataset");
                        List<string> m_DropOptions = new List<string>();
                        infos.common_id_dataset = new List<int>();
                        infos.datasetNames = new Dictionary<int, string>();
                        m_DropOptions.Add("Choose a dataset");
                        infos.common_id_dataset.Add(-1);
                        for (int i = 0; i < N.Count; i++)
                        { //Number of Infos(as id,infos,field
                          //MorphoDebug.Log("N->"+N[i].ToString());
                            if (int.Parse(N[i]["id"]) != MorphoTools.GetDataset().id_dataset)
                            {
                                m_DropOptions.Add(N[i]["name"]);
                                infos.common_id_dataset.Add(int.Parse(N[i]["id"]));
                                infos.datasetNames[int.Parse(N[i]["id"])] = N[i]["name"];
                            }
                        }
                        Dropdown datasets = infos.MenuCommon.transform.Find("datasets").GetComponent<Dropdown>();
                        datasets.ClearOptions();
                        datasets.AddOptions(m_DropOptions);
                    }
                }
                else
                    MorphoDebug.Log("Nothing ...");
            }
            www.Dispose();
        }

        public IEnumerator listInfosFromDataset(int id_dataset, string whichDropdown)
        {
            DataSetInformations infos = MorphoTools.GetInformations();
            //MorphoDebug.Log ("listInfosFromDataset from "+id_dataset+" for "+whichDropdown);
            if (whichDropdown == "thisbridge")
            {
                if (infos.CorrespondenceThisBridge == null)
                    infos.CorrespondenceThisBridge = new List<CorrespondenceOtherData>();
                else
                    infos.CorrespondenceThisBridge.Clear();
            }
            if (whichDropdown == "otherbridge")
            {
                if (infos.CorrespondenceOtherBridge == null)
                    infos.CorrespondenceOtherBridge = new List<CorrespondenceOtherData>();
                else
                    infos.CorrespondenceOtherBridge.Clear();
            }
            if (whichDropdown == "otherinfos")
            {
                if (infos.CorrespondenceInfos == null)
                    infos.CorrespondenceInfos = new List<CorrespondenceOtherData>();
                else
                    infos.CorrespondenceInfos.Clear();
            }

            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("download", "1");
            form.AddField("id_dataset", id_dataset.ToString());
            //form.AddField("id_people", SetsManager.instance.IDUser.ToString());
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/correspondence/?id_dataset=" + id_dataset.ToString());
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                //MorphoDebug.Log ("www.downloadHandler.text=" + www.downloadHandler.text);
                if (www.downloadHandler.text != "")
                {
                    var N = JSONNode.Parse(www.downloadHandler.text);
                    if (N != null && N.Count > 0)
                    {
                        MorphoDebug.Log("Found " + N.Count + " infos");
                        List<string> m_DropOptions = new List<string>();
                        if (whichDropdown == "thisbridge")
                            m_DropOptions.Add("Choose a bridge for this dataset");
                        if (whichDropdown == "otherbridge")
                            m_DropOptions.Add("Choose a bridge for the other dataset");
                        if (whichDropdown == "otherinfos")
                            m_DropOptions.Add("Choose a infos");
                        for (int i = 0; i < N.Count; i++)
                        { //Number of Infos(as id,infos,field
                          //MorphoDebug.Log("N->"+N[i].ToString());
                            int id_infos = int.Parse(N[i]["id"].ToString().Replace('"', ' ').Trim());
                            int id_owner = int.Parse(N[i]["id_people"].ToString().Replace('"', ' ').Trim());
                            string infoname = N[i]["infos"].ToString().Replace('"', ' ').Trim();
                            string link = N[i]["link"].ToString().Replace('"', ' ').Trim();
                            uint version = uint.Parse(N[i]["version"].ToString().Replace('"', ' ').Trim());
                            int type = int.Parse(N[i]["type"].ToString().Replace('"', ' ').Trim());
                            string datatype = N[i]["datatype"].ToString().Replace('"', ' ').Trim();
                            string uploaddate = N[i]["date"].ToString().Replace('"', ' ').Trim();
                            if ((whichDropdown == "thisbridge" || whichDropdown == "otherbridge") && (datatype == "string" || datatype == "text"))
                            {
                                CorrespondenceOtherData co = new CorrespondenceOtherData(id_dataset, id_owner, type, id_infos, infoname, link, version, link == "null", datatype, uploaddate, i);
                                if (whichDropdown == "thisbridge")
                                    infos.CorrespondenceThisBridge.Add(co);
                                if (whichDropdown == "otherbridge")
                                    infos.CorrespondenceOtherBridge.Add(co);
                                m_DropOptions.Add(co.name);
                            }
                            if (whichDropdown == "otherinfos" && (datatype == "string" || datatype == "float" || datatype == "selection" || datatype == "label"|| datatype == "text" || datatype == "number"))
                            {
                                CorrespondenceOtherData co = new CorrespondenceOtherData(id_dataset, id_owner, type, id_infos, infoname, link, version, link == "null", datatype, uploaddate, i);
                                m_DropOptions.Add(co.name);
                                infos.CorrespondenceInfos.Add(co);
                            }
                        }
                        infos.MenuCommon.transform.Find(whichDropdown).gameObject.SetActive(true);
                        Dropdown dd = infos.MenuCommon.transform.Find(whichDropdown).GetComponent<Dropdown>();
                        dd.value = 0;
                        dd.ClearOptions();
                        dd.AddOptions(m_DropOptions);
                    }
                }
                else
                    MorphoDebug.Log("Nothing ...");
            }
            www.Dispose();
        }

        public IEnumerator uploadInfosUpdate(string morpho, Correspondence cor, DataSetInformations source)
        {
            WWWForm form = new WWWForm();
            form.AddField("id_infos", cor.id_infos.ToString());
            form.AddField("id_dataset", source.AttachedDataset.id_dataset.ToString());
            form.AddBinaryData("new_info", UtilsManager.Zip(System.Text.Encoding.UTF8.GetBytes(morpho)));
            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/updatecorrespondence/", form);
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }
            /*UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer() + "api/createcorrespondence/?hash=" + SetsManager.instance.hash + "&id_people=" + SetsManager.instance.IDUser.ToString() + "&infos=" + name + "&type=3&datatype=" + datatype + "&id_dataset=" + AttachedDataset.id_dataset.ToString() + "&field=" + UtilsManager.Zip(System.Text.Encoding.UTF8.GetBytes(morpho)));*/
            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                MorphoDebug.Log("Error : " + www.error);
                MorphoDebug.Log("Error during information update",1);
            }
            else
            {
                //MorphoDebug.Log(www.result.ToString());
                MorphoDebug.Log("Information update successful",1);
            }
            www.Dispose();
        }

        #endregion Informations

        #region Transformations

        //Download Rotation, Position, Scale at specific time point
        public IEnumerator downloadPositions(DataSet dataset)
        {
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/datasetpositions/?id_dataset=" + dataset.id_dataset + "&hash=" + SetsManager.instance.hash);
            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                MorphoDebug.Log("Error positions : " + www.error);
            else
            {
                if (www.downloadHandler.text != "")
                {
                    var N = JSONNode.Parse(www.downloadHandler.text);
                    if (N != null && N.Count > 0)
                    {
                        CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                        ci.NumberFormat.CurrencyDecimalSeparator = ".";
                        for (int i = 0; i < N.Count; i++)
                        {
                            //MorphoDebug.Log("Position : ");
                            //MorphoDebug.Log(N[i].ToString()) ;
                            int t = int.Parse(N[i]["t"].ToString().Replace('"', ' ').Trim());
                            string[] rot = UtilsManager.convertJSON(N[i]["rotation"]).Replace('(', ' ').Replace(')', ' ').Trim().Split(',');
                            if (rot.Length == 4)
                            {
                                dataset.TransformationsManager.RotationAt[t] = new Quaternion(float.Parse(rot[0], NumberStyles.Any, ci), float.Parse(rot[1], NumberStyles.Any, ci), float.Parse(rot[2], NumberStyles.Any, ci), float.Parse(rot[3], NumberStyles.Any, ci));
                            }
                            string[] trans = UtilsManager.convertJSON(N[i]["translation"]).Replace('(', ' ').Replace(')', ' ').Trim().Split(',');
                            if (trans.Length == 3)
                            {
                                dataset.TransformationsManager.TranslationAt[t] = new Vector3(float.Parse(trans[0], NumberStyles.Any, ci), float.Parse(trans[1], NumberStyles.Any, ci), float.Parse(trans[2], NumberStyles.Any, ci));
                            }

                            string[] sca = UtilsManager.convertJSON(N[i]["scale"]).Replace('(', ' ').Replace(')', ' ').Trim().Split(',');
                            if (sca.Length == 3)
                            {
                                dataset.TransformationsManager.ScaleAt[t] = new Vector3(float.Parse(sca[0], NumberStyles.Any, ci), float.Parse(sca[1], NumberStyles.Any, ci), float.Parse(sca[2], NumberStyles.Any, ci));
                            }
                        }
                    }
                }
            }
            www.Dispose();
        }

        #endregion Transformations

        #region Load_Mesh

        public Vector3 parseCenter(string cent)
        {
            // MorphoDebug.Log ("Found Center " + cent);
            if (cent == "")
                return Vector3.zero;
            string[] cents = cent.Split(',');
            if (cents.Length != 3)
                return Vector3.zero;
            float x, y, z;
            CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            ci.NumberFormat.CurrencyDecimalSeparator = ".";

            if (float.TryParse(cents[0], NumberStyles.Any, ci, out x) && float.TryParse(cents[1], NumberStyles.Any, ci, out y) && float.TryParse(cents[2], NumberStyles.Any, ci, out z))
            {
                //MorphoDebug.Log("Try parse parse center reussi ! ");
                return new Vector3(x, y, z);
            }

            return Vector3.zero;
        }

        public IEnumerator start_Download_Mesh_Type()
        {
            
            bool found_mesh = false;
            MorphoTools.GetDataset().mesh_list = new Dictionary<int, List<MeshType>>(); //TODO WE CAN DELETE THIS 2 VARIABLES AT THE END OF THE LOADING PROCESS
            MorphoDebug.Log("Wait during the streaming of the dataset",1);

            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/mesh/?id_dataset=" + MorphoTools.GetDataset().id_dataset.ToString() + "&quality=" + MorphoTools.GetDataset().Quality.ToString() + "&hash=" + SetsManager.instance.hash);

            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                MorphoDebug.Log("Error : " + www.error,1);
            }
            else if (www.downloadHandler.text != "")
            {
                var N = JSONNode.Parse(www.downloadHandler.text);
                SetsManager.instance.TotalNumberOfMesh = N.Count;
                DataSet dataset = MorphoTools.GetDataset();
                Vector3 embryoCenter = dataset.embryoCenter = Vector3.zero;

                if (N != null && SetsManager.instance.TotalNumberOfMesh > 0)
                {
                    MorphoDebug.Log("Found " + SetsManager.instance.TotalNumberOfMesh + " meshes",1);
                    int nbCenters = 0;

                    for (int i = 0; i < SetsManager.instance.TotalNumberOfMesh; i++)
                    {
                        //Info in each node:  id ; t ; link ; isnull(obj) ; version ; center ; name

                        int meshId = int.Parse(N[i]["id"].ToString().Replace('"', ' ').Trim());
                        int time = int.Parse(N[i]["t"].ToString().Replace('"', ' ').Trim());

                        if (time >= dataset.MinTime && time <= dataset.MaxTime)
                        {
                            string link = N[i]["link"].ToString().Replace('"', ' ').Trim();

                            if (dataset.bundleType != "WebGL")
                                link = link.Replace("WebGL", dataset.bundleType);

                            Vector3 centerT = parseCenter(N[i]["center"].ToString().Replace('"', ' ').Trim());
                            string channel_name = N[i]["channel"].ToString().Replace('"', ' ').Trim();

                            embryoCenter += centerT;
                            nbCenters += 1;

                            uint version = 1;
                            string meshName = "name";

                            if (N[i]["name"] != null || N[i]["name"] != "")
                            {
                                meshName = UtilsManager.convertJSON(N[i]["name"]);
                            }

                            MeshType meshType = new MeshType(link, version, link.ToLower() == "null", meshId, time, meshName, channel_name);

                            if (!dataset.mesh_list.ContainsKey(time))
                                dataset.mesh_list[time] = new List<MeshType>();

                            dataset.mesh_list[time].Add(meshType);
                        }
                    }
                    found_mesh = true;
                    MorphoTools.GetDataset().embryoCenter = embryoCenter / nbCenters;
                    SetsManager.instance.nbMeshesDownloaded = 0;
                    SetsManager.instance.nbMeshesLoaded = 0;

                    ProgressBar.addMeshes();

                    downloadNextMesh(0);
                   
                }
                else
                {
                   
                    MorphoDebug.Log("No Mesh associated for this dataset , trying again in a few seconds",1);

                }
            }
            else
            {
                MorphoDebug.Log("No Mesh associated for this dataset",1);
            }

  
            www.Dispose();
            
            if (!found_mesh) // If we didn't find a mesh because of an error , or internet problem , trying again in a few seconds
            {
                yield return new WaitForSeconds(5f);
                StartCoroutine(start_Download_Mesh_Type());
            }
        }

        public void downloadNextMesh(int nextBundle)
        {
            SetsManager.instance.nbMeshesLoaded = SetsManager.instance.nbMeshesDownloaded;

            DataSet dataSet = MorphoTools.GetDataset();

            //MorphoDebug.Log("downloadNextMesh : "+ MorphoTools.get_dataset().LoadingTime+"  next mesh : " + nextBundle);
            //UIManager.instance.setComment("LoadingTime=" + MorphoTools.get_dataset().LoadingTime);

            if (dataSet.mesh_list.ContainsKey(dataSet.LoadingTime))
            {
                if (nextBundle < dataSet.mesh_list[dataSet.LoadingTime].Count)
                {
                    //MESSAGE DURING LAODING
                    string message;
                    float loadingPercent = Mathf.Round(100.0f * nextBundle / dataSet.mesh_list[dataSet.LoadingTime].Count);

                    if (dataSet.MaxTime != dataSet.MinTime)
                    {  //SERVERAL TIME STEP
                        if (dataSet.mesh_list[dataSet.LoadingTime].Count == 1)
                            message = $"Stream time point {dataSet.LoadingTime}";
                        else
                            message = $"Load  {loadingPercent}% at {dataSet.LoadingTime}";
                    }
                    else
                    {
                        if (dataSet.mesh_list[dataSet.LoadingTime].Count == 1)
                            message = "Load this dataset";
                        else
                            message = $"Loading  {loadingPercent}%";
                    }

                    MorphoDebug.Log(message,1);

                    download_OBJorBundle(dataSet.LoadingTime, nextBundle);
                    
                }
                else
                {
                    //MorphoDebug.Log("downloadNextMesh : else : " + nextBundle);
                    StartCoroutine(donwloadNextLoadingMesh());
                }
            }
            else
            {
                //MorphoDebug.Log("downloadNextMesh : no mesh : " + nextBundle);
                MorphoDebug.Log("No mesh specification   at " + dataSet.LoadingTime,1);
                StartCoroutine(donwloadNextLoadingMesh());
            }
            //MorphoDebug.Log("downloadNextMesh : fin : " + nextBundle);
        }

        public IEnumerator Download_Next_Bundle(int time_to_download, int nextBundle)
        {
            //MorphoDebug.Log("DL next bundle " + time_to_download);
            DataSet dataSet = MorphoTools.GetDataset();
            string url = "";
            bool distant = false;
            if (dataSet.alternativ_server_adress != "" && dataSet.alternativ_server_adress != null)
            {
                distant = true;
                url = dataSet.alternativ_server_adress + "/" + dataSet.alternativ_server_path + "/" + dataSet.mesh_list[time_to_download][nextBundle].Link;
            }
            else
            {
                url = MorphoTools.GetServer() + dataSet.mesh_list[time_to_download][nextBundle].Link;
            }
            url = url.Replace("/var/www/html/", "");

            uint version = dataSet.mesh_list[time_to_download][nextBundle].Version;
            while (!Caching.ready)
                yield return null;
            UnityWebRequest www_FBX = UnityWebRequestAssetBundles.GetAssetBundle(url, "", version, 0);
            if (distant == true)
                www_FBX.SetRequestHeader("Access-Control-Allow-Origin", "*");
            yield return www_FBX.SendWebRequest();
            if (www_FBX.result == UnityWebRequest.Result.ConnectionError || www_FBX.result == UnityWebRequest.Result.ProtocolError)
            {
                //MorphoDebug.LogError("Erreur DL FBX " + www_FBX.error + " from " + url);
                if (distant == true)
                {
                    url = MorphoTools.GetServer() + dataSet.mesh_list[time_to_download][nextBundle].Link;
                    url = url.Replace("/var/www/html/", "");
                    www_FBX = UnityWebRequestAssetBundles.GetAssetBundle(url, "", version, 0);
                    yield return www_FBX.SendWebRequest();
                }
            }
            if (www_FBX.result == UnityWebRequest.Result.ConnectionError || www_FBX.result == UnityWebRequest.Result.ProtocolError)
            {
                MorphoDebug.Log("ERROR Download dataset from " + url,1);
            }
            else
            {
                
                AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(www_FBX);
                string[] AssetNames = bundle.GetAllAssetNames();
                //MorphoDebug.Log(" Load Asset " + AssetNames[0]);
                var request = bundle.LoadAssetAsync<GameObject>(AssetNames[0]);
                yield return new WaitUntil(() => request.isDone);

                GameObject goe = Instantiate((GameObject)request.asset);
                LoadMesh(time_to_download, goe, false, dataSet.mesh_list[time_to_download][nextBundle].Name, dataSet.mesh_list[time_to_download][nextBundle].Channel);
                if (SetsManager.instance.meshes_with_textures.Contains(dataSet.mesh_list[time_to_download][nextBundle].Id))
                    StartCoroutine(DataLoader.instance.GetTexture(url.Replace("Bundle/WebGL/", "OBJ/") + ".png", time_to_download, dataSet.mesh_list[time_to_download][nextBundle].Id));
                if (dataSet.mesh_list[dataSet.LoadingTime].Count == 1 || nextBundle == dataSet.mesh_list[dataSet.LoadingTime].Count - 1)
                {
                    MorphoTools.CreateTimePoint(dataSet.LoadingTime);
                }
                //update poses, to avoir small offset during one frame
                dataSet.TransformationsManager.updateDataSetRotation();
                bundle.Unload(true);
                DestroyImmediate(goe, true);
                www_FBX.Dispose();
                if (!SetsManager.MeshDownloaded)
                {
                    
                    InterfaceManager.instance.setTime(time_to_download); //CHANGE TIME DURING DOWNLOAD
                    downloadNextMesh(nextBundle + 1);
                }
                else
                    SetsManager.instance.ReLoadMesh();
            }
            
            SetsManager.instance.nbMeshesDownloaded++;
            // MorphoDebug.Log("DL next bundle : " + "Fin");
        }

        private void OnFinsihedLoading(GameObject obj, int time_to_download, int nextBundle, UnityWebRequest www_FBX, AssetBundle bundle)
        {
        }

        

        /*public void OnDestroy()
        {
            StopAllCoroutines();
        }*/

        public void LoadMesh(int timeToLoad, GameObject goe, bool invShiftX, string name, string channel_name = "", bool reinstantiate = true, List<Cell> new_cells = null)
        {
            TransformationsManager transformationsManager = MorphoTools.GetTransformationsManager();
            DataSet dataset = MorphoTools.GetDataset();
            
            if (transformationsManager != null)
                transformationsManager.resetDataSetRotation();

            if (!dataset.MeshAtTimestampExists(timeToLoad))
            { //First Time we create the embryo at this time ste
                Transform trns = goe.transform;
                Vector3 last_cell = trns.position;
                if (trns.childCount>0)
                    last_cell = trns.GetChild(trns.childCount - 1).gameObject.transform.localPosition;

                if (goe.transform.childCount == 1)
                { //Only One child, usually with no name we rename it
                    GameObject uniqueChild = goe.transform.GetChild(0).gameObject;
                    if (uniqueChild.name == "default")
                        uniqueChild.name = timeToLoad + "," + uniqueChild.name;
                }
                if (reinstantiate)
                    dataset.mesh_by_time[timeToLoad] = Instantiate(goe);//InstantiateObjects(goe);
                else
                    dataset.mesh_by_time[timeToLoad] = goe;
                
                //dataset.mesh_by_time[timeToLoad] = goe;
                dataset.mesh_by_time[timeToLoad].name = name;
                dataset.mesh_by_time[timeToLoad].transform.SetParent(dataset.embryo_container.transform);
                //dataset.mesh_by_time[timeToLoad].transform.localPosition = Vector3.zero; GOE.local
                dataset.mesh_by_time[timeToLoad].transform.localPosition = Vector3.zero;



                Transform trns2 = dataset.mesh_by_time[timeToLoad].transform;
                if (trns.childCount > 0)
                {
                    GameObject go = trns2.GetChild(trns2.childCount - 1).gameObject;
                    if (go.transform.localPosition != last_cell)
                    {
                        go.transform.localPosition = last_cell;
                    }
                }
                
                LoadObjects(dataset.mesh_by_time[timeToLoad], timeToLoad, channel_name);

                float scale = InterfaceManager.instance.initcanvasScale;
                //if not set yet, get mesh bounds
                if (dataset.DatasetBounds.size == Vector3.zero)
                    dataset.DatasetBounds = ComputeMeshBounds(dataset.mesh_by_time[timeToLoad]);
                //compute optimal size for dataset with mesh bounds at first init
                if (dataset.DatasetBounds.size != Vector3.zero)
                {
                    float CanvasWorldX = InterfaceManager.instance.initcanvasScale * InterfaceManager.instance.initCanvasWidth;
                    float CanvasWorldY = InterfaceManager.instance.initcanvasScale * InterfaceManager.instance.initCanvasHeight;
                    scale = Math.Min(CanvasWorldX / dataset.DatasetBounds.size.x, CanvasWorldY / dataset.DatasetBounds.size.y) / 2f;
                    InterfaceManager.instance.initCanvasDatasetScale = scale;
                    MorphoTools.GetRawImages().initCubeScale();
                }
                    

                //dataset.mesh_by_time[timeToLoad].SetActive(false);
                dataset.mesh_by_time[timeToLoad].transform.localScale = new Vector3(scale, scale, scale);
                if (!MorphoTools.GetPlotIsActive())
                {
                    dataset.mesh_by_time[timeToLoad].transform.Translate(
                        dataset.embryoCenter.x * scale,
                        -dataset.embryoCenter.y * scale,
                        -dataset.embryoCenter.z * scale);
                }
        
            }
            else
            { //Already things .... me add the element as a cell
                //GameObject Egoe = InstantiateObjects(goe);
                
                if (goe.GetComponent<MeshRenderer>() != null)
                {
                    goe.GetComponent<MeshRenderer>().enabled = true;
                }

                Transform trns = goe.transform;
                while (trns.childCount > 0)
                {
                    GameObject go = trns.GetChild(0).gameObject;

                    string channelName;

                    if (channel_name != "" && dataset.getChannelString(go.name) == "")
                        channelName = channel_name;
                    else
                        channelName = dataset.getChannelString(go.name);

                    if (channelName == "")
                        channelName = dataset.currentChannel;

                    Cell ce = dataset.getCell(go.name, true);
                    if(new_cells!=null)
                        new_cells.Add(ce);
                    go.transform.SetParent(dataset.mesh_by_time[timeToLoad].transform);

                    go.transform.localPosition = Vector3.zero;
                    Channel cho = dataset.createChannel(channelName); //We have to create a sub menu for each channel
                    ce.addChannel(channelName, go, cho);
                    go.transform.localScale = new Vector3(1f, 1f, 1f);
                    go.transform.localRotation = Quaternion.Euler(0, 0, 0);
                }
                //since the object is now essentially empty, remove it
                Destroy(goe);
            }

            if (transformationsManager != null)
                transformationsManager.updateDataSetRotation();

            //update time step visibility
            dataset.DisplayDataSet(timeToLoad);

        }

        public void LoadObjects(GameObject goe, int t, string channel)
        {
            if (goe != null)
            {
                Component ret = goe.GetComponent<Component>();

                if (ret != null)
                {
                    Transform trns = goe.transform;
                    int nbCell = trns.childCount;

                    for (int c = 0; c < nbCell; c++)
                    {
                        GameObject to = trns.GetChild(c).gameObject;
                        string ch = "0";
                        int ch_i = 0;
                        if (channel != "" && int.TryParse(channel,out ch_i))
                            ch = channel;

                        if (to.name.Length > 5 && to.name.Substring(0, 4) == "cell")
                            //to.name = t + "," + to.name.Substring(5) + ",0"; //OLD VERSION
                            to.name = t + "," + to.name.Substring(5) + ","+ch; //OLD VERSION

                        Cell ce = MorphoTools.GetDataset().getCell(to, true);

                        if (ce != null)
                        {
                            GameObject go = to.gameObject;
                            string channelName = MorphoTools.GetDataset().getChannelString(go.name);

                            if (channelName == "")
                                channelName = channel;

                            Channel cho = MorphoTools.GetDataset().createChannel(channelName); //We have to create a sub menu for each channel
                            ce.addChannel(channelName, go, cho);

                            if (SetsManager.instance.directPlot != null && SetsManager.instance.directPlot.type != "" && SetsManager.instance.directPlot.type == "lpy")
                            {
                                ce.LoadColorSelection(DataSetInformations.instance.getCorrespondenceByName("simulation-color").id_infos);
                            }
                        }
                    }
                }
            }
        }

        public Bounds ComputeMeshBounds(GameObject go)
        {
            Bounds b = new Bounds();
            foreach(Transform t in go.transform)
            {
                if(t.gameObject.TryGetComponent<MeshFilter>(out MeshFilter m))
                {
                    b.Encapsulate(m.mesh.bounds);
                }
            }
            return b;
        }

        public IEnumerator Donwload_OBJ(int timeToLoad, int nextBundle)
        {
            DataSet dataSet = MorphoTools.GetDataset();
            MeshType currentMeshDownload = dataSet.mesh_list[timeToLoad][nextBundle];
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/meshByParams/?hash=" + SetsManager.instance.hash + "&id_dataset=" + dataSet.id_dataset.ToString() + "&quality=" + dataSet.Quality.ToString() + "&t=" + currentMeshDownload.T.ToString() + "&id=" + currentMeshDownload.Id.ToString());
            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                MorphoDebug.Log("Error : " + www.error);
            }
            else
            {
                if (www.downloadHandler.data.Length > 0)
                {
                    string embryoOBJ = Encoding.UTF8.GetString(UtilsManager.Unzip(www.downloadHandler.data));
                    if (embryoOBJ.Contains(PRIMITIVE_PREFIX))
                    {
                        while (!SetsManager.instance.primitive_downloaded)
                        {//Wait for all primitives loaded
                            yield return new WaitForSeconds(1);
                        }
                        createMeshesFromPrimitive(embryoOBJ, currentMeshDownload.Name, timeToLoad);
                    }
                    else
                    {
                        GameObject goe = DataLoader.instance.createGameFromObj(embryoOBJ);
                        if (goe != null)
                        {
                            LoadMesh(timeToLoad, goe, true, currentMeshDownload.Name, currentMeshDownload.Channel);
                            DestroyImmediate(goe, true);
                        }
                    }
                }
                if (dataSet.mesh_list[dataSet.LoadingTime].Count == 1 || nextBundle == dataSet.mesh_list[dataSet.LoadingTime].Count - 1)
                {
                    MorphoTools.CreateTimePoint(dataSet.LoadingTime);
                }
            }
            www.Dispose();
            if (!dataSet.MeshAtTimestampExists(currentMeshDownload.T))
            {
                MorphoTools.GetTransformationsManager().resetDataSetRotation();
                var initcanvasScale = InterfaceManager.instance.initCanvasDatasetScale;
                var dataset = dataSet;
                dataset.mesh_by_time[currentMeshDownload.T] = new GameObject();
                dataset.mesh_by_time[currentMeshDownload.T].name = currentMeshDownload.Name;
                dataset.mesh_by_time[currentMeshDownload.T].SetActive(false);
                dataset.mesh_by_time[currentMeshDownload.T].transform.SetParent(dataset.embryo_container.transform);
                dataset.mesh_by_time[currentMeshDownload.T].transform.localScale = new Vector3(initcanvasScale, initcanvasScale, initcanvasScale);
                dataset.mesh_by_time[currentMeshDownload.T].transform.Translate(
                    dataset.embryoCenter.y * initcanvasScale,
                    -dataset.embryoCenter.x * initcanvasScale,
                    -dataset.embryoCenter.z * initcanvasScale);

                MorphoTools.GetTransformationsManager().updateDataSetRotation();
            }
            if (!SetsManager.MeshDownloaded)
            {
                InterfaceManager.instance.setTime(timeToLoad); //CHANGE TIME DURING DOWNLOAD
                downloadNextMesh(nextBundle + 1);
            }
            else
            {
                SetsManager.instance.ReLoadMesh();
            }

            SetsManager.instance.nbMeshesDownloaded++;
        }

        public void CreateTimePoint(Transform container, Transform content, int force_number=-1)
        {
            DataSet ds = MorphoTools.GetDataset();

            TimePoint tsb = Instantiate(_TimestampBehaviourPrefab, container);
            tsb.Init(content, force_number);

            if (ds.TimePointExists(tsb.Number))
                ds.deleteAt(tsb.Number);

            ds.TimePoints.Add(tsb);
        }

        public void download_OBJorBundle(int time_to_download, int nextBundle)
        {
            SetsManager.instance.CurrentTimeReload = nextBundle + 1;
          
            if (MorphoTools.GetDataset().mesh_list[time_to_download][nextBundle].Obj)
            { //DONWLOAD OBJ FORMAT
                StartCoroutine(Donwload_OBJ(time_to_download, nextBundle));
            }
            else
            {// LINK
                StartCoroutine(Download_Next_Bundle(time_to_download, nextBundle));
            }
            
        }

        public IEnumerator donwloadNextLoadingMesh()
        {
            DataSet currentDataSet = MorphoTools.GetDataset();
            yield return new WaitUntil(() => currentDataSet.allow_time_download);
            //Calculer centre de gravit? (pond?rer) tableau qui stocke le gravity center par pas de temps (pond?ration somme gravity de chaque celulle / le nombre de cellules)

            currentDataSet.LoadingTime++;
            if (currentDataSet.LoadingTime + 1 == SetsManager.instance.API_Time)
                InterfaceManager.instance.setTime(SetsManager.instance.API_Time);

            if (currentDataSet.LoadingTime <= currentDataSet.MaxTime)
            {
                downloadNextMesh(0);
            }
            else
            {
                SetsManager.MeshDownloaded = true;
                Resources.UnloadUnusedAssets();
                System.GC.Collect();
                if (Time.time <= MENU_SHORTCUT_UNLOAD_TIME)
                    Invoke("HideShortcutMenu", MENU_SHORTCUT_UNLOAD_TIME - Time.time);
                else
                    InterfaceManager.instance.MenuShortcuts.SetActive(false);
                MorphoDebug.Log("",1);
                SetsManager.instance.nbMeshesDownloaded = SetsManager.instance.TotalNumberOfMesh; //To Cancel The Progreess Bar
                SetsManager.instance.nbMeshesLoaded = SetsManager.instance.TotalNumberOfMesh;
                InterfaceManager.stopLoading();

                if (InterfaceManager.instance.play_loading != null)
                    InterfaceManager.instance.play_loading.gameObject.SetActive(false);

                if (InterfaceManager.instance.pause_loading != null)
                    InterfaceManager.instance.pause_loading.gameObject.SetActive(false);

                if (SetsManager.instance.API_Time != -1)
                    InterfaceManager.instance.setTime(SetsManager.instance.API_Time);

                if (SetsManager.instance.API_Territory != null && SetsManager.instance.API_Territory != "")
                    currentDataSet.update_Territory(SetsManager.instance.API_Territory, SetsManager.instance.API_Stage);

                SetsManager.instance.TriggerOnMeshDownloaded();

                //init raw for net mode + activate fully
                if (MorphoTools.GetDataset().RawImages != null)
                {
                    if (MorphoTools.GetDataset().RawImages.ShaderParamsInitialized())
                    {
                        MorphoTools.GetDataset().RawImages.SetNetDatasetIsLoaded();
                        MorphoTools.GetDataset().RawImages.showRawImages();
                    }
                    
                }
            }

            if (currentDataSet.MinTime == currentDataSet.MaxTime && LoadParameters.instance.id_dataset != 0)
            {
                InterfaceManager.instance.lineage_button.SetActive(false);
            }
            
        }

        public GameObject ReadSTL(byte[] content)
        {
            GameObject emb = new GameObject();
            MorphoDebug.Log(LoadParameters.instance.STL.Length.ToString());
            var reader = new BinaryReader(new MemoryStream(LoadParameters.instance.STL));
            reader.ReadBytes(80);
            int MaxVertexCount = 64500;
            var trianglesCount = reader.ReadUInt32();

            var vertices = new List<Vector3>();
            var triangles = new List<int>();
            var triangle = 0;
            var items = new Dictionary<string, Vector3>();

            for (var i = 0; i < trianglesCount; i++)
            {
                // the normal
                reader.ReadSingle();
                reader.ReadSingle();
                reader.ReadSingle();

                var vertex1 = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                var vertex2 = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                var vertex3 = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

                vertices.Add(vertex1);
                vertices.Add(vertex2);
                vertices.Add(vertex3);

                triangles.Add(triangle++);
                triangles.Add(triangle++);
                triangles.Add(triangle++);

                reader.ReadUInt16(); // byte count

                if (vertices.Count <= MaxVertexCount - 1)
                {
                    continue;
                }

                GameObject cell = DataLoader.instance.CreateCellGameObject("cell", vertices, triangles);
                cell.transform.SetParent(emb.transform);

                vertices.Clear();
                triangles.Clear();
                items.Clear();
                triangle = 0;
            }

            return emb;
        }

        public GameObject createGameFromStl(string embryoOBJ)
        {
            string[] lines = embryoOBJ.Trim().Split("\n"[0]);
            GameObject emb = new GameObject();

            MorphoDebug.Log(embryoOBJ);
            //Now we look for the face and their group
            string meshname = "";
            bool inside_face = false;
            List<Vector3> vertex_face = new List<Vector3>();
            foreach (string line in lines)
            {
                string trimed_line = line.Replace("  ", " ");
                MorphoDebug.Log(trimed_line);
                if (trimed_line.StartsWith("solid"))
                {
                    meshname = trimed_line.Split(' ')[1];
                }

                if (trimed_line.Contains("facet"))
                {
                    inside_face = true;
                }

                if (trimed_line.Contains("endfacet"))
                {
                    MorphoDebug.Log("endfacet");
                    inside_face = false;
                    vertex_face.Clear();
                }

                if (trimed_line.Contains("endloop"))
                {
                    //parse a cell here
                    MorphoDebug.Log("end face");
                    GameObject cell = CreateCellGameObjectSTL(meshname, vertex_face);
                    cell.transform.SetParent(emb.transform);
                    MorphoDebug.Log("Fin end face");
                    //Set parent of cell to emb.transform
                }

                if (trimed_line.Contains("vertex") && inside_face == true)
                {
                    string[] datas = line.Split(' ');
                    Vector3 tempVector3;
                    CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    ci.NumberFormat.CurrencyDecimalSeparator = ".";

                    if (float.TryParse(datas[1], NumberStyles.Any, ci, out tempVector3.x) && float.TryParse(datas[2], NumberStyles.Any, ci, out tempVector3.y) && float.TryParse(datas[3], NumberStyles.Any, ci, out tempVector3.z))
                    {
                        vertex_face.Add(tempVector3);
                        MorphoDebug.Log(tempVector3.ToString());
                    }
                }

                if (trimed_line.Contains("endsolid"))
                {
                    MorphoDebug.Log("end solid");
                    break;
                }
            }
            return emb;
        }

        public GameObject CreateCellGameObjectSTL(string name, List<Vector3> vertices)
        {
            GameObject cell = new GameObject();
            Mesh mesh = new Mesh();
            mesh.Clear();
            mesh.name = name;
            mesh.vertices = vertices.ToArray();
            ;
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
            cell.AddComponent<MeshFilter>().mesh = mesh;
            cell.AddComponent<MeshRenderer>();

            cell.name = name;
            return cell;
        }

        public GameObject CreateCellGameObject(string name, List<Vector3> vertices, List<int> triangles)
        {
            if (triangles.Max() != vertices.Count - 1)
                MorphoDebug.LogError("The number of faces do not correspond with the number of vertices");
            GameObject cell = GameObject.Instantiate(SetsManager.instance.cell_default);
            int limit = 65000;
            if (vertices.Count > limit)
            { //We have to split the vertices in serveral meshes
                Mesh mesh = new Mesh();

                mesh.Clear();
                mesh.name = name;
                mesh.RecalculateNormals();
                mesh.RecalculateBounds();
                cell.AddComponent<MeshFilter>().mesh = mesh;
                cell.AddComponent<MeshRenderer>();
            }
            else
            {
                Mesh mesh = new Mesh();
                mesh.Clear();
                mesh.name = name;
                mesh.vertices = vertices.ToArray();
                mesh.triangles = triangles.ToArray();
                mesh.RecalculateNormals();
                mesh.RecalculateBounds();
                cell.AddComponent<MeshFilter>().mesh = mesh;
                cell.AddComponent<MeshRenderer>();
            }

            cell.name = name;
            return cell;
        }

        //see http://pastebin.com/7sLteqNw
        public GameObject createGameFromObj(string embryoOBJ)
        {
            //Lire d'abord toutes les vertex et ensuite les assign au mesh cell
            //MorphoDebug.Log("createGameFromObj for " + embryoOBJ);
            string[] lines = embryoOBJ.Trim().Split('\n');
            //First we store all vertx
            Vector3 tempVector3 = Vector3.zero;
            Dictionary<int, Vector3> allVertex = new Dictionary<int, Vector3>();
            int iv = 0;
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i].Trim();
                if (line.Length > 2)
                {
                    if (line[0] == 'v' && line[1] == ' ')
                    {
                        try
                        {
                            while (line.IndexOf("  ") >= 0)
                                line = line.Replace("  ", " "); //Problem of double space
                            string[] datas = line.Split(' ');

                            CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                            ci.NumberFormat.CurrencyDecimalSeparator = ".";

                            if (float.TryParse(datas[1], NumberStyles.Any, ci, out tempVector3.x) && float.TryParse(datas[2], NumberStyles.Any, ci, out tempVector3.y) && float.TryParse(datas[3], NumberStyles.Any, ci, out tempVector3.z))
                            {
                                if (!MorphoTools.GetPlotIsActive())
                                    tempVector3.x = -tempVector3.x; //flip the obj vertices

                                allVertex[iv] = tempVector3;
                            }
                            else if (iv > 0)
                            {
                                allVertex[iv] = allVertex[iv - 1];
                            }
                            else
                            {
                                allVertex[iv] = Vector3.zero;
                            }

                            iv += 1;
                        }
                        catch (System.Exception ex)
                        {
                            MorphoDebug.Log("PARSE ERROR (vertex) : line " + i + " error " + ex);
                        }
                    }
                }
            }

            /*if (allVertex.Count <= 3)
                return null;*/

            GameObject emb = new GameObject();
            emb.transform.localPosition = Vector3.zero;

            //Now we look for the face and their group
            string meshname = "";
            List<int> triangles = new List<int>();

            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i].Replace(" ", "  ").Trim();
                string[] datas = line.Split(' ');
                if (datas.Length > 1)
                {
                    if (line[0] == 'g')
                    {
                        if (meshname != "" && triangles.Count > 0)
                        {
                            int minT = triangles.Min();
                            int maxT = triangles.Max();
                            List<Vector3> vertices = new List<Vector3>();
                            List<int> shiftTriangles = new List<int>();
                            List<int> tVerse = triangles;//reverse triangles because of mesh flip

                            if (!MorphoTools.GetPlotIsActive())
                                tVerse.Reverse();

                            foreach (int f in tVerse)
                                shiftTriangles.Add(f - minT);

                            for (int idf = minT; idf <= maxT; idf++)
                            {
                                if (!allVertex.ContainsKey(idf))
                                    MorphoDebug.LogError("Value  " + idf + " is not in vertex ... ");

                                Vector3 tempVertex = allVertex[idf];

                                vertices.Add(tempVertex);//embryoCenter
                            }

                            GameObject cell = CreateCellGameObject(meshname, vertices, shiftTriangles);

                            cell.transform.SetParent(emb.transform);
                            cell.transform.localPosition = Vector3.zero;
                        }
                        triangles = new List<int>();
                        meshname = datas[datas.Length - 1];
                    }

                    // triangle (face)
                    if (line[0] == 'f')
                    {
                        try
                        {
                            int[] fidx = new int[3];
                            int nbD = datas.Length;
                            int nfi = 0;
                            for (int fi = 0; fi < nbD - 1; fi++)
                            {
                                if (datas[1 + fi].Trim().Length > 0)
                                {
                                    fidx[nfi] = int.Parse(datas[1 + fi].Split('/')[0]);
                                    triangles.Add(fidx[nfi] - 1);
                                    nfi += 1;
                                }
                            }
                        }
                        catch (System.Exception ex)
                        {
                            MorphoDebug.Log("PARSE ERROR (triangle) : line " + i + " (" + line + ") while loading mesh : " + meshname + "\n" + ex);
                        }
                    }
                }
            }

            if (triangles.Count > 0)
            { //We Do the last cell...
                int minT = triangles.Min();
                int maxT = triangles.Max();
                List<Vector3> vertices = new List<Vector3>();
                List<int> shiftTriangles = new List<int>();
                List<int> tVerse = triangles; //reverse triangles because of mesh flip

                if (!MorphoTools.GetPlotIsActive())
                    tVerse.Reverse();

                foreach (int f in tVerse)
                    shiftTriangles.Add(f - minT);

                for (int idf = minT; idf <= maxT; idf++)
                {
                    if (!allVertex.ContainsKey(idf))
                        MorphoDebug.Log("Value  " + idf + " is not in vertex ... ");

                    Vector3 tempVertex = allVertex[idf];

                    vertices.Add(tempVertex);//embryoCenter
                }

                GameObject cellfinal = CreateCellGameObject(meshname, vertices, shiftTriangles);
                cellfinal.transform.SetParent(emb.transform);

                //center obj position at start!!!
                cellfinal.transform.localPosition = Vector3.zero;
            }

            return emb;
        }

        #endregion Load_Mesh

        #region Primitives

        public void createMeshesFromPrimitive(string Primit, string mesh_name, int timetoload)
        {
            GameObject emb = new GameObject();
            if (SetsManager.instance.isTimePrimitive == null)
                SetsManager.instance.isTimePrimitive = new Dictionary<int, bool>();
            SetsManager.instance.isTimePrimitive[timetoload] = true;
            string[] lines = Primit.Trim().Split("\n"[0]);

            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i].Trim();

                if (line[0] == 'p' && line[1] == ' ')
                {
                    string[] datas = line.Split(' ');
                    string name = datas[1];

                    if (datas.Length != 6)
                        MorphoDebug.Log("Error Primitives definition");

                    string primi = datas[2];

                    if (!SetsManager.instance.Primitives.ContainsKey(primi))
                    {
                        MorphoDebug.Log("Miss primitive " + primi);
                    }
                    else
                    {
                        CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                        ci.NumberFormat.CurrencyDecimalSeparator = ".";

                        GameObject goe = SetsManager.instance.GetNextAvailablePrimitive(primi, emb.transform); //(GameObject)InstantiateGO(Primitives[primi], emb.transform);

                        Cell obj = MorphoTools.GetDataset().getCell(name, true);
                        obj.primi = new Primitive(name, primi);
                        goe.name = name;

                        string position = datas[3];
                        string[] positions = position.Substring(1, position.Length - 2).Split(',');

                        goe.transform.position = new Vector3(float.Parse(positions[0], NumberStyles.Any, ci), float.Parse(positions[1], NumberStyles.Any, ci), float.Parse(positions[2], NumberStyles.Any, ci));
                        obj.primi.Position = goe.transform.position;

                        float scale = float.Parse(datas[4], NumberStyles.Any, ci);
                        goe.transform.localScale = new Vector3(scale, scale, scale);
                        obj.primi.Scale = goe.transform.localScale;

                        string rotation = datas[5];
                        string[] rotations = rotation.Substring(1, rotation.Length - 2).Split(',');

                        goe.transform.localRotation = new Quaternion(float.Parse(rotations[0]), float.Parse(rotations[1], NumberStyles.Any, ci), float.Parse(rotations[2], NumberStyles.Any, ci), float.Parse(rotations[3], NumberStyles.Any, ci));
                        obj.primi.Rotation = goe.transform.localRotation;
                    }
                }
            }
            LoadMesh(timetoload, emb, true, mesh_name, "", false);

            Resources.UnloadUnusedAssets();
            System.GC.Collect();
        }

        public IEnumerator Donwload_Primitive(string primitiveName, int id_dataset) //Download a specitif Primitive
        {
            WWWForm form = new WWWForm();
            form.AddField("id_dataset", id_dataset);
            form.AddField("name", primitiveName);
            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/primitiveobj/", form);

            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                MorphoDebug.Log("Error : " + www.error);
            }
            else
            {
                if (www.downloadHandler.data.Length > 0 && www.downloadHandler.data.ToString() != "failed")
                {
                    string primitive_OBJ = Encoding.UTF8.GetString(UtilsManager.Unzip(www.downloadHandler.data));
                    createPrimitive(primitiveName, primitive_OBJ);
                }
                else
                    MorphoDebug.Log("Primivitve Nothing to download for " + primitiveName);
            }
            www.Dispose();
        }

        public void createPrimitive(string primitiveName, string primitive_OBJ)
        {
            GameObject goe_parent = new GameObject();
            GameObject cell;
            Mesh mesh;
            MeshFilter meshFilter = null;
            int limit = 65000;
            List<string> primitivesClassiques = new List<string>();
            primitivesClassiques.Add("Cone");
            primitivesClassiques.Add("Cube");
            primitivesClassiques.Add("Cylinder");
            primitivesClassiques.Add("Sphere");
            primitivesClassiques.Add("Disc");
            primitivesClassiques.Add("frustum0.2");
            primitivesClassiques.Add("frustum0.4");
            primitivesClassiques.Add("frustum0.6");
            primitivesClassiques.Add("frustum0.8");
            primitivesClassiques.Add("frustum1.2");
            primitivesClassiques.Add("frustum1.4");
            primitivesClassiques.Add("frustum1.6");
            primitivesClassiques.Add("frustum1.8");
            primitivesClassiques.Add("frustum2");
            if (primitivesClassiques.Contains(primitiveName))
            {
                switch (primitiveName)
                {
                    case "Cone":
                        meshFilter = SetsManager.instance.coneLPY;
                        break;

                    case "Cube":
                        meshFilter = SetsManager.instance.cubeLPY;
                        break;

                    case "Cylinder":
                        meshFilter = SetsManager.instance.cylinderLPY;
                        break;

                    case "Sphere":
                        meshFilter = SetsManager.instance.sphereLPY;
                        break;

                    case "Disc":
                        meshFilter = SetsManager.instance.discLPY;
                        break;

                    case "frustum0.2":
                        meshFilter = SetsManager.instance.frustum02;
                        break;

                    case "frustum0.4":
                        meshFilter = SetsManager.instance.frustum04;
                        break;

                    case "frustum0.6":
                        meshFilter = SetsManager.instance.frustum06;
                        break;

                    case "frustum0.8":
                        meshFilter = SetsManager.instance.frustum08;
                        break;

                    case "frustum1.2":
                        meshFilter = SetsManager.instance.frustum12;
                        break;

                    case "frustum1.4":
                        meshFilter = SetsManager.instance.frustum14;
                        break;

                    case "frustum1.6":
                        meshFilter = SetsManager.instance.frustum16;
                        break;

                    case "frustum1.8":
                        meshFilter = SetsManager.instance.frustum18;
                        break;

                    case "frustum2":
                        meshFilter = SetsManager.instance.frustum2;
                        break;

                    default:
                        break;
                }
                cell = GameObject.Instantiate(SetsManager.instance.cell_default);
                if (meshFilter.sharedMesh.vertices.Count() > limit)
                { //We have to split the vertices in serveral meshes
                    mesh = new Mesh();
                    mesh.Clear();
                    mesh.name = primitiveName;
                    mesh.RecalculateNormals();
                    mesh.RecalculateBounds();
                    cell.AddComponent<MeshFilter>().mesh = mesh;
                    cell.AddComponent<MeshRenderer>();
                }
                else
                {
                    mesh = new Mesh();
                    mesh.Clear();
                    mesh.name = primitiveName;
                    mesh.vertices = meshFilter.sharedMesh.vertices;
                    mesh.triangles = meshFilter.sharedMesh.triangles;
                    mesh.RecalculateNormals();
                    mesh.RecalculateBounds();
                    cell.AddComponent<MeshFilter>().mesh = mesh;
                    cell.AddComponent<MeshRenderer>();
                }
                cell.name = primitiveName;
                cell.transform.SetParent(goe_parent.transform);
            }
            else
            {
                goe_parent = createGameFromObj(primitive_OBJ); //OBJ
            }
            goe_parent.SetActive(false);
            goe_parent.name = "primitive";
            GameObject goe = goe_parent.transform.GetChild(0).gameObject;
            goe.name = primitiveName;
            if (SetsManager.instance.Primitives == null)
                SetsManager.instance.Primitives = new Dictionary<string, GameObject>();
            SetsManager.instance.Primitives[primitiveName] = goe;
        }

        public IEnumerator Donwload_Primitives(int id_dataset)
        {//Download All Primitivies
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/primitive/?id_dataset=" + id_dataset.ToString());

            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                MorphoDebug.Log("Error : " + www.error);
            }
            else
            {
                var N = JSONNode.Parse(www.downloadHandler.text);
                if (N != null && N.Count > 0)
                {
                    if (SetsManager.instance.Primitives == null)
                        SetsManager.instance.Primitives = new Dictionary<string, GameObject>();
                    for (int i = 0; i < N.Count; i++)
                    {
                        string primi = N[i]["name"].ToString().Replace('"', ' ').Trim();
                        SetsManager.instance.Primitives[primi] = null;
                        StartCoroutine(Donwload_Primitive(primi, id_dataset));
                        MorphoTools.GetDataset().range_time_loading = false; //On deseactive le system de cache si on a des primitives
                    }
                }
            }

            www.Dispose();

            //Wait all primitives are well downloaded
            while (!SetsManager.instance.primitive_downloaded)
            {
                SetsManager.instance.primitive_downloaded = true;

                foreach (KeyValuePair<string, GameObject> primi in SetsManager.instance.Primitives)
                {
                    if (primi.Value == null)
                        SetsManager.instance.primitive_downloaded = false;
                }

                if (!SetsManager.instance.primitive_downloaded)
                    yield return new WaitForSeconds(1);
            }
        }

        #endregion Primitives

        #region Primitves et objets

        public GameObject createMeshesFromPrimitivesAndObj(string PrimitObj, string mesh_name, int timetoload)
        {
            GameObject emb = new GameObject();
            GameObject cell;
            string name, primi, position, scale, rotation, type, forward, up, axis, meshname;
            string[] datas, positions, scales, rotations, forward_vec, up_vec, axis_vec;
            float angle;
            Vector3 tempVector3, tempVertex;
            int iv, maxT, nbD, nfi;
            List<int> tVerse = null;
            int[] fidx = null;
            bool test = false;

            meshname = "";
            tempVector3 = Vector3.zero;
            iv = 0;
            List<int> triangles = new List<int>();
            List<Vector3> vertices = new List<Vector3>();
            Dictionary<int, Vector3> vertex = new Dictionary<int, Vector3>();

            CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            ci.NumberFormat.CurrencyDecimalSeparator = ".";

            if (!SetsManager.instance.Primitives.ContainsKey("Cone"))
            {
                createPrimitive("Cone", "");
                createPrimitive("Cube", "");
                createPrimitive("Cylinder", "");
                createPrimitive("Sphere", "");
                createPrimitive("Disc", "");
                createPrimitive("frustum0.2", "");
                createPrimitive("frustum0.4", "");
                createPrimitive("frustum0.6", "");
                createPrimitive("frustum0.8", "");
                createPrimitive("frustum1.2", "");
                createPrimitive("frustum1.4", "");
                createPrimitive("frustum1.6", "");
                createPrimitive("frustum1.8", "");
                createPrimitive("frustum2", "");
            }

            string[] lines = PrimitObj.Trim().Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                datas = lines[i].Trim().Split(' ');
                if (datas[0] == "p")
                {
                    if (meshname != "" && triangles.Count > 0)
                    {
                        maxT = triangles.Max();
                        vertices.Clear();

                        tVerse = triangles;
                        tVerse.Reverse();
                        for (int idf = 0; idf <= maxT; idf++)
                        {
                            if (!vertex.ContainsKey(idf))
                                MorphoDebug.LogError("Value  " + idf + " is not in vertex ... ");
                            tempVertex = vertex[idf];
                            vertices.Add(tempVertex);
                        }
                        if (triangles.Max() != vertices.Count - 1)
                            MorphoDebug.LogError("The number of faces do not correspond with the number of vertices");

                        cell = CreateCellGameObject(meshname, vertices, tVerse);
                        cell.name = meshname;

                        cell.transform.SetParent(emb.transform);
                        cell.transform.localPosition = Vector3.zero;
                        meshname = "";
                        tempVector3 = Vector3.zero;
                        vertex.Clear();
                        iv = 0;
                        triangles.Clear();
                    }

                    if (SetsManager.instance.isTimePrimitive == null)
                        SetsManager.instance.isTimePrimitive = new Dictionary<int, bool>();
                    SetsManager.instance.isTimePrimitive[timetoload] = true;

                    name = datas[1];

                    primi = datas[2];

                    if (!SetsManager.instance.Primitives.ContainsKey(primi))
                    {
                        MorphoDebug.Log("Miss primitive " + primi);
                    }
                    else
                    {
                        cell = SetsManager.instance.GetNextAvailablePrimitive(primi, emb.transform);

                        Cell obj = MorphoTools.GetDataset().getCell(name, true);
                        if (obj != null)
                            obj.primi = new Primitive(name, primi);

                        cell.name = name;

                        position = datas[3];
                        positions = position.Split(',');
                        cell.transform.localPosition = new Vector3(float.Parse(positions[0], NumberStyles.Any, ci), float.Parse(positions[1], NumberStyles.Any, ci), float.Parse(positions[2], NumberStyles.Any, ci));
                        obj.primi.Position = cell.transform.position;

                        scale = datas[4];
                        scales = scale.Split(',');
                        cell.transform.localScale = new Vector3(float.Parse(scales[0], NumberStyles.Any, ci), float.Parse(scales[1], NumberStyles.Any, ci), float.Parse(scales[2], NumberStyles.Any, ci));
                        obj.primi.Scale = cell.transform.localScale;

                        rotation = datas[5];
                        rotations = rotation.Split(',');
                        if (datas.Length == 6)
                        {
                            cell.transform.localRotation = Quaternion.Euler(float.Parse(rotations[0], NumberStyles.Any, ci), float.Parse(rotations[1], NumberStyles.Any, ci), float.Parse(rotations[2], NumberStyles.Any, ci));
                        }
                        else
                        {
                            type = datas[6];
                            if (type.Contains("oriented"))
                            {
                                forward = datas[7];
                                forward_vec = forward.Split(',');
                                up = datas[8];
                                up_vec = up.Split(',');
                                cell.transform.localRotation = Quaternion.Euler(float.Parse(rotations[0], NumberStyles.Any, ci), float.Parse(rotations[1], NumberStyles.Any, ci), float.Parse(rotations[2], NumberStyles.Any, ci));

                                cell.transform.localRotation *= Quaternion.LookRotation(new Vector3(float.Parse(forward_vec[0], NumberStyles.Any, ci), float.Parse(forward_vec[1], NumberStyles.Any, ci), float.Parse(forward_vec[2], NumberStyles.Any, ci)), new Vector3(float.Parse(up_vec[0], NumberStyles.Any, ci), float.Parse(up_vec[1], NumberStyles.Any, ci), float.Parse(up_vec[2], NumberStyles.Any, ci)));
                            }
                            else
                            {
                                cell.transform.localRotation = Quaternion.Euler(float.Parse(rotations[0], NumberStyles.Any, ci), float.Parse(rotations[1], NumberStyles.Any, ci), float.Parse(rotations[2], NumberStyles.Any, ci));
                                axis = datas[7];
                                axis_vec = axis.Split(',');
                                angle = float.Parse(datas[8], NumberStyles.Any, ci);

                                cell.transform.RotateAround(cell.transform.position, new Vector3(float.Parse(axis_vec[0], NumberStyles.Any, ci), float.Parse(axis_vec[1], NumberStyles.Any, ci), float.Parse(axis_vec[0], NumberStyles.Any, ci)), angle);
                            }
                        }
                        obj.primi.Rotation = cell.transform.localRotation;
                    }
                }
                else if (datas[0] == "g")
                {
                    if (meshname != "")
                    {
                        maxT = triangles.Max();
                        vertices.Clear();

                        tVerse = triangles;
                        tVerse.Reverse();
                        for (int idf = 0; idf <= maxT; idf++)
                        {
                            if (!vertex.ContainsKey(idf))
                                MorphoDebug.LogError("Value  " + idf + " is not in vertex ... ");
                            tempVertex = vertex[idf];
                            vertices.Add(tempVertex);
                        }
                        if (triangles.Max() != vertices.Count - 1)
                            MorphoDebug.LogError("The number of faces do not correspond with the number of vertices");

                        cell = CreateCellGameObject(meshname, vertices, tVerse);
                        cell.name = meshname;

                        cell.transform.SetParent(emb.transform);
                        cell.transform.localPosition = Vector3.zero;

                        meshname = "";
                        tempVector3 = Vector3.zero;
                        vertex.Clear();
                        iv = 0;
                        triangles.Clear();
                    }

                    meshname = datas[1];
                }
                else if (datas[0] == "v")
                {
                    test = true;
                    try
                    {
                        if (float.TryParse(datas[1], NumberStyles.Any, ci, out tempVector3.x) && float.TryParse(datas[2], NumberStyles.Any, ci, out tempVector3.y) && float.TryParse(datas[3], NumberStyles.Any, ci, out tempVector3.z))
                        {
                            vertex[iv] = tempVector3;
                        }
                        else if (iv > 0)
                            vertex[iv] = vertex[iv - 1];
                        else
                            vertex[iv] = Vector3.zero;
                        iv += 1;
                    }
                    catch (System.Exception ex)
                    {
                        MorphoDebug.Log("PARSE ERROR (vertex) : line " + i + " error " + ex);
                    }
                }
                else
                {
                    if (test)
                    {
                        test = false;
                    }
                    try
                    {
                        fidx = new int[3];
                        nbD = datas.Length;
                        nfi = 0;
                        for (int fi = 0; fi < nbD - 1; fi++)
                        {
                            if (datas[1 + fi].Trim().Length > 0)
                            {
                                fidx[nfi] = int.Parse(datas[1 + fi].Split('/')[0]);
                                triangles.Add(fidx[nfi] - 1);
                                nfi += 1;
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        MorphoDebug.Log("PARSE ERROR (triangle) : line " + i + " (" + datas + ") while loading mesh : " + meshname + "\n" + ex);
                    }
                }
            }

            if (meshname != "")
            {
                maxT = triangles.Max();
                vertices.Clear();
                tVerse = triangles;
                tVerse.Reverse();
                for (int idf = 0; idf <= maxT; idf++)
                {
                    if (!vertex.ContainsKey(idf))
                        MorphoDebug.LogError("Value  " + idf + " is not in vertex ... ");
                    tempVertex = vertex[idf];
                    vertices.Add(tempVertex);
                }
                if (triangles.Max() != vertices.Count - 1)
                    MorphoDebug.LogError("The number of faces do not correspond with the number of vertices");

                cell = CreateCellGameObject(meshname, vertices, tVerse);
                cell.name = meshname;

                cell.transform.SetParent(emb.transform);
                cell.transform.localPosition = Vector3.zero;
            }
            return emb;
        }

        #endregion Primitves et objets

        #region RawImages

        //Detect if dataset has raw images; if yes we can give user access to raw images panel
        public IEnumerator Get3DTexture(string url, RawImages images,int channel)
        {
            //images.wbrImages = UnityWebRequests.Get(url, "");
            UnityWebRequest iwr = UnityWebRequests.Get(url, "");
            //ProgressBar.addImages(images.wbrImages);
            ProgressBar.addImages(iwr);

            yield return iwr.SendWebRequest();
            if(iwr.result == UnityWebRequest.Result.ConnectionError || iwr.result == UnityWebRequest.Result.ProtocolError)
            {
                MorphoDebug.Log("Error on Get3DTexture " + iwr.error);
            }
            else
            {
                byte[] bytes = iwr.downloadHandler.data;
                Texture3D t = images.bytesToTex3D(UtilsManager.Unzip(bytes));
                images.SetTempTexture(t, channel);
                //images.setCube(UtilsManager.Unzip(bytes));
                Resources.UnloadUnusedAssets();
                System.GC.Collect();
            }
        }

        public IEnumerator Get3DTextureAssetBundle(string url, RawImages images, int channel)
        {
            //necessary to avoid some bundles not being unloaded on quick moving between time steps
            //AssetBundle.UnloadAllAssetBundles(true);

            Resources.UnloadUnusedAssets();
            bool distant = false;
            if (MorphoTools.GetDataset().alternativ_server_adress != "" && MorphoTools.GetDataset().alternativ_server_adress != null)
            {
                distant = true;
                url = MorphoTools.GetDataset().alternativ_server_adress + "/" + MorphoTools.GetDataset().alternativ_server_path + "/" + url;
            }
            else
            {
                url = MorphoTools.GetServer() + url;
            }
            url = url.Replace("RAWBUNDLE", "RAWBUNDLES");//change symlink ?
            url = url.Replace("/var/www/html/", "");

            //replace webGL with current build from morphonet tools
            if (MorphoTools.GetDataset().bundleType != "WebGL")
                url = url.Replace("WebGL", MorphoTools.GetDataset().bundleType);

            while (!Caching.ready)
                yield return null;
            UnityWebRequest www_raw = UnityWebRequestAssetBundle.GetAssetBundle(url);
            ProgressBar.addImages(www_raw);
            if (url.StartsWith("https"))
                www_raw.certificateHandler = new AcceptAllCertificatesSignedWithASpecificKeyPublicKey();

            if (distant == true)
                www_raw.SetRequestHeader("Access-Control-Allow-Origin", "*");
            yield return www_raw.SendWebRequest();
            if (www_raw.result == UnityWebRequest.Result.ConnectionError)
            {
                if (distant == true)
                {
                    www_raw = UnityWebRequestAssetBundle.GetAssetBundle(url);
                    if (url.StartsWith("https"))
                        www_raw.certificateHandler = new AcceptAllCertificatesSignedWithASpecificKeyPublicKey();
                    yield return www_raw.SendWebRequest();
                }
                MorphoDebug.Log("ERROR Download dataset from " + url,1);
            }
            else
            {
                AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(www_raw);

                string[] AssetNames = bundle.GetAllAssetNames();

                var request = bundle.LoadAssetAsync<Texture3D>(AssetNames[0]);
                yield return new WaitUntil(() => request.isDone);

                Texture3D tex = (Texture3D)request.asset;
                Texture3D copy = new Texture3D(tex.width, tex.height, tex.depth, tex.format, false);
                copy.SetPixels32(tex.GetPixels32());
                copy.Apply();


                images.SetTempTexture(copy, channel);
                //images.setCubeFromTexture(copy);
                Resources.UnloadUnusedAssets();
                System.GC.Collect();

                bundle.Unload(true);
                www_raw.Dispose();
            }
        }

        public IEnumerator containsRawImages(int id_dataset)
        {
            if (id_dataset > 0)
            {
                WWWForm formC = new WWWForm();
                formC.AddField("containsrawimages", id_dataset);
                UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/containsrawimage/", formC);

                yield return www.SendWebRequest();

                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                {
                    MorphoDebug.Log("Error containsRawImages : " + www.error);
                    InterfaceManager.instance.containerImages.SetActive(false);
                }
                else
                {
                    if (www.downloadHandler.text != "")
                    {
                        var N = JSONNode.Parse(www.downloadHandler.text);
                        if (N != null)
                        {
                            int raw_count = int.Parse(N["count"]);
                            //InterfaceManager.instance.containerImages.SetActive(raw_count > 0);
                        }
                        else
                        {
                            InterfaceManager.instance.containerImages.SetActive(false);
                        }
                    }
                    else
                    {
                        InterfaceManager.instance.containerImages.SetActive(false);
                    }
                }
                www.Dispose();
            }
            else
            {
                //InterfaceManager.instance.containerImages.SetActive(true);
            }
        }

        #endregion RawImages

        #region Textures

        public IEnumerator GetTexture(string url, int timeLoaded, int meshid)
        {
            WWWForm formC = new WWWForm();
            formC.AddField("id_mesh", meshid);
            UnityWebRequest www1 = UnityWebRequests.Post(MorphoTools.GetServer(), "api/textureformat/?id_mesh", formC);
            yield return www1.SendWebRequest();
            if (www1.result == UnityWebRequest.Result.ConnectionError || www1.result == UnityWebRequest.Result.ProtocolError)
            { MorphoDebug.Log("Error on Texture " + www1.error); }
            else
            {
                if (www1.downloadHandler.text != "")
                {
                    var N = JSONNode.Parse(www1.downloadHandler.text);

                    if (N != null)
                    {
                        if (N["status"] == "failed")
                        {
                            MorphoDebug.Log("No textures found");
                        }
                        else
                        {
                            string format = N["type"];
                            url = url.Replace(".png", "." + format);
                            UnityWebRequest www = UnityWebRequestTextures.GetTexture(url, "");

                            yield return www.SendWebRequest();

                            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                            {
                                MorphoDebug.Log("Error on Texture " + www.error);
                            }
                            else
                            {
                                Texture texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                                if (texture != null && !www.downloadHandler.text.Contains("<!doctype html>"))
                                {
                                    MorphoTools.GetDataset().mesh_by_time[timeLoaded].AddComponent<MeshRenderer>();
                                    Material mat = MorphoTools.GetDataset().mesh_by_time[timeLoaded].GetComponent<Renderer>().material;
                                    mat.shader = Shader.Find("Standard"); //As to be reinitialise ...
                                    mat.SetTexture("_MainTex", texture);
                                    //We have to refesh the objects shader
                                    foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[timeLoaded])
                                    {
                                        if (cell.Channels != null)
                                            foreach (KeyValuePair<string, CellChannelRenderer> itemc in cell.Channels)
                                                itemc.Value.ShouldBeUpdated = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public IEnumerator MeshesWithTextures(int id_dataset)
        {
            SetsManager.instance.meshes_with_textures = new List<int>();
            WWWForm form = new WWWForm();
            form.AddField("id_dataset", id_dataset);
            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/mesheswithtextures/", form);
            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                MorphoDebug.Log("Error positions : " + www.error);
            else
            {
                if (www.downloadHandler.text != "")
                {
                    var N = JSONNode.Parse(www.downloadHandler.text);
                    if (N != null && N.Count > 0)
                    {
                        CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                        ci.NumberFormat.CurrencyDecimalSeparator = ".";
                        for (int i = 0; i < N["result"].Count; i++)
                        {
                            int t = int.Parse(N["result"][i].ToString().Replace('"', ' ').Trim());
                            if (!SetsManager.instance.meshes_with_textures.Contains(t))
                            {
                                SetsManager.instance.meshes_with_textures.Add(t);
                            }
                        }
                    }
                }
            }
            www.Dispose();
        }

        #endregion Textures

        #region Developmental_table

        public IEnumerator downloadDevelopmentalTable(int id_datasettype)
        {
            DevelopmentTable table = MorphoTools.GetDevelopmentalTable();
            //MorphoDebug.Log("downloadDevelopmentalTable for " + id_datasettype);
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/devtable/?developmental_table=" + id_datasettype.ToString() + "&hash=" + SetsManager.instance.hash);
            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                MorphoDebug.Log("Error : " + www.error + " with " + www.responseCode);
            }
            else
            {
                table.Dvpts[id_datasettype] = new List<Dvpt>();
                //MorphoDebug.Log("FNE downloadDevelopmentalTable " + www.downloadHandler.text);
                if (www.downloadHandler.text != "")
                {
                    var N = JSONNode.Parse(www.downloadHandler.text);
                    for (int i = 0; i < N.Count; i++)
                    {
                        // MorphoDebug.Log (" FNE Add " + i + "->" + N [i].ToString ());
                        string period = N[i]["period"].ToString().Replace('"', ' ').Trim();
                        string stage = N[i]["stage"].ToString().Replace('"', ' ').Trim();
                        string developmentaltstage = N[i]["developmentaltstage"].ToString().Replace('"', ' ').Trim();
                        string description = N[i]["description"].ToString().Replace('"', ' ').Trim();
                        string hpf = N[i]["hpf"].ToString().Replace('"', ' ').Trim();
                        string hatch = N[i]["hatch"].ToString().Replace('"', ' ').Trim();
                        table.Dvpts[id_datasettype].Add(new Dvpt(period, stage, developmentaltstage, description, hpf, hatch));
                    }
                }
            }
            www.Dispose();
            //MorphoDebug.Log("FIN DL development Table : avant update");
            table.updateDatasetStages();
            //MorphoDebug.Log("FIN DL development Table : apres update");
        }

        #endregion Developmental_table

        #region utils

        private void HideShortcutMenu()
        {
            InterfaceManager.instance.MenuShortcuts.SetActive(false);
        }

        #endregion utils
    }
}