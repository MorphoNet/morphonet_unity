﻿using UnityEngine;
using System.Collections.Generic;
using System;
using MorphoNet.Extensions.Unity;
using UnityEngine.UI;
using System.Collections;

namespace MorphoNet
{
    public class CellChannelRenderer
    {
        public Cell cell; //Pointer back on the cell
        public Channel cho; //Associated Channel GameObject Menu

        //Main Mesh for this channel
        public GameObject AssociatedCellObject;

        public MeshFilter MeshFilter;

        private bool _ShouldBeUpdated; //To kown if we have to update the shader

        public bool ShouldBeUpdated
        {
            get => _ShouldBeUpdated;
            set
            {
                _ShouldBeUpdated = value;
                if (_ShouldBeUpdated)
                    cell.SetToUpdate();
            }
        }

        public bool ShouldBeHiglighted;

        //Gravity Center
        public Vector3 Gravity; //GravityCenter
        public Vector3 MeshGravity;

        public GameObject center; //Sphere associate to the gravity position

        //Tracking
        public GameObject vector; //For tracking

        public CellChannelRenderer(GameObject go, Cell cell, Channel cho)
        {
            this.cell = cell;
            this.cho = cho;

            AssociatedCellObject = go;
            Renderer = go.GetComponent<Renderer>();
            MeshFilter = go.GetComponent<MeshFilter>();

            ShouldBeUpdated = true;

            calculGravityCenter(); //probleme ici

            try
            {
                if (go.transform.childCount == 0)
                {
                    //Default the Mesh is only one object attached to the cell GameObject
                    go.GetOrAddComponent<MeshCollider>();
                }
                else
                {
                    //Multiples Mesh are attachaed to childs of this GameObject
                    for (int i = 0; i < go.transform.childCount; i++)
                    {
                        go.transform.GetChild(i).gameObject.GetOrAddComponent<MeshCollider>();
                    }
                }
            }
            catch (Exception ex)
            {
                MorphoDebug.Log("MeshCollider ERROR " + ex);
            }

            MeshCollider meshCollider = AssociatedCellObject.GetComponent<MeshCollider>();

            if (meshCollider != null && meshCollider.sharedMesh == null)
                meshCollider.sharedMesh = AssociatedCellObject.GetComponent<MeshFilter>().sharedMesh;

            AssignMaterial(SetsManager.instance.Default);
            //AssignColor(Color.white);
        }

        //Return the name of the channel (channel 0 etc...)
        public string getName()
        {
            //We could store the name in a field but it whill increase memory...
            if (cell.Channels == null)
                return "";

            foreach (KeyValuePair<string, CellChannelRenderer> item in cell.Channels)
                if (item.Value == this)
                    return item.Key;

            return "";
        }

        public string getTuple()
        {
            return $"{cell.t},{cell.ID},{getName()}";
        }

        /////////////////  CENTER
        public void calculGravityCenter()
        {
            Gravity = new Vector3(0, 0, 0);

            MeshFilter filter = AssociatedCellObject.GetComponent<MeshFilter>();

            if (filter != null)
            {
                Vector3[] objectVerts = filter.mesh.vertices;
                for (int i = 0; i < objectVerts.Length; i++)
                    for (int j = 0; j < 3; j++)
                        Gravity[j] += objectVerts[i][j];
                for (int j = 0; j < 3; j++)
                    Gravity[j] /= objectVerts.Length;
            }
            else
            {
                //Multiples subb Ojbect with different filter are associaed

                for (int c = 0; c < AssociatedCellObject.transform.childCount; c++)
                {
                    filter = AssociatedCellObject.transform.GetChild(c).GetComponent<MeshFilter>();

                    int nbG = 0;

                    if (filter != null)
                    {
                        Vector3[] objectVerts = filter.mesh.vertices;

                        for (int i = 0; i < objectVerts.Length; i++)
                            for (int j = 0; j < 3; j++)
                                Gravity[j] += objectVerts[i][j];

                        nbG += objectVerts.Length;
                    }

                    for (int j = 0; j < 3; j++)
                        Gravity[j] /= nbG;
                }
            }

            MeshGravity = new Vector3(Gravity.x, Gravity.y, Gravity.z);

            //recentering rework : for all elements, gravity is center of gravith of mesh + local position, to be uniform
            Gravity += AssociatedCellObject.transform.localPosition;

            if (Double.IsNaN(Gravity.x) || Double.IsNaN(Gravity.y) || Double.IsNaN(Gravity.z))
                Gravity = Vector3.zero;

            if (MorphoTools.GetTransformationsManager() != null)
                MorphoTools.GetTransformationsManager().updateXYZCutSliders(Gravity);

            if (LoadParameters.instance.id_dataset == 0)
                MorphoTools.GetScatter().FillCenterTab(); // make sure all time tabs are ready for scatter storing

            if (MorphoTools.GetScatter() != null)
                MorphoTools.GetScatter().updateCenter(cell.t, Gravity);
        }

        public void createCenter()
        {
            MorphoTools.GetTransformationsManager().resetDataSetRotation();

            center = UnityEngine.Object.Instantiate(SetsManager.instance.sphere);
            center.name = AssociatedCellObject.name; //Same name as the nmesh (TODO CHECK...)
            center.transform.SetParent(AssociatedCellObject.transform);
            center.transform.localScale = new Vector3(cho.centerSize, cho.centerSize, cho.centerSize);
            center.transform.position = (Gravity - MorphoTools.GetDataset().embryoCenter) * InterfaceManager.instance.initCanvasDatasetScale;
            center.GetComponent<Renderer>().sharedMaterial = SetsManager.instance.Default;

            ShouldBeUpdated = true;

            MorphoTools.GetTransformationsManager().updateDataSetRotation();
        }

        public void destroyCenter()
        {
            UnityEngine.Object.DestroyImmediate(center, true);
        }

        public void resizeCenter()
        {
            if (center != null)
                center.transform.localScale = new Vector3(cho.centerSize, cho.centerSize, cho.centerSize);
        }

        //get current gravity center of cell (local position + vertices position, the same for primitives and assetbundles/obj meshes)
        public Vector3 GetCurrentGravityCenter()
        {
            Vector3 gc = new Vector3(0f, 0f, 0f);

            MeshFilter filter = AssociatedCellObject.GetComponent<MeshFilter>();

            if (filter != null)
            {
                Vector3[] objectVerts = filter.mesh.vertices;

                for (int i = 0; i < objectVerts.Length; i++)
                    for (int j = 0; j < 3; j++)
                        gc[j] += objectVerts[i][j];

                for (int j = 0; j < 3; j++)
                    gc[j] /= objectVerts.Length;
            }
            else
            {
                //Multiples subb Ojbect with different filter are associaed

                for (int c = 0; c < AssociatedCellObject.transform.childCount; c++)
                {
                    filter = AssociatedCellObject.transform.GetChild(c).GetComponent<MeshFilter>();
                    int nbG = 0;

                    if (filter != null)
                    {
                        Vector3[] objectVerts = filter.mesh.vertices;

                        for (int i = 0; i < objectVerts.Length; i++)
                            for (int j = 0; j < 3; j++)
                                gc[j] += objectVerts[i][j];

                        nbG += objectVerts.Length;
                    }

                    for (int j = 0; j < 3; j++)
                        gc[j] /= nbG;
                }
            }

            gc += AssociatedCellObject.transform.localPosition;

            return gc;
        }

        #region Object Render

        public Renderer Renderer;

        private GameObject _DisplayInfo = null;

        private List<GameObject> _SisterLinks = null;
        public bool previouslyActivate = true;

        private const string _ShaderInstanceSuffix = " (Instance)";
        private const string _ShaderName_2ColorNoise = "Custom_2colors_noise";
        private const string _ShaderName_2ColorNoise_instance = _ShaderName_2ColorNoise + _ShaderInstanceSuffix;
        private const string _ShaderName_FresnellCell = "FresnellCell";
        private const string _ShaderName_FresnellCell_instance = _ShaderName_FresnellCell + _ShaderInstanceSuffix;

        private void SetRendererEnabled(bool v)
        {
            if (Renderer != null)
            {
                if (Renderer.enabled != v)
                    Renderer.enabled = v;
            }

            Transform trns = AssociatedCellObject.transform;

            for (int c = 0; c < trns.childCount; c++)
                trns.GetChild(c).gameObject.GetComponent<Renderer>().enabled = v;
        }

        public static Vector3 WorldToScreenSpace(Vector3 worldPos, Camera cam, RectTransform area)
        {
            Vector3 screenPoint = cam.WorldToScreenPoint(worldPos);
            screenPoint.z = 0;

            Vector2 screenPos;

            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(area, screenPoint, cam, out screenPos))
                return screenPos;

            return screenPoint;
        }

        public static Vector3 GetScreenPosition(Vector3 transform, Canvas canvas, Camera cam)
        {
            Vector3 pos;
            float width = canvas.GetComponent<RectTransform>().sizeDelta.x;
            float height = canvas.GetComponent<RectTransform>().sizeDelta.y;
            float x = Camera.main.WorldToScreenPoint(transform).x / Screen.width;
            float y = Camera.main.WorldToScreenPoint(transform).y / Screen.height;

            pos = new Vector3(width * x - width / 2, y * height - height / 2);

            return pos;
        }

        public void DestroyInfoDisplay()
        {
            if (_DisplayInfo != null)
            {
                UnityEngine.Object.Destroy(_DisplayInfo);
            }
        }

        public void UpdateDisplayInfoPosition()
        {
            if (_DisplayInfo == null)
            {
                DisplayTextMeshPro();
                return;
            }

            Vector3 init_pos = Renderer.bounds.center;
            Vector3 effective_position = Camera.main.WorldToScreenPoint(init_pos);
            Vector2 final_pos = new Vector2();

            RectTransformUtility.ScreenPointToLocalPointInRectangle(InterfaceManager.instance.canvas.GetComponent<RectTransform>(), RectTransformUtility.WorldToScreenPoint(Camera.main, init_pos), Camera.main, out final_pos);

            _DisplayInfo.transform.localPosition = final_pos;
        }

        public void UpdateDisplayInfoNoPosition()
        {
            Text t = _DisplayInfo.GetComponent<Text>();

            t.fontSize = MorphoTools.GetFigureManager().FontSize;
            t.font = InterfaceManager.instance.default_font;

            string init_text = t.text;
            string final_text = init_text;
            string info = "";

            foreach (KeyValuePair<int, string> pair in cell.Infos)
            {
                Correspondence c = MorphoTools.GetDataset().Infos.Correspondences.Find(corr => { return corr.id_infos == pair.Key; });

                if (c.isValueShow)
                {
                    string text = pair.Value;

                    if (MorphoTools.isAnterior(text))
                        text = text.Replace("a", "A").Replace("b", "B");

                    if (MorphoTools.GetFigureManager().use_shortcut_names && c.name.ToLower().Contains("name"))
                        text = MorphoTools.ShortCutCellName(text);

                    info += text + "\n";
                }
            }

            if (info.Length > 3)
                info = info.Substring(0, info.Length - 1);

            final_text = info;

            t.text = final_text;
            t.material = InterfaceManager.instance.font_material;

            _DisplayInfo.transform.localScale = new Vector3(MorphoTools.GetFigureManager().ScaleTextInfo, MorphoTools.GetFigureManager().ScaleTextInfo, MorphoTools.GetFigureManager().ScaleTextInfo);
        }

        public void MakeLine(float ax, float ay, float bx, float by, Color col, string name)
        {
            if (_SisterLinks == null)
                _SisterLinks = new List<GameObject>();

            GameObject NewObj = new GameObject();

            _SisterLinks.Add(NewObj);

            NewObj.name = name;

            Image NewImage = NewObj.AddComponent<Image>();
            NewImage.color = col;

            RectTransform rect = NewObj.GetComponent<RectTransform>();
            rect.SetParent(InterfaceManager.instance.canvas.transform);
            rect.localScale = Vector3.one;

            Vector3 a = new Vector3(ax * 1, ay * 1, 0);
            Vector3 b = new Vector3(bx * 1, by * 1, 0);

            rect.anchorMin = Vector2.zero;
            rect.anchorMax = Vector2.zero;
            rect.localPosition = (a + b) / 2;

            Vector3 dif = a - b;

            rect.sizeDelta = new Vector3(dif.magnitude, MorphoTools.GetFigureManager().LineThickness);
            rect.rotation = Quaternion.Euler(new Vector3(0, 0, 180 * Mathf.Atan(dif.y / dif.x) / Mathf.PI));

            _SisterLinks.Add(NewObj);
        }

        public void DestroySisterLines(Cell sister)
        {
            if (_SisterLinks == null || sister == null)
                return;

            foreach (GameObject game in _SisterLinks)
            {
                if (game != null && game.name.Split('-')[1] == sister.getName())
                {
                    _SisterLinks.Remove(game);
                    UnityEngine.Object.Destroy(game);
                    return;
                }
            }
        }

        public void DestroyAllLines()
        {
            if (_SisterLinks == null)
                return;

            foreach (GameObject game in _SisterLinks)
                UnityEngine.Object.Destroy(game);

            _SisterLinks.Clear();
        }

        public void RebuildLines(Cell sister)
        {
            if (_SisterLinks == null)
                return;

            foreach (GameObject game in _SisterLinks)
            {
                if (game.name.Split('-')[1] == sister.getName())
                {
                    _SisterLinks.Remove(game);
                    UnityEngine.Object.Destroy(game);
                    DisplaySisterLines(sister);
                    return;
                }
            }
        }

        public void DisplaySisterLines(Cell sister)
        {
            Vector3 meshCenter = Renderer.bounds.center;
            Vector2 finalPosition = new Vector2();

            Vector3 sisterMeshCenter = sister.getFirstChannel().Renderer.bounds.center;
            Vector2 sisterFinalPosition = new Vector2();

            RectTransform rectTransform = InterfaceManager.instance.canvas.GetComponent<RectTransform>();

            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                rectTransform,
                RectTransformUtility.WorldToScreenPoint(Camera.main, meshCenter),
                Camera.main,
                out finalPosition);

            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                rectTransform,
                RectTransformUtility.WorldToScreenPoint(Camera.main, sisterMeshCenter),
                Camera.main,
                out sisterFinalPosition);

            if (_SisterLinks == null)
                _SisterLinks = new List<GameObject>();

            DestroySisterLines(sister);
            MakeLine(finalPosition.x, finalPosition.y, sisterFinalPosition.x, sisterFinalPosition.y, Color.black, $"{cell.getName()}-{sister.getName()}");
        }

        public int DisplayTextMeshPro()
        {
            int info_count = 0;

            if (cell.show)
            {
                InterfaceManager interfaceManager = InterfaceManager.instance;
                FigureManager figureManager = MorphoTools.GetFigureManager();
                Vector3 initialPosition = Renderer.bounds.center;
                GameObject displayedInfo = UnityEngine.Object.Instantiate(interfaceManager.display_info_template, interfaceManager.canvas.transform);
                Vector2 screenPosition = new Vector2();

                displayedInfo.SetActive(true);

                RectTransformUtility.ScreenPointToLocalPointInRectangle(
                    interfaceManager.canvas.GetComponent<RectTransform>(),
                    RectTransformUtility.WorldToScreenPoint(Camera.main, initialPosition), Camera.main, out screenPosition);

                displayedInfo.transform.localPosition = screenPosition;
                displayedInfo.transform.localScale = new Vector3(
                    figureManager.ScaleTextInfo,
                    figureManager.ScaleTextInfo,
                    figureManager.ScaleTextInfo);

                Text t = displayedInfo.GetComponent<Text>();
                t.fontSize = figureManager.FontSize;
                t.font = interfaceManager.default_font;

                string info = "";

                foreach (KeyValuePair<int, string> pair in cell.Infos)
                {
                    Correspondence c = MorphoTools.GetDataset().Infos.Correspondences.Find(corr => { return corr.id_infos == pair.Key; });

                    if (c.isValueShow)
                    {
                        string text = pair.Value;

                        if (MorphoTools.isAnterior(text))
                            text = text.Replace("a", "A").Replace("b", "B");

                        if (figureManager.use_shortcut_names && c.name.ToLower().Contains("name"))
                            text = MorphoTools.ShortCutCellName(text);

                        info_count++;
                        info += text + "\n";
                    }
                }

                if (info.Length > 3)
                    info = info.Substring(0, info.Length - 1);

                t.text = info;
                t.color = figureManager.TextColor;
                t.material = interfaceManager.font_material;

                if (info_count == 0)
                    UnityEngine.Object.Destroy(displayedInfo);
                else
                    _DisplayInfo = displayedInfo;
            }

            return info_count;
        }

        private void SetMeshColliderEnabled(bool v)
        {
            MeshCollider meshCollider = AssociatedCellObject.GetComponent<MeshCollider>();
            if (meshCollider != null && meshCollider.enabled != v)
                meshCollider.enabled = v;

            Transform trns = AssociatedCellObject.transform;
            for (int c = 0; c < trns.childCount; c++)
                if (trns.GetChild(c).gameObject.GetComponent<MeshCollider>() != null)
                    trns.GetChild(c).gameObject.GetComponent<MeshCollider>().enabled = v;
        }

        private void SetRendererMaterial(Material m)
        {
            if (Renderer != null)
                Renderer.sharedMaterial = m;

            Transform trns = AssociatedCellObject.transform;

            for (int c = 0; c < trns.childCount; c++)
                trns.GetChild(c).gameObject.GetComponent<Renderer>().sharedMaterial = m;
        }

        private void SetColorToRenderer(Renderer rend, Color col)
        {
            rend.UpdateColorProperty("_Color", col); //LAG WHEN ON
        }

        private void SetColorHaloToRenderer(Renderer rend, Color col)
        {
            rend.UpdateColorProperty("_FresnelColor", col);
            rend.UpdateFloatProperty("_Emission", 1f);
        }

        private void SetSecondColorToRenderer(Renderer rend, Color col, Color col2)
        {
            rend.UpdateColorProperty("_Color1", col);
            rend.UpdateColorProperty("_Color2", col2);
        }

        private void SetColorToRendererIndex(int colorIndex, Renderer rend, Color col)
        {
            rend.UpdateColorProperty($"_Color{colorIndex}", col);
        }

        private void SetRendererMaterialColor(Color col)
        {
            if (Renderer != null)
                SetColorToRenderer(Renderer, col);

            Transform trns = AssociatedCellObject.transform;

            for (int c = 0; c < trns.childCount; c++)
                SetColorToRenderer(trns.GetChild(c).gameObject.GetComponent<Renderer>(), col);
        }

        private void SetRendererMaterialColorHalo(Color col)
        {
            if (Renderer != null)
                SetColorHaloToRenderer(Renderer, col);

            Transform trns = AssociatedCellObject.transform;

            for (int c = 0; c < trns.childCount; c++)
                SetColorHaloToRenderer(trns.GetChild(c).gameObject.GetComponent<Renderer>(), col);
        }

        private void SetRendererMaterialSecondColor(Color col, Color col2)
        {
            if (Renderer != null)
                SetSecondColorToRenderer(Renderer, col, col2);

            Transform trns = AssociatedCellObject.transform;

            for (int c = 0; c < trns.childCount; c++)
                SetSecondColorToRenderer(trns.GetChild(c).gameObject.GetComponent<Renderer>(), col, col2);
        }

        private void SetRendererMaterialColorIndex(int index, Color col)
        {
            if (Renderer != null)
                SetColorToRendererIndex(index, Renderer, col);

            Transform trns = AssociatedCellObject.transform;
            for (int c = 0; c < trns.childCount; c++)
                SetColorToRendererIndex(index, trns.GetChild(c).gameObject.GetComponent<Renderer>(), col);
        }

        public void Activate(bool value)
        {
            //if channel is not visible, hide & set prev value to hide
            if (!cho.Visible)
            {
                if (previouslyActivate)//if channel is invis, and previous was vis, just hide
                {
                    previouslyActivate = false;
                    //HIDDING
                    //OBJECT
                    SetRendererEnabled(false);
                    SetMeshColliderEnabled(false);
                    //NUCLEI
                    if (center != null)
                    {
                        center.GetComponent<Renderer>().enabled = false;
                        center.GetComponent<SphereCollider>().enabled = false;
                    }
                    //TRACKING
                    if (vector != null)
                    {
                        for (int iy = 0; iy < vector.transform.childCount; iy++)
                        {
                            GameObject nvd = vector.transform.GetChild(iy).gameObject;
                            nvd.GetComponent<LineRenderer>().enabled = false;
                        }
                    }
                }
            }
            else//if channel is visible : 
            {
                if (previouslyActivate)//if if was visible
                {
                    if (!value)//if it was visible and now you want to hide, then hide
                    {
                        previouslyActivate = !previouslyActivate;
                        //HIDDING
                        //OBJECT
                        SetRendererEnabled(value);
                        SetMeshColliderEnabled(value);
                        //NUCLEI
                        if (center != null)
                        {
                            center.GetComponent<Renderer>().enabled = value;
                            center.GetComponent<SphereCollider>().enabled = value;
                        }
                        //TRACKING
                        if (vector != null)
                        {
                            for (int iy = 0; iy < vector.transform.childCount; iy++)
                            {
                                GameObject nvd = vector.transform.GetChild(iy).gameObject;
                                nvd.GetComponent<LineRenderer>().enabled = value;
                            }
                        }
                    }
                }
                else
                {
                    if(value)//if was not visible and now is, show it!
                    {
                        previouslyActivate = !previouslyActivate;
                        //VISIBLE
                        SetRendererEnabled(cho.Visible);
                        SetMeshColliderEnabled(cho.Visible);

                        //NUCLEI
                        if (cho.center)
                        {
                            if (center == null)
                                createCenter();

                            center.GetComponent<Renderer>().enabled = value;
                            center.GetComponent<SphereCollider>().enabled = value;
                        }
                        else if (center != null)
                        {
                            center.GetComponent<Renderer>().enabled = !value;
                            center.GetComponent<SphereCollider>().enabled = !value;
                        }

                        //TRACKING
                        if (cho.tracking && vector != null)
                        {
                            for (int iy = 0; iy < vector.transform.childCount; iy++)
                            {
                                GameObject nvd = vector.transform.GetChild(iy).gameObject;
                                nvd.GetComponent<LineRenderer>().enabled = value;
                            }
                        }
                        else if (vector != null)
                        {
                            for (int iy = 0; iy < vector.transform.childCount; iy++)
                            {
                                GameObject nvd = vector.transform.GetChild(iy).gameObject;
                                nvd.GetComponent<LineRenderer>().enabled = !value;
                            }
                        }
                    }
                }
            }

            
        }

        private void DestroyPreviousMaterial()
        {
            if (Renderer != null)
            {
                if (Renderer.material != null)
                    UnityEngine.Object.DestroyImmediate(Renderer.material, true);
            }
            else
            { //Child ...
                Transform trns = AssociatedCellObject.transform;

                for (int c = 0; c < trns.childCount; c++)
                {
                    GameObject goc = trns.GetChild(c).gameObject;

                    if (goc.GetComponent<Renderer>().material != null)
                        UnityEngine.Object.DestroyImmediate(goc.GetComponent<Renderer>().material, true);
                }
            }

            if (center != null)
                UnityEngine.Object.DestroyImmediate(center.GetComponent<Renderer>().material, true);

            if (vector != null)
            {
                for (int iy = 0; iy < vector.transform.childCount; iy++)
                    UnityEngine.Object.DestroyImmediate(vector.transform.GetChild(iy).gameObject.GetComponent<LineRenderer>().material);
            }
        }

        private void AssignMaterial(Material m)
        {
            SetRendererMaterial(m);

            if (center != null)
                center.GetComponent<Renderer>().sharedMaterial = m;

            if (vector != null)
            {
                for (int iy = 0; iy < vector.transform.childCount; iy++)
                    vector.transform.GetChild(iy).gameObject.GetComponent<LineRenderer>().sharedMaterial = m;
            }
        }

        private void AssignColor(Color c)
        {
            SetRendererMaterialColor(c); // Lag when ON

            if (center != null)
                center.GetComponent<Renderer>().material.color = c;

            if (vector != null)
            {
                for (int iy = 0; iy < vector.transform.childCount; iy++)
                    vector.transform.GetChild(iy).gameObject.GetComponent<LineRenderer>().material.color = c;
            }
        }

        private void AssignColorHalo(Color c)
        {
            SetRendererMaterialColorHalo(c);
        }

        private void AssignSecondColor(Color c, Color c2)
        {
            SetRendererMaterialSecondColor(c, c2);
        }

        private void AssignColorIndex(int index, Color c)
        {
            SetRendererMaterialColorIndex(index, c);

            if (index == 1)
            {
                if (center != null)
                    center.GetComponent<Renderer>().material.color = c;

                if (vector != null)
                {
                    for (int iy = 0; iy < vector.transform.childCount; iy++)
                        vector.transform.GetChild(iy).gameObject.GetComponent<LineRenderer>().material.color = c;
                }
            }
        }

        private bool CanActivate()
        {
            if (cell == null || AssociatedCellObject == null)
                return false;

            bool result = true;
            TransformationsManager transformationsManager = MorphoTools.GetTransformationsManager();

            if (MorphoTools.GetDataset() != null
                && transformationsManager != null)
            {
                if (!InterfaceManager.instance.MenuDataSet.FixedPlanToggle.isOn)
                {
                    if ((transformationsManager.XMin > Gravity.x
                    || transformationsManager.XMax < Gravity.x
                    || transformationsManager.YMin > Gravity.y
                    || transformationsManager.YMax < Gravity.y
                    || transformationsManager.ZMin > Gravity.z
                    || transformationsManager.ZMax < Gravity.z))
                    {
                        result = false;
                    }
                }
                else if(transformationsManager.outOfPlan(Gravity))
                {
                    result = false;
                }
            }
            if (!cell.show)
            {
                result = false;
            }

            Activate(result);
            return result;
        }

        public IEnumerator UpdateChannelRenderer()
        {
            if (!CanActivate() || !ShouldBeUpdated)
                yield break;

            //heavy check ? something lighter maybe ?
            if (MorphoTools.GetDataset().PickedManager.HighlightedCells.Count > 0)
            {
                if (MorphoTools.GetDataset().PickedManager.HighlightedCells.Contains(cell))
                {
                    AssignMaterial(SetsManager.instance.HighlightSelectedMaterial);
                    AssignColor(cell.GetCellColor());
                    ShouldBeUpdated = false;
                    yield break;
                }
            }

            /*if (false && (!cell.selected && (cell.selection == null || cell.selection.Count < 1)))
            {
                SetsManager.instance.Default.UpdateFloatProperty("_Glossiness",
                    InterfaceManager.instance.defaultShaderSmoothness);
                SetsManager.instance.Default.UpdateFloatProperty("_ActivateFrenel",
                    InterfaceManager.instance.defaultShaderUseFresnel ? 1f : 0f);
                SetsManager.instance.Default.UpdateFloatProperty("_FresnelPower",
                    InterfaceManager.instance.defaultShaderFresnelPower);
                SetsManager.instance.Default.UpdateFloatProperty("_Emission",
                    InterfaceManager.instance.defaultShaderEmission);
                SetsManager.instance.Default.UpdateFloatProperty("_Roughness",
                    InterfaceManager.instance.defaultShaderRoughness);
                SetsManager.instance.Default.UpdateFloatProperty("_Metallic",
                    InterfaceManager.instance.defaultShaderMetallic);
                SetsManager.instance.Default.UpdateColorProperty("_FresnelColor",
                    new Color(InterfaceManager.instance.r_fresnel_color, InterfaceManager.instance.g_fresnel_color,
                        InterfaceManager.instance.b_fresnel_color, InterfaceManager.instance.a_fresnel_color));

                if (!cell.UploadedColor)
                    SetsManager.instance.Default.UpdateColorProperty("_Color",
                        new Color(InterfaceManager.instance.r_native_color, InterfaceManager.instance.g_native_color,
                            InterfaceManager.instance.b_native_color, InterfaceManager.instance.a_native_color));
            }*/

            if ((cell.UploadedMainColor
                 || cell.UploadedHaloColor
                 || cell.UploadedSecondColor
                 || cell.UploadedMetallicParameter
                 || cell.UploadedSmoothnessParameter
                 || cell.UploadedTransparencyParameter)
                && !cell.selected)
            {
                Color materialColor = Color.white;
                if (Renderer.sharedMaterial == null)
                {
                    if (cell.UploadedSecondColor)
                        AssignMaterial(SetsManager.instance.StainSelection[0]);
                    else
                    {
                        AssignMaterial(SetsManager.instance.ColorDefault);
                        //AssignColor(materialColor);
                    }
                }
                else
                {
                    if (Renderer.sharedMaterial.name != _ShaderName_2ColorNoise_instance
                        && Renderer.sharedMaterial.name != _ShaderName_2ColorNoise
                        && cell.UploadedSecondColor)
                    {
                        if (cell.UploadedMainColor)
                            materialColor = cell.UploadColorMain;

                        AssignMaterial(SetsManager.instance.StainSelection[0]);
                    }
                    else if ((Renderer.sharedMaterial.name == _ShaderName_2ColorNoise_instance
                              || Renderer.sharedMaterial.name == _ShaderName_2ColorNoise)
                          && !cell.UploadedSecondColor)
                    {
                        materialColor = Renderer.material.GetColor("_Color1");
                        AssignMaterial(SetsManager.instance.ColorDefault);
                        AssignColor(materialColor);
                    }
                    else if ((Renderer.sharedMaterial.name == _ShaderName_2ColorNoise_instance
                            || Renderer.sharedMaterial.name == _ShaderName_2ColorNoise)
                        && cell.UploadedSecondColor)
                    {
                        materialColor = Renderer.material.GetColor("_Color1");
                    }
                    else if (Renderer.sharedMaterial.name != _ShaderName_FresnellCell_instance
                             && Renderer.sharedMaterial.name != _ShaderName_FresnellCell
                             && cell.UploadedMainColor)
                    {
                        AssignMaterial(SetsManager.instance.ColorDefault);
                    }
                }

                if (cell.UploadedSecondColor)
                {
                    AssignSecondColor(materialColor, cell.UploadColorSecond);
                }
                else
                {
                    if (cell.UploadedMainColor)
                        AssignColor(cell.UploadColorMain);
                    if (cell.UploadedHaloColor)
                    {
                        AssignColorHalo(cell.UploadColorHalo);
                        Renderer.UpdateFloatProperty("_FresnelPower", 1.5f);
                        Renderer.UpdateFloatProperty("_ActivateFrenel", 1f);
                    }
                    else
                    {
                        Renderer.UpdateFloatProperty("_Emission", 0f);
                    }

                    if (cell.UploadedMetallicParameter)
                        Renderer.UpdateFloatProperty("_Metallic", cell.UploadMetallicParameter);
                    else
                        Renderer.UpdateFloatProperty("_Metallic", InterfaceManager.instance.defaultShaderMetallic);

                    if (cell.UploadedSmoothnessParameter)
                        Renderer.UpdateFloatProperty("_Smoothness", cell.UploadSmoothnessParameter);
                    else
                        Renderer.UpdateFloatProperty("_Smoothness", InterfaceManager.instance.defaultShaderSmoothness);

                    if (cell.UploadedTransparencyParameter)
                        Renderer.UpdateFloatProperty("_Transparency", cell.UploadTransparencyParameter);
                    else
                        Renderer.UpdateFloatProperty("_Transparency", InterfaceManager.instance.defaultShaderTransparency);
                }
            }
            else
            {
                DestroyPreviousMaterial();

                if (cell.UploadedColor)
                {
                    AssignMaterial(SetsManager.instance.ColorDefault);
                    AssignColor(cell.UploadColor);
                }
                else if (cell.selection != null && cell.selection.Count > 0)
                {
                    if (cell.selection.Count == 1)
                    {
                        ApplySingleSelection(cell);
                    }
                    else if (cell.selection.Count > 1 && cell.selection.Count < 5)
                    {
                        ApplyMultipleSelection(cell);
                    }
                    else if (cell.selection.Count >= 5)
                    {
                        ApplyManySelection(cell);
                    }
                }
                else
                {
                    if (AssociatedCellObject.transform.parent.gameObject.GetComponent<Renderer>() != null)
                    {
                        //The Parent (embryo or data) is textured
                        Material mat = AssociatedCellObject.transform.parent.gameObject.GetComponent<Renderer>().material;
                        AssignMaterial(mat);
                    }
                    else if (!cell.UploadedColor)
                    {
                        if (MorphoTools.GetRawImages().MeshIsCut)
                        {
                            SetsManager.instance.Default.UpdateFloatProperty("_DoClip", 1f);
                            AssignMaterial(SetsManager.instance.Default);
                            MorphoTools.GetDataset().RawImages.updateCellRenderer(this);
                        }
                        else
                        {
                            SetsManager.instance.Default.UpdateFloatProperty("_DoClip", 0f);
                            
                            //AssignMaterial(SetsManager.instance.Selected);
                            AssignMaterial(SetsManager.instance.Default);
                            //AssignColor(Color.white); //LAG WHEN ON
                        }
                    }
                }

                //Finally if it is a selected cell we apply the selected shader...
                if (cell.selected)
                    AssignMaterial(SetsManager.instance.Selected);
               
                

                
            }

            ShouldBeUpdated = false;

            yield return null;
        }

        private void ApplyManySelection(Cell cell)
        {
            var multiSelectionIndexes = MorphoTools.GetDataset().multiSelectionIndexes;

            if (!multiSelectionIndexes.ContainsKey(cell.t))
                multiSelectionIndexes.Add(cell.t, new Dictionary<string, int>());

            if (!multiSelectionIndexes[cell.t].ContainsKey(cell.ID))
                multiSelectionIndexes[cell.t].Add(cell.ID, 0);

            int s = (int)Mathf.Round(multiSelectionIndexes[cell.t][cell.ID] / MorphoTools.GetDataset().timeChangeShader);

            if (cell.selection.Count > s)
            {
                var selectedMaterial = SelectionManager.getSelectedMaterial(cell.selection[s]);

                if (MorphoTools.GetRawImages().MeshIsCut && selectedMaterial.name == "CutMeshes")
                {
                    AssignMaterial(SetsManager.instance.Default);//CLASSICAL MODE
                }
                else
                {
                    SetsManager.instance.Default.UpdateFloatProperty("_DoClip", 0f);
                    AssignMaterial(selectedMaterial);
                }

                //AssignColor(selectedMaterial.color);
            }
            multiSelectionIndexes[cell.t][cell.ID] += 1;

            if (multiSelectionIndexes[cell.t][cell.ID] >= cell.selection.Count * MorphoTools.GetDataset().timeChangeShader)
                multiSelectionIndexes[cell.t][cell.ID] = 0;
        }

        private void ApplyMultipleSelection(Cell cell)
        {
            if (InterfaceManager.instance.shaderType == "stain")
            {
                AssignMaterial(SetsManager.instance.StainSelection[cell.selection.Count - 2]);
            }
            else
            {
                AssignMaterial(SetsManager.instance.BarSelection[cell.selection.Count - 2]);
            }

            for (int i = 0; i < cell.selection.Count; i++)
            {
                AssignColorIndex(i + 1, SelectionManager.getSelectedMaterial(cell.selection[i]).color);
            }
        }

        private void ApplySingleSelection(Cell cell)
        {
            int value = cell.selection[0];
            //Only One  Material
            Material m = SelectionManager.getSelectedMaterial(value);
            if (m.name == "CutMeshes" || m.name == "FresnellCell")
            {
                //MATERIAL IS DEFAULT -> CHECK ASSIGN TRANSPARENCE MATERIAL
                if (cell.UploadColor.a != 0f
                    && cell.UploadColor.r == 0f
                    && cell.UploadColor.g == 0f
                    && cell.UploadColor.b == 0f)
                {
                    //SEPCIAL CASE FOR TRANSPARENCY
                    AssignMaterial(SetsManager.instance.Transparent); //ASSIGN TRANSPARENCE MATERIAL
                    AssignColor(new Color(m.color.r, m.color.g, m.color.b, cell.UploadColor.a));
                }
                else
                {
                    if (MorphoTools.GetRawImages().MeshIsCut)
                    {
                        SelectionManager.getSelectedMaterial(value).UpdateFloatProperty("_DoClip", 1f);
                        AssignMaterial(SelectionManager.getSelectedMaterial(value));//CLASSICAL MODE
                    }
                    else
                    {
                        SelectionManager.getSelectedMaterial(value).UpdateFloatProperty("_DoClip", 0f);
                        AssignMaterial(SelectionManager.getSelectedMaterial(value));//CLASSICAL MODE
                    }
                    //AssignColor(SelectionManager.getSelectedMaterial(value).color);
                }
            }
            else
            {
                AssignMaterial(m);//CLASSICAL MODE
                //AssignColor(m.color);
            }
        }

        #endregion Object Render
    }
}