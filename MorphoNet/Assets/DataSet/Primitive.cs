using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Primitive
{
    public string Name;
    public string Type;
    public Vector3 Position;
    public Vector3 Scale;
    public Quaternion Rotation;


    public Primitive (string name, string type)
    {
        Name = name;
        Type = type;
    }
}
