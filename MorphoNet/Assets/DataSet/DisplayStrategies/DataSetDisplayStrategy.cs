using System;
using System.Collections.Generic;
using UnityEngine;

namespace MorphoNet
{
    [CreateAssetMenu(fileName = "Default Display Strategy", menuName = "ScriptableObjects/Dataset Display Strategy/Default")]
    public class DataSetDisplayStrategy : ScriptableObject
    {
        [SerializeField]
        public string StrategyName = "";
        
        [SerializeField]
        protected bool _MultiTimestamp = false;

        [SerializeField]
        protected bool _AllVisible = false;

        [SerializeField]
        protected int _CurrentTimestamp;

        public bool MultiTimestamp { get => _MultiTimestamp; protected set => _MultiTimestamp = value; }

        public SortedSet<int> VisibleTimestamp { get; } = new SortedSet<int>();

        public bool IsTimestampVisible(int timeStamp)
        {
            if (_CurrentTimestamp == timeStamp || _AllVisible)
                return true;

            return VisibleTimestamp.Contains(timeStamp);
        }

        public void AddVisibleTimestamp(int timestamp) => VisibleTimestamp.Add(timestamp);

        public void AddVisibleTimestampRange(int start, int end)
        {
            (start, end) = (Mathf.Min(start, end), Mathf.Max(start, end));
            for (int i = start; i <= end; i++)
                AddVisibleTimestamp(i);
        }

        public void RemoveVisibleTimestamp(int timestamp) => VisibleTimestamp.Remove(timestamp);

        public void RemoveVisibleTimestampRange(int start, int end)
        {
            (start, end) = (Mathf.Min(start, end), Mathf.Max(start, end));
            for (int i = start; i <= end; i++)
                RemoveVisibleTimestamp(i);
        }

        public void ClearVisibleTimestamp() => VisibleTimestamp.Clear();

        public virtual void ChangeTimestamp(int currentTimestamp)
        {
            _CurrentTimestamp = currentTimestamp;
        }

        protected int TimeOffset(int currentTimestamp, int targetTimestamp)
        {
            if (_AllVisible)
                return targetTimestamp - currentTimestamp;

            int timeOffset = 0;
            var (min, max) = (Mathf.Min(currentTimestamp, targetTimestamp), Mathf.Max(currentTimestamp, targetTimestamp));

            for (int i = min; i < max; i++)
            {
                if (VisibleTimestamp.Contains(i) || i == currentTimestamp)
                    timeOffset++;
            }

            if (targetTimestamp < currentTimestamp)
                return -timeOffset;
            else
                return timeOffset;
        }

        public virtual Vector3 RelativePosition(int currentTimestamp, int targetTimestamp) => Vector3.zero;

        public virtual Quaternion RelativeRotation(Quaternion currentGlobalRotation, int currentTimestamp, int targetTimestamp) => currentGlobalRotation;

        public Pose RelativePose(Quaternion currentGlobalRotation, int currentTimestamp, int targetTimestamp)
            => new Pose(
                    RelativePosition(currentTimestamp, targetTimestamp),
                    RelativeRotation(currentGlobalRotation, currentTimestamp, targetTimestamp));
    }
}