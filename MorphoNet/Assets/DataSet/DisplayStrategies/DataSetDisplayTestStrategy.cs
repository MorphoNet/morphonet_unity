using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MorphoNet
{
    [CreateAssetMenu(fileName = "Test Display Strategy", menuName = "ScriptableObjects/Dataset Display Strategy/Test")]
    public class DataSetDisplayTestStrategy : DataSetDisplayStrategy
    {
        [SerializeField]
        private float _Offset = 1f;

        [SerializeField]
        protected int _DisplayedItemOnEachSide;

        public override Vector3 RelativePosition(int currentTimestamp, int targetTimestamp)
        {
            int timeOffset = TimeOffset(currentTimestamp, targetTimestamp);

            Vector3 positionOffset = Vector3.zero;
            positionOffset.x = _Offset * timeOffset;
            positionOffset.z = -_Offset * timeOffset * 0.5f;

            return positionOffset;
        }

        public override Quaternion RelativeRotation(
            Quaternion currentGlobalRotation,
            int currentTimestamp,
            int targetTimestamp)
        {
            return base.RelativeRotation(currentGlobalRotation, currentTimestamp, targetTimestamp);
        }

        public override void ChangeTimestamp(int currentTimestamp)
        {
            ClearVisibleTimestamp();
            AddVisibleTimestampRange(
                currentTimestamp - _DisplayedItemOnEachSide,
                currentTimestamp + _DisplayedItemOnEachSide);
        }
    }
}