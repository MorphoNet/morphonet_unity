using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MorphoNet
{
    [CreateAssetMenu(fileName = "Helicoid Display Strategy", menuName = "ScriptableObjects/Dataset Display Strategy/Helicoid")]
    public class HelicoidDisplayStrategy : DataSetDisplayStrategy
    {
        [SerializeField]
        private int _TimestampPerRevolution = 10;

        [SerializeField]
        private float _RevolutionRadius = 20;

        [SerializeField]
        private float _RevolutionHight = 15f;

        public override Vector3 RelativePosition(int currentTimestamp, int targetTimestamp)
        {
            Vector3 pos = Vector3.zero;
            int timeOffset = TimeOffset(currentTimestamp, targetTimestamp);

            float angle = 360f / (_TimestampPerRevolution + 1) * timeOffset * Mathf.Deg2Rad;

            var z = pos.z + _RevolutionRadius;
            var x = pos.x;
            pos.x = x * Mathf.Cos(angle) - z * Mathf.Sin(angle);
            pos.z = x * Mathf.Sin(angle) + z * Mathf.Cos(angle);
            pos.y = timeOffset == 0 ? 0 : _RevolutionHight / _TimestampPerRevolution * timeOffset;
            pos.z -= _RevolutionRadius;
            return pos;
        }

        public override Quaternion RelativeRotation(
            Quaternion currentGlobalRotation,
            int currentTimestamp,
            int targetTimestamp)
        {
            Quaternion rotation = currentGlobalRotation;

            XRCameraObject targetCamera = XRCameraObject.Instance;

            if (targetCamera != null && targetCamera.transform != null)
            {
                Vector3 lookAtPosition = targetCamera.transform.position;
                Vector3 relativePosition = RelativePosition(currentTimestamp, targetTimestamp);

                lookAtPosition.y = relativePosition.y;

                rotation = Quaternion.LookRotation(relativePosition - lookAtPosition, Vector3.up) * rotation;
            }

            return rotation;
        }
        

        public override void ChangeTimestamp(int currentTimestamp)
        {
            base.ChangeTimestamp(currentTimestamp);
        }
    }
}