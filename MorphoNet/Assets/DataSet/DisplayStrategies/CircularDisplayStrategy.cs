using UnityEngine;

namespace MorphoNet
{
    [CreateAssetMenu(fileName = "Circular Display Strategy", menuName = "ScriptableObjects/Dataset Display Strategy/Circular")]
    public class CircularDisplayStrategy : DataSetDisplayStrategy
    {
        [SerializeField]
        private float _BaseRadius = 40;

        [SerializeField]
        private int _DisplayedItemOnEachSide = 15;

        public override Vector3 RelativePosition(int currentTimestamp, int targetTimestamp)
        {
            Vector3 pos = Vector3.zero;
            int timeOffset = TimeOffset(currentTimestamp, targetTimestamp);

            float angle = 360 / (VisibleTimestamp.Count + 1) * timeOffset * Mathf.Deg2Rad;

            var z = pos.z + _BaseRadius;
            var x = pos.x;
            pos.x = x * Mathf.Cos(angle) - z * Mathf.Sin(angle);
            pos.z = x * Mathf.Sin(angle) + z * Mathf.Cos(angle);
            pos.y = 0;
            pos.z -= _BaseRadius;
            return pos;
        }

        public override Quaternion RelativeRotation(
            Quaternion currentGlobalRotation,
            int currentTimestamp,
            int targetTimestamp)
        {
            Quaternion rotation = currentGlobalRotation;

            XRCameraObject targetCamera = XRCameraObject.Instance;

            if (targetCamera != null && targetCamera.transform != null)
            {
                Vector3 lookAtPosition = targetCamera.transform.position;
                Vector3 relativePosition = RelativePosition(currentTimestamp, targetTimestamp);

                lookAtPosition.y = relativePosition.y;

                rotation = Quaternion.LookRotation(relativePosition - lookAtPosition, Vector3.up) * rotation;
            }

            return rotation;
        }

        public override void ChangeTimestamp(int currentTimestamp)
        {
            ClearVisibleTimestamp();

            if (_AllVisible)
            {
                AddVisibleTimestampRange(
                    currentTimestamp - 100000,
                    currentTimestamp + 100000);
            }
            else
            {
                AddVisibleTimestampRange(
                    currentTimestamp - _DisplayedItemOnEachSide,
                    currentTimestamp + _DisplayedItemOnEachSide);
            }
        }
    }
}