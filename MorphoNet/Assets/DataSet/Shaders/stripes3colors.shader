﻿Shader "Custom/stripes3colors"
{
    Properties
    {
        _Color1 ("Color1", Color) = (1,1,1,1)
        _Color2 ("Color2", Color) = (0,0,0,1)
        _Color3 ("Color3", Color) = (0.5,0.5,0.5,1)
        _Scale ("Scale", float) = 0.1
        [Toggle] _DoClip("Do Clip", float) = 0
        _XClippingPlane("XClipping Plane", Vector) = (0,0,0,0)
        _XViewPlan("X View Plan",int) = 1
        _YClippingPlane("Y Clipping Plane", Vector) = (0,0,0,0)
        _YViewPlan("X View Plan",int) = 1
        _ZClippingPlane("Z Clipping Plane", Vector) = (0,0,0,0)
        _ZViewPlan("Z View Plan",int) = 1
        _Thickness("Thickness", Range(0,1)) = 0.05
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        float4 _Color1;
        float4 _Color2;
        float4 _Color3;
        float _Scale;
        uniform float _DoClip;
        uniform float4 _XClippingPlane;
        uniform int _XViewPlan;
        uniform float4 _YClippingPlane;
        uniform int _YViewPlan;
        uniform float4 _ZClippingPlane;
        uniform int _ZViewPlan;
        float _Thickness;

        struct Input
        {
            float3 objPos;
        };

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        
        void vert (inout appdata_full v, out Input o) {
            UNITY_INITIALIZE_OUTPUT(Input,o);
            o.objPos = v.vertex;
        }
        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            if (_DoClip > 0.5) {
                float clipVX = IN.objPos.x * _XClippingPlane.x + IN.objPos.y * _XClippingPlane.y + IN.objPos.z * _XClippingPlane.z - _XClippingPlane.w;
                float clipPX = (_XViewPlan == 1 && clipVX >= -_Thickness && clipVX <= _Thickness ? 1 : 0);
                float clipVY = IN.objPos.x * _YClippingPlane.x + IN.objPos.y * _YClippingPlane.y + IN.objPos.z * _YClippingPlane.z - _YClippingPlane.w;
                float clipPY = (_YViewPlan == 1 && clipVY >= -_Thickness && clipVY <= _Thickness ? 1 : 0);
                float clipVZ = IN.objPos.x * _ZClippingPlane.x + IN.objPos.y * _ZClippingPlane.y + IN.objPos.z * _ZClippingPlane.z - _ZClippingPlane.w;
                float clipPZ = (_ZViewPlan == 1 && clipVZ >= -_Thickness && clipVZ <= _Thickness ? 1 : 0);
                float clipP = (clipPX == 1 || clipPY == 1 || clipPZ == 1 ? 1 : 0);

                if (clipP < 0.5) discard;
            }
            // Albedo comes from a texture tinted by color
            float4 c;
            float stripes=frac(_Scale * IN.objPos.x);
            float step1=step(stripes,0.33);
            float step2=step(stripes,0.66);
            float step3=1-step1-step2;
            if(step1>0)c=_Color2;
            else if(step2>0)c=_Color1;
            else if(step3>0)c=_Color3;

            o.Albedo = c.xyz;
            // Metallic and smoothness come from slider variables
            o.Metallic = 0.0;
            o.Smoothness = 0.5;
            o.Alpha =1.0;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
