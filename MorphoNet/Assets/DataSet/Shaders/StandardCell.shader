﻿Shader "MorphoNet/Cell/Standard Cell"
{
	Properties
	{
		_Color("Color", Color) = (0.6784314, 0.3098039, 0.2196078, 1.0)
		_Roughness("Roughness", Float) = 1.0
		_NoiseScale("Noise Scale", Float) = 1.0
		_FresnelPower("Fresnel Power", Float) = 2.0
		_Smoothness("Smoothness", Range(0, 1)) = 0.0
		_Metallic("Metallic", Range(0, 1)) = 0.0
	}

	SubShader
	{
		Tags
		{ 
			"RenderType" = "Opaque"
		}

		CGPROGRAM
		#pragma surface surface Standard vertex:vert
		#pragma target 3.0

		//#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"          

		//#include "Noise/SimplexNoise3D.hlsl"
		#include "Noise/ClassicNoise3D.hlsl"

		float4 _Color;
		float _Roughness;
		float _NoiseScale;
		float _FresnelPower;
		float _Smoothness;
		float _Metallic;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		struct Input
		{
			// Built-in
			float3 viewDir;
			float3 worldPos;
			INTERNAL_DATA
			// Custom
			float3 osPosition;
		};

		void vert (inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.osPosition = v.vertex;
		}

		float FresnelEffect(float3 normal, float3 viewDir, float power)
		{
			return pow(1.0 - saturate(dot(normal, viewDir)), power);
		}

		void surface (Input IN, inout SurfaceOutputStandard o)
		{
			// Surface data are given in tangent space, the normal vector is (0, 0, 1)
			float3 worldNormal = WorldNormalVector(IN, float3(0, 0, 1));

			// Compute normal from height
			float height = cnoise(IN.osPosition * _NoiseScale);
			float3 newNormal = normalize(float3(ddx(height) * _Roughness, ddy(height) * _Roughness, 1.0));
			
			o.Normal = newNormal;
			o.Albedo = _Color.rgb * (1.0 - FresnelEffect(float3(0, 0, 1), IN.viewDir, _FresnelPower));
			o.Metallic = _Metallic;
			o.Smoothness = _Smoothness;
			o.Alpha = 1.0;
		}
		ENDCG
	}
	FallBack "Diffuse"
}