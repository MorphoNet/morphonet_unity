﻿Shader "Custom/Fresnel Cell"
{
	Properties
	{
		_Color("Color", Color) = (0.6784314, 0.3098039, 0.2196078, 1.0)
		_FresnelColor("Fresnel Color", Color) = (0.5, 0.01, 0.4, 1.0)
		_Emission("Emission", Range(0, 1)) = 0.5
		_Roughness("Roughness", Float) = 1.0
		_NoiseScale("Noise Scale", Float) = 1.0
		_FresnelPower("Fresnel Power", Float) = 2.0
		_Smoothness("Smoothness", Range(0, 1)) = 0.0
		_Metallic("Metallic", Range(0, 1)) = 0.0
		_ActivateFrenel("ActivateFrenel",Range(0, 1)) = 0.0
		_Transparency("Transparency", Range(0, 1)) = 1.0
		[Toggle] _DoClip("Do Clip", float) = 0
		_Thickness("Thickness", Range(0.01,2)) = 0.05
		[Toggle]_ThinMode("Thin mode", int) = 0
		[Toggle]_ToggleFreeCuttingPlane("Use Free Cutting Plane", float) = 0
		_FreeCuttingPlaneRepresentation("Free Cutting Plane Representation", Vector) = (0,0,0,0)
		_OffsetPlan("OffsetPlan", Vector) = (0,0,0)
	}

	SubShader
	{
		Tags
		{ 
			"RenderType" = "Opaque"
		}

		
		Cull Off

		CGPROGRAM
		#pragma surface surface Standard vertex:vert
		#pragma target 3.0
		
		//#include "Noise/SimplexNoise3D.hlsl"
		#include "Noise/ClassicNoise3D.hlsl"

		float4 _Color;
		float4 _FresnelColor;
		float _Roughness;
		float _NoiseScale;
		float _FresnelPower;
		float _Smoothness;
		float _Metallic;
		uniform float _DoClip;
		float _Thickness;
		float _Emission;
		float _ActivateFrenel;
		float _Transparency;
		
		uniform float _ToggleFreeCuttingPlane;
		float4 _FreeCuttingPlaneRepresentation;
		int _ThinMode;
		

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		struct Input
		{
			// Built-in
			float3 viewDir;
			float3 worldPos;
			INTERNAL_DATA
			// Custom
			float3 osPosition;
		};

		void vert (inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.osPosition = v.vertex;
		}

		float FresnelEffect(float3 normal, float3 viewDir, float power)
		{
			return pow(1.0 - saturate(dot(normal, viewDir)), power);
		}

		void surface (Input IN, inout SurfaceOutputStandard o)
		{	
			if (_DoClip > 0.5 && _ToggleFreeCuttingPlane > 0.5)
			{
				float distance = dot(IN.worldPos, _FreeCuttingPlaneRepresentation.xyz);
				distance = distance + _FreeCuttingPlaneRepresentation.w;
				if (_ThinMode > 0.5) {
					clip(distance + _Thickness);
					clip(-distance + _Thickness);
				}
				else
					clip(-distance);
				
				
			}
			

			// Compute normal from height
			float height = cnoise(IN.osPosition * _NoiseScale);
			float3 newNormal = normalize(float3(ddx(height) * _Roughness, ddy(height) * _Roughness, 1.0));

			o.Normal = newNormal;
			o.Albedo = _Color.rgb;
			o.Emission = _ActivateFrenel<0.5?_Emission * FresnelEffect(newNormal, IN.viewDir, _FresnelPower) : _Emission * _FresnelColor.rgb * FresnelEffect(newNormal, IN.viewDir, _FresnelPower);
			o.Metallic = _Metallic;
			o.Smoothness = _Smoothness;
			o.Alpha = _Transparency;
		}
		ENDCG

		Pass
		{
			Name "ShadowCaster"
			Tags { "LightMode" = "ShadowCaster" }

			Fog {Mode Off}
			ZWrite On ZTest LEqual Cull Off
			Offset 1, 1

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_shadowcaster
			#include "UnityCG.cginc"

			float _ToggleFreeCuttingPlane;
			float4 _FreeCuttingPlaneRepresentation;
			int _ThinMode;
			uniform float _DoClip;
			float _Thickness;
			float3 _OffsetPlan;

			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 ver : TEXCOORD1;
			};

			v2f vert(appdata_base v)
			{
				v2f o;
				TRANSFER_SHADOW_CASTER(o);
				o.ver = mul(unity_ObjectToWorld,v.vertex.xyz);
				return o;
			}

			float4 frag(v2f i) : SV_Target
			{
				if (_DoClip > 0.5 && _ToggleFreeCuttingPlane > 0.5)
				{

					float distance = dot(float4(i.ver + _OffsetPlan ,1.0), _FreeCuttingPlaneRepresentation);
					if (_ThinMode > 0.5) {
						clip(distance + _Thickness);
						clip(-distance + _Thickness);
					}
					else
						clip(-distance);
				}

				SHADOW_CASTER_FRAGMENT(i);

			}
			ENDCG

		}
	}
	//FallBack "Diffuse"
}