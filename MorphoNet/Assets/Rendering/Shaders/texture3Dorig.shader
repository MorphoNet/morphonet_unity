﻿Shader "3DTexture/Texture3DPreviewOrig"
{	Properties{
		_MainTex("Texture", 3D) = "white" {}
		_Transparency("Transparency", Range(0,1)) = 1
		_SamplingQuality("Sampling Quality", int) = 250
		_Threshold("Threshold", Range(0,1)) = 0
		_Intensity("Intensity", Range(0,5)) = 1

		_Offset("Offset", Vector) = (0,0,0)

		_FreeClippingPlaneActivated("Activate Free Clipping Plane", int) = 0
		_FreeClippingPlane("Free Clipping Plane", Vector) = (0,0,0,0)

		_XClippingPlane("X Clipping Plane", Vector) = (0,0,0,0)
		_YClippingPlane("Y Clipping Plane", Vector) = (0,0,0,0)
		_ZClippingPlane("Z Clipping Plane", Vector) = (0,0,0,0)
		_XPlanView("X Plan View",int) = 1
		_YPlanView("Y Plan View",int) = 1
		_ZPlanView("Z Plan View",int) = 1
		_OffsetPlan("OffsetPlan", Vector) = (0,0,0)

		_Thickness("Thickness", Range(0,1)) = 0.05

		_ColorBarTex("ColorBarTex",2D) = "white" {}

		_NoPlanMode("No Plans Mode",int) = 1
		[Enum(UnityEngine.Rendering.CullMode)] _Cull("Cull", Float) = 1
}
SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
		}

		CGINCLUDE
		#include "UnityCG.cginc"

		int _SamplingQuality;
		sampler3D _MainTex;
		uniform float4 _MainTex_ST;
		float _Transparency;
		float _Front;
		float _Threshold;
		float _Intensity;
		float _Thickness;

		int _FreeClippingPlaneActivated;
		float4 _FreeClippingPlane;

		int _XPlanView;
		int _YPlanView;
		int _ZPlanView;

		float4 _XClippingPlane;
		float4 _YClippingPlane;
		float4 _ZClippingPlane;
		float3 _OffsetPlan;
		float3 _Offset;
		sampler2D _ColorBarTex;
		int _NoPlanMode;

		struct v2f
		{
			float4 pos : SV_POSITION;
			float3 localPos : TEXCOORD0;
			float4 screenPos : TEXCOORD1;
			float3 worldPos : TEXCOORD2;
		};

		v2f vert(appdata_base v)
		{
			v2f OUT;
			OUT.pos = UnityObjectToClipPos(v.vertex);
			OUT.localPos = v.vertex.xyz;
			OUT.screenPos = ComputeScreenPos(OUT.pos);
			COMPUTE_EYEDEPTH(OUT.screenPos.z);
			OUT.worldPos = mul(unity_ObjectToWorld, v.vertex);
			return OUT;
		}

		// usual ray/cube intersection algorithm
		struct Ray
		{
			float3 origin;
			float3 direction;
		};
		bool IntersectBox(Ray ray, out float entryPoint, out float exitPoint)
		{
			float3 invR = 1.0 / ray.direction;
			float3 tbot = invR * (float3(-0.5, -0.5, -0.5) - ray.origin);
			float3 ttop = invR * (float3(0.5, 0.5, 0.5) - ray.origin);
			float3 tmin = min(ttop, tbot);
			float3 tmax = max(ttop, tbot);
			float2 t = max(tmin.xx, tmin.yz);
			entryPoint = max(t.x, t.y);
			t = min(tmax.xx, tmax.yz);
			exitPoint = min(t.x, t.y);
			return entryPoint <= exitPoint;
		}

		float4 frag(v2f IN) : COLOR
		{
			float3 localCameraPosition = UNITY_MATRIX_IT_MV[3].xyz;
			Ray ray;
			ray.origin = localCameraPosition;
			ray.direction = normalize(IN.localPos - localCameraPosition);

			float entryPoint, exitPoint;
			IntersectBox(ray, entryPoint, exitPoint);

			if (entryPoint < 0.0) entryPoint = 0.0;

			float3 rayStart = ray.origin + ray.direction * entryPoint;
			float3 rayStop = ray.origin + ray.direction * exitPoint;
			float3 start = rayStart;

			float dist = distance(rayStop, rayStart);
			float stepSize = dist / float(_SamplingQuality);
			float3 ds = normalize(rayStop - rayStart) * stepSize;

			float4 color = float4(0,0,0,0);//xyz w == rgb a
			float counter = 0;
			bool first = true;

			int i = 0;
			[loop]
			while (first && i <= _SamplingQuality)
			{
				float3 pos = start.xyz;
				pos.xyz += _Offset;

				float3 invPos = mul(unity_ObjectToWorld, start) + _OffsetPlan;

				//no blur
				float4 mask = tex3D(_MainTex, pos + float3(_MainTex_ST.zw,0));

				//set1
				if (first && (mask.x >= _Threshold))
				{
					bool view = false;

					if (!view && _FreeClippingPlaneActivated == 1)
					{
							float clipFree
								= invPos.x * _FreeClippingPlane.x
								+ invPos.y * _FreeClippingPlane.y
								+ invPos.z * _FreeClippingPlane.z
								- _FreeClippingPlane.w;

							// determine the visibility
							if (clipFree >= 0)
								view = true;
					}
					else if (_NoPlanMode == 1) {
						view = true;
					}
					else
					{
						if (!view && _XPlanView == 1)
						{
							float clipX
								= invPos.x * _XClippingPlane.x
								+ invPos.y * _XClippingPlane.y
								+ invPos.z * _XClippingPlane.z
								- _XClippingPlane.w;

							if (clipX >= -_Thickness && clipX <= _Thickness)
								view = true;
						}

						if (!view && _YPlanView == 1)
						{
							float clipY
								= invPos.x * _YClippingPlane.x
								+ invPos.y * _YClippingPlane.y
								+ invPos.z * _YClippingPlane.z
								- _YClippingPlane.w;

							if (clipY >= -_Thickness && clipY <= _Thickness)
								view = true;
						}

						if (!view && _ZPlanView == 1)
						{
							float clipZ
								= invPos.x * _ZClippingPlane.x
								+ invPos.y * _ZClippingPlane.y
								+ invPos.z * _ZClippingPlane.z
								- _ZClippingPlane.w;

							if (clipZ >= -_Thickness && clipZ <= _Thickness)
								view = true;
						}
					}

					if (view) {
						color.x += mask.x;
						color.y += mask.x;
						color.z += mask.x;
						if (_Transparency == 1)
						{
								first = false;
								color.w = 1;
						}
						else {
							color.w += mask.x * mask.x * _Transparency * _Transparency;

							if (color.w > 1)
								first = false;
						}
					}
				}
				start += ds;
				i++;
			}

			color.r *= _Intensity;
			color.g *= _Intensity;
			color.b *= _Intensity;

			if (color.w < _Threshold) {
				discard;
			}
			float4 colorize = tex2D(_ColorBarTex, float2(color.x, 1));
			color.xyz = colorize.xyz;

			return color;
		}
		ENDCG

		Pass
		{
			Cull[_Cull]
			Blend SrcAlpha OneMinusSrcAlpha // Premultiplied transparency

			ZWrite false

			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag

			ENDCG
		}
	}

	Fallback "Diffuse"
}