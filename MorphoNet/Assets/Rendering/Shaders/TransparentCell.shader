﻿Shader "Custom/TransparentCell" {
    Properties {
        _Color("Color", Color) = (1,1,1,1)
        _Transparency("Transparency", Range(0, 1)) = 0.5
    }
    SubShader {
        Tags { "Queue"="Transparent"   "RenderType"="Transparent" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows alpha

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        struct Input {
            float2 uv_MainTex;
        };

        fixed4 _Color;
        float _Transparency;

        void surf (Input IN, inout SurfaceOutputStandard o) {
            o.Albedo = _Color.rgb;
            o.Alpha= _Transparency;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
