﻿Shader "LordZargon/ColorByPosition" {
	Properties{
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_ColorPalette("Colour Palette (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma surface surf Standard fullforwardshadows vertex:vert

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0

			sampler2D _MainTex, _ColorPalette;

			struct Input {
				float2 uv_MainTex;
				float4 color : COLOR;
			};

			// Random generator based on 3 values in, 3 values out by Dave Hoskins: https://www.shadertoy.com/view/4djSRW
			float3 random(float3 seed)
			{
				seed = frac(seed * float3(443.897, 441.423, 437.195));
				seed += dot(seed, seed.yxz + 19.19);
				return frac((seed.xxy + seed.yxx) * seed.zyx);
			}

			half _Glossiness;
			half _Metallic;

			// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
			// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
			// #pragma instancing_options assumeuniformscaling
			UNITY_INSTANCING_BUFFER_START(Props)
				// put more per-instance properties here
			UNITY_INSTANCING_BUFFER_END(Props)

			void vert(inout appdata_full v) {

				// randomizedWPos is center of object in world space, then randomized using the position as a seed
				fixed3 randomizedWPos = random(mul(unity_ObjectToWorld, float4(0, 0, 0, 1)).xyz);

				// randomizedWPos is then used to modify UVs against a colour palette texture, this is less efficient than just using
				// a random noise generator, but it gives us ultimate control over the output color
				fixed3 colSampled = tex2Dlod(_ColorPalette, float4(randomizedWPos.r + randomizedWPos.b, randomizedWPos.g + randomizedWPos.b , 0, 0));

				// Pass through as vertex color to fragment/surface
				v.color.rgb = saturate(colSampled);
			}

			void surf(Input IN, inout SurfaceOutputStandard o) {
				// Albedo comes from a texture tinted by vertex color - in this case, it's randomized from a palette based on worldpos
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
				o.Albedo = (c.rgb * IN.color.rgb * (1 - c.a)) + (c.rgb * c.a);
				o.Metallic = _Metallic;
				o.Smoothness = _Glossiness;
			}
			ENDCG
		}
			FallBack "Diffuse"
}