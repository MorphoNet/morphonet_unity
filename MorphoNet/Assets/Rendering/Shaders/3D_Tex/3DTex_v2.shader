Shader "3DTexture/3DTex_v2"
{	Properties{
		_MainTex("Texture", 3D) = "white" {}
		_Transparency("Transparency", Range(0,1)) = 1
		_SamplingQuality("Sampling Quality", int) = 300
		_Threshold("Threshold min", Range(0,1)) = 0
		_ThresholdMax("Threshold max", Range(0,1)) = 1
		_Intensity("Intensity", Range(0,2)) = 1


		_FreeClippingPlaneActivated("Activate Free Clipping Plane", int) = 0
		_FreeClippingPlane("Free Clipping Plane", Vector) = (0,0,0,0)

		_OffsetPlan("OffsetPlan", Vector) = (0,0,0)


		_ColorBarTex("ColorBarTex",2D) = "white" {}

		_NoPlanMode("No Plans Mode",int) = 1
		_StepSize("Step Size", float) = 0.003
}
SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
			}

			CGINCLUDE
			#include "UnityCG.cginc"

			// Maximum amount of raymarching samples
			#define MAX_STEP_COUNT 128

			// Allowed floating point inaccuracy
			#define EPSILON 0.00001f

			int _SamplingQuality;
			sampler3D _MainTex;
			uniform float4 _MainTex_ST;
			float _Transparency;
			float _Front;
			float _Threshold;
			float _ThresholdMax;
			float _Intensity;

			int _FreeClippingPlaneActivated;
			float4 _FreeClippingPlane;


			float3 _OffsetPlan;
			sampler2D _ColorBarTex;
			int _NoPlanMode;
			float _StepSize;

			//new depth stuff
			sampler2D _CameraDepthTexture;

			struct v2f
			{
				float4 pos : SV_POSITION;
				float3 localPos : TEXCOORD0;
				float4 screenPos : TEXCOORD1;
				float3 camRelativeWorldPos : TEXCOORD2;
			};

			v2f vert(appdata_base v)
			{
				v2f OUT;
				OUT.pos = UnityObjectToClipPos(v.vertex);
				OUT.localPos = v.vertex.xyz;
				OUT.screenPos = ComputeScreenPos(OUT.pos);
				//COMPUTE_EYEDEPTH(OUT.screenPos.z);
				//OUT.camRelativeWorldPos = mul(unity_ObjectToWorld, v.vertex) - _WorldSpaceCameraPos;
				OUT.camRelativeWorldPos = mul(unity_ObjectToWorld, float4(v.vertex.xyz, 1.0)).xyz - _WorldSpaceCameraPos;
				return OUT;
			}


			float4 BlendUnder(float4 color, float4 newColor)
			{
				color.rgb += (1.0 - color.a) * newColor.a * newColor.rgb;
				color.a += (1.0 - color.a) * newColor.a;
				return color;
			}

			float4 frag(v2f IN) : COLOR
			{
				//depth
				float2 screenUV = IN.screenPos.xy / IN.screenPos.w;
				float cameraDepth = tex2D(_CameraDepthTexture, screenUV).r;
				float eyeDepthLinear = LinearEyeDepth(cameraDepth);
				
				// credit to bgolus:
				// calculate the view plane vector
                // note: Something like normalize(i.camRelativeWorldPos.xyz) is what you'll see other
                // examples do, but that is wrong! You need a vector that at a 1 unit view depth, not
                // a 1 unit magnitude.
				float3 viewPlane = IN.camRelativeWorldPos.xyz / dot(IN.camRelativeWorldPos.xyz, unity_WorldToCamera._m20_m21_m22);

				// calculate the world position of the last depth buffer from the other meshes
				// multiply the view plane by the linear depth to get the camera relative world space position
				// add the world space camera position to get the world space position from the depth texture
				float3 maximumDepthWorldPos = viewPlane * eyeDepthLinear + _WorldSpaceCameraPos;
				maximumDepthWorldPos = mul(unity_CameraToWorld, float4(maximumDepthWorldPos, 1.0));// -_WorldSpaceCameraPos;

				//base
				float3 localCameraPosition = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos, 1.0));

				//new stuff
				float3 rayOrigin = IN.localPos;

				float3 rayDirection = normalize(IN.localPos - localCameraPosition);

				float3 start = rayOrigin;

				float4 color = float4(0,0,0,0);//xyz w == rgb a

				float4 offset = float4(0.5f, 0.5f, 0.5f, 1);

				[loop]
				for (int i = 0; i < _SamplingQuality; i++)
				{
					float3 vstart = mul((float3x3)unity_ObjectToWorld, start - localCameraPosition);

					// Accumulate color only within unit cube bounds
					if (max(abs(start.x), max(abs(start.y), abs(start.z))) < 0.5f + EPSILON) {
						if (length(vstart) < length(maximumDepthWorldPos - _WorldSpaceCameraPos)) {
							float3 pos = start.xyz;
							pos.xyz += offset;

							float3 invPos = mul(unity_ObjectToWorld, start) + _OffsetPlan;

							//float4 sampledColor = tex3D(_MainTex, pos + float3(_MainTex_ST.zw, 0));
							float4 sampledColor = tex3D(_MainTex, pos);
							if (sampledColor.r >= _Threshold && sampledColor.r <= _ThresholdMax) {
								sampledColor.a *= _Transparency;
								color = BlendUnder(color, sampledColor);

							}
							start += rayDirection * _StepSize;
						}
					}

				}


				color.r *= _Intensity;
				color.g = color.r;
				color.b = color.r;

				/*if (color.w < _Threshold) {
					discard;
				}*/
				float4 colorize = tex2D(_ColorBarTex, float2(color.x, 0));
				color.xyz = colorize.xyz;

				return color;

			}
			ENDCG

			Pass
			{
				Cull Back
				Blend SrcAlpha OneMinusSrcAlpha // Premultiplied transparency

				ZWrite false

				CGPROGRAM
				#pragma target 3.0
				#pragma vertex vert
				#pragma fragment frag

				ENDCG
			}
		}

			Fallback "Diffuse"
}