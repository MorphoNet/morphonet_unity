﻿Shader "3DTexture/3DTex_3C"
{	Properties{

		_MainTex("Texture", 3D) = "white" {}
		_XSize("X texture Size",int) = 256
		_YSize("Y texture Size",int) = 256
		_ZSize("Z texture Size",int) = 256
		_TransparencyC0("Transparency C0", Range(0,1)) = 1
		_ThresholdC0("Threshold min C0", Range(0,1)) = 0
		_ThresholdMaxC0("Threshold max C0", Range(0,1)) = 1
		_IntensityC0("Intensity C0", Range(0,20)) = 1
		_ColorBarTexC0("ColorBarTex C0",2D) = "white" {}
		_AlphaXMinC0("Alpha X min C0",Range(0,1)) = 0
		_AlphaXMaxC0("Alpha X max C0",Range(0,1)) = 1
		_AlphaYMinC0("Alpha Y min C0",Range(0,1)) = 0
		_AlphaYMaxC0("Alpha Y max C0",Range(0,1)) = 1
		_MinScalingC0("Min scaling C0",Range(0,1)) = 0
		_MaxScalingC0("Max scaling C0",Range(0,1)) = 1
		[Toggle]_C0("Channel 0 active",int) = 1


		_TransparencyC1("Transparency C1", Range(0,1)) = 1
		_ThresholdC1("Threshold min C1", Range(0,1)) = 0
		_ThresholdMaxC1("Threshold max C1", Range(0,1)) = 1
		_IntensityC1("IntensityC1", Range(0,20)) = 1
		_ColorBarTexC1("ColorBarTex C1",2D) = "white" {}
		_AlphaXMinC1("Alpha X min C1",Range(0,1)) = 0
		_AlphaXMaxC1("Alpha X max C1",Range(0,1)) = 1
		_AlphaYMinC1("Alpha Y min C1",Range(0,1)) = 0
		_AlphaYMaxC1("Alpha Y max C1",Range(0,1)) = 1
		_MinScalingC1("Min scaling C1",Range(0,1)) = 0
		_MaxScalingC1("Max scaling C1",Range(0,1)) = 1
		[Toggle]_C1("Channel 1 active",int) = 1


		_TransparencyC2("Transparency C2", Range(0,1)) = 1
		_ThresholdC2("Threshold min C2", Range(0,1)) = 0
		_ThresholdMaxC2("Threshold max C2", Range(0,1)) = 1
		_IntensityC2("IntensityC2", Range(0,20)) = 1
		_ColorBarTexC2("ColorBarTex C2",2D) = "white" {}
		_AlphaXMinC2("Alpha X min C2",Range(0,1)) = 0
		_AlphaXMaxC2("Alpha X max C2",Range(0,1)) = 1
		_AlphaYMinC2("Alpha Y min C2",Range(0,1)) = 0
		_AlphaYMaxC2("Alpha Y max C2",Range(0,1)) = 1
		_MinScalingC2("Min scaling C2",Range(0,1)) = 0
		_MaxScalingC2("Max scaling C2",Range(0,1)) = 1
		[Toggle]_C2("Channel 2 active",int) = 1

		_TransparencyC3("Transparency C3", Range(0,1)) = 1
		_ThresholdC3("Threshold min C3", Range(0,1)) = 0
		_ThresholdMaxC3("Threshold max C3", Range(0,1)) = 1
		_IntensityC3("IntensityC3", Range(0,20)) = 1
		_ColorBarTexC3("ColorBarTex C3",2D) = "white" {}
		_AlphaXMinC3("Alpha X min C3",Range(0,1)) = 0
		_AlphaXMaxC3("Alpha X max C3",Range(0,1)) = 1
		_AlphaYMinC3("Alpha Y min C3",Range(0,1)) = 0
		_AlphaYMaxC3("Alpha Y max C3",Range(0,1)) = 1
		_MinScalingC3("Min scaling C3",Range(0,1)) = 0
		_MaxScalingC3("Max scaling C3",Range(0,1)) = 1
		[Toggle]_C3("Channel 3 active",int) = 1


		[Toggle]_ClippingPlaneActivated("Activate Clipping Plane", int) = 0
		[Toggle]_ClippingPlaneThin("Clipping Plane thin mode", int) = 0
		_Thickness("Clipping Plane thickness", Range(0.01,2)) = 0.05
		_ClippingPlane("Clipping Plane", Vector) = (0,0,0,0)
		_OffsetPlan("OffsetPlan", Vector) = (0,0,0)

		_SamplingQuality("Sampling Quality", int) = 400
		_StepSize("Step Size", float) = 0.003
}
SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
			}

			CGINCLUDE
			#include "UnityCG.cginc"

			#define MAX_VAL 3.402823466e+38F

			// Allowed floating point inaccuracy
			#define EPSILON 0.00001f
			#define MAX_SAMPLES 500

			int _SamplingQuality;
			sampler3D _MainTex;
			float _XSize;
			float _YSize;
			float _ZSize;
			float _TransparencyC0;
			float _ThresholdC0;
			float _ThresholdMaxC0;
			float _IntensityC0;
			sampler2D _ColorBarTexC0;
			float _AlphaXMinC0;
			float _AlphaXMaxC0;
			float _AlphaYMinC0;
			float _AlphaYMaxC0;
			float _MinScalingC0;
			float _MaxScalingC0;
			int _C0;

			//sampler3D _MainTexC1;
			float _TransparencyC1;
			float _ThresholdC1;
			float _ThresholdMaxC1;
			float _IntensityC1;
			sampler2D _ColorBarTexC1;
			float _AlphaXMinC1;
			float _AlphaXMaxC1;
			float _AlphaYMinC1;
			float _AlphaYMaxC1;
			float _MinScalingC1;
			float _MaxScalingC1;
			int _C1;

			//sampler3D _MainTexC2;
			float _TransparencyC2;
			float _ThresholdC2;
			float _ThresholdMaxC2;
			float _IntensityC2;
			sampler2D _ColorBarTexC2;
			float _AlphaXMinC2;
			float _AlphaXMaxC2;
			float _AlphaYMinC2;
			float _AlphaYMaxC2;
			float _MinScalingC2;
			float _MaxScalingC2;
			int _C2;

			float _TransparencyC3;
			float _ThresholdC3;
			float _ThresholdMaxC3;
			float _IntensityC3;
			sampler2D _ColorBarTexC3;
			float _AlphaXMinC3;
			float _AlphaXMaxC3;
			float _AlphaYMinC3;
			float _AlphaYMaxC3;
			float _MinScalingC3;
			float _MaxScalingC3;
			int _C3;

			int _ClippingPlaneActivated;
			int _ClippingPlaneThin;
			float4 _ClippingPlane;
			float _Thickness;


			float3 _OffsetPlan;

			int _NoPlanMode;
			float _StepSize;

			//new depth stuff
			sampler2D _CameraDepthTexture;

			struct v2f
			{
				float4 pos : SV_POSITION;
				float3 localPos : TEXCOORD0;
				float4 screenPos : TEXCOORD1;
				float3 camRelativeWorldPos : TEXCOORD2;
			};

			v2f vert(appdata_base v)
			{
				v2f OUT;
				OUT.pos = UnityObjectToClipPos(v.vertex);
				OUT.localPos = v.vertex.xyz;
				OUT.screenPos = ComputeScreenPos(OUT.pos);
				OUT.camRelativeWorldPos = mul(unity_ObjectToWorld, float4(v.vertex.xyz, 1.0)).xyz - _WorldSpaceCameraPos;
				return OUT;
			}


			float4 BlendUnder(float4 color, float4 newColor)
			{
				color.rgb += (1.0 - color.a) * newColor.a * newColor.rgb;
				color.a += (1.0 - color.a) * newColor.a;
				return color;
			}

			float3 BoxRayIntersect(float3 vstart, float3 dir, float3 boxMin, float3 boxMax)
			{
				float3 start = vstart + 0.1f * dir;
				float3 invDir = 1.0f / dir;

				// Find the ray intersection with box plane
				float3 firstPlaneIntersect = (boxMin - start) * invDir;
				float3 secondPlaneIntersect = (boxMax - start) * invDir;

				// Get the closest/furthest of these intersections along the ray (Ok because x/0 give +inf and -x/0 give �inf )
				float3 closestPlane = min(firstPlaneIntersect, secondPlaneIntersect);

				float3 furthestPlane = max(firstPlaneIntersect, secondPlaneIntersect);


				float2 intersections;
				// Find the furthest near intersection
				intersections.x = max(closestPlane.x, max(closestPlane.y, closestPlane.z));
				// Find the closest far intersection
				intersections.y = min(min(furthestPlane.x, furthestPlane.y), furthestPlane.z);

				float3 exit = start + intersections.y * dir;

				//return intersections;
				//return furthestPlane == start ? closestPlane : furthestPlane;
				return exit;
			}

			float4 frag(v2f IN) : COLOR
			{
				// Some vector constants for the shader:
				const float4 f4EPSILON = float4(EPSILON, EPSILON, EPSILON, EPSILON);
				const float4 f4ZERO = float4(0, 0, 0, 0);
				const float4 f4ONE = float4(1, 1, 1, 1);

				// First of all, setup the color and alpha functions as they
				// do not depend on anything done in the ray-marching pass.
				bool4 bActiveColorChannels = bool4(
					_C0 == 1,
					_C1 == 1,
					_C2 == 1,
					_C3 == 1
				);
				// Very-early return:
				if (!any(bActiveColorChannels)) {
					return float4(0, 0, 0, 0);
				}

				bool planeActive = _ClippingPlaneActivated == 1;
				bool planeThin = _ClippingPlaneThin == 1;

				// Color scaling:
				const float4 _DataMinScaling = float4(_MinScalingC0, _MinScalingC1, _MinScalingC2, _MinScalingC3);
				const float4 _DataMaxScaling = float4(_MaxScalingC0, _MaxScalingC1, _MaxScalingC2, _MaxScalingC3);

				// Alpha functions per-channel:
				const float4 _AlphaYMax = float4(_AlphaYMaxC0, _AlphaYMaxC1, _AlphaYMaxC2, _AlphaYMaxC3);
				const float4 _AlphaYMin = float4(_AlphaYMinC0, _AlphaYMinC1, _AlphaYMinC2, _AlphaYMinC3);
				const float4 _AlphaXMax = float4(_AlphaXMaxC0, _AlphaXMaxC1, _AlphaXMaxC2, _AlphaXMaxC3);
				const float4 _AlphaXMin = float4(_AlphaXMinC0, _AlphaXMinC1, _AlphaXMinC2, _AlphaXMinC3);
				const float4 _AlphaA = (_AlphaYMax - _AlphaYMin) / (clamp(_AlphaXMax, f4EPSILON, f4ONE) - _AlphaXMin);
				const float4 _AlphaB = _AlphaYMin - _AlphaA * _AlphaXMin;

				//depth
				float2 screenUV = IN.screenPos.xy / IN.screenPos.w;
				float cameraDepth = tex2D(_CameraDepthTexture, screenUV).r;
				float eyeDepthLinear = LinearEyeDepth(cameraDepth);

				// credit to bgolus:
				// calculate the view plane vector
				// note: Something like normalize(i.camRelativeWorldPos.xyz) is what you'll see other
				// examples do, but that is wrong! You need a vector that at a 1 unit view depth, not
				// a 1 unit magnitude.
				float3 viewPlane = IN.camRelativeWorldPos.xyz / dot(IN.camRelativeWorldPos.xyz, unity_WorldToCamera._m20_m21_m22);

				// calculate the world position of the last depth buffer from the other meshes
				// multiply the view plane by the linear depth to get the camera relative world space position
				// add the world space camera position to get the world space position from the depth texture
				float3 maximumDepthWorldPos = viewPlane * eyeDepthLinear;// +_WorldSpaceCameraPos;
				maximumDepthWorldPos = mul(unity_CameraToWorld, float4(maximumDepthWorldPos, 1.0));

				//base
				float3 localCameraPosition = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos, 1.0));

				//new stuff
				float3 rayOrigin = IN.localPos;
				float3 rayDirection = normalize(rayOrigin - localCameraPosition);
				float3 rayOffset = float3(0.5f, 0.5f, 0.5f);
				float4 rayColor = float4(0, 0, 0, 0);
				float3 rayCurrentPosition = rayOrigin;

				// Setup some bounds for the shader loop
				const float maxRayMarchingDepth = length(maximumDepthWorldPos - _WorldSpaceCameraPos);
				const float3 maxRayMarchingBound = float3(0.5f + EPSILON, 0.5f + EPSILON, 0.5f + EPSILON);


				int3 TexSize = int3(_XSize, _YSize, _ZSize);

				float3 initPosTexel = (rayOrigin.xyz) * TexSize;
				//float3 rayPositionTexel = float3(floor(initPosTexel + 0.0f));
				float3 rayPositionTexel = float3((initPosTexel + 0.0f));
				float3 rayDirectionTexel = rayDirection * TexSize.xyz;

				//float3 movedO = rayOrigin + 0.001f * rayDirection;
				float3 endRayPosTexel = BoxRayIntersect(rayOrigin, rayDirection, float3(-0.5f, -0.5f, -0.5f), float3(0.5f, 0.5f, 0.5f));
				
				endRayPosTexel *= TexSize;

				float3 test = endRayPosTexel - initPosTexel;

				float dx = endRayPosTexel.x - initPosTexel.x;
				float dy = endRayPosTexel.y - initPosTexel.y;
				float dz = endRayPosTexel.z - initPosTexel.z;

				float locStep;
				if (abs(dx) >= abs(dy) && abs(dx) >= abs(dz))
					locStep = abs(dx);
				else if (abs(dy) >= abs(dz) && abs(dy) >= abs(dz))
					locStep = abs(dy);
				else
					locStep = abs(dz);

				if (locStep > MAX_SAMPLES) {
					locStep = MAX_SAMPLES;
				}
					

				if (locStep != 0) {
					dx /= locStep;
					dy /= locStep;
					dz /= locStep;
				}

				int samples = int(ceil(locStep));

				[loop]
				for (int i = 0; i < samples; i++)
				{

					rayCurrentPosition = rayPositionTexel.xyz / TexSize.xyz;
					float3 rayPosition_ViewSpace = mul((float3x3)unity_ObjectToWorld, rayCurrentPosition - localCameraPosition);

					// ---------------- Accumulate color only within unit cube bounds ----------------
					// N.B: `all(step())` ensures all components of the bvec3 returned by `step()` are
					// set to 1 and `step(left, right)` is 1 on its components only if `left >= right`
					if (max(abs(rayCurrentPosition.x), max(abs(rayCurrentPosition.y), abs(rayCurrentPosition.z))) < 0.5f) {
						// Note: maybe need `step(...).asbool()` above.
						if (length(rayPosition_ViewSpace) < maxRayMarchingDepth) {
							float3 pos = rayCurrentPosition.xyz + rayOffset.xyz;
							float3 invPos = mul(unity_ObjectToWorld, rayCurrentPosition) + _OffsetPlan;
							float clip = dot(invPos, _ClippingPlane.xyz) - _ClippingPlane.w;


							//check if visible with cutting plane
							if (!planeActive || (planeActive && !planeThin && clip >= 0) || (planeActive && planeThin && clip > -_Thickness && clip < _Thickness))
							{
								// Get the raw value from the image and 'process' it:
								float4 sampledData = tex3D(_MainTex, pos);

								float4 sampledFilteredData = (f4ONE / (_DataMaxScaling - _DataMinScaling) * (sampledData - _DataMinScaling));
								// Get the colorbar's color:
								float3 barColor0 = tex2D(_ColorBarTexC0, float2(sampledFilteredData.x, 0)).rgb * _IntensityC0;
								float3 barColor1 = tex2D(_ColorBarTexC1, float2(sampledFilteredData.y, 0)).rgb * _IntensityC1;
								float3 barColor2 = tex2D(_ColorBarTexC2, float2(sampledFilteredData.z, 0)).rgb * _IntensityC2;
								float3 barColor3 = tex2D(_ColorBarTexC3, float2(sampledFilteredData.w, 0)).rgb * _IntensityC3;

								// If below the AlphaMin, only apply the 'b' factor of linear function 'ax+b', otherwise
								// apply the function and clamp it on [0,1] for all components:
								float4 colorFromData = clamp(((step(_AlphaXMin, sampledFilteredData) * sampledFilteredData) * _AlphaA + _AlphaB), f4ZERO, f4ONE);

								if (bActiveColorChannels.r && sampledData.r >= _ThresholdC0 && sampledData.r < _ThresholdMaxC0 && (sampledFilteredData.x >= 0 && sampledFilteredData.x <= 1)) {
									rayColor = BlendUnder(rayColor, float4(barColor0, colorFromData.r * _TransparencyC0));

								}
								if (bActiveColorChannels.g && sampledData.g >= _ThresholdC1 && sampledData.g < _ThresholdMaxC1 && (sampledFilteredData.y >= 0 && sampledFilteredData.y <= 1)) {
									rayColor = BlendUnder(rayColor, float4(barColor1, colorFromData.g * _TransparencyC1));
								}
								if (bActiveColorChannels.b && sampledData.b >= _ThresholdC2 && sampledData.b < _ThresholdMaxC2 && (sampledFilteredData.z >= 0 && sampledFilteredData.z <= 1)) {
									rayColor = BlendUnder(rayColor, float4(barColor2, colorFromData.b * _TransparencyC2));
								}
								if (bActiveColorChannels.a && sampledData.a >= _ThresholdC3 && sampledData.a < _ThresholdMaxC3 && (sampledFilteredData.w >= 0 && sampledFilteredData.w <= 1)) {
									rayColor = BlendUnder(rayColor, float4(barColor3, colorFromData.a * _TransparencyC3));
								}
								if (rayColor.a >= 1.0f)
									return rayColor;
							}
						}
						else {
							return rayColor;
						}
					}
					//3D DDA
					rayPositionTexel.x += dx;
					rayPositionTexel.y += dy;
					rayPositionTexel.z += dz;
					
				}

				return rayColor;

			}
			ENDCG

			Pass
			{
				Cull Back
				Blend SrcAlpha OneMinusSrcAlpha // Premultiplied transparency

				ZWrite false

				CGPROGRAM
				#pragma target 3.0
				#pragma vertex vert
				#pragma fragment frag

				ENDCG
			}
					}
					Fallback "Diffuse"
}