using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MorphoNet
{
    public class ShaderPresetsManager : MonoBehaviour
    {
        public List<GameObject> PresetMenus;
        private List<List<MaterialPreset>> _presets = new List<List<MaterialPreset>>();
        public List<GameObject> highlighters = new List<GameObject>();

        public void Awake()
        {
            _presets = new List<List<MaterialPreset>>();
            //init the material array now, do it only once!
            int i = 0;
            foreach (Transform g in transform)
            {
                _presets.Add(new List<MaterialPreset>());
                foreach (Transform preset in g)
                {
                    if (preset.GetChild(0) != null)
                    {
                        MaterialPreset m = preset.GetChild(0).gameObject.GetComponent<MaterialPreset>();
                        if (m != null)
                        {
                            _presets[i].Add(m);
                        }
                    }
                }
                i++;
            }
            foreach (GameObject g in highlighters)
                g.SetActive(false);
        }

        //display corresponding menu for preset shaders (ids are identical to drop-down menu)
        public void DisplayPresetsId(int id)
        {
            foreach (GameObject g in PresetMenus)
            {
                g.SetActive(false);
            }

            if (id >= PresetMenus.Count)
            {
                Debug.LogError("ERROR DisplayPresetsId : id given out of range! ");
            }
            else
            {
                PresetMenus[id].SetActive(true);
            }
            InteractionManager.instance.DisplayShaderOptionsMenu();
            InterfaceManager.instance.ObjectsColorManager.chooseShaders(id);

            foreach (GameObject g in highlighters)
                g.SetActive(false);
            highlighters[id].SetActive(true);

            InterfaceManager.instance.ObjectsColorManager.ClearAdvancedMenu();

            _presets[id][0].SetAsSelected();
        }

        public void SetShaderTypeDefaultAsSelected(int id)
        {
            InterfaceManager.instance.ObjectsColorManager.GenerateAdvancedMenu(_presets[id][0]);
        }

        public void ChangeAllPresetsColor(Color c)
        {
            foreach (List<MaterialPreset> lp in _presets)
                foreach (MaterialPreset p in lp)
                {
                    p.SetAndUpdateColor(c);
                }
        }
    }
}