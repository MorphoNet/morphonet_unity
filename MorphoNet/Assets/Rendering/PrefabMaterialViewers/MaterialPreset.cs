using System;
using MorphoNet.Extensions.Unity;
using UnityEngine;
using UnityEngine.Serialization;

namespace MorphoNet
{
    public class MaterialPreset : MonoBehaviour
    {
        [FormerlySerializedAs("onClickEnable")]
        public bool OnClickEnable = true;

        [Serializable]
        public struct ShaderProperty
        {
            public string name;
            public float value;
            public float min;
            public float max;
            public bool whole;
            public bool isColor;
            public Color color;
        }

        [FormerlySerializedAs("properties")]
        public ShaderProperty[] Properties;

        private Color _rgba = new Color();

        /**HOW TO ADD A NEW MATERIAL PREFAB**/
        /*Associate the script to the correct Object (a sphere generally) with the material you want to use as a preset. Then for each parameter of the shader, add a ShaderProperty with the corresponding
         attributes. The name of the property must correspond with the property in the shader code. For instance, if your shader has a property called _Smoothness, the Shaderproperty must be called
        _Smoothness as well*/

        // Start is called before the first frame update
        private void Start()
        {
            if (gameObject.GetComponent<MeshRenderer>().material != null)
            {
                Material currentMat = new Material(gameObject.GetComponent<MeshRenderer>().material);

                foreach (ShaderProperty p in Properties)
                {
                    if (!p.isColor)
                    {
                        if (currentMat.HasProperty(p.name))
                            currentMat.UpdateFloatProperty(p.name, p.value);
                        else
                        {
                            Debug.LogWarning("WARNING : MaterialPreset " + transform.parent.gameObject.name + " does not have a material with the " + p.name + " property! please check the attached material");
                        }
                    }
                    else
                    {
                        if (currentMat.HasProperty(p.name))
                            currentMat.UpdateColorProperty(p.name, p.color);
                        else
                        {
                            Debug.LogWarning("WARNING : MaterialPreset " + transform.parent.gameObject.name + " does not have a material with the " + p.name + " color! please check the attached material");
                        }
                    }
                }

                gameObject.GetComponent<MeshRenderer>().material = currentMat;
            }
            else
            {
                Debug.LogError("Error, object " + transform.parent.gameObject.name + " needs a Material!");
            }
        }

        public void SetAndUpdateColor(Color c)
        {
            _rgba = c;
            gameObject.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", _rgba);
        }

        public void OnMouseDown()
        {
            if (OnClickEnable)
            {
                SetAsSelected();
                InterfaceManager.instance.ObjectsColorManager.AdditionnalParameterPicker.gameObject.SetActive(false);
            }
        }

        public void SetAsSelected()
        {
            InterfaceManager.instance.ObjectsColorManager.GenerateAdvancedMenu(this);

            Material currentMat = new Material(gameObject.GetComponent<MeshRenderer>().material);
            //then set value to preview object
            foreach (ShaderProperty p in Properties)
            {
                if (!p.isColor)
                {
                    if (currentMat.HasProperty(p.name))
                        InterfaceManager.instance.ObjectsColorManager.changeShaderValue(p.name, p.value);
                }
                else
                {
                    if (currentMat.HasProperty(p.name))
                    {
                        InterfaceManager.instance.ObjectsColorManager.changeShaderColor(p.name, p.color);
                    }
                }
            }
        }
    }
}