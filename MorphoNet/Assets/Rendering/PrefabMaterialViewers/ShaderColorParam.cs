using UnityEngine;
using UnityEngine.UI;

namespace MorphoNet
{
    public class ShaderColorParam : MonoBehaviour
    {
        public InputField RValue;
        public InputField GValue;
        public InputField BValue;
        public Text ParameterName;

        public ColorPickerTriangle Picker;

        public void TogglePicker()
        {
            if (Picker.gameObject.activeSelf)
            {
                Picker.gameObject.SetActive(false);
            }
            else
            {
                Picker.gameObject.SetActive(true);
            }
        }

        public void OnclickPickerOptions()
        {
            if (Picker == null)
            {
                Picker = InterfaceManager.instance.ObjectsColorManager.AdditionnalParameterPicker;
                Picker.gameObject.SetActive(true);
                InterfaceManager.instance.ObjectsColorManager.CurrentAdvancedColor = this;
                UpdatePicker();
            }
            else
            {
                TogglePicker();
            }
        }

        public void UpdatePicker()
        {
            if (Picker != null)
            {
                Picker.SetNewColor(new Color(float.Parse(RValue.text) / 255f, float.Parse(GValue.text) / 255f, float.Parse(BValue.text) / 255f, 1.0f));//option for alpha?
            }
            else
            {
                Debug.Log("PICKER NULL!!!");
            }
        }

        public void UpdateFields(Color color)
        {
            RValue.SetTextWithoutNotify(((int)(255f * color.r)).ToString());
            GValue.SetTextWithoutNotify(((int)(255f * color.g)).ToString());
            BValue.SetTextWithoutNotify(((int)(255f * color.b)).ToString());
        }
    }
}