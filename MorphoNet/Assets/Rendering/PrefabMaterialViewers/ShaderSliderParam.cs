using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShaderSliderParam : MonoBehaviour
{
    [SerializeField]
    public InputField Value;
    public Slider Slider;
    public Text ParameterName;

    public void UpdateTextField()
    {
        Value.SetTextWithoutNotify(Slider.value.ToString());
    }


    public void UpdateSlider()
    {
        Slider.SetValueWithoutNotify(float.Parse(Value.text));
    }
    
}
