﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RenderOptions : MonoBehaviour
{

    public Camera cam;
    public Toggle ambientocclusion;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    //AMBIENT OCCLUSION
    public void onAmbientOcclusion()
    {
        if (ambientocclusion.isOn)
        {
            cam.GetComponent<AmplifyOcclusionEffect>().enabled = true;
            //La camera est maitenant en Perspective .... au lieu de Orthographic
        }
        else cam.GetComponent<AmplifyOcclusionEffect>().enabled = false;
    }

    public void onIntensity(Single v)
    {
        cam.GetComponent<AmplifyOcclusionEffect>().Intensity = v;
    }
    public void onRadius(Single v)
    {
        cam.GetComponent<AmplifyOcclusionEffect>().Radius = v;
    }
    public void onPowerExponent(Single v)
    {
        cam.GetComponent<AmplifyOcclusionEffect>().PowerExponent = v;
    }
}
