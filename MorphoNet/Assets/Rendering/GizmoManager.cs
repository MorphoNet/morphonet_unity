﻿using UnityEngine;
using System.Collections.Generic;

namespace MorphoNet
{
    public class GizmosTest
    {
        public static void DrawLine(Vector3 a, Vector3 b, int param_infos_id, string id, int time, Color? color = null)
        {
            if (!GizmoManager.instance.Show)
                return;

            if (!GizmoManager.instance.lines.ContainsKey(time))
                GizmoManager.instance.lines[time] = new List<GizmoManager.GizmoLine>();
            GizmoManager.instance.lines[time].Add(new GizmoManager.GizmoLine(a, b, color ?? Color.black, id, time, param_infos_id));
        }

        public static void DrawVector(Vector3 a, Vector3 b, int param_infos_id, string id, int time, Color? color = null)
        {
            if (!GizmoManager.instance.Show)
                return;

            if (!GizmoManager.instance.vectors.ContainsKey(time))
                GizmoManager.instance.vectors[time] = new List<GizmoManager.GizmoLine>();
            GizmoManager.instance.vectors[time].Add(new GizmoManager.GizmoLine(a, b, color ?? Color.black, id, time, param_infos_id));
            DrawArrowHead(a, b, id, time, color ?? Color.black, param_infos_id);
        }

        public static void DrawArrowHead(Vector3 source, Vector3 dest, string id, int time, Color color, int param_infos_id)
        {
            if (!GizmoManager.instance.Show)
                return;

            //  DrawPolygon(position, radius, 18, id, time,color, param_infos_id);
            //compute line direction (angle)
            //apply direction to a distance, rotating around vector, from destination (distance = x% from the origin -> destination)

            //add a vector going from origin to new destination

            Vector3 direction = source - dest;

            Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + 20.0f, 0) * new Vector3(0, 0, 1) * direction.magnitude;
            Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - 20.0f, 0) * new Vector3(0, 0, 1) * direction.magnitude;

            GizmoManager.instance.vectors[time].Add(new GizmoManager.GizmoLine(source, (source + right * 0.25f), color, id, time, param_infos_id));
            GizmoManager.instance.vectors[time].Add(new GizmoManager.GizmoLine(source, (source + left * 0.25f), color, id, time, param_infos_id));
        }
    }

    //Must be attached to a camera.
    public class GizmoManager : MonoBehaviour
    {
        public static GizmoManager instance = null;

        [Range(0.01f, 1)]
        public float arrowHeadSize;

        public void Awake()
        {
            if (instance == null)
                instance = this;
            else
                Destroy(gameObject);
        }

        public struct GizmoLine
        {
            public Vector3 a;
            public Vector3 b;
            public Color color;
            public string cell_id;
            public int cell_time;
            public int infos_id;

            public GizmoLine(Vector3 a, Vector3 b, Color color, string param_id, int param_time, int param_infos_id)
            {
                this.a = a;
                this.b = b;
                this.color = color;
                cell_id = param_id;
                cell_time = param_time;
                infos_id = param_infos_id;
            }
        }

        public bool showGizmos = true;
        public Material material;
        public Dictionary<int, List<GizmoLine>> lines = new Dictionary<int, List<GizmoLine>>();
        public Dictionary<int, List<GizmoLine>> vectors = new Dictionary<int, List<GizmoLine>>();

        public bool Show { get; private set; }

        private void Update()
        {
            Show = showGizmos;
        }

        public void ClearVectors()
        {
            vectors.Clear();
        }

        public void ClearLines()
        {
            lines.Clear();
        }

        public void DeleteInfos(int id)
        {
            foreach (int k in lines.Keys)
            {
                lines[k].RemoveAll(item1 => item1.infos_id == id);
            }

            foreach (int j in vectors.Keys)
            {
                vectors[j].RemoveAll(item2 => item2.infos_id == id);
            }
        }

        private void OnPostRender()
        {
            //activate material rendering
            material.SetPass(0);

            DataSet dataset = MorphoTools.GetDataset();

            if (dataset != null && lines != null && lines.ContainsKey(dataset.CurrentTime) && lines[dataset.CurrentTime].Count > 0)
            {
                //Start drawing lines
                GL.Begin(GL.LINES);

                //for each registered line
                for (int i = 0; i < lines[dataset.CurrentTime].Count; i++)
                {
                    var line = lines[dataset.CurrentTime][i];
                    Cell cell = dataset.CellsByTimePoint[lines[dataset.CurrentTime][i].cell_time].Find(c => c.ID == line.cell_id);
                    if (cell.show)
                    {
                        //apply transformations matrix to origin and dest of the lines
                        Vector3 origin = lines[dataset.CurrentTime][i].a;
                        Vector3 destination = lines[dataset.CurrentTime][i].b;
                        origin.Scale(MorphoTools.GetTransformationsManager().CurrentScale);
                        origin = MorphoTools.GetTransformationsManager().CurrentRotation * origin;
                        origin += new Vector3(MorphoTools.GetTransformationsManager().CurrentPosition.x, MorphoTools.GetTransformationsManager().CurrentPosition.y, MorphoTools.GetTransformationsManager().CurrentPosition.z);

                        destination.Scale(MorphoTools.GetTransformationsManager().CurrentScale);
                        destination = MorphoTools.GetTransformationsManager().CurrentRotation * destination;
                        destination += new Vector3(MorphoTools.GetTransformationsManager().CurrentPosition.x, MorphoTools.GetTransformationsManager().CurrentPosition.y, MorphoTools.GetTransformationsManager().CurrentPosition.z);

                        //draw the line with the color
                        GL.Color(lines[dataset.CurrentTime][i].color);
                        GL.Vertex(origin);
                        GL.Vertex(destination);
                    }
                }

                GL.End();
                //lines.Clear();
            }

            if (dataset != null && vectors != null && vectors.ContainsKey(dataset.CurrentTime) && vectors[dataset.CurrentTime].Count > 0)
            {
                //Start drawing lines
                GL.Begin(GL.LINES);

                //for each registered line
                for (int i = 0; i < vectors[dataset.CurrentTime].Count; i++)
                {
                    var vector = vectors[dataset.CurrentTime][i];
                    Cell cell = dataset.CellsByTimePoint[lines[dataset.CurrentTime][i].cell_time].Find(c => c.ID == vector.cell_id);
                    if (cell.show)
                    {
                        //apply transformations matrix to origin and dest of the lines
                        Vector3 origin = vectors[dataset.CurrentTime][i].a;
                        Vector3 destination = vectors[dataset.CurrentTime][i].b;
                        origin.Scale(MorphoTools.GetTransformationsManager().CurrentScale);
                        origin = MorphoTools.GetTransformationsManager().CurrentRotation * origin;
                        origin += new Vector3(MorphoTools.GetTransformationsManager().CurrentPosition.x, MorphoTools.GetTransformationsManager().CurrentPosition.y, MorphoTools.GetTransformationsManager().CurrentPosition.z);

                        destination.Scale(MorphoTools.GetTransformationsManager().CurrentScale);
                        destination = MorphoTools.GetTransformationsManager().CurrentRotation * destination;
                        destination += new Vector3(MorphoTools.GetTransformationsManager().CurrentPosition.x, MorphoTools.GetTransformationsManager().CurrentPosition.y, MorphoTools.GetTransformationsManager().CurrentPosition.z);

                        //draw the line with the color
                        GL.Color(vectors[dataset.CurrentTime][i].color);
                        GL.Vertex(origin);
                        GL.Vertex(destination);
                    }
                }

                GL.End();
                //lines.Clear();
            }
        }
    }
}