using MorphoNet.XR.Timeline;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace MorphoNet.Core
{
    /// <summary>
    /// Place on the pivot
    /// </summary>
    public class TimePoint : MonoBehaviour
    {
        [SerializeField]
        private TimelineTile _Tile;

        [SerializeField]
        private Transform _Pivot;

        public Transform Pivot { get => _Pivot; private set => _Pivot = value; }

        private Transform TimestampTileTransform => _Tile.transform;

        public int Number { get; private set; }
        public Transform TimePointRoot { get => Pivot.GetChild(0); }

        private readonly List<Cell> _Cells = new List<Cell>();
        private bool _Visible = false;

        private readonly Queue<Cell> _CellToUpdate = new Queue<Cell>();

        public void AddCellToRendererUpdate(Cell cell)
        {
            if (_Cells.Contains(cell) && !_CellToUpdate.Contains(cell))
            {
                _CellToUpdate.Enqueue(cell);
            }
        }

        private void Update()
        {
            if (_Visible)
            {
                while (_CellToUpdate.Count() != 0)
                {
                    _CellToUpdate.Dequeue().UpdateAllChannel();
                }
            }
        }

        public void Init(Transform containedTimestamp, int force_nb=-1)
        {
            transform.localScale = Vector3.one;
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;

            name = $"{containedTimestamp.name}Behaviour";
            containedTimestamp.SetParent(Pivot);

            DataSet ds = SetsManager.instance.DataSet;

            int number;
            if (containedTimestamp.childCount > 0)
            {
                ds.TryParseCellName(containedTimestamp.GetChild(0).name, out _, out number);
            }
            else
            {
                number = force_nb;
            }
            if (number != -1)
            {
                Number = number;
                _Tile.SetTileNumber(Number);

                foreach (Cell cell in ds.CellsByTimePoint[Number])
                {
                    _Cells.Add(cell);
                    cell.TimePoint = this;
                    _CellToUpdate.Enqueue(cell);
                }

                Hide();
            }
            else
            {
                Debug.LogError("ERROR : tried to create time step with time=" + number);
            }
            
            
        }

        public void AddCellsToTimePoint()
        {
            DataSet ds = SetsManager.instance.DataSet;
            foreach (Cell cell in ds.CellsByTimePoint[Number])
            {
                if(!_Cells.Contains(cell))
                    _Cells.Add(cell);
                if(cell.TimePoint==null)
                    cell.TimePoint = this;
                _CellToUpdate.Enqueue(cell);
            }
        }

        public void Hide()
        {
            TimestampTileTransform.gameObject.SetActive(false);
            _Visible = false;
        }

        public void Show()
        {
            TimestampTileTransform.gameObject.SetActive(true);
            _Visible = true;
        }

        public void SetRotation(Quaternion rotation)
        {
            Pivot.transform.rotation = rotation;

            Transform rotationTarget = Camera.main.transform;

            if (XRCameraObject.Instance != null && XRCameraObject.Instance.transform != null)
                rotationTarget = XRCameraObject.Instance.transform;

            _Tile.transform.LookAt(rotationTarget);
        }

        public void SetLocalPosition(Vector3 position)
        {
            Pivot.localPosition = position;

            TimestampTileTransform.localPosition = Pivot.localPosition;
            TimestampTileTransform.LookAt(Camera.main.transform); // TODO: get XR camera
            TimestampTileTransform.localPosition = TimestampTileTransform.localPosition + new Vector3(0, -5, 0);
        }
    }
}