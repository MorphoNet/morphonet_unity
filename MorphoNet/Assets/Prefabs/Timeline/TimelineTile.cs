using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace MorphoNet.XR.Timeline
{
    public class TimelineTile : MonoBehaviour
    {
        [SerializeField]
        private TextMeshPro _TileNumber;

        public void SetTileNumber(int number) => _TileNumber.text = number.ToString();
    }
}