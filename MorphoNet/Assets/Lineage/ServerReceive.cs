using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;


namespace MorphoNet
{
    public class ServerReceive : MonoBehaviour
    {
        public ServerSend ssend;
        public LineageViewer lv;
        public IEnumerator receivePythonCommand(int portNumber)
        {
            WWWForm form = new WWWForm();
            // MorphoDebug.Log("Listening on :" + plot_adress + portNumber);
            UnityWebRequest www = UnityWebRequests.Post("localhost:" + portNumber, "/get.html", form);
            yield return new WaitForSeconds(0.1f);
            //MorphoDebug.Log("wait receive");
            //THE FOLLOWING LINE CRASH AFTER RECEIVING 1 TIME STEP
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                //MorphoDebug.Log("Failed receiving command, starting again the waiting");
                www.Dispose();
                StartCoroutine(receivePythonCommand(portNumber));
            }
            else
            {
                String commande = Encoding.UTF8.GetString(www.downloadHandler.data);
                commande = commande.Replace("%20", " ");
                //MorphoDebug.Log("found commande : " + commande);
                // MorphoDebug.Log("Recieve  " + cmds[0]);
                if (commande.StartsWith("lineage_info"))
                {
                    MorphoDebug.Log("Received lineage: " + commande.ToString());
                    string lineage = commande.Split(';')[1];
                }
                else if (commande.StartsWith("send_times"))
                {
                    string lineage = commande.Split(';')[1];
                }
                else if (commande.StartsWith("select_cells"))
                {
                    string cell = commande.Split(';')[1];
                    InterfaceManager.instance.lineage_viewer.ShowCell(cell);
                }
                www.Dispose();
                StartCoroutine(receivePythonCommand(portNumber));
            }
        }

        void Start()
        {
            StartCoroutine(receivePythonCommand(9800));
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}

