﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Text;
using System.Runtime.InteropServices;

namespace MorphoNet
{
    public class Lineage : MonoBehaviour
    {
        //Lineage in a new winwo
        public static bool isLineage = false;

        public static List<Color> colorToPlot;
        public static List<Cell> basicstoUpdate;
        public GameObject objectSave = null;
        public static Dictionary<Color, List<Cell>> new_colors;
        public static Dictionary<Cell, List<Color>> new_colors_cell;
        public static List<Cell> cell_to_hide;
        public static List<Cell> cell_to_show;
        public static Color NormalColor = Color.gray;
        public static Color SelectedColor = Color.white;

        public static Dictionary<string, List<Cell>> propagate_cells;
        public static Dictionary<string, List<Cell>> show_cells;
        public static Dictionary<string, List<Cell>> clear_cells;
        public static Dictionary<string, List<Cell>> hide_cells;
        //public static Dictionary<BasicObject, List<Color>> new_colors_cell;

        //Editor references to avoid Resources.load
        public Sprite LineageTreeWhite;

        public Sprite EyeCloseWhite;

#if UNITY_WEBGL
	[DllImport("__Internal")]
	private static extern void OpenLineage(int i);

	[DllImport("__Internal")]
	private static extern void CloseLineage();

	[DllImport("__Internal")]
	private static extern void ClearAll();

	[DllImport("__Internal")]
	private static extern void Hide(string tid);

	[DllImport("__Internal")]
	private static extern void Show(string tid);

	[DllImport("__Internal")]
	private static extern void HideCellsAllTime(string vlistcells);

	[DllImport("__Internal")]
	private static extern void Colorize(string tidrgb);

	[DllImport("__Internal")]
	private static extern void Uncolorize(string tid);

	[DllImport("__Internal")]
	private static extern void ApplySelection(string rbglistcells);

	[DllImport("__Internal")]
	private static extern void ApplySelectionMap(string rbglistcells);

	[DllImport("__Internal")]
	private static extern void ResetSelectionOnSelectedCells(string listcells);

	[DllImport("__Internal")]
	private static extern void HideCells(string vlistcells);

	[DllImport("__Internal")]
	private static extern void PropagateThis(string vrgblistcells);

	[DllImport("__Internal")]
	private static extern void SendLineageMorphoPlot(string lineage);

	[DllImport("__Internal")]
	private static extern void ReceiveMaxTime(int max_time);

	[DllImport("__Internal")]
	private static extern void ReceiveMinTime(int min_time);

	[DllImport("__Internal")]
	private static extern void SendInfoToLineage(string infostring);

	[DllImport("__Internal")]
	private static extern void UncolorizeMultiple(string listcells);

	[DllImport("__Internal")]
	private static extern void ColorizeMultiple(string clistcells);

	[DllImport("__Internal")]
	private static extern void updateInfoDeployed(int info_id);

	[DllImport("__Internal")]
	private static extern void multipleSelection(string listcellscolor);

	[DllImport("__Internal")]
	private static extern void resetLineage();

	[DllImport("__Internal")]
	private static extern void ReceiveColorInfos(string iddata);

	[DllImport("__Internal")]
	private static extern void SendColors(string listcellscolor);

	[DllImport("__Internal")]
	private static extern void SendShowCells(string celllist);

	[DllImport("__Internal")]
	private static extern void SendHideCells(string celllist);

	[DllImport("__Internal")]
	private static extern void SendPropagateTime(string celllist);
	[DllImport("__Internal")]
	private static extern void clearCells(string celllist);
	[DllImport("__Internal")]
	private static extern void SendBackgroundColor(string color);
	[DllImport("__Internal")]
	private static extern void updateSelectionColor(string parames);
	[DllImport("__Internal")]
	private static extern void cancelPropagationListCells(string parames);
	[DllImport("__Internal")]
	private static extern void pickCells(string parames);

#else

        private static void OpenLineage(int i)
        { }

        private static void CloseLineage()
        { }

        private static void ClearAll()
        { }

        private static void Hide(string tid)
        { }

        private static void Show(string tid)
        { }

        private static void HideCellsAllTime(string vlistcells)
        { }

        private static void Colorize(string tidrgb)
        { }

        private static void Uncolorize(string tid)
        { }

        private static void ApplySelection(string rbglistcells)
        { }

        private static void ApplySelectionMap(string rbglistcells)
        { }

        private static void ResetSelectionOnSelectedCells(string listcells)
        { }

        private static void HideCells(string vlistcells)
        { }

        private static void PropagateThis(string vrgblistcells)
        { }

        private static void SendLineageMorphoPlot(string lineage)
        { }

        private static void ReceiveMaxTime(int max_time)
        { }

        private static void ReceiveMinTime(int min_time)
        { }

        private static void ReceiveColorInfos(string iddata)
        { }

        private static void ColorizeMultiple(string clistcells)
        { }

        private static void SendColors(string listcellscolor)
        { }

        private static void SendShowCells(string celllist)
        { }

        private static void SendHideCells(string celllist)
        { }

        private static void SendPropagateTime(string celllist)
        { }

        private static void clearCells(string celllist)
        { }

        private static void SendBackgroundColor(string color)
        { }

        private static void updateSelectionColor(string parames)
        { }

        private static void cancelPropagationListCells(string parames)
        { }

        private static void pickCells(string parames)
        { }

#endif

        public static bool cmapflag = false;

        private void Start()
        {
            if (colorToPlot == null)
                colorToPlot = new List<Color>();
            else
                colorToPlot.Clear();
            if (new_colors == null)
                new_colors = new Dictionary<Color, List<Cell>>();
            else
                new_colors.Clear();
            if (new_colors_cell == null)
                new_colors_cell = new Dictionary<Cell, List<Color>>();
            else
                new_colors_cell.Clear();
            if (cell_to_hide == null)
                cell_to_hide = new List<Cell>();
            else
                cell_to_hide.Clear();
            if (cell_to_show == null)
                cell_to_show = new List<Cell>();
            else
                cell_to_show.Clear();
            if (basicstoUpdate == null)
                basicstoUpdate = new List<Cell>();
            else
                basicstoUpdate.Clear();

            if (propagate_cells == null)
                propagate_cells = new Dictionary<string, List<Cell>>();
            else
                propagate_cells.Clear();
            if (show_cells == null)
                show_cells = new Dictionary<string, List<Cell>>();
            else
                show_cells.Clear();
            if (clear_cells == null)
                clear_cells = new Dictionary<string, List<Cell>>();
            else
                clear_cells.Clear();
            if (hide_cells == null)
                hide_cells = new Dictionary<string, List<Cell>>();
            else
                hide_cells.Clear();
        }

        public static Color GetCellColor(Cell cell)
        {
            if (MorphoTools.GetDataset().PickedManager.clickedCells.Contains(cell))
            {
                return SelectedColor;
            }
            else if (cell.selection != null && cell.selection.Count > 0)
            {
                return SelectionManager.getSelectedMaterial(cell.selection[cell.selection.Count - 1]).color;
            }
            else if (cell.UploadedColor == true)
            {
                return cell.UploadColor;
            }
            return NormalColor;
        }

        public static Color GetCellColor_No_Selection(Cell cell)
        {
            if (cell.selection != null && cell.selection.Count > 0)
            {
                return SelectionManager.getSelectedMaterial(cell.selection[cell.selection.Count - 1]).color;
            }
            else if (cell.UploadedColor == true)
            {
                return cell.UploadColor;
            }
            return NormalColor;
        }

        public static int GetModificationCount()
        {
            int count = 0;
            if (new_colors != null)
            {
                foreach (KeyValuePair<Color, List<Cell>> pair in new_colors)
                {
                    if (pair.Value != null && pair.Value.Count > 0)
                    {
                        count += pair.Value.Count;
                    }
                }
            }

            if (cell_to_show != null)
            {
                count += cell_to_show.Count;
            }

            if (cell_to_hide != null)
            {
                count += cell_to_hide.Count;
            }
            return count;
        }

        public static void AddToColor(Cell cell, Color color)
        {
            if (new_colors == null)
            {
                new_colors = new Dictionary<Color, List<Cell>>();
            }

            if (!new_colors.ContainsKey(color))
            {
                new_colors.Add(color, new List<Cell>());
            }

            if (new_colors[color] == null)
            {
                new_colors[color] = new List<Cell>();
            }
            if (!new_colors[color].Contains(cell))
            {
                new_colors[color].Add(cell);
            }

            if (new_colors_cell != null && new_colors_cell.ContainsKey(cell))
            {
                foreach (Color c in new_colors_cell[cell])
                {
                    if (c != color)
                        new_colors[c].Remove(cell);
                }
            }

            if (new_colors_cell == null)
            {
                new_colors_cell = new Dictionary<Cell, List<Color>>();
            }
            if (!new_colors_cell.ContainsKey(cell))
            {
                new_colors_cell.Add(cell, new List<Color>());
            }
            new_colors_cell[cell].Add(color);
        }

        public static void AddToClear(string method, Cell cell)
        {
            if (clear_cells == null)
            {
                clear_cells = new Dictionary<string, List<Cell>>();
            }

            if (!clear_cells.ContainsKey(method))
            {
                clear_cells.Add(method, new List<Cell>());
            }

            if (clear_cells[method] == null)
            {
                clear_cells[method] = new List<Cell>();
            }

            if (!clear_cells[method].Contains(cell))
            {
                clear_cells[method].Add(cell);
            }
        }

        public static void AddToHide(string method, Cell cell)
        {
            if (hide_cells == null)
            {
                hide_cells = new Dictionary<string, List<Cell>>();
            }

            if (!hide_cells.ContainsKey(method))
            {
                hide_cells.Add(method, new List<Cell>());
            }

            if (hide_cells[method] == null)
            {
                hide_cells[method] = new List<Cell>();
            }

            if (!hide_cells[method].Contains(cell))
            {
                hide_cells[method].Add(cell);
            }
        }

        public static void AddToShow(string method, Cell cell)
        {
            if (show_cells == null)
            {
                show_cells = new Dictionary<string, List<Cell>>();
            }

            if (!show_cells.ContainsKey(method))
            {
                show_cells.Add(method, new List<Cell>());
            }

            if (show_cells[method] == null)
            {
                show_cells[method] = new List<Cell>();
            }

            if (!show_cells[method].Contains(cell))
            {
                show_cells[method].Add(cell);
            }
        }

        public static void AddToPropagate(string method, Cell cell)
        {
            if (propagate_cells == null)
            {
                propagate_cells = new Dictionary<string, List<Cell>>();
            }

            if (!propagate_cells.ContainsKey(method))
            {
                propagate_cells.Add(method, new List<Cell>());
            }

            if (propagate_cells[method] == null)
            {
                propagate_cells[method] = new List<Cell>();
            }

            if (!propagate_cells[method].Contains(cell))
            {
                propagate_cells[method].Add(cell);
            }
        }

        public static void AddToHide(Cell cell)
        {
            if (cell_to_hide == null)
            {
                cell_to_hide = new List<Cell>();
            }

            if (!cell_to_hide.Contains(cell))
            {
                cell_to_hide.Add(cell);
            }
        }

        public static void AddToShow(Cell cell)
        {
            if (cell_to_show == null)
            {
                cell_to_show = new List<Cell>();
            }

            if (!cell_to_show.Contains(cell))
            {
                cell_to_show.Add(cell);
            }
        }

        public IEnumerator SendPropagate(string c)
        {
#if UNITY_WEBGL
		if (c != null && c != "")
		{
			if (c.Length < 40000)
			{
				SendPropagateTime(">COMPLETESTART" + c);
			}
			else
			{
				SendPropagateTime(">START");
				string info_json = "";
				string[] lineage_lines = c.Split(':');
				foreach (string line in lineage_lines)
				{
					var new_line = line + ":";
					info_json += new_line.Replace("\t", "").Replace("\r", "");
					if (info_json.Length > 40000)
					{
						string temp = info_json;
						yield return new WaitForSeconds(0.01f);
						SendPropagateTime(temp);
						info_json = "";
					}
				}
				string temp2 = info_json;
				yield return new WaitForSeconds(0.01f);
				SendPropagateTime(temp2);
				yield return new WaitForSeconds(0.01f);
				SendPropagateTime("END<");
				cmapflag = true;
			}
		}
		//SetsManager.instance.StartCoroutine(SendInfoChucnked(info_json));
#endif
            yield return null;
        }

        public IEnumerator SendShow(string c)
        {
#if UNITY_WEBGL
		if (c != null && c != "")
		{
			if (c.Length < 40000)
			{
				SendShowCells(">COMPLETESTART" + c);
			}
			else
			{
				SendShowCells(">START");
				string info_json = "";
				string[] lineage_lines = c.Split(':');
				foreach (string line in lineage_lines)
				{
					var new_line = line + ":";
					info_json += new_line.Replace("\t", "").Replace("\r", "");
					if (info_json.Length > 40000)
					{
						string temp = info_json;
						yield return new WaitForSeconds(0.01f);
						SendShowCells(temp);
						info_json = "";
					}
				}
				string temp2 = info_json;
				yield return new WaitForSeconds(0.01f);
				SendShowCells(temp2);
				yield return new WaitForSeconds(0.01f);
				SendShowCells("END<");
				cmapflag = true;
			}
		}
		//SetsManager.instance.StartCoroutine(SendInfoChucnked(info_json));
#endif
            yield return null;
        }

        public IEnumerator SendHide(string c)
        {
#if UNITY_WEBGL
		if (c != null && c != "")
		{
			if (c.Length < 40000)
			{
				SendHideCells(">COMPLETESTART" + c);
			}
			else
			{
				SendHideCells(">START");
				string info_json = "";
				string[] lineage_lines = c.Split(':');
				foreach (string line in lineage_lines)
				{
					var new_line = line + ":";
					info_json += new_line.Replace("\t", "").Replace("\r", "");
					if (info_json.Length > 40000)
					{
						string temp = info_json;
						yield return new WaitForSeconds(0.01f);
						SendHideCells(temp);
						info_json = "";
					}
				}
				string temp2 = info_json;
				yield return new WaitForSeconds(0.01f);
				SendHideCells(temp2);
				yield return new WaitForSeconds(0.01f);
				SendHideCells("END<");
				cmapflag = true;
			}
		}
		//SetsManager.instance.StartCoroutine(SendInfoChucnked(info_json));
#endif
            yield return null;
        }

        public IEnumerator SendNewColors(string c)
        {
#if UNITY_WEBGL
		if (c != null && c != "")
		{
			if (c.Length < 40000)
			{
				SendColors(">COMPLETESTART" + c);
			}
			else
			{
				SendColors(">START");
				string info_json = "";
				string[] lineage_lines = c.Split(':');
				foreach (string line in lineage_lines)
				{
					var new_line = line + ":";
					info_json += new_line.Replace("\t", "").Replace("\r", "");
					if (info_json.Length > 40000)
					{
						string temp = info_json;
						yield return new WaitForSeconds(0.01f);
						SendColors(temp);
						info_json = "";
					}
				}
				string temp2 = info_json;
				yield return new WaitForSeconds(0.01f);
				SendColors(temp2);
				yield return new WaitForSeconds(0.01f);
				SendColors("END<");
				cmapflag = true;
			}
		}
		//SetsManager.instance.StartCoroutine(SendInfoChucnked(info_json));
#endif
            yield return null;
        }

        public static void UpdateWebsiteLineage()
        {
            if (new_colors.Count > 0)
            {
                StringBuilder cell_string = new StringBuilder();

                int nb_cell_colored = 0;
                List<int> timepoints = new List<int>();
                foreach (KeyValuePair<Color, List<Cell>> pair in new_colors)
                {
                    if (pair.Key != null && pair.Value != null)
                    {
                        int select_for_color = MorphoTools.GetDataset().selection_manager.getSelectionFromColor(pair.Key);
                        cell_string.Append(select_for_color.ToString() + "/" + Mathf.Round(pair.Key.r * 255).ToString() + "/" + Mathf.Round(pair.Key.g * 255).ToString() + "/" + Mathf.Round(pair.Key.b * 255).ToString() + "/");

                        foreach (Cell c in pair.Value)
                        {
                            cell_string.Append(c.t + ";" + c.ID + ":");
                            if (!timepoints.Contains(c.t))
                            {
                                timepoints.Add(c.t);
                            }
                            nb_cell_colored++;
                        }
                        cell_string.Append("!");
                    }
                }
                //MorphoDebug.Log(nb_cell_colored + " cells treated through " + timepoints.Count + " timepoints");
                InterfaceManager.instance.StartCoroutine(InterfaceManager.instance.lineage.SendNewColors(cell_string.ToString()));
            }

            if (cell_to_hide.Count > 0)
            {
                StringBuilder cell_string = new StringBuilder();
                int nb_cell_hidden = 0;
                List<int> timepoints = new List<int>();

                foreach (Cell c in cell_to_hide)
                {
                    if (c != null)
                    {
                        cell_string.Append(c.t + ";" + c.ID + ":");
                        if (!timepoints.Contains(c.t))
                        {
                            timepoints.Add(c.t);
                        }
                        nb_cell_hidden++;
                    }
                }
                //MorphoDebug.Log(nb_cell_hidden + " cells hidden through " + timepoints.Count + " timepoints");
                InterfaceManager.instance.StartCoroutine(InterfaceManager.instance.lineage.SendHide(cell_string.ToString()));
            }

            if (cell_to_show.Count > 0)
            {
                StringBuilder cell_string = new StringBuilder();
                int nb_cell_shown = 0;
                List<int> timepoints = new List<int>();

                foreach (Cell c in cell_to_show)
                {
                    if (c != null)
                    {
                        cell_string.Append(c.t + ";" + c.ID + ":");
                        if (!timepoints.Contains(c.t))
                        {
                            timepoints.Add(c.t);
                        }
                        nb_cell_shown++;
                    }
                }

                //MorphoDebug.Log(nb_cell_shown + " cells shown through " + timepoints.Count + " timepoints");
                InterfaceManager.instance.StartCoroutine(InterfaceManager.instance.lineage.SendShow(cell_string.ToString()));
            }

            if (propagate_cells.Count > 0)
            {
                StringBuilder cell_string = new StringBuilder();

                int nb_cell_colored = 0;
                List<int> timepoints = new List<int>();
                foreach (KeyValuePair<string, List<Cell>> pair in propagate_cells)
                {
                    if (pair.Key != null && pair.Value != null)
                    {
                        cell_string.Append(pair.Key + "/");

                        foreach (Cell c in pair.Value)
                        {
                            cell_string.Append(c.t + ";" + c.ID + ":");
                            if (!timepoints.Contains(c.t))
                            {
                                timepoints.Add(c.t);
                            }
                            nb_cell_colored++;
                        }
                        cell_string.Append("!");
                    }
                }
                //MorphoDebug.Log(nb_cell_colored + " cells treated through " + timepoints.Count + " timepoints");
                InterfaceManager.instance.StartCoroutine(InterfaceManager.instance.lineage.SendPropagate(cell_string.ToString()));
            }

            cell_to_show.Clear();
            cell_to_hide.Clear();
            new_colors.Clear();
            new_colors_cell.Clear();
            propagate_cells.Clear();
        }

        //Open a new window with the lineage
        public void showLineage(GameObject st, int id_info)
        {
            if (!isLineage)
            {
                OpenLineage(id_info);
            }
            else
            {
                CloseLineage();
            }
            isLineage = !isLineage;
            if (objectSave != st)
            {
                objectSave = st;
            }

            if (!isLineage)
                st.transform.Find("show").GetComponent<Image>().sprite = LineageTreeWhite;
            else
                st.transform.Find("show").GetComponent<Image>().sprite = EyeCloseWhite;

            if (isLineage)
            {
                if (LoadParameters.instance.id_dataset == 0 && isLineage)
                {
                    //UIManager.instance.UpdateLineageLoading(true,0)
                    StartCoroutine(SendAllInfos());
                    string lineage = MorphoTools.GetDataset().Infos.getLineageMorphoPlot();
                    StartCoroutine(InterfaceManager.instance.lineage.SendLineageChuncks(lineage));
                }
                //UIManager.instance.HideLineageLoading();
                StartCoroutine(SendColorDelayed());
            }
        }

        public void forceLineageClose(int i)
        {
            if (isLineage)
            {
                CloseLineage();
            }

            isLineage = !isLineage;
            if (!isLineage)
                objectSave.transform.Find("show").GetComponent<Image>().sprite = LineageTreeWhite;
        }

        private bool flag = false;

        public IEnumerator SendAllInfos()
        {
            if (MorphoTools.GetDataset().Infos != null)
            {
                int count = 0;
                foreach (Correspondence c in MorphoTools.GetDataset().Infos.Correspondences)
                {
                    //	UIManager.instance.UpdateLineageLoading(true,(int)(count/ MorphoTools.get_dataset().infos.Correspondences.Count * 100));
                    if (c != null && c.datatype != "time")
                    {
                        flag = false;
                        StartCoroutine(SendInfo(c));
                        yield return new WaitUntil(() => flag == true);
                    }
                    count++;
                }
            }
        }

        public IEnumerator SendColorDelayed()
        {
            yield return new WaitForSeconds(2.0f);
            foreach (int i in MorphoTools.GetDataset().CellsByTimePoint.Keys)
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[i])
                {
                    if (cell.selection != null && cell.selection.Count > 0)
                        AddToColor(cell, SelectionManager.getSelectedMaterial(cell.selection[0]).color);
                    else if (cell.UploadedColor == true)
                        AddToColor(cell, cell.UploadColor);
                }
            }
            UpdateWebsiteLineage();
            yield return new WaitForEndOfFrame();
        }

        public void CloseFromWebsite()
        {
        }

        public static void UpdateLineage()
        {
#if UNITY_WEBGL && !UNITY_EDITOR
		InterfaceManager.instance.lineage.StartCoroutine(InterfaceManager.instance.lineage.SendLineageChuncks(MorphoTools.GetDataset().Infos.getLineageMorphoPlot()));
		ReceiveMaxTime(MorphoTools.GetDataset().MaxTime);
		ReceiveMinTime(MorphoTools.GetDataset().MinTime);
		resetLineage();
#endif
        }

        public IEnumerator SendLineageChuncks(string c)
        {
            SendLineageMorphoPlot(">START");
            string info_json = "";
            string[] lineage_lines = c.Split('\n');
            foreach (string line in lineage_lines)
            {
                var new_line = line + "\\n";
                info_json += new_line.Replace("\t", "").Replace("\r", "");
                if (info_json.Length > 40000)
                {
                    string temp = info_json;
                    yield return new WaitForSeconds(0.05f);
                    SendLineageMorphoPlot(temp);
                    info_json = "";
                }
            }
            string temp2 = info_json;
            yield return new WaitForSeconds(0.05f);
            SendLineageMorphoPlot(temp2);
            yield return new WaitForSeconds(0.05f);
            SendLineageMorphoPlot("END<");
            flag = true;
            //SetsManager.instance.StartCoroutine(SendInfoChucnked(info_json));
        }

        public static void UpdateLineageWebsite()
        {
#if UNITY_WEBGL
	resetLineage();
#endif
        }

        //When we click on a cell in the lineafge frame
        public static void showCell(string p)
        {
            Cell overcell = MorphoTools.GetDataset().getCell(p, false);

            if (overcell != null)
            {
                //MorphoDebug.Log ("Cell  found ..." + p);
                MorphoTools.GetPickedManager().ClearSelectedCells();
                MorphoTools.GetPickedManager().ClearClickedCell();
                MorphoTools.GetPickedManager().selectedCell = overcell;
                MorphoTools.GetPickedManager().AddCellSelected(MorphoTools.GetPickedManager().selectedCell);
                if (MorphoTools.GetDataset().CurrentTime != overcell.t)
                    InterfaceManager.instance.setTime(overcell.t);
            }
            else
                MorphoDebug.Log("Cell not found ..." + p);
        }

        public static void colorize(Cell c, Color col)
        {
            Colorize(c.t + "/" + c.ID + "/" + Mathf.Round(col.r * 255) + "/" + Mathf.Round(col.g * 255) + "/" + Mathf.Round(col.b * 255));
            //Application.ExternalEval ("colorize(" + c.t + "," + c.ID + ","+Mathf.Round(col.r*255)+","+Mathf.Round(col.g*255)+","+Mathf.Round(col.b*255)+")");
        }

        public static void uncolorize(Cell c)
        {
#if !UNITY_EDITOR
		Uncolorize(c.t+"/"+c.ID);
#endif
            //Application.ExternalEval ("uncolorize(" + c.t + "," + c.ID +")");
        }

        public static void show(Cell c, bool v)
        {
            if (!v)
            {
                //MorphoDebug.Log("hide(" + c.t + "," + c.ID + ")");
                Hide(c.t + "/" + c.ID);
            }
            else
            {
                //MorphoDebug.Log("show(" + c.t + "," + c.ID + ")");
                Show(c.t + "/" + c.ID);
            }
        }

        public static void addColorCell(Cell c, Color col)
        {
            if (!colorToPlot.Contains(col))
                colorToPlot.Add(col);
            basicstoUpdate.Add(c);
        }

        public static void applyMultipleCellColorSelection(Dictionary<int, List<Cell>> cells, Dictionary<int, Color> colors)
        {
            StringBuilder finallist = new StringBuilder();
            StringBuilder listcells = new StringBuilder();
            foreach (KeyValuePair<int, List<Cell>> basics in cells)
            {
                int i = basics.Key;
                if (basics.Value != null && colors[i] != null)
                {
                    foreach (Cell c in basics.Value)
                    {
                        listcells.Append(":" + c.t + "," + c.ID);
                    }
                    listcells.Append("-" + colors[i].r + "," + colors[i].g + "," + colors[i].b);
                    //MorphoDebug.Log(listcells.ToString());
                    finallist.Append(listcells.ToString() + ";");
                    listcells.Clear();
                }
            }
#if UNITY_WEBGL
		//MorphoDebug.Log(finallist.ToString());
		multipleSelection(finallist.ToString());
#endif
        }

        public static void LoadInfoInLineage(int info_id)
        {
#if UNITY_WEBGL
		updateInfoDeployed(info_id);
#endif
        }

        public static void applyCellColorsStart()
        {
            StringBuilder listcells = new StringBuilder();
            foreach (Color col in colorToPlot)
            {
                listcells.Append(Mathf.Round(col.r * 255).ToString() + "/" + Mathf.Round(col.g * 255).ToString() + "/" + Mathf.Round(col.b * 255).ToString() + "/");
                //	UIManager.instance.UpdateLineageLoading(true, (int)(count / colorToPlot.Count * 100));
                List<Cell> cellToUpdateT = new List<Cell>();

                foreach (Cell c in basicstoUpdate.FindAll(bo => bo.UploadColor == col))
                {
                    if (c.UploadColor == col)
                    {
                        listcells.Append(c.t + ";" + c.ID + ":");
                    }
                }
                //MorphoDebug.Log(listcells.ToString());

                //Application.ExternalEval ("applySelection(" + Mathf.Round(col.r*255)+","+Mathf.Round(col.g*255)+","+Mathf.Round(col.b*255) + ",\"" + listcells.ToString()+"\")");
                listcells.Append("!");
            }
            listcells.Remove(listcells.Length - 2, 1);
            //cmapflag = false;
            InterfaceManager.instance.StartCoroutine(InterfaceManager.instance.lineage.SendCellColors_Map(listcells.ToString()));
            //yield return new WaitUntil(() => cmapflag == true);
            //	UIManager.instance.HideLineageLoading();
            colorToPlot.Clear();
            basicstoUpdate.Clear();
        }

        public IEnumerator SendCellColors_Map(string c)
        {
#if UNITY_WEBGL
		if (c != null && c != "")
		{
			if (c.Length < 40000)
			{
				ApplySelectionMap(">COMPLETESTART" + c);
			}
			else
			{
				ApplySelectionMap(">START");
				string info_json = "";
				string[] lineage_lines = c.Split(':');
				foreach (string line in lineage_lines)
				{
					var new_line = line + ":";
					info_json += new_line.Replace("\t", "").Replace("\r", "");
					if (info_json.Length > 40000)
					{
						string temp = info_json;
						yield return new WaitForSeconds(0.01f);
						ApplySelectionMap(temp);
						info_json = "";
					}
				}
				string temp2 = info_json;
				yield return new WaitForSeconds(0.01f);
				ApplySelectionMap(temp2);
				yield return new WaitForSeconds(0.01f);
				ApplySelectionMap("END<");
				cmapflag = true;
			}
		}
		//SetsManager.instance.StartCoroutine(SendInfoChucnked(info_json));
#endif
            yield return null;
        }

        public IEnumerator SendCellColors(string c)
        {
#if UNITY_WEBGL
		if (c != null && c != "") {
			if (c.Length < 40000)
			{
				ApplySelection(">COMPLETESTART" + c);
			}
			else
			{
				ApplySelection(">START");
				string info_json = "";
				string[] lineage_lines = c.Split(':');
				foreach (string line in lineage_lines)
				{
					var new_line = line + ":";
					info_json += new_line.Replace("\t", "").Replace("\r", "");
					if (info_json.Length > 40000)
					{
						string temp = info_json;
						yield return new WaitForSeconds(0.01f);
						ApplySelectionMap(temp);
						info_json = "";
					}
				}
				string temp2 = info_json;
				yield return new WaitForSeconds(0.01f);
				ApplySelection(temp2);
				yield return new WaitForSeconds(0.01f);
				ApplySelection("END<");
				cmapflag = true;
			}
		}
		//SetsManager.instance.StartCoroutine(SendInfoChucnked(info_json));
#endif
            yield return null;
        }

        public IEnumerator applyCellColors()
        {
            //	UIManager.instance.UpdateLineageLoading(true, 0);
            StringBuilder listcells = new StringBuilder();
            foreach (Color col in colorToPlot)
            {
                listcells.Append(Mathf.Round(col.r * 255).ToString() + "/" + Mathf.Round(col.g * 255).ToString() + "/" + Mathf.Round(col.b * 255).ToString() + "/");
                //	UIManager.instance.UpdateLineageLoading(true, (int)(count / colorToPlot.Count * 100));
                List<Cell> cellToUpdateT = new List<Cell>();

                foreach (Cell c in basicstoUpdate.FindAll(bo => bo.UploadColor == col))
                {
                    if (c.UploadColor == col)
                    {
                        listcells.Append(c.t + ";" + c.ID + ":");
                    }
                }
                //MorphoDebug.Log(listcells.ToString());

                //Application.ExternalEval ("applySelection(" + Mathf.Round(col.r*255)+","+Mathf.Round(col.g*255)+","+Mathf.Round(col.b*255) + ",\"" + listcells.ToString()+"\")");
                listcells.Append("!");
            }
            listcells.Remove(listcells.Length - 2, 1);
            cmapflag = false;
            StartCoroutine(SendCellColors_Map(listcells.ToString()));
            yield return new WaitUntil(() => cmapflag == true);
            //	UIManager.instance.HideLineageLoading();
            colorToPlot.Clear();
            basicstoUpdate.Clear();
        }

        //SELECTION

        public static void AddCellSelected(Cell c)
        {
            AddToColor(c, NormalColor);
        }

        public static void AddAllSelected()
        {
            for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
            {
                Cell c = MorphoTools.GetPickedManager().clickedCells[i];
                AddToColor(c, NormalColor);
            }
            UpdateWebsiteLineage();
        }

        public static List<string> GetChunks(string value, int chunkSize)
        {
            List<string> triplets = new List<string>();
            while (value.Length > chunkSize)
            {
                triplets.Add(value.Substring(0, chunkSize));
                value = value.Substring(chunkSize);
            }
            if (value != "")
                triplets.Add(value);
            return triplets;
        }

        public IEnumerator SendInfo(Correspondence c)
        {
#if UNITY_WEBGL
		SendInfoToLineage(">START");
		string info_json = "{";
		if (c.name != null)
			info_json += "\"infos\":" + "\""+c.name + "\"";

		if (c.id_infos != null)
			info_json += ",\"id\":" + c.id_infos;
		if (c.lines != null)
		{
			info_json += ",\"lines\":\"";
			foreach (string line in c.lines)
			{
				var new_line = line + "\\n";
				info_json += new_line.Replace("\t","").Replace("\r","");
				if (info_json.Length > 40000)
				{
					string temp = info_json;
					yield return new WaitForSeconds(0.05f);
					SendInfoToLineage(temp);
					info_json = "";
				}
			}
			info_json += "\"";
		}

		info_json += "}";
		string temp2 = info_json;
		yield return new WaitForSeconds(0.05f);
		SendInfoToLineage(temp2);
		yield return new WaitForSeconds(0.05f);
		SendInfoToLineage("END<");
		flag = true;
		//SetsManager.instance.StartCoroutine(SendInfoChucnked(info_json));
#endif
            yield return null;
        }

        public static IEnumerator SendInfoChucnked(string info)
        {
#if UNITY_WEBGL
		List<string> chuncks_to_send = GetChunks(info, 500);
		SendInfoToLineage(">START");
		for (int i = 0; i < chuncks_to_send.Count; i++)
		{
			yield return new WaitForSeconds(0.05f);
			SendInfoToLineage(chuncks_to_send[i]);
			chuncks_to_send[i] = "";
		}
		yield return new WaitForSeconds(0.05f);
		SendInfoToLineage("END<");
#endif
            yield return null;
        }

        public static void RemoveCellSelected(Cell c)
        {
            if (c.UploadedColor)
                colorize(c, c.UploadColor);
            else if (c.selection == null || c.selection.Count == 0)
                uncolorize(c);
            else
                colorize(c, SelectionManager.getSelectedMaterial(c.selection[0]).color);
        }

        public static void hideSelectedCells(bool alltimes, List<Cell> new_cells)
        {
            //	UIManager.instance.UpdateLineageLoading(true, 0);
            int count = 0;
            if (alltimes)
            { //ALL TIMES
                StringBuilder listcells = new StringBuilder();
                for (int i = 0; i < new_cells.Count; i++)
                {
                    //		UIManager.instance.UpdateLineageLoading(true, (int)(count / SelectionCell.clickedCells.Count * 100));
                    Cell c = new_cells[i];
                    listcells.Append(c.t + ";" + c.ID + ":");
                    count++;
                }
                HideCellsAllTime("1/" + listcells.ToString());
                //Application.ExternalEval ("hideCellsAllTime(0,\"" + listcells.ToString()+"\")");
            }
            else
            { //ONLY THIS TIME
                StringBuilder listcells = new StringBuilder();
                for (int i = 0; i < new_cells.Count; i++)
                {
                    //	UIManager.instance.UpdateLineageLoading(true, (int)(count / SelectionCell.clickedCells.Count * 100));
                    Cell c = new_cells[i];
                    listcells.Append(c.t + ";" + c.ID + ":");
                    count++;
                }
                //MorphoDebug.Log("List cells : ");
                //MorphoDebug.Log(listcells.ToString());
                HideCells("1/" + listcells.ToString());
                //Application.ExternalEval ("hideCells(0,\"" + listcells.ToString()+"\")");
            }

            //UIManager.instance.HideLineageLoading();
        }

        public static void showSelectedCells(bool alltimes)
        {
            StringBuilder listcells = new StringBuilder();
            int count = 0;
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
            {
                if (!cell.show)
                    listcells.Append($"{cell.t};{cell.ID}:");
                count++;
            }

            if (alltimes)
                HideCellsAllTime("0/" + listcells.ToString());
            else
            {
                HideCells("0/" + listcells.ToString());
            }
        }

        public static void applySelection(Color col)
        {
            StringBuilder listcells = new StringBuilder();
            int count = 0;

            for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
            {
                //For all cells selected
                Cell c = MorphoTools.GetPickedManager().clickedCells[i];
                listcells.Append(c.t + ";" + c.ID + ":");
                count++;
            }
            InterfaceManager.instance.StartCoroutine(InterfaceManager.instance.lineage.SendCellColors(Mathf.Round(col.r * 255).ToString() + "/" + Mathf.Round(col.g * 255).ToString() + "/" + Mathf.Round(col.b * 255).ToString() + "/" + listcells.ToString() + "!"));
        }

        public static void applySelectionOnCell(Cell cell, Color col)
        {
            StringBuilder listcells = new StringBuilder();
            listcells.Append(cell.t + ";" + cell.ID + ":");
            InterfaceManager.instance.StartCoroutine(InterfaceManager.instance.lineage.SendCellColors(Mathf.Round(col.r * 255).ToString() + "/" + Mathf.Round(col.g * 255).ToString() + "/" + Mathf.Round(col.b * 255).ToString() + "/" + listcells.ToString() + "!"));
        }

        public static void applySelectionInfo(Correspondence cor)
        {
            string add_data = "";
            for (int i = 0; i < 255; i++)
            {
                Color col = SelectionManager.getSelectedMaterial(i).color;
                add_data += i + ":" + Mathf.Round(col.r * 255) + "," + Mathf.Round(col.g * 255) + "," + Mathf.Round(col.b * 255) + ";";
            }
            add_data = add_data.Remove(add_data.Length - 1);
            ReceiveColorInfos("selection!" + cor.id_infos + "!" + add_data);
        }

        public static void applyColormapInfo(Correspondence cor, int typecmap, int minC, int maxC, float minV, float maxV, bool apply_all, List<Cell> cells = null)
        {
            Dictionary<int, Color> data = MenuColorBar.instance.getTextureArray(typecmap, minC, maxC, minV, maxV);
            string add_data = "";
            foreach (KeyValuePair<int, Color> pair in data)
            {
                add_data += pair.Key + ":" + Mathf.Round(pair.Value.r * 255) + "," + Mathf.Round(pair.Value.g * 255) + "," + Mathf.Round(pair.Value.b * 255) + ";";
            }

            string cell_list = "all";
            if (!apply_all && cells != null && cells.Count > 0)
            {
                cell_list = "";
                foreach (Cell cell in cells)
                {
                    cell_list += cell.t + ":" + cell.ID + ";";
                }
                if (cell_list.Length > 1)
                {
                    cell_list = cell_list.Remove(cell_list.Length - 1);
                }
            }
            add_data = add_data.Remove(add_data.Length - 1);
            ReceiveColorInfos("colormap!" + cor.id_infos + "!" + add_data + "/" + minC + "/" + maxC + "/" + minV + "/" + maxV + "/" + cell_list);
        }

        public static void applySelectionToCells(Color col, List<Cell> lc)
        {
            int count = 0;
            StringBuilder listcells = new StringBuilder();
            foreach (Cell c in lc)
            {
                listcells.Append(c.t + ";" + c.ID + ":");
                count++;
            }
            InterfaceManager.instance.StartCoroutine(InterfaceManager.instance.lineage.SendCellColors(Mathf.Round(col.r * 255).ToString() + "/" + Mathf.Round(col.g * 255).ToString() + "/" + Mathf.Round(col.b * 255).ToString() + "/" + listcells.ToString() + "!"));
        }

        public static void resetSelectionMultipleCells(List<Cell> cells)
        {
            Dictionary<Color, List<Cell>> cells_to_update = new Dictionary<Color, List<Cell>>();
            foreach (Cell cell in cells)
            {
                if (cell.selection != null && cell.selection.Count > 0)
                {
                    if (!cells_to_update.ContainsKey(SelectionManager.getSelectedMaterial(cell.selection[cell.selection.Count - 1]).color))
                    {
                        cells_to_update.Add(SelectionManager.getSelectedMaterial(cell.selection[cell.selection.Count - 1]).color, new List<Cell>());
                    }
                    cells_to_update[SelectionManager.getSelectedMaterial(cell.selection[cell.selection.Count - 1]).color].Add(cell);
                }
                else
                {
                    if (!cells_to_update.ContainsKey(Color.white))
                    {
                        cells_to_update.Add(Color.white, new List<Cell>());
                    }

                    cells_to_update[Color.white].Add(cell);
                }
            }

            foreach (KeyValuePair<Color, List<Cell>> pair in cells_to_update)
            {
                ColorizeListCells(pair.Key, pair.Value);
            }
        }

        public static void ColorizeListCells(Color col, List<Cell> lc)
        {
            StringBuilder listcells = new StringBuilder();
            int count = 0;
            foreach (Cell c in lc)
            {
                listcells.Append(c.t + ";" + c.ID + ":");
                count++;
            }
#if UNITY_WEBGL
		ColorizeMultiple(Mathf.Round(col.r * 255).ToString() + "/" + Mathf.Round(col.g * 255).ToString() + "/" + Mathf.Round(col.b * 255).ToString() + "/" + listcells.ToString());
#endif
        }

        public static void resetSelectionOnSelectedCells(bool timeisOn, List<Cell> p_cells)
        {
            StringBuilder listcells = new StringBuilder();
            if (timeisOn)
            { //ALL TIMES
                for (int i = 0; i < p_cells.Count; i++)
                {
                    List<Cell> cells = p_cells[i].getAllCellLife();
                    foreach (Cell c in cells)
                        listcells.Append(c.t + ";" + c.ID + ":");
                }
            }
            else
            { //JUST THIS TIME STEP
                for (int i = 0; i < p_cells.Count; i++)
                {
                    Cell c = p_cells[i];
                    listcells.Append(c.t + ";" + c.ID + ":");
                }
            }
            ResetSelectionOnSelectedCells(listcells.ToString());
            //Application.ExternalEval ("resetSelectionOnSelectedCells(\""+listcells.ToString()+"\")");
        }

        public static void ClearSelectedCells(bool timeisOn)
        {
            List<Cell> celltoreset = new List<Cell>();
            int count = 0;
            if (timeisOn)
            { //ALL TIMES
                for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
                {
                    List<Cell> cells = MorphoTools.GetPickedManager().clickedCells[i].getAllCellLife();
                    foreach (Cell c in cells)
                        celltoreset.Add(c);
                    count++;
                }
            }
            else
            { //JUST THIS TIME STEP
                for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
                {
                    Cell c = MorphoTools.GetPickedManager().clickedCells[i];
                    celltoreset.Add(c);
                    count++;
                }
            }
            if (celltoreset.Count > 0)
                UncolorizeMultipleCells(celltoreset);
        }

        public static void ManagePickedCells(List<Cell> cells, bool pick)
        {
            int va = 0;
            if (pick)
                va = 1;
            StringBuilder listcells = new StringBuilder();
            foreach (Cell c in cells)
            {
                listcells.Append(c.t + ";" + c.ID + ":");
            }
            pickCells(listcells.ToString() + "/" + va);
        }

        public static void UpdateSelectionCells(int old_select, int new_select)
        {
            MorphoDebug.Log("update color : " + old_select.ToString() + " => " + new_select.ToString());
            Color c = SelectionManager.getSelectedMaterial(new_select).color;
            updateSelectionColor(old_select.ToString() + "/" + new_select.ToString() + "/" + c.r + "/" + c.g + "/" + c.b);
        }

        public static void ClearSelectedCellsList(List<Cell> p_cells, bool timeisOn)
        {
            List<Cell> celltoreset = new List<Cell>();
            StringBuilder listcells = new StringBuilder();
            int count = 0;
            if (timeisOn)
            { //ALL TIMES
                for (int i = 0; i < p_cells.Count; i++)
                {
                    List<Cell> cells = p_cells[i].getAllCellLife();
                    foreach (Cell c in cells)
                        celltoreset.Add(c);
                    count++;
                }
            }
            else
            { //JUST THIS TIME STEP
                for (int i = 0; i < p_cells.Count; i++)
                {
                    Cell c = p_cells[i];
                    celltoreset.Add(c);
                    count++;
                }
            }
            if (celltoreset.Count > 0)
                UncolorizeMultipleCells(celltoreset);
        }

        public static void resetSelectionOnSelectedMultiCells(bool timeisOn)
        {
            int count = 0;
            StringBuilder listcells = new StringBuilder();
            if (timeisOn)
            { //ALL TIMES
                for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
                {
                    List<Cell> cells = MorphoTools.GetPickedManager().clickedCells[i].getAllCellLife();
                    foreach (Cell c in cells)
                        if (c.selection != null && c.selection.Count > 1)
                            listcells.Append(c.t + ";" + c.ID + ":");
                    count++;
                }
            }
            else
            { //JUST THIS TIME STEP
                for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
                {
                    Cell c = MorphoTools.GetPickedManager().clickedCells[i];
                    if (c.selection != null && c.selection.Count > 1)
                        listcells.Append(c.t + ";" + c.ID + ":");
                    count++;
                }
            }
            ResetSelectionOnSelectedCells(listcells.ToString());
        }

        public static void clearSelectedCells(List<Cell> selected_cells)
        {
            StringBuilder listcells = new StringBuilder();
            StringBuilder allcells = new StringBuilder();
            Dictionary<int, List<Cell>> cells_for_selection = new Dictionary<int, List<Cell>>();
            int count = 0;
            foreach (Cell c in selected_cells)
            {
                if (c.selection != null && c.selection.Count > 0)
                {
                    if (!cells_for_selection.ContainsKey(c.selection[0]))
                    {
                        cells_for_selection.Add(c.selection[0], new List<Cell>());
                    }
                    cells_for_selection[c.selection[0]].Add(c);
                }
                else
                    listcells.Append(c.t + ";" + c.ID + ":");
                count++;
            }

            foreach (KeyValuePair<int, List<Cell>> select in cells_for_selection)
            {
                Color col = SelectionManager.getSelectedMaterial(select.Key).color;
                allcells.Append(Mathf.Round(col.r * 255).ToString() + "/" + Mathf.Round(col.g * 255).ToString() + "/" + Mathf.Round(col.b * 255).ToString() + "/");
                foreach (Cell b in select.Value)
                {
                    allcells.Append(b.t + ";" + b.ID + ":");
                }
                allcells.Append("!");
            }
            InterfaceManager.instance.StartCoroutine(InterfaceManager.instance.lineage.SendCellColors_Map(allcells.ToString()));
            ResetSelectionOnSelectedCells(listcells.ToString());
        }

        public static void addAllSelectedCells(List<Cell> selected)
        {
            StringBuilder listcells = new StringBuilder();
            int count = 0;
            foreach (Cell c in selected)
            {
                listcells.Append(c.t + ";" + c.ID + ":");
                count++;
            }
            InterfaceManager.instance.StartCoroutine(InterfaceManager.instance.lineage.SendCellColors("255/255/255/" + listcells.ToString() + "!"));
        }

        public static void UncolorizeMultipleCells(List<Cell> rawlistcells)
        {
            StringBuilder listcells = new StringBuilder();
            int count = 0;
            foreach (Cell c in rawlistcells)
            {
                listcells.Append(c.t + ";" + c.ID + ":");
                count++;
            }
#if UNITY_WEBGL
		UncolorizeMultiple(listcells.ToString());
#endif
        }

        public static void clearMulti(bool timeisOn)
        {
            List<Cell> celltoreset = new List<Cell>();
            if (timeisOn)
            {
                StringBuilder listcells = new StringBuilder();
                int count = 0;
                foreach (int i in MorphoTools.GetDataset().CellsByTimePoint.Keys)
                {
                    foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[i])
                    {
                        if (cell.selection != null && cell.selection.Count > 1)
                            celltoreset.Add(cell);
                        count++;
                    }
                }

                if (celltoreset.Count > 0)
                    UncolorizeMultipleCells(celltoreset);
            }
            else
            {
                int count = 0;
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
                {
                    if (cell.selection != null && cell.selection.Count > 1)
                        celltoreset.Add(cell);
                    count++;
                }

                if (celltoreset.Count > 0)
                    UncolorizeMultipleCells(celltoreset);
            }
        }

        public static void hideSelection(int select, bool type)
        {
            List<Cell> cells = new List<Cell>();

            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
            {
                if (cell.selection != null && cell.selection.Count > 0 && cell.selection.Contains(select))
                {
                    cells.Add(cell);
                }
            }

            if (type)
            {
                hideCellList(cells, "select", id_select: select);
            }
            else
            {
                showCellList(cells, "select", id_select: select);
            }
        }

        public static void hideMultiple(bool type)
        {
            string method = InterfaceManager.instance.GetTimeMethod();
            if (type)
            {
                HideCells($"{method}/;/multiple/-1/{MorphoTools.GetDataset().CurrentTime}");
            }
            else
            {
                SendShowCells($"{method}/;/multiple/-1/{MorphoTools.GetDataset().CurrentTime}");
            }
        }

        public static void hideFree(bool type)
        {
            string method = InterfaceManager.instance.GetTimeMethod();
            if (type)
            {
                HideCells($"{method}/;/free/-1/{MorphoTools.GetDataset().CurrentTime}");
            }
            else
            {
                SendShowCells($"{method}/;/free/-1/{MorphoTools.GetDataset().CurrentTime}");
            }
        }

        public static void hideCellList(List<Cell> cells, string type, int id_select = -1)
        {
            if (cells != null)
            {
                StringBuilder listcells = new StringBuilder();
                foreach (Cell c in cells)
                {
                    listcells.Append(c.t + ";" + c.ID + ":");
                }
                string method = InterfaceManager.instance.GetTimeMethod();
                HideCells($"{method}/{listcells}/{type}/{id_select}/{MorphoTools.GetDataset().CurrentTime}");
            }
        }

        public static void hideCellListAllTimes(List<Cell> cells, string type, int id_select = -1)
        {
            if (cells != null)
            {
                StringBuilder listcells = new StringBuilder();
                foreach (Cell c in cells)
                {
                    listcells.Append(c.t + ";" + c.ID + ":");
                }
                HideCells($"all/{listcells}/{type}/{id_select}/{MorphoTools.GetDataset().CurrentTime}");
            }
        }

        public static void hideCellListCurrentTimes(List<Cell> cells, string type, int id_select = -1)
        {
            if (cells != null)
            {
                StringBuilder listcells = new StringBuilder();
                foreach (Cell c in cells)
                {
                    listcells.Append(c.t + ";" + c.ID + ":");
                }
                HideCells($"normal/{listcells}/{type}/{id_select}/{MorphoTools.GetDataset().CurrentTime}");
            }
        }

        public static void showCellList(List<Cell> cells, string type, int id_select = -1)
        {
            if (cells != null)
            {
                StringBuilder listcells = new StringBuilder();
                foreach (Cell c in cells)
                {
                    listcells.Append(c.t + ";" + c.ID + ":");
                }
                string method = InterfaceManager.instance.GetTimeMethod();
                SendShowCells($"{method}/{listcells}/{type}/{id_select}/{MorphoTools.GetDataset().CurrentTime}");
            }
        }

        public static void applyhideThis(bool v, bool timeisOn, int selectValue)
        {
            StringBuilder listcells = new StringBuilder();
            int count = 0;
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
            {
                if (cell.selection != null && cell.selection.Contains(selectValue))
                    listcells.Append($"{cell.t};{cell.ID}:");
                count++;
            }

            int va = 0;
            if (v)
                va = 1;

            if (timeisOn)
                HideCellsAllTime($"{va}/{listcells}");
            else
                HideCells($"{va}/{listcells}/{MorphoTools.GetDataset().CurrentTime}");
        }

        public static void applyhideMulti(bool v, bool timeisOn)
        {
            StringBuilder listcells = new StringBuilder();
            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
            {
                if (cell.selection != null && cell.selection.Count > 1)
                    listcells.Append($"{cell.t};{cell.ID}:");
            }

            int va = 0;
            if (v)
                va = 1;

            if (timeisOn)
                HideCellsAllTime($"{va}/{listcells}");
            else
                HideCells($"{va}/{listcells}/{MorphoTools.GetDataset().CurrentTime}");
        }

        public static void applyhideAll(bool v)
        {
            string method = InterfaceManager.instance.GetTimeMethod();
            if (v)
            {
                HideCells($"{method}/;/all/-1/{MorphoTools.GetDataset().CurrentTime}");
            }
            else
            {
                SendShowCells($"{method}/;/all/-1/{MorphoTools.GetDataset().CurrentTime}");
            }
        }

        public static void propagateThis(bool v, int selectValue, List<Cell> cells, string type)
        {
            if (cells != null)
            {
                StringBuilder listcells = new StringBuilder();
                foreach (Cell c in cells)
                {
                    listcells.Append(c.t + ";" + c.ID + ":");
                }

                int va = 0;
                if (v)
                    va = 1;

                string method = InterfaceManager.instance.GetTimeMethod();
                Color col = SelectionManager.getSelectedMaterial(selectValue).color;
                PropagateThis($"{method}/{va}/{Mathf.Round(col.r * 255)}/{Mathf.Round(col.g * 255)}/{Mathf.Round(col.b * 255)}/{listcells}/{type}/{selectValue}/{MorphoTools.GetDataset().CurrentTime}");
            }
        }

        public static void sendBackgroundColor(Color color)
        {
            SendBackgroundColor(color.r + "," + color.g + "," + color.b);
        }

        public static void clearThis(int selectValue, List<Cell> cells, string type)
        {
            if (cells != null)
            {
                StringBuilder listcells = new StringBuilder();
                foreach (Cell c in cells)
                {
                    listcells.Append(c.t + ";" + c.ID + ":");
                }
                string method = InterfaceManager.instance.GetTimeMethod();
                clearCells($"{method}/{listcells}/{type}/{selectValue}/{MorphoTools.GetDataset().CurrentTime}");
            }
        }

        public static void CancelPropagationThis(int selectValue, List<Cell> cells, string type, bool v)
        {
            if (cells != null)
            {
                StringBuilder listcells = new StringBuilder();
                foreach (Cell c in cells)
                {
                    listcells.Append(c.t + ";" + c.ID + ":");
                }
                string method = InterfaceManager.instance.GetTimeMethod();

                int va = 0;
                if (v)
                    va = 1;
                cancelPropagationListCells($"{method}/{listcells}/{type}/{selectValue}/{MorphoTools.GetDataset().CurrentTime}/{va}");
            }
        }

        public static void CancelPropagationAll(bool v)
        {
            string method = InterfaceManager.instance.GetTimeMethod();

            int va = 0;
            if (v)
                va = 1;

            cancelPropagationListCells($"{method}/;/all/-1/{MorphoTools.GetDataset().CurrentTime}/{va}");
        }

        public static void CancelPropagationMultiNew(bool v)
        {
            string method = InterfaceManager.instance.GetTimeMethod();

            int va = 0;
            if (v)
                va = 1;

            cancelPropagationListCells($"{method}/;/multiple/-1/{MorphoTools.GetDataset().CurrentTime}/{va}");
        }

        public static void cancelPropagationPicked(bool v)
        {
            StringBuilder listcells = new StringBuilder();

            int va = 0;
            if (v)
                va = 1;

            foreach (Cell c in MorphoTools.GetPickedManager().clickedCells)
            {
                listcells.Append(c.t + ";" + c.ID + ":");
            }

            string method = InterfaceManager.instance.GetTimeMethod();
            cancelPropagationListCells($"{method}/{listcells}/picked/-1/{MorphoTools.GetDataset().CurrentTime}/{va}");
        }

        public static void clearThisSelected(int selectValue)
        {
            List<Cell> listcells = new List<Cell>();
            foreach (var itemc in MorphoTools.GetPickedManager().clickedCells)
            {
                Cell c = itemc;
                if (c.selection != null && c.selection.Contains(selectValue))
                    listcells.Add(c);
            }
            clearThis(selectValue, listcells, "select");
        }

        public static void clearAll()
        {
            string method = InterfaceManager.instance.GetTimeMethod();
            clearCells($"{method}/;/all/-1/{MorphoTools.GetDataset().CurrentTime}");
        }

        public static void clearMultiNew()
        {
            string method = InterfaceManager.instance.GetTimeMethod();
            clearCells($"{method}/;/multiple/-1/{MorphoTools.GetDataset().CurrentTime}");
        }

        public static void clearPicked()
        {
            StringBuilder listcells = new StringBuilder();
            foreach (Cell c in MorphoTools.GetPickedManager().clickedCells)
            {
                listcells.Append(c.t + ";" + c.ID + ":");
            }

            string method = InterfaceManager.instance.GetTimeMethod();
            clearCells($"{method}/{listcells}/picked/-1/{MorphoTools.GetDataset().CurrentTime}");
        }

        public static void propagateThisSelected(bool v, int selectValue)
        {
            List<Cell> listcells = new List<Cell>();
            foreach (var itemc in MorphoTools.GetPickedManager().clickedCells)
            {
                Cell c = itemc;
                if (c.selection != null && c.selection.Contains(selectValue))
                    listcells.Add(c);
            }
            propagateThis(v, selectValue, listcells, "select");
        }

        public static void propagateAll(bool v)
        {
            int va = 0;
            if (v)
                va = 1;

            string method = InterfaceManager.instance.GetTimeMethod();
            PropagateThis($"{method}/{va}/{Mathf.Round(0 * 255)}/{Mathf.Round(0 * 255)}/{Mathf.Round(0 * 255)}/;/all/-1/{MorphoTools.GetDataset().CurrentTime}");
        }

        public static void propagateMulti(bool v)
        {
            int va = 0;
            if (v)
                va = 1;

            string method = InterfaceManager.instance.GetTimeMethod();
            PropagateThis($"{method}/{va}/{Mathf.Round(0 * 255)}/{Mathf.Round(0 * 255)}/{Mathf.Round(0 * 255)}/;/multiple/-1/{MorphoTools.GetDataset().CurrentTime}");
        }

        public static void clearAllSelected()
        {
            List<int> SelectedValues = getSelectionValues();
            foreach (int selectValue in SelectedValues)
                clearThisSelected(selectValue);
        }

        public static void propagatePicked(bool v)
        {
            List<int> SelectedValues = getSelectionValues();
            foreach (int selectValue in SelectedValues)
            {
                List<Cell> cells = new List<Cell>();
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
                {
                    if (cell.selection != null && cell.selection.Contains(selectValue) && MorphoTools.GetPickedManager().clickedCells.Contains(cell))
                    {
                        if (!cells.Contains(cell))
                        {
                            cells.Add(cell);
                        }
                    }
                }
                if (cells.Count > 0)
                {
                    propagateThis(v, selectValue, cells, "select");
                }
            }
        }

        public static void propagateAllSelected(bool v)
        {
            List<int> SelectedValues = getSelectionValues();
            foreach (int selectValue in SelectedValues)
                propagateThisSelected(v, selectValue);
        }

        public static List<int> getSelectionValues()
        {
            List<int> SelectedValues = new List<int>();

            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
            {
                if (cell.selection != null && cell.selection.Count > 0)
                {
                    int selectedValue = cell.selection[0];
                    if (!SelectedValues.Contains(selectedValue))
                        SelectedValues.Add(selectedValue);
                }
            }
            return SelectedValues;
        }

        public static List<int> getAllTimesSelectionValues()
        {
            List<int> SelectedValues = new List<int>();
            foreach (var cellsAtTimePoint in MorphoTools.GetDataset().CellsByTimePoint.Values)
            {
                foreach (Cell cell in cellsAtTimePoint)
                {
                    if (cell.selection != null && cell.selection.Count > 0)
                    {
                        int selectedValue = cell.selection[0];
                        if (!SelectedValues.Contains(selectedValue))
                            SelectedValues.Add(selectedValue);
                    }
                }
            }
            return SelectedValues;
        }

        public static List<Cell> _Listcells;

        public static void getCellInGroup(Group gp)
        {
            if (gp.SubGroups != null)
                foreach (Group sg in gp.SubGroups)
                    getCellInGroup(sg);

            if (gp.Objects != null)
                foreach (GroupObjects sog in gp.Objects)
                    if (!_Listcells.Contains(sog.c))
                        _Listcells.Add(sog.c);
        }

        public static void colorizeGroup(Group gp)
        {
            int selectionValue = 1;
            if (gp.SubGroups != null)
                foreach (Group sg in gp.SubGroups)
                {
                    _Listcells = new List<Cell>();
                    getCellInGroup(sg);
                    StringBuilder listcells = new StringBuilder();
                    foreach (Cell c in _Listcells)
                        listcells.Append(c.t + ";" + c.ID + ":");
                    Color col = SelectionManager.getSelectedMaterial(selectionValue).color;
                    InterfaceManager.instance.StartCoroutine(InterfaceManager.instance.lineage.SendCellColors(Mathf.Round(col.r * 255).ToString() + "/" + Mathf.Round(col.g * 255).ToString() + "/" + Mathf.Round(col.b * 255).ToString() + "/" + listcells.ToString() + "!"));
                    selectionValue++;
                }

            if (gp.Objects != null)
                foreach (GroupObjects sog in gp.Objects)
                {
                    colorize(sog.c, SelectionManager.getSelectedMaterial(selectionValue).color);
                    selectionValue++;
                }
        }
    }
}