﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;


public class OnMouseOverLineageCell : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public Image i;
	public bool hover = false;
	void Start()
	{
		i = GetComponent<Image>();
	}
    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
		if (i != null)
		{
			i.color = new Color(i.color.r, i.color.g, i.color.b, 1);
		}
	}


	void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
    {
		if (i != null)
		{
			i.color = new Color(i.color.r, i.color.g, i.color.b, 0);
		}
	}

}