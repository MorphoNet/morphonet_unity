﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AssemblyCSharp;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.UI;


namespace MorphoNet
{
	public struct CellData
	{
		public GameObject cell;
		public float w;
		public float x;
		public float y;
		public float z;

		public CellData(GameObject c, float _x, float _y, float _z, float _w)
		{
			cell = c;
			x = _x;
			y = _y;
			z = _z;
			w = _w;
		}
	}

	public class LineageViewer : MonoBehaviour
	{

		public bool hasFocus = false;
		public bool isActive = false;

		private DataSet AttachedDataset;
		public Button LineageButton;
		public ServerSend ssend;
		public Image LineageImage;

		public bool lineage_shown = false;
		public bool isAvaible = true;

		public static int PORT_LINEAGE = 9803;
		public Color backup_button_color;

		private Queue<IEnumerator> _CurrentCoroutines = new Queue<IEnumerator>();


		void Awake()
		{
			if (LineageButton == null)
				LineageButton = InterfaceManager.instance.BoutonLineageViewer.GetComponent<Button>();
			backup_button_color = LineageButton.colors.normalColor;

			
		}

        void Start()
        {
			StartCoroutine(CoroutineCoordinator());
		}

        public void AddPythonCommandToQueue(string action, string data, int port)
		{
			//MorphoDebug.LogWarning("adding command ! " + action);
			_CurrentCoroutines.Enqueue(sendPythonCommand(action, data, port));				

		}

		public IEnumerator CoroutineCoordinator()
        {
            while (true)
            {
				while (_CurrentCoroutines.Count > 0)
                {
					yield return StartCoroutine(_CurrentCoroutines.Dequeue());
				}
				yield return new WaitForEndOfFrame();
			}
			
        }

		public IEnumerator sendPythonCommand(string action, string data, int port)
		{
			if (!isAvaible)
			{
				MorphoDebug.Log("Wait a command is already running");
			}
			yield return new WaitUntil(() => isAvaible);
			isAvaible = false;

			WWWForm form = new WWWForm();
			form.AddField("action", action);
			form.AddField("time", MorphoTools.GetDataset().CurrentTime);
			form.AddField("data", data);

			UnityWebRequest www = UnityWebRequests.Post("localhost:" + port, "/ send.html", form);
			yield return www.SendWebRequest();
			if (www.isHttpError || www.isNetworkError)
				MorphoDebug.Log("Error sending command ! : " + www.error);
			else
			{
				//MorphoDebug.Log("Successfully sent command "+action + " // ");
				//String commande = Encoding.UTF8.GetString(www.downloadHandler.data);
				//if (btn.name != "showraw") UIManager.instance.MenuObjects.transform.Find("Selection").GetComponent<SelectionColor>().clearAllColors();

			}
			isAvaible = true;
			www.Dispose();
		}


		public Coroutine corout = null;

		public void UpdateLineageColorButton(Color c)
		{
			var colors = LineageButton.colors;
			colors.normalColor = c;
			LineageButton.colors = colors;
		}
		public void StartStopLineage()
		{

			lineage_shown = !lineage_shown;
			if (lineage_shown)
			{
				//LineageImage.sprite = InterfaceManager.instance.LineageCrossedSprite;
				LineageImage.color = Color.green;
				//UpdateLineageColorButton(Color.green);
				//launch lineage app with pyinstaller
				AddPythonCommandToQueue("show_lineage", "", StandaloneMainMenuInteractions.PYINSTALLER_PORT);

				string lineage = MorphoTools.GetDataset().Infos.getLineageMorphoPlot(use_names:true);
				corout = StartCoroutine(receivePythonCommand(9801));
				AddPythonCommandToQueue("send_times", MorphoTools.GetDataset().MinTime + ":" + MorphoTools.GetDataset().MaxTime, PORT_LINEAGE);
				AddPythonCommandToQueue("lineage_info", lineage, PORT_LINEAGE);
				SendCellsColor();
				SendCellsHidden(true);

				StartCoroutine(receivePythonCommand(9873,false));

				foreach(Correspondence c in MorphoTools.GetDataset().Infos.Correspondences)
                {
                    if (c.isDisplayed)
                    {
						SendInfo(c.id_infos);
					}
                }
			}
			else
			{
				LineageImage.color = Color.white;
				//LineageImage.sprite = InterfaceManager.instance.LineageSprite;
				//launch lineage app with pyinstaller
				StopStandaloneLineage();

				if (corout != null)
				{
					StopCoroutine(corout);
				}
			}

		}

		public void RestartLineage()
        {
			if (lineage_shown)
			{
				string lineage = MorphoTools.GetDataset().Infos.getLineageMorphoPlot(use_names: true);
				//AddPythonCommandToQueue("send_times", MorphoTools.GetDataset().MinTime + ":" + MorphoTools.GetDataset().MaxTime, PORT_LINEAGE);
				AddPythonCommandToQueue("lineage_info", lineage, PORT_LINEAGE);
				SendCellsColor();
				SendCellsHidden(true);
				foreach(Correspondence c in MorphoTools.GetDataset().Infos.Correspondences)
				{
					if (c.isDisplayed)
					{
						SendInfo(c.id_infos);
					}
				}
			}
		}

		private void StopLineage()
        {
			lineage_shown = false;
			//LineageImage.sprite = InterfaceManager.instance.LineageSprite;
			LineageImage.color = Color.white;
			if (corout != null)
			{
				StopCoroutine(corout);
			}
		}

		public void OnDisable()
		{
			if (corout != null)
			{
				StopCoroutine(corout);
			}
		}

		public IEnumerator receivePythonCommand(int portNumber, bool save = true)
		{
			WWWForm form = new WWWForm();
			// MorphoDebug.Log("Listening on :" + plot_adress + portNumber);
			UnityWebRequest www = UnityWebRequests.Post("localhost:" + portNumber, "/get.html", form);
			yield return new WaitForSeconds(0.1f);
			//MorphoDebug.Log("wait receive");
			//THE FOLLOWING LINE CRASH AFTER RECEIVING 1 TIME STEP
			yield return www.SendWebRequest();
			if (www.isHttpError || www.isNetworkError)
			{
				//MorphoDebug.Log("Failed receiving command, starting again the waiting");
				www.Dispose();
				if(save)
					corout = StartCoroutine(receivePythonCommand(portNumber));
				else
					StartCoroutine(receivePythonCommand(portNumber));
			}
			else
			{
				String commande = Encoding.UTF8.GetString(www.downloadHandler.data);
				commande = commande.Replace("%20", " ");
				if (commande.StartsWith("select_cells"))
				{
					MorphoDebug.Log("Received select_cell: " + commande.ToString());
					ShowCell(commande.Split(';')[1],false);
				}
				else if (commande.StartsWith("lineage_stop") || commande.StartsWith("LINEAGE_STOP"))
				{
					MorphoDebug.Log("Received lineage_stop: " + commande.ToString());
					StopLineage();
				}
				else if (commande.StartsWith("refresh_lineage") || commande.StartsWith("REFRESH_LINEAGE"))
				{
					MorphoDebug.Log("Received refresh_lineage: " + commande.ToString());
					RestartLineage();
				}
				www.Dispose();
				corout = StartCoroutine(receivePythonCommand(portNumber));
			}
		}

		/// <summary>
		/// Coming from the lineage, select a cell in MorphoNet viewer
		/// </summary>
		/// <param name="p">cell t,id to select</param>
		public void ShowCell(string p, bool zoom = true)
		{
			Cell overcell = MorphoTools.GetDataset().getCell(p, false);
			PickedManager pm = MorphoTools.GetPickedManager();

			if (!pm.SelectCell(overcell))
				MorphoDebug.LogWarning("unable to find cell " + p);
		}

		[Serializable]
		public class CellColor
		{
			public Color color;
			public string[] cell_name;

			public CellColor(Color color, string[] name)
			{
				this.color = color;
				this.cell_name = name;
			}
		}

		[Serializable]
		public class CellColorList
		{
			public CellColor[] cellColorItems;

			public CellColorList(CellColor[] array)
			{
				cellColorItems = array;
			}
		}


		/// <summary>
		/// Send the color status of each cells to the standalone lineage window
		/// </summary>
		/// <param name="cells">List of cells to update status in the lineage , if null , all cells will be updated (longer)</param>
		public void SendCellsColor()
		{
			StringBuilder st = new StringBuilder();
			//Dictionary<Color, List<string>> cellsByColor = new Dictionary<Color, List<string>>();
			if (MorphoTools.GetDataset() != null && MorphoTools.GetDataset().CellsByTimePoint != null)
			{
				foreach (KeyValuePair<int, List<Cell>> cellsbytime in MorphoTools.GetDataset().CellsByTimePoint)
				{
					foreach (Cell cell in cellsbytime.Value)
					{
						st.AppendLine(cell.getName() + ":" + cell.GetCellColor());
					}
				}

			}
			AddPythonCommandToQueue("load_color", st.ToString(), PORT_LINEAGE);

		}

		/// <summary>
		/// Send the visibilility of multiples / all cells to the standalone lineage window
		/// </summary>
		/// <param name="cells"> List of cells to update status in the lineage , if null , all cells will be updated (longer)</param>
		public void SendCellsHidden(bool onlyHidden=false)
		{
			StringBuilder st = new StringBuilder();
			//Dictionary<Color, List<string>> cellsByColor = new Dictionary<Color, List<string>>();
			if (MorphoTools.GetDataset() != null && MorphoTools.GetDataset().CellsByTimePoint != null)
			{
				foreach (KeyValuePair<int, List<Cell>> cellsbytime in MorphoTools.GetDataset().CellsByTimePoint)
				{
					foreach (Cell cell in cellsbytime.Value)
					{
                        if (onlyHidden)
                        {
							if (!cell.show)
								st.AppendLine(cell.getName() + ":1");
						}
                        else
                        {
							st.AppendLine(cell.getName() + ":" + (cell.show ? "0" : "1"));
						}
							
					}
				}

			}
			AddPythonCommandToQueue("load_hide", st.ToString(), PORT_LINEAGE);
		}


		/// <summary>
		/// Clear all picked cells in the lineage
		/// </summary>
		public void SendClearPicked(List<Cell> cells)
		{
			StringBuilder st = new StringBuilder();
			if (MorphoTools.GetDataset() != null && MorphoTools.GetDataset().CellsByTimePoint != null)
			{
				foreach (Cell cell in cells)
				{
					st.AppendLine(cell.getName() + ":" + "0");
				}
			}

			AddPythonCommandToQueue("load_picked", st.ToString(), PORT_LINEAGE);
		}

		/// <summary>
		/// Send the visibilility of multiples / all cells to the standalone lineage window
		/// </summary>
		/// <param name="cells"> List of cells to update status in the lineage , if null , all cells will be updated (longer)</param>
		public void SendCellsPicked(bool zoom=true)
		{
			StringBuilder st = new StringBuilder();
			//Dictionary<Color, List<string>> cellsByColor = new Dictionary<Color, List<string>>();
			if (MorphoTools.GetDataset() != null && MorphoTools.GetDataset().CellsByTimePoint != null)
			{
				foreach (Cell cell in MorphoTools.GetPickedManager().clickedCells)
				{
					st.AppendLine(cell.getName() + ":" + "1");
				}
			}
            if (zoom) 
			{
				AddPythonCommandToQueue("load_picked", st.ToString(), PORT_LINEAGE);
			}
            else
            {
				MorphoTools.GetDataset().cellsearched = true;
				AddPythonCommandToQueue("back_load_picked", st.ToString(), PORT_LINEAGE);
			}
		}

		/// <summary>
		/// Build the content of an info before sending it to the lineage standalone window
		///
		/// </summary>
		/// <param name="id_info">Id of the info to build the content from (in the DatasetInfomations)</param>
		public void SendInfo(int id_info)
		{
			MorphoDebug.Log("Sending info to lineage");
			Correspondence choosen = null;
			string txt_to_send = "";
			foreach (Correspondence c in MorphoTools.GetInformations().Correspondences)
			{
				if (c.id_infos == id_info)
				{
					txt_to_send += c.GetTxtInfos().ToString();
					
					break;
				}
			}
			txt_to_send += "end_info";
			AddPythonCommandToQueue("load_info", txt_to_send, PORT_LINEAGE);
		}

		/// <summary>
		/// set information as displayed for the lineage, so it can be loaded in menus properly
		/// </summary>
		/// /// <param name="value">value to give to isDisplayed</param>
		/// <param name="id_info">id of the info to display</param>
		public void SetIsDisplayed(bool value, int id_info)
        {
			foreach (Correspondence c in MorphoTools.GetInformations().Correspondences)
			{
				if (c.id_infos == id_info)
				{
					c.isDisplayed = value;
					break;
				}
			}
			
		}

		/// <summary>
		/// Generate info content using an id and send this contect to be cleared from lineage
		/// </summary>
		/// <param name="id_info">Info from which the content will be removed</param>
		public void ClearInfo(int id_info)
		{
			MorphoDebug.Log("Clearing info to lineage");
			Correspondence choosen = null;
			string txt_to_send = "";
			foreach (Correspondence c in MorphoTools.GetInformations().Correspondences)
			{
				if (c.id_infos == id_info)
				{
					txt_to_send += c.GetTxtInfos().ToString();
					break;
				}
			}
			txt_to_send += "end_info";
			AddPythonCommandToQueue("clear_info", txt_to_send, PORT_LINEAGE);
		}

		/// <summary>
		/// Send the command to the Standalone system to hide the lineage
		/// </summary>
		public void StopStandaloneLineage()
		{
			//UpdateLineageColorButton(backup_button_color);
			AddPythonCommandToQueue("hide_lineage", "", StandaloneMainMenuInteractions.PYINSTALLER_PORT);

		}

	}

}
