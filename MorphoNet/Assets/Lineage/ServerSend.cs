using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Security.Principal;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using MorphoNet;

public class StreamString
{
    private Stream ioStream;
    private UnicodeEncoding streamEncoding;

    public StreamString(Stream ioStream)
    {
        this.ioStream = ioStream;
        streamEncoding = new UnicodeEncoding();
    }

    public string ReadString()
    {
        int len = 0;

        len = ioStream.ReadByte() * 256;
        len += ioStream.ReadByte();
        byte[] inBuffer = new byte[len];
        ioStream.Read(inBuffer, 0, len);

        return streamEncoding.GetString(inBuffer);
    }

    public int WriteString(string outString)
    {
        byte[] outBuffer = streamEncoding.GetBytes(outString);
        int len = outBuffer.Length;
        if (len > UInt16.MaxValue)
        {
            len = (int)UInt16.MaxValue;
        }
        ioStream.WriteByte((byte)(len / 256));
        ioStream.WriteByte((byte)(len & 255));
        ioStream.Write(outBuffer, 0, len);
        ioStream.Flush();

        return outBuffer.Length + 2;
    }
}

public class ServerSend : MonoBehaviour
{
    public Text feedback;
    // Start is called before the first frame update
    public void TheSendMessage(string lineage)
    {
        //Create Client Instance
        NamedPipeClientStream client = new NamedPipeClientStream(".", "MorphoNetLineage",
                       PipeDirection.InOut, PipeOptions.None,
                       TokenImpersonationLevel.None);

        //Connect to server
        client.Connect();
        //Created stream for reading and writing
        StreamString clientStream = new StreamString(client);
        //Read from Server
        string dataFromServer = clientStream.ReadString();
        MorphoDebug.Log("Received from Server: " + dataFromServer);
        //Send Message to Server
        clientStream.WriteString(lineage);
        //Close client
        client.Close();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
