﻿using UnityEngine;
using UnityEngine.UI;

namespace MorphoNet
{
	public class OnMouseOverLineage : MonoBehaviour
	{
		public LineageViewer lineage_viewer;
		private Vector3 prevMousePosition;
		public Camera lineage_camera;
		private float wait = 0f;
		private GameObject current;
		public GraphicRaycaster lineagecanvas;

		public GameObject cell_found = null;
		private bool hitted;

		private void Update()
		{
			if (lineage_viewer.isActive && lineage_viewer.hasFocus)
			{
				if (Input.GetMouseButtonDown(0))
				{
					RaycastHit hitInfo = new RaycastHit();
					bool hiting = false;

					hiting = Physics.Raycast(lineage_camera.ScreenPointToRay(Input.mousePosition), out hitInfo);
					int cell_time = 0;
					int cell_id = 0;
					int cell_channel = 0;
					if (hiting)
					{
						//MorphoDebug.Log("Hit : " + hitInfo.transform.gameObject.name);
						string[] cell_params = hitInfo.transform.gameObject.name.Split(',');

						if (cell_params.Length == 2 && !hitInfo.transform.gameObject.name.Contains("bar"))
						{
							cell_time = int.Parse(cell_params[0]);
							cell_id = int.Parse(cell_params[1]);
							cell_found = hitInfo.transform.gameObject;

							if (cell_found != null)
							{
								if (cell_found.transform.childCount > 0)
								{
									cell_found.transform.GetChild(0).gameObject.SetActive(true);
								}

								//	UIManager.instance.lineage_viewer.CancelOnMouseOver();
								//UIManager.instance.lineage_viewer.ShowObjectOnMouseOver(cell_found, cell_id, cell_time, cell_channel);
								//InterfaceManager.instance.lineage_viewer.ClickedObjectOnMouseOver(cell_found, cell_id, cell_time, cell_channel);
								MorphoDebug.LogError("Implement this lineage communication");
								hitted = true;
							}
						}
						else if (cell_params.Length == 3 && !hitInfo.transform.gameObject.name.Contains("bar"))
						{
							cell_time = int.Parse(cell_params[0]);
							cell_id = int.Parse(cell_params[1]);
							cell_channel = int.Parse(cell_params[2]);
							if (cell_found != null && cell_found.transform.childCount > 0)
							{
								cell_found.transform.GetChild(0).gameObject.SetActive(false);
							}
							cell_found = hitInfo.transform.gameObject;
							if (cell_found != null)
							{

								if (cell_found.transform.childCount > 0)
								{
									cell_found.transform.GetChild(0).gameObject.SetActive(true);
								}

								//UIManager.instance.lineage_viewer.CancelOnMouseOver();
								//UIManager.instance.lineage_viewer.ShowObjectOnMouseOver(cell_found, cell_id, cell_time, cell_channel);
								//nterfaceManager.instance.lineage_viewer.ClickedObjectOnMouseOver(cell_found, cell_id, cell_time, cell_channel);
								MorphoDebug.LogError("Implement this lineage communication");
								hitted = true;
							}
						}
						else
						{
							MorphoDebug.Log("Cell format is incorrect ");
						}

					}




				}

				//lineagecanvas.enabled = true;
				if (Vector3.Distance(Input.mousePosition, prevMousePosition) < 0.04f)
				{
					wait += Time.unscaledDeltaTime;
					//wait++;
				}
				else
				{
					wait = 0f;
					if (cell_found != null && cell_found.transform.childCount > 0)
					{
						cell_found.transform.GetChild(0).gameObject.SetActive(false);
					}
					MorphoDebug.LogError("Implement this lineage communication");
					//InterfaceManager.instance.lineage_viewer.CancelOnMouseOver();
					cell_found = null;
					hitted = false;
				}

				if (wait >= 0.1f && !hitted && cell_found == null)
				{
					RaycastHit hitInfo = new RaycastHit();
					bool hiting = false;

					hiting = Physics.Raycast(lineage_camera.ScreenPointToRay(Input.mousePosition), out hitInfo);
					int cell_time = 0;
					int cell_id = 0;
					int cell_channel = 0;
					if (hiting)
					{
						if (cell_found == null)
						{
							string[] cell_params = hitInfo.transform.gameObject.name.Split(',');

							if (cell_params.Length == 2 && !hitInfo.transform.gameObject.name.Contains("bar"))
							{
								cell_time = int.Parse(cell_params[0]);
								cell_id = int.Parse(cell_params[1]);
								cell_found = hitInfo.transform.gameObject;
							}
							else if (cell_params.Length == 3 && !hitInfo.transform.gameObject.name.Contains("bar"))
							{
								cell_time = int.Parse(cell_params[0]);
								cell_id = int.Parse(cell_params[1]);
								cell_channel = int.Parse(cell_params[2]);
								cell_found = hitInfo.transform.gameObject;
							}
							else
							{
								MorphoDebug.Log("Cell format is incorrect ");
							}
						}
					}


					if (cell_found != null)
					{
						if (cell_found.transform.childCount > 0)
						{
							cell_found.transform.GetChild(0).gameObject.SetActive(true);
						}
						MorphoDebug.LogError("Implement this lineage communication");
						//InterfaceManager.instance.lineage_viewer.ShowObjectOnMouseOver(cell_found, cell_id, cell_time, cell_channel);
						hitted = true;
					}
				}
				prevMousePosition = Input.mousePosition;
			}
			else
			{
				//lineagecanvas.enabled = false;
			}
		}

	}
}