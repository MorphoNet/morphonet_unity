﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using SimpleJSON;
using System.Text;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine.Networking;
using System.Globalization;
using System.Text.RegularExpressions;

namespace MorphoNet
{
    public class Curation
    {
        public int id;
        public MenuCurrated parent;
        public Cell c;
        public string value;
        public int id_people;
        public string date;
        public GameObject go;
        public bool isLast;
        public bool isProtected = false;
        public bool isActive = false;

        public Curation(GameObject goparent, int id, Cell c, string value, int id_people, string date, MenuCurrated source, bool is_protected = false, bool is_active = false)
        {
            parent = source;
            this.id = id;
            this.c = c;
            this.value = value;
            this.id_people = id_people;
            this.date = date;
            isLast = false;
            isProtected = is_protected;
            isActive = is_active;
            go = (GameObject)GameObject.Instantiate(parent.currationDefault, goparent.transform);
            go.name = this.id.ToString();
            if (parent.Users != null && parent.Users.ContainsKey(id_people))
            {
                go.transform.Find("people").gameObject.transform.GetComponent<Text>().text = parent.Users[id_people];
            }else if (LoadParameters.instance.id_dataset == 0)
            {
                go.transform.Find("people").gameObject.transform.GetComponent<Text>().text = "You";
            }
            go.transform.Find("date").gameObject.transform.GetComponent<Text>().text = date;
            go.transform.Find("object").gameObject.transform.GetComponent<Text>().text = c.getName();
            go.transform.Find("value").gameObject.transform.GetComponent<Text>().text = value;

            parent.propageateDescriptionListen(go.transform.Find("propagateFutur").gameObject.GetComponent<Button>(), this, true);
            parent.propageateDescriptionListen(go.transform.Find("propagatePast").gameObject.GetComponent<Button>(), this, false);

            if (isProtected == true)
            {
                go.transform.Find("delete").gameObject.SetActive(false);
            }

            go.transform.Find("propagateFutur").gameObject.SetActive(false);
            go.transform.Find("propagatePast").gameObject.SetActive(false);

            parent.deleteCurrationListen(go.transform.Find("delete").gameObject.transform.GetComponent<Button>(), this);
            go.SetActive(true);
        }


        public void delete()
        {
            //MorphoDebug.Log (" DEestroy " + this.id);
            GameObject.Destroy(go);
        }

        public void SetPosition(int Height)
        {
            go.SetActive(true);
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, Height - 80, go.transform.localPosition.z);
        }

        //We change color if it's the last curration
        public void active(Correspondence cor)
        {
            //this.c.setCuration(cor.id_infos, this);
            if (cor.datatype != "time")
            {
                go.transform.Find("propagateFutur").gameObject.SetActive(true);
                go.transform.Find("propagatePast").gameObject.SetActive(true);

                go.transform.Find("delete").gameObject.SetActive(true);
            }
            changeColor(new Color(255, 255, 255));
        }

        public void inActive()
        {
            go.transform.Find("propagateFutur").gameObject.SetActive(false);
            go.transform.Find("propagatePast").gameObject.SetActive(false);
            changeColor(new Color(125, 125, 125));

            go.transform.Find("delete").gameObject.SetActive(false);
        }

        public void changeColor(Color col)
        {
            go.transform.Find("people").gameObject.transform.GetComponent<Text>().color = col;
            go.transform.Find("object").gameObject.transform.GetComponent<Text>().color = col;
            go.transform.Find("date").gameObject.transform.GetComponent<Text>().color = col;
            go.transform.Find("value").gameObject.transform.GetComponent<Text>().color = col;
        }
    }

    public class MenuCurrated : MonoBehaviour
    {
        public static MenuCurrated instance = null;

        private void Awake()
        {
            if (instance == null)
                instance = this;
            else
                Destroy(gameObject);
        }

        public GameObject MenuCurration;
        public Dictionary<int, string> Users = new Dictionary<int, string>();
        public GameObject currationDefault;
        public Correspondence activeCorrespondence; //When Menu is open, it's easier ...
        public List<GameObject> Correspondences; //Listo of game object for each correspondence categories
        public Text CurationName;
        public InputField newvalue;
        public Text NbCell;
        public GameObject scrollbar;
        public DataSet AttachedDataset;
        public int directPlotCurId = 0;

        public bool FileCurationActive = true;
        public bool FileCurationProtected = true;
        public Dictionary<string, string> FileCurationCell = new Dictionary<string, string>();

        public void AttachDataset(DataSet source)
        {
            AttachedDataset = source;
        }

        public void init()
        {
            if (currationDefault == null)
            {
                MenuCurration = GameObject.Find("Canvas").transform.Find("bouton layout group").transform.Find("Properties").transform.Find("MenuProperties").gameObject.transform.Find("MenuCurration").gameObject;
                //initialCurrValue = MenuCurration.transform.Find("currationInitialValues").gameObject;
                currationDefault = MenuCurration.transform.Find("curration").gameObject;
                currationDefault.SetActive(false);
                if (LoadParameters.instance.id_dataset != 0)
                    StartCoroutine(listUsers());
                
                newvalue = MenuCurration.transform.Find("newvalue").GetComponent<InputField>();
                CurationName = MenuCurration.transform.Find("CurationName").GetComponent<Text>();
                NbCell = MenuCurration.transform.Find("NbCell").GetComponent<Text>();
                scrollbar = MenuCurration.transform.Find("Scrollbar").gameObject;
                scrollbar.GetComponent<Scrollbar>().onValueChanged.AddListener(delegate{ onScroll(); });
                scrollbar.SetActive(false);
                MenuCurration.transform.Find("Curate").GetComponent<Button>().onClick.AddListener(() => curate());
                //newvalue.onValueChanged.AddListener((s) => curate());
                if (MorphoTools.GetDataset().id_dataset == 0)
                {
                    MenuCurration.transform.Find("DownloadCurations").gameObject.SetActive(false);
                }
                else
                {
                    MenuCurration.transform.Find("DownloadCurations").GetComponent<Button>().onClick.AddListener(() => StartCoroutine(downloadCurationsFile()));
                }
                
            }
        }

        public void BindCurrationInitValue()
        {
            FileCurationCell = new Dictionary<string, string>();
            List<string> cell_names = new List<string>();
            List<string> cell_values = new List<string>();
            int active_count = 0;
            int inactive_count = 0;
            if (!InterfaceManager.instance.MenuInfos.activeSelf || MenuCurration == null || !MenuCurration.activeSelf)
            {
                return;
            }
            //initialCurrValue.SetActive(true);
            if (AttachedDataset.PickedManager.clickedCells.Count > 0)
            {
                foreach (Cell cell in AttachedDataset.PickedManager.clickedCells)
                {
                    if (activeCorrespondence != null && activeCorrespondence.Curations != null && activeCorrespondence.Curations.Find((curation) => (curation.c == cell && curation.isProtected == true)) != null)
                    {
                        //initialCurrValue.SetActive(false);
                        return;
                    }
                }
            }
            foreach (Cell c in AttachedDataset.PickedManager.clickedCells)
            {
                if (c.Infos != null && c.Infos.ContainsKey(activeCorrespondence.id_infos))
                {
                    FileCurationCell.Add(c.getName(), c.Infos[activeCorrespondence.id_infos]);
                    if (!cell_names.Contains(c.getName()))
                    {
                        cell_names.Add(c.getName());
                    }

                    if (!cell_values.Contains(c.Infos[activeCorrespondence.id_infos]))
                    {
                        cell_values.Add(c.Infos[activeCorrespondence.id_infos]);
                    }
                }
                if (activeCorrespondence.Curations == null)
                {
                    activeCorrespondence.Curations = new List<Curation>();
                }
                if (activeCorrespondence != null && activeCorrespondence.Curations != null && activeCorrespondence.Curations.Find((curation) => (curation.c == c && curation.isProtected == true)) == null || activeCorrespondence.Curations.Find((curation) => (curation.c == c && curation.isProtected == true)).isActive)
                {
                    active_count++;
                }
                else
                {
                    inactive_count++;
                }
            }

            if (AttachedDataset.PickedManager.clickedCells.Count == 1)
            {
                FileCurationActive = (activeCorrespondence != null && activeCorrespondence.Curations != null && (activeCorrespondence.Curations.Find((curation) => (curation.c == AttachedDataset.PickedManager.clickedCells[0] && curation.isProtected == true)) == null || activeCorrespondence.Curations.Find((curation) => (curation.c == AttachedDataset.PickedManager.clickedCells[0] && curation.isProtected == true)).isActive));
            }

            if (AttachedDataset.PickedManager.clickedCells.Count > 1)
            {
                FileCurationActive = (active_count >= inactive_count);
            }


            

            string displayed_name = "";
            bool over_name = false;
            int name_count = 0;
            foreach (string s in cell_names)
            {
                name_count++;
                if (name_count >= 5)
                { over_name = true; break; }
                displayed_name += s + ",";
            }

            string displayed_value = "";
            bool over_value = false;
            int value_count = 0;
            foreach (string s in cell_values)
            {
                value_count++;
                if (value_count >= 5)
                { over_value = true; break; }
                displayed_value += s + ",";
            }

            if (displayed_name.Length > 0)
                displayed_name = displayed_name.Remove(displayed_name.Length - 1, 1);
            if (displayed_value.Length > 0)
                displayed_value = displayed_value.Remove(displayed_value.Length - 1, 1);

            if (over_name)
            { displayed_name += ",..."; }
            if (over_value)
            { displayed_value += ",..."; }

            /*initialCurrValue.transform.Find("object").GetComponent<Text>().text = displayed_name;
            initialCurrValue.transform.Find("value").GetComponent<Text>().text = displayed_value;

            if(displayed_name!="" && displayed_value != "")
            {
                if (initialCurrValue != null && initialCurrValue.transform.Find("people") != null && LoadParameters.instance.id_dataset != 0)
                    if (initialCurrValue.transform.Find("people").GetComponent<Text>() != null && Users.ContainsKey(activeCorrespondence.id_owner))
                        initialCurrValue.transform.Find("people").GetComponent<Text>().text = Users[activeCorrespondence.id_owner];

                if (LoadParameters.instance.id_dataset == 0)
                    initialCurrValue.transform.Find("people").GetComponent<Text>().text = "You";
            }
            else
            {
                initialCurrValue.transform.Find("people").GetComponent<Text>().text = "";
            }*/

            //initialCurrValue.transform.Find("object").GetComponent<Text>().text = current;
        }

        public GameObject getCorrespondence(Correspondence cor)
        {
            if (Correspondences == null)
                Correspondences = new List<GameObject>();
            foreach (GameObject gocor in Correspondences)
                if (gocor.name == cor.id_infos.ToString())
                    return gocor;
            //Create a new one
            GameObject dflt = MenuCurration.transform.Find("correspondenceDefault").gameObject;
            GameObject newgocor = (GameObject)Instantiate(dflt);
            newgocor.transform.SetParent(MenuCurration.transform, false);
            newgocor.transform.position = new Vector3(dflt.transform.position.x, dflt.transform.position.y, dflt.transform.position.z);
            newgocor.name = cor.id_infos.ToString();
            Correspondences.Add(newgocor);
            return newgocor;
        }

        public Correspondence getCorrespondenceByName(string name)
        {
            if (AttachedDataset.Infos != null)
            {
                foreach (Correspondence cor in AttachedDataset.Infos.Correspondences)
                {
                    if (cor.name == name)
                    {
                        return cor;
                    }
                }
            }

            return null;
        }

        //List All USers
        public IEnumerator listUsers()
        {
            WWWForm form = new WWWForm();
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/unitypeople/?hash=" + SetsManager.instance.hash);

            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                Users = new Dictionary<int, string>();
                var N = JSONNode.Parse(www.downloadHandler.text);
                if (N != null && N.Count > 0)
                {
                    //MorphoDebug.Log("Found " + N.Count+ " users");
                    for (int i = 0; i < N.Count; i++)
                    { // : id,name,surname,email,login
                        int id_user = int.Parse(N[i]["id"].ToString().Replace('"', ' ').Trim());
                        string name = N[i]["name"].ToString().Replace('"', ' ').Trim();
                        string surname = N[i]["surname"].ToString().Replace('"', ' ').Trim();
                        Users[id_user] = surname + " " + name;
                    }
                }
            }
            www.Dispose();
        }

        public void loadCurationFromLine(string line)
        {
            string[] cur_infos = line.Split(',');
            string info_name = cur_infos[0].Replace('"', ' ').Trim();
            string id_object = cur_infos[1].Replace('"', ' ').Trim();
            string value = cur_infos[2].Replace('"', ' ').Trim();
            int id_people = 0;
            string date = cur_infos[3].Replace('"', ' ').Trim();
            Cell c = AttachedDataset.getCell(id_object, false);
            if (c != null)
            {
                Correspondence cor = getCorrespondenceByName(info_name);
                if (cor != null)
                {
                    GameObject gocor = getCorrespondence(cor);
                    Curation cur = new Curation(gocor, directPlotCurId, c, value, id_people, date, this);
                    cor.addCuration(cur);
                    setLastCuration(cor, c, cur);
                    directPlotCurId++;
                }
            }
        }

        //DOWNLOAD ALL  curration associated to a correspondence
        public IEnumerator downloadCurations(Correspondence cor)
        {
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/curration/?id_dataset=" + AttachedDataset.id_dataset.ToString() + "&id_correspondence=" + cor.id_infos.ToString());
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                //MorphoDebug.Log ("Currated : " + wwwCurration.text);
                if (www.downloadHandler.text != "")
                {
                    //We create a false curration with the original value
                    var N = JSONNode.Parse(www.downloadHandler.text);
                    if (N != null && N.Count > 0)
                    {
                        //MorphoDebug.Log("Found " + N.Count+ " Currated");
                        for (int i = 0; i < N.Count; i++)
                        { //id,id_object,value,id_people,date
                            //	MorphoDebug.Log(N[i].ToString());
                            int id = int.Parse(N[i]["id"].ToString().Replace('"', ' ').Trim());
                            string id_object = N[i]["id_object"].ToString().Replace('"', ' ').Trim();
                            string value = N[i]["value"].ToString().Replace('"', ' ').Trim();
                            int id_people = int.Parse(N[i]["id_people"].ToString().Replace('"', ' ').Trim());
                            string date = N[i]["date"].ToString().Replace('"', ' ').Trim();
                            bool active = false;
                            if (N[i]["active"] != null)
                                active = bool.Parse(N[i]["active"].ToString().Replace('"', ' ').Trim());
                            bool is_protected = false;
                            if (N[i]["protected"] != null)
                                is_protected = bool.Parse(N[i]["protected"].ToString().Replace('"', ' ').Trim());
                            //	MorphoDebug.Log(active + "  " + is_protected);
                            Cell c = AttachedDataset.getCell(id_object, true);
                            if (c != null)
                            {
                                GameObject gocor = getCorrespondence(cor);
                                Curation cur = new Curation(gocor, id, c, value, id_people, date, this, is_protected, active);
                                cor.addCuration(cur);
                                if (cur.isActive)
                                    setLastCuration(cor, c, cur);

                            }
                        }
                    }
                }
            }
            BindCurrationInitValue();
            www.Dispose();
        }

        public IEnumerator downloadCurationsFile()
        {
            int id_infos = activeCorrespondence.id_infos;
            MorphoDebug.Log(" downloadCurations File for " + id_infos);
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/curationfile/?id_dataset=" + AttachedDataset.id_dataset.ToString() + "&id_infos=" + id_infos.ToString());
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                if (www.downloadHandler.text != "")
                {
                    string urlinfos = MorphoTools.GetServer() + www.downloadHandler.text;//Instantiation.urlSERVER + "Infos/" + Instantiation.id_dataset + "-" + id_infos + ".txt";
#if !UNITY_EDITOR
				Recording.openWindow(urlinfos);
#else
                    MorphoDebug.Log("OPEN " + urlinfos);
#endif
                }
            }
        }

        //We remove the previous lasqt 	and set this new one
        public void setLastCuration(Correspondence cor, Cell c, Curation cur)
        {
            foreach (Curation curr in cor.Curations)
                if (curr.c == c)
                    curr.isLast = false;
            cur.isLast = true;
            c.setCuration(cor.id_infos, cur);

            CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            ci.NumberFormat.CurrencyDecimalSeparator = ".";

            if (cor.datatype == "genetic")
                AttachedDataset.GeneticManager.addCellGene(c, cor.name, float.Parse(cur.value, NumberStyles.Any, ci), cor);
            //MorphoDebug.Log ("For "+c.ID+" -> Last is " + cur.id);
        }

        //CREATE A NEW CURRATION Listen to curate button
        //public static void curateListen(Button b,Correspondence cor)  {b.onClick.AddListener (() => MenuCurrated.curate(cor)); }
        public void curate()
        {
            //MorphoDebug.Log("Curate for "+ LoadParameters.instance.id_dataset);
            foreach (Cell c in AttachedDataset.PickedManager.clickedCells)
                StartCoroutine(addCuration(activeCorrespondence, c));
            newvalue.text = "";
        }

        public void activeCurrationListen(Button b, Curation cur)
        { b.onClick.AddListener(() => { StartCoroutine(activeCuration(cur)); }); }

        public IEnumerator activeCuration(Curation cur)
        {
            MorphoDebug.Log("activeCurration for " + cur.id);
            if (LoadParameters.instance.id_dataset != 0)
            {
                string uri = "activecuration";
                if (cur.isActive)
                {
                    uri = "deactivecuration";
                }
                UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/" + uri + "/?id_curration=" + cur.id.ToString());
                yield return www.SendWebRequest();
                if (www.isHttpError || www.isNetworkError)
                    MorphoDebug.Log("Error : " + www.error);
                www.Dispose();
            }
            else
            {
                if (SetsManager.instance.directPlot != null)
                {
                    //SetsManager.instance.directPlot.DeleteCuration(activeCorrespondence.name, Regex.Escape(cur.c.getName()), cur.value, cur.date);
                }
            }
            cur.isActive = !cur.isActive;
            if (!cur.isActive && activeCorrespondence.Curations != null && activeCorrespondence.Curations.Count > 0) //Maybe not currations ...
                foreach (Curation curr2 in activeCorrespondence.Curations) //Un peu barbard tout ça ... on peut le faire que sur la dernière
                    if (curr2.id != cur.id && curr2.c == cur.c)
                    {
                        if (curr2.isActive)
                            setLastCuration(activeCorrespondence, curr2.c, curr2);
                    }

            if (cur.isActive)
            {
                setLastCuration(activeCorrespondence, cur.c, cur);
            }
            BindCurrationInitValue();
        }

        public void activeFileCurrationListen(Button b)
        { b.onClick.AddListener(() => { StartCoroutine(ActivateFileCur()); }); }

        private int flags_routine = 0;

        public IEnumerator ActivateFileCur()
        {
            foreach (KeyValuePair<string, string> pair in FileCurationCell)
            {
                StartCoroutine(activeFileCuration(!FileCurationActive, activeCorrespondence, pair.Value, pair.Key));
            }

            yield return new WaitUntil(() => flags_routine >= FileCurationCell.Count);
            FileCurationActive = !FileCurationActive;
            BindCurrationInitValue();
            //StartCoroutine(downloadCurations(activeCorrespondence));
            //showCuration();
            flags_routine = 0;
        }

        public IEnumerator activeFileCuration(bool status, Correspondence cor, string value, string c)
        {
            if (LoadParameters.instance.id_dataset != 0)
            {
                string uri = "activefilecuration";
                if (status)
                {
                    uri = "deactivefilecuration";
                }
                UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/" + uri + "/?id_dataset=" + AttachedDataset.id_dataset.ToString() + "&id_infos=" + cor.id_infos.ToString() + "&cell=" + c + "&value=" + value + "&date=" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
                {
                    www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
                }

                yield return www.SendWebRequest();
                if (www.isHttpError || www.isNetworkError)
                    MorphoDebug.Log("Error : " + www.error);

                int id_curation;
                MorphoDebug.Log(Encoding.UTF8.GetString(www.downloadHandler.data));
                if (int.TryParse(Encoding.UTF8.GetString(www.downloadHandler.data), out id_curation))
                {
                    Cell cell = AttachedDataset.getCell(c, false);
                    if (cell != null)
                    {
                        GameObject gocor = getCorrespondence(cor);
                        Curation cur = new Curation(gocor, id_curation, cell, value, SetsManager.instance.IDUser, DateTime.Now.ToString(), this, is_protected: true, is_active: status);
                        cor.addCuration(cur);
                        //setLastCuration(cor, cell, cur);
                    }
                    //StartCoroutine(downloadCurations(activeCorrespondence));
                    //openCuration(activeCorrespondence);
                }

                www.Dispose();
            }
            else
            {
                if (SetsManager.instance.directPlot != null)
                {
                    //SetsManager.instance.directPlot.DeleteCuration(activeCorrespondence.name, Regex.Escape(cur.c.getName()), cur.value, cur.date);
                }
            }

            flags_routine++;
        }



        public IEnumerator protectCuration(Curation cur)
        {
            //MorphoDebug.Log ("deleteCurration for " +cur.id);
            if (LoadParameters.instance.id_dataset != 0)
            {
                string uri = "protectcuration";
                if (cur.isProtected)
                {
                    uri = "unprotectcuration";
                }
                UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/" + uri + "/?id_curration=" + cur.id.ToString());
                yield return www.SendWebRequest();
                if (www.isHttpError || www.isNetworkError)
                    MorphoDebug.Log("Error : " + www.error);
                www.Dispose();
            }
            else
            {
                if (SetsManager.instance.directPlot != null)
                {
                    //SetsManager.instance.directPlot.DeleteCuration(activeCorrespondence.name, Regex.Escape(cur.c.getName()), cur.value, cur.date);
                }
            }
        }

        ///Add a curation in the DB (from the input filed)
        public IEnumerator addCuration(Correspondence cor, Cell c, string value = "")
        {
            //MorphoDebug.Log(cor.ToString());
            if (value == "")
                value = newvalue.text;
            if (LoadParameters.instance.id_dataset != 0)
            {
                activeCorrespondence = cor;
                //MorphoDebug.Log(" Add A curration in the DB for " + c.getName());
                WWWForm form = new WWWForm();
                form.AddField("id_dataset", AttachedDataset.id_dataset.ToString());
                form.AddField("id_correspondence", cor.id_infos.ToString());
                form.AddField("id_object", c.getName());
                form.AddField("date", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                form.AddField("value", value);
                form.AddField("active", 1);
                UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/createcurration/", form);
                if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
                {
                    www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
                }

                yield return www.SendWebRequest();
                if (www.isHttpError || www.isNetworkError)
                    MorphoDebug.Log("Error : " + www.error);
                else
                {
                    int id_curation;
                    MorphoDebug.Log(Encoding.UTF8.GetString(www.downloadHandler.data));
                    if (int.TryParse(Encoding.UTF8.GetString(www.downloadHandler.data), out id_curation))
                    {
                        GameObject gocor = getCorrespondence(cor);
                        Curation cur = new Curation(gocor, id_curation, c, value, SetsManager.instance.IDUser, DateTime.Now.ToString(), this, is_active: true);
                        cor.addCuration(cur);
                        setLastCuration(cor, c, cur);
                        //StartCoroutine(downloadCurations(activeCorrespondence));
                        //openCuration(activeCorrespondence);
                    }
                    else
                        MorphoDebug.Log("ERROR Curration " + www.downloadHandler.text);
                }
                www.Dispose();
                showCuration();
            }
            else//DIRECT PLOT MODE
            {
                if (SetsManager.instance.directPlot != null)
                {
                    SetsManager.instance.directPlot.CurateInfo(cor.name, Regex.Escape(c.getName()), value);
                    GameObject gocor = getCorrespondence(cor);
                    Curation cur = new Curation(gocor, directPlotCurId, c, value, SetsManager.instance.IDUser, DateTime.Now.ToString(), this);
                    directPlotCurId++;
                    cor.addCuration(cur);
                    setLastCuration(cor, c, cur);
                    showCuration();
                }
            }
        }

        //SHOW   Menu Curration click
        public void showCurationListen(Button b, Correspondence cor)
        { b.onClick.AddListener(() => openCuration(cor)); }

        //Show list of curration when click on menu
        public void openCuration(Correspondence cor)
        {
            newvalue.text = "";
            CurationName.text = cor.name;
            if (MenuCurration.activeSelf)
            {//Menu Already Open, we close it only if the coorespondence ar the same
                if (activeCorrespondence.id_infos != cor.id_infos)
                {
                    activeCorrespondence = cor;
                    MenuCurration.SetActive(true);
                    showCuration();
                }
                else
                    MenuCurration.SetActive(false);
            }
            else
            { //Else we just open it
                activeCorrespondence = cor;
                MenuCurration.SetActive(true);
                showCuration();
            }

            BindCurrationInitValue();
        }

        public void showCuration()
        {
            //We We only active the correspondence GameOjbect
            if (AttachedDataset.PickedManager.clickedCells.Count == 1)
            {
                string value = AttachedDataset.PickedManager.clickedCells[0].getName();
                NbCell.text = value;
            }
            else if (AttachedDataset.PickedManager.clickedCells.Count > 0)
                NbCell.text = AttachedDataset.PickedManager.clickedCells.Count.ToString() + " objects";
            else
                NbCell.text = "No object";
            if (Correspondences != null)
                foreach (GameObject gocir in Correspondences)
                    if (gocir.name == activeCorrespondence.id_infos.ToString())
                    {
                        //MorphoDebug.Log ("OK FOR gocir.name=" + gocir.name);
                        gocir.SetActive(true);
                    }
                    else
                    {
                        //MorphoDebug.Log ("NO FOR gocir.name=" + gocir.name);
                        gocir.SetActive(false);
                    }
            nbSCroll = 1;
            if (activeCorrespondence.Curations != null)
                updateShowCuration();
            else
                scrollbar.SetActive(false);
        }

        public void updateShowCuration()
        {
            int nbCurated = 0;
            int startY = 0;
            foreach (Curation cur in activeCorrespondence.Curations)
            {
                if (AttachedDataset.PickedManager.clickedCells.Contains(cur.c))
                {
                    nbCurated += 1;
                    if (nbCurated >= nbSCroll && nbCurated < nbSCroll + maxNbDraw)
                    {
                        cur.SetPosition(startY);
                        startY -= 20;
                        if (cur.isLast)
                            cur.active(activeCorrespondence);
                        else
                            cur.inActive();
                    }
                    else
                        cur.go.SetActive(false);
                }
                else
                    cur.go.SetActive(false);
            }
            if (nbCurated > maxNbDraw)
            {
                scrollbar.SetActive(true);
                scrollbar.GetComponent<Scrollbar>().numberOfSteps = 1 + nbCurated - maxNbDraw; //DEFINE SCROLLBAR LENGTH
                scrollbar.GetComponent<Scrollbar>().size = 1f / (float)(1 + nbCurated - maxNbDraw);
            }
            else
            {
                scrollbar.SetActive(false);
                nbSCroll = 1;
            }
        }

        public void LinkCellsByCuration()
        {
        }

        public void UnlinkCellsByCuration()
        {
        }

        //FOR SCROLL BAR
        public int maxNbDraw = 10;

        public float sizeBar = 20f;
        public int nbSCroll = 1;

        public void onScroll()
        {
            int nbSteps = scrollbar.GetComponent<Scrollbar>().numberOfSteps;
            float value = scrollbar.GetComponent<Scrollbar>().value;
            nbSCroll = nbSteps - (int)Mathf.Round(nbSteps - value * (nbSteps - 1) - 1);
            updateShowCuration();
        }

        //Propagate a string descriont
        public void propageateDescriptionListen(Button b, Curation cur, bool v)
        { b.onClick.AddListener(() => propageateDescription(cur, v)); }

        public void propageateDescription(Curation cur, bool futur)
        {  //Futur=true, Past=False
            Cell c = cur.c;
            MorphoDebug.Log("Propagate" + cur.c.ID + "to " + futur);
            if (futur)
            { //Futur
                while (c.Daughters != null && c.Daughters.Count == 1 && c.Daughters[0].Mothers.Count == 1)
                { //Only One Daughter
                    Cell d = c.Daughters[0];
                    if (d.getCuration(activeCorrespondence.id_infos) != cur.value)
                    {
                        d.setCuration(activeCorrespondence.id_infos, cur);
                        StartCoroutine(addCuration(activeCorrespondence, d, cur.value));
                    }
                    c = d;
                }
            }
            else
            { //PAST
                while (c.Mothers != null && c.Mothers.Count == 1 && c.Mothers[0].Daughters.Count == 1)
                { //Only One Mother and never divide
                    Cell m = c.Mothers[0];
                    if (m.getInfos(activeCorrespondence.id_infos) != cur.value)
                    {
                        m.setCuration(activeCorrespondence.id_infos, cur);
                        StartCoroutine(addCuration(activeCorrespondence, m, cur.value));
                    }
                    c = m;
                }
            }
        }

        //DELETE A curration
        public void deleteCurrationListen(Button b, Curation cur)
        { b.onClick.AddListener(() => StartCoroutine(deleteCurration(cur))); }

        public IEnumerator deleteCurration(Curation cur)
        {
            if (LoadParameters.instance.id_dataset != 0)
            {
                UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/deletecurration/?id_curration=" + cur.id.ToString());
                yield return www.SendWebRequest();
                if (www.isHttpError || www.isNetworkError)
                    MorphoDebug.Log("Error : " + www.error);
                www.Dispose();
            }
            else
            {
                if (SetsManager.instance.directPlot != null)
                {
                    SetsManager.instance.directPlot.DeleteCuration(activeCorrespondence.name, Regex.Escape(cur.c.getName()), cur.value, cur.date);
                }
            }
            //WE need to find the last curration for this cell
            if (activeCorrespondence.Curations != null && activeCorrespondence.Curations.Count > 0) //Maybe not currations ...
                foreach (Curation curr in activeCorrespondence.Curations) //Un peu barbard tout ça ... on peut le faire que sur la dernière
                    if (curr.id != cur.id && curr.c == cur.c)
                    {
                        //if (cur.isActive)
                            setLastCuration(activeCorrespondence, curr.c, curr);
                    }
                    else
                        cur.c.deleteCuration(activeCorrespondence.id_infos, curr); //Remove the Curation in the cell

            activeCorrespondence.deleteCuration(cur); //Remove the curration from the correspondence
            showCuration();
        }
    }
}