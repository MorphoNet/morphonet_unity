using UnityEngine;

namespace MorphoNet
{
    public class ConnectionInfo : MonoBehaviour
    {
        public GameObject Source;
        public GameObject Destination;
        public Vector3 SourcePos;
        public Vector3 DestPos;
        public CellChannelRenderer SCh;
        public CellChannelRenderer DCh;
    }
}