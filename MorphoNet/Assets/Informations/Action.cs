﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace AssemblyCSharp
{
	public class Action
	{
		public GameObject go;
		public Quaternion rotation;
		public Vector3 position;
		public float scale;
		public int time;
		public bool interpolate;
		public int framerate;

		public Action (GameObject go,int time,float scale,Vector3 position,Quaternion rotation,bool interpolate,int framerate)
		{
			this.go = go;
			this.time = time;
			this.position = position;
			this.scale = scale;
			this.rotation = rotation;
			this.interpolate = interpolate;
			this.framerate = framerate;
			go.SetActive (true);
			go.transform.Find ("name").gameObject.GetComponent<Text> ().text = "time "+time.ToString();
			go.transform.Find ("framerate").gameObject.GetComponent<InputField> ().text = framerate.ToString();
			if (interpolate)
				go.transform.Find ("interpolate").gameObject.GetComponent<Toggle> ().isOn = true;
			else
				go.transform.Find ("interpolate").gameObject.GetComponent<Toggle> ().isOn = false;
			go.transform.Find ("framerate").gameObject.GetComponent<InputField>().onValueChanged.AddListener (delegate {changeFrameRate ();});
			go.transform.Find ("interpolate").gameObject.GetComponent<Toggle> ().onValueChanged.AddListener (delegate {changeInterpolate ();});
		}



		public void changeFrameRate(){
			int fps = 0;
			if(int.TryParse(go.transform.Find ("framerate").gameObject.GetComponent<InputField> ().text, out fps ) )
				framerate=fps;
		}
		public void changeInterpolate(){
			interpolate=go.transform.Find ("interpolate").gameObject.GetComponent<Toggle> ().isOn;
		}
		//Just render this action active
		public void active(){
			go.GetComponent<Image> ().color = new  Color (1f, 1f, 1f, 150f / 255f);
		}
		public void unActive(){
			go.GetComponent<Image> ().color = new  Color  (1f, 1f, 1f, 100f / 255f);
		}

	}



}

