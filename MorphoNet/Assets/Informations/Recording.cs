﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using SimpleJSON;
using System.Text;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using AssemblyCSharp;
using System.Globalization;
using System.IO;
using Unity.Collections;
using SimpleFileBrowser;

namespace MorphoNet
{
    public class Recording : MonoBehaviour
    {
        public GameObject MenuScenario;
        public GameObject MenuActions;
        public GameObject MenuShare;
        public GameObject defaultScenario;
        public GameObject defaultAction;
        public GameObject scrollActionGO;
        public Scrollbar scrollAction;
        public Dictionary<int, GameObject> Scenarios = new Dictionary<int, GameObject>();
        public Dictionary<int, Action> Actions;
        public bool actionDone = false;
        public int actionCurrent;
        public Text currenttextloaded;

        public GameObject framerate;
        public GameObject loadingtext;
        public GameObject BoutonDataset;
        public GameObject BoutonImages;
        public GameObject BoutonInfos;
        public GameObject Shortcut;
        public GameObject BoutonObjects;
        public GameObject BoutonAniseed;
        public GameObject BoutonMovie;
        public GameObject BoutonUsers;
        public GameObject BoutonDebug;
        public GameObject PreviousStep;
        public GameObject BoutonGroups;
        public GameObject NexStep;
        public GameObject MenuClicked;
        public GameObject MenuRender;

        public GameObject AxisFigure;
        public GameObject FigureParams;
        public GameObject LegendFigure;
        public GameObject InfoPicked;

        public GameObject hologramO;
        public DataSet AttachedDataset;

        public GameObject lineage_button;
        public GameObject time_bar;
        public Camera ShotCamera;
        public List<Texture2D> shots = new List<Texture2D>();

        public GameObject screenshotPanel;
        public string backup_dataset_name;
        public Text dataset_name_object;
        public string custom_screenshot_text;
        public GameObject morphonet_logo;
        public bool include_time = false;
        public bool change_text = false;

        private string _MoviePath = null;


        // Use this for initialization
        public void AttachDataset(DataSet source)
        {
            AttachedDataset = source;
        }

        private void Start()
        {
            dataset_name_object = InterfaceManager.instance.datasetname;
            morphonet_logo = InterfaceManager.instance.MorphoNetLogo;
            screenshotPanel = InterfaceManager.instance.MenuMovie.transform.Find("Layout").Find("MenuScenario").Find("CustomText").gameObject;
            MenuScenario = InterfaceManager.instance.MenuScenario;
            MenuActions = InterfaceManager.instance.MenuActions;
            MenuShare = InterfaceManager.instance.MenuShare;
            defaultScenario = InterfaceManager.instance.default_scenar;
            defaultAction = InterfaceManager.instance.default_action;
            scrollActionGO = InterfaceManager.instance.scrollbar_actionO;
            scrollAction = InterfaceManager.instance.scrollbar_action;
            framerate = InterfaceManager.instance.Framerate.gameObject;
            loadingtext = InterfaceManager.instance.loading_text;
            BoutonDataset = InterfaceManager.instance.BoutonDataset;
            BoutonImages = InterfaceManager.instance.BoutonImages;
            BoutonInfos = InterfaceManager.instance.BoutonInfos;
            Shortcut = InterfaceManager.instance.BoutonShortcuts;
            BoutonObjects = InterfaceManager.instance.BoutonObjects;
            BoutonAniseed = InterfaceManager.instance.BoutonAniseed;
            BoutonMovie = InterfaceManager.instance.BoutonMovie;
            //BoutonDebug = UIManager.instance.BoutonDebug;
            PreviousStep = InterfaceManager.instance.PreviousStep;
            BoutonGroups = InterfaceManager.instance.BoutonGroups;
            NexStep = InterfaceManager.instance.NexStep;
            MenuClicked = InterfaceManager.instance.MenuClicked;
            hologramO = InterfaceManager.instance.hologramO;
            ShotCamera = InterfaceManager.instance.ShotCamera;
            hologram = InterfaceManager.instance.hologram;
            MenuRender = InterfaceManager.instance.MenuRender;

            AxisFigure = InterfaceManager.instance.AxisFigure;
            FigureParams = InterfaceManager.instance.FigureParams;
            LegendFigure = InterfaceManager.instance.LegendFigure;
            InfoPicked = InterfaceManager.instance.InfoPicked;

            time_bar = InterfaceManager.instance.TimeBar;
            Scenarios = new Dictionary<int, GameObject>();
            actionDone = false;
            //openWindow (""); //We have to initialize it first
            defaultScenario.SetActive(false);
            CheckScenarios();
            //StartCoroutine (checkScenarios ());
            //NetworkServer.Listen(4444);
            //NetworkServer.RegisterHandler(MsgType.Ready, OnPlayerReadyMessage);
            //if(SetsManager.instance.IDUser==1) hologramO.SetActive(true);else hologramO.SetActive(false);

#if UNITY_STANDALONE
            MenuActions.transform.Find("save").gameObject.SetActive(false);
            MenuActions.transform.Find("savelocal").gameObject.SetActive(true);
#else
            MenuActions.transform.Find("save").gameObject.SetActive(true);
            MenuActions.transform.Find("savelocal").gameObject.SetActive(false);
#endif
            
            
            if (SetsManager.instance.userRight >= 2)
            {//PULBIC
                MenuActions.transform.Find("save").gameObject.SetActive(false);
                MenuActions.transform.Find("record").gameObject.SetActive(false);
                MenuActions.transform.Find("cancel").gameObject.SetActive(false);
                MenuActions.transform.Find("savelocal").gameObject.SetActive(false);
            }

#if UNITY_STANDALONE

            if (LoadParameters.instance.id_dataset == 0)
            {
                MenuActions.transform.Find("save").gameObject.SetActive(false);
                MenuActions.transform.Find("record").gameObject.SetActive(true);
                MenuActions.transform.Find("cancel").gameObject.SetActive(false);
                MenuActions.transform.Find("savelocal").gameObject.SetActive(false);
            }
            

#endif

            //StartCoroutine(checkScenarios());
        }

        public void CheckScenarios()
        {
            if (LoadParameters.instance.id_dataset != 0)
                StartCoroutine(checkScenarios());
        }

        public void hideThings(bool v, bool menuws)
        {
            if (lineage_button == null)
            {
                lineage_button = InterfaceManager.instance.lineage_button;
            }

            if (lineage_button != null)
            {
                lineage_button.SetActive(v);
            }

            framerate.SetActive(v);
            loadingtext.SetActive(v);
            BoutonDataset.SetActive(v);
            BoutonImages.SetActive(v);
            BoutonInfos.SetActive(v);
            Shortcut.SetActive(v);
            BoutonObjects.SetActive(v);
#if !UNITY_WEBGL
            InterfaceManager.instance.BoutonLineageViewer.SetActive(v);
#endif
            InterfaceManager.instance.Params.SetActive(false);
            InterfaceManager.instance.BoutonSettings.SetActive(v);
            InterfaceManager.instance.TimeBar.SetActive(v);
            InterfaceManager.instance.recordingPanel.SetActive(v);
            if (InterfaceManager.instance.MeasurementPanel != null)
            {
                InterfaceManager.instance.MeasurementPanel.SetActive(v);
            }

            if (InterfaceManager.instance.MeasurementMenu != null)
            {
                InterfaceManager.instance.MeasurementMenu.SetActive(v);
            }
            //BoutonHelp.SetActive (v);
            BoutonAniseed.SetActive(v);
            BoutonMovie.SetActive(v);
            if (MenuRender != null)
                MenuRender.SetActive(v);
            BoutonGroups.SetActive(v);
            PreviousStep.SetActive(v);
            foreach (GameObject g in InterfaceManager.instance.snap_back)
            {
                if (v)
                {
                    g.SetActive((g.transform.parent.parent.GetChild(1) != g.transform.parent.parent.GetChild(1).gameObject.activeSelf && g.transform.parent.parent.GetComponent<MoveMenu>() != null && g.transform.parent.parent.GetComponent<MoveMenu>().is_fixed));
                }
                else
                {
                    g.SetActive(false);
                }
            }

            foreach (GameObject g in InterfaceManager.instance.HelpButtons)
            {
                g.SetActive(v);
            }
            NexStep.SetActive(v);
            InterfaceManager.instance.containerCurrations.SetActive(v == true ? LoadParameters.instance.id_dataset == 0 : false);
            MenuClicked.SetActive(v);

            if (InterfaceManager.instance.ButtonBackToMenu != null)
            {
                InterfaceManager.instance.ButtonBackToMenu.SetActive(v);
            }

            if (InterfaceManager.instance.ButtonVR != null)
            {
                InterfaceManager.instance.ButtonVR.gameObject.SetActive(v);
            }
            if (InterfaceManager.instance.BoutonSettings != null)
            {
                InterfaceManager.instance.BoutonSettings.SetActive(v);
            }

            if (v)
            { //Reactive
                if (menuws)
                {//From Record)
                    MenuActions.SetActive(true);
                    MenuScenario.SetActive(false);
                }
                else
                { //From Snapshot
                    MenuActions.SetActive(false);
                    MenuScenario.SetActive(true);
                }
                AxisFigure.SetActive(true);
                FigureParams.SetActive(false);
                LegendFigure.SetActive(true);
                InfoPicked.SetActive(false);
            }
            else
            {
                AxisFigure.SetActive(v);
                FigureParams.SetActive(v);
                LegendFigure.SetActive(v);
                InterfaceManager.instance.RenderButtonPanel.SetActive(v);
                if(RightManager.Instance.canAccessRecorder)InterfaceManager.instance.RecordButtonPanel.SetActive(v);
                InfoPicked.SetActive(v);
                MenuActions.SetActive(v);
                MenuScenario.SetActive(v);
            }
        }

        /// ////////////SNAPSHOT

        //Create a screen shot and export it
        public void screenshot()
        {
            string filename = "user_" + SetsManager.instance.IDUser.ToString() + ".png";// + "_screenshot" + System.DateTime.Now.ToString ("dd-MM-yyyy-HH-mm-ss") + ".png";
            StartCoroutine(TakeScreenShot(filename));
        }

        public void ShowPanel()
        {
            screenshotPanel.SetActive(!screenshotPanel.activeSelf);
        }

        public void onToggleText(bool value)
        {
            change_text = value;
        }

        public void onToggleTime(bool value)
        {
            include_time = value;
        }

        public void StoreCustomScreenshotText(string text)
        {
            custom_screenshot_text = text;
        }

        public IEnumerator TakeScreenShot(string filename)
        {
            Debug.Log("Taking screenshot at time : "+AttachedDataset.CurrentTime);
            backup_dataset_name = dataset_name_object.text;
            string new_display_text = change_text ? custom_screenshot_text : dataset_name_object.text;
            if (include_time)
            {
                new_display_text += " (time point : " + AttachedDataset.CurrentTime + ")";
            }
            dataset_name_object.text = new_display_text;

            if (morphonet_logo != null)
                morphonet_logo.SetActive(true);
            Debug.Log("Hiding thinks before Taking screenshot at time : "+AttachedDataset.CurrentTime);
            hideThings(false, false);
            yield return new WaitForEndOfFrame();
            var tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false); // Create a texture the size of the screen, RGB24 format
            tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0); // Read screen contents into the texture
            tex.Apply();
            Debug.Log("Generated texture Taking screenshot at time : "+AttachedDataset.CurrentTime);
            byte[] bytes = tex.EncodeToPNG(); // Encode texture into PNG
            Debug.Log("Generated bytes before Taking screenshot at time : "+AttachedDataset.CurrentTime);
            Destroy(tex);

#if UNITY_STANDALONE

            SetFileBrowserPath(bytes);

#elif UNITY_WEBGL

            WWWForm form = new WWWForm();
            form.AddField("movie", 0);
            form.AddField("name", filename);
            form.AddBinaryData("fileUpload", bytes, "screenShot.png", "image/png");
            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/screenshot/", form);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                MorphoDebug.Log("You can find the screenshot at : " + MorphoTools.GetServer() + "Snapshot/" + filename);
#if !UNITY_EDITOR
			openWindow(MorphoTools.GetServer()+"Snapshot/"+filename);
#endif
            }
            www.Dispose();
#endif
            Debug.Log("Written texture Taking screenshot at time : "+AttachedDataset.CurrentTime);
            hideThings(true, false);
            Debug.Log("Displayed things Taking screenshot at time : "+AttachedDataset.CurrentTime);
            dataset_name_object.text = backup_dataset_name;
            if (morphonet_logo != null)
                morphonet_logo.SetActive(false);
        }

#if UNITY_WEBGL
		//Call Open Window in Javascript (Plugins/WebGL)
		[DllImport("__Internal")]
		public static extern void openWindow(string url);
#else

        public static void openWindow(string url)
        { }

#endif

        /// ///////////////////MOVIE
        private bool record = false;

        private int recordAction = 0;
        public int id_scenario_active = -1;
        public int id_scenario_owner = -1;
        public Toggle hologram;
        public int nbHoloAngle = 0;

        public void onRecord()
        { StartCoroutine(createRecord()); }

        public IEnumerator createRecord()
        {
#if UNITY_WEBGL
            WWWForm form = new WWWForm();
            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/createrecord/", form);
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                if (Actions.Count > 0)
                {
                    hideThings(false, true);
                    actionCurrent = 0;
                    currentAction = Actions[actionCurrent];
                    ScenarioPlay = false;
                    actionDone = true;
                    stepFrame = 0;
                    record = true;
                    recordAction = 2;
                    frameNB = 0;
                }
            }
            www.Dispose();
#elif UNITY_STANDALONE

            StartCoroutine(WaitForMoviePathPicker());
            
#endif
            yield return null;
        }

        private int frameNB = 0;

        public IEnumerator frameRecord()
        {
            //currenttextloaded.text = "Record frame "+actionNB+"/"+lastAction;
            //MorphoDebug.Log (currenttextloaded.text);
            yield return new WaitForEndOfFrame();

            var tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false); // Create a texture the size of the screen, RGB24 format
            tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0); // Read screen contents into the texture
            tex.Apply();
            byte[] bytes = tex.EncodeToPNG(); // Encode texture into PNG
            Destroy(tex);

#if UNITY_WEBGL
            WWWForm form = new WWWForm();
            form.AddField("name", "movie_" + frameNB + ".png");
            form.AddBinaryData("fileUpload", bytes, "screenShot.png", "image/png");
            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/recordframe/", form);
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            www.Dispose();
#elif UNITY_STANDALONE

            if (_MoviePath != null) {
                File.WriteAllBytes(Path.Combine(_MoviePath,"movie_"+frameNB+".png"), bytes);
            }

#endif

            frameNB++;
            recordAction = 2;

        }

        public int framerate_test;
        public int nb_shots;
        public int delay_seconds;

        public void startShot()
        {
            StartCoroutine(testMovieCreation());
        }

        public void StoreFramerate(string value)
        { int.TryParse(value, out framerate_test); }

        public void StoreNbShots(string number)
        { int.TryParse(number, out nb_shots); }

        public void StoreDelay(string value)
        { int.TryParse(value, out delay_seconds); }

        public IEnumerator testMovieCreation()
        {
            yield return null;
        }

        public IEnumerator frameRecordHologram()
        {
            //currenttextloaded.text = "Record frame "+frameNB+" Angle "+nbHoloAngle;
            //MorphoDebug.Log (currenttextloaded.text);
            //if(nbHoloAngle>0) ArcBall.rotate (Quaternion.AngleAxis (nbHoloAngle*90f, new Vector3 (0, 1f, 0))*Quaternion.AngleAxis (-90f, new Vector3 (1f, 0, 0)));
            //else  ArcBall.rotate (Quaternion.AngleAxis (-90f, new Vector3 (1f, 0, 0)));

            yield return new WaitForEndOfFrame();
            var tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false); // Create a texture the size of the screen, RGB24 format
            tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0); // Read screen contents into the texture
            tex.Apply();
            byte[] bytes = tex.EncodeToPNG(); // Encode texture into PNG
            Destroy(tex);
            WWWForm form = new WWWForm();
            form.AddField("name", "movie_" + frameNB + "_" + nbHoloAngle + ".png");
            form.AddBinaryData("fileUpload", bytes, "screenShot.png", "image/png");
            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/recordframe/", form);
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            www.Dispose();
            nbHoloAngle++;
            AttachedDataset.TransformationsManager.SetRotation(Quaternion.Euler(new Vector3(0f, nbHoloAngle * 90f, 0f)));
            //ArcBall.addRotate(Quaternion.Euler(new Vector3(0f, 90f, 0f)));
            //ArcBall.addRotate (Quaternion.AngleAxis (90f, Vector3.up));

            recordAction = 0;
            if (nbHoloAngle == 4)
            {
                nbHoloAngle = 0;
                frameNB++;
                recordAction = 2;
            }
            //Perform the rotation

            MorphoDebug.Log("END Record frame " + frameNB + " Angle " + nbHoloAngle);
        }

        public IEnumerator finishRecord()
        {
            loadingtext.SetActive(true);
            loadingtext.GetComponent<Text>().text = "Wait a litte for your movie...";
            //MorphoDebug.Log ("Save record");
            record = false;
            recordAction = -1;
            actionDone = false;

#if UNITY_WEBGL

            WWWForm form = new WWWForm();
            string outputname = "Movie_" + SetsManager.instance.IDUser.ToString() + ".mp4";//+"_"+System.DateTime.Now.ToString ("dd-MM-yyyy-HH-mm-ss")+".mp4";
            form.AddField("framerate", 10);
            form.AddField("outputname", outputname);
            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/endrecord/", form);
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                MorphoDebug.Log("OK : " + www.downloadHandler.text);

			openWindow(www.downloadHandler.text);

            }
            www.Dispose();
#elif UNITY_STANDALONE

           
            WWWForm form = new WWWForm();
            form.AddField("action", "record_movie");
            form.AddField("time", MorphoTools.GetDataset().CurrentTime);
            form.AddField("objects", _MoviePath);

            UnityWebRequest www = UnityWebRequests.Post("localhost:" + StandaloneMainMenuInteractions.PYINSTALLER_PORT, "/ send.html", form);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error during movie making ! : " + www.error,1);
            else
            {
                MorphoDebug.Log("Your movie has beed saved at " + _MoviePath, 1);

            }
            www.Dispose();


            //

#endif
            loadingtext.GetComponent<Text>().text = "";
            hideThings(true, true);
            yield return null;
        }

        public IEnumerator finishRecordHologram()
        {
            loadingtext.SetActive(true);
            loadingtext.GetComponent<Text>().text = "Wait a litte for your movie...";
            //MorphoDebug.Log ("Save record Hologram");
            record = false;
            recordAction = -1;
            actionDone = false;
            WWWForm form = new WWWForm();
            string outputname = "Movie_" + SetsManager.instance.IDUser.ToString() + ".mp4";//+"_"+System.DateTime.Now.ToString ("dd-MM-yyyy-HH-mm-ss")+".mp4";
            form.AddField("movie", 5);
            form.AddField("id_user", SetsManager.instance.IDUser.ToString());
            form.AddField("framerate", 10);
            form.AddField("outputname", outputname);//embryon3d.crbm.cnrs.fr/dev/movie.php?movie=5&id_user=1&outputname=Movie_1_04-08-2017-11-27-48.mp4
            UnityWebRequest www = UnityWebRequests.Post(SetsManager.instance.urlMovie, "", form);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
#if !UNITY_EDITOR
			openWindow(MorphoTools.GetServer()+"Movies/"+outputname);
#endif
            }
            www.Dispose();
            loadingtext.GetComponent<Text>().text = "";
            hideThings(true, true);
        }

        /// ///////////////////SCENARIO
        private bool ScenarioPlay = false;

        //SHARE A SCENARIO (IF OWNER)
        public void onShare()
        {
            if (id_scenario_active >= 0 && id_scenario_owner == SetsManager.instance.IDUser)
            {
                if (MenuShare.activeSelf)
                    MenuShare.SetActive(false);
                else
                {
                    MenuShare.SetActive(true);
                    MenuShare.transform.position = new Vector3(Mathf.RoundToInt(-InterfaceManager.instance.canvasWidth / 2f + 273) * InterfaceManager.instance.canvasScale, MenuShare.transform.position.y, MenuShare.transform.position.z);
                }
            }
        }

        //SAVE A SCENARIO
        public void onSaveScenario()
        { StartCoroutine(saveScenario()); }

        public IEnumerator saveScenario()
        {
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            //int scenario_type = 3;
            if (id_scenario_active == -2)
            {
                id_scenario_owner = SetsManager.instance.IDUser;
                //scenario_type = 3;
                form.AddField("scenario", "3");
            }
            else
            {
                form.AddField("scenario", "4");
                //scenario_type = 4;
                form.AddField("id_scenario", id_scenario_active);
            }
            string namescenario = MenuActions.transform.Find("NameScenario").GetComponent<InputField>().text;
            if (namescenario == "")
                namescenario = "scenario";
            form.AddField("name", namescenario);
            form.AddField("id_dataset", AttachedDataset.id_dataset.ToString());
            MorphoDebug.Log(AttachedDataset.id_dataset.ToString());
            form.AddField("date", System.DateTime.Now.ToString("yyyy-mm-dd hh:mm:ss"));
            //Convert All actions in String
            string ActionStr = "";
            foreach (KeyValuePair<int, Action> actItem in Actions)
            {
                Action act = actItem.Value;
                ActionStr += actItem.Key + ":" + act.time + ":" + act.position + ":" + act.rotation + ":" + act.scale + ":" + act.framerate + ":" + act.interpolate + "\n";
            }
            byte[] ActionBy = Encoding.UTF8.GetBytes(ActionStr);
            form.AddBinaryData("actions", ActionBy);
            //MorphoDebug.Log (Instantiation.urlScenario + "?hash=" + Instantiation.hash + "&scenario=3&name="+MenuActions.transform.Find("NameScenario").GetComponent<InputField>().text+"&id_people=" + Instantiation.IDUser.ToString ()+"&id_dataset=" + Instantiation.id_dataset.ToString ());
            //currenttextloaded.text = Instantiation.urlScenario + "?hash=" + Instantiation.hash + "&scenario=3&name=" + MenuActions.transform.Find ("NameScenario").GetComponent<InputField> ().text + "&id_people=" + Instantiation.IDUser.ToString () + "&id_dataset=" + Instantiation.id_dataset.ToString ();
            //MorphoDebug.Log (ActionStr);

            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/savescenario/", form);
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            www.Dispose();
            StartCoroutine(listActions());
            backToScenario();
            CheckScenarios();
        }

        //LIST ALL AVAILBLAE SCENARIOS
        //Just remove everything from the menu of recall
        public void clearAllScenarios()
        {
            foreach (KeyValuePair<int, GameObject> item in Scenarios)
                Destroy(item.Value);
            Scenarios = new Dictionary<int, GameObject>();
        }

        //Check the uploaded data in the DB
        public IEnumerator checkScenarios()
        {
            //MorphoDebug.Log("Check scenarios");
            yield return new WaitUntil(() => AttachedDataset != null);
            //MorphoDebug.Log("Check scenarios 2");
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("scenario", "1");
            form.AddField("id_dataset", AttachedDataset.id_dataset.ToString());
            form.AddField("id_people", SetsManager.instance.IDUser.ToString());
            //MorphoDebug.Log (Instantiation.urlScenario + "?hash=" + Instantiation.hash + "&scenario=1&id_people=" + Instantiation.IDUser.ToString ()+"&id_dataset=" + Instantiation.id_dataset.ToString ());
            //currenttextloaded.text = Instantiation.urlScenario + "?hash=" + Instantiation.hash + "&scenario=1&id_people=" + Instantiation.IDUser.ToString () + "&id_dataset=" + Instantiation.id_dataset.ToString ();
            //MorphoDebug.Log("Check scenarios 3");
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/userscenarios/?id_dataset=" + AttachedDataset.id_dataset.ToString());
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            //MorphoDebug.Log("Check scenarios 4");
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error scenarios : " + www.error);
            else
            {
                //Return id,filename,date,type,nbCell,minV,maxV
                var N = JSONNode.Parse(www.downloadHandler.text);
                //MorphoDebug.Log("scenarios : " + www.downloadHandler.text);
                if (N != null && N.Count > 0)
                {
                    //currenttextloaded.text = "Number of scenarios  " + N.Count;
                    //MorphoDebug.Log("Number of scenarios  " + N.Count);
                    clearAllScenarios();
                    for (int i = 0; i < N.Count; i++)
                    { //Number of scenario : id,name,date
                      //	MorphoDebug.Log("scenarios : " + i + " " + N[i].ToString());
                        int id_scenario = int.Parse(N[i]["id"].ToString().Replace('"', ' ').Trim());
                        string name = N[i]["name"].ToString().Replace('"', ' ').Trim();
                        //string date = N [i] [2].ToString ().Replace('"',' ').Trim();
                        int id_owner = int.Parse(N[i]["id_people"].ToString().Replace('"', ' ').Trim());
                        GameObject udui = (GameObject)Instantiate(defaultScenario);
                        udui.transform.SetParent(MenuScenario.transform, false);
                        udui.transform.Translate(0, -Scenarios.Count * 30f * InterfaceManager.instance.canvasScale, 0, Space.World);
                        udui.SetActive(true);
                        udui.transform.Find("name").gameObject.GetComponent<Text>().text = name;
                        udui.transform.Find("id_owner").gameObject.GetComponent<Text>().text = id_owner.ToString();
                        showScenarioListen(udui.transform.Find("show").gameObject.GetComponent<Button>().GetComponent<Button>(), id_scenario);
                        Scenarios[id_scenario] = udui;
                    }
                }
            }
            www.Dispose();
        }

        public void initActionMenus()
        {
            MenuScenario.SetActive(false);
            if (Actions != null)
                foreach (KeyValuePair<int, Action> act in Actions)
                    Destroy(act.Value.go);
            scrollActionGO.SetActive(false);
            Actions = new Dictionary<int, Action>();
            MenuActions.SetActive(true);
            defaultAction.SetActive(false);
        }

        public void showScenarioListen(Button b, int id_scenario)
        { b.onClick.AddListener(() => showScenario(id_scenario)); }

        public void showScenario(int id_scenario)
        {
            id_scenario_active = id_scenario;
            initActionMenus();
            //MorphoDebug.Log ("Show Scenario "+id_scenario_active);
            string name = Scenarios[id_scenario_active].transform.Find("name").gameObject.GetComponent<Text>().text;
            id_scenario_owner = int.Parse(Scenarios[id_scenario_active].transform.Find("id_owner").gameObject.GetComponent<Text>().text);
            MenuActions.transform.Find("NameScenario").gameObject.GetComponent<InputField>().text = name;
            if (id_scenario_owner != SetsManager.instance.IDUser)
            {
                //MenuActions.transform.Find ("share").gameObject.SetActive (false);
#if UNITY_STANDALONE
                MenuActions.transform.Find("savelocal").gameObject.SetActive(false);
#else
                MenuActions.transform.Find("save").gameObject.SetActive(false);
#endif
                MenuActions.transform.Find("cancel").gameObject.SetActive(false);
            }
            else if (SetsManager.instance.IDUser > 0)
            {
                //aMenuShare.transform.position =new Vector3 (Mathf.RoundToInt(-UIManager.instance.canvasWidth/2f+273)*UIManager.instance.canvasScale,MenuShare.transform.position.y, MenuShare.transform.position.z);
                //MenuActions.transform.Find ("share").gameObject.SetActive (true);
#if UNITY_STANDALONE
                MenuActions.transform.Find("savelocal").gameObject.SetActive(true);
#else
                MenuActions.transform.Find("save").gameObject.SetActive(true);
#endif
                MenuActions.transform.Find("cancel").gameObject.SetActive(true);
            }
            StartCoroutine(listActions());
        }

        //DELETE A SCENARIO
        public void onDeleteScenario()
        { StartCoroutine(deleteScenario()); }

        public IEnumerator deleteScenario()
        {
            if (id_scenario_active != -2)
            { //Otherwise it has never been save so nothing to delete...
                WWWForm form = new WWWForm();
                form.AddField("id_scenario", id_scenario_active.ToString());
                //MorphoDebug.Log (Instantiation.urlScenario + "?hash=" + Instantiation.hash + "&scenario=2&id_scenario=" +id_scenario.ToString ());
                UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/deleteScenario/", form);
                if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
                {
                    www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
                }

                yield return www.SendWebRequest();
                if (www.isHttpError || www.isNetworkError)
                    MorphoDebug.Log("Error : " + www.error);
                else
                {
                    if (www.downloadHandler.text != "")
                        MorphoDebug.Log("Error : " + www.downloadHandler.text);
                }
                www.Dispose();
                //CheckScenarios ();
            }
            CheckScenarios();
            backToScenario();
        }

        //LIST ALL ACTIONS FOR A SPECIFIC SCENARIO
        public IEnumerator listActions()
        {
            //MorphoDebug.Log ("Play " + id_scenario_active);
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/actions/?id_scenario=" + id_scenario_active.ToString());
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                //Return id,filename,date,type,nbCell,minV,maxV
                var N = JSONNode.Parse(www.downloadHandler.text);
                if (N != null && N.Count > 0)
                {
                    Actions = new Dictionary<int, Action>();
                    //MorphoDebug.Log("Number of actions " + N.Count);

                    CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    ci.NumberFormat.CurrencyDecimalSeparator = ".";
                    //MorphoDebug.Log("actions : " + N.ToString());
                    for (int i = 0; i < N.Count; i++)
                    { //Number of scenario : idx,time,position,rotation,scale,framerate,interpolate
                      //int idx=int.Parse(N [i] [0].ToString ().Replace('"',' ').Trim());
                        int time = int.Parse(N[i]["time"].ToString().Replace('"', ' ').Trim());
                        string position = N[i]["position"].ToString().Replace('"', ' ').Trim();
                        string rotation = N[i]["rotation"].ToString().Replace('"', ' ').Trim();
                        float scale = float.Parse(N[i]["scale"].ToString().Replace('"', ' ').Trim(), NumberStyles.Any, ci);
                        int fps = int.Parse(N[i]["framerate"].ToString().Replace('"', ' ').Trim());
                        int inter = int.Parse(N[i]["interpolate"].ToString().Replace('"', ' ').Trim());

                        GameObject udui = (GameObject)Instantiate(defaultAction);
                        udui.transform.SetParent(MenuActions.transform, false);
                        udui.transform.position = new Vector3(udui.transform.position.x, defaultAction.transform.position.y - Actions.Count * 30 * InterfaceManager.instance.canvasScale, udui.transform.position.z);
                        Action act = new Action(udui, time, scale, parseVector3(position), parseQuaterion(rotation), inter == 1, fps);
                        delActionListen(udui.transform.Find("cancel").gameObject.GetComponent<Button>(), Actions.Count);
                        gotoActionListen(udui.transform.Find("goto").gameObject.GetComponent<Button>(), Actions.Count);

                        Actions[Actions.Count] = act;
                        if (Actions.Count > 20)
                            udui.SetActive(false);
                    }
                }
                checkScrollBar();
            }
            www.Dispose();
        }

        //Check if the scroll bar is necessay
        public void checkScrollBar()
        {
            if (Actions.Count > 20)
            {//Start To scroll ...
                float heightAction = 31.5f;  //heht=630 pour 20 Actions
                scrollActionGO.SetActive(true);
                scrollAction.size = 630f / (heightAction * Actions.Count); //Size of the scroll bar..
            }
            else
                scrollActionGO.SetActive(false);
        }

        //On scrollbar mouvmeent
        public void scrollBarAction()
        {
            int startAction = (int)Mathf.Round(scrollAction.value * (Actions.Count - 20));
            int idx = 0;
            foreach (KeyValuePair<int, Action> act in Actions)
            {
                GameObject goA = act.Value.go;
                if (idx >= startAction && idx < startAction + 20)
                {
                    goA.SetActive(true);
                    goA.transform.position = new Vector3(goA.transform.position.x, defaultAction.transform.position.y - (idx - startAction) * 30 * InterfaceManager.instance.canvasScale, goA.transform.position.z);
                }
                else
                    goA.SetActive(false);
                idx++;
            }
        }

        //Weh change the scroll bare value to have this action at first...
        public void scrollTo(Action act)
        { }

        //When we create a new scenario
        public void createScenario()
        {
            initActionMenus();
            id_scenario_active = -2; //ID FOR NEW SCENARIO
                                     //id_scenario_owner=Instantiation.IDUser;
            MenuActions.transform.Find("share").gameObject.SetActive(false);//We can share it once it's save...
                                                                            //MenuActions.transform.Find ("save").gameObject.SetActive (false);
                                                                            //MenuActions.transform.Find ("cancel").gameObject.SetActive (false);
        }

        //Close the menu action
        public void backToScenario()
        {
            id_scenario_active = -1;
            MenuScenario.SetActive(true);
            MenuActions.SetActive(false);
        }

        public int stepFrame = 0;
        public Action currentAction;

        //When click on play Scenario
        public void onPlay()
        {
            if (Actions.Count > 0)
            {
                actionCurrent = 0;
                currentAction = Actions[actionCurrent];
                ScenarioPlay = true;
                actionDone = true;
                stepFrame = 0;
            }
        }

        public void playActions()
        {
            actionDone = false;
            //MorphoDebug.Log ("playActions "+actionCurrent+" / "+stepFrame);
            currentAction.active();
            //MorphoDebug.Log ("Perform " + currentAction.time+" ->"+currentAction.interpolate);
            if (currentAction.interpolate && actionCurrent < Actions.Count - 1)
            { //INTERPOLATION
                Action nextAct = Actions[actionCurrent + 1];
                float t = (float)stepFrame / (float)currentAction.framerate;
                AttachedDataset.TransformationsManager.rescale(Vector3.Lerp(new Vector3(currentAction.scale, currentAction.scale, currentAction.scale), new Vector3(nextAct.scale, nextAct.scale, nextAct.scale), t));
                AttachedDataset.TransformationsManager.SetRotation(Quaternion.Slerp(currentAction.rotation, nextAct.rotation, t));
                AttachedDataset.TransformationsManager.move(Vector3.Lerp(currentAction.position, nextAct.position, t));
                AttachedDataset.update = true;
                int newTime = (int)Mathf.Round(currentAction.time + t * (nextAct.time - currentAction.time));
                //MorphoDebug.Log ("playActions "+actionCurrent+" / "+stepFrame+ " t=="+t+ "->Interpolate Time " + newTime+" Rotation=="+Instantiation.CurrentRotation.ToString());
                if (newTime >= AttachedDataset.MinTime && newTime <= AttachedDataset.MaxTime)
                {
                    if (AttachedDataset.CurrentTime == newTime)
                        finishAction(); //We already are at the well time point...
                    else
                        InterfaceManager.instance.setTime(newTime);
                }
                else
                    finishAction();
            }
            else
            {
                AttachedDataset.TransformationsManager.rescale(currentAction.scale);
                AttachedDataset.TransformationsManager.SetRotation(currentAction.rotation);
                AttachedDataset.TransformationsManager.move(currentAction.position);
                if (currentAction.time >= AttachedDataset.MinTime && currentAction.time <= AttachedDataset.MaxTime)
                {
                    if (AttachedDataset.CurrentTime == currentAction.time)
                        finishAction(); //We already are at the well time point...
                    else
                        InterfaceManager.instance.setTime(currentAction.time);
                }
                else
                    finishAction();
            }
        }

        //End after aaction
        public void finishAction()
        {
            if (currentAction != null)
            {
                //MorphoDebug.Log ("End Action");
                if (stepFrame < currentAction.framerate)
                    stepFrame++;
                else
                {
                    currentAction.unActive();
                    stepFrame = 0;
                    actionCurrent++;
                }

                if (Actions.ContainsKey(actionCurrent))
                {
                    if (record)
                        recordAction = 0;
                    currentAction = Actions[actionCurrent];
                }
                else
                {
                    currentAction = null;
                    ScenarioPlay = false;
                }
            }
            actionDone = true;
        }

        public void Update()
        {
            if (actionDone)
            {
                if (ScenarioPlay)
                { //JUST PLAY THE SCENARIO
                    playActions();
                }
                if (record)
                { //RECORD
                  //MorphoDebug.Log ("Action" + actionCurrent + ":" + recordAction);
                    if (recordAction == 0)
                    { //Perform the recod
                        recordAction = 1;
                        //if (hologram.isOn) {
                        //if(nbHoloAngle==0) recordAction = 1;
                        //	StartCoroutine (frameRecordHologram ());
                        //}
                        //else {
                        StartCoroutine(frameRecord());
                        //}
                    }
                    if (recordAction == 2)
                    { //Record finish -> Next Action
                        if (currentAction == null)
                        {
                            StartCoroutine(finishRecord());
                        }
                        else
                        {
                            playActions();
                        }
                    }
                }
            }
            if (id_scenario_active == -2 || id_scenario_active >= 0)
                if (Input.GetKeyDown(KeyCode.A) && !MenuActions.transform.Find("NameScenario").GetComponent<InputField>().isFocused)
                {
                    addNewAction();
                }
        }

        private int defaultFramerate = 10;

        //When we click on 'a' to add a new actio,n
        public void addNewAction()
        {
            GameObject udui = (GameObject)Instantiate(defaultAction);
            udui.transform.SetParent(MenuActions.transform, false);
            udui.transform.position = new Vector3(udui.transform.position.x, defaultAction.transform.position.y - Actions.Count * 30 * InterfaceManager.instance.canvasScale, udui.transform.position.z);
            delActionListen(udui.transform.Find("cancel").gameObject.GetComponent<Button>(), Actions.Count);
            Action act = new Action(udui, AttachedDataset.CurrentTime, AttachedDataset.TransformationsManager.CurrentScale.x, AttachedDataset.TransformationsManager.CurrentPosition, AttachedDataset.TransformationsManager.CurrentRotation, true, defaultFramerate);
            Actions[Actions.Count] = act;
            checkScrollBar();
        }

        //Add the listener to delete the Action
        public void delActionListen(Button b, int id_action)
        { b.onClick.AddListener(() => delAction(id_action)); }

        public void delAction(int id_action)
        {
            Dictionary<int, Action> NewActions = new Dictionary<int, Action>();
            foreach (KeyValuePair<int, Action> idA in Actions)
            {
                if (idA.Key != id_action)
                {
                    Action act = idA.Value;
                    act.go.transform.position = new Vector3(act.go.transform.position.x, defaultAction.transform.position.y - NewActions.Count * 30 * InterfaceManager.instance.canvasScale, act.go.transform.position.z);
                    act.go.transform.Find("cancel").gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
                    delActionListen(act.go.transform.Find("cancel").gameObject.GetComponent<Button>(), NewActions.Count);
                    NewActions[NewActions.Count] = act;
                }
                else
                {
                    Action act = idA.Value;
                    Destroy(act.go);
                }
            }
            Actions = NewActions;
            checkScrollBar();
        }

        public void gotoActionListen(Button b, int id_action)
        { b.onClick.AddListener(() => gotoAction(id_action)); }

        public void gotoAction(int id_action)
        {
            //currenttextloaded.text = "GOTO " + id_action;
            //MorphoDebug.Log ("GOTO " + id_action);
            if (Actions.ContainsKey(id_action))
            {
                Action act = Actions[id_action];
                AttachedDataset.TransformationsManager.rescale(act.scale);
                AttachedDataset.TransformationsManager.SetRotation(act.rotation);
                AttachedDataset.TransformationsManager.move(act.position);
                if (act.time >= AttachedDataset.MinTime && act.time <= AttachedDataset.MaxTime)
                    if (AttachedDataset.CurrentTime != act.time)
                        InterfaceManager.instance.setTime(act.time);
            }
        }

        public Vector3 parseVector3(string rString)
        {
            CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            ci.NumberFormat.CurrencyDecimalSeparator = ".";
            string[] temp = rString.Substring(1, rString.Length - 2).Split(',');
            float x = float.Parse(temp[0], NumberStyles.Any, ci);
            float y = float.Parse(temp[1], NumberStyles.Any, ci);
            float z = float.Parse(temp[2], NumberStyles.Any, ci);
            Vector3 rValue = new Vector3(x, y, z);
            return rValue;
        }

        public Quaternion parseQuaterion(string rString)
        {
            CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            ci.NumberFormat.CurrencyDecimalSeparator = ".";
            string[] temp = rString.Substring(1, rString.Length - 2).Split(',');
            float x = float.Parse(temp[0], NumberStyles.Any, ci);
            float y = float.Parse(temp[1], NumberStyles.Any, ci);
            float z = float.Parse(temp[2], NumberStyles.Any, ci);
            float w = float.Parse(temp[3], NumberStyles.Any, ci);
            Quaternion rValue = new Quaternion(x, y, z, w);
            return rValue;
        }

#region filebrowser_standalone

        public void SetFileBrowserPath(byte[] img)
        {
            FileBrowser.SetFilters(true, new FileBrowser.Filter("Image", ".png"));
            FileBrowser.SetDefaultFilter(".png");
            StartCoroutine(WaitForDirectoryPicker(img));

        }

        public IEnumerator WaitForDirectoryPicker(byte[] img)
        {
            yield return FileBrowser.WaitForSaveDialog(FileBrowser.PickMode.Files, false, null, "screenshot.png", "Save image", "Save");
            
            if (FileBrowser.Success)
            {

                if (!File.Exists(FileBrowser.Result[0]))
                {
                    Debug.Log("WRITTEN IMAGE");
                    File.WriteAllBytes(FileBrowser.Result[0], img);
                }
                else
                {
                    MorphoDebug.LogError("ERROR: screen capture export file already exists");
                }


            }
        }

        public IEnumerator WaitForMoviePathPicker()
        {
            yield return FileBrowser.WaitForLoadDialog(FileBrowser.PickMode.Folders, false, null, null, "Save movie in directory", "Save");

            if (FileBrowser.Success)
            {

                if (Directory.Exists(FileBrowser.Result[0]))
                {
                    if (Actions.Count > 0)
                    {
                        _MoviePath = FileBrowser.Result[0];
                        hideThings(false, true);
                        actionCurrent = 0;
                        currentAction = Actions[actionCurrent];
                        ScenarioPlay = false;
                        actionDone = true;
                        stepFrame = 0;
                        record = true;
                        recordAction = 2;
                        frameNB = 0;
                    }
                }
                else
                {
                    MorphoDebug.LogError("ERROR: export directory does not exist");
                }


            }
        }

#endregion


    }
}