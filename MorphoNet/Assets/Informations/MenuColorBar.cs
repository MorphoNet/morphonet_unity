﻿using System;
using AssemblyCSharp;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using MorphoNet.Extensions.Unity;

namespace MorphoNet
{
    public class MenuColorBar : MonoBehaviour
    {
        public GameObject MenuBarColor;
        public GameObject SubMenuColor;
        public GameObject SubMenuOther;
        public ToggleGroup ParametersChoice;
        public Toggle ThresholdToggle;
        public Dropdown MapColor;
        public GameObject Colorbar;
        public Slider SliderMin;
        public Slider SliderMax;

        public static MenuColorBar instance = null;

        public int MapColorV = 0;
        private int MinC;
        private int MaxC;

        //For the Real Value
        private InputField MinVText;

        private InputField MaxVText;
        public float MinV;
        public float MaxV;
        public Slider SliderMinV;
        public Slider SliderMaxV;
        public float MinVal;
        public float MaxVal;

        //For other
        private InputField MinVTextOther;

        private InputField MaxVTextOther;
        private float MinVOther;
        private float MaxVOther;
        public Slider SliderMinVOther;
        public Slider SliderMaxVOther;

        public  Text infosNameT;
        public  Text infosNameT2;
        public  string infosName;
        public  GameObject buttonToColorize;
        public  Toggle activeToggle;
        public bool isupdating = false;

        public GameObject ShaderMin;
        public GameObject ShaderMoy;
        public GameObject ShaderMax;

        private Material MatMin;
        private Material MatMoy;
        private Material MatMax;

        public static Button ApplyButton;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this.gameObject);
            }
        }

        // Use this for initialization
        private void Start()
        {
            init();
        }

        public float GetMinVal() => MinVal;

        public float GetMaxVal() => MaxVal;

        public float GetMinV() => MinV;

        public float GetMaxV() => MaxV;

        public float GetMinThresholdBounds() => SliderMinV.minValue;

        public float GetMinThresholdValue() => SliderMinV.value;

        public float GetMaxThresholdBounds() => SliderMaxV.maxValue;

        public float GetMaxThresholdValue() => SliderMaxV.value;

        public int GetMinC() => MinC;

        public int GetMaxC() => MaxC;

        public int GetMinCBounds() => (int)SliderMin.minValue;

        public int GetMaxCBounds() => (int)SliderMax.maxValue;

        public float GetMinVOther() => MinVOther;

        public float GetMaxVOther() => MaxVOther;

        public bool isDefaultOrColoredMaterial(Material mat)
        {
            return mat.name == "FresnellCell (Instance)" || mat.name == "FresnellCell" ||
                   mat.name == "ColorFresnell (Instance)" || mat.name == "ColorFresnell";
        }
        

        public void init()
        {
            if (MenuBarColor == null)
            {
                MenuBarColor = InterfaceManager.instance.MenuColorBar;
                SubMenuColor = MenuBarColor.transform.Find("SubMenuColor").gameObject;
                SubMenuOther = MenuBarColor.transform.Find("SubMenuOther").gameObject;
                ParametersChoice = MenuBarColor.transform.Find("ToggleGroup").gameObject.GetComponent<ToggleGroup>();

                ThresholdToggle = MenuBarColor.transform.Find("Threshold").gameObject.GetComponent<Toggle>();

                MapColor = MenuBarColor.transform.Find("SubMenuColor").Find("ColorMap").gameObject.GetComponent<Dropdown>();
                Colorbar = MenuBarColor.transform.Find("SubMenuColor").Find("Colorbar").gameObject;
                SliderMin = MenuBarColor.transform.Find("SubMenuColor").Find("SliderMin").gameObject.GetComponent<Slider>();
                SliderMax = MenuBarColor.transform.Find("SubMenuColor").Find("SliderMax").gameObject.GetComponent<Slider>();
                //FOR ReallValue
                SliderMinV = MenuBarColor.transform.Find("SliderMinV").gameObject.GetComponent<Slider>();
                SliderMaxV = MenuBarColor.transform.Find("SliderMaxV").gameObject.GetComponent<Slider>();
                MinVText = MenuBarColor.transform.Find("MinV").gameObject.GetComponent<InputField>();
                MaxVText = MenuBarColor.transform.Find("MaxV").gameObject.GetComponent<InputField>();
                infosNameT = MenuBarColor.transform.Find("InfosText").gameObject.GetComponent<Text>();
                infosNameT2 = MenuBarColor.transform.Find("Info").gameObject.GetComponent<Text>();

                //FOR ReallValue
                SliderMinVOther = MenuBarColor.transform.Find("SubMenuOther").Find("SliderMinV").gameObject.GetComponent<Slider>();
                SliderMaxVOther = MenuBarColor.transform.Find("SubMenuOther").Find("SliderMaxV").gameObject.GetComponent<Slider>();
                MinVTextOther = MenuBarColor.transform.Find("SubMenuOther").Find("MinV").gameObject.GetComponent<InputField>();
                MaxVTextOther = MenuBarColor.transform.Find("SubMenuOther").Find("MaxV").gameObject.GetComponent<InputField>();

                //FOR sphere shader render
                ShaderMin = MenuBarColor.transform.Find("Min").gameObject;
                ShaderMoy = MenuBarColor.transform.Find("Moy").gameObject;
                ShaderMax = MenuBarColor.transform.Find("Max").gameObject;
                    /*
                ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);

                ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", 0f);
                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", 0f);
                ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", 0f);

                ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_FresnelPower", 1.5f);
                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_FresnelPower", 1.5f);
                ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_FresnelPower", 1.5f);

                ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", 1f);
                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", 1f);
                ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", 1f);*/

                MatMin = ShaderMin.GetComponent<MeshRenderer>().material;
                MatMoy = ShaderMoy.GetComponent<MeshRenderer>().material;
                MatMax = ShaderMax.GetComponent<MeshRenderer>().material;

                ApplyButton = MenuBarColor.transform.Find("apply").GetComponent<Button>();
            }
            //if (MapColorV != 0) changeColorMap(MapColorV);
            updatSliders();
        }

        public void AssignValues(int type, int minC, int maxC, float minV, float maxV, string name, float min = 0, float max = 0, float minVOther = 0, float maxVOther = 1, string parameter = "Main Color")
        {
            MapColorV = type;
            MinC = minC;
            MaxC = maxC;
            MinV = minV;
            MaxV = maxV;
            MinVOther = minVOther;
            MaxVOther = maxVOther;
            MinVal = min;
            MaxVal = max;

            if (SliderMaxV.maxValue < MaxVal)
            {
                SliderMaxV.maxValue = MaxVal;
            }
            
            if (SliderMaxV.minValue > MinVal)
            {
                SliderMaxV.minValue = MinVal;
            }

            if (SliderMinV.maxValue < MaxVal)
            {
                SliderMinV.maxValue = MaxVal;
            }
            
            if (SliderMinV.minValue > MinVal)
            {
                SliderMinV.minValue = MinVal;
            }
            infosName = name;

            //also update min and max sliders for colorbar modulation
            SliderMin.value = minC;
            SliderMax.value = maxC;

            if (parameter == "")
            {
                parameter = "Main Color";
            }
            ParametersChoice.transform.Find(parameter).gameObject.GetComponent<Toggle>().isOn = true;
            activeToggle = ParametersChoice.transform.Find(parameter).gameObject.GetComponent<Toggle>();
            if (activeToggle.name == "Main Color" || activeToggle.name == "Halo Color" || activeToggle.name == "Second Color")
            {
                SubMenuColor.SetActive(true);
                SubMenuOther.SetActive(false);
            }
            else
            {
                SubMenuColor.SetActive(false);
                SubMenuOther.SetActive(true);
            }

            MatMin = ShaderMin.GetComponent<MeshRenderer>().material;
            MatMoy = ShaderMoy.GetComponent<MeshRenderer>().material;
            MatMax = ShaderMax.GetComponent<MeshRenderer>().material;

            updatSliders();

            //update colorbar selected for the property: get the name and find out the ID, then reapply with up-to-date-parameters
            string colormapname = buttonToColorize.transform.Find("Text").GetComponent<Text>().text;
            int colormapvalue = 0;
            for(int i=0;i<MapColor.options.Count();i++)
            {
                if (colormapname == MapColor.options[i].text)
                {
                    colormapvalue = i;
                }
                    
            }
            MapColor.value = colormapvalue;
            AssignSprite(colormapvalue,minC,maxC);
        }

        private float wait = 0;

        // TODO CHANGE THAT .... Update is called once per frame
        private void Update()
        {
            wait += Time.unscaledDeltaTime;
            if (MenuBarColor.activeSelf && wait > 1)
            {
                wait = 0;
            }
        }

        private static float minSlider = float.MinValue;
        private static float maxSlider = float.MaxValue;
        private static bool updateSliders = false;
        private static float minSliderOther = float.MinValue;
        private static float maxSliderOther = float.MaxValue;
        private static bool updateSlidersOther = false;

        public void updateMinFromInputField(string value)
        {
            float result;
            if (float.TryParse(value, out result))
            {
                SliderMinV.SetValueWithoutNotify(result);
                MinV = result;
            }
        }

        public void updateMaxFromInputField(string value)
        {
            float result;
            if (float.TryParse(value, out result))
            {
                SliderMaxV.SetValueWithoutNotify(result);
                MaxV = result;
                maxSlider = result;
            }
        }

        public void updateMinFromInputFieldOther(string value)
        {
            float result;
            if (float.TryParse(value, out result))
            {
                SliderMinVOther.SetValueWithoutNotify(result);
                MinVOther = result;
                minSliderOther = result;
                updatSliders();
            }
        }

        public void updateMaxFromInputFieldOther(string value)
        {
            float result;
            if (float.TryParse(value, out result))
            {
                SliderMaxVOther.SetValueWithoutNotify(result);
                MaxVOther = result;
                maxSliderOther = result;
                updatSliders();
            }
        }

        public bool dontupdate = false;
        public void updatSliders()
        {
            UpdateMinV(MinV,true);
            SliderMinV.minValue = MinVal;
            SliderMinV.maxValue = MaxVal;
           
            
            UpdateMaxV(MaxV, true);
            SliderMaxV.minValue = MinVal;
            SliderMaxV.maxValue = MaxVal;
            

            if (SubMenuOther.activeSelf)
            {
                if (SliderMinVOther.value != MinVOther)
                { SliderMinVOther.minValue = 0; SliderMinVOther.CancelInvoke("updateMinFromSliderOther"); SliderMinVOther.maxValue = 1; SliderMinVOther.CancelInvoke("updateMaxFromSliderOther"); SliderMinVOther.SetValueWithoutNotify(MinVOther); }
                if (SliderMaxVOther.value != MaxVOther)
                { SliderMaxVOther.minValue = 0; SliderMaxVOther.CancelInvoke("updateMinFromSliderOther"); SliderMaxVOther.maxValue = 1; SliderMaxVOther.CancelInvoke("updateMaxFromSliderOther"); SliderMaxVOther.SetValueWithoutNotify(MaxVOther); }
            }


            if (SubMenuOther.activeSelf)
            {
                if (MinVTextOther != null && MinVTextOther.text != MinVOther.ToString())
                    MinVTextOther.SetTextWithoutNotify(MinVOther.ToString());
                if (MaxVTextOther != null && MaxVTextOther.text != MaxVOther.ToString())
                    MaxVTextOther.SetTextWithoutNotify(MaxVOther.ToString());
            }

            if (infosNameT.text != infosName)
                infosNameT.text = infosName;
            if (infosNameT2.text != infosName)
                infosNameT2.text = infosName;

            if (buttonToColorize != null)
            {
                if (isDefaultOrColoredMaterial(MatMin))
                {
                    ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                    ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                    ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMin.GetColor("_Color"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMoy.GetColor("_Color"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMax.GetColor("_Color"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", MatMin.GetColor("_FresnelColor"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", MatMoy.GetColor("_FresnelColor"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", MatMax.GetColor("_FresnelColor"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", MatMin.GetFloat("_Emission"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", MatMoy.GetFloat("_Emission"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", MatMax.GetFloat("_Emission"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", MatMin.GetFloat("_ActivateFrenel"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", MatMoy.GetFloat("_ActivateFrenel"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", MatMax.GetFloat("_ActivateFrenel"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", MatMin.GetFloat("_Metallic"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", MatMoy.GetFloat("_Metallic"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", MatMax.GetFloat("_Metallic"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", MatMin.GetFloat("_Smoothness"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", MatMoy.GetFloat("_Smoothness"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", MatMax.GetFloat("_Smoothness"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", MatMin.GetFloat("_Transparency"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", MatMoy.GetFloat("_Transparency"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", MatMax.GetFloat("_Transparency"));
                }
                else
                {
                    ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                    ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                    ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", MatMin.GetColor("_Color1"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", MatMoy.GetColor("_Color1"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", MatMax.GetColor("_Color1"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", MatMin.GetColor("_Color2"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", MatMoy.GetColor("_Color2"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", MatMax.GetColor("_Color2"));
                }

                Sprite def;
                Texture2D texty;
                int min, max;

                def = MapColor.options[int.Parse(buttonToColorize.transform.Find("type").gameObject.GetComponentInChildren<Text>().text, System.Globalization.NumberStyles.Any)].image;

                min = int.Parse(buttonToColorize.transform.Find("minVOther").gameObject.GetComponentInChildren<Text>().text, System.Globalization.NumberStyles.Any);
                max = int.Parse(buttonToColorize.transform.Find("maxVOther").gameObject.GetComponentInChildren<Text>().text, System.Globalization.NumberStyles.Any);

                if (MapColor.options[MapColor.value].image == null && activeToggle.name == "Main Color")
                {
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                }

                if (buttonToColorize.transform.Find("parameter").gameObject.GetComponentInChildren<Text>().text != activeToggle.name)
                {
                    switch (buttonToColorize.transform.Find("parameter").gameObject.GetComponentInChildren<Text>().text)
                    {
                        case "Main Color":
                            if (isDefaultOrColoredMaterial(MatMax))
                            {
                                
                                if (def != null)
                                {
                                    texty = def.texture;
                                    if (MatMax.GetColor("_Color") == texty.GetPixel(MaxC, 0))
                                    {
                                        ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                                        ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                                        ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                                    }
                                }
                                else
                                {
                                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                                }
                            }
                            break;

                        case "Halo Color":
                            if (isDefaultOrColoredMaterial(MatMax))
                            {
                                if (def != null)
                                {
                                    texty = def.texture;
                                    if (MatMax.GetColor("_FresnelColor") == texty.GetPixel(MaxC, 0))
                                    {
                                        if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_Emission"))
                                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", 0f);
                                        if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_Emission"))
                                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", 0f);
                                        if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_Emission"))
                                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", 0f);
                                    }
                                }
                            }
                            break;

                        case "Second Color":
                            if (!isDefaultOrColoredMaterial(MatMax))
                            {
                                if (def != null)
                                {
                                    texty = def.texture;
                                    if (MatMax.GetColor("_Color2") == texty.GetPixel(MaxC, 0))
                                    {
                                        ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                                        ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                                        ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                                        ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMin.GetColor("_Color1"));
                                        ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMoy.GetColor("_Color1"));
                                        ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMax.GetColor("_Color1"));
                                    }
                                }
                            }
                            break;

                        case "Metallic":
                            if (isDefaultOrColoredMaterial(MatMax))
                            {
                                if (MatMax.GetFloat("_Metallic") == max && MatMin.GetFloat("_Metallic") == min)
                                {
                                    if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_Metallic"))
                                        ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", InterfaceManager.instance.defaultShaderMetallic);
                                    if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_Metallic"))
                                        ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", InterfaceManager.instance.defaultShaderMetallic);
                                    if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_Metallic"))
                                        ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", InterfaceManager.instance.defaultShaderMetallic);
                                }
                            }
                            break;

                        case "Smoothness":
                            if (isDefaultOrColoredMaterial(MatMax))
                            {
                                if (MatMax.GetFloat("_Smoothness") == max && MatMin.GetFloat("_Smoothness") == min)
                                {
                                    if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_Smoothness"))
                                        ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", InterfaceManager.instance.defaultShaderSmoothness);
                                    if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_Smoothness"))
                                        ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", InterfaceManager.instance.defaultShaderSmoothness);
                                    if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_Smoothness"))
                                        ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", InterfaceManager.instance.defaultShaderSmoothness);
                                }
                            }
                            break;

                        case "Transparency":
                            if (isDefaultOrColoredMaterial(MatMax))
                            {
                                if (MatMax.GetFloat("_Transparency") == max && MatMin.GetFloat("_Transparency") == min)
                                {
                                    if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_Smoothness"))
                                        ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", InterfaceManager.instance.defaultShaderSmoothness);
                                    if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_Smoothness"))
                                        ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", InterfaceManager.instance.defaultShaderSmoothness);
                                    if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_Smoothness"))
                                        ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", InterfaceManager.instance.defaultShaderSmoothness);
                                }
                            }
                            break;

                        default:
                            break;
                    }
                }

                switch (activeToggle.name)
                {
                    case "Main Color":
                        if (!isDefaultOrColoredMaterial(MatMax))
                        {
                            ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMin.GetColor("_Color1"));
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMoy.GetColor("_Color1"));
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMax.GetColor("_Color1"));
                        }
                        def = MapColor.options[MapColor.value].image;
                        if (def != null)
                        {
                            texty = def.texture;
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", texty.GetPixel(MinC, 0));
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", texty.GetPixel((int)(MaxC - MinC) / 2 + MinC, 0));
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", texty.GetPixel(MaxC, 0));
                        }
                        break;

                    case "Halo Color":
                        if (!isDefaultOrColoredMaterial(MatMax))
                        {
                            Color col1, col2, col3;
                            if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_Color1"))
                                col1 = ShaderMin.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            else
                                col1 = ShaderMin.GetComponent<MeshRenderer>().material.GetColor("_Color");
                            if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_Color1"))
                                col2 = ShaderMoy.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            else
                                col2 = ShaderMoy.GetComponent<MeshRenderer>().material.GetColor("_Color");
                            if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_Color1"))
                                col3 = ShaderMax.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            else
                                col3 = ShaderMax.GetComponent<MeshRenderer>().material.GetColor("_Color");
                            ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col1);
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col2);
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col3);
                        }
                        def = MapColor.options[MapColor.value].image;
                        if (def != null)
                        {
                            texty = def.texture;
                            if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_Emission"))
                                ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", 1f);
                            if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_Emission"))
                                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", 1f);
                            if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_Emission"))
                                ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", 1f);

                            if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_ActivateFrenel"))
                                ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", 1f);
                            if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_ActivateFrenel"))
                                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", 1f);
                            if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_ActivateFrenel"))
                                ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", 1f);

                            if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_FresnelColor"))
                                ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", texty.GetPixel(MinC, 0));
                            if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_FresnelColor"))
                                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", texty.GetPixel((int)(MaxC - MinC) / 2 + MinC, 0));
                            if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_FresnelColor"))
                                ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", texty.GetPixel(MaxC, 0));
                        }
                        break;

                    case "Second Color":
                        def = MapColor.options[MapColor.value].image;
                        if (def != null)
                        {
                            texty = def.texture;
                            Color col1, col2, col3;
                            if (!isDefaultOrColoredMaterial(MatMax)) 
                            {
                                if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_Color1"))
                                    col1 = ShaderMin.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                                else
                                    col1 = ShaderMin.GetComponent<MeshRenderer>().material.GetColor("_Color");
                                if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_Color1"))
                                    col2 = ShaderMoy.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                                else
                                    col2 = ShaderMoy.GetComponent<MeshRenderer>().material.GetColor("_Color");
                                if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_Color1"))
                                    col3 = ShaderMax.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                                else
                                    col3 = ShaderMax.GetComponent<MeshRenderer>().material.GetColor("_Color");
                                ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                                ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                                ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                                ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", col1);
                                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", col2);
                                ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", col3);
                            }
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", texty.GetPixel(MinC, 0));
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", texty.GetPixel((int)(MaxC - MinC) / 2 + MinC, 0));
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", texty.GetPixel(MaxC, 0));
                        }
                        break;

                    case "Metallic":
                        if (!isDefaultOrColoredMaterial(MatMax))
                        {
                            Color col1, col2, col3;
                            if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_Color1"))
                                col1 = ShaderMin.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            else
                                col1 = ShaderMin.GetComponent<MeshRenderer>().material.GetColor("_Color");
                            if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_Color1"))
                                col2 = ShaderMoy.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            else
                                col2 = ShaderMoy.GetComponent<MeshRenderer>().material.GetColor("_Color");
                            if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_Color1"))
                                col3 = ShaderMax.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            else
                                col3 = ShaderMax.GetComponent<MeshRenderer>().material.GetColor("_Color");
                            ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col1);
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col2);
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col3);
                        }
                        ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", MinVOther);
                        ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", (float)(MaxVOther - MinVOther) / 2.0f + MinVOther);
                        ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", MaxVOther);
                        break;

                    case "Smoothness":
                        if (!isDefaultOrColoredMaterial(MatMax))
                        {
                            Color col1, col2, col3;
                            if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_Color1"))
                                col1 = ShaderMin.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            else
                                col1 = ShaderMin.GetComponent<MeshRenderer>().material.GetColor("_Color");
                            if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_Color1"))
                                col2 = ShaderMoy.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            else
                                col2 = ShaderMoy.GetComponent<MeshRenderer>().material.GetColor("_Color");
                            if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_Color1"))
                                col3 = ShaderMax.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            else
                                col3 = ShaderMax.GetComponent<MeshRenderer>().material.GetColor("_Color");
                            ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col1);
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col2);
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col3);
                        }
                        if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_Smoothness"))
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", MinVOther);
                        if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_Smoothness"))
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", (float)(MaxVOther - MinVOther) / 2.0f + MinVOther);
                        if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_Smoothness"))
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", MaxVOther);
                        break;

                    case "Transparency":
                        if (!isDefaultOrColoredMaterial(MatMax))
                        {
                            Color col1, col2, col3;
                            if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_Color1"))
                                col1 = ShaderMin.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            else
                                col1 = ShaderMin.GetComponent<MeshRenderer>().material.GetColor("_Color");
                            if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_Color1"))
                                col2 = ShaderMoy.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            else
                                col2 = ShaderMoy.GetComponent<MeshRenderer>().material.GetColor("_Color");
                            if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_Color1"))
                                col3 = ShaderMax.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            else
                                col3 = ShaderMax.GetComponent<MeshRenderer>().material.GetColor("_Color");
                            ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col1);
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col2);
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col3);
                        }
                        if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_Transparency"))
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", MinVOther);
                        if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_Transparency"))
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", (float)(MaxVOther - MinVOther) / 2.0f + MinVOther);
                        if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_Transparency"))
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", MaxVOther);
                        break;

                    default:
                        break;
                }
            }
        }

        public void UpdateRenderSphere()
        {
            if (SubMenuColor.activeSelf)
            {
                if (MapColor != null && MapColor.value != MapColorV)
                    MapColor.value = MapColorV;
                if (SliderMin != null && SliderMin.value != MinC)
                    SliderMin.value = MinC;
                if (SliderMax != null && SliderMax.value != MaxC)
                    SliderMax.value = MaxC;
            }

            if (buttonToColorize != null)
            {
                if (isDefaultOrColoredMaterial(MatMax))
                {
                    ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                    ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                    ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMin.GetColor("_Color"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMoy.GetColor("_Color"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMax.GetColor("_Color"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", MatMin.GetColor("_FresnelColor"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", MatMoy.GetColor("_FresnelColor"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", MatMax.GetColor("_FresnelColor"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", MatMin.GetFloat("_Emission"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", MatMoy.GetFloat("_Emission"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", MatMax.GetFloat("_Emission"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", MatMin.GetFloat("_ActivateFrenel"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", MatMoy.GetFloat("_ActivateFrenel"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", MatMax.GetFloat("_ActivateFrenel"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", MatMin.GetFloat("_Metallic"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", MatMoy.GetFloat("_Metallic"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", MatMax.GetFloat("_Metallic"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", MatMin.GetFloat("_Smoothness"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", MatMoy.GetFloat("_Smoothness"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", MatMax.GetFloat("_Smoothness"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", MatMin.GetFloat("_Transparency"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", MatMoy.GetFloat("_Transparency"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", MatMax.GetFloat("_Transparency"));
                }
                else
                {
                    ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                    ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                    ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", MatMin.GetColor("_Color1"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", MatMoy.GetColor("_Color1"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", MatMax.GetColor("_Color1"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", MatMin.GetColor("_Color2"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", MatMoy.GetColor("_Color2"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", MatMax.GetColor("_Color2"));
                }

                Sprite def;
                Texture2D texty;
                int min, max;
                def = MapColor.options[int.Parse(buttonToColorize.transform.Find("type").gameObject.GetComponentInChildren<Text>().text, System.Globalization.NumberStyles.Any)].image;

                min = int.Parse(buttonToColorize.transform.Find("minVOther").gameObject.GetComponentInChildren<Text>().text, System.Globalization.NumberStyles.Any);
                max = int.Parse(buttonToColorize.transform.Find("maxVOther").gameObject.GetComponentInChildren<Text>().text, System.Globalization.NumberStyles.Any);

                if (MapColor.options[MapColor.value].image == null && activeToggle.name == "Main Color")
                {
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                }

                if (buttonToColorize.transform.Find("parameter").gameObject.GetComponentInChildren<Text>().text != activeToggle.name)
                {
                    switch (buttonToColorize.transform.Find("parameter").gameObject.GetComponentInChildren<Text>().text)
                    {
                        case "Main Color":
                            if (isDefaultOrColoredMaterial(MatMax))
                            {
                                
                                if (def != null)
                                {
                                    texty = def.texture;
                                    if (MatMax.GetColor("_Color") == texty.GetPixel(MaxC, 0))
                                    {
                                        ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                                        ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                                        ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                                    }
                                }
                                else
                                {
                                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                                }
                            }
                            break;

                        case "Halo Color":
                            if (isDefaultOrColoredMaterial(MatMax))
                            {
                                if (def != null)
                                {
                                    texty = def.texture;
                                    if (MatMax.GetColor("_FresnelColor") == texty.GetPixel(MaxC, 0))
                                    {
                                        ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", 0f);
                                        ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", 0f);
                                        ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", 0f);
                                    }
                                }
                            }
                            break;

                        case "Second Color":
                            if (!isDefaultOrColoredMaterial(MatMax))
                            {
                                if (def != null)
                                {
                                    texty = def.texture;
                                    if (MatMax.GetColor("_Color2") == texty.GetPixel(MaxC, 0))
                                    {
                                        ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                                        ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                                        ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                                        ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMin.GetColor("_Color1"));
                                        ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMoy.GetColor("_Color1"));
                                        ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMax.GetColor("_Color1"));
                                    }
                                }
                            }
                            break;

                        case "Metallic":
                            if (isDefaultOrColoredMaterial(MatMax))
                            {
                                if (MatMax.GetFloat("_Metallic") == max && MatMin.GetFloat("_Metallic") == min)
                                {
                                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", InterfaceManager.instance.defaultShaderMetallic);
                                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", InterfaceManager.instance.defaultShaderMetallic);
                                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", InterfaceManager.instance.defaultShaderMetallic);
                                }
                            }
                            break;

                        case "Smoothness":
                            if (isDefaultOrColoredMaterial(MatMax))
                            {
                                if (MatMax.GetFloat("_Smoothness") == max && MatMin.GetFloat("_Smoothness") == min)
                                {
                                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", InterfaceManager.instance.defaultShaderSmoothness);
                                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", InterfaceManager.instance.defaultShaderSmoothness);
                                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", InterfaceManager.instance.defaultShaderSmoothness);
                                }
                            }
                            break;

                        case "Transparency":
                            if (isDefaultOrColoredMaterial(MatMax))
                            {
                                if (MatMax.GetFloat("_Transparency") == max && MatMin.GetFloat("_Transparency") == min)
                                {
                                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", InterfaceManager.instance.defaultShaderSmoothness);
                                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", InterfaceManager.instance.defaultShaderSmoothness);
                                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", InterfaceManager.instance.defaultShaderSmoothness);
                                }
                            }
                            break;

                        default:
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", Color.white);
                            break;
                    }
                }

                switch (activeToggle.name)
                {
                    case "Main Color":
                        if (!isDefaultOrColoredMaterial(MatMax))
                        {
                            ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMin.GetColor("_Color1"));
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMoy.GetColor("_Color1"));
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMax.GetColor("_Color1"));
                        }
                        def = MapColor.options[MapColor.value].image;
                        if (def != null)
                        {
                            texty = def.texture;
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", texty.GetPixel(MinC, 0));
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", texty.GetPixel((int)(MaxC - MinC) / 2 + MinC, 0));
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", texty.GetPixel(MaxC, 0));
                        }
                        break;

                    case "Halo Color":
                        if (!isDefaultOrColoredMaterial(MatMax))
                        {
                            Color col1, col2, col3;
                            col1 = ShaderMin.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            col2 = ShaderMoy.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            col3 = ShaderMax.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col1);
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col2);
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col3);
                        }
                        def = MapColor.options[MapColor.value].image;
                        if (def != null)
                        {
                            texty = def.texture;
                            if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_Emission"))
                                ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", 1f);
                            if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_Emission"))
                                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", 1f);
                            if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_Emission"))
                                ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", 1f);

                            if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_ActivateFrenel"))
                                ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", 1f);
                            if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_ActivateFrenel"))
                                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", 1f);
                            if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_ActivateFrenel"))
                                ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", 1f);

                            if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_FresnelColor"))
                                ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", texty.GetPixel(MinC, 0));
                            if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_FresnelColor"))
                                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", texty.GetPixel((int)(MaxC - MinC) / 2 + MinC, 0));
                            if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_FresnelColor"))
                                ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", texty.GetPixel(MaxC, 0));
                        }
                        break;

                    case "Second Color":
                        def = MapColor.options[MapColor.value].image;
                        if (def != null)
                        {
                            texty = def.texture;
                            Color col1, col2, col3;
                            if (isDefaultOrColoredMaterial(MatMax))
                            {
                                col1 = ShaderMin.GetComponent<MeshRenderer>().material.GetColor("_Color");
                                col2 = ShaderMoy.GetComponent<MeshRenderer>().material.GetColor("_Color");
                                col3 = ShaderMax.GetComponent<MeshRenderer>().material.GetColor("_Color");
                                ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                                ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                                ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                                ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", col1);
                                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", col2);
                                ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", col3);
                            }
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", texty.GetPixel(MinC, 0));
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", texty.GetPixel((int)(MaxC - MinC) / 2 + MinC, 0));
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", texty.GetPixel(MaxC, 0));
                        }
                        break;

                    case "Metallic":
                        if (!isDefaultOrColoredMaterial(MatMax))
                        {
                            Color col1, col2, col3;
                            col1 = ShaderMin.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            col2 = ShaderMoy.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            col3 = ShaderMax.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col1);
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col2);
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col3);
                        }
                        if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_Metallic"))
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", MinVOther);
                        if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_Metallic"))
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", (float)(MaxVOther - MinVOther) / 2.0f + MinVOther);
                        if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_Metallic"))
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", MaxVOther);
                        break;

                    case "Smoothness":
                        if (!isDefaultOrColoredMaterial(MatMax))
                        {
                            Color col1, col2, col3;
                            col1 = ShaderMin.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            col2 = ShaderMoy.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            col3 = ShaderMax.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col1);
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col2);
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col3);
                        }
                        if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_Smoothness"))
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", MinVOther);
                        if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_Smoothness"))
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", (float)(MaxVOther - MinVOther) / 2.0f + MinVOther);
                        if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_Smoothness"))
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", MaxVOther);
                        break;

                    case "Transparency":
                        if (!isDefaultOrColoredMaterial(MatMax))
                        {
                            Color col1, col2, col3;
                            col1 = ShaderMin.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            col2 = ShaderMoy.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            col3 = ShaderMax.GetComponent<MeshRenderer>().material.GetColor("_Color1");
                            ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col1);
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col2);
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", col3);
                        }
                        if (ShaderMin.GetComponent<MeshRenderer>().material.HasProperty("_Transparency"))
                            ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", MinVOther);
                        if (ShaderMoy.GetComponent<MeshRenderer>().material.HasProperty("_Transparency"))
                            ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", (float)(MaxVOther - MinVOther) / 2.0f + MinVOther);
                        if (ShaderMax.GetComponent<MeshRenderer>().material.HasProperty("_Transparency"))
                            ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", MaxVOther);
                        break;

                    default:
                        break;
                }
            }
        }

        public Dictionary<int, Color> getTextureArray(int typecmap, int minC, int maxC, float minV, float maxV)
        {
            //MorphoDebug.Log ("getColor " + v + " minC=" + minC + " maxC=" + maxC + " minV=" + minV + " maxV=" + maxV);
            Dictionary<int, Color> result = new Dictionary<int, Color>();
            Texture2D texty = null;
            Sprite def = MapColor.options[typecmap].image;
            texty = def.texture;
            if (texty != null)
            {
                for (int i = 0; i < texty.width; i++)
                {
                    /*int x = minC + (int)Mathf.Round((maxC - minC) * (i- minV) / (maxV - minV));
                    if (x >= texty.width) x = texty.width - 1;
                    if (x < 0) x = 0;*/
                    result.Add(i, texty.GetPixel(i, 0));
                }
            }
            else
            {
                return null;
            }
            return result;
        }

        public Color GetColor(int typecmap, float v, int minC, int maxC, float minV, float maxV, bool threshold = false, bool return_nocolor = false)
        {
            if (threshold)
                if (v < minV || v > maxV)
                    if (!return_nocolor)
                        return new Color(1f, 1f, 1f);
                    else
                        return new Color(0f, 0f, 0f);
            Texture2D texty = null;
            Sprite def = MapColor.options[typecmap].image;
            texty = def.texture;
            if (texty != null)
            {
                int x = minC + (int)Mathf.Round((maxC - minC) * (v - minV) / (maxV - minV));
                if (x >= texty.width)
                    x = texty.width - 1;
                if (x < 0)
                    x = 0;

                return texty.GetPixel(x, 0);
            }
            return new Color(0f, 0f, 0f);
        }

        public void changeSlidersMin()
        {
            if (SliderMin.value >= SliderMax.value)
                SliderMin.value = SliderMax.value - 1;
            if (SliderMin.value < 0)
                SliderMin.value = 0;
            MinC = (int)Mathf.Round(SliderMin.value);
            changeColorMap(MapColorV);
        }

        public void changeSlidersMax()
        {
            if (SliderMax.value <= SliderMin.value)
                SliderMax.value = SliderMin.value + 1;
            if (SliderMax.value > 2047)
                SliderMax.value = 2047;
            MaxC = (int)Mathf.Round(SliderMax.value);
            changeColorMap(MapColorV);
        }

        public void updateMaxFromSlider(float value) => UpdateMaxV(value);

        public void UpdateMaxV(float value, bool updateSlider = false)
        {
            MaxVText.SetTextWithoutNotify(value.ToString());
            MaxV = value;

            if (updateSlider)
            {
                SliderMaxV.SetValueWithoutNotify(value);
            }
                
        }

        public void updateMinFromSlider(float value) => UpdateMinV(value);

        public void UpdateMinV(float value, bool updateSlider = false)
        {
            MinVText.SetTextWithoutNotify(value.ToString());
            MinV = value;

            if (updateSlider)
                SliderMinV.SetValueWithoutNotify(value);
        }

        public void UpdateColorMapRange(float min, float max)
        {
            if (min >= max)
                min = max - 1;
            if (min < 0)
                min = 0;

            if (max <= min)
                max = min + 1;
            if (max > 2047)
                max = 2047;

            SliderMin.SetValueWithoutNotify(min);
            SliderMax.SetValueWithoutNotify(max);

            MinC = (int)Mathf.Round(min);
            MaxC = (int)Mathf.Round(max);

            changeColorMap(MapColorV);
        }

        public void updateMaxFromSliderOther(float value)
        {
            MaxVTextOther.SetTextWithoutNotify(value.ToString());
            MaxVOther = value;
            maxSliderOther = value;
        }

        public void updateMinFromSliderOther(float value)
        {
            MinVTextOther.SetTextWithoutNotify(value.ToString());
            MinVOther = value;
            minSliderOther = value;
        }

        public void changeSlidersMinV()
        {
            if (SliderMinV.value > SliderMaxV.value)
                SliderMinV.value = SliderMaxV.value;
        }

        public void changeSlidersMaxV()
        {
            if (SliderMaxV.value < SliderMinV.value)
                SliderMaxV.value = SliderMinV.value;
        }

        public void changeSlidersMinVOther()
        {
            if (SliderMinVOther.value > SliderMaxVOther.value)
                SliderMinVOther.value = SliderMaxVOther.value;
        }

        public void changeSlidersMaxVOther()
        {
            if (SliderMaxVOther.value < SliderMinVOther.value)
                SliderMaxVOther.value = SliderMinVOther.value;
        }

        public void changeColorMap(int v)
        {
            MapColorV = v;
            Colorbar.GetComponent<Image>().sprite = AssignSprite(v);
            if (!InterfaceManager.instance.ColorMapScale)
                InterfaceManager.instance.ColorMapScale.sprite = AssignSprite(v);
            UpdateRenderSphere();
        }

        public Sprite AssignSprite(int v, float minValue = -1, float maxValue = -1)
        {
            Sprite def = MapColor.options[v].image;
            if (def == null)
                return null;

            if (minValue == -1 || maxValue == -1)
                return ExtractSprite(def);
            else
                return ExtractSprite(def, minValue, maxValue);
        }

        public Sprite ExtractSprite(Sprite sprite)
        {
            return ExtractSprite(sprite, SliderMin.value, SliderMax.value);
        }

        public static Sprite ExtractSprite(Sprite sprite, float minValue, float maxValue)
        {
            return Sprite.Create(sprite.texture, new Rect(minValue, 0, maxValue - minValue, 30), new Vector2(0.5f, 0.5f));
        }

        public void updateToggleOn(Toggle t)
        {
            if (t.isOn)
            {
                activeToggle = t;
                if (t.name == "Main Color" || t.name == "Halo Color" || t.name == "Second Color")
                {
                    SubMenuColor.SetActive(true);
                    SubMenuOther.SetActive(false);
                }
                else
                {
                    SubMenuColor.SetActive(false);
                    SubMenuOther.SetActive(true);
                }
                if (isDefaultOrColoredMaterial(MatMin))
                {
                    ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                    ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                    ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMin.GetColor("_Color"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMoy.GetColor("_Color"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMax.GetColor("_Color"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", MatMin.GetColor("_FresnelColor"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", MatMoy.GetColor("_FresnelColor"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", MatMax.GetColor("_FresnelColor"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", MatMin.GetFloat("_Emission"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", MatMoy.GetFloat("_Emission"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", MatMax.GetFloat("_Emission"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", MatMin.GetFloat("_ActivateFrenel"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", MatMoy.GetFloat("_ActivateFrenel"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", MatMax.GetFloat("_ActivateFrenel"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", MatMin.GetFloat("_Metallic"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", MatMoy.GetFloat("_Metallic"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", MatMax.GetFloat("_Metallic"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", MatMin.GetFloat("_Smoothness"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", MatMoy.GetFloat("_Smoothness"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", MatMax.GetFloat("_Smoothness"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", MatMin.GetFloat("_Transparency"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", MatMoy.GetFloat("_Transparency"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", MatMax.GetFloat("_Transparency"));
                }
                else
                {
                    ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                    ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                    ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", MatMin.GetColor("_Color1"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", MatMoy.GetColor("_Color1"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", MatMax.GetColor("_Color1"));
                    ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", MatMin.GetColor("_Color2"));
                    ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", MatMoy.GetColor("_Color2"));
                    ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", MatMax.GetColor("_Color2"));
                }
                updatSliders();
            }
        }

        public void cancel()
        {
            if (isDefaultOrColoredMaterial(MatMin))
            {
                ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.ColorDefault;
                ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMin.GetColor("_Color"));
                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMoy.GetColor("_Color"));
                ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color", MatMax.GetColor("_Color"));
                ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", MatMin.GetColor("_FresnelColor"));
                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", MatMoy.GetColor("_FresnelColor"));
                ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_FresnelColor", MatMax.GetColor("_FresnelColor"));
                ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", MatMin.GetFloat("_Emission"));
                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", MatMoy.GetFloat("_Emission"));
                ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Emission", MatMax.GetFloat("_Emission"));
                ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", MatMin.GetFloat("_ActivateFrenel"));
                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", MatMoy.GetFloat("_ActivateFrenel"));
                ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_ActivateFrenel", MatMax.GetFloat("_ActivateFrenel"));
                ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", MatMin.GetFloat("_Metallic"));
                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", MatMoy.GetFloat("_Metallic"));
                ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Metallic", MatMax.GetFloat("_Metallic"));
                ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", MatMin.GetFloat("_Smoothness"));
                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", MatMoy.GetFloat("_Smoothness"));
                ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Smoothness", MatMax.GetFloat("_Smoothness"));
                ShaderMin.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", MatMin.GetFloat("_Transparency"));
                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", MatMoy.GetFloat("_Transparency"));
                ShaderMax.GetComponent<MeshRenderer>().material.UpdateFloatProperty("_Transparency", MatMax.GetFloat("_Transparency"));
            }
            else
            {
                ShaderMin.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                ShaderMoy.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                ShaderMax.GetComponent<MeshRenderer>().material = SetsManager.instance.StainSelection[0];
                ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", MatMin.GetColor("_Color1"));
                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", MatMoy.GetColor("_Color1"));
                ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color1", MatMax.GetColor("_Color1"));
                ShaderMin.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", MatMin.GetColor("_Color2"));
                ShaderMoy.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", MatMoy.GetColor("_Color2"));
                ShaderMax.GetComponent<MeshRenderer>().material.UpdateColorProperty("_Color2", MatMax.GetColor("_Color2"));
            }

            MenuBarColor.SetActive(false);
        }

        public string UpdateColorPropertyMapName(int v)
        { return MapColor.options[v].text; }

        public int NameColorMapToInt(string s)
        {
            for (int i = 0; i < MapColor.options.Count; i++)
                if (s == MapColor.options[i].text)
                    return i;
            return 0;
        }

        public bool select_object_in_value_range;

        public void SetObjectInValueRange(bool val)
        {
            select_object_in_value_range = val;
        }
        
        public void apply()
        { applyS(MapColorV); }

        public void applyS(int colv)
        {

            List<Cell> list = new List<Cell>();

            foreach (Cell cell in MorphoTools.GetDataset().PickedManager.clickedCells)
            {
                list.Add(cell);
            }
            MorphoTools.GetDataset().PickedManager.clickedCells.Clear();

            if (SetsManager.instance.directPlot != null && SetsManager.instance.directPlot.type != "" && SetsManager.instance.directPlot.type == "lpy")
            {
                Correspondence co = DataSetInformations.instance.getCorrespondenceByName("simulation-color");
                co.hideColor(true);
            }
            foreach (Cell cell in list)
            {
                MorphoTools.GetDataset().PickedManager.clickedCells.Add(cell);
            }
            list.Clear();

            Image ColorMapScale;
            List<Text> ColorMapScalePercent;

            if (activeToggle.name == "Metallic" || activeToggle.name == "Smoothness" || activeToggle.name == "Transparency")
            {
                if (buttonToColorize.transform.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "1")
                    InterfaceManager.instance.ColorMapScale.gameObject.SetActive(false);
                else
                {
                    if (buttonToColorize.transform.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "2")
                        InterfaceManager.instance.ColorMapScale2.gameObject.SetActive(false);
                }
                buttonToColorize.transform.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "";
                buttonToColorize.GetComponent<Image>().sprite = null;
            }
            else
            {
                buttonToColorize.GetComponent<Image>().sprite = AssignSprite(colv);
                if (!InterfaceManager.instance.ColorMapScale.gameObject.activeSelf || buttonToColorize.transform.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "1" || activeToggle.name == "Main Color")
                {
                    ColorMapScale = InterfaceManager.instance.ColorMapScale;
                    buttonToColorize.transform.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "1";
                    ColorMapScalePercent = InterfaceManager.instance.ColorMapScalePercent;
                }
                else
                {
                    ColorMapScale = InterfaceManager.instance.ColorMapScale2;
                    buttonToColorize.transform.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "2";
                    ColorMapScalePercent = InterfaceManager.instance.ColorMapScalePercent2;
                }

                buttonToColorize.GetComponentInChildren<Text>().text = UpdateColorPropertyMapName(colv);
                
                float current_scale = 0f;
                foreach (Text t in ColorMapScalePercent)
                {
                    if (current_scale <= 0f)
                    { t.text = 
                        Math.Round(MinV, 2).ToString(); }
                    else if (current_scale >= 1f)
                    {
                        t.text = Math.Round(MaxV, 2).ToString();
                    }
                    else
                    {
                        t.text = Math.Round((MinV + (MaxV - MinV) * current_scale), 2).ToString();
                    }
                    current_scale += 1f / (ColorMapScalePercent.Count - 1 == 0 ? 1f : ColorMapScalePercent.Count - 1);
                }

                float mini = SliderMin.value;
                float maxi = SliderMax.value;

                ColorMapScale.gameObject.SetActive(true);
                ColorMapScale.sprite = AssignSprite(colv, mini,maxi);
            }

            Correspondence c = MorphoTools.GetDataset().Infos.Correspondences[0];
            foreach (Correspondence cor in MorphoTools.GetDataset().Infos.Correspondences)
            {
                if (cor.inf.transform.Find("InfosColor").gameObject == buttonToColorize)
                {
                    c = cor;
                }
            }

            Sprite def;
            Texture2D texty;
            int min, max;
            min = int.Parse(buttonToColorize.transform.Find("minVOther").gameObject.GetComponentInChildren<Text>().text, System.Globalization.NumberStyles.Any);
            max = int.Parse(buttonToColorize.transform.Find("maxVOther").gameObject.GetComponentInChildren<Text>().text, System.Globalization.NumberStyles.Any);

            def = MapColor.options[colv].image;
            switch (buttonToColorize.transform.Find("parameter").gameObject.GetComponentInChildren<Text>().text)
            {
                case "Main Color":
                    if (isDefaultOrColoredMaterial(MatMax))
                    {
                        
                        if (def != null)
                        {
                            texty = def.texture;
                            if (MatMax.GetColor("_Color") == texty.GetPixel(MaxC, 0))
                            {
                                c.CancelMainCmap();
                            }
                        }
                        else
                        {
                            c.CancelMainCmap();
                        }
                    }
                    break;

                case "Halo Color":
                    if (isDefaultOrColoredMaterial(MatMax))
                    {
                        if (def != null)
                        {
                            texty = def.texture;
                            if (MatMax.GetColor("_FresnelColor") == texty.GetPixel(MaxC, 0))
                            {
                                c.CancelHaloCmap();
                            }
                        }
                        else
                        {
                            c.CancelHaloCmap();
                        }
                    }
                    break;

                case "Second Color":
                    if (!isDefaultOrColoredMaterial(MatMax))
                    {
                        if (def != null)
                        {
                            texty = def.texture;
                            if (MatMax.GetColor("_Color2") == texty.GetPixel(MaxC, 0))
                            {
                                c.CancelSecondCmap();
                            }
                        }
                        else
                        {
                            c.CancelSecondCmap();
                        }
                    }
                    break;

                case "Metallic":
                    if (isDefaultOrColoredMaterial(MatMax))
                    {
                        
                        if (MatMax.GetFloat("_Metallic") == max && MatMin.GetFloat("_Metallic") == min)
                        {
                            c.CancelMetallicMap();
                        }
                    }
                    break;

                case "Smoothness":
                    if (isDefaultOrColoredMaterial(MatMax))
                    {
                        if (MatMax.GetFloat("_Smoothness") == max && MatMin.GetFloat("_Smoothness") == min)
                        {
                            c.CancelSmoothnessMap();
                        }
                    }
                    break;

                case "Transparency":
                    if (isDefaultOrColoredMaterial(MatMax))
                    {
                        if (MatMax.GetFloat("_Transparency") == max && MatMin.GetFloat("_Transparency") == min)
                        {
                            c.CancelSmoothnessMap();
                        }
                    }
                    break;

                default:
                    break;
            }

            SetButtonToColorizeValues(colv.ToString());

            //RESET THE OTHE
            foreach (Correspondence cor in MorphoTools.GetDataset().Infos.Correspondences)
            {
                Transform infc = cor.inf.transform.Find("InfosColor");
                if (infc.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == buttonToColorize.transform.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text 
                    && buttonToColorize.transform.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text != "" && infc.gameObject != buttonToColorize)
                {
                    infc.gameObject.GetComponentInChildren<Text>().text = "";
                    infc.gameObject.GetComponent<Image>().sprite = null;
                    infc.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "";
                }
                if (infc.Find("parameter").gameObject.GetComponentInChildren<Text>().text == buttonToColorize.transform.Find("parameter").gameObject.GetComponentInChildren<Text>().text 
                    && buttonToColorize.transform.Find("parameter").gameObject.GetComponentInChildren<Text>().text != "" && infc.gameObject != buttonToColorize)
                {
                    if (activeToggle.name == "Main Color" || activeToggle.name == "Halo Color" || activeToggle.name == "Second Color")
                    {
                        if (infc.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "1")
                        {
                            InterfaceManager.instance.ColorMapScale.gameObject.SetActive(false);
                            infc.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "";
                            infc.gameObject.GetComponent<Image>().sprite = null;
                        }
                        if (infc.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "2")
                        {
                            InterfaceManager.instance.ColorMapScale2.gameObject.SetActive(false);
                            infc.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "";
                            infc.gameObject.GetComponent<Image>().sprite = null;
                        }
                    }
                    infc.Find("parameter").gameObject.GetComponentInChildren<Text>().text = "";
                    infc.Find("lastParameter").gameObject.GetComponentInChildren<Text>().text = "";
                }
                if ((activeToggle.name == "Main Color" || activeToggle.name == "Halo Color" || activeToggle.name == "Metallic" || activeToggle.name == "Smoothness" || activeToggle.name == "Transparency") && infc.Find("parameter").gameObject.GetComponentInChildren<Text>().text == "Second Color")
                {
                    infc.gameObject.GetComponentInChildren<Text>().text = "";
                    if (infc.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "1")
                    {
                        InterfaceManager.instance.ColorMapScale.gameObject.SetActive(false);
                        infc.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "";
                        infc.gameObject.GetComponent<Image>().sprite = null;
                    }
                    if (infc.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "2")
                    {
                        InterfaceManager.instance.ColorMapScale2.gameObject.SetActive(false);
                        infc.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "";
                        infc.gameObject.GetComponent<Image>().sprite = null;
                    }
                }
                if (activeToggle.name == "Second Color" && infc.Find("parameter").gameObject.GetComponentInChildren<Text>().text == "Halo Color")
                {
                    infc.gameObject.GetComponentInChildren<Text>().text = "";
                    if (infc.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "1")
                    {
                        InterfaceManager.instance.ColorMapScale.gameObject.SetActive(false);
                        infc.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "";
                        infc.gameObject.GetComponent<Image>().sprite = null;
                    }
                    if (infc.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "2")
                    {
                        InterfaceManager.instance.ColorMapScale2.gameObject.SetActive(false);
                        infc.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "";
                        infc.gameObject.GetComponent<Image>().sprite = null;
                    }
                }
            }

            MatMin = ShaderMin.GetComponent<MeshRenderer>().material;
            MatMoy = ShaderMoy.GetComponent<MeshRenderer>().material;
            MatMax = ShaderMax.GetComponent<MeshRenderer>().material;

            MenuBarColor.SetActive(false);
        }

        public void SetButtonToColorizeValues(string type)
        {
            if (buttonToColorize.transform.Find("parameter").gameObject.GetComponentInChildren<Text>().text != "")
                buttonToColorize.transform.Find("lastParameter").gameObject.GetComponentInChildren<Text>().text = buttonToColorize.transform.Find("parameter").gameObject.GetComponentInChildren<Text>().text;
            buttonToColorize.transform.Find("parameter").gameObject.GetComponentInChildren<Text>().text = activeToggle.name;
            buttonToColorize.transform.Find("type").gameObject.GetComponentInChildren<Text>().text = type;
            buttonToColorize.transform.Find("min").gameObject.GetComponentInChildren<Text>().text = SliderMin.value.ToString();
            buttonToColorize.transform.Find("max").gameObject.GetComponentInChildren<Text>().text = SliderMax.value.ToString();
            buttonToColorize.transform.Find("minV").gameObject.GetComponentInChildren<Text>().text = SliderMinV.value.ToString();
            buttonToColorize.transform.Find("maxV").gameObject.GetComponentInChildren<Text>().text = SliderMaxV.value.ToString();
            buttonToColorize.transform.Find("minVOther").gameObject.GetComponentInChildren<Text>().text = SliderMinVOther.value.ToString();
            buttonToColorize.transform.Find("maxVOther").gameObject.GetComponentInChildren<Text>().text = SliderMaxVOther.value.ToString();
            buttonToColorize.transform.Find("execute").gameObject.GetComponentInChildren<Button>().onClick.Invoke();
        }

        public void ApplyGen() => applyGenetic(MapColorV, buttonToColorize.transform.parent.parent.name);

        public void applyGenetic(int colv, string gene)
        {
            List<Cell> list = new List<Cell>();
            foreach (Cell cell in MorphoTools.GetDataset().PickedManager.clickedCells)
            {
                list.Add(cell);
            }
            MorphoTools.GetDataset().PickedManager.clickedCells.Clear();

            foreach (Cell cell in list)
            {
                MorphoTools.GetDataset().PickedManager.clickedCells.Add(cell);
            }
            list.Clear();

            Image ColorMapScale;
            List<Text> ColorMapScalePercent;

            if (activeToggle.name == "Metallic" || activeToggle.name == "Smoothness" || activeToggle.name == "Transparency")
            {
                if (buttonToColorize.transform.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "1")
                    InterfaceManager.instance.ColorMapScale.gameObject.SetActive(false);
                else
                {
                    if (buttonToColorize.transform.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "2")
                        InterfaceManager.instance.ColorMapScale2.gameObject.SetActive(false);
                }
                buttonToColorize.transform.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "";
                buttonToColorize.GetComponent<Image>().sprite = null;
            }
            else
            {
                buttonToColorize.GetComponent<Image>().sprite = AssignSprite(colv);
                if (!InterfaceManager.instance.ColorMapScale.gameObject.activeSelf || buttonToColorize.transform.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "1" || activeToggle.name == "Main Color")
                {
                    ColorMapScale = InterfaceManager.instance.ColorMapScale;
                    buttonToColorize.transform.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "1";
                    ColorMapScalePercent = InterfaceManager.instance.ColorMapScalePercent;
                }
                else
                {
                    ColorMapScale = InterfaceManager.instance.ColorMapScale2;
                    buttonToColorize.transform.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "2";
                    ColorMapScalePercent = InterfaceManager.instance.ColorMapScalePercent2;
                }

                buttonToColorize.GetComponentInChildren<Text>().text = UpdateColorPropertyMapName(colv);
                ColorMapScale.gameObject.SetActive(true);
                ColorMapScale.sprite = AssignSprite(colv);
                float current_scale = 0f;
                foreach (Text t in ColorMapScalePercent)
                {
                    if (current_scale <= 0f)
                    { t.text = MinVal.ToString("F2"); }
                    else if (current_scale >= 1f)
                    {
                        t.text = MaxVal.ToString("F2");
                    }
                    else
                    {
                        t.text = (MinVal + (MaxVal - MinVal) * current_scale).ToString("F2");
                    }

                    current_scale += 1f / (ColorMapScalePercent.Count - 1 == 0 ? 1f : ColorMapScalePercent.Count - 1);
                }
            }

            Gene g;
            if (MorphoTools.GetDataset().GeneticManager.Genes.ContainsKey(gene))
                g = MorphoTools.GetDataset().GeneticManager.Genes[gene];
            else
                return;

            Sprite def;
            Texture2D texty;
            int min, max;
            min = int.Parse(buttonToColorize.transform.Find("minVOther").gameObject.GetComponentInChildren<Text>().text, System.Globalization.NumberStyles.Any);
            max = int.Parse(buttonToColorize.transform.Find("maxVOther").gameObject.GetComponentInChildren<Text>().text, System.Globalization.NumberStyles.Any);

            def = MapColor.options[int.Parse(buttonToColorize.transform.Find("type").gameObject.GetComponentInChildren<Text>().text, System.Globalization.NumberStyles.Any)].image;
            switch (buttonToColorize.transform.Find("parameter").gameObject.GetComponentInChildren<Text>().text)
            {
                case "Main Color":
                    if (isDefaultOrColoredMaterial(MatMax))
                    {
                        
                        if (def != null)
                        {
                            texty = def.texture;
                            if (MatMax.GetColor("_Color") == texty.GetPixel(MaxC, 0))
                            {
                                g.CancelMainCmap();
                            }
                        }
                        else
                        {
                            g.CancelMainCmap();
                        }
                    }
                    break;

                case "Halo Color":
                    if (isDefaultOrColoredMaterial(MatMax))
                    {
                        if (def != null)
                        {
                            texty = def.texture;
                            if (MatMax.GetColor("_FresnelColor") == texty.GetPixel(MaxC, 0))
                            {
                                g.CancelHaloCmap();
                            }
                        }
                        else
                        {
                            g.CancelHaloCmap();
                        }
                    }
                    break;

                case "Second Color":
                    if (!isDefaultOrColoredMaterial(MatMax))
                    {
                        if (def != null)
                        {
                            texty = def.texture;
                            if (MatMax.GetColor("_Color2") == texty.GetPixel(MaxC, 0))
                            {
                                g.CancelSecondCmap();
                            }
                        }
                        else
                        {
                            g.CancelSecondCmap();
                        }
                    }
                    break;

                case "Metallic":
                    if (isDefaultOrColoredMaterial(MatMax))
                    {
                        if (MatMax.GetFloat("_Metallic") == max && MatMin.GetFloat("_Metallic") == min)
                        {
                            g.CancelMetallicMap();
                        }
                    }
                    break;

                case "Smoothness":
                    if (isDefaultOrColoredMaterial(MatMax))
                    {
                        if (MatMax.GetFloat("_Smoothness") == max && MatMin.GetFloat("_Smoothness") == min)
                        {
                            g.CancelSmoothnessMap();
                        }
                    }
                    break;

                case "Transparency":
                    if (isDefaultOrColoredMaterial(MatMax))
                    {
                        if (MatMax.GetFloat("_Transparency") == max && MatMin.GetFloat("_Transparency") == min)
                        {
                            g.CancelSmoothnessMap();
                        }
                    }
                    break;

                default:
                    break;
            }

            SetButtonToColorizeValues(colv.ToString());

            //RESET THE OTHE
            foreach (KeyValuePair<string, Gene> dgen in MorphoTools.GetDataset().GeneticManager.Genes)
            {
                Gene gen = dgen.Value;
                Transform quant = gen.go.transform.Find("SubGene").Find("Quantitative");

                if (quant.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == buttonToColorize.transform.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text && buttonToColorize.transform.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text != "" && quant.gameObject != buttonToColorize)
                {
                    quant.gameObject.GetComponentInChildren<Text>().text = "";
                    quant.gameObject.GetComponent<Image>().sprite = null;
                    quant.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "";
                }
                if (quant.Find("parameter").gameObject.GetComponentInChildren<Text>().text == buttonToColorize.transform.Find("parameter").gameObject.GetComponentInChildren<Text>().text && buttonToColorize.transform.Find("parameter").gameObject.GetComponentInChildren<Text>().text != "" && quant.gameObject != buttonToColorize)
                {
                    if (activeToggle.name == "Main Color" || activeToggle.name == "Halo Color" || activeToggle.name == "Second Color")
                    {
                        if (quant.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "1")
                        {
                            InterfaceManager.instance.ColorMapScale.gameObject.SetActive(false);
                            quant.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "";
                            quant.gameObject.GetComponent<Image>().sprite = null;
                        }
                        if (quant.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "2")
                        {
                            InterfaceManager.instance.ColorMapScale2.gameObject.SetActive(false);
                            quant.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "";
                            quant.gameObject.GetComponent<Image>().sprite = null;
                        }
                    }
                    quant.Find("parameter").gameObject.GetComponentInChildren<Text>().text = "";
                    quant.Find("lastParameter").gameObject.GetComponentInChildren<Text>().text = "";
                }
                if ((activeToggle.name == "Main Color" || activeToggle.name == "Halo Color" || activeToggle.name == "Metallic" || activeToggle.name == "Smoothness" || activeToggle.name == "Transparency") && quant.Find("parameter").gameObject.GetComponentInChildren<Text>().text == "Second Color")
                {
                    quant.gameObject.GetComponentInChildren<Text>().text = "";
                    if (quant.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "1")
                    {
                        InterfaceManager.instance.ColorMapScale.gameObject.SetActive(false);
                        quant.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "";
                        quant.gameObject.GetComponent<Image>().sprite = null;
                    }
                    if (quant.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "2")
                    {
                        InterfaceManager.instance.ColorMapScale2.gameObject.SetActive(false);
                        quant.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "";
                        quant.gameObject.GetComponent<Image>().sprite = null;
                    }
                }
                if (activeToggle.name == "Second Color" && quant.Find("parameter").gameObject.GetComponentInChildren<Text>().text == "Halo Color")
                {
                    quant.gameObject.GetComponentInChildren<Text>().text = "";
                    if (quant.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "1")
                    {
                        InterfaceManager.instance.ColorMapScale.gameObject.SetActive(false);
                        quant.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "";
                        quant.gameObject.GetComponent<Image>().sprite = null;
                    }
                    if (quant.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text == "2")
                    {
                        InterfaceManager.instance.ColorMapScale2.gameObject.SetActive(false);
                        quant.Find("numMapScale").gameObject.GetComponentInChildren<Text>().text = "";
                        quant.gameObject.GetComponent<Image>().sprite = null;
                    }
                }
            }

            MatMin = ShaderMin.GetComponent<MeshRenderer>().material;
            MatMoy = ShaderMoy.GetComponent<MeshRenderer>().material;
            MatMax = ShaderMax.GetComponent<MeshRenderer>().material;

            MenuBarColor.SetActive(false);
        }

        //setup menu for genetic colorbar
        public void SetUpGenetic()
        {
            ApplyButton.onClick.RemoveAllListeners();
            ApplyButton.onClick.AddListener(() => ApplyGen());
        }

        //setup menu for information colorbar
        public void SetupCorrespondance()
        {
            ApplyButton.onClick.RemoveAllListeners();
            ApplyButton.onClick.AddListener(() => apply());
        }
    }
}