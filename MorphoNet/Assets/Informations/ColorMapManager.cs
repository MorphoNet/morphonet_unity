﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using MorphoNet.Extensions.Unity;

namespace MorphoNet
{
    public class ColorMapManager : MonoBehaviour
    {
        public GameObject MenuLoadSelection;
        public GameObject MenuObjects;
        public GameObject MenuShare;

        //Colormap
        public InputField NameColormap;

        public Dropdown ListColormap;
        private Dictionary<int, int> CorrespondIdColormap;
        private Dictionary<int, int> CorrespondPeopleColormap;
        private Dictionary<int, bool> ColorMapSharedStatus;
        public GameObject BdeleteColormap;
        public GameObject BshareColormap;
        public GameObject BloadColormap;
        public GameObject ButtonDownloadMap;
        public GameObject UploadButton;
        public GameObject uploadPanel;
        public Image SaveButton;
        public InputField uploadName;
        public Toggle uploadFileName;

        public Sprite UnsharedSprite;
        public Sprite SharedSprite;

        public Sprite DotsSprite;
        public Sprite UploadSprite;

        private bool _uploadingColormap = false;

#if UNITY_WEBGL
    [DllImport("__Internal")]
    private static extern void selectColormap();
#else

        private static void selectColormap()
        { }

#endif

        public int selected_colormap = -1;

        public void Start()
        {
            BdeleteColormap.SetActive(false);
            BshareColormap.SetActive(false);
            BloadColormap.SetActive(false);
            ButtonDownloadMap.SetActive(false);

            if (SetsManager.instance.userRight >= 2)
            {//PUBLIC ACCESS
             //UNACCESS  SAVE COLOR MAP
                MenuLoadSelection.transform.Find("save").gameObject.SetActive(false);
                UploadButton.gameObject.SetActive(false);
                NameColormap.text = "log in to save colormaps";
                NameColormap.enabled = false;
            }
        }

        //LIST COLORMAPS
        public void listAllColormap()
        {
            StartCoroutine(queryColormapList());
        }

        public void LoadDefaultMap()
        {
            for (int i = 0; i < 255; i++)
            {
                SelectionManager.createMaterial(i);
            }
        }

        public IEnumerator queryColormapList()
        {
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/partialcolormap/?id_dataset=" + SetsManager.instance.DataSet.id_dataset + "&id_people=" + SetsManager.instance.IDUser);
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                ListColormap.ClearOptions();
                
                CorrespondIdColormap = new Dictionary<int, int>();
                CorrespondPeopleColormap = new Dictionary<int, int>();

                ColorMapSharedStatus = new Dictionary<int, bool>();

                //Return id,name,date
                var N = JSONNode.Parse(www.downloadHandler.text);
                List<string> ColormapList = new List<string>();
                ColormapList.Add("None");
                Debug.LogError("ENORME");
                int it = 1;
                if (N != null && N.Count > 0)
                {
                    for (int i = 0; i < N.Count; i++)
                    { //Number of Uploaded Data
                        int id_colormap = N[i][0].AsInt;
                        string name = N[i][1].ToString().Replace('"', ' ').Trim();
                        int id_people = N[i][3].AsInt;
                        int id_dataset = N[i][4].AsInt;
                        bool shared = N[i][5].AsBool;

                        string colormapnname = name;
                        if ((id_people == SetsManager.instance.IDUser || shared) && id_dataset == SetsManager.instance.DataSet.id_dataset)
                        {
                            ColormapList.Add(colormapnname);
                            CorrespondIdColormap[it] = id_colormap;
                            CorrespondPeopleColormap[it] = id_people;
                            ColorMapSharedStatus[it] = shared;
                            it++;
                        }
                    }
                }
                ListColormap.AddOptions(ColormapList);
                int mapOrder = -1;
                if (selected_colormap != -1)
                {
                    foreach (KeyValuePair<int, int> pair in CorrespondIdColormap)
                    {
                        if (pair.Value == selected_colormap)
                        {
                            mapOrder = pair.Key;
                            break;
                        }
                    }
                }
                ListColormap.value = mapOrder;
                onChooseColormap(mapOrder);
            }
            www.Dispose();
        }

        //When we change the list of colormap we appear/diseapar delete and shared button
        public void onChooseColormap(int v)
        {
            if (v == -1)
                v = ListColormap.value;
            BdeleteColormap.SetActive(false);
            BshareColormap.SetActive(false);
            ButtonDownloadMap.SetActive(false);
            if (v == 0)
            {
                BloadColormap.SetActive(false);
            }
            else
            {
                BloadColormap.SetActive(true);
                if (CorrespondIdColormap.ContainsKey(v))
                {
                    selected_colormap = CorrespondIdColormap[v];
                    ButtonDownloadMap.SetActive(true);
                    if (CorrespondPeopleColormap.ContainsKey(v))
                    {
                        if (CorrespondPeopleColormap[v] == SetsManager.instance.IDUser)
                        {
                            BdeleteColormap.SetActive(true);
                            BshareColormap.SetActive(true);
                            if (ColorMapSharedStatus[v])
                            {
                                BshareColormap.GetComponent<Image>().sprite = SharedSprite;
                            }
                            else
                            {
                                BshareColormap.GetComponent<Image>().sprite = UnsharedSprite;
                            }
                        }
                    }
                }
            }
        }

        //When we decide to share a colormap
        public void onShareColormap()
        {
            int v = ListColormap.value;
            ToggleShared(v);
        }

        public void downloadColorField()
        {
            if (selected_colormap != -1)
            {
                StartCoroutine(DownloadMapField());
            }
        }

        public IEnumerator DownloadMapField()
        {
            WWWForm formC = new WWWForm();
            formC.AddField("id_colormap", selected_colormap);

            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "/api/downloadcolormap/", formC);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                if (www.downloadHandler.text != "")
                {
                    string urlinfos = MorphoTools.GetServer() + www.downloadHandler.text;
#if !UNITY_EDITOR
				Recording.openWindow(urlinfos);
#else
                    MorphoDebug.Log("OPEN " + urlinfos);
#endif
                }
            }
            www.Dispose();
        }

        //LOAD A COLORMAP
        public void loadColormap()
        {
            int v = ListColormap.value;
            if (v != 0 && CorrespondIdColormap.ContainsKey(v))
            {
                StartCoroutine(queryLoadColormap(CorrespondIdColormap[v]));
            }
        }

        public IEnumerator queryLoadColormap(int id_colormap)
        {
            WWWForm form = new WWWForm();
            form.AddField("colormap", "2");
            form.AddField("id_colormap", id_colormap.ToString());
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "/api/colormapinfos/?hash=" + SetsManager.instance.hash + "&id_colormap=" + id_colormap.ToString());
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                //split the returned text for memory managment in webGL

                //Return colors ....
                string[] sep = { "b\'{" };
                string[] sep2 = { "}," };
                string[] colors = www.downloadHandler.text.Split(sep, System.StringSplitOptions.None)[1].Split(sep2, System.StringSplitOptions.None);
                yield return new WaitForEndOfFrame();

                if (colors.Length > 0)
                {
                    for (int i = 0; i < colors.Length; i++)
                    { //Number of Uploaded Data
                        var Map = new JSONNode();
                        if (i == colors.Length - 1)
                        {
                            string text = "{" + colors[i].Substring(0, colors[i].Length - 4);
                            Map = JSONNode.Parse(text);
                        }
                        else
                        {
                            Map = JSONNode.Parse("{" + colors[i] + "}}");
                        }
                        string shader = Map[0]["shader"].ToString().Replace("\"", "");
                        shader = shader.Replace(" (Instance)", "");
                        Material m = ChangeColorSelection.getMaterialByName(shader);

                        int R = Map[0]["R"].AsInt;
                        int G = Map[0]["G"].AsInt;
                        int B = Map[0]["B"].AsInt;
                        int A = Map[0]["A"].AsInt;
                        m.color = new Color((float)R / 255f, (float)G / 255f, (float)B / 255f, (float)A / 255f);
                        //then set special parameters
                        foreach (string k in Map[0]["params"].Keys.ToList())
                        {
                            if (m.HasProperty(k))
                            {
                                if (Map[0]["params"][k].AsArray != null && Map[0]["params"][k].AsArray.Count > 1)
                                {//if color
                                    var ArrRBGA = Map[0]["params"][k].AsArray;
                                    string[] rgba = new string[4];
                                    for (int j = 0; j < ArrRBGA.Count; j++)
                                    {
                                        rgba[j] = ArrRBGA[j].ToString().Replace("\"", "");
                                    }
                                    m.UpdateColorProperty(k, new Color(float.Parse(rgba[0], System.Globalization.CultureInfo.InvariantCulture), float.Parse(rgba[1], System.Globalization.CultureInfo.InvariantCulture), float.Parse(rgba[2], System.Globalization.CultureInfo.InvariantCulture), float.Parse(rgba[3], System.Globalization.CultureInfo.InvariantCulture)));
                                }
                                else
                                {
                                    m.UpdateFloatProperty(k, float.Parse(Map[0]["params"][k].Value, System.Globalization.CultureInfo.InvariantCulture));
                                }
                            }
                        }

                        SelectionManager.materials[i] = m;
                        yield return new WaitForEndOfFrame();
                    }
                }
                MenuObjects.gameObject.transform.Find("Selection").gameObject.GetComponent<SelectionManager>().resetAllMaterials();
            }
            www.Dispose();
            ButtonDownloadMap.SetActive(false);
            InterfaceManager.instance.MenuObjects.transform.Find("MenuLoadSelection").Find("Upload Panel").gameObject.SetActive(false);
            MenuLoadSelection.SetActive(false);
        }

        public void LoadColormapPlot(string colormap)
        {
            //Return colors ....
            string[] sep = { "}," };
            string[] colors = colormap.Split(sep, System.StringSplitOptions.None);

            if (colors.Length > 0)
            {
                for (int i = 0; i < colors.Length; i++)
                { //Number of Uploaded Data
                    var Map = new JSONNode();
                    if (i == colors.Length - 1)
                    {
                        string text = "{" + colors[i]/*.Substring(0, colors[i].Length - 3)*/;
                        Map = JSONNode.Parse(text);
                    }
                    else
                    {
                        Map = JSONNode.Parse("{" + colors[i] + "}}");
                    }
                    string shader = Map[0]["shader"].ToString().Replace("\"", "");
                    Material m = ChangeColorSelection.getMaterialByName(shader);

                    int R = Map[0]["R"].AsInt;
                    int G = Map[0]["G"].AsInt;
                    int B = Map[0]["B"].AsInt;
                    int A = Map[0]["A"].AsInt;
                    m.color = new Color((float)R / 255f, (float)G / 255f, (float)B / 255f, (float)A / 255f);
                    SelectionManager.materials[i] = m;
                }
            }
        }

        //DELETE A COLORMAP
        public void deleteColormap()
        {
            int v = ListColormap.value;
            selected_colormap = -1;
            ButtonDownloadMap.SetActive(false);
            if (v != 0 && CorrespondIdColormap.ContainsKey(v))
            {
                if (CorrespondPeopleColormap.ContainsKey(v))
                {
                    if (CorrespondPeopleColormap[v] == SetsManager.instance.IDUser)
                        StartCoroutine(queryDeleteColormap(CorrespondIdColormap[v]));
                }
            }
        }

        public void DisplayUploadPanel()
        {
            uploadPanel.SetActive(!uploadPanel.activeSelf);
            if (uploadPanel.activeSelf)
            {
                uploadName.text = "Default map name";
            }
        }

        public void UploadColorMap()
        {
#if UNITY_EDITOR
            string path = UnityEditor.EditorUtility.OpenFilePanel("Open text", "", "");
            if (!System.String.IsNullOrEmpty(path))
            {
                FileSelected("file:///" + path + ";" + Path.GetFileName(path));
            }
#else
		selectColormap();
#endif
            MenuLoadSelection.SetActive(false);
        }

        private void FileSelected(string urlname)
        {
            if (SetsManager.instance.IDUser != -1)
            {
                MorphoDebug.Log(urlname);
                StartCoroutine(uploadData(urlname));
            }
        }

        public IEnumerator uploadData(string urlname)
        {
            string[] urls = urlname.Split(';');
            string url = urls[0];
            string[] filenames = urls[1].Split('.');
            string filename = "";
            if (filenames.Count() == 1)
                filename = filenames[0];
            else
            {
                for (int i = 0; i < filenames.Count() - 1; i++)
                {
                    filename += filenames[i];
                    if (i < filenames.Count() - 2)
                        filename += ".";
                }
            }

            MorphoDebug.Log(" URL is " + url);
#if UNITY_EDITOR
            if (url.IndexOf("file:///") == -1 && url.IndexOf("file://") == 0)
                url = url.Replace("file://", "file:///");
            url = url.Replace("file:////", "file:///");
#endif
            MorphoDebug.Log(" URL is " + url);
            UnityWebRequest www = UnityWebRequests.Get(url, "");
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
                MorphoDebug.Log("ERROR :" + www.error);
            else
            {
                string textToLoad = www.downloadHandler.text;
                string name = "";
                if (uploadFileName.isOn)
                {
                    string[] parts = url.Split('/');
                    string total_filename = parts[parts.Length - 1];
                    string[] division = total_filename.Split('.');
                    name = division[0];
                }
                else
                {
                    name = uploadName.text;
                }
                WWWForm form = new WWWForm();
                form.AddField("id_people", SetsManager.instance.IDUser.ToString());
                form.AddField("name", name);
                form.AddField("content", textToLoad);
                form.AddField("id_dataset", SetsManager.instance.DataSet.id_dataset);
                UnityWebRequest www2 = UnityWebRequests.Post(MorphoTools.GetServer(), "/api/savecolormaptext/", form);
                if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
                {
                    www2.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
                }

                yield return www2.SendWebRequest();
                if (www2.isHttpError || www2.isNetworkError)
                    MorphoDebug.Log("Error : " + www2.error);
                else
                {
                    if (www2.downloadHandler.text != "done")
                        MorphoDebug.Log("Error during upload " + www2.downloadHandler.text);
                }
                www.Dispose();
            }
            www.Dispose();
        }

        public IEnumerator queryDeleteColormap(int id_colormap)
        {
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("colormap", "3");
            form.AddField("id_people", SetsManager.instance.IDUser.ToString());
            form.AddField("id_colormap", id_colormap.ToString());
            UnityWebRequest www = UnityWebRequests.Delete(MorphoTools.GetServer(), "api/deletecolormap/?hash=" + SetsManager.instance.hash + "&id_people=" + SetsManager.instance.IDUser.ToString() + "&id_colormap=" + id_colormap.ToString());
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                if (www.downloadHandler != null)
                {
                    if (www.downloadHandler.text != "")
                        MorphoDebug.Log("Error Delete " + www.downloadHandler.text);
                }
            }
            www.Dispose();
            MenuLoadSelection.SetActive(false);
        }

        //SAVE A COLORMAP
        public void saveColormap()
        {
            if (NameColormap.text != "" && !_uploadingColormap && SetsManager.instance.IDUser != -1)
            {
                StartCoroutine(querySaveColormap(NameColormap.text));
                SaveButton.sprite = DotsSprite;
                _uploadingColormap = true;
            }
        }

        private void Update()
        {
            //uploadName.gameObject.SetActive(!uploadFileName.isOn);
        }

        public IEnumerator querySaveColormap(string colormapname)
        {
            //Fist we look for all colormap value
            var S = new JSONClass();
            for (int i = 0; i < SelectionManager.indexcolors.Length; i++)
            {
                string shader = SetsManager.instance.Default.name;
                int R = 0;
                int G = 0;
                int B = 0;
                int A = 0;
                if (SelectionManager.materials[i] != null)
                {
                    shader = SelectionManager.materials[i].name;
                    R = (int)Mathf.Round(255F * SelectionManager.materials[i].color.r);
                    G = (int)Mathf.Round(255F * SelectionManager.materials[i].color.g);
                    B = (int)Mathf.Round(255F * SelectionManager.materials[i].color.b);
                    A = (int)Mathf.Round(255F * SelectionManager.materials[i].color.a);
                }
                else
                {
                    string hexcol = SelectionManager.indexcolors[SelectionManager.colororder[i]];
                    Color newColor = new Color();
                    ColorUtility.TryParseHtmlString(hexcol, out newColor);
                    R = (int)Mathf.Round(255F * newColor.r);
                    G = (int)Mathf.Round(255F * newColor.g);
                    B = (int)Mathf.Round(255F * newColor.b);
                    A = (int)Mathf.Round(255F * newColor.a);
                }
                shader = shader.Replace(" (Instance)", "");
                //get materialPreset for advanced params:
                MaterialPreset pres = ChangeColorSelection.GetPresetByMaterialName(shader);
                var p = new JSONClass();
                foreach (MaterialPreset.ShaderProperty prop in pres.Properties)
                {
                    if (!prop.isColor)
                    {
                        if (SelectionManager.materials[i].HasProperty(prop.name))
                            p[prop.name] = SelectionManager.materials[i].GetFloat(prop.name).ToString().Replace(",", ".");
                    }
                    else
                    {
                        if (SelectionManager.materials[i].HasProperty(prop.name))
                        {
                            var Arr = p[prop.name].AsArray;
                            string[] ColorValues = SelectionManager.materials[i].GetColor(prop.name).ToString().Replace("RGBA(", "").Replace(")", "").Replace(" ", "").Split(',');
                            foreach (var val in ColorValues)
                                Arr.Add(val.Replace(",", "."));
                        }
                    }
                }

                S[i.ToString()]["shader"] = shader;
                S[i.ToString()]["R"].AsInt = R;
                S[i.ToString()]["G"].AsInt = G;
                S[i.ToString()]["B"].AsInt = B;
                S[i.ToString()]["A"].AsInt = A;

                S[i.ToString()]["params"] = p;
            }
            WWWForm form = new WWWForm();
            form.AddField("id_people", SetsManager.instance.IDUser.ToString());
            form.AddField("name", colormapname);
            form.AddField("id_dataset", SetsManager.instance.DataSet.id_dataset);
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(S.ToString());
            form.AddBinaryData("colormap.txt", bytes);
            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/savecolormap/", form);
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                if (www.downloadHandler.text != "done")
                    MorphoDebug.Log("Error saving colormap  " + www.downloadHandler.text);
            }
            www.Dispose();
            NameColormap.text = "";
            MenuLoadSelection.SetActive(false);
            SaveButton.sprite = UploadSprite;
            _uploadingColormap = false;
        }

        public void ToggleShared(int v)
        {
            StartCoroutine(queryToggleShared(CorrespondIdColormap[v]));
        }

        public IEnumerator queryToggleShared(int id_colormap)
        {
            WWWForm form = new WWWForm();
            form.AddField("id_colormap", id_colormap);
            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/togglecolormapshared/", form);
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                int map_id = -1;
                foreach (KeyValuePair<int, int> kv in CorrespondIdColormap)
                {
                    if (kv.Value == id_colormap)
                        map_id = kv.Key;
                }
                if (map_id != -1)
                {
                    ColorMapSharedStatus[map_id] = !ColorMapSharedStatus[map_id];
                    if (ColorMapSharedStatus[map_id])
                    {
                        BshareColormap.GetComponent<Image>().sprite = SharedSprite;
                    }
                    else
                    {
                        BshareColormap.GetComponent<Image>().sprite = UnsharedSprite;
                    }
                }
            }
        }

        public void ReloadColormap()
        {
            var S = new JSONClass();
            for (int i = 0; i < SelectionManager.indexcolors.Length; i++)
            {
                string shader = SetsManager.instance.Default.name;
                float R = 0;
                float G = 0;
                float B = 0;
                float A = 0;
                if (SelectionManager.materials[i] != null)
                {
                    shader = SelectionManager.materials[i].name;
                    R = SelectionManager.materials[i].color.r;
                    G = SelectionManager.materials[i].color.g;
                    B = SelectionManager.materials[i].color.b;
                    A = SelectionManager.materials[i].color.a;
                }
                else
                {
                    string hexcol = SelectionManager.indexcolors[SelectionManager.colororder[i]];
                    Color newColor = new Color();
                    ColorUtility.TryParseHtmlString(hexcol, out newColor);
                    R = newColor.r;
                    G = newColor.g;
                    B = newColor.b;
                    A = newColor.a;
                }
                shader = shader.Replace(" (Instance)", "");
                Material m = ChangeColorSelection.getMaterialByName(shader);
                m.color = new Color(R, G, B, A);
                //get materialPreset for advanced params:
                MaterialPreset pres = ChangeColorSelection.GetPresetByMaterialName(shader);
                var p = new JSONClass();
                foreach (MaterialPreset.ShaderProperty prop in pres.Properties)
                {
                    if (!prop.isColor)
                    {
                        if (SelectionManager.materials[i].HasProperty(prop.name))
                            m.UpdateFloatProperty(prop.name, SelectionManager.materials[i].GetFloat(prop.name));
                    }
                    else
                    {
                        if (SelectionManager.materials[i].HasProperty(prop.name))
                        {
                            m.UpdateColorProperty(prop.name, SelectionManager.materials[i].GetColor(prop.name));
                        }
                    }
                }

                SelectionManager.materials[i] = m;
            }
        }
    }
}