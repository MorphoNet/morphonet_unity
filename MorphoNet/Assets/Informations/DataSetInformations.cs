﻿using AssemblyCSharp;
using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using SimpleFileBrowser;

namespace MorphoNet
{
    public class CorrespondenceType
    {
        public string name;
        private DataSetInformations parent;
        public List<Correspondence> Correspondences;
        public GameObject go;
        public bool isDeploy;

        public CorrespondenceType(string name, DataSetInformations source)
        {
            parent = source;
            isDeploy = true;
            this.name = name;
            Correspondences = new List<Correspondence>();
            go = SetsManager.instance.InstantiateGO(parent.DefaultCT, parent.MenuInfos.transform, true);
            go.name = name;
            go.transform.Find("datatype").transform.GetComponent<Text>().text = name;
            go.transform.Find("Infos").gameObject.SetActive(false);
            go.transform.Find("compare").gameObject.SetActive(false);
            go.transform.Find("downloadAll").gameObject.SetActive(false);
            go.SetActive(true);
            go.transform.Find("reploy").gameObject.SetActive(false);
            if (SetsManager.instance.userRight <= 1)
            {//NO READER
                parent.deployCTListen(go.transform.Find("deploy").transform.GetComponent<Button>(), this);
                parent.deployCTListen(go.transform.Find("reploy").transform.GetComponent<Button>(), this);
            }
            else
            {
                go.transform.Find("deploy").gameObject.SetActive(false);
                go.transform.Find("reploy").gameObject.SetActive(false);
            }
        }

        public void deploy()
        {
            if (isDeploy)
            {
                go.transform.Find("reploy").gameObject.SetActive(true);
                go.transform.Find("deploy").gameObject.SetActive(false);
            }
            else
            {
                go.transform.Find("reploy").gameObject.SetActive(false);
                go.transform.Find("deploy").gameObject.SetActive(true);
            }
            isDeploy = !isDeploy;
            foreach (Correspondence cor in Correspondences)
                if (cor.isActive)
                    cor.inf.SetActive(isDeploy);
            parent.reOrganize();
        }

        public bool Contains(Correspondence cor)
        {
            return Correspondences.Contains(cor);
        }

        public void addCorrespondence(Correspondence cor)
        {
            Correspondences.Add(cor);
            parent.Correspondences.Add(cor);
        }

        public void removeCorrespondence(Correspondence cor)
        {
            List<Correspondence> NC = new List<Correspondence>();
            foreach (Correspondence corr in Correspondences)
                if (corr.id_infos != cor.id_infos)
                    NC.Add(corr);
            Correspondences = NC;

            List<Correspondence> NNC = new List<Correspondence>();
            foreach (Correspondence corr in parent.Correspondences)
                if (corr.id_infos != cor.id_infos)
                    NNC.Add(corr);
            parent.Correspondences = NNC;
        }

        public int SetPosition(int Height)
        {
            //SCROLL ACTIVATION

            parent.inScroll++;
            if (parent.inScroll <= parent.nbSCroll)
            {
                go.transform.Find("datatype").gameObject.SetActive(false);
                if (name == "Quantitative")
                    go.transform.Find("compare").gameObject.SetActive(false);
                if (isDeploy)
                    go.transform.Find("deploy").gameObject.SetActive(false);
                else
                {
                    go.transform.Find("reploy").gameObject.SetActive(true);
                    go.SetActive(false);
                }
            }
            else if ((parent.inScroll - parent.nbSCroll) * 22 > parent.MaxHeight)
            { //TOO LONG BOTTOM
                go.SetActive(false);
            }
            else
            { //PILE POILE
                go.SetActive(true);
                go.transform.Find("datatype").gameObject.SetActive(true);
                if (isDeploy)
                    go.transform.Find("deploy").gameObject.SetActive(true);     //But  We have to check if all are deploy
                else
                    go.transform.Find("reploy").gameObject.SetActive(true);
            }

            go.transform.localPosition = new Vector3(go.transform.localPosition.x, parent.nbSCroll * 22 - Height - parent.shiftStart, go.transform.localPosition.z); //76 Correspond to the shift of menu top
            int sizeH = 0;
            if (isDeploy)
                foreach (Correspondence cor in Correspondences)
                    if (cor.isActive)
                        sizeH += cor.SetPosition(sizeH);

            sizeH += 22;
            return sizeH;
        }
    }

    public class CorrespondenceOtherData
    {
        public int id_owner;
        public int id_infos; //ID IN database
        public string name;
        public string link;
        public uint version;
        public bool txt;//Bundle version of direct download ?
        public string datatype; //string,float,etc...
        public string data_show; //For space , time groups, we change the output
        public int type; //1 FOR PUBLIC, 2 FOR UPLOAD, 3 FOR SAVED SELECTION
        public int pos;
        public string uploadDate;
        public int id_dataset;

        public CorrespondenceOtherData(int id_dataset, int id_owner, int type, int id_infos, string name, string link, uint version, bool txt, string datatype, string uploadDate, int pos)
        {
            this.type = type;
            this.id_infos = id_infos;
            this.link = link;
            this.version = version;
            this.txt = txt;
            this.name = name;
            this.id_owner = id_owner;
            this.id_dataset = id_dataset;
            if (datatype == "rgb" || datatype == "rgba")
                datatype = "color";//TEMP TO DESTROY
            this.datatype = datatype;
            this.pos = pos;
            this.uploadDate = uploadDate;
        }
    }

    public class Correspondence
    {
        public bool isActive;
        public int id_owner;
        public int id_infos; //ID IN database
        public string name;
        public string link;
        public uint version;
        public bool txt;//Bundle version of direct download ?
        public string datatype; //string,float,etc...
        public string data_show; //For space , time groups, we change the output
        public int type; //1 FOR PUBLIC, 2 FOR UPLOAD, 3 FOR SAVED SELECTION
        public float MinInfos;
        public float MaxInfos;
        public bool show_all;
        public int common;
        public bool isDisplayed = false;
        public bool isValueShow; //Do we want to see the string value
        public bool IsActive = false;

        public GameObject inf;
        public GameObject SubInfos;
        public CorrespondenceType ct;
        public int id_dataset;
        public List<Curation> Curations;
        public Curation initalCuration = null;
        public DataSetInformations parent;

        public event Action<Correspondence> OnCorrespondenceActivation;

        private static string MORPHONET_URL_START = "https://morphonet.org";

        public override string ToString()
        {
            string value = "";
            value += "Info (" + id_infos + ") : " + "\n";
            value += "---> owner : " + id_owner + "\n";
            value += "---> name : " + name + "\n";
            value += "---> link : " + link + "\n";
            value += "---> datatype : " + datatype + "\n";

            return value;
        }

        public Correspondence(int id_dataset, CorrespondenceType ct, int id_owner, int type, int id_infos, string name, string link, uint version, bool txt, string datatype, string uploadDate, int common, DataSetInformations source, bool sk_prop = false)
        {
            parent = source;
            isActive = true;
            if (datatype == "time" || name.ToLower().Contains("name"))
                type = 1;
            if (SetsManager.instance.API_Correspondence != null && SetsManager.instance.API_Correspondence.ContainsKey(id_infos) && SetsManager.instance.API_Correspondence[id_infos] >= 2)
                type = 1;
            this.type = type;
            this.id_infos = id_infos;
            this.link = link;
            this.version = version;
            this.txt = txt;
            this.name = name;
            this.id_owner = id_owner;
            if (datatype == "rgb" || datatype == "rgba")
                datatype = "color";//TEMP TO DESTROY
            this.datatype = datatype;
            MinInfos = float.MaxValue;
            MaxInfos = float.MinValue;
            isValueShow = false;
            this.id_dataset = id_dataset;
            this.ct = ct;
            data_show = datatype;
            if (datatype == "time")
                data_show = "number";
            if (datatype == "space")
                data_show = "number";
            if (datatype == "group")
                data_show = "text";

            this.common = common;
            inf = SetsManager.instance.InstantiateGO(ct.go.transform.Find("Infos").gameObject, ct.go.transform, false);
            if ((datatype == "selection" || datatype == "label") && ( SetsManager.instance.userRight < 2 || LoadParameters.instance.id_dataset==0) )
            {
                inf.transform.Find("SubInfos").Find("Update").gameObject.SetActive(true);
                parent.UpdateSelectionListen(inf.transform.Find("SubInfos").Find("Update").GetComponent<Button>(), this);
            }
            else
            {
                inf.transform.Find("SubInfos").Find("Update").gameObject.SetActive(false);
            }

            if (!(datatype != "time" || SetsManager.instance.userRight > 1))
            {
                parent.StartCoroutine(parent.AttachedDataset.Curations.downloadCurations(this));
            }

            inf.SetActive(true);
            inf.name = "cor_" + this.id_infos;
            inf.transform.Find("infos").gameObject.GetComponent<Text>().text = this.name;

            inf.transform.Find("UploadDate").gameObject.GetComponent<Text>().text = uploadDate;
            inf.transform.Find("UploadDate").gameObject.SetActive(false);

            unActive(); //Default everything button if turn off

            if (id_dataset != 0)
            { //DIRECT PLOT
                if (datatype == "time")
                {
                    parent.loadListen(inf.transform.Find("load").gameObject.GetComponent<Button>(), this);
                }

                if (id_dataset == parent.AttachedDataset.id_dataset && type == 1)
                    parent.addDownloadDATA(this);
                else if (datatype == "genetic")
                    parent.addDownloadDATA(this);
                else
                {
                    parent.loadListen(inf.transform.Find("load").gameObject.GetComponent<Button>(), this);
                }
            }

            if (id_dataset == 0)//if on plot, make the download button ask for info contents
            {
                if (!sk_prop)
                {
                    parent.loadPlotListen(inf.transform.Find("load").gameObject.GetComponent<Button>(), this);
                }
                else
                {
                    parent.loadPlotSKListen(inf.transform.Find("load").gameObject.GetComponent<Button>(), this);
                }
            }

            //REMOVE || datatype == "space"
            if (SetsManager.instance.userRight >= 2)
                inf.transform.Find("deploy").gameObject.SetActive(false); //READER CANNOT MANAGE INFOS
        }



        public StringBuilder GetTxtInfos(bool add_names = false)
        {
            int id_name_info = -1;
            if (add_names)
            {
                foreach (Correspondence c in MorphoTools.GetDataset().Infos.Correspondences)
                {
                    if ((c.name.ToLower().Contains("cell_name") || (c.name.ToLower().Contains("cell") && c.name.ToLower().Contains("name"))) && (c.datatype.ToLower() == "text" || c.datatype.ToLower() == "string"))
                    {
                        id_name_info = c.id_infos;
                    }
                }
            }
            StringBuilder st = new StringBuilder();
            st.AppendLine("#" + name);
            st.AppendLine("type:" + datatype);
            if (datatype == "time")
            {
                if (parent.AttachedDataset != null && parent.AttachedDataset.CellsByTimePoint != null)
                {
                    foreach (var cellsbytime in parent.AttachedDataset.CellsByTimePoint)
                    {
                        foreach (Cell cell in cellsbytime.Value)
                        {
                            if (cell.Mothers != null && cell.Mothers.Count > 0)
                            {
                                foreach (Cell mother in cell.Mothers)
                                {
                                    string str_to_add = mother.getName() + ":" + cell.getName();
                                    if (mother.Infos!=null && id_name_info != -1 && mother.Infos.ContainsKey(id_name_info))
                                    {
                                        str_to_add += ":" + mother.Infos[id_name_info];
                                    }
                                    st.AppendLine(str_to_add);
                                }
                            }
                            else if(cell.Daughters == null || cell.Daughters != null && cell.Daughters.Count == 0)//case where we have single cells (no mothers and no daughters) : create an "single mother cell"
                            {
                                string str_to_add_alone = cell.getName() + ":None";
                                if (id_name_info != -1 && cell.Infos!=null && cell.Infos.ContainsKey(id_name_info))
                                {
                                    str_to_add_alone += ":" + cell.Infos[id_name_info];
                                }
                                st.AppendLine(str_to_add_alone);
                            }
                        }
                    }
                }
            }
            else
            {
                //TODO : REWORK THIS FOR UPDATED SYSTEM IN MAIN
                if (parent.AttachedDataset != null && parent.AttachedDataset.CellsByTimePoint != null)
                {
                    foreach (KeyValuePair<int, List<Cell>> cellsbytime in parent.AttachedDataset.CellsByTimePoint)
                    {
                        foreach (Cell cell in cellsbytime.Value)
                        {
                            if (cell.Infos != null && cell.Infos.ContainsKey(this.id_infos))
                            {
                                st.AppendLine(cell.getName() + ":" + cell.Infos[this.id_infos]);
                            }
                        }
                    }
                }

            }
            return st;
        }

        public int SetPosition(int Height)
        {
            parent.inScroll++;
            if (parent.inScroll <= parent.nbSCroll)
                inf.SetActive(false);  //OUT BEFORE
            else if ((parent.inScroll - parent.nbSCroll) * 22 > parent.MaxHeight)
                inf.SetActive(false);  //OUT AFTER
            else
                inf.SetActive(true);
            if (SubInfos.activeSelf)
                parent.inScroll++;
            inf.transform.localPosition = new Vector3(inf.transform.localPosition.x, -Height - 15, inf.transform.localPosition.z);
            int shiftY = 22;
            if (SubInfos.activeSelf)
                shiftY += 22;
            return shiftY;
        }

        public void unActive()
        {
            if (inf != null)
            {
                inf.transform.Find("load").gameObject.SetActive(true);
                inf.transform.Find("show").gameObject.SetActive(false);
                inf.transform.Find("show").gameObject.GetComponent<Button>().onClick.RemoveAllListeners();

                inf.transform.Find("InfosColor").gameObject.SetActive(false);
                inf.transform.Find("InfosColor").gameObject.GetComponent<Button>().onClick.RemoveAllListeners();

                inf.transform.Find("value").gameObject.SetActive(false);
                inf.transform.Find("deploy").gameObject.SetActive(false);
                inf.transform.Find("deploy").gameObject.GetComponent<Button>().onClick.RemoveAllListeners();

                SubInfos = inf.transform.Find("SubInfos").gameObject;
                SubInfos.SetActive(false);
                SubInfos.transform.Find("download").gameObject.SetActive(false);
                SubInfos.transform.Find("remove").gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
                SubInfos.transform.Find("cancel").gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
                SubInfos.transform.Find("onShare").gameObject.GetComponent<Toggle>().onValueChanged.RemoveAllListeners();

                //Curration
                if (Curations != null)
                {
                    Curations.Clear();
                }
                SubInfos.transform.Find("Curate").gameObject.SetActive(false);
                SubInfos.transform.Find("Curate").gameObject.GetComponent<Button>().onClick.RemoveAllListeners();

                IsActive = false;
            }
        }

        //After download the data, we active the buttons
        public void active()
        {
            if (datatype == "time")
            {
                foreach (Correspondence c in parent.Correspondences)
                {
                    if (c.id_infos != id_infos && c.datatype == "time" && id_infos > c.id_infos)
                    {
                        c.remove(id_infos);
                    }
                }
            }

            //FOR ALL
            IsActive = true;
            inf.transform.Find("load").gameObject.SetActive(false);
            inf.transform.Find("UploadDate").gameObject.SetActive(false);
            if (SetsManager.instance.userRight < 2 || id_owner == SetsManager.instance.IDUser || LoadParameters.instance.id_dataset == 0)
            { //ONLY FOR OWNER AND CREATOR
                inf.transform.Find("deploy").gameObject.SetActive(true);
                parent.showSubInfosListen(inf.transform.Find("deploy").gameObject.GetComponent<Button>(), this);
            }
            if (data_show == "color")
            { //COLOR
                inf.transform.Find("value").gameObject.SetActive(true);
                inf.transform.Find("show").gameObject.SetActive(true);
                inf.transform.Find("show").Find("Text").gameObject.GetComponent<Text>().text = "apply";
                parent.loadColorListen(inf.transform.Find("show").gameObject.GetComponent<Button>(), this, true);
            }
            else if (data_show == "string" || data_show == "text")
            { //STRING ...
                inf.transform.Find("value").gameObject.SetActive(true);
                parent.changeValueListen(inf.transform.Find("value").gameObject.GetComponent<Button>(), this);
                inf.transform.Find("show").gameObject.SetActive(true);
                parent.showStringListen(inf.transform.Find("show").gameObject.GetComponent<Button>(), this, true);
            }
            else if (data_show == "float" || data_show == "number")
            { //FLOAT
                inf.transform.Find("value").gameObject.SetActive(true);
                inf.transform.Find("InfosColor").gameObject.SetActive(true);
                parent.changeColorBarListen(inf.transform.Find("InfosColor").gameObject.GetComponent<Button>(), this);
                parent.changeColorMapListen(inf.transform.Find("InfosColor").gameObject.transform.Find("execute").gameObject.GetComponent<Button>(), this, false);
                parent.changeColorMapListen(inf.transform.Find("InfosColor").gameObject.transform.Find("plotexecute").gameObject.GetComponent<Button>(), this, true);
                inf.transform.Find("InfosColor").gameObject.GetComponentInChildren<Text>().text = "";
            }
            else if (data_show == "selection" || data_show == "label")
            { //SELECTION
                inf.transform.Find("value").gameObject.SetActive(true);
                inf.transform.Find("show").gameObject.SetActive(true);
                inf.transform.Find("show").Find("Text").gameObject.GetComponent<Text>().text = "apply";
                parent.applySelectionListen(inf.transform.Find("show").gameObject.GetComponent<Button>(), this);
            }
            else if (data_show == "sphere")
            { //SPHERE
                inf.transform.Find("value").gameObject.SetActive(false);
                inf.transform.Find("show").gameObject.SetActive(true);
                parent.applySphereListen(inf.transform.Find("show").gameObject.GetComponent<Button>(), this, true);
            }
            else if (data_show == "vector")
            { //VECTOR
                inf.transform.Find("value").gameObject.SetActive(false);
                inf.transform.Find("show").gameObject.SetActive(true);
                parent.applyVectorListen(inf.transform.Find("show").gameObject.GetComponent<Button>(), this, true);
            }
            else if (data_show == "line")
            { //VECTOR
                inf.transform.Find("value").gameObject.SetActive(false);
                inf.transform.Find("show").gameObject.SetActive(true);
                parent.applyLinesListen(inf.transform.Find("show").gameObject.GetComponent<Button>(), this, true);
            }
            else if (data_show == "arrow")
            { //VECTOR
                inf.transform.Find("value").gameObject.SetActive(false);
                inf.transform.Find("show").gameObject.SetActive(true);
                parent.applyArrowListen(inf.transform.Find("show").gameObject.GetComponent<Button>(), this, true);
            }
            else if (data_show == "dict")
            { //DICTIONNARY
                inf.transform.Find("value").gameObject.SetActive(false);
                inf.transform.Find("InfosColor").gameObject.SetActive(true);
                parent.changeColorBarListen(inf.transform.Find("InfosColor").gameObject.GetComponent<Button>(), this);
                parent.changeColorMapListen(inf.transform.Find("InfosColor").gameObject.transform.Find("execute").gameObject.GetComponent<Button>(), this);
                parent.changeColorMapListen(inf.transform.Find("InfosColor").gameObject.transform.Find("plotexecute").gameObject.GetComponent<Button>(), this);
                inf.transform.Find("InfosColor").gameObject.GetComponentInChildren<Text>().text = "";

                //neighbors test
                /*if (!InterfaceManager.instance.MenuClicked.gameObject.transform.Find("Bouton layout").gameObject.transform.Find("NeighborsSelect").gameObject.activeSelf)
                {
                    InterfaceManager.instance.MenuClicked.gameObject.transform.Find("Bouton layout").gameObject.transform.Find("NeighborsSelect").gameObject.SetActive(true);
                    InterfaceManager.instance.MenuClicked.transform.Find("Bouton layout").Find("RandomSelected").Find("Separator").gameObject.SetActive(true);
                    InterfaceManager.instance.MenuClicked.transform.Find("Background").GetComponent<Image>().fillAmount += 0.099f;
                }*/
            }
            else if (data_show == "genetic")
            { //DICTIONNARY
                inf.transform.Find("value").gameObject.SetActive(true);
                inf.transform.Find("InfosColor").gameObject.SetActive(false);
            }
            else if (data_show == "connection")
            {
                inf.transform.Find("show").gameObject.SetActive(true);
                parent.applyConnectionListen(inf.transform.Find("show").gameObject.GetComponent<Button>(), this, true);

                //neighbors test
                if (!InterfaceManager.instance.MenuClicked.gameObject.transform.Find("Bouton layout").gameObject.transform.Find("NeighborsSelect").gameObject.activeSelf)
                {
                    InterfaceManager.instance.MenuClicked.gameObject.transform.Find("Bouton layout").gameObject.transform.Find("NeighborsSelect").gameObject.SetActive(true);
                    //InterfaceManager.instance.MenuClicked.transform.Find("Bouton layout").Find("RandomSelected").Find("Separator").gameObject.SetActive(true);
                    InterfaceManager.instance.MenuClicked.transform.Find("Background").GetComponent<Image>().fillAmount += 0.099f;
                }
            }
            else
                MorphoDebug.Log("Non Treaded <" + data_show + "> : " + datatype);

            if ((type != 1 && data_show != "genetic") || datatype == "time")
            { //WE CANNOT REMOVE ORIGINAL LOADED DATA
                SubInfos.transform.Find("remove").gameObject.SetActive(true);
                parent.removeListen(SubInfos.transform.Find("remove").gameObject.GetComponent<Button>(), this);
            }
            else
                SubInfos.transform.Find("remove").gameObject.SetActive(false);

            if (common == 1)
                SubInfos.transform.Find("onShare").gameObject.GetComponent<Toggle>().isOn = true;
            else
                SubInfos.transform.Find("onShare").gameObject.GetComponent<Toggle>().isOn = false;

            if (SetsManager.instance.userRight <= 1 || LoadParameters.instance.id_dataset == 0)
            {
                //READER ACCESS CAN NOT DOWNLOAD DATA
                //Curration
                if (datatype != "selection" && datatype != "label")
                {
                    SubInfos.transform.Find("Curate").gameObject.SetActive(true);
                    parent.AttachedDataset.Curations.showCurationListen(SubInfos.transform.Find("Curate").gameObject.GetComponent<Button>(), this);
                }
            }

            if (LoadParameters.instance.id_dataset == 0 && datatype == "time")
            {
                SubInfos.transform.Find("Curate").gameObject.SetActive(false);
            }

            if (SetsManager.instance.userRight <= 1)
            {//ONLY CREATOR AND OWNER CAN DELETE AND SHARE DATA
                parent.StartCoroutine(parent.AttachedDataset.Curations.downloadCurations(this)); //Download all currations from db
                SubInfos.transform.Find("download").gameObject.SetActive(true);
                parent.downloadInfosListen(SubInfos.transform.Find("download").gameObject.GetComponent<Button>(), this);
                SubInfos.transform.Find("cancel").gameObject.SetActive(true);
                parent.cancelListen(SubInfos.transform.Find("cancel").gameObject.GetComponent<Button>(), this);
                //parent.deleteListen(SubInfos.transform.Find("cancel").gameObject.GetComponent<Button>(), this);
                SubInfos.transform.Find("onShare").gameObject.SetActive(true);
                parent.onShareListen(SubInfos.transform.Find("onShare").gameObject.GetComponent<Toggle>(), this);
            }
            else
            {
                SubInfos.transform.Find("cancel").gameObject.SetActive(false);
                SubInfos.transform.Find("onShare").gameObject.SetActive(false);
            }

            if (id_dataset == 0)
            { //FOR DIRECT PLOT
                SubInfos.transform.Find("remove").gameObject.SetActive(false);
                SubInfos.transform.Find("download").gameObject.SetActive(false);
                SubInfos.transform.Find("onShare").gameObject.SetActive(false);
                SubInfos.transform.Find("cancel").gameObject.SetActive(true);
                SubInfos.transform.Find("cancel").gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
                parent.cancelListen(SubInfos.transform.Find("cancel").gameObject.GetComponent<Button>(), this);
                //parent.deleteListen(SubInfos.transform.Find("cancel").gameObject.GetComponent<Button>(), this);
            }

            OnCorrespondenceActivation?.Invoke(this);
        }

        //We have to change the button if data type and nb cells change

        //We have to change the button if data type and nb cells change
        public void setButtons(int nbCells)
        {
            if (data_show != "selection" && data_show != "genetic" && datatype != "time" && data_show != "label")
            {
                if (SetsManager.instance.userRight <= 1 || LoadParameters.instance.id_dataset == 0)
                    SubInfos.transform.Find("Curate").gameObject.SetActive(true); //CURRATION
            }
        }

        public void showSubInfos()
        {
            SubInfos.SetActive(!SubInfos.activeSelf);
            parent.reOrganize();
        }

        //Load Color in load
        public void activeColorLPY()
        {
            inf.transform.Find("show").gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
            inf.transform.Find("show").gameObject.transform.Find("Text").gameObject.transform.GetComponent<Text>().text = "hide";
            parent.hideColorListen(inf.transform.Find("show").gameObject.GetComponent<Button>(), this, true);
        }

        //Load Uploaded Color
        public void loadColor(bool v)
        {
            inf.transform.Find("show").gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
            if (v)
            {
                if (DataSetInformations.instance.getCorrespondenceByName("simulation-color") != null && id_infos == DataSetInformations.instance.getCorrespondenceByName("simulation-color").id_infos)
                {
                    if (parent.AttachedDataset.PickedManager.clickedCells.Count == 0)
                    { //ALL CELLS
                        for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                        {
                            foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                                cell.LoadColorSelection(id_infos);
                        }
                    }
                    else
                    { //ONLY SELECTED CELLS
                        foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                            c.LoadColorSelection(id_infos);
                        parent.AttachedDataset.PickedManager.ClearSelectedCells();
                    }
                    inf.transform.Find("show").gameObject.transform.Find("Text").gameObject.transform.GetComponent<Text>().text = "hide";
                    parent.hideColorListen(inf.transform.Find("show").gameObject.GetComponent<Button>(), this, true);
                }
                else
                {
                    if (parent.AttachedDataset.PickedManager.clickedCells.Count == 0)
                    { //ALL CELLS
                        for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                        {
                            foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                                cell.loadColor(id_infos);
                        }
                    }
                    else
                    { //ONLY SELECTED CELLS
                        foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                            c.loadColor(id_infos);
                        parent.AttachedDataset.PickedManager.ClearSelectedCells();
                    }
                }
                inf.transform.Find("show").gameObject.transform.Find("Text").gameObject.transform.GetComponent<Text>().text = "hide";
                parent.hideColorListen(inf.transform.Find("show").gameObject.GetComponent<Button>(), this, true);
            }
            else
            {
                if (parent.AttachedDataset.PickedManager.clickedCells.Count == 0)
                { //ALL CELLS
                    for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                    {
                        foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                            cell.RemoveUploadColor();
                    }
                }
                else
                { //ONLY SELECTED CELLS
                    foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                        c.RemoveUploadColor();
                    parent.AttachedDataset.PickedManager.ClearSelectedCells();
                }
            }
        }

        //Hide Uploaded Color
        public void hideColor(bool v)
        {
            inf.transform.Find("show").gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
            if (v)
            {
                if (DataSetInformations.instance.getCorrespondenceByName("simulation-color") != null && id_infos == DataSetInformations.instance.getCorrespondenceByName("simulation-color").id_infos)
                {
                    if (parent.AttachedDataset.PickedManager.clickedCells.Count == 0)
                    { //ALL CELLS
                        for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                        {
                            foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                                cell.UnloadColorSelection(id_infos);
                        }
                    }
                    else
                    { //ONLY SELECTED CELLS
                        foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                            c.UnloadColorSelection(id_infos);
                        parent.AttachedDataset.PickedManager.ClearSelectedCells();
                    }
                    inf.transform.Find("show").gameObject.transform.Find("Text").gameObject.transform.GetComponent<Text>().text = "apply";
                    parent.loadColorListen(inf.transform.Find("show").gameObject.GetComponent<Button>(), this, true);
                }
                else
                {
                    if (parent.AttachedDataset.PickedManager.clickedCells.Count == 0)
                    { //ALL CELLS
                        for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                        {
                            foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                                cell.RemoveUploadColorInfo(id_infos);
                        }
                    }
                    else
                    { //ONLY SELECTED CELLS
                        foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                            c.RemoveUploadColorInfo(id_infos);
                        parent.AttachedDataset.PickedManager.ClearSelectedCells();
                    }
                    inf.transform.Find("show").gameObject.transform.Find("Text").gameObject.transform.GetComponent<Text>().text = "apply";
                    parent.loadColorListen(inf.transform.Find("show").gameObject.GetComponent<Button>(), this, true);
                }
            }
        }

        //Open the menu Color Map
        public void changeColorBar()
        {
            MenuColorBar.instance.buttonToColorize = inf.transform.Find("InfosColor").gameObject;
            int ctype = int.Parse(MenuColorBar.instance.buttonToColorize.transform.Find("type").gameObject.GetComponentInChildren<Text>().text);
            //Calcul Min Max Boudaries
            float MinV = MinInfos;
            float MaxV = MaxInfos;
            if (parent.AttachedDataset.PickedManager.clickedCells.Count > 0)
            {
                //ONLY ON SELECTED CELLS
                MinV = float.MaxValue;
                MaxV = float.MinValue;

                CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                ci.NumberFormat.CurrencyDecimalSeparator = ".";
                if (datatype == "dict")
                    foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                    {
                        float v;
                        string[] all_v = c.getInfos(id_infos).Split(';');
                        foreach (string vs in all_v)
                        {
                            string[] vz = vs.Split(':');
                            if (vz.Count() == 2)
                                if (float.TryParse(vz[1], NumberStyles.Any, ci, out v))
                                {
                                    if (v > MaxV)
                                        MaxV = v;
                                    if (v < MinV)
                                        MinV = v;
                                }
                        }
                    }
                else
                    foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                    {
                        float v;
                        if (float.TryParse(c.getInfos(id_infos), NumberStyles.Any, ci, out v))
                        {
                            if (v > MaxV)
                                MaxV = v;
                            if (v < MinV)
                                MinV = v;
                        }
                    }
            }
            else
            {
                //ALL CELLS
                MinV = float.MaxValue;
                MaxV = float.MinValue;

                CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                ci.NumberFormat.CurrencyDecimalSeparator = ".";
                if (datatype == "dict")
                {
                    for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                    {
                        foreach (Cell c in parent.AttachedDataset.CellsByTimePoint[t])
                        {
                            float v;
                            string[] all_v = c.getInfos(id_infos).Split(';');
                            foreach (string vs in all_v)
                            {
                                string[] vz = vs.Split(':');
                                if (vz.Count() == 2)
                                {
                                    if (float.TryParse(vz[1], NumberStyles.Any, ci, out v))
                                    {
                                        if (v > MaxV)
                                            MaxV = v;
                                        if (v < MinV)
                                            MinV = v;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    MinV = this.MinInfos;
                    MaxV = this.MaxInfos;
                }

                float savedmininfo = float.Parse(MenuColorBar.instance.buttonToColorize.transform.Find("minV").gameObject.GetComponentInChildren<Text>().text);
                float savedmaxinfo = float.Parse(MenuColorBar.instance.buttonToColorize.transform.Find("maxV").gameObject.GetComponentInChildren<Text>().text);
                if (!(savedmaxinfo == savedmininfo && savedmaxinfo == 0))
                {
                    MinV = savedmininfo;
                    MaxV = savedmaxinfo;
                }

                if (MenuColorBar.instance.infosName == name) // Retrieve previous thresholds if same property
                {
                    if (MenuColorBar.instance.MinV < MinV)
                    {
                        MinV = MenuColorBar.instance.MinV;
                    }
                    if (MenuColorBar.instance.MaxV > MaxV)
                    {
                        MaxV = MenuColorBar.instance.MaxV;
                    }
                }
                    

            }

            


            //First we have to check if
            if (InterfaceManager.instance.MenuColorBar.activeSelf)
            {
                MenuColorBar.instance.cancel();
            }

            /*if (float.Parse(MenuColorBar.instance.buttonToColorize.transform.Find("minV").gameObject.GetComponentInChildren<Text>().text) < MinV)
                MenuColorBar.instance.buttonToColorize.transform.Find("minV").gameObject.GetComponentInChildren<Text>().text = MinV.ToString();
            if (float.Parse(MenuColorBar.instance.buttonToColorize.transform.Find("maxV").gameObject.GetComponentInChildren<Text>().text) > MaxV)
                MenuColorBar.instance.buttonToColorize.transform.Find("maxV").gameObject.GetComponentInChildren<Text>().text = MaxV.ToString();*/
            int min_c = int.Parse(MenuColorBar.instance.buttonToColorize.transform.Find("min").gameObject.GetComponentInChildren<Text>().text);
            int max_c = int.Parse(MenuColorBar.instance.buttonToColorize.transform.Find("max").gameObject.GetComponentInChildren<Text>().text);

            float minimum = MinInfos;
            float maximum = MaxInfos;
            float min_v_other = float.Parse(MenuColorBar.instance.buttonToColorize.transform.Find("minVOther").gameObject.GetComponentInChildren<Text>().text);
            float max_v_other = float.Parse(MenuColorBar.instance.buttonToColorize.transform.Find("maxVOther").gameObject.GetComponentInChildren<Text>().text);

            string param = inf.transform.Find("InfosColor").gameObject.transform.Find("parameter").gameObject.GetComponentInChildren<Text>().text;

            //replace menucolorbar appropriately:
            if (parent.AttachedDataset.Infos.MenuBarColor.activeSelf)
            {
                parent.AttachedDataset.Infos.MenuBarColor.SetActive(false);
                parent.AttachedDataset.Infos.MenuBarColor.transform.SetParent(InterfaceManager.instance.canvas.transform, false);
            }
            else
            {
                parent.AttachedDataset.Infos.MenuBarColor.SetActive(true);
                parent.AttachedDataset.Infos.MenuBarColor.transform.SetParent(InterfaceManager.instance.MenuInfos.transform, false);
            }

            MenuColorBar.instance.AssignValues(ctype, min_c, max_c, MinV, MaxV, name, minimum, maximum, min_v_other, max_v_other, param);
            InterfaceManager.instance.MenuColorBar.SetActive(true);
        }

        public void ChangeColorMap(bool force = false)
        {
            GameObject buttonToColorize = inf.transform.Find("InfosColor").gameObject;
            string parameter = buttonToColorize.transform.Find("parameter").gameObject.GetComponentInChildren<Text>().text;
            string lastParameter = buttonToColorize.transform.Find("lastParameter").gameObject.GetComponentInChildren<Text>().text;
            int typecmap = int.Parse(buttonToColorize.transform.Find("type").gameObject.GetComponentInChildren<Text>().text);
            int minC = int.Parse(buttonToColorize.transform.Find("min").gameObject.GetComponentInChildren<Text>().text);
            int maxC = int.Parse(buttonToColorize.transform.Find("max").gameObject.GetComponentInChildren<Text>().text);
            float minVal = MenuColorBar.instance.GetMinVal();
            float maxVal = MenuColorBar.instance.GetMaxVal();
            float maxV = MenuColorBar.instance.GetMaxV();
            float minV = MenuColorBar.instance.GetMinV();
            float minVOther = MenuColorBar.instance.GetMinVOther();
            float maxVOther = MenuColorBar.instance.GetMaxVOther();
            float minThresh = MenuColorBar.instance.GetMinThresholdValue();
            float maxThresh = MenuColorBar.instance.GetMaxThresholdValue();

            if (parameter != lastParameter && lastParameter != "")
            {
                switch (lastParameter)
                {
                    case "Main Color":
                        CancelMainCmap();
                        break;

                    case "Halo Color":
                        CancelHaloCmap();
                        break;

                    case "Second Color":
                        CancelSecondCmap();
                        break;

                    case "Metallic":
                        CancelMetallicMap();
                        break;

                    case "Smoothness":
                        CancelSmoothnessMap();
                        break;

                    case "Transparency":
                        CancelTransparencyMap();
                        break;

                    default:
                        break;
                }
            }

            switch (parameter)
            {
                case "Main Color":
                    if (typecmap >= 0)
                    {
                        CancelSecondCmap();
                        ApplyMainCmap(typecmap, minC, maxC, minV, maxV, this, force);
                    }
                    else
                        CancelMainCmap();
                    break;

                case "Halo Color":
                    if (typecmap >= 0)
                    {
                        CancelSecondCmap();
                        ApplyHaloCmap(typecmap, minC, maxC, minV, maxV, this, force);
                    }
                    else
                        CancelHaloCmap();
                    break;

                case "Second Color":
                    if (typecmap >= 0)
                    {
                        ApplySecondCmap(typecmap, minC, maxC, minV, maxV, this, force);
                    }
                    else
                        CancelSecondCmap();
                    break;

                case "Metallic":
                    CancelSecondCmap();
                    ApplyMetallicMap(minV, maxV, minVOther, maxVOther, this, force);
                    break;

                case "Smoothness":
                    CancelSecondCmap();
                    ApplySmoothnessMap(minV, maxV, minVOther, maxVOther, this, force);
                    break;

                case "Transparency":
                    CancelSecondCmap();
                    ApplyTransparencyMap(minV, maxV, minVOther, maxVOther, this, force);
                    break;

                default:
                    break;
            }
        }

        public void ApplyMainCmap(int typecmap, int minC, int maxC, float minV, float maxV, Correspondence cor, bool force=false)
        {
            List<Cell> cell_lineage = new List<Cell>();
            bool thresh = MenuColorBar.instance.ThresholdToggle.isOn;
            List<Cell> cells = new List<Cell>(parent.AttachedDataset.PickedManager.clickedCells);
            //MorphoTools.ResetLineageUpdatingCells();
            bool lineage_all = false;
            if (parent.AttachedDataset.PickedManager.clickedCells.Count == 0 || force)
            { //ALL CELLS
                lineage_all = true;
                for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                {
                    List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                    foreach (Cell cell in CellAtT)
                    {
                        float v;

                        if (float.TryParse(cell.getInfos(id_infos), out v))
                        {
                            Color col = MenuColorBar.instance.GetColor(typecmap, v, minC, maxC, minV, maxV, thresh);
                            cell.UploadMainColor(col);
                            cell_lineage.Add(cell);
                            if (cell.t == MorphoTools.GetDataset().CurrentTime)
                            {
                                if (MenuColorBar.instance.select_object_in_value_range)
                                {
                                    if (v >= minV && v <= maxV){
                                        MorphoTools.GetPickedManager().AddCellSelected(cell);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (SetsManager.instance.directPlot != null && SetsManager.instance.directPlot.type == "lpy")
                            {
                                cell.UploadMainColor(new Color(1f, 1f, 1f, 1.0f));
                                cell_lineage.Add(cell);
                            }
                        }
                    }
                }
            }
            else
            { //ONLY SELECTED CELLS
                if (datatype == "dict")
                {
                    lineage_all = false;
                    foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                    {
                        float v;
                        string[] all_v = c.getInfos(id_infos).Split(';');
                        foreach (string vs in all_v)
                        {
                            string[] vz = vs.Split(':');
                            if (vz.Count() == 2)
                                if (float.TryParse(vz[1],NumberStyles.Float,CultureInfo.InvariantCulture, out v))
                                {
                                    Color col = MenuColorBar.instance.GetColor(typecmap, v, minC, maxC, minV, maxV, thresh);
                                    Cell n2 = parent.AttachedDataset.getCell(vz[0], false);
                                    if (n2 != null)
                                    {
                                        n2.UploadMainColor(col);
                                        cell_lineage.Add(n2);
                                        //MorphoTools.AddCellToLineageCellsn2);
                                    }
                                    
                                }
                        }
                    }
                }
                else
                {
                    lineage_all = false;
                    foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                    {
                        float v;
                        if (float.TryParse(c.getInfos(id_infos), out v))
                        {
                            Color col = MenuColorBar.instance.GetColor(typecmap, v, minC, maxC, minV, maxV, thresh);
                            c.UploadMainColor(col);
                            cell_lineage.Add(c);
                        }
                        else
                        {
                            if (SetsManager.instance.directPlot != null && SetsManager.instance.directPlot.type == "lpy")
                            {
                                c.UploadMainColor(new Color(1f, 1f, 1f, 1.0f));
                                cell_lineage.Add(c);
                            }
                        }
                    }
                }
                parent.AttachedDataset.PickedManager.ClearSelectedCells();
            }
#if UNITY_WEBGL
		if (Lineage.isLineage) { Lineage.applyColormapInfo(cor, typecmap, minC, maxC, minV, maxV, lineage_all,cell_lineage); }
#else
            MorphoTools.SendInfoToLineage(this.id_infos);
            MorphoTools.SendCellsColorToLineage();

#endif
            //update selected cells interface
            if (MenuColorBar.instance.select_object_in_value_range)
            {
                if (MorphoTools.GetDataset().Infos != null)
                    MorphoTools.GetDataset().Infos.describe();
            }

        }

        public void ApplyHaloCmap(int typecmap, int minC, int maxC, float minV, float maxV, Correspondence cor, bool force=false)
        {
            List<Cell> cell_lineage = new List<Cell>();
            bool thresh = MenuColorBar.instance.ThresholdToggle.isOn;
            //MorphoTools.ResetLineageUpdatingCells();
            bool lineage_all = false;
            if (parent.AttachedDataset.PickedManager.clickedCells.Count == 0 || force)
            { //ALL CELLS
                lineage_all = true;
                for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                {
                    List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                    foreach (Cell cell in CellAtT)
                    {
                        float v;
                        if (float.TryParse(cell.getInfos(id_infos), out v))
                        {
                            Color col = MenuColorBar.instance.GetColor(typecmap, v, minC, maxC, minV, maxV, thresh, true);
                            cell.UploadHaloColor(col);
                            cell_lineage.Add(cell);
                        }
                    }
                }
            }
            else
            { //ONLY SELECTED CELLS
                if (datatype == "dict")
                {
                    lineage_all = false;

                    foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                    {
                        float v;
                        string[] all_v = c.getInfos(id_infos).Split(';');
                        foreach (string vs in all_v)
                        {
                            string[] vz = vs.Split(':');
                            if (vz.Count() == 2)
                                if (float.TryParse(vz[1], NumberStyles.Float, CultureInfo.InvariantCulture, out v))
                                {
                                    Color col = MenuColorBar.instance.GetColor(typecmap, v, minC, maxC, minV, maxV, thresh, true);
                                    Cell n2 = parent.AttachedDataset.getCell(vz[0], false);
                                    if (n2 != null)
                                    {
                                        n2.UploadHaloColor(col);
                                        cell_lineage.Add(n2);
                                        //MorphoTools.AddCellToLineageCellsn2);
                                    }

                                }
                        }
                    }
                }
                else
                {
                    lineage_all = false;
                    foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                    {
                        float v;
                        if (float.TryParse(c.getInfos(id_infos), out v))
                        {
                            Color col = MenuColorBar.instance.GetColor(typecmap, v, minC, maxC, minV, maxV, thresh, true);
                            c.UploadHaloColor(col);
                            cell_lineage.Add(c);
                        }
                    }
                }
                parent.AttachedDataset.PickedManager.ClearSelectedCells();
            }
#if UNITY_WEBGL
		if (Lineage.isLineage) { Lineage.applyColormapInfo(cor, typecmap, minC, maxC, minV, maxV, lineage_all,cell_lineage) ; }
#else
            MorphoTools.SendInfoToLineage(this.id_infos);
            MorphoTools.SendCellsColorToLineage();

#endif
        }

        public void ApplySecondCmap(int typecmap, int minC, int maxC, float minV, float maxV, Correspondence cor, bool force=false)
        {
            List<Cell> cell_lineage = new List<Cell>();
            bool thresh = MenuColorBar.instance.ThresholdToggle.isOn;
            //MorphoTools.ResetLineageUpdatingCells();
            bool lineage_all = false;
            if (parent.AttachedDataset.PickedManager.clickedCells.Count == 0 || force)
            { //ALL CELLS
                lineage_all = true;
                for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                {
                    List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                    foreach (Cell cell in CellAtT)
                    {
                        float v;
                        if (float.TryParse(cell.getInfos(id_infos), out v))
                        {
                            Color col = MenuColorBar.instance.GetColor(typecmap, v, minC, maxC, minV, maxV, thresh);
                            cell.UploadSecondColor(col);
                            cell_lineage.Add(cell);
                        }
                    }
                }
            }
            else
            { //ONLY SELECTED CELLS
                if (datatype == "dict")
                {
                    lineage_all = false;

                    foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                    {
                        float v;
                        string[] all_v = c.getInfos(id_infos).Split(';');
                        foreach (string vs in all_v)
                        {
                            string[] vz = vs.Split(':');
                            if (vz.Count() == 2)
                                if (float.TryParse(vz[1], NumberStyles.Float, CultureInfo.InvariantCulture, out v))
                                {
                                    Color col = MenuColorBar.instance.GetColor(typecmap, v, minC, maxC, minV, maxV, thresh);
                                    Cell n2 = parent.AttachedDataset.getCell(vz[0], false);
                                    if (n2 != null)
                                    {
                                        n2.UploadSecondColor(col);
                                        cell_lineage.Add(n2);
                                        //MorphoTools.AddCellToLineageCellsn2);
                                    }

                                }
                        }
                    }
                }
                else
                {
                    lineage_all = false;
                    foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                    {
                        float v;
                        if (float.TryParse(c.getInfos(id_infos), out v))
                        {
                            Color col = MenuColorBar.instance.GetColor(typecmap, v, minC, maxC, minV, maxV, thresh);
                            c.UploadSecondColor(col);
                            cell_lineage.Add(c);
                        }
                    }
                }
                parent.AttachedDataset.PickedManager.ClearSelectedCells();
            }
#if UNITY_WEBGL
		if (Lineage.isLineage) { Lineage.applyColormapInfo(cor, typecmap, minC, maxC, minV, maxV, lineage_all,cell_lineage) ; }
#else
            MorphoTools.SendInfoToLineage(this.id_infos);
            MorphoTools.SendCellsColorToLineage();

#endif
        }

        public void ApplyMetallicMap(float minV, float maxV, float minVOther, float maxVOther, Correspondence cor, bool force=false)
        {
            bool thresh = MenuColorBar.instance.ThresholdToggle.isOn;
            //MorphoTools.ResetLineageUpdatingCells();
            bool lineage_all = false;
            if (parent.AttachedDataset.PickedManager.clickedCells.Count == 0 || force)
            { //ALL CELLS
                lineage_all = true;
                for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                {
                    List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                    foreach (Cell cell in CellAtT)
                    {
                        float v;
                        if (float.TryParse(cell.getInfos(id_infos), out v))
                        {
                            float val = minVOther + (maxVOther - minVOther) * (v - minV) / (maxV - minV);
                            if (thresh)
                                if (v > maxV || v < minV)
                                    val = 0f;
                            cell.UploadMetallic(val);
                        }
                    }
                }
            }
            else
            { //ONLY SELECTED CELLS
                if (datatype == "dict")
                {
                    lineage_all = false;

                    foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                    {
                        float v;
                        string[] all_v = c.getInfos(id_infos).Split(';');
                        foreach (string vs in all_v)
                        {
                            string[] vz = vs.Split(':');
                            if (vz.Count() == 2)
                                if (float.TryParse(vz[1], NumberStyles.Float, CultureInfo.InvariantCulture, out v))
                                {
                                    float val = minVOther + (maxVOther - minVOther) * (v - minV) / (maxV - minV);
                                    Cell n2 = parent.AttachedDataset.getCell(vz[0], false);
                                    if (thresh)
                                        if (v > maxV || v < minV)
                                            val = 0f;
                                    if (n2 != null)
                                    {
                                        n2.UploadMetallic(val);
                                        //MorphoTools.AddCellToLineageCellsn2);
                                    }

                                }
                        }
                    }
                }
                else
                {
                    lineage_all = false;
                    foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                    {
                        float v;
                        if (float.TryParse(c.getInfos(id_infos), out v))
                        {
                            float val = minVOther + (maxVOther - minVOther) * (v - minV) / (maxV - minV);
                            if (thresh)
                                if (v > maxV || v < minV)
                                    val = 0f;
                            c.UploadMetallic(val);
                        }
                    }
                }
                parent.AttachedDataset.PickedManager.ClearSelectedCells();
            }
#if UNITY_WEBGL
		//if (Lineage.isLineage) { Lineage.applyColormapInfo(cor, typecmap, minC, maxC, minV, maxV, lineage_all,MorphoTools.GetLineageUpdatingCells()) ; }
#else
            MorphoTools.SendInfoToLineage(this.id_infos);
            MorphoTools.SendCellsColorToLineage();

#endif
        }

        public void ApplySmoothnessMap(float minV, float maxV, float minVOther, float maxVOther, Correspondence cor, bool force=false)
        {
            bool thresh = MenuColorBar.instance.ThresholdToggle.isOn;
            List<Cell> cells = new List<Cell>(parent.AttachedDataset.PickedManager.clickedCells);
            //MorphoTools.ResetLineageUpdatingCells();
            bool lineage_all = false;
            if (parent.AttachedDataset.PickedManager.clickedCells.Count == 0 || force)
            { //ALL CELLS
                lineage_all = true;
                for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                {
                    List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                    foreach (Cell cell in CellAtT)
                    {
                        float v;
                        if (float.TryParse(cell.getInfos(id_infos), out v))
                        {
                            float val = minVOther + (maxVOther - minVOther) * (v - minV) / (maxV - minV);
                            if (thresh)
                                if (v > maxV || v < minV)
                                    val = 0f;
                            cell.UploadSmoothness(val);
                        }
                    }
                }
            }
            else
            { //ONLY SELECTED CELLS
                if (datatype == "dict")
                {
                    lineage_all = false;

                    foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                    {
                        float v;
                        string[] all_v = c.getInfos(id_infos).Split(';');
                        foreach (string vs in all_v)
                        {
                            string[] vz = vs.Split(':');
                            if (vz.Count() == 2)
                                if (float.TryParse(vz[1], NumberStyles.Float, CultureInfo.InvariantCulture, out v))
                                {
                                    float val = minVOther + (maxVOther - minVOther) * (v - minV) / (maxV - minV);
                                    Cell n2 = parent.AttachedDataset.getCell(vz[0], false);
                                    if (thresh)
                                        if (v > maxV || v < minV)
                                            val = 0f;
                                    if (n2 != null)
                                    {
                                        n2.UploadSmoothness(val);
                                        //MorphoTools.AddCellToLineageCellsn2);
                                    }

                                }
                        }
                    }
                }
                else
                {
                    lineage_all = false;
                    foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                    {
                        float v;
                        if (float.TryParse(c.getInfos(id_infos), out v))
                        {
                            float val = minVOther + (maxVOther - minVOther) * (v - minV) / (maxV - minV);
                            if (thresh)
                                if (v > maxV || v < minV)
                                    val = 0f;
                            c.UploadSmoothness(val);
                        }
                    }
                }
                parent.AttachedDataset.PickedManager.ClearSelectedCells();
            }
#if UNITY_WEBGL
		//if (Lineage.isLineage) { Lineage.applyColormapInfo(cor, typecmap, minC, maxC, minV, maxV, lineage_all,MorphoTools.GetLineageUpdatingCells()) ; }
#else
            MorphoTools.SendInfoToLineage(this.id_infos);
            MorphoTools.SendCellsColorToLineage();

#endif
        }

        public void ApplyTransparencyMap(float minV, float maxV, float minVOther, float maxVOther, Correspondence cor, bool force=false)
        {
            bool thresh = MenuColorBar.instance.ThresholdToggle.isOn;
            //MorphoTools.ResetLineageUpdatingCells();
            bool lineage_all = false;
            if (parent.AttachedDataset.PickedManager.clickedCells.Count == 0 || force)
            { //ALL CELLS
                lineage_all = true;
                for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                {
                    List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                    foreach (Cell cell in CellAtT)
                    {
                        float v;
                        if (float.TryParse(cell.getInfos(id_infos), out v))
                        {
                            float val = minVOther + (maxVOther - minVOther) * (v - minV) / (maxV - minV);
                            if (thresh)
                                if (v > maxV || v < minV)
                                    val = 0f;
                            cell.UploadTransparency(val);
                        }
                    }
                }
            }
            else
            { //ONLY SELECTED CELLS
                if (datatype == "dict")
                {
                    lineage_all = false;

                    foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                    {
                        float v;
                        string[] all_v = c.getInfos(id_infos).Split(';');
                        foreach (string vs in all_v)
                        {
                            string[] vz = vs.Split(':');
                            if (vz.Count() == 2)
                                if (float.TryParse(vz[1], NumberStyles.Float, CultureInfo.InvariantCulture, out v))
                                {
                                    float val = minVOther + (maxVOther - minVOther) * (v - minV) / (maxV - minV);
                                    Cell n2 = parent.AttachedDataset.getCell(vz[0], false);
                                    if (thresh)
                                        if (v > maxV || v < minV)
                                            val = 0f;
                                    if (n2 != null)
                                    {
                                        n2.UploadTransparency(val);
                                        //MorphoTools.AddCellToLineageCellsn2);
                                    }

                                }
                        }
                    }
                }
                else
                {
                    lineage_all = false;
                    foreach (Cell c in parent.AttachedDataset.PickedManager.clickedCells)
                    {
                        float v;
                        if (float.TryParse(c.getInfos(id_infos), out v))
                        {
                            float val = minVOther + (maxVOther - minVOther) * (v - minV) / (maxV - minV);
                            if (thresh)
                                if (v > maxV || v < minV)
                                    val = 0f;
                            c.UploadTransparency(val);
                        }
                    }
                }
                parent.AttachedDataset.PickedManager.ClearSelectedCells();
            }
#if UNITY_WEBGL
		//if (Lineage.isLineage) { Lineage.applyColormapInfo(cor, typecmap, minC, maxC, minV, maxV, lineage_all,MorphoTools.GetLineageUpdatingCells()) ; }
#else
            MorphoTools.SendInfoToLineage(this.id_infos);
            MorphoTools.SendCellsColorToLineage();

#endif
        }

        //Reset Upload Color
        public void CancelCmap()
        {
            for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
            {
                List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                foreach (Cell cell in CellAtT)
                {
                    cell.UploadedColor = false;
                    cell.updateShader();
                }
            }
#if !UNITY_WEBGL
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        //Reset Upload Main Color
        public void CancelMainCmap()
        {
            for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
            {
                List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                foreach (Cell cell in CellAtT)
                {
                    cell.UploadMainColor(Color.white);
                    cell.updateShader();
                }
            }

#if !UNITY_WEBGL
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        //Reset Upload Halo Color
        public void CancelHaloCmap()
        {
            for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
            {
                List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                foreach (Cell cell in CellAtT)
                {
                    cell.UploadedHaloColor = false;
                    cell.updateShader();
                }
            }
#if !UNITY_WEBGL
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        //Reset Upload Second Color
        public void CancelSecondCmap()
        {
            for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
            {
                List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                foreach (Cell cell in CellAtT)
                {
                    cell.UploadedSecondColor = false;
                    cell.updateShader();
                }
            }
#if !UNITY_WEBGL
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        //Reset Upload Metallic
        public void CancelMetallicMap()
        {
            for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
            {
                List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                foreach (Cell cell in CellAtT)
                {
                    cell.UploadedMetallicParameter = false;
                    cell.updateShader();
                }
            }
#if !UNITY_WEBGL
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        //Reset Upload Smoothness
        public void CancelSmoothnessMap()
        {
            for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
            {
                List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                foreach (Cell cell in CellAtT)
                {
                    cell.UploadedSmoothnessParameter = false;
                    cell.updateShader();
                }
            }
#if !UNITY_WEBGL
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        //Reset Upload Transparency
        public void CancelTransparencyMap()
        {
            for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
            {
                List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                foreach (Cell cell in CellAtT)
                {
                    cell.UploadedTransparencyParameter = false;
                    cell.updateShader();
                }
            }
#if !UNITY_WEBGL
            MorphoTools.SendCellsColorToLineage();
#endif

        }

        //When we click on a value in the infos we want to change it
        public void changeValue()
        {
            if (parent.AttachedDataset.PickedManager.clickedCells.Count == 1)
            {
                GameObject nv = inf.transform.Find("newvalue").gameObject;
                nv.SetActive(true);
                nv.GetComponent<InputField>().Select();
                nv.GetComponent<InputField>().ActivateInputField();
                nv.GetComponent<InputField>().text = parent.AttachedDataset.PickedManager.clickedCells[0].getInfos(id_infos);
            }
        }

        //When we click on button show for s string (on cell)
        public void showString(bool v)
        {
            MorphoTools.SendInfoToLineage(this.id_infos);
            isValueShow = v;
            GameObject show = inf.transform.Find("show").gameObject;
            show.GetComponent<Button>().onClick.RemoveAllListeners();
            parent.showStringListen(show.GetComponent<Button>(), this, !v);
            if (v)
                show.transform.Find("Text").gameObject.transform.GetComponent<Text>().text = "hide";
            else
                show.transform.Find("Text").gameObject.transform.GetComponent<Text>().text = "show";
            if (v)
                parent.NbStringOn++;
            else
                parent.NbStringOn--;
        }

        public List<GameObject> displayed_text;

        public void LoadShowAll()
        {
            if (show_all)
            {
                if (displayed_text == null)
                {
                    displayed_text = new List<GameObject>();
                }
                foreach (GameObject b in displayed_text)
                {
                    GameObject.Destroy(b);
                }
                displayed_text = new List<GameObject>();
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[MorphoTools.GetDataset().CurrentTime])
                {
                    if (cell.Infos != null && cell.Infos.Count > 0 && cell.Infos.ContainsKey(id_infos))
                    {
                        GameObject g = GameObject.Instantiate(MorphoTools.GetDataset().goShowString);
                        string stringToShow = cell.Infos[id_infos];
                        g.GetComponent<TextMesh>().text = stringToShow;

                        //correction transposer cell to screen puis screen to world avec correction
                        Vector3 screenPoint = Camera.main.WorldToScreenPoint(cell.getFirstChannel().AssociatedCellObject.transform.position);
                        Vector3 finalPoint = Camera.main.ScreenToWorldPoint(screenPoint);
                        g.transform.localPosition = finalPoint;
                        g.transform.localScale = new Vector3(.1f, .1f, .1f);
                        g.GetComponent<Renderer>().enabled = true;
                        displayed_text.Add(g);
                    }
                }
            }
        }

        //SPHERE
        public void applySphere(bool v)
        {
            GameObject show = inf.transform.Find("show").gameObject;
            show.GetComponent<Button>().onClick.RemoveAllListeners();
            parent.applySphereListen(show.GetComponent<Button>(), this, !v);
            if (v)
                show.transform.Find("Text").gameObject.transform.GetComponent<Text>().text = "hide";
            else
                show.transform.Find("Text").gameObject.transform.GetComponent<Text>().text = "show";
            if (v)
                for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                {
                    List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                    foreach (Cell cell in CellAtT)
                    {
                        string info = cell.getInfos(id_infos);
                        if (info != "")
                            cell.loadSphere(id_infos, info);
                    }
                }
            else
                for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                {
                    List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                    foreach (Cell cell in CellAtT)
                    {
                        string info = cell.getInfos(id_infos);
                        if (info != "")
                            cell.removeObject(id_infos);
                    }
                }
        }

        //VECTOR
        public void applyVector(bool v)
        {
            GameObject show = inf.transform.Find("show").gameObject;
            show.GetComponent<Button>().onClick.RemoveAllListeners();
            parent.applyVectorListen(show.GetComponent<Button>(), this, !v);
            if (v)
                show.transform.Find("Text").gameObject.transform.GetComponent<Text>().text = "hide";
            else
                show.transform.Find("Text").gameObject.transform.GetComponent<Text>().text = "show";
            if (v)
                for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                {
                    List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                    foreach (Cell cell in CellAtT)
                    {
                        string info = cell.getInfos(id_infos);
                        if (info != "")
                        {
                            cell.loadVector(id_infos, info);
                        }
                    }
                }
            else
                for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                {
                    List<Cell> CellAtT = parent.AttachedDataset.CellsByTimePoint[t];
                    foreach (Cell cell in CellAtT)
                    {
                        string info = cell.getInfos(id_infos);
                        if (info != "")
                            cell.removeObject(id_infos);
                    }
                }
        }

        //VECTOR
        public void applyLines(bool v)
        {
            GameObject show = inf.transform.Find("show").gameObject;
            show.GetComponent<Button>().onClick.RemoveAllListeners();
            parent.applyLinesListen(show.GetComponent<Button>(), this, !v);
            if (v)
                show.transform.Find("Text").gameObject.transform.GetComponent<Text>().text = "hide";
            else
                show.transform.Find("Text").gameObject.transform.GetComponent<Text>().text = "show";
            if (v)
                for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                {
                    foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                    {
                        string info = cell.getInfos(id_infos);
                        if (info != "")
                        {
                            cell.loadLines(id_infos, info);
                        }
                    }
                }
            else
                for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                {
                    foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                    {
                        string info = cell.getInfos(id_infos);
                        if (info != "")
                            cell.removeObject(id_infos);

                        GizmoManager.instance.ClearLines();
                    }
                }
        }

        //VECTOR
        public void applyArrow(bool v)
        {
            GameObject show = inf.transform.Find("show").gameObject;
            show.GetComponent<Button>().onClick.RemoveAllListeners();
            parent.applyArrowListen(show.GetComponent<Button>(), this, !v);
            if (v)
                show.transform.Find("Text").gameObject.transform.GetComponent<Text>().text = "hide";
            else
                show.transform.Find("Text").gameObject.transform.GetComponent<Text>().text = "show";
            if (v)
                for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                {
                    foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                    {
                        string info = cell.getInfos(id_infos);
                        if (info != "")
                            cell.loadArrow(id_infos, info);
                    }
                }
            else
                for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                {
                    foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                    {
                        string info = cell.getInfos(id_infos);
                        if (info != "")
                            cell.removeObject(id_infos);

                        GizmoManager.instance.ClearVectors();
                    }
                }
        }

        //CONNECTIONS
        public void applyConnection(bool v)
        {
            if (v)
            {
                applyConnection(!v);

                MorphoTools.GetDataset().Scatter.OnScatterReset();
                ResetConnections();

                for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                {
                    foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                    {
                        if (cell.selected || MorphoTools.GetDataset().PickedManager.clickedCells.Count == 0)
                        {
                            float min = InterfaceManager.instance.ConnectionMinThreshold.value;
                            float max = InterfaceManager.instance.ConnectionMaxThreshold.value;
                            string info = cell.getInfos(id_infos);
                            if (info != "")
                                foreach (string i in info.Split(';'))
                                    cell.AddConnection(id_infos, i, min, max);
                        }
                    }
                }

                MorphoTools.GetDataset().Scatter.UpdateInfoConnectionPosition();
            }
            else
                for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                {
                    foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                    {
                        string info = cell.getInfos(id_infos);
                        if (info != "")
                            cell.removeConnection(id_infos);
                    }
                }
        }

        public void ResetConnections()
        {
            DataSet dataSet = MorphoTools.GetDataset();
            if (dataSet.InfoConnectionParent != null)
            {
                if (dataSet.InfoConnectionParent.transform.childCount > 0)//if already, reapply them all ?
                {
                    List<GameObject> ToRestart = new List<GameObject>();
                    foreach (Transform g in dataSet.InfoConnectionParent.transform)
                    {
                        g.SetParent(dataSet.InfoConnectionParent.transform.parent);
                        ToRestart.Add(g.gameObject);
                    }
                    GameObject.DestroyImmediate(dataSet.InfoConnectionParent);
                    dataSet.InfoConnectionParent = new GameObject("Connection_Parent");

                    dataSet.InfoConnectionParent.transform.parent = dataSet.GetTimepointAt(dataSet.CurrentTime).Pivot;
                    foreach (GameObject g in ToRestart)
                    {
                        g.transform.SetParent(dataSet.InfoConnectionParent.transform);
                        LineRenderer lr = g.GetComponent<LineRenderer>();
                        ConnectionInfo ci = g.GetComponent<ConnectionInfo>();

                        Vector3 originpos = dataSet.InfoConnectionParent.transform.InverseTransformPoint(ci.Source.transform.TransformPoint(ci.SCh.MeshGravity));

                        Vector3 destpos = dataSet.InfoConnectionParent.transform.InverseTransformPoint(ci.Destination.transform.TransformPoint(ci.DCh.MeshGravity));
                        if (Double.IsNaN(originpos.x) || Double.IsNaN(originpos.y) || Double.IsNaN(originpos.z) || Double.IsNaN(destpos.x) || Double.IsNaN(destpos.y) || Double.IsNaN(destpos.z))
                        {
                            lr.SetPosition(0, originpos);
                            lr.SetPosition(1, destpos);
                        }
                    }
                }
            }
        }

        // !!
        //Donwload text infos file in a new window
        public IEnumerator downloadInfos()
        {
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("download", "7");
            form.AddField("id_dataset", parent.AttachedDataset.id_dataset.ToString());
            form.AddField("id_infos", id_infos.ToString());

            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/correspondencefilename/?hash=" + SetsManager.instance.hash + "&id_dataset=" + parent.AttachedDataset.id_dataset.ToString() + "&id_infos=" + id_infos.ToString());

            yield return www.SendWebRequest();

            if (www.isHttpError || www.isNetworkError)
            {
                MorphoDebug.Log("Error : " + www.error);
            }
            else
            {
                if (www.downloadHandler.text != "")
                {
                    string urlinfos = MorphoTools.GetServer() + www.downloadHandler.text;
#if UNITY_WEBGL
				Recording.openWindow(urlinfos);
#else
                    MorphoDebug.Log("OPEN " + urlinfos);
                    if (urlinfos.StartsWith(MORPHONET_URL_START))
                    {
                        Application.OpenURL(urlinfos);
                    }
#endif
                }
            }
            www.Dispose();
            yield return null;
        }

        public IEnumerator cancel()
        {
            remove();
            if (LoadParameters.instance.id_dataset == 0)
            {
                SetsManager.instance.directPlot.DeleteInformation(name);

                ct.Correspondences.Remove(this);
                parent.Correspondences.Remove(this);
                SetsManager.instance.DestroyGO(inf);
                parent.deleteCorrespondence(this, true);
                
            }
            else
            {
                //delete correspondence
                UnityWebRequest www = UnityWebRequests.Delete(MorphoTools.GetServer(), "api/deletecorrespondence/?hash=" + SetsManager.instance.hash + "&id_dataset=" + parent.AttachedDataset.id_dataset.ToString() + "&id_infos=" + id_infos.ToString() + "&id_people=" + SetsManager.instance.IDUser.ToString());
                yield return www.SendWebRequest();
                if (www.isHttpError || www.isNetworkError)
                    MorphoDebug.Log("Error : " + www.error);
                else
                {
                }
                www.Dispose();

                ct.Correspondences.Remove(this);
                parent.Correspondences.Remove(this);
                SetsManager.instance.DestroyGO(inf);
                parent.deleteCorrespondence(this, true);
                parent.AttachedDataset.Infos.Refresh();
                parent.AttachedDataset.Infos.reOrganize();
            }
            
        }

        //CURRATION
        public void addCuration(Curation cur)
        {
            if (Curations == null)
                Curations = new List<Curation>();
            Curations.Add(cur);

            if (datatype == "time" && cur.isActive)
            {
                Cell c = cur.c;
                Cell d = parent.AttachedDataset.getCell(cur.value, true);
                if (c != null && d != null)
                { //TIME CAN BE OUT OF RANGE
                    if (c.t < d.t)
                        c.addDaughter(d);
                    if (c.t > d.t)
                        d.addDaughter(c);
                }
            }
        }

        public void deleteCuration(Curation cur)
        {
            List<Curation> NewCurations = new List<Curation>();
            foreach (Curation curr in Curations)
                if (curr.id != cur.id)
                    NewCurations.Add(curr);
                else
                {
                    cur.delete();
                }
            Curations = NewCurations;
        }

        //SHARE THE CORRESPONDENCE
        public IEnumerator onShare()
        {
            bool v = SubInfos.transform.Find("onShare").gameObject.GetComponent<Toggle>().isOn;
            if (v)
                type = 1;
            else
                type = 2;
            WWWForm form = new WWWForm();
            form.AddField("id_dataset", parent.AttachedDataset.id_dataset.ToString());
            form.AddField("id_infos", id_infos.ToString());
            form.AddField("common", v.ToString());
            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/sharecorrespondence/", form);
            if (!string.IsNullOrEmpty(LoadParameters.instance.user_token))
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
                www.Dispose();
        }

        //CLEAR THE DATA
        public void remove(int id_info = -1)
        {
            //First we have to inactivate (hide)
            if ((datatype == "string" || datatype == "text") && isValueShow)
                showString(false);

            if (datatype == "sphere")
                applySphere(false);

            if (datatype == "vector")
                applyVector(false);

            if (datatype == "arrow")
                applyArrow(false);

            if (datatype == "line")
                applyLines(false);

            if (datatype == "time")
            {
                parent.clearLineageRelations(id_info);
                Transform button = InterfaceManager.instance.canvas.transform.Find("bouton layout group").Find("time_" + id_infos);
                if (button != null)
                { GameObject.Destroy(button.gameObject); }
                if (Lineage.isLineage)
                { InterfaceManager.instance.lineage.forceLineageClose(1); }
            }

            for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
            {
                foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                    cell.clearInfos(id_infos);
            }

            MorphoTools.ClearInfoInLineage(this.id_infos);
            unActive();
            parent.reOrganize();
        }

        public int nbCellParsed;
        public int startIndex = 0;
        public string[] lines;
        public bool parse = false;

        public string parseTuple(string infoslist, string infoname)
        { //TYPE 1 infos load au carchement, 2 upoad, 3 selection
            nbCellParsed = 0;

            lines = infoslist.Split('\n');

            if (lines.Length >= 1)
            { //OTHER WISE WE DON'T HAVE ENOUGH INFOS..
                while (lines[startIndex][0] == '#')
                {
                    startIndex++; //We skip comments (TODO We can add the comment when we put the mouse on the button
                }

                string types = "";

                if (datatype == "")
                {
                    string[] infotypes = lines[startIndex].Split(':');
                    types = infotypes[0].Trim();
                    datatype = infotypes[1];
                    startIndex++;
                }
                else
                {
                    types = "type";
                    startIndex++;
                }

                if (types != "type")
                {
                    MorphoDebug.Log("Error in type definition :" + lines[startIndex]);
                }
                else
                {
                    if (datatype == "time")
                    {
                        parent.AttachedDataset.createSpaceTime(id_infos, datatype, type);
                        if(LoadParameters.instance.id_dataset!=0)
                            parent.StartCoroutine(parent.AttachedDataset.Curations.downloadCurations(this));
                    }
                    if (datatype == "space")
                    {// Space Relationship (Neigbhors)
                        parent.AttachedDataset.createSpaceTime(id_infos, datatype, type);
                        InterfaceManager.instance.MenuClicked.gameObject.transform.Find("Bouton layout").gameObject.transform.Find("NeighborsSelect").gameObject.SetActive(true);
                        //InterfaceManager.instance.MenuClicked.transform.Find("Bouton layout").Find("RandomSelected").Find("Separator").gameObject.SetActive(true);
                        InterfaceManager.instance.MenuClicked.transform.Find("Background").GetComponent<Image>().fillAmount += 0.099f;
                    }
                    if (datatype == "genetic")
                    {
                        if(parent.AttachedDataset.GeneticManager!=null)
                            parent.AttachedDataset.GeneticManager.init();
                    }

                    if (datatype == "group")
                    {
                        parent.AttachedDataset.tissue.init(id_infos, infoname);
                    }
                    parse = true;
                    ProgressBar.addCor(this);
                }
            }
            return datatype;
        }

        public string reparseTuple(string infoslist, string infoname)
        { //TYPE 1 infos load au carchement, 2 upoad, 3 selection
            lines = infoslist.Split('\n');
            parse = true;
            ProgressBar.addCor(this);
            return datatype;
        }

        public Curation ContainsFileCurationForCell(Cell cell)
        {
            Curation res = null;
            return res;
        }


        public void parseLine(int lines_to_parse=200)
        {
            int nbLine = lines_to_parse;
            while (parse && nbLine > 0)
            {
                if (datatype == "time")
                {
                    string[] infos = lines[startIndex].Split(':');
                    if (infos.Count() == 2)
                    {
                        Cell c = parent.AttachedDataset.getCell(infos[0], true);
                        Cell d = parent.AttachedDataset.getCell(infos[1], true);
                        if (c != null && d != null)
                        { //TIME CAN BE OUT OF RANGE
                            if (c.t < d.t)
                                c.addDaughter(d);
                            if (c.t > d.t)
                                d.addDaughter(c);
                            nbCellParsed++;
                        }
                    }
                }
                if (datatype == "space")
                {
                    string[] infos = lines[startIndex].Split(':');
                    if (infos.Count() == 2)
                    {
                        Cell c = parent.AttachedDataset.getCell(infos[0], true);
                        Cell n = parent.AttachedDataset.getCell(infos[1], true);
                        if (c != null && n != null)
                        { //TIME CAN BE OUT OF RANGE
                            c.addNeigbhors(n);
                            nbCellParsed++;
                            if (c.Neigbhors != null)
                                c.setInfos(id_infos, c.Neigbhors.Count.ToString());
                            else
                                c.setInfos(id_infos, "0");
                            int v = c.Neigbhors.Count();
                            if (v < MinInfos)
                                MinInfos = v;
                            if (v > MaxInfos)
                                MaxInfos = v;
                        }
                    }
                }

                if (datatype == "genetic")
                {// Genetic Database
                    string[] infos = lines[startIndex].Split(':');
                    if (infos.Count() == 2)
                    {
                        Cell c = parent.AttachedDataset.getCell(infos[0], true);
                        if (c != null)
                        { //TIME CAN BE OUT OF RANGE
                            nbCellParsed++;
                            float v = 0;
                            if (float.TryParse(infos[1], out v))
                            { }
                            string gene = name;
                            if(parent.AttachedDataset.GeneticManager!=null)
                                parent.AttachedDataset.GeneticManager.addCellGene(c, gene, v, this);
                            c.setInfos(id_infos, infos[1]);
                        }
                    }
                }

                if (datatype == "float" || datatype == "number")
                {// float number
                    string[] infos = lines[startIndex].Split(':');
                    if (infos.Count() == 2)
                    {
                        Cell c = parent.AttachedDataset.getCell(infos[0], true);
                        if (c != null)
                        { //TIME CAN BE OUT OF RANGE
                            float v;
                            CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                            ci.NumberFormat.CurrencyDecimalSeparator = ".";
                            if (float.TryParse(infos[1], NumberStyles.Any, ci, out v))
                            {
                                nbCellParsed++;
                                c.setInfos(id_infos, v.ToString()); //TODO FOR INFOS AS FLOAT WE HAVE TO PASSE IT AS FLOAT
                                if (v < MinInfos)
                                    MinInfos = v;
                                if (v > MaxInfos)
                                    MaxInfos = v;
                            }
                        }
                    }
                }

                if (datatype == "string" || datatype == "text")
                {// string
                    string[] infos = lines[startIndex].Split(':');
                    if (infos.Count() == 2)
                    {
                        Cell c = parent.AttachedDataset.getCell(infos[0], true);
                        if (c != null)
                        {
                            nbCellParsed++;
                            c.setInfos(id_infos, infos[1]);
                        }
                    }
                }

                if (datatype == "group")
                {// fate map
                    string[] infos = lines[startIndex].Split(':');
                    if (infos.Count() >= 2)
                    { //We can have mutliples sub grous
                        Cell c = parent.AttachedDataset.getCell(infos[0], true);
                        if (c != null)
                        { //TIME CAN BE OUT OF RANGE
                            nbCellParsed++;
                            string gp = "";
                            for (int ii = 1; ii < infos.Length; ii++)
                            {
                                gp += infos[ii];
                                if (ii < infos.Length - 1)
                                    gp += ":";
                            }
                            c.addInfos(id_infos, gp);
                        }
                    }
                }

                if (datatype == "selection" || datatype == "label")
                {//Selection
                    string[] infos = lines[startIndex].Split(':');
                    if (infos.Count() == 2)
                    {
                        Cell c = parent.AttachedDataset.getCell(infos[0], true);
                        if (c != null)
                        { //TIME CAN BE OUT OF RANGE
                            int selectionnb = -1;
                            if (int.TryParse(infos[1], out selectionnb))
                            {
                                nbCellParsed++;
                                c.addInfos(id_infos, infos[1]);
                            }
                        }
                    }
                }

                if (datatype == "color")
                {// r,g,b
                    string[] infos = lines[startIndex].Split(':');
                    if (infos.Count() == 2)
                    {
                        Cell c = parent.AttachedDataset.getCell(infos[0], true);
                        if (c != null)
                        {
                            c.setInfos(id_infos, infos[1]);
                            nbCellParsed++;
                        }
                    }
                }

                if (datatype == "sphere")
                {
                    string[] infos = lines[startIndex].Split(':');
                    if (infos.Count() == 2)
                    {
                        Cell c = parent.AttachedDataset.getCell(infos[0], true);
                        if (c != null)
                        {
                            c.setInfos(id_infos, infos[1]);
                            nbCellParsed++;
                        }
                    }
                }

                if (datatype == "dict")
                {
                    string[] infos = lines[startIndex].Split(':');
                    if (infos.Count() == 3)
                    {
                        Cell c = parent.AttachedDataset.getCell(infos[0], true);
                        Cell c2 = parent.AttachedDataset.getCell(infos[1], true);
                        if (c != null && c2 != null)
                        {
                            float v;
                            if (float.TryParse(infos[2],NumberStyles.Float, CultureInfo.InvariantCulture, out v))
                            {
                                c.addInfos(id_infos, infos[1] + ":" + infos[2]);
                                nbCellParsed++;
                                if (v < MinInfos)
                                    MinInfos = v;
                                if (v > MaxInfos)
                                    MaxInfos = v;
                            }
                        }
                    }
                }

                if (datatype == "vector")
                {
                    string[] infos = lines[startIndex].Split(':');
                    if (infos.Count() == 3)
                    {//Typle:x,y,z,r:x,y,z,r + thicknass
                        Cell c = parent.AttachedDataset.getCell(infos[0], true);
                        if (c != null)
                        {
                            c.setInfos(id_infos, infos[1] + ":" + infos[2]);
                        }
                    }
                }

                if (datatype == "connection")
                {
                    string[] infos = lines[startIndex].Split(':');
                    if (infos.Count() == 3)
                    {//Tuple Source:Destination:Value
                        Cell c = parent.AttachedDataset.getCell(infos[0], true);
                        Cell d = parent.AttachedDataset.getCell(infos[1], true);
                        if (c != null)
                        {
                            float v;
                            c.addInfos(id_infos, infos[1] + ":" + infos[2]);
                            if (float.TryParse(infos[2], NumberStyles.Any, CultureInfo.InvariantCulture, out v))
                            {
                                if (v < MinInfos)
                                    MinInfos = v;
                                if (v > MaxInfos)
                                    MaxInfos = v;
                            }
                            if (d != null)
                            {
                                c.addNeigbhors(d);
                                d.addNeigbhors(c);
                            }
                        }
                    }
                }

                if (datatype == "line")
                {
                    string[] infos = lines[startIndex].Split(':');
                    if (infos.Count() == 3 || infos.Count() == 4)
                    {//Typle:x,y,z,r:x,y,z,r avec couleur optionelle
                        Cell c = parent.AttachedDataset.getCell(infos[0], true);
                        if (c != null)
                        {
                            if (infos.Count() == 3)
                                c.setInfos(id_infos, infos[1] + ":" + infos[2]);
                            else
                                c.setInfos(id_infos, infos[1] + ":" + infos[2] + ":" + infos[3]);
                        }
                    }
                }

                if (datatype == "arrow")
                {
                    string[] infos = lines[startIndex].Split(':');
                    if (infos.Count() == 3 || infos.Count() == 4)
                    {///Typle:x,y,z:x,y,z avec couleur optionelle
                        Cell c = parent.AttachedDataset.getCell(infos[0], true);
                        if (c != null)
                        {
                            if (infos.Count() == 3)
                                c.setInfos(id_infos, infos[1] + ":" + infos[2]);
                            else
                                c.setInfos(id_infos, infos[1] + ":" + infos[2] + ":" + infos[3]);
                        }
                    }
                }

                nbLine--;
                startIndex++;

                if (startIndex >= lines.Length)
                {
                    parse = false;
                }
            }
            if (!parse)
            { //AT THE END
                if (LoadParameters.instance.id_dataset != 0) active();
                if (datatype == "time")
                {
                    // Lineage//si morphoplot, envoi lineage morphoplot a JS
                    //Now We Add Cell Life
                    for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
                    {
                        //lineage may not be "whole" for time steps : cannot do on timestamps that have not been created (not created means no lineage info anyways)
                        if (parent.AttachedDataset.CellsByTimePoint.ContainsKey(t))
                        {
                            foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                            {
                                cell.lifePast = cell.getPastLife(0);
                                cell.lifeFutur = cell.getFuturLife(0);
                                float v = 1 + cell.lifePast + cell.lifeFutur;
                                if (v < MinInfos)
                                    MinInfos = v;
                                if (v > MaxInfos)
                                    MaxInfos = v;
                                cell.setInfos(id_infos, v.ToString());
                            }
                        }
                        
                    }
                    if (LoadParameters.instance.id_dataset == 0)
                    {
                        Lineage.UpdateLineage();
#if !UNITY_WEBGL
                        //if on plot and lineage is on, always re-send after a plugin command ?
                        if (InterfaceManager.instance.lineage_viewer != null && InterfaceManager.instance.lineage_viewer.lineage_shown)
                        {
                            InterfaceManager.instance.lineage_viewer.RestartLineage();
                        }
                        InterfaceManager.instance.DirectPlot.GoToNextErrorButton.interactable = true;
                        InterfaceManager.instance.DirectPlot.GoToPreviousErrorButton.interactable = true;
#endif
                    }
                }
                if (datatype == "genetic")
                {
                    if(parent.AttachedDataset.GeneticManager!=null)
                        parent.AttachedDataset.GeneticManager.updateGenesNbCells();
                }
                if (datatype == "group")
                    parent.AttachedDataset.tissue.setTime(parent.AttachedDataset.CurrentTime);
                if (id_dataset > 0 && datatype != "time")
                {
                    lines = null;
                }
                Resources.UnloadUnusedAssets();
                System.GC.Collect();

                //if quantitative, set values for colorbarmenu
                if (datatype == "float" || datatype == "number")
                {
                    inf.transform.Find("InfosColor").Find("minV").gameObject.GetComponent<Text>().text = MinInfos.ToString();
                    inf.transform.Find("InfosColor").Find("maxV").gameObject.GetComponent<Text>().text = MaxInfos.ToString();
                }

                if (SetsManager.instance.API_Menu != null && SetsManager.instance.API_Menu == "Genetic" && name.ToLower().Contains("name"))
                {
                    InteractionManager.instance.MenuGeneticActive();
                    InteractionManager.instance.GeneticMenuClicked();
                }
                if (SetsManager.instance.API_Fate != null && SetsManager.instance.API_Fate != "" && name.ToLower().Contains("fate"))
                    parent.AttachedDataset.update_Fate(SetsManager.instance.API_Fate);
                if (SetsManager.instance.API_Territory != null && SetsManager.instance.API_Territory != "" && name.ToLower().Contains("name"))
                    parent.AttachedDataset.update_Territory(SetsManager.instance.API_Territory, SetsManager.instance.API_Stage);
                //AUTOMATIC APPLY FROM URL QUERY
                if (SetsManager.instance.API_Correspondence != null && SetsManager.instance.API_Correspondence.ContainsKey(id_infos) && SetsManager.instance.API_Correspondence[id_infos] == 3)
                {
                    if (datatype == "genetic")
                    {
                        foreach (KeyValuePair<string, Gene> gsn in parent.AttachedDataset.GeneticManager.Genes)
                        {
                            Gene gn = gsn.Value;
                            if (gn.cor == this)
                                gn.go.transform.Find("selection").gameObject.GetComponent<Toggle>().isOn = true;
                        }
                    }
                    else
                    {
                        if (inf.transform.Find("show").gameObject.activeSelf)
                        { //Only when the bytton is active (string, selection etc..)
                            inf.transform.Find("show").gameObject.GetComponent<Button>().onClick.Invoke();
                        }
                        else
                        { //float,time,space
                            changeColorBar(); //We Calcul Min Max
                            MenuColorBar.instance.init();
                            MenuColorBar.instance.applyS(1);
                        }
                    }
                }

                if (MorphoTools.GetDataset().id_dataset == 0) // After loading an info , apply it if it was previously applied in plot
                {
                    ReApplyCorrespondence();

                }
            }
        }

        public void ReApplyCorrespondence()
        {
            if (isActive)
            {
                if (data_show == "float" || data_show == "dict" || data_show == "number")
                {
                    inf.transform.Find("InfosColor").gameObject.transform.Find("plotexecute").gameObject.GetComponent<Button>().onClick.Invoke();
                }
                else
                {
                    inf.transform.Find("show").gameObject.GetComponent<Button>().onClick.Invoke();
                }
            }
        }

        //only for simulation-color
        public void parseLineLPY(int mintime, int maxtime)
        {
            int nbLine = lines.Length;
            while (parse && nbLine > 0)
            {
                string[] infos = lines[startIndex].Split(':');
                if (infos.Count() == 2)
                {
                    Cell c = parent.AttachedDataset.getCell(infos[0], true);
                    if (c != null)
                    { //TIME CAN BE OUT OF RANGE
                        if (c.t >= mintime && c.t <= maxtime)
                        {
                            int selectionnb = -1;
                            if (int.TryParse(infos[1], out selectionnb))
                            {
                                nbCellParsed++;
                                c.setInfos(id_infos, infos[1]);
                            }
                        }
                    }
                }

                nbLine--;
                startIndex++;
                if (startIndex >= lines.Length)
                    parse = false;
            }

            if (!parse)
            { //AT THE END
              //FOR ALL
                inf.transform.Find("load").gameObject.SetActive(false);
                inf.transform.Find("UploadDate").gameObject.SetActive(false);
                if (SetsManager.instance.userRight < 2 || id_owner == SetsManager.instance.IDUser && !inf.transform.Find("deploy").gameObject.activeSelf)
                { //ONLY FOR OWNER AND CREATOR
                    inf.transform.Find("deploy").gameObject.SetActive(true);
                    parent.showSubInfosListen(inf.transform.Find("deploy").gameObject.GetComponent<Button>(), this);
                }
                inf.transform.Find("value").gameObject.SetActive(true);
                inf.transform.Find("show").gameObject.SetActive(true);
                inf.transform.Find("show").Find("Text").gameObject.GetComponent<Text>().text = "apply";
                parent.applySelectionListen(inf.transform.Find("show").gameObject.GetComponent<Button>(), this);

                if ((type != 1 && data_show != "genetic") || datatype == "time")
                { //WE CANNOT REMOVE ORIGINAL LOADED DATA
                    SubInfos.transform.Find("remove").gameObject.SetActive(true);
                    parent.removeListen(SubInfos.transform.Find("remove").gameObject.GetComponent<Button>(), this);
                }
                else
                    SubInfos.transform.Find("remove").gameObject.SetActive(false);

                SubInfos.transform.Find("cancel").gameObject.SetActive(false);
                SubInfos.transform.Find("onShare").gameObject.SetActive(false);

                SubInfos.transform.Find("remove").gameObject.SetActive(false);
                SubInfos.transform.Find("download").gameObject.SetActive(false);
                SubInfos.transform.Find("cancel").gameObject.SetActive(true);
                parent.cancelListen(SubInfos.transform.Find("cancel").gameObject.GetComponent<Button>(), this);
                parent.deleteListen(SubInfos.transform.Find("cancel").gameObject.GetComponent<Button>(), this);

                Resources.UnloadUnusedAssets();
                System.GC.Collect();
            }
        }
    }

    public class DataSetInformations : MonoBehaviour
    {
        public DataSet AttachedDataset;
        public List<CorrespondenceType> CorrespondenceTypes;
        public List<Correspondence> Correspondences;
        public int shiftStart = 76;
        public int NbStringOn = -1; //Number of string to visualize

        public GameObject DefaultCT; //CorrespondenceType Default Object
        public GameObject MenuInfos;

        //Descrtiptoin
        public Text Description;

        public Text DescriptionID;
        public Text DescriptionMotherID;
        public Text DescriptionDaughtersID;
        public GameObject LineMotherCell;
        public GameObject LineCellDaughters;
        public GameObject MenuBarColor;
        public MenuColorBar ClassColorBar;
        public GameObject NewDataType;

        //Common
        public GameObject MenuCommon;

        //Comparison
        public GameObject MenuCalcul;

        //ScrollBar
        public GameObject scrollbar;

        private bool _PickerInUse = false;

#if UNITY_WEBGL
    [DllImport("__Internal")]
    private static extern void selectFile();
#else

        private void selectFile()
        {
            if (!_PickerInUse)
            {
                _PickerInUse = true;
                FileBrowser.SetFilters(true, new FileBrowser.Filter("File", ".txt"));
                FileBrowser.SetDefaultFilter(".txt");
                StartCoroutine(WaitForFilePicker());
            }
        }

        public IEnumerator WaitForFilePicker()
        {
            yield return FileBrowser.WaitForLoadDialog(FileBrowser.PickMode.Files, false, null, null, "Load file", "Select");

            if (FileBrowser.Success)
            {
                if (File.Exists(FileBrowser.Result[0]))//if simple file
                {
                    //here logic for upload
                    if (!MorphoTools.GetPlotIsActive())
                    {
                        yield return StartCoroutine(uploadData(FileBrowser.Result[0]));
                    }
                    else
                    {
                        MorphoTools.GetPlot().AddPropertyFromFile(FileBrowser.Result[0]);
                    }
                    
                }
                else
                {
                    MorphoDebug.LogWarning("WARNING : incorrect path input");
                }


            }
            _PickerInUse = false;
        }

        


#endif

        public static DataSetInformations instance = null;

        private void Awake()
        {
            if (instance == null)
                instance = this;
            else
                Destroy(gameObject);
        }

        public void AttachDataset(DataSet parent)
        {
            AttachedDataset = parent;
        }

        public void Init()
        {
            Correspondences = new List<Correspondence>();
            CorrespondenceTypes = new List<CorrespondenceType>();
            NbStringOn = 0;
            if (MenuInfos == null)
            {
                if (GameObject.Find("Canvas"))
                {
                    MenuInfos = InterfaceManager.instance.MenuInfos.gameObject;
                    MenuCommon = InterfaceManager.instance.MenuCommon.gameObject;
                    MenuCommon.SetActive(false);

                    MenuCalcul = InterfaceManager.instance.MenuCalcul.gameObject;
                    MenuCalcul.transform.Find("defaultInfos").gameObject.SetActive(false);
                    MenuCalcul.SetActive(false);

                    scrollbar = MenuInfos.transform.Find("Scrollbar").gameObject;
                    scrollbar.GetComponent<Scrollbar>().onValueChanged.AddListener(delegate
                    { onScroll(); });
                    scrollbar.SetActive(false);

                    if (LoadParameters.instance.id_dataset == 0)
                    {
                        MenuInfos.transform.Find("UploadFile").gameObject.SetActive(false);
                        MenuInfos.transform.Find("UploadLocal").gameObject.SetActive(true);
                    }
                    else
                    {
                        MenuInfos.transform.Find("UploadLocal").gameObject.SetActive(false);
                    }

                    if (SetsManager.instance.IDUser <= 0)
                    {
                        MenuInfos.transform.Find("UploadFile").gameObject.SetActive(false);
                        MenuInfos.transform.Find("New").gameObject.SetActive(false);
                        MenuInfos.transform.Find("Refresh").gameObject.SetActive(false);
                    }
                    
                    if (LoadParameters.instance.id_dataset == 0)
                    {
                        MenuInfos.transform.Find("New").gameObject.SetActive(true);
                    }
                }
                DefaultCT = MenuInfos.transform.Find("CorrespondenceType").gameObject;
                DefaultCT.SetActive(false);
            }
            if (Description == null && MenuInfos != null)
            {
                Description = MenuInfos.transform.Find("CellDescription").gameObject.transform.GetComponent<Text>();
                DescriptionID = MenuInfos.transform.Find("CellID").gameObject.transform.GetComponent<Text>();
                LineMotherCell = MenuInfos.transform.Find("LineMotherCell").gameObject;
                LineCellDaughters = MenuInfos.transform.Find("LineCellDaughters").gameObject;
                DescriptionDaughtersID = MenuInfos.transform.Find("CellDaughtersID").gameObject.transform.GetComponent<Text>();
                DescriptionMotherID = MenuInfos.transform.Find("CellMotherID").gameObject.transform.GetComponent<Text>();
            }
            MenuBarColor = InterfaceManager.instance.MenuColorBar;
            MenuBarColor.SetActive(false);
            ClassColorBar = InterfaceManager.instance.transform.GetComponent<MenuColorBar>();

            //Create the list to intialilze the order
            getCorrespondenceType("Label");
            getCorrespondenceType("Number");
            getCorrespondenceType("Text");

            //New DataType
            NewDataType = MenuInfos.transform.Find("NewDataType").gameObject;
            NewDataType.SetActive(false);
            reOrganize();
        }

        public void Start_clearAllColors()
        {
            StartCoroutine(clearAllColors());
        }

        public IEnumerator clearAllColors()
        { //Cear Seectopn and colors !-all times step
            InterfaceManager.instance.ColorMapScale.gameObject.SetActive(false);
            InterfaceManager.instance.ColorMapScale2.gameObject.SetActive(false);

            for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
            {
                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
                {
                    cell.RemoveUploadColor();
                    cell.RemoveUploadMainColor();
                    cell.RemoveUploadHaloColor();
                    cell.RemoveUploadSecondColor();
                    cell.RemoveUploadMetallicParameter();
                    cell.RemoveUploadSmoothnessParameter();
                    cell.RemoveUploadTransparencyParameter();
                    cell.RemoveAllSelection();
                }
                if (Lineage.isLineage)
                    Lineage.UpdateWebsiteLineage();
                yield return new WaitForEndOfFrame();
            }
            MorphoTools.SendCellsColorToLineage();

            //TODO ADD update menu labels number of labels
            if (AttachedDataset.selection_manager != null)
                AttachedDataset.selection_manager.UpdateNumberCells();
        }

        public void clearLineageRelations(int id_info)
        {
            for (int t = AttachedDataset.MinTime; t <= AttachedDataset.MaxTime; t++)
            {
                foreach (Cell cell in AttachedDataset.CellsByTimePoint[t])
                {
                    if (cell.Daughters != null)
                    {
                        cell.Daughters.Clear();
                    }
                    if (cell.Mothers != null)
                    {
                        cell.Mothers.Clear();
                    }
                }
            }

            foreach (Correspondence c in Correspondences)
            {
                if (c.datatype == "time" && c.isActive && c.id_infos != id_info)
                {
                    c.startIndex = 0;
                    c.parse = true;
                }
            }
        }

        //Load data from db
        public void loadListen(Button b, Correspondence cor)
        { b.onClick.AddListener(() => StartCoroutine(downloadDATA(cor))); }

        //load data on plot mode
        public void loadPlotListen(Button b, Correspondence cor)
        { b.onClick.AddListener(() => StartCoroutine(downloadPlotDATA(cor))); }

        //load data on plot mode, for skimage properties
        public void loadPlotSKListen(Button b, Correspondence cor)
        { b.onClick.AddListener(() => StartCoroutine(downloadPlotSKDATA(cor))); }

        //Apply Sphere
        public void applySphereListen(Button b, Correspondence cor, bool v)
        { b.onClick.AddListener(() => applySphere(cor, v)); }

        public void applySphere(Correspondence cor, bool v)
        { cor.applySphere(v); }

        //Apply Vector
        public void applyVectorListen(Button b, Correspondence cor, bool v)
        { b.onClick.AddListener(() => applyVector(cor, v)); }

        public void applyVector(Correspondence cor, bool v)
        { cor.applyVector(v); }

        //apply connection
        public void applyConnectionListen(Button b, Correspondence cor, bool v = true)
        {
            b.onClick.AddListener(() => { InterfaceManager.instance.DisplayInfosConnectionMenu(true); InterfaceManager.instance.ApplyConnection.onClick.AddListener(() => applyConnection(cor, v)); InterfaceManager.instance.CancelConnection.onClick.AddListener(() => applyConnection(cor, !v)); InterfaceManager.instance.SetCurrentConnectionMinMaxValues(cor.MinInfos, cor.MaxInfos); });
        }

        public void applyConnection(Correspondence cor, bool v)
        { cor.applyConnection(v); }

        public void applyArrowListen(Button b, Correspondence cor, bool v)
        { b.onClick.AddListener(() => applyArrow(cor, v)); }

        public void applyArrow(Correspondence cor, bool v)
        { cor.applyArrow(v); }

        public void applyLinesListen(Button b, Correspondence cor, bool v)
        { b.onClick.AddListener(() => applyLines(cor, v)); }

        public void applyLines(Correspondence cor, bool v)
        { cor.applyLines(v); }

        public void DownloadAndApplyCorrespondance(Correspondence cor)
        {
            StartCoroutine(StartDownloadAndApplyCorrespondance(cor));
        }

        public IEnumerator StartDownloadAndApplyCorrespondance(Correspondence cor)
        {
            yield return downloadDATA(cor);
            yield return null;
            yield return applySelection(cor);
        }

        public void DownloadCorrespondence(Correspondence cor) => StartCoroutine(StartDownloadCorrespondence(cor));

        public IEnumerator StartDownloadCorrespondence(Correspondence cor)
        {
            yield return downloadDATA(cor);
            yield return null;
        }

        //Apply Selection
        public void applySelectionListen(Button b, Correspondence cor)
        { b.onClick.AddListener(() => StartApplyingSelection(cor)); }

        public void StartApplyingSelection(Correspondence cor)
        {
            StartCoroutine(applySelection(cor));
        }

        public IEnumerator applySelection(Correspondence cor)
        {
            //MorphoTools.ResetLineageUpdatingCells();

            Dictionary<int, List<Cell>> ListSelections = new Dictionary<int, List<Cell>>();

            for (int t = AttachedDataset.MinTime; t <= AttachedDataset.MaxTime; t++)
            {
                foreach (Cell cell in AttachedDataset.CellsByTimePoint[t])
                {
                    string s = cell.getInfos(cor.id_infos);
                    if (s != "")
                    {
                        if (s.Contains(";"))
                        {
                            string[] selections = s.Split(';');
                            foreach (string the_select in selections)
                            {
                                int selectionnb = -1;
                                if (int.TryParse(the_select, out selectionnb))
                                {
#if UNITY_WEBGL
							if (Lineage.isLineage && LoadParameters.instance.id_dataset == 0)
									{
										if (!ListSelections.ContainsKey(selectionnb)) ListSelections[selectionnb] = new List<Cell>();
										ListSelections[selectionnb].Add(cell);
									}
#endif
                                    cell.addSelection(selectionnb);
                                }
                            }
                        }
                        else
                        {
                            int selectionnb = -1;
                            if (int.TryParse(s, out selectionnb))
                            {
                                if (Lineage.isLineage)
                                {
                                    if (!ListSelections.ContainsKey(selectionnb))
                                        ListSelections[selectionnb] = new List<Cell>();
                                    ListSelections[selectionnb].Add(cell);
                                }
                                cell.addSelection(selectionnb);
                            }
                        }
                    }
                }
            }
#if UNITY_WEBGL
		if (Lineage.isLineage){
			if (LoadParameters.instance.id_dataset > 0) {
			Lineage.applySelectionInfo(cor);
			}
			else
			{
				foreach (var item in ListSelections) {
					int selectionnb = item.Key;
					List<Cell> lc=item.Value;
					foreach (Cell cell in item.Value)
					{
						Lineage.AddToColor(cell, SelectionManager.getSelectedMaterial(selectionnb).color);
					}
					Lineage.UpdateWebsiteLineage();
					yield return new WaitForEndOfFrame();
				}
			}
		}
#else
            MorphoTools.SendInfoToLineage(cor.id_infos);
            MorphoTools.SendCellsColorToLineage();

#endif
            yield return null;
        }

        //Load Color
        public void loadColorListen(Button b, Correspondence cor, bool v)
        { b.onClick.AddListener(() => loadColor(cor, v)); }

        public void loadColor(Correspondence cor, bool v)
        {
            InterfaceManager.instance.ColorMapScale.gameObject.SetActive(false); cor.loadColor(v);
#if UNITY_WEBGL
        if (Lineage.isLineage) { Lineage.LoadInfoInLineage(cor.id_infos); }
#else
            MorphoTools.SendInfoToLineage(cor.id_infos);
#endif
        }


        //Hide Color
        public void hideColorListen(Button b, Correspondence cor, bool v)
        { b.onClick.AddListener(() => hideColor(cor, v)); }

        public void hideColor(Correspondence cor, bool v)
        { cor.hideColor(v); }

        //Change Value
        public void changeValueListen(Button b, Correspondence cor)
        { b.onClick.AddListener(() => changeValue(cor)); }

        public void changeValue(Correspondence cor)
        {
            cor.changeValue();
#if UNITY_WEBGL
        if (Lineage.isLineage) { Lineage.LoadInfoInLineage(cor.id_infos); }
#else
            MorphoTools.SendInfoToLineage(cor.id_infos);
#endif
        }

        //Public when we click on a button to see the colorbar
        public void changeColorBarListen(Button b, Correspondence cor)
        { b.onClick.AddListener(() => changeColorBar(cor)); }

        public void changeColorBar(Correspondence cor)
        { cor.changeColorBar(); }

        //Change Colormap
        public void changeColorMapListen(Button b, Correspondence cor, bool force=false)
        { b.onClick.AddListener(() => changeColorMap(cor,force)); }

        public void changeColorMap(Correspondence cor, bool force=false)
        { cor.ChangeColorMap(force); }

        //Show string
        public void showStringListen(Button b, Correspondence cor, bool v)
        { b.onClick.AddListener(() => showString(cor, v)); }

        public void showString(Correspondence cor, bool v)
        {
            cor.showString(v);
#if UNITY_WEBGL
        if (Lineage.isLineage) { Lineage.LoadInfoInLineage(cor.id_infos); }
#else
            MorphoTools.SendInfoToLineage(cor.id_infos);
#endif
        }


        //Download Infos
        public void downloadInfosListen(Button b, Correspondence cor)
        { b.onClick.AddListener(() => downloadInfos(cor)); }

        public void downloadInfos(Correspondence cor)
        { StartCoroutine(cor.downloadInfos()); }

        //Remove correspondence from database
        public void cancelListen(Button b, Correspondence cor)
        { b.onClick.AddListener(() => cancel(cor)); }

        public void cancel(Correspondence cor)
        {
            Confirm.init();
            Confirm.setMessage("delete " + cor.name + " information.");
            Confirm.yes.onClick.AddListener(() => StartCoroutine(cor.cancel()));
            Confirm.confirm();
        }

        //Show Sub Infos
        public void showSubInfosListen(Button b, Correspondence cor)
        { b.onClick.AddListener(() => showSubInfos(cor)); }

        public void showSubInfos(Correspondence cor)
        { cor.showSubInfos(); }

        //Share Infos
        public void onShareListen(Toggle b, Correspondence cor)
        { b.onValueChanged.AddListener(delegate { onShare(cor); }); }

        public void onShare(Correspondence cor)
        { StartCoroutine(cor.onShare()); }

        //Remove from memory on upload
        public void removeListen(Button b, Correspondence cor)
        { b.onClick.AddListener(() => remove(cor)); }

        public void remove(Correspondence cor)
        { cor.remove(); }

        //Show Sub Infos
        public void deployCTListen(Button b, CorrespondenceType ct)
        { b.onClick.AddListener(() => deployCT(ct)); }

        public void deployCT(CorrespondenceType ct)
        { ct.deploy(); }

        //For Direct Plot Delete the correspondance from te Menu
        public void deleteListen(Button b, Correspondence cor)
        { b.onClick.AddListener(() => delete(cor)); }

        public void delete(Correspondence cor)
        {
            cor.remove();
            cor.parent.deleteCorrespondence(cor, true);
        }

        //////////////////////////// DOWLOAD ALL TYPE OF COORESPONDENCE WE HAVE TO PERFORM  (TEXT OR LINK)
        public IEnumerator start_Download_Correspondences()
        {
            //First We List all existing Correspondence
            Dictionary<int, bool> ExistingCorrespondence = new Dictionary<int, bool>();
            if (Correspondences != null)
                foreach (Correspondence cor in Correspondences)
                    ExistingCorrespondence[cor.id_infos] = false;

            string request_target = "correspondence";
            if (LoadParameters.instance.id_user == 0)
            {
                request_target = "publiccorrespondence";
            }
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/" + request_target + "/?id_dataset=" + AttachedDataset.id_dataset.ToString());
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null && LoadParameters.instance.id_user != 0)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();

            if (www.isHttpError || www.isNetworkError)
            {
                MorphoDebug.Log("Error : " + www.error);
            }
            else
            {
                if (www.downloadHandler.text != "")
                {
                    JSONNode N = JSONNode.Parse(www.downloadHandler.text);
                    if (N != null && N.Count > 0)
                    {
                        for (int i = 0; i < N.Count; i++)
                        { //Number of Infos(as id,infos,field
                            int id_infos = int.Parse(N[i]["id"].ToString().Replace('"', ' ').Trim());
                            bool load_info = true;//For url passing

                            if (SetsManager.instance.API_Correspondence != null && SetsManager.instance.API_Correspondence.ContainsKey(id_infos) && SetsManager.instance.API_Correspondence[id_infos] == 1)
                                load_info = false;
                            if (ExistingCorrespondence.ContainsKey(id_infos))
                            { load_info = false; ExistingCorrespondence[id_infos] = true; }
                            if (load_info)
                            {
                                int id_owner = int.Parse(N[i]["id_people"].ToString().Replace('"', ' ').Trim());
                                string infoname = N[i]["infos"].ToString().Replace('"', ' ').Trim();
                                string link = N[i]["link"].ToString().Replace('"', ' ').Trim();
                                uint version = uint.Parse(N[i]["version"].ToString().Replace('"', ' ').Trim());
                                int type = int.Parse(N[i]["type"].ToString().Replace('"', ' ').Trim());
                                string datatype = N[i]["datatype"].ToString().Replace('"', ' ').Trim();
                                if (datatype == "subgroup")
                                    datatype = "group";//For few old dataset...
                                string uploaddate = N[i]["date"].ToString().Replace('"', ' ').Trim();
                                int common = int.Parse(N[i]["common"].ToString().Replace('"', ' ').Trim());
                                if (datatype == "time")
                                {
                                    StartCoroutine(MorphoTools.sendPythonCommandPyinstaller("init_lineage", ""));
                                }
                                CorrespondenceType ct = getCorrespondenceType(datatype);
                                Correspondence co = new Correspondence(AttachedDataset.id_dataset, ct, id_owner, type, id_infos, infoname, link, version, (link == "null" || link == null), datatype, uploaddate, common, this);
                                ct.addCorrespondence(co);
                            }
                        }
                    }
                }
                else
                    MorphoDebug.Log("Nothing ...");
            }
            www.Dispose();
            //Remove RUnsued Correspondence
            foreach (KeyValuePair<int, bool> corV in ExistingCorrespondence)
                if (!corV.Value)
                {
                    Correspondence cor = getCorrespondence(corV.Key);
                    cor.remove();
                    SetsManager.instance.DestroyGO(cor.inf);
                    deleteCorrespondence(cor, false); //REMOVE THIS CORRESPONDENCE
                }
            reOrganize();
            InteractionManager.instance.infos_dl = null;
        }

        //////////////////////////// REDRESH THE CORRESPONDENCE LIST
        /// //On Button refresh click, we check all the correspondence if they are still valid or not ...
        public void Refresh()
        {
            if (LoadParameters.instance.id_dataset == 0)
            {
                SetsManager.instance.directPlot.ReloadInfos();
            }
            else
            {
                StartCoroutine(start_Download_Correspondences());
            }
        }

        public CorrespondenceType getCorrespondenceType(string name)
        {
            if (name == "time" || name == "float" || name == "space" || name == "number")
                name = "Number";
            if (name == "string" || name == "group" || name == "text")
                name = "Text";
            if (name == "selection" || name == "label")
                name = "Label";
            if (name == "genetic")
                name = "Genetic";
            if (name != "Label" && name != "Number" && name != "Text" && name != "Genetic")
                name = "Others";
            foreach (CorrespondenceType ct in CorrespondenceTypes)
            {
                if (ct.name == name)
                    return ct;
            }
            //CREATE A NEW ONE
            CorrespondenceType ctn = new CorrespondenceType(name, this);
            CorrespondenceTypes.Add(ctn);
            
            return ctn;
        }

        public Correspondence getCorrespondence(int id_infos)
        {
            foreach (Correspondence cor in Correspondences)
                if (cor.id_infos == id_infos)
                    return cor;
            return null;
        }

        public Correspondence getCorrespondenceByName(string name)
        {
            foreach (Correspondence cor in Correspondences)
                if (cor.name == name)
                    return cor;
            return null;
        }

        public List<Correspondence> CorrespondenceToDownload;

        public void addDownloadDATA(Correspondence cor)
        {
            cor.inf.transform.Find("load").gameObject.SetActive(false);
            bool startDownload = false;
            if (CorrespondenceToDownload == null)
            {
                startDownload = true;
                CorrespondenceToDownload = new List<Correspondence>();
            }
            CorrespondenceToDownload.Add(cor);
            if (startDownload)
                startdownloadDATA();
        }

        public IEnumerator downloadDATA(Correspondence cor)
        {
            cor.inf.transform.Find("load").gameObject.SetActive(false);
            if (!cor.IsActive)
            {
                if (cor.txt)
                    yield return StartCoroutine(downloadDataTXT(cor, false));
                else
                    yield return StartCoroutine(DataLoader.instance.downloadDataBundle(cor, false, this));
            }
        }

        public IEnumerator downloadPlotDATA(Correspondence cor)
        {
            //cor.inf.transform.Find("load").gameObject.SetActive(false);
            if (!cor.IsActive)
            {
                yield return InterfaceManager.instance.DirectPlot.sendPythonCommand(cor.name,"get_information_full", cor);
                
            }
        }

        public IEnumerator downloadPlotSKDATA(Correspondence cor)
        {
            //cor.inf.transform.Find("load").gameObject.SetActive(false);
            if (!cor.IsActive)
            {
                cor.inf.transform.Find("load").gameObject.SetActive(false);
                yield return InterfaceManager.instance.DirectPlot.sendPythonCommand(cor.name, "get_sk_information", cor);
            }
        }

        public void startdownloadDATA()
        {
            if (CorrespondenceToDownload != null && CorrespondenceToDownload.Count > 0)
            {
                Correspondence cor = CorrespondenceToDownload[0];
                CorrespondenceToDownload.RemoveAt(0);
                if (cor.txt)
                    StartCoroutine(downloadDataTXT(cor, true));
                else
                    StartCoroutine(DataLoader.instance.downloadDataBundle(cor, true, this));
            }
            else
                CorrespondenceToDownload = null;
        }

        private const int PROP_LINES_TO_READ = 200;

        private void Update()
        {
            if (Correspondences != null)
            {
                //find out optimal number of lines to parse (divide by amount of current props to load simultaneously)
                int lines_to_read = PROP_LINES_TO_READ;
                int concurrent_props = 0;
                foreach (Correspondence cor in Correspondences)
                    if (cor.parse && !cor.name.Contains("simulation-color"))
                        concurrent_props++;
                lines_to_read = lines_to_read / (concurrent_props == 0 ? 1 : concurrent_props);
                foreach (Correspondence cor in Correspondences)
                {
                    if (cor.parse && !cor.name.Contains("simulation-color"))
                    {
                        cor.parseLine(lines_to_read);
                    }
                }
            }
        }

        //Download all datas for a specific infos
        public IEnumerator downloadDataTXT(Correspondence cor, bool continu)
        {
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("download", "2");
            form.AddField("id_dataset", AttachedDataset.id_dataset.ToString());
            form.AddField("id_infos", cor.id_infos.ToString());
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/correspondencefield/?hash=" + SetsManager.instance.hash + "&download=2&id_dataset=" + AttachedDataset.id_dataset.ToString() + "&id_infos=" + cor.id_infos.ToString());
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                if (www.downloadHandler.text != "")
                {
                    string infoslist = Encoding.UTF8.GetString(UtilsManager.Unzip(www.downloadHandler.data));
                    cor.startIndex = 0;
                    cor.parseTuple(infoslist, cor.name);
                }
                else
                    MorphoDebug.Log("Error Loading " + cor.name);
            }
            www.Dispose();
            if (continu)
                startdownloadDATA();
        }

        //Donwload a Bundle from a url

        //when we delete a correspondence
        public void deleteCorrespondence(Correspondence corToRemove, bool reorg)
        {
            CorrespondenceType ctToRemove = corToRemove.ct;
            corToRemove.isActive = false;
            ctToRemove.removeCorrespondence(corToRemove);
            if (ctToRemove.Correspondences.Count == 0)
            {
                CorrespondenceTypes.Remove(ctToRemove);
            }
                
            if (reorg)
                reOrganize();
        }

        //To visualiy redistribute all the correspondence type and correspondance
        public void reOrganize()
        {
            if (Correspondences != null)
                foreach (Correspondence cor in Correspondences)
                    if (cor.isActive)
                        cor.setButtons(AttachedDataset.PickedManager.clickedCells.Count);

            int Height = 0;
            inScroll = 0;
            if (CorrespondenceTypes != null)
            {
                foreach (CorrespondenceType ct in CorrespondenceTypes)
                {
                    int add_height = ct.SetPosition(Height);
                    Height += add_height;
                }
                if (Height > MaxHeight)
                {
                    scrollbar.SetActive(true);
                    scrollbar.GetComponent<Scrollbar>().numberOfSteps = 2 + (Height - MaxHeight) / 22; //DEFINE SCROLLBAR LENGTH
                    scrollbar.GetComponent<Scrollbar>().size = 1f / (float)(2 + (Height - MaxHeight) / 22);
                }
            }
        }

        //SCROLLBAR
        public int MaxHeight = 600;

        public int nbSCroll = 0;
        public int inScroll = 0;

        public void onScroll()
        {
            int nbSteps = scrollbar.GetComponent<Scrollbar>().numberOfSteps;
            float value = scrollbar.GetComponent<Scrollbar>().value;
            nbSCroll = (int)Mathf.Round(nbSteps - value * (nbSteps - 1) - 1);
            reOrganize();
        }

        //Given a name of feature return the index in the list
        public int getInfosIndex(string infoname)
        {
            foreach (Correspondence cor in Correspondences)
                if (cor.name.ToLower().Trim() == infoname.ToLower().Trim())
                    return cor.id_infos;
            foreach (Correspondence cor in Correspondences)
                if (cor.name.ToLower().Contains(infoname.ToLower().Trim()))
                    return cor.id_infos;
            return -1;
        }
        
        public List<Correspondence> getAllInfosIndex(string infoname)
        {
            List<Correspondence> result = new List<Correspondence>();
            foreach (Correspondence cor in Correspondences)
                if (cor.name.ToLower().Trim() == infoname.ToLower().Trim())
                    result.Append(cor);
            foreach (Correspondence cor in Correspondences)
                if (cor.name.ToLower().Contains(infoname.ToLower().Trim()))
                    result.Append(cor);
            return result;
        }

        public string getInfosName(int idx)
        {
            foreach (Correspondence cor in Correspondences)
                if (cor.id_infos == idx)
                    return cor.name;
            return "";
        }

        //REturn the id containing the cell name ..
        public int getIdInfosForName()
        {
            int id_infos = getInfosIndex("name");
            if (id_infos != -1)
                return id_infos;
            id_infos = getInfosIndex("names");
            if (id_infos != -1)
                return id_infos;
            foreach (Correspondence cor in Correspondences)
                if (cor.name.ToLower().Contains("name"))
                    return cor.id_infos;
            return -1;
        }
        
        public int getAllIdInfosForName()
        {
            List<Correspondence> infos = getAllInfosIndex("name");
            if (infos.Count > 0)
            {
                foreach (Correspondence c in infos)
                {
                    if (c.datatype.ToLower() == "text" || c.datatype.ToLower() == "string")
                    {
                        return c.id_infos;
                    }
                }
            }
            infos = getAllInfosIndex("names");
            if (infos.Count > 0)
            {
                foreach (Correspondence c in infos)
                {
                    if (c.datatype.ToLower() == "text" || c.datatype.ToLower() == "string")
                    {
                        return c.id_infos;
                    }
                }
            }
            foreach (Correspondence cor in Correspondences)
                if (cor.name.ToLower().Contains("name") && (cor.datatype.ToLower()=="text" || cor.datatype.ToLower() == "string"))
                    return cor.id_infos;
            return -1;
        }

        //WE UPDATE THE SELETED CELL BOX IN SELEXION COLOR
        public void onupdateSelectionOnSelectedCells(bool v)
        { updateSelectionOnSelectedCells(); } //When we click on all times

        public void updateSelectionOnSelectedCells()
        {
            GameObject MenuObjects = InterfaceManager.instance.MenuObjects;
            GameObject thiscells = MenuObjects.transform.Find("Selection").Find("TopPanel").gameObject.transform.Find("thiscells").gameObject;
            bool alltimes = thiscells.transform.Find("AllTimes").gameObject.transform.GetComponent<Toggle>().isOn;
            //Count for this cell the number of selection
            List<int> NbSelection = new List<int>();
            List<Cell> NbCells = new List<Cell>();
            for (int i = 0; i < AttachedDataset.PickedManager.clickedCells.Count; i++)
            {
                Cell sc = AttachedDataset.PickedManager.clickedCells[i];
                if (!alltimes)
                {
                    if (!NbCells.Contains(sc))
                        NbCells.Add(sc);
                    if (sc.selection != null)
                        foreach (int s in sc.selection)
                            if (!NbSelection.Contains(s))
                                NbSelection.Add(s);
                }
                else
                {
                    List<Cell> cells = sc.getAllCellLife();
                    foreach (Cell c in cells)
                        if (!NbCells.Contains(c))
                        {
                            NbCells.Add(c);
                            if (c.selection != null)
                                foreach (int s in c.selection)
                                    if (!NbSelection.Contains(s))
                                        NbSelection.Add(s);
                        }
                }
            }
            int nb_cells = NbCells.Count;
            string nb = NbCells.Count.ToString();

            if (nb_cells == 1)
            {
                InterfaceManager.instance.MenuClicked.transform.Find("selectedcells").Find("comment").Find("Text").GetComponent<Text>().text = "";
                InterfaceManager.instance.MenuClicked.transform.Find("selectedcells").Find("comment").Find("Image").GetComponent<Image>().fillAmount = 0f;
                nb = AttachedDataset.PickedManager.clickedCells[0].getName();
            }
            else if (nb_cells > 1)
            {
                int count_display = 0;
                InterfaceManager.instance.MenuClicked.transform.Find("selectedcells").Find("comment").Find("Text").GetComponent<Text>().text = "";
                InterfaceManager.instance.MenuClicked.transform.Find("selectedcells").Find("comment").Find("Image").GetComponent<Image>().fillAmount = 0f;
                foreach (Cell b in NbCells)
                {
                    if (count_display > 4)
                    { InterfaceManager.instance.MenuClicked.transform.Find("selectedcells").Find("comment").Find("Text").GetComponent<Text>().text += " - ..."; InterfaceManager.instance.MenuClicked.transform.Find("selectedcells").Find("comment").Find("Image").GetComponent<Image>().fillAmount += 0.18f; break; }
                    if (count_display > 0)
                    { InterfaceManager.instance.MenuClicked.transform.Find("selectedcells").Find("comment").Find("Text").GetComponent<Text>().text += " - "; }
                    InterfaceManager.instance.MenuClicked.transform.Find("selectedcells").Find("comment").Find("Text").GetComponent<Text>().text += b.getName();
                    InterfaceManager.instance.MenuClicked.transform.Find("selectedcells").Find("comment").Find("Image").GetComponent<Image>().fillAmount += 0.18f;
                    count_display++;
                }
            }
            else
            {
                InterfaceManager.instance.MenuClicked.transform.Find("selectedcells").Find("comment").Find("Text").GetComponent<Text>().text = "";
                InterfaceManager.instance.MenuClicked.transform.Find("selectedcells").Find("comment").Find("Image").GetComponent<Image>().fillAmount = 0f;
            }
            InterfaceManager.instance.MenuClicked.transform.Find("selectedcells").gameObject.transform.GetComponent<Text>().text = nb;
        }

        //When we click on a cell we update all ingos on the cell Menu
        public void describe() //THIS THINGS IS ALWAYS CALL ...
        {
            if (Description != null)
            {
                AttachedDataset.tissue.describe();

                updateSelectionOnSelectedCells();//LAGGY
                if (AttachedDataset.Curations != null && AttachedDataset.Curations.MenuCurration != null && AttachedDataset.Curations.MenuCurration.activeSelf)
                {
                    AttachedDataset.Curations.BindCurrationInitValue();
                }
                if (AttachedDataset.PickedManager.clickedCells.Count == 1)
                { //ONLY ONE OBJECT
                    Cell c = AttachedDataset.PickedManager.clickedCells[0];
                    LineMotherCell.SetActive(true);
                    LineCellDaughters.SetActive(true);
                    DescriptionID.text = c.t + "\n" + c.ID;

                    //MOTHERS (while only on mother and this not divide
                    if (AttachedDataset.MinTime != AttachedDataset.MaxTime)
                    {
                        if (c.Mothers != null)
                        {
                            List<Cell> past = c.Mothers;
                            while (past.Count == 1 && past[0].Daughters != null && past[0].Mothers != null && past[0].Daughters.Count == 1)
                                past = past[0].Mothers;
                            if (past.Count == 0)
                                DescriptionMotherID.text = "X";
                            else if (past.Count == 1)
                                DescriptionMotherID.text = past[0].t + "\n" + past[0].ID;
                            else if (past.Count > 1)
                            {
                                DescriptionMotherID.text = "At " + past[0].t + "\n";
                                foreach (Cell m in past)
                                    DescriptionMotherID.text += m.ID + " ";
                            }
                        }
                        else
                            DescriptionMotherID.text = "X";

                        //MOTHERS (while only one daughter and this has only one mother
                        if (c.Daughters != null)
                        {
                            List<Cell> futur = c.Daughters;
                            while (futur.Count == 1 && futur[0].Mothers != null && futur[0].Daughters != null && futur[0].Mothers.Count == 1)
                                futur = futur[0].Daughters;
                            if (futur.Count == 0)
                                DescriptionDaughtersID.text = "X";
                            else if (futur.Count == 1)
                                DescriptionDaughtersID.text = futur[0].t + "\n" + futur[0].ID;
                            else if (futur.Count > 1)
                            {
                                DescriptionDaughtersID.text = futur[0].t + "\n";
                                foreach (Cell d in futur)
                                    DescriptionDaughtersID.text += d.ID + " ";
                            }
                        }
                        else
                            DescriptionDaughtersID.text = "X";
                    }
                    else
                    {
                        DescriptionMotherID.text = "";
                        DescriptionDaughtersID.text = "X";
                    }

                    if (AttachedDataset.Curations.MenuCurration != null && AttachedDataset.Curations.MenuCurration.activeSelf)
                        AttachedDataset.Curations.showCuration(); //Update Curration

                    foreach (Correspondence cor in Correspondences)
                    {
                        cor.inf.transform.Find("value").transform.gameObject.GetComponent<Text>().text = AttachedDataset.PickedManager.clickedCells[0].getDisplayInfos(cor.id_infos);
                    }
                }
                else
                { //MULTIPLE CELLS
                    LineMotherCell.SetActive(false);
                    LineCellDaughters.SetActive(false);
                    DescriptionMotherID.text = "";
                    DescriptionDaughtersID.text = "";
                    if (AttachedDataset.PickedManager.clickedCells.Count > 1)
                    {
                        DescriptionID.text = AttachedDataset.PickedManager.clickedCells.Count + " objects";
                        resetDescription();
                        //Calcul The average for this selection
                        foreach (Correspondence cor in Correspondences)
                        {
                            if (cor.data_show == "float" || cor.data_show == "number")
                            {
                                float av = 0;
                                int nb = 0;
                                for (int i = 0; i < AttachedDataset.PickedManager.clickedCells.Count; i++)
                                {
                                    string s = AttachedDataset.PickedManager.clickedCells[i].getDisplayInfos(cor.id_infos);
                                    if (s != "")
                                    {
                                        //av += float.Parse(s);
                                        av += float.Parse(s.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
                                        nb += 1;
                                    }
                                }
                                if (nb > 0)
                                    cor.inf.transform.Find("value").transform.gameObject.GetComponent<Text>().text = "" + Mathf.Round((av / nb) * 1000) / 1000;
                                else
                                    cor.inf.transform.Find("value").transform.gameObject.GetComponent<Text>().text = "";
                            }
                        }

                        if (AttachedDataset.Curations.MenuCurration != null && AttachedDataset.Curations.MenuCurration.activeSelf)
                            AttachedDataset.Curations.showCuration();  //Update Curration

                        //Update Curation init value
                    }
                    else
                    { //NO CELLS
                        DescriptionMotherID.text = "";
                        DescriptionDaughtersID.text = "";
                        DescriptionID.text = "No Objects selected";
                        resetDescription();
                        /*if (AttachedDataset.Curations.MenuCurration != null && AttachedDataset.Curations.MenuCurration.activeSelf)
                            AttachedDataset.Curations.MenuCurration.SetActive(false); //Deasctive menu curration if it was open*/
                    }
                }
            }
        }

        //Rest the Neigbhors description and detail for each cell
        public void resetDescription()
        {
            Description.text = "";
            foreach (Correspondence cor in Correspondences)
            {
                cor.inf.transform.Find("value").transform.gameObject.GetComponent<Text>().text = "";
            }
        }

        //UPLOAD NEW CORRESPONDANCE
        public void Upload()
        {
#if UNITY_EDITOR
            /*string path = UnityEditor.EditorUtility.OpenFilePanel("Open text", "", "");
            if (!System.String.IsNullOrEmpty(path))
            {
                FileSelected("file:///" + path + ";" + Path.GetFileName(path));
            }*/
#elif UNITY_WEBGL
		selectFile();
#endif
#if UNITY_STANDALONE
            selectFile();
#endif

        }

        private void FileSelected(string urlname)
        {
            StartCoroutine(uploadData(urlname));
        }

        //Back From Javascript after click on upload
        public void onFinishLoadData(string id_upload_toLoadImmediately)
        {
            StartCoroutine(DataLoader.instance.start_Download_ID_Correspondences(int.Parse(id_upload_toLoadImmediately), this));
        }

        public IEnumerator UploadLocalData(string urlName)
        {
            Debug.LogWarning(urlName);

            string filename = urlName.Split(Path.DirectorySeparatorChar)[urlName.Split(Path.DirectorySeparatorChar).Length - 1].Split('.')[0];
            Debug.LogWarning(filename);

            List<string> textToLoad = new List<string>();
            StringBuilder infocontent = new StringBuilder();
            using (StreamReader sr = new StreamReader(urlName))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    textToLoad.Add(line);
                    infocontent.AppendLine(line);
                }
            }

            yield return null;
        }


        /// <summary>
        /// upload correspondence to MorphoNet servers
        /// </summary>
        /// <param name="urlname"></param>
        /// <returns></returns>
        public IEnumerator uploadData(string urlname)
        {
            Debug.LogWarning(urlname);

            string filename = urlname.Split(Path.DirectorySeparatorChar)[urlname.Split(Path.DirectorySeparatorChar).Length - 1].Split('.')[0];
            Debug.LogWarning(filename);

            List<string> textToLoad = new List<string>();
            StringBuilder infocontent = new StringBuilder();
            using (StreamReader sr = new StreamReader(urlname))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    textToLoad.Add(line);
                    infocontent.AppendLine(line);
                }
            }
                
            //First We have to get the Type
            int startIndex = 0;
            while (textToLoad[startIndex][0] == '#')
                startIndex++; //We skip comments (TODO We can add the comment when we put the mouse on the button
            string[] infotypes = textToLoad[startIndex].Split(':');
            if (infotypes.Count() != 2)
                MorphoDebug.Log("ERROR UPLOAD uncorrect format type specification " + textToLoad[startIndex]);
            else
            {
                string datatype = infotypes[1];
                // Show results as text
                WWWForm form = new WWWForm();
                form.AddField("infos", filename);
                form.AddField("type", "2");
                form.AddField("datatype", datatype);
                form.AddField("id_dataset", AttachedDataset.id_dataset.ToString());
                form.AddBinaryData("r_field", UtilsManager.Zip(Encoding.ASCII.GetBytes(infocontent.ToString())));
                UnityWebRequest www2 = UnityWebRequests.Post(MorphoTools.GetServer(), "api/createcorrespondence/", form);
                if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
                {
                    www2.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
                }

                yield return www2.SendWebRequest();
                if (www2.isNetworkError || www2.isHttpError)
                    MorphoDebug.Log("ERROR :" + www2.error);
                else
                {
                    int id_infos_upload;
                    if (int.TryParse(Encoding.UTF8.GetString(www2.downloadHandler.data), out id_infos_upload))
                    {
                        CorrespondenceType ct = getCorrespondenceType(datatype);
                        string uploaddate = DateTime.Now.ToString();
                        Correspondence co = new Correspondence(AttachedDataset.id_dataset, ct, SetsManager.instance.IDUser, 2, id_infos_upload, filename, "", 1, true, datatype, uploaddate, 0, this);
                        ct.addCorrespondence(co);
                        reOrganize();
                    }
                    else
                    {
                        MorphoDebug.Log("ERROR Parsing after upload: " + www2.downloadHandler.text);
                    }
                        
                    www2.Dispose();
                }
            
            }
        }

        public void createNewInfos()
        {
            NewDataType.SetActive(true);
            NewDataType.transform.Find("Dropdown").gameObject.GetComponent<Dropdown>().value = 1;
            NewDataType.transform.Find("Dropdown").gameObject.SetActive(true);
            NewDataType.transform.Find("NameType").gameObject.GetComponent<InputField>().text = "";
            NewDataType.transform.Find("datatype").gameObject.GetComponent<Text>().text = "Create an empty property";
            NewDataType.transform.Find("create_empty").gameObject.SetActive(true);
            NewDataType.transform.Find("create_selection").gameObject.SetActive(false);
        }

        public void createNewInfosSelection()
        {
            NewDataType.SetActive(true);
            NewDataType.transform.Find("Dropdown").gameObject.GetComponent<Dropdown>().value = 1;
            NewDataType.transform.Find("Dropdown").gameObject.SetActive(false);
            NewDataType.transform.Find("NameType").gameObject.GetComponent<InputField>().text = "";
            NewDataType.transform.Find("datatype").gameObject.GetComponent<Text>().text = "Create a new label property";
            NewDataType.transform.Find("create_empty").gameObject.SetActive(false);
            NewDataType.transform.Find("create_selection").gameObject.SetActive(true);
        }

        public void closeNewInfos()
        {
            NewDataType.SetActive(false);
        }

        public void uploadNewEmptyInfos()
        {
            
            string NameType = NewDataType.transform.Find("NameType").gameObject.GetComponent<InputField>().text;
            int v = NewDataType.transform.Find("Dropdown").gameObject.GetComponent<Dropdown>().value;
            //v=1 pour Selection, 2 pour Quantitative, 3 pour Qualitative
            if (v == 0 || NameType == "")
                MorphoDebug.Log("Upload->" + NameType + " -> " + v);
            else
            {


                string datatype = "number";
                if (v == 2)
                    datatype = "text";
                //CREATE THE EMPTY FORMAT
                string morpho = "#Infos " + NameType + "\n";
                morpho += "type:" + datatype + "\n";
                if (!MorphoTools.GetPlotIsActive())
                {
                    StartCoroutine(DataLoader.instance.uploadInfos(morpho, NameType, datatype, this)); //SELECTION
                }
                else
                {
                    MorphoTools.GetPlot().CreateInfo(morpho, NameType, datatype);
                }
                    

                closeNewInfos(); //.????
            }
        }

        public void uploadNewSelectionInfos()
        {
            string NameType = NewDataType.transform.Find("NameType").gameObject.GetComponent<InputField>().text;

            StringBuilder morpho = new StringBuilder();
            morpho.Append("#Selection " + NameType + "\n");
            morpho.Append("type:selection\n");
            for (int t = AttachedDataset.MinTime; t <= AttachedDataset.MaxTime; t++)
            {
                foreach (Cell cell in AttachedDataset.CellsByTimePoint[t])
                {
                    if (cell.selection != null)
                        foreach (int s in cell.selection)
                            morpho.Append(cell.t + "," + cell.ID + ",0:" + s + "\n");
                }
            }
            
            if (!MorphoTools.GetPlotIsActive())
            {
                StartCoroutine(DataLoader.instance.uploadInfos(morpho.ToString(), NameType, "label", this)); //SELECTION
            }
            else
            {
                MorphoTools.GetPlot().CreateInfo(morpho.ToString(), NameType, "selection");
            }

            closeNewInfos(); //.????
        }

        public void UpdateSelectionListen(Button b, Correspondence cor)
        {
            b.onClick.AddListener(() =>
            {
                Confirm.init();
                Confirm.setMessage("update " + cor.name + " information.");
                Confirm.yes.onClick.AddListener(() => { UpdateSelectionInfo(cor); });
                Confirm.confirm();
            });
        }

        public void UpdateSelectionInfo(Correspondence cor)
        {
            //CREATE THE FORMAT
            StringBuilder morpho = new StringBuilder();
            morpho.Append("#Selection " + cor.name + "\n");
            morpho.Append("type:selection\n");
            for (int t = AttachedDataset.MinTime; t <= AttachedDataset.MaxTime; t++)
            {
                foreach (Cell cell in AttachedDataset.CellsByTimePoint[t])
                {
                    if (cell.selection != null)
                        foreach (int s in cell.selection)
                        {
                            if (cell.Channels != null && cell.Channels.Count() > 0)
                            {
                                morpho.Append(cell.t + "," + cell.ID + "," + cell.getFirstChannel().getName() + ":" + s + "\n");
                            }
                            else
                                morpho.Append(cell.t + "," + cell.ID + ",0:" + s + "\n");
                        }
                }
            }
            if (LoadParameters.instance.id_dataset == 0) //DIRECTPLOT
                StartCoroutine(SetsManager.instance.directPlot.CreateInfoRequest(morpho.ToString(), cor.name, cor.datatype, reload:true));
            else
                StartCoroutine(DataLoader.instance.uploadInfosUpdate(morpho.ToString(), cor, this)); //SELECTION
        }

        //COMMON
        //Donwload List of common dataset
        public void downloadCommonDataset()
        {
            if (MenuCommon.activeSelf)
                MenuCommon.SetActive(false);
            else
            {
                MenuCommon.SetActive(true);
                coOtherInfos = null;
                id_bridge = -1;
                id_bridge_other = -1;
                MenuCommon.transform.Find("load").gameObject.SetActive(false);
                MenuCommon.transform.Find("otherbridge").gameObject.SetActive(false);
                MenuCommon.transform.Find("otherinfos").gameObject.SetActive(false);
                MenuCommon.transform.Find("name").gameObject.SetActive(false);
                MenuCommon.transform.Find("datasets").GetComponent<Dropdown>().ClearOptions();
                MenuCommon.transform.Find("datasets").GetComponent<Dropdown>().value = 0;
                StartCoroutine(DataLoader.instance.listInfosFromDataset(AttachedDataset.id_dataset, "thisbridge"));
                StartCoroutine(DataLoader.instance.start_downloadCommonDataset(this)); //Download all currations from db
            }
        }

        public List<int> common_id_dataset;
        public Dictionary<int, string> datasetNames;

        public void onSelectCommonDataset(Int32 v)
        {
            int vv = MenuCommon.transform.Find("datasets").GetComponent<Dropdown>().value;
            int choose_id_dataset = common_id_dataset[vv];
            if (choose_id_dataset != -1)
            {
                MorphoDebug.Log("select " + vv + " -> id=" + common_id_dataset[vv]);
                StartCoroutine(DataLoader.instance.listInfosFromDataset(common_id_dataset[vv], "otherbridge"));
                StartCoroutine(DataLoader.instance.listInfosFromDataset(common_id_dataset[vv], "otherinfos"));
            }
            else
            {
                MenuCommon.transform.Find("load").gameObject.SetActive(false);
                MenuCommon.transform.Find("otherbridge").gameObject.SetActive(false);
                MenuCommon.transform.Find("otherinfos").gameObject.SetActive(false);
            }
        }

        public List<CorrespondenceOtherData> CorrespondenceThisBridge;
        public List<CorrespondenceOtherData> CorrespondenceOtherBridge;
        public List<CorrespondenceOtherData> CorrespondenceInfos;

        public int id_bridge = -1;

        public void onSelectThisBridge(Int32 v)
        {
            int vv = MenuCommon.transform.Find("thisbridge").GetComponent<Dropdown>().value;
            if (vv > 0)
            {
                id_bridge = CorrespondenceThisBridge[vv - 1].id_infos;
                MorphoDebug.Log("Select This Bridge " + id_bridge);
            }
            else
                id_bridge = -1;
        }

        public int id_bridge_other = -1;

        public void onSelectOtherBridge(Int32 v)
        {
            int vv = MenuCommon.transform.Find("otherbridge").GetComponent<Dropdown>().value;
            if (vv > 0)
            {
                id_bridge_other = CorrespondenceOtherBridge[vv - 1].id_infos;
                MorphoDebug.Log("Select Other Bridge " + id_bridge_other);
            }
            else
                id_bridge_other = -1;
        }

        public CorrespondenceOtherData coOtherInfos = null;

        public void onSelectOtherInfos(Int32 v)
        {
            int vv = MenuCommon.transform.Find("otherinfos").GetComponent<Dropdown>().value;
            if (vv > 0)
            {
                coOtherInfos = CorrespondenceInfos[vv - 1];
                coOtherInfos.name = coOtherInfos.name + "(" + datasetNames[coOtherInfos.id_dataset] + ")";
                MorphoDebug.Log("Select Other Infos " + coOtherInfos.name);
                MenuCommon.transform.Find("load").gameObject.SetActive(true);
                MenuCommon.transform.Find("name").gameObject.SetActive(true);
                MenuCommon.transform.Find("name").GetComponent<InputField>().text = coOtherInfos.name;
            }
            else
            {
                MenuCommon.transform.Find("load").gameObject.SetActive(false);
                MenuCommon.transform.Find("name").gameObject.SetActive(false);
                coOtherInfos = null;
            }
        }

        //Add an Infos
        public void addInfosFromDataset()
        {
            MorphoDebug.Log("Add Infos" + coOtherInfos.id_infos);
            CorrespondenceType ct = getCorrespondenceType(coOtherInfos.datatype);
            string newname = MenuCommon.transform.Find("name").GetComponent<InputField>().text;
            Correspondence co = new Correspondence(coOtherInfos.id_dataset, ct, SetsManager.instance.IDUser, 2, coOtherInfos.id_infos, newname, "", 1, true, coOtherInfos.datatype, DateTime.Now.ToString(), 0, this);
            ct.addCorrespondence(co);
            co.inf.transform.Find("load").gameObject.SetActive(false);//We inactivate the button because we are downlaoad them automatically
            reOrganize();
            StartCoroutine(createCommonInfos(co));
            MenuCommon.SetActive(false);
        }

        public IEnumerator createCommonInfos(Correspondence cor)
        {
            MorphoDebug.Log("this.id_bridge = " + id_bridge);
            MorphoDebug.Log("this.id_bridge_other = " + id_bridge_other);
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("download", "12");
            form.AddField("id_bridge", id_bridge.ToString());
            form.AddField("id_bridge_other", id_bridge_other.ToString());
            form.AddField("id_dataset", AttachedDataset.id_dataset.ToString());
            form.AddField("id_dataset_infos", cor.id_dataset.ToString());
            form.AddField("id_infos", cor.id_infos.ToString());
            form.AddField("infos", cor.name.ToString());
            form.AddField("id_people", SetsManager.instance.IDUser.ToString());
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/createcommon/?hash=" + SetsManager.instance.hash + "&id_bridge = " + id_bridge + " & id_bridge_other = " + id_bridge_other + " & id_infos = " + cor.id_infos + " & id_dataset = " + AttachedDataset.id_dataset.ToString() + " & id_dataset_infos = " + cor.id_dataset.ToString() + " & infos = " + cor.name);
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                if (www.downloadHandler.text != "")
                {
                    cor.id_infos = int.Parse(Encoding.UTF8.GetString(www.downloadHandler.data));
                    cor.id_dataset = AttachedDataset.id_dataset;
                    MorphoDebug.Log("cor.id_infos =" + cor.id_infos);
                }
                else
                    MorphoDebug.Log("Error Loading " + cor.name);
            }
            www.Dispose();
            downloadDATA(cor);
        }

        //COMPARISON
        public Dictionary<int, GameObject> InfosToCompare;

        public void openMenuCalcul()
        {
            if (MenuCalcul.activeSelf)
                MenuCalcul.SetActive(false);
            else
            {
                cleanInfosToCompare();
                listQualitativeInfos();
                MenuCalcul.SetActive(true);
            }
        }

        public void cleanInfosToCompare()
        {
            if (InfosToCompare != null)
            {
                foreach (KeyValuePair<int, GameObject> itc in InfosToCompare)
                {
                    int id_infos = itc.Key;
                    GameObject go = itc.Value;
                    Destroy(go);
                }
                InfosToCompare.Clear();
            }
            else
                InfosToCompare = new Dictionary<int, GameObject>();
            MenuCalcul.transform.Find("function").gameObject.SetActive(false);
            MenuCalcul.transform.Find("apply").gameObject.SetActive(false);
            MenuCalcul.transform.Find("functionUnique").gameObject.SetActive(false);
        }

        public void listQualitativeInfos()
        {
            CorrespondenceType ct = getCorrespondenceType("Quantitative");
            List<string> m_DropOptions = new List<string>();
            m_DropOptions.Add("Choose a quantitative infos");
            foreach (Correspondence cor in ct.Correspondences)
                if (cor.datatype == "float" || cor.datatype == "number")
                    m_DropOptions.Add(cor.name);

            Dropdown dd = MenuCalcul.transform.Find("newinfos").GetComponent<Dropdown>();
            dd.value = 0;
            dd.ClearOptions();
            dd.AddOptions(m_DropOptions);
        }

        public void addQualitativeInfos()
        {
            int v = MenuCalcul.transform.Find("newinfos").GetComponent<Dropdown>().value;
            if (v > 0)
            {
                CorrespondenceType ct = getCorrespondenceType("Quantitative");
                Correspondence corSelected = null;
                int vi = 0;
                foreach (Correspondence cor in ct.Correspondences)
                    if (cor.datatype == "float" || cor.datatype == "number")
                    {
                        vi += 1;
                        if (vi == v)
                            corSelected = cor;
                    }
                GameObject go = (GameObject)Instantiate(MenuCalcul.transform.Find("defaultInfos").gameObject, MenuCalcul.transform, false);
                go.transform.Find("infos").gameObject.GetComponent<Text>().text = InfosToCompare.Count() + " : " + corSelected.name;
                go.transform.Find("datatype").gameObject.GetComponent<Text>().text = corSelected.datatype;
                go.SetActive(true);
                go.name = corSelected.name;
                go.transform.Translate(0, -24 * InfosToCompare.Count() * InterfaceManager.instance.canvasScale, 0, Space.World);
                InfosToCompare[corSelected.id_infos] = go;
                if (InfosToCompare.Count() == 1)
                {
                    MenuCalcul.transform.Find("functionUnique").gameObject.SetActive(true);
                    MenuCalcul.transform.Find("function").gameObject.SetActive(false);
                    MenuCalcul.transform.Find("apply").gameObject.SetActive(true);
                }
                if (InfosToCompare.Count() >= 2)
                {
                    MenuCalcul.transform.Find("functionUnique").gameObject.SetActive(false);
                    MenuCalcul.transform.Find("function").gameObject.SetActive(true);
                    MenuCalcul.transform.Find("apply").gameObject.SetActive(true);
                }
            }
        }

        public void onChangeFunctionUnique(Int32 v)
        {
            int vv = MenuCalcul.transform.Find("functionUnique").GetComponent<Dropdown>().value;
            if (vv > 0)
            {
                MenuCalcul.transform.Find("apply").gameObject.SetActive(true);
                CorrespondenceType ct = getCorrespondenceType("Quantitative");
                string name = "";
                if (vv == 1)
                    name = "Time(";
                if (vv == 2)
                    name = "STDTime(";
                if (vv == 3)
                    name = "SpaceNeigbhors(";
                if (vv == 4)
                    name = "STDSpaceNeigbhors(";
                if (vv == 5)
                    name = "Normalize(";
                foreach (KeyValuePair<int, GameObject> itc in InfosToCompare)
                {
                    int id_infos = itc.Key;
                    foreach (Correspondence cor in ct.Correspondences)
                        if (cor.id_infos == id_infos)
                            name += cor.name;
                }
                name += ")";
                MenuCalcul.transform.Find("name").GetComponent<InputField>().text = name;
            }
        }

        public void onChangeFunction(Int32 v)
        {
            int vv = MenuCalcul.transform.Find("function").GetComponent<Dropdown>().value;
            if (vv > 0)
            {
                CorrespondenceType ct = getCorrespondenceType("Quantitative");
                string name = "";
                if (vv == 1)
                    name = "Average(";
                if (vv == 2)
                    name = "STD(";
                int i = 0;
                foreach (KeyValuePair<int, GameObject> itc in InfosToCompare)
                {
                    int id_infos = itc.Key;
                    foreach (Correspondence cor in ct.Correspondences)
                        if (cor.id_infos == id_infos)
                            name += cor.name;
                    if (i < InfosToCompare.Count() - 1)
                        name += ",";
                    i += 1;
                }
                name += ")";
                MenuCalcul.transform.Find("name").GetComponent<InputField>().text = name;
            }
        }

        public void calculCompare()
        {
            MorphoDebug.Log("calculCompare on " + InfosToCompare.Count());
            if (InfosToCompare.Count() == 1)
            {
                int vv = MenuCalcul.transform.Find("functionUnique").GetComponent<Dropdown>().value;
                if (vv > 0)
                {
                    MenuCalcul.SetActive(false);
                    CorrespondenceType ct = getCorrespondenceType("Quantitative");
                    string name = MenuCalcul.transform.Find("name").GetComponent<InputField>().text;
                    string uploaddate = DateTime.Now.ToString();
                    Correspondence co = new Correspondence(AttachedDataset.id_dataset, ct, SetsManager.instance.IDUser, 2, -1, name, "", 0, true, "number", uploaddate, 0, this);
                    StartCoroutine(createComparisonInfos(co, vv));
                    ct.addCorrespondence(co);
                    reOrganize();
                }
            }
            if (InfosToCompare.Count() >= 2)
            {
                int vv = MenuCalcul.transform.Find("function").GetComponent<Dropdown>().value;
                if (vv > 0)
                {
                    MenuCalcul.SetActive(false);
                    CorrespondenceType ct = getCorrespondenceType("Quantitative");
                    string name = MenuCalcul.transform.Find("name").GetComponent<InputField>().text;
                    string uploaddate = DateTime.Now.ToString();
                    Correspondence co = new Correspondence(AttachedDataset.id_dataset, ct, SetsManager.instance.IDUser, 2, -1, name, "", 0, true, "number", uploaddate, 0, this);
                    StartCoroutine(createComparisonInfos(co, vv + 5)); //5 -> NUMBER OF UNIQUE
                    ct.addCorrespondence(co);
                    reOrganize();
                }
            }
        }

        public IEnumerator createComparisonInfos(Correspondence cor, int vv)
        {
            cor.inf.transform.Find("load").gameObject.SetActive(false);
            WWWForm form = new WWWForm();
            string dS = SetsManager.instance.urlCorrespondence + "?hash=" + SetsManager.instance.hash + "&download=13&function=" + vv + "&id_dataset=" + cor.id_dataset + "&infos=" + cor.name + "&id_people=" + SetsManager.instance.IDUser;
            form.AddField("download", "13");
            form.AddField("function", vv.ToString());
            form.AddField("id_dataset", AttachedDataset.id_dataset.ToString());
            form.AddField("infos", cor.name.ToString());
            int i = 0;
            foreach (KeyValuePair<int, GameObject> itc in InfosToCompare)
            {
                int id_infos_f = itc.Key;
                form.AddField("id_infos_" + i.ToString(), id_infos_f.ToString());
                dS += "&id_infos_" + i.ToString() + "=" + id_infos_f.ToString();
                i += 1;
            }
            UnityWebRequest www = UnityWebRequests.Post(MorphoTools.GetServer(), "api/createcomparaison/", form);
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }

            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                if (www.downloadHandler.text != "")
                {
                    cor.id_infos = int.Parse(Encoding.UTF8.GetString(www.downloadHandler.data));
                }
                else
                    MorphoDebug.Log("Error Loading " + cor.name);
            }
            www.Dispose();
            downloadDATA(cor);
        }

        public string getLineageMorphoPlot(bool use_names=false)
        {
            foreach (Correspondence c in Correspondences)
            {
                if (c.datatype == "time")
                {
                    return c.GetTxtInfos(add_names:use_names).ToString();
                }
            }

            return "";
        }
    }
}