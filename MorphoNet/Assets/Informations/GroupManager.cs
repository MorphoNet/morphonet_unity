﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AssemblyCSharp;
using UnityEngine.UI;
using System;

namespace MorphoNet
{
    public class GroupObjects
    {
        public GameObject go;
        public GroupManager parent;
        public Cell c;
        private string cellName;
        public float width;
        public bool isView;

        public GroupObjects(Cell c, GameObject grpParent, GroupManager source)
        {
            this.c = c;
            cellName = c.ID;
            isView = false;

            if (grpParent != null)
            {
                parent = source;
                go = SetsManager.instance.InstantiateGO(source.defaultGroup, grpParent.transform);
                go.name = cellName;
                setName(cellName);
                parent.viewListenO(go.transform.Find("def").Find("is").gameObject.GetComponent<Toggle>(), this);
                SetsManager.instance.DestroyGO(go.transform.Find("def").Find("reploy").gameObject);
                SetsManager.instance.DestroyGO(go.transform.Find("def").Find("deploy").gameObject);
                GameObject colorize = go.transform.Find("def").Find("colorize").gameObject;
                Vector3 prevpos = go.transform.Find("def").Find("name").position;
                // this.go.transform.Find("def").Find("is").gameObject.SetActive(true);
                go.transform.Find("def").Find("name").position = new Vector3(colorize.transform.position.x, prevpos.y, prevpos.z);
                colorize.SetActive(false);
            }
        }

        public bool describe()
        {
            if (go != null && go.transform.Find("def").Find("is").GetComponent<Toggle>().isOn != c.selected)
                go.transform.Find("def").Find("is").GetComponent<Toggle>().isOn = c.selected;
            return c.selected;
            //return true;
        }

        public void setName(string name)
        {
            if (go != null)
            {
                GameObject nameGO = go.transform.Find("def").Find("name").gameObject;
                nameGO.GetComponent<Text>().text = name;
                width = nameGO.GetComponent<Text>().preferredWidth;
                RectTransform ct = nameGO.GetComponent<RectTransform>();
                ct.sizeDelta = new Vector2(width, parent.shiftY);
            }
        }

        public void checkWidth(int decalsX)
        {
            if (go != null)
            {
                //float w = (456.2f + (this.go.transform.position.x) / Instantiation.canvasScale);
                float w = decalsX * 20f;
                //MorphoDebug.Log (this.cellName+" -> w=" + w);
                w += +width;
                if (w > parent.maxWidth)
                    parent.maxWidth = w;
            }
        }

        public void draw(float shiftX)
        {
            if (cellName == c.ID)
            {
                if (parent.idXName == -1)
                    parent.idXName = parent.AttachedDataset.Infos.getIdInfosForName();
                if (parent.idXName != -1)
                {
                    cellName = c.getInfos(parent.idXName);
                    if (cellName == "")
                        cellName = c.ID;
                    setName(cellName);
                }
            }
            if (go != null)
            {
                if (shiftX != -1)
                    go.transform.position = go.transform.parent.position - new Vector3(-parent.shiftY * InterfaceManager.instance.canvasScale, shiftX * InterfaceManager.instance.canvasScale, 0f);
                if (shiftX < parent.maxWidth)
                    go.SetActive(true);
            }
        }

        public void undraw()
        {
            if (go != null)
                go.SetActive(false);
        }

        //We we want to see this group
        public void changeView()
        {
            isView = !isView;
            View(isView);
        }

        public void View(bool v)
        {
            //MorphoDebug.Log (" View " + this.cellName+ "-> "+v);
            if (go != null)
                go.transform.Find("def").Find("is").gameObject.GetComponent<Toggle>().isOn = v;
            if (v)
            {
                parent.AttachedDataset.PickedManager.AddCellSelected(c, false);
#if !UNITY_WEBGL
                MorphoTools.SendCellsPickedToLineage();
#endif
            }

            else
            {
                parent.AttachedDataset.PickedManager.RemoveCellSelected(c, false);
            }
        }

        public void shift(float shiftX)
        {
            //MorphoDebug.Log ("Shift  " + this.name+ " at " +shiftX);
            if (go != null)
                go.transform.position = go.transform.position - new Vector3(0, shiftX * InterfaceManager.instance.canvasScale, 0f);
        }

        public int checkScroll(int n, int decals)
        {
            //string sc = ""; for (int i = 0; i < n - decals; i++) sc += this.cellName;
            //this.go.transform.Find ("def").Find ("name").gameObject.GetComponent<Text> ().text = sc;
            if (n - decals >= 0 && n - decals < parent.maxNbDraw)
            {
                if (go != null && !go.activeSelf)
                {
                    if (go != null)
                        go.SetActive(true);
                    parent.nbSCroll += 1;
                }
            }
            else if (go.activeSelf)
            {
                if (go != null)
                    go.SetActive(false);
                parent.nbSCroll += 1;
            }
            return n + 1;
        }

        public void setColor(int selectionValue)
        {
            //Lineage.AddToColor(c, SelectionManager.getSelectedMaterial(selectionValue).color);
            c.addSelection(selectionValue);
        }

        public void clear()
        {
            SetsManager.instance.DestroyGO(go);
        }
    }

    public class Group
    {
        public string name;
        public GroupManager source;
        public List<GroupObjects> Objects = null;
        public List<Group> SubGroups = null;
        public GameObject go;
        public Group parent;
        public bool isdeploy;
        public bool isView;
        public float width;

        public bool active; //When we change time instead of reinitialize everything we keep the active group

        public Group(string name, GameObject grpParent, GroupManager param_source)
        {
            source = param_source;
            //MorphoDebug.Log ("Create group " + name +" "+Tissue.defaultGroup.name);
            isdeploy = false;
            isView = false;
            this.name = name;
            active = true;

            go = SetsManager.instance.InstantiateGO(source.defaultGroup, grpParent.transform);
            go.name = this.name;
            GameObject nameGO = go.transform.Find("def").Find("name").gameObject;
            nameGO.GetComponent<Text>().text = this.name;
            width = nameGO.GetComponent<Text>().preferredWidth;
            RectTransform ct = nameGO.GetComponent<RectTransform>();
            ct.sizeDelta = new Vector2(width, source.shiftY);
            nameGO.GetComponent<Text>().fontStyle = FontStyle.Italic;
            source.viewListen(go.transform.Find("def").Find("is").gameObject.GetComponent<Toggle>(), this);
            source.deployListen(go.transform.Find("def").Find("deploy").gameObject.GetComponent<Button>(), this);
            source.reployListen(go.transform.Find("def").Find("reploy").gameObject.GetComponent<Button>(), this);
            source.colorizeListen(go.transform.Find("def").Find("colorize").gameObject.GetComponent<Button>(), this);
            //this.go.transform.Find("def").Find("deploy").gameObject.SetActive(true);
            go.transform.Find("def").Find("reploy").gameObject.SetActive(false);
        }

        public void addCell(Cell c)
        {
            if (Objects == null)
                Objects = new List<GroupObjects>();
            Objects.Add(new GroupObjects(c, go, source));
        }

        public Group getGroup(string[] names, int s)
        {
            if (names.Length == s)
            {  //We are at the end
                active = true;
                return this;
            }
            //MorphoDebug.Log ("names.Length==" + names.Length + " s=" + s + " -> " + names [s]);

            if (SubGroups == null)
                SubGroups = new List<Group>();

            foreach (Group sg in SubGroups)
                if (sg.name == names[s])
                {
                    active = true;
                    return sg.getGroup(names, s + 1);
                }
            Group gp = new Group(names[s], go, source);
            gp.parent = this;
            //MorphoDebug.Log ("Create group " + gp.name+ " with parent " + gp.parent.name);

            SubGroups.Add(gp);
            return gp.getGroup(names, s + 1);
        }

        public void show(int s)
        {
            string st = "";
            for (int i = 0; i < s; i++)
                st += "-";
            st += "->" + name;
            if (Objects == null)
                st += " contains no cells";
            else
                st += " contains " + Objects.Count + " cells";
            MorphoDebug.Log(st);
            if (SubGroups != null)
                foreach (Group sg in SubGroups)
                    sg.show(s + 1);
        }

        public void checkWidth(int decalsX)
        {
            if (go != null)
            {
                //float w = (456.2f+(this.go.transform.position.x ) / Instantiation.canvasScale);
                float w = decalsX * 20f;
                //MorphoDebug.Log (this.name+" -> w=" + w+" decalsX="+(Instantiation.canvasWidth/2)+ " witdht="+this.width);
                w += +width;
                if (w > source.maxWidth)
                    source.maxWidth = w;
                if (isdeploy)
                {
                    if (SubGroups != null)
                        foreach (Group sg in SubGroups)
                            sg.checkWidth(decalsX + 1);
                    if (Objects != null)
                        foreach (GroupObjects sog in Objects)
                            sog.checkWidth(decalsX + 1);
                }
            }
        }

        public void draw(float shiftX)
        {
            if (go != null)
            {
                if (shiftX != -1)
                    go.transform.position = go.transform.parent.position - new Vector3(-source.shiftY * InterfaceManager.instance.canvasScale, shiftX * InterfaceManager.instance.canvasScale, 0f);
                //else this.go.transform.localPosition = new Vector3 (0f, 0f, 0f);
                go.SetActive(true);
            }
        }

        public void undraw()
        {
            if (go != null)
                go.SetActive(false);
        }

        public void deploy()
        {
            if (go != null)
            {
                go.transform.Find("def").Find("reploy").gameObject.SetActive(true);
                go.transform.Find("def").Find("deploy").gameObject.SetActive(false);

                isdeploy = true;
                int isg = 1;
                if (SubGroups != null)
                    foreach (Group sg in SubGroups)
                    {
                        sg.draw(source.shift * isg);
                        isg += 1;
                    }
                if (Objects != null)
                    foreach (GroupObjects sog in Objects)
                    {
                        sog.draw(source.shift * isg);
                        isg += 1;
                    }

                shiftNext(isg - 1);
            }
        }

        public void shift(float shiftX)
        {
            //MorphoDebug.Log ("Shift  " + this.name+ " at " +shiftX);
            if (go != null)
                go.transform.position = go.transform.position - new Vector3(0, shiftX * InterfaceManager.instance.canvasScale, 0f);
        }

        //We have to shift the next SubGroup
        public void shiftNext(int nbShift)
        {
            //MorphoDebug.Log ("Shift Next " + this.name);
            if (name != source.mainGroupName)
            {
                //MorphoDebug.Log ("Parent " + this.parent.name+" ->"+ this.parent.SubGroups.Count);
                bool isShift = false;
                foreach (Group sg in parent.SubGroups)
                {
                    if (sg.name == name)
                        isShift = true;
                    else if (isShift)
                    {
                        sg.shift(nbShift * source.shift);
                    }
                }
                //We also have to shift the potentiel objects
                if (parent.Objects != null)
                    foreach (GroupObjects sog in parent.Objects)
                        sog.shift(nbShift * source.shift);

                parent.shiftNext(nbShift);
            }
        }

        public void reploy()
        {
            if (go != null)
            {
                go.transform.Find("def").Find("reploy").gameObject.SetActive(false);
                go.transform.Find("def").Find("deploy").gameObject.SetActive(true);

                isdeploy = false;
                int isg = 1;
                if (SubGroups != null)
                {
                    foreach (Group sg in SubGroups)
                    {
                        sg.undraw();
                        isg += 1;
                        if (sg.isdeploy)
                            sg.reploy();
                    }
                }

                if (Objects != null)
                {
                    foreach (GroupObjects sog in Objects)
                    {
                        sog.undraw();
                        isg += 1;
                    }
                }
                shiftNext(-isg + 1);
            }
        }

        //We we want to see this group
        public void View(bool v)
        {
            //MorphoDebug.Log (" View Group " + this.name+ "-> "+v);
            if (go != null)
                go.transform.Find("def").Find("is").gameObject.GetComponent<Toggle>().isOn = v;
            if (SubGroups != null)
            {
                foreach (Group sg in SubGroups)
                {
                    sg.View(v);
                }
            }
            if (Objects != null)
            {
                foreach (GroupObjects sog in Objects)
                {
                    sog.View(v);
                }
            }
        }

        //We we want to see this group
        public void changeView()
        {
            //MorphoDebug.Log ("changeView=" + this.isView);
            isView = !isView;
            View(isView);
        }

        //Return the total deploy size
        public int getHeight()
        {
            int height = 1;
            if (isdeploy)
            {
                if (SubGroups != null)
                {
                    foreach (Group sg in SubGroups)
                        height += sg.getHeight();
                }
                if (Objects != null)
                {
                    //MorphoDebug.Log ("Objec topen " + this.name + " contains " + this.Objects.Count);
                    height += Objects.Count;
                }
            }
            return height;
        }

        public int checkScroll(int n, int decals)
        {
            if (n - decals >= 0 && n - decals < source.maxNbDraw)
            {
                if (go != null && !go.transform.Find("def").gameObject.activeSelf)
                {
                    go.transform.Find("def").gameObject.SetActive(true);
                    source.nbSCroll += 1;
                }
            }
            else if (go != null && go.transform.Find("def").gameObject.activeSelf)
            {
                go.transform.Find("def").gameObject.SetActive(false);
                source.nbSCroll += 1;
            }

            n += 1;
            if (isdeploy)
            {
                if (SubGroups != null)
                    foreach (Group sg in SubGroups)
                        n = sg.checkScroll(n, decals);
                if (Objects != null)
                    foreach (GroupObjects sog in Objects)
                        n = sog.checkScroll(n, decals);
            }
            return n;
        }

        public bool describe()
        {
            //MorphoDebug.Log ("Describe " + this.name);
            if (Objects == null && SubGroups == null && go != null)
                go.transform.Find("def").Find("deploy").gameObject.SetActive(false);
            if (isdeploy)
                deploy();
            bool allOn = true;
            if (SubGroups != null)
                foreach (Group sg in SubGroups)
                    if (!sg.describe())
                        allOn = false;
            if (Objects != null)
                foreach (GroupObjects sog in Objects)
                    if (!sog.describe())
                        allOn = false;
            if (Objects == null && SubGroups == null)
                allOn = false;
            //MorphoDebug.Log (" Describe " + this.name+ "-> "+allOn);
            if (go != null)
                go.transform.Find("def").Find("is").gameObject.GetComponent<Toggle>().isOn = allOn;
            isView = allOn;
            return allOn;
        }

        //Choose sequential color for all this subgroups element
        public void colorize()
        {
            SetsManager.instance.StartCoroutine(colorize_coroutine());
        }

        public IEnumerator colorize_coroutine()
        {
            //if (Lineage.isLineage) Lineage.colorizeGroup (this);
            int selectionValue = 1;
            if (SubGroups != null)
                foreach (Group sg in SubGroups)
                {
                    sg.setColor(selectionValue);
                    selectionValue++;
                    if (Lineage.isLineage)
                        Lineage.UpdateWebsiteLineage();
                    yield return new WaitForEndOfFrame();
                }

            if (Objects != null)
                foreach (GroupObjects sog in Objects)
                {
                    sog.setColor(selectionValue);
                    selectionValue++;
                    if (Lineage.isLineage)
                        Lineage.UpdateWebsiteLineage();
                    yield return new WaitForEndOfFrame();
                }
        }

        public void setColor(int selection)
        {
            if (SubGroups != null)
                foreach (Group sg in SubGroups)
                    sg.setColor(selection);
            if (Objects != null)
                foreach (GroupObjects sog in Objects)
                    sog.setColor(selection);
        }

        //Remove unactive groups
        public void purgeGroups()
        {
            if (!active)
            {
                clear();
            }
            else
            {
                if (SubGroups != null)
                {
                    List<Group> TempSubGroups = new List<Group>();
                    foreach (Group sg in SubGroups)
                    {
                        sg.purgeGroups();
                        if (sg.active)
                            TempSubGroups.Add(sg);
                    }
                    if (TempSubGroups.Count > 0)
                        SubGroups = TempSubGroups;
                    else
                    {
                        SubGroups.Clear();
                        SubGroups = null;
                    }
                }
            }
        }

        //Remove all childs ...
        public void clear()
        {
            //MorphoDebug.Log ("Clear GRP");
            SetsManager.instance.DestroyGO(go);
            if (SubGroups != null)
            {
                foreach (Group sg in SubGroups)
                    sg.clear();
                SubGroups.Clear();
            }
            SubGroups = null;
            if (Objects != null)
            {
                foreach (GroupObjects sog in Objects)
                    sog.clear();
                Objects.Clear();
            }
            Objects = null;
        }

        //Remove only cells childs ...
        public void clearCells()
        {
            active = false;
            if (SubGroups != null)
            {
                foreach (Group sg in SubGroups)
                    sg.clearCells();
            }
            if (Objects != null)
            {
                foreach (GroupObjects sog in Objects)
                    sog.clear();
                Objects.Clear();
            }
        }
    }

    public class GroupManager : MonoBehaviour
    {
        public DataSet AttachedDataset;
        public int CurrentTime = -1;
        public Dictionary<int, string> Tissues;
        public Dropdown TissueDropDown;
        public GameObject MenuGroups;
        public GameObject defaultGroup;
        public int drawingTime = -2;
        public int idXName = -1;
        public GameObject scrollBar;

        public int shift = 20;
        public int shiftY = 20;

        public Group grp;
        public Dictionary<int, string> nbFates;

        public string mainGroupName = " Visualize Objects Group By";

        public void AttachDataset(DataSet source)
        {
            AttachedDataset = source;
        }

        public void init(int nbInfo, string infoname)
        {
            //Tissue.nbFate = nbInfo;
            if (nbFates == null)
                nbFates = new Dictionary<int, string>();
            if (MenuGroups == null)
            {
                MenuGroups = InterfaceManager.instance.MenuGroup;
                MenuGroups.SetActive(true);
                defaultGroup = InterfaceManager.instance.default_group;
                defaultGroup.SetActive(false);
                InterfaceManager.instance.BoutonGroups.SetActive(true);
                InterfaceManager.instance.containerGroups.SetActive(true);
                if (scrollBar == null)
                    scrollBar = InterfaceManager.instance.scrollbar_menugroup;
                scrollBar.SetActive(false);
                grp = new Group(mainGroupName, MenuGroups, this);
                deploy(grp);
                grp.go.transform.Find("def").Find("is").gameObject.SetActive(false);
                grp.go.transform.Find("def").Find("deploy").gameObject.SetActive(false);
                grp.go.transform.Find("def").Find("colorize").gameObject.SetActive(false);
                grp.go.transform.Find("def").Find("reploy").gameObject.SetActive(false);
                grp.go.transform.Find("def").Find("name").transform.Translate(new Vector3(-40 * InterfaceManager.instance.canvasScale, 0, 0));
            }
            MenuGroups.SetActive(false);
            nbFates[nbInfo] = infoname;
            drawingTime = -1;
        }

        public void onMenuClick()
        {
            describe();
        }

        public void OnEnable()
        {
            describe();
        }

        //Set a specific time point
        public void setTime(int t)
        {
            //MorphoDebug.Log("Debut setTime tissue ! ");
            //MorphoDebug.Log ("SET TIME at " + t);
            grp.clearCells();
            grp.active = true;
            foreach (var nbFate in nbFates)
                grp.getGroup(nbFate.Value.Split(':'), 0).active = true;

            CurrentTime = t;
            List<Cell> CellAtT = AttachedDataset.CellsByTimePoint[t];
            foreach (Cell cell in CellAtT)
            {
                foreach (var nbFate in nbFates)
                {
                    string infos = cell.getInfos(nbFate.Key);
                    if (infos != "")
                    {
                        string[] infoSplit = infos.Split(';');
                        foreach (string info in infoSplit)
                        {
                            addCell((nbFate.Value + ":" + info).Split(':'), cell);
                        }
                    }
                }
            }
            //Now We Purge the unactive groups
            grp.purgeGroups();
            grp.draw(-1);
            updateWidth();
            updateHeight();
            //grp.show(1); //Diplay group on debug
            //MorphoDebug.Log("Fin SetTime tissue ! ");
        }

        //Add a cell (and creaet subgroup if necessary)
        public void addCell(string[] names, Cell c)
        {
            Group sg = grp.getGroup(names, 0);
            //MorphoDebug.Log (" Found " + sg.name+ " for " +c.ID+" at " +c.t);
            if (c != null)
                sg.addCell(c);
        }

        //DEPLOY A GROUP
        public void deployListen(Button b, Group gp)
        { b.onClick.AddListener(() => deploy(gp)); }

        public void deploy(Group gp)
        { gp.deploy(); updateHeight(); updateWidth(); nbSCroll = 1; }

        //REPLOY A GROYP
        public void reployListen(Button b, Group gp)
        { b.onClick.AddListener(() => reploy(gp)); }

        public void reploy(Group gp)
        { gp.reploy(); updateHeight(); updateWidth(); nbSCroll = 1; }

        //VIEW
        public void viewListen(Toggle b, Group gp)
        { b.onValueChanged.AddListener(delegate { View(gp); }); }

        public void View(Group gp)
        {
            if (!blockToogle)
            {
                blockToogle = true;
                gp.changeView();
                AttachedDataset.Infos.describe();
                blockToogle = false;
            }
        }

        public void viewListenO(Toggle b, GroupObjects gop)
        { b.onValueChanged.AddListener(delegate { ViewO(gop); }); }

        public void ViewO(GroupObjects gop)
        { gop.changeView(); }

        //FOR SCROLL BAR
        public int maxNbDraw = 34;

        public float sizeBar = 20f;

        public void updateHeight()
        {
            int height = grp.getHeight();
            //MorphoDebug.Log ("updateHeight=" + height);
            if (height > maxNbDraw)
            {
                scrollBar.SetActive(true);
                //DEFINE SCROLLBAR LENGTH
                scrollBar.GetComponent<Scrollbar>().numberOfSteps = 1 + height - maxNbDraw;
                scrollBar.GetComponent<Scrollbar>().size = 1f / (float)(1 + height - maxNbDraw);
                grp.checkScroll(0, 0);
            }
            else
            {
                scrollBar.GetComponent<Scrollbar>().numberOfSteps = 1;
                scrollBar.GetComponent<Scrollbar>().size = 1f;
                scrollBar.SetActive(false);
                grp.go.transform.position = new Vector3(grp.go.transform.position.x, -0.1460054f, 0f);//RESET POSITION
                nbSCroll = 1;
            }
        }

        public int nbSCroll = 1;

        public void onSroll(Single value)
        {
            //MorphoDebug.Log("onSroll");
            int nbSteps = scrollBar.GetComponent<Scrollbar>().numberOfSteps;
            int decals = (int)Mathf.Round(nbSteps - value * (nbSteps - 1) - 1);
            grp.go.transform.localPosition = new Vector3(12f, decals * shiftY, 0f);
            nbSCroll = 0;
            grp.checkScroll(0, decals);
        }

        private void Update()
        {
            if (InterfaceManager.instance.MenuGroup.activeSelf)
            { if (scrollBar != null && nbSCroll > 0) onSroll(scrollBar.GetComponent<Scrollbar>().value); } //PB elle s'active tout le temps  !!
        }

        //FOR WIDTH LARGER
        public float heightGroups = 675;

        public float maxWidth = 0;

        public void updateWidth()
        {
            //MorphoDebug.Log("updateWith");
            maxWidth = 0;
            grp.checkWidth(0);
            RectTransform ct = MenuGroups.GetComponent<RectTransform>();
            if (maxWidth > 150)
                ct.sizeDelta = new Vector2(maxWidth + 80, heightGroups);
            else
                ct.sizeDelta = new Vector2(230, heightGroups);
        }

        //Just view reorganizeation
        public bool blockToogle = false; //When we update the toggle values from outise we want to block the listening operation

        public void describe()
        {
            //MorphoDebug.Log("Debut describe ! ");
            if (nbFates != null && MenuGroups.activeSelf)
            {
                blockToogle = true;
                if (AttachedDataset.CurrentTime != CurrentTime)
                    setTime(AttachedDataset.CurrentTime);
                //MorphoDebug.Log ("DESCRIBE");
                grp.describe();
                blockToogle = false;
            }
            //  MorphoDebug.Log("Fin describe ! ");
        }

        //RANDOM COLORIZE
        public void colorizeListen(Button b, Group gp)
        { b.onClick.AddListener(() => colorize(gp)); }

        public void colorize(Group gp)
        { gp.colorize(); }
    }
}