﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using MorphoNet.UI;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using MorphoNet.Extensions.Unity;

namespace MorphoNet
{
    public class RawImages : MonoBehaviour, IMenuController<IRawDataUI>
    {
        #region Shaders Properties Names

        
        private const string _SP_MainTex = "_MainTex";

        private const string _SP_XSize = "_XSize";
        private const string _SP_YSize = "_YSize";
        private const string _SP_ZSize = "_ZSize";

        private const string _SP_ThresholdC0 = "_ThresholdC0";
        private const string _SP_ThresholdMaxC0 = "_ThresholdMaxC0";
        private const string _SP_TransparencyC0 = "_TransparencyC0";
        private const string _SP_IntensityC0 = "_IntensityC0";
        private const string _SP_ColorBarTexC0 = "_ColorBarTexC0";
        private const string _SP_Channel0Active = "_C0";
        private const string _SP_AlphaXMinC0 = "_AlphaXMinC0";
        private const string _SP_AlphaXMaxC0 = "_AlphaXMaxC0";
        private const string _SP_AlphaYMinC0 = "_AlphaYMinC0";
        private const string _SP_AlphaYMaxC0 = "_AlphaYMaxC0";
        private const string _SP_MinScalingC0 = "_MinScalingC0";
        private const string _SP_MaxScalingC0 = "_MaxScalingC0";

        private const string _SP_ThresholdC1 = "_ThresholdC1";
        private const string _SP_ThresholdMaxC1 = "_ThresholdMaxC1";
        private const string _SP_TransparencyC1 = "_TransparencyC1";
        private const string _SP_IntensityC1 = "_IntensityC1";
        private const string _SP_ColorBarTexC1 = "_ColorBarTexC1";
        private const string _SP_Channel1Active = "_C1";
        private const string _SP_AlphaXMinC1 = "_AlphaXMinC1";
        private const string _SP_AlphaXMaxC1 = "_AlphaXMaxC1";
        private const string _SP_AlphaYMinC1 = "_AlphaYMinC1";
        private const string _SP_AlphaYMaxC1 = "_AlphaYMaxC1";
        private const string _SP_MinScalingC1 = "_MinScalingC1";
        private const string _SP_MaxScalingC1 = "_MaxScalingC1";

        private const string _SP_ThresholdC2 = "_ThresholdC2";
        private const string _SP_ThresholdMaxC2 = "_ThresholdMaxC2";
        private const string _SP_TransparencyC2 = "_TransparencyC2";
        private const string _SP_IntensityC2 = "_IntensityC2";
        private const string _SP_ColorBarTexC2 = "_ColorBarTexC2";
        private const string _SP_Channel2Active = "_C2";
        private const string _SP_AlphaXMinC2 = "_AlphaXMinC2";
        private const string _SP_AlphaXMaxC2 = "_AlphaXMaxC2";
        private const string _SP_AlphaYMinC2 = "_AlphaYMinC2";
        private const string _SP_AlphaYMaxC2 = "_AlphaYMaxC2";
        private const string _SP_MinScalingC2 = "_MinScalingC2";
        private const string _SP_MaxScalingC2 = "_MaxScalingC2";

        private const string _SP_ThresholdC3 = "_ThresholdC3";
        private const string _SP_ThresholdMaxC3 = "_ThresholdMaxC3";
        private const string _SP_TransparencyC3 = "_TransparencyC3";
        private const string _SP_IntensityC3 = "_IntensityC3";
        private const string _SP_ColorBarTexC3 = "_ColorBarTexC3";
        private const string _SP_Channel3Active = "_C3";
        private const string _SP_AlphaXMinC3 = "_AlphaXMinC3";
        private const string _SP_AlphaXMaxC3 = "_AlphaXMaxC3";
        private const string _SP_AlphaYMinC3 = "_AlphaYMinC3";
        private const string _SP_AlphaYMaxC3 = "_AlphaYMaxC3";
        private const string _SP_MinScalingC3 = "_MinScalingC3";
        private const string _SP_MaxScalingC3 = "_MaxScalingC3";

        private const string _SP_Thickness = "_Thickness";
        private const string _SP_ToggleFreeCuttingPlane = "_ToggleFreeCuttingPlane";
        private const string _SP_FreeCuttingPlaneRepresentation = "_FreeCuttingPlaneRepresentation";
        private const string _SP_RawClippingPlaneActivated = "_ClippingPlaneActivated";
        private const string _SP_RawClippingPlane = "_ClippingPlane";
        private const string _SP_RawClippingThinMode = "_ClippingPlaneThin";
        private const string _SP_MeshThinMode = "_ThinMode";
        private const string _SP_OffsetPlan = "_OffsetPlan";

        private const string _SP_DoClip = "_DoClip";


        #endregion Shaders Properties Names

        #region Input Properties Names

        private const string _Input_ScrollWheel = "Mouse ScrollWheel";

        #endregion Input Properties Names

        #region IMenuController<IRwaDataUI> Properties

        public List<IRawDataUI> RawDataUIs = new List<IRawDataUI>();

        public event Action<IRawDataUI> OnUIParametersChange;

        #endregion IMenuController<IRwaDataUI> Properties

        public DataSet dataset;

        public void AttachDataset(DataSet d)
        {
            dataset = d;
        }

        public float initScale = 0.01392758F;
        public bool RawImagesAreVisible = false;
        public bool MeshIsCut { get => _MeshIsCut; private set => _MeshIsCut = value; }
        public GameObject CubePlan;
        public GameObject CubeScale;
        public Renderer Cube;
        public GameObject XPlan;
        public GameObject YPlan;
        public GameObject ZPlan;
        public List<Texture3D> textures;
        public Texture3D texture;

        private Texture3D[] _TempTextures;

        private bool _RawClippingPlaneActivated = false;

        public float PlaneValue { get => _PlaneValue; set => _PlaneValue = Mathf.Clamp(value, _MinPlaneValue, _MaxPlaneValue); }
        private float _PlaneValue = 0.5f; //plane position
        private float _MinPlaneValue=0f;
        private float _MaxPlaneValue=1f;

        //Thin plane or cutting plane
        private bool _ThinPlanMode = false;
        public bool ThinPlaneMode { get => _ThinPlanMode; set => _ThinPlanMode = value; }

        public float ThicknessValue { get => _ThicknessValue; set => _ThicknessValue = Mathf.Clamp(value, _MinThickness, _MaxThickness); }
        private float _ThicknessValue = 0.05f; //Plane thickness
        private const float _MaxThickness = 3f;
        private const float _MinThickness = 0.05f;

        //"axis" of plane : 0=x,1=y,2=z,3=free
        private int _PlaneAxis = 0;
        private int _MinPlaneAxis = 0;
        private int _MaxPlaneAxis = 3;
        public int PlaneAxis { get => _PlaneAxis; set => _PlaneAxis = Mathf.Clamp(value, _MinPlaneAxis, _MaxPlaneAxis); }

        private bool _PlanInverse = false;
        public bool PlanInverse { get => _PlanInverse; set => _PlanInverse = value; }

        //channel change UI
        public Text channelText;

        public GameObject channelNext;
        public GameObject channelPrevious;

        //Cut Meshes
        public Vector4 FreeMeshesCutNorm = default;

        public Vector4 XMeshesCutNorm;
        public Vector4 YMeshesCutNorm;
        public Vector4 ZMeshesCutNorm;

        public GameObject CurationButton;

        public int t_downloaded = -1;
        public int[] RawDim;
        public int[] OriginalRawDim;
        public float[] VoxelSize;
        public float scaleFactor = 1;
        public float zScaleFactor = 1;
        public float rescaleFactor = 1f;


        private Vector3 _CubePosition = Vector3.zero;

        //The Downloaded
        public Coroutine rawImagesDownloaded;

        public UnityWebRequest wbrImages;

        private Coroutine _RawImageTextureLoading;

        //Moving
        private bool _Moving = false;

        private bool _Downloaded = false;

        //test : array of dicts to consider channels
        public List<Dictionary<int, string>> imagepathAt = new List<Dictionary<int, string>>();

        public List<Dictionary<int, string>> sizeAt = new List<Dictionary<int, string>>();
        public List<Dictionary<int, string>> voxel_sizeAt = new List<Dictionary<int, string>>();
        public List<Dictionary<int, float>> scaleAt = new List<Dictionary<int, float>>();
        public List<Dictionary<int, bool>> bundleAt = new List<Dictionary<int, bool>>();

        public List<Dictionary<int, int>> MinValuesAt = new List<Dictionary<int, int>>();
        public List<Dictionary<int, int>> MaxValuesAt = new List<Dictionary<int, int>>();

        private int _CurrentChannel = 0;
        private int _ChannelMin = 0;
        private int _ChannelMax = 0;
        private bool _ChangedChannel = false;

        private ChannelShaderParams[] ShaderChannelParams;

        public Dropdown MapColor;

        private bool _MeshIsCut = false;
        private Vector4 _MeshFreeCuttingPlaneRepresentation;

        //optimal params as calculated at first loading
        private bool _AutoShaderParamsComputed = false;

        private bool _NetDatasetFullyLoaded = false;

        public void Start()
        {
            //default: inactive
            //InterfaceManager.instance.containerImages.SetActive(false);

            RawDim = new int[3];
            OriginalRawDim = new int[3];
            VoxelSize = new float[3];
            VoxelSize[0] = 1f;
            VoxelSize[1] = 1f;
            VoxelSize[2] = 1f;
            //meshesCut = InterfaceManager.instance.meshesCut;
            CubePlan = InterfaceManager.instance.CubePlan;
            CubeScale = InterfaceManager.instance.CubeScale;
            Cube = InterfaceManager.instance.Cube;
            XPlan = InterfaceManager.instance.XPlan;
            YPlan = InterfaceManager.instance.YPlan;
            ZPlan = InterfaceManager.instance.ZPlan;

            CubeScale.SetActive(false);



            SetsManager.instance.Default.UpdateFloatProperty(_SP_DoClip, 0f);

            rawImagesDownloaded = null;
            initCubeScale();
            
            if ((!MorphoTools.GetPlotIsActive() && !_Downloaded))
                StartCoroutine(GetDimensionsAndPath());


            var desktopUI = InterfaceManager.instance.MenuImages;
            desktopUI.SetController(this);
        }


    public void initCubeScale()
    {
        initScale = InterfaceManager.instance.initCanvasDatasetScale;
        CubeScale.transform.localScale = new Vector3(initScale * rescaleFactor, initScale * rescaleFactor, initScale * rescaleFactor);
       
        if (!MorphoTools.GetPlotIsActive()) CubeScale.transform.localPosition = new Vector3(dataset.embryoCenter.x * InterfaceManager.instance.initCanvasDatasetScale, -dataset.embryoCenter.y * InterfaceManager.instance.initCanvasDatasetScale, -dataset.embryoCenter.z * InterfaceManager.instance.initCanvasDatasetScale);
        else CubeScale.transform.localPosition = new Vector3(-dataset.embryoCenter.x * InterfaceManager.instance.initCanvasDatasetScale, -dataset.embryoCenter.y * InterfaceManager.instance.initCanvasDatasetScale, -dataset.embryoCenter.z * InterfaceManager.instance.initCanvasDatasetScale);

    }

        public void SetChannels(int channel_min,int channel_max)
        {
            RawImagesAreVisible = true;

            _ChannelMin = channel_min;
            _ChannelMax = channel_max;
            _CurrentChannel = _ChannelMin;

            ShaderChannelParams = new ChannelShaderParams[channel_max + 1];
            _TempTextures = new Texture3D[channel_max - channel_min + 1];

            for (int i = 0; i <= channel_max-channel_min; i++)
            {
                sizeAt.Add(new Dictionary<int, string>());
                imagepathAt.Add(new Dictionary<int, string>());
                voxel_sizeAt.Add(new Dictionary<int, string>());
                scaleAt.Add(new Dictionary<int, float>());
                bundleAt.Add(new Dictionary<int, bool>());

                MinValuesAt.Add(new Dictionary<int, int>());
                MaxValuesAt.Add(new Dictionary<int, int>());
                for(int j = MorphoTools.GetDataset().MinTime; j <= MorphoTools.GetDataset().MaxTime; j++)
                {
                    MinValuesAt[i].Add(j, 0);
                    MaxValuesAt[i].Add(j, 255);
                }

                ShaderChannelParams[i] = new ChannelShaderParams();
                ShaderChannelParams[i].ColorBarTexture = 3;//default : set to gray raw

                if (channel_max > 0)
                    ShaderChannelParams[i].ColorBarTexture = i;

                _TempTextures[i] = null;
            }
            //do not show channels that do not exist
            if(channel_max - channel_min < 2)
                Cube.material.UpdateFloatProperty(_SP_Channel2Active, 0);
            if (channel_max - channel_min < 1)
                Cube.material.UpdateFloatProperty(_SP_Channel1Active, 0);

            InterfaceManager.instance.MenuImages.SetChannels(channel_max);

            var desktopUI = InterfaceManager.instance.MenuImages;
            RegisterUI(desktopUI);
        }

        public void SetHistogram(long[] histogram, int channel, long max)
        {
            ShaderChannelParams[channel].Histogram = histogram;
            ShaderChannelParams[channel].HistoMax = max;
        }

        public long[] GetHistogram(int channel)
        {
            return ShaderChannelParams[channel].Histogram;
        }

        public long GetHistogramMax(int channel)
        {
            return ShaderChannelParams[channel].HistoMax;
        }

        public Vector2 GetHistoMinAlphaPoint(int channel)
        {
            return ShaderChannelParams[channel].AlphaMin;
        }

        public Vector2 GetHistoMaxAlphaPoint(int channel)
        {
            return ShaderChannelParams[channel].AlphaMax;
        }


        public IEnumerator GetDimensionsAndPath() //GET ALL IMAGES DIMENSION AT ALL TIME STEP
        {
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/getrawimages/?id_dataset=" + dataset.id_dataset.ToString());
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
                MorphoDebug.Log("Error on get RawImages " + www.error);
            else
            {
                var N = JSONNode.Parse(www.downloadHandler.text);

                if (N.Count > 0)
                {
                    //init the arrays of dict first
                    int channelMax = -1;
                    int channelMin = int.MaxValue;
                    for (int i = 0; i < N.Count; i++) // id,t,size,scale,imagepath,channel
                    {
                        int c = N[i]["channel"].AsInt;
                        if (c > channelMax)
                        {
                            channelMax = c;
                        }
                        if (c < channelMin)
                        {
                            channelMin = c;
                        }
                    }
                    SetChannels(channelMin, channelMax);

                    for (int i = 0; i < N.Count; i++) // id,t,size,scale,imagepath,channel
                    {
                        int t = N[i]["t"].AsInt;
                        int c = N[i]["channel"].AsInt;
                        sizeAt[c][t] = N[i]["size"].Value.Replace('(', ' ').Replace(')', ' ').Trim();
                        scaleAt[c][t] = UtilsManager.convertJSONToFloat(N[i]["scale"]);
                        imagepathAt[c][t] = MorphoTools.GetServer() + "RAWIMAGES/" + N[i]["imagepath"].Value;
                        voxel_sizeAt[c][t] = N[i]["voxel_size"].Value.Trim();
                        bundleAt[c][t] = false;

                        MinValuesAt[c][t] = 0;
                        MaxValuesAt[c][t] = 255;
                        if (N[i]["link"].Value != "" && N[i]["link"].Value != null && N[i]["link"].Value != "null")
                        {
                            imagepathAt[c][t] = N[i]["link"].Value;
                            bundleAt[c][t] = true;
                        }
                        InterfaceManager.instance.MenuImages.SetImagePropertyLabels(sizeAt[c][t], voxel_sizeAt[c][t], scaleAt[c][t].ToString());
                    }
                }
                _Downloaded = true;
                //showRawImages();
            }
        }

        public bool ShaderParamsInitialized()
        {
            if (ShaderChannelParams == null)
                return false;
            if (ShaderChannelParams.Length == 0)
                return false;
            return true;
        }

        public void SetTempTexture(Texture3D t, int channel)
        {
            _TempTextures[channel] = t;
        }

        public bool AllTempTexturesSet()
        {
            foreach(Texture3D t in _TempTextures)
            {
                if (t == null)
                    return false;
            }
            return true;
        }

        public void Update()
        {
            OnUpdate();
        }

        private void OnUpdate()
        {
            if (!RawImagesAreVisible)
                return;


            Cube.material.SetVector(_SP_OffsetPlan, Cube.transform.position);
            //on move/rotate update planes if active

            bool moving = (Input.GetMouseButton(0) || Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.Space));
#if UNITY_STANDALONE_OSX
            moving = (Input.GetMouseButton(0) || Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.LeftControl));
#endif

            if (_RawClippingPlaneActivated)// && ( moving || Input.mouseScrollDelta.y != 0f))
            {
                ActivateRawDataMultiPlanCutting((new RawDataSource(this)));
                if (_MeshIsCut)
                {
                    Vector4 oldRep = _MeshFreeCuttingPlaneRepresentation;
                    _MeshFreeCuttingPlaneRepresentation = GetActivePlane();
                    _MeshFreeCuttingPlaneRepresentation.x *= -1;
                    _MeshFreeCuttingPlaneRepresentation.y *= -1;
                    _MeshFreeCuttingPlaneRepresentation.z *= -1;
                    if(oldRep != _MeshFreeCuttingPlaneRepresentation)
                        updateMeshShader();
                }
            }



        }


        public void changeTimeStep()
        {
            if (!MorphoTools.GetPlotIsActive() && !_NetDatasetFullyLoaded)
                return;
            showRawImages();
        }


        public int GetCurrentChannel() => _CurrentChannel;

        public void SetCurrentChannel(int value) 
        {
            _CurrentChannel = value;
            //TODO : on change channel, properly update GUI, always!
            UpdateUIFrom(new RawDataSource(this));
        }

        public void showRawImages()
        {
            //if no params, should leave this function
            if (!ShaderParamsInitialized())
                return;

            bool all_channels_active = false;
            foreach(var p in ShaderChannelParams)
            {
                if (p.IsActive)
                    all_channels_active = true;
            }

            if (RawImagesAreVisible && (t_downloaded != dataset.CurrentTime || _ChangedChannel) && all_channels_active)
            {
                _ChangedChannel = false;
                t_downloaded = dataset.CurrentTime;

                if (MorphoTools.GetPlotIsActive() || LoadParameters.instance.id_dataset == 0)
                {  //DIRECT PLOT
                    InterfaceManager.instance.DirectPlot.show_raw();
                }
                else if (sizeAt[_CurrentChannel].ContainsKey(t_downloaded))
                {
                    string[] sizet = sizeAt[_CurrentChannel][t_downloaded].Split(',');
                    string[] voxel_sizet = voxel_sizeAt[_CurrentChannel][t_downloaded].Replace("[","").Replace("]", "").Split(',');

                    scaleFactor = scaleAt[_CurrentChannel][t_downloaded];
                    OriginalRawDim[0] = Mathf.RoundToInt(int.Parse(sizet[0].Trim(), CultureInfo.InvariantCulture) * scaleFactor);
                    OriginalRawDim[1] = Mathf.RoundToInt(int.Parse(sizet[1].Trim(), CultureInfo.InvariantCulture) * scaleFactor);
                    OriginalRawDim[2] = Mathf.RoundToInt(int.Parse(sizet[2].Trim(), CultureInfo.InvariantCulture) * scaleFactor);

                    RawDim[0] = Mathf.RoundToInt(int.Parse(sizet[0].Trim(), CultureInfo.InvariantCulture) * scaleFactor * float.Parse(voxel_sizet[0].Trim(), CultureInfo.InvariantCulture));
                    RawDim[1] = Mathf.RoundToInt(int.Parse(sizet[1].Trim(), CultureInfo.InvariantCulture) * scaleFactor * float.Parse(voxel_sizet[1].Trim(), CultureInfo.InvariantCulture));
                    RawDim[2] = Mathf.RoundToInt(int.Parse(sizet[2].Trim(), CultureInfo.InvariantCulture) * scaleFactor * float.Parse(voxel_sizet[2].Trim(), CultureInfo.InvariantCulture));
                    VoxelSize[0] = float.Parse(voxel_sizet[0].Trim(), CultureInfo.InvariantCulture);
                    VoxelSize[1] = float.Parse(voxel_sizet[1].Trim(), CultureInfo.InvariantCulture);
                    VoxelSize[2] = float.Parse(voxel_sizet[2].Trim(), CultureInfo.InvariantCulture);

                    if (rawImagesDownloaded != null)
                    {
                        if (wbrImages != null)
                        { wbrImages.Abort(); }
                        StopCoroutine(rawImagesDownloaded);
                    }
                    //reset temp textures before we load
                    _TempTextures = new Texture3D[_ChannelMax - _ChannelMin + 1];
                    for (int i = 0; i <= _ChannelMax - _ChannelMin; i++)
                        _TempTextures[i] = null;
                    //here download all channels & mix together
                    rawImagesDownloaded = StartCoroutine(GetTextures());


                }
            }
            else if (!RawImagesAreVisible)
            {
                CubePlan.SetActive(false);
                CubeScale.SetActive(false); //We can direct reactive the cube
            }
            else
            {
                CubePlan.SetActive(true);
                CubeScale.SetActive(true); //We can direct reactive the cube
            }
        }

        /// <summary>
        /// Coroutine to get texture or textures for time step. Assembles up to 3 textures from different channels onto 1 RGB texture and sets the shader.
        /// </summary>
        /// <returns>Coroutine</returns>
        public IEnumerator GetTextures()
        {
            //Coroutine current_running;
            for (int i = 0; i <= _ChannelMax - _ChannelMin; i++)
            {
                if (bundleAt[_CurrentChannel][t_downloaded] && imagepathAt[_CurrentChannel][t_downloaded].Contains("RAWBUNDLE"))
                    StartCoroutine(DataLoader.instance.Get3DTextureAssetBundle(imagepathAt[i][t_downloaded], this,i));
                else
                    StartCoroutine(DataLoader.instance.Get3DTexture(imagepathAt[i][t_downloaded], this,i));
            }
            //wait for all channels to be downloaded
            yield return new WaitUntil(() => AllTempTexturesSet());
            try
            {
                Texture3D finalTexture = new Texture3D(_TempTextures[0].width, _TempTextures[0].height, _TempTextures[0].depth,TextureFormat.RGBA32,false);
                Color32[] colors = _TempTextures[0].GetPixels32();
                if (_TempTextures.Length > 1)
                {
                    Color32[] c = _TempTextures[1].GetPixels32();
                    for (int j = 0; j < c.Length; j++)
                    {
                        colors[j].g = c[j].r;//get red since all individual textures are encoded in R8
                    }
                }
                if (_TempTextures.Length > 2)
                {
                    Color32[] c = _TempTextures[2].GetPixels32();
                    for (int j = 0; j < c.Length; j++)
                    {
                        colors[j].b = c[j].r;//get red since all individual textures are encoded in R8
                    }
                }
                
                finalTexture.SetPixels32(colors);
                finalTexture.Apply();
                setCubeFromTexture(finalTexture);
            }
            catch
            {
                MorphoDebug.LogError("ERROR: all intensity images of the same time step must have identical dimensions. unable to process", 0);
            }
            
        }


        public void setCube(byte[] bytes, int channels=0)
        {
            initCubeScale();
            DestroyImmediate(Cube.material.mainTexture, true);
            if (texture)
                DestroyImmediate(texture);
            if (channels == 0)
                texture = bytesToTex3D(bytes);
            else
                texture = bytesToTex3D(bytes,channels);
            texture.wrapMode = TextureWrapMode.Clamp;
            Cube.material.SetTexture(_SP_MainTex, texture);
            Cube.material.SetFloat(_SP_XSize, texture.width);
            Cube.material.SetFloat(_SP_YSize, texture.height);
            Cube.material.SetFloat(_SP_ZSize, texture.depth);


            //set init params
            if (!_AutoShaderParamsComputed)
            {
                for (int i = 0; i <= _ChannelMax; i++)
                {
                    SetAutoShaderParams(GetHistogram(i), texture.width * texture.height * texture.depth, ShaderChannelParams[i]);
                }
                _AutoShaderParamsComputed = true;
            }

            setCubePosition();
            setCubeSize();
            CubeScale.SetActive(true);
            CubePlan.SetActive(true);
            
            InterfaceManager.instance.MenuImages.AutoSetCurrentHistogram();
        }

        

        public void setCubeFromTexture(Texture3D tex)
        {
            initCubeScale();
            DestroyImmediate(Cube.material.mainTexture, true);
            if (texture)
                DestroyImmediate(texture);

            texture = tex;
            texture.wrapMode = TextureWrapMode.Clamp;
            Cube.material.SetTexture(_SP_MainTex, texture);
            Cube.material.SetFloat(_SP_XSize, texture.width);
            Cube.material.SetFloat(_SP_YSize, texture.height);
            Cube.material.SetFloat(_SP_ZSize, texture.depth);

            HistogramsFromTexture(tex);
            //set init params
            if (!_AutoShaderParamsComputed)
            {
                for (int i = 0; i <= _ChannelMax; i++)
                {
                    SetAutoShaderParams(GetHistogram(i), tex.width * tex.height * tex.depth, ShaderChannelParams[i]);
                }
                _AutoShaderParamsComputed = true;
            }
            setCubePosition();
            setCubeSize();
            CubeScale.SetActive(true);
            CubePlan.SetActive(true);
            
                
            InterfaceManager.instance.MenuImages.AutoSetCurrentHistogram();
        }

        /// <summary>
        /// Set auto "optimal shaders parameters"
        /// </summary>
        private void SetAutoShaderParams(long[] histo, int nbPixels, ChannelShaderParams parameters)
        {
            if(_AutoShaderParamsComputed == false)
            {
                int minThresh = OtsuThreshold(histo, nbPixels);
                int maxThresh = PercentileMaxThreshold(minThresh, 0.01f,histo, nbPixels);

                float vmin = (float)minThresh / 255f;
                parameters.AlphaMin = new Vector2(vmin, 0);

                float vmax = (float)maxThresh / 255f;
                parameters.AlphaMax = new Vector2(vmax, 1);
                //update immonde
                for(int i = 0; i <= _ChannelMax; i++)
                {
                    _CurrentChannel = i;
                    UpdateRawDataShaderParameters(new RawDataSource(this));
                }

                _CurrentChannel = 0;

            }
        }

        private int OtsuThreshold(long[] histogram, int pixelCount)
        {
            //normalize histo
            double[] nHist = new double[histogram.Length];
            double globMean = 0;
            for (int i = 0; i < histogram.Length; i++)
            {
                nHist[i] = (double)histogram[i] / (double)pixelCount;
                globMean += i * nHist[i];
            }
            //max between class variance
            double bcv = 0;
            int thresh = 1;
            for (int i = 0; i < nHist.Length; i++)
            {
                double cs = 0;
                double m = 0;
                for(int j = 0; j < i; j++)
                {
                    cs += nHist[j];
                    m += j * nHist[j];
                }
                if (cs == 0)
                    continue;
                double old_bcv = bcv;
                bcv = Math.Max(bcv, Math.Pow(globMean * cs - m, 2) / (cs * (1-cs)));
                if (bcv > old_bcv)
                    thresh = i;
            }
            return thresh;
        }

        /// <summary>
        /// percentile threshold on max percent of values
        /// </summary>
        /// <param name="startValue">value to start from. all values before are ignored</param>
        /// <param name="percent">percentile to remove</param>
        /// <param name="histogram">image histogram</param>
        /// <param name="pixelCount">nb of pixels in image</param>
        /// <returns></returns>
        private int PercentileMaxThreshold(int startValue, float percent, long[] histogram, int pixelCount)
        {
            int thresh = 0;
            float currentP = 0f;
            long currentNb = 0;
            long culledPixelCount = pixelCount;
            for (int i = 0; i < startValue; i++)
                culledPixelCount -= histogram[i];
            for (int i = 255; i >= 0; i--)
            {
                currentNb += histogram[i];
                currentP = (float)currentNb / (float)culledPixelCount;
                if (currentP >= percent)
                {
                    thresh = i;
                    break;
                }
                    
            }

            return thresh;
        }

        public void HistogramsFromTexture(Texture3D t)
        {
            long[] histo_r = new long[256];
            long[] histo_g = new long[256];
            long[] histo_b = new long[256];
            long[] histo_a = new long[256];
            long max_r = 0;
            long max_g = 0;
            long max_b = 0;
            long max_a = 0;
            for (int i = 0; i < 256; i++)
            {
                histo_r[i] = 0;
                histo_g[i] = 0;
                histo_b[i] = 0;
                histo_a[i] = 0;
            }
            //compute histogram
            Color32[] colors = t.GetPixels32();
            foreach(Color32 c in colors)
            {
                histo_r[c.r] ++;
                histo_g[c.g] ++;
                histo_b[c.b] ++;
                histo_a[c.a] ++;
            }

            //histo and max
            for (int i = 0; i < 256; i++)
            {
                if (histo_r[i] > max_r)
                    max_r = histo_r[i];
                if (histo_g[i] > max_g)
                    max_g = histo_g[i];
                if (histo_b[i] > max_b)
                    max_b = histo_b[i];
                if (histo_a[i] > max_a)
                    max_a = histo_a[i];

            }
            SetHistogram(histo_r, 0, max_r);
            if (ShaderChannelParams.Length > 1)
                SetHistogram(histo_g, 1, max_g);
            if (ShaderChannelParams.Length > 2)
                SetHistogram(histo_b, 2, max_b);
            if (ShaderChannelParams.Length > 3)
                SetHistogram(histo_a, 3, max_a);
        }

        public Texture3D bytesToTex3D(byte[] bytes)
        {
            long[] histo_r = new long[256];
            long max_r = 0;
            for (int i = 0; i < 256; i++)
            {
                histo_r[i] = 0;
            }

            int[] ResizeRawDim = new int[3];

            if (sizeAt.Count > 0 && !MorphoTools.GetPlotIsActive())
            {
                if (sizeAt[_CurrentChannel].ContainsKey(t_downloaded))
                {
                    string[] sizet = sizeAt[_CurrentChannel][t_downloaded].Split(',');
                    for (int i = 0; i < 3; i++)
                        ResizeRawDim[i] = int.Parse(sizet[i].Trim());
                }
            }
            else
            {
                ResizeRawDim[0] = (int)Mathf.Round((float)RawDim[0] / (float)scaleFactor);
                ResizeRawDim[1] = (int)Mathf.Round((float)RawDim[1] / (float)scaleFactor);
                ResizeRawDim[2] = (int)Mathf.Round((float)RawDim[2] / (float)zScaleFactor);
            }

            Texture3D texture = new Texture3D(ResizeRawDim[0], ResizeRawDim[1], ResizeRawDim[2], TextureFormat.RGBA32, false);

            int header = bytes.Length - ResizeRawDim[0] * ResizeRawDim[1] * ResizeRawDim[2];
            //List<Color32> colors32 = new List<Color32>();
            Color32[] colors32 = new Color32[ResizeRawDim[0] * ResizeRawDim[1] * ResizeRawDim[2]];
            byte c32;
            int index;
            int it = 0;
            for (int z = 0; z < ResizeRawDim[2]; z++)
                for (int y = 0; y < ResizeRawDim[1]; y++)
                    for (int x = 0; x < ResizeRawDim[0]; x++)
                    {
                        index = x + y * ResizeRawDim[0] + z * ResizeRawDim[0] * ResizeRawDim[1];
                        c32 = bytes[index + header];
                        colors32[it] = (new Color32(c32, c32, c32, 255));
                        histo_r[c32]++;
                        it++;
                    }
            texture.SetPixels32(colors32);
            texture.Apply(false);

            for (int i = 0; i < 256; i++)
            {
                if (histo_r[i] > max_r)
                    max_r = histo_r[i];
            }
            SetHistogram(histo_r, _CurrentChannel, max_r);

            return texture;
        }

        public Texture3D bytesToTex3D(byte[] bytes, int channels)
        {
            long[] histo_r = new long[256];
            long[] histo_g = new long[256];
            long[] histo_b = new long[256];
            long[] histo_a = new long[256];
            long max_r = 0;
            long max_g = 0;
            long max_b = 0;
            long max_a = 0;
            for(int i = 0; i < 256; i++)
            {
                histo_r[i] = 0;
                histo_g[i] = 0;
                histo_b[i] = 0;
                histo_a[i] = 0;
            }
            int[] ResizeRawDim = new int[3];

            if (sizeAt.Count > 0 && !MorphoTools.GetPlotIsActive())
            {
                if (sizeAt[_CurrentChannel].ContainsKey(t_downloaded))
                {
                    string[] sizet = sizeAt[_CurrentChannel][t_downloaded].Split(',');
                    for (int i = 0; i < 3; i++)
                        ResizeRawDim[i] = int.Parse(sizet[i].Trim());
                }
            }
            else
            {
                ResizeRawDim[0] = (int)Mathf.Floor((float)RawDim[0]/ (float)scaleFactor);
                ResizeRawDim[1] = (int)Mathf.Floor((float)RawDim[1]/ (float)scaleFactor);
                ResizeRawDim[2] = (int)Mathf.Floor((float)RawDim[2]/ (float)zScaleFactor);
            }
            Texture3D texture = new Texture3D(ResizeRawDim[0], ResizeRawDim[1], ResizeRawDim[2], TextureFormat.RGBA32, false);
            
            int header = bytes.Length - ResizeRawDim[0] * ResizeRawDim[1] * ResizeRawDim[2] * channels;
            Color32[] colors32 = new Color32[ResizeRawDim[0] * ResizeRawDim[1] * ResizeRawDim[2]];
            byte c32r, c32g, c32b, c32a;
            int index;
            int it = 0;
            for (int z = 0; z < ResizeRawDim[2]; z++)
                for (int y = 0; y < ResizeRawDim[1]; y++)
                    for (int x = 0; x < ResizeRawDim[0]; x++)
                    {
                        index = (x + y * ResizeRawDim[0] + z * ResizeRawDim[0] * ResizeRawDim[1]);
                        c32r = bytes[index + header];
                        c32g = (byte)0;
                        c32b = (byte)0;
                        c32a = (byte)0;
                        if (channels >= 2)
                            c32g = bytes[index + header + (bytes.Length-header) / channels];
                        if (channels >= 3)
                            c32b = bytes[index + header + (bytes.Length - header) * 2 / channels];
                        if (channels >= 4)
                            c32a = bytes[index + header + (bytes.Length - header) * 3 / channels];
                        colors32[it] = (new Color32(c32r, c32g, c32b, c32a));
                        histo_r[c32r]++;
                        histo_g[c32g]++;
                        histo_b[c32b]++;
                        histo_a[c32a]++;
                        it++;
                    }
            texture.SetPixels32(colors32);
            texture.Apply(false);
            //histo and max
            for (int i = 0; i < 256; i++)
            {
                if (histo_r[i] > max_r)
                    max_r = histo_r[i];
                if (histo_g[i] > max_g)
                    max_g = histo_g[i];
                if (histo_b[i] > max_b)
                    max_b = histo_b[i];
                if (histo_a[i] > max_a)
                    max_a = histo_a[i];

            }
            SetHistogram(histo_r, 0, max_r);
            if (ShaderChannelParams.Length > 1)
                SetHistogram(histo_g, 1, max_g);
            if (ShaderChannelParams.Length > 2)
                SetHistogram(histo_b, 2, max_b);
            if (ShaderChannelParams.Length > 3)
                SetHistogram(histo_a, 3, max_a);

            return texture;
        }

        private void ToggleMeshCut(bool cut, bool refreshPlane=false)
        {
            MeshIsCut = cut;
            if (refreshPlane)
            {
                UpdateMeshFreeCuttingPlaneRepresentation();
            }
                
            UpdateShader();
        }

        public void UpdateMeshFreeCuttingPlaneRepresentation()
        {
            _MeshFreeCuttingPlaneRepresentation = GetActivePlane();
            _MeshFreeCuttingPlaneRepresentation.x *= -1;
            _MeshFreeCuttingPlaneRepresentation.y *= -1;
            _MeshFreeCuttingPlaneRepresentation.z *= -1;
        }


        public Vector4 GetActivePlane()
        {
            float mult = -1f;
            if (!_PlanInverse)
                mult = 1f;
            if (PlaneAxis == 1)
            {
                Vector3 Ynormal = mult*YPlan.transform.forward.normalized;
                return new Vector4(Ynormal.x, Ynormal.y, Ynormal.z, Vector3.Dot(Ynormal, YPlan.transform.position));
            }

            if (PlaneAxis == 2)
            {
                Vector3 Znormal = mult*ZPlan.transform.forward.normalized;
                return new Vector4(Znormal.x, Znormal.y, Znormal.z, Vector3.Dot(Znormal, ZPlan.transform.position));
            }
            //atm missing free plane
            if (!MorphoTools.GetPlotIsActive())
                mult = -mult;
            Vector3 Xnormal = mult*XPlan.transform.forward.normalized;
            return new Vector4(Xnormal.x, Xnormal.y, Xnormal.z, Vector3.Dot(Xnormal, XPlan.transform.position));
        }

        public void updateCellRenderer(CellChannelRenderer c)
        {
            UpdateMeshFreeCuttingPlaneRepresentation();
            float cutval;
            bool plot = MorphoTools.GetPlotIsActive();
            if (c != null)
            {
                if (c.Renderer != null)
                {
                    Material firstChannelMaterial = c.Renderer.material;
                    if (_MeshIsCut /*&& _RawClippingPlaneActivated*/)
                    {
                        firstChannelMaterial.UpdateFloatProperty(_SP_ToggleFreeCuttingPlane, 1f);
                        firstChannelMaterial.UpdateVectorProperty(_SP_FreeCuttingPlaneRepresentation, _MeshFreeCuttingPlaneRepresentation);
                        if (plot)
                            firstChannelMaterial.SetVector(_SP_OffsetPlan, Cube.transform.position);
                        else
                        {
                            Vector3 v = Cube.transform.position;
                            v -= Cube.transform.rotation * (Cube.transform.lossyScale / 2f);
                            firstChannelMaterial.SetVector(_SP_OffsetPlan, v);
                        }
                    }
                    else
                    {
                        firstChannelMaterial.UpdateFloatProperty(_SP_ToggleFreeCuttingPlane, 0f);
                    }
                    cutval = 0f;
                    if (_MeshIsCut /*&& _RawClippingPlaneActivated*/)
                        cutval = 1;

                    firstChannelMaterial.UpdateFloatProperty(_SP_DoClip, cutval);
                    firstChannelMaterial.UpdateIntProperty(_SP_MeshThinMode, _ThinPlanMode == true ? 1 : 0);
                    firstChannelMaterial.UpdateFloatProperty(_SP_Thickness, ThicknessValue);
                }
            }
        }

        public void UpdateDefaultMaterial()
        {
            UpdateMeshFreeCuttingPlaneRepresentation();
            float cutval;
            bool plot = MorphoTools.GetPlotIsActive();
            Material defaultMaterial = SetsManager.instance.Default;

            if (_MeshIsCut)
            {
                defaultMaterial.UpdateFloatProperty(_SP_ToggleFreeCuttingPlane, 1f);
                defaultMaterial.UpdateVectorProperty(_SP_FreeCuttingPlaneRepresentation, _MeshFreeCuttingPlaneRepresentation);
                if (plot)
                    defaultMaterial.SetVector(_SP_OffsetPlan, Cube.transform.position);
                else
                {
                    Vector3 v = Cube.transform.position;
                    v -= Cube.transform.rotation * (Cube.transform.lossyScale / 2f);
                    defaultMaterial.SetVector(_SP_OffsetPlan, v);
                }

            }
            else
            {
                defaultMaterial.UpdateFloatProperty(_SP_ToggleFreeCuttingPlane, 0f);
            }

            cutval = 0f;
            if (MeshIsCut)
                cutval = 1;

            defaultMaterial.UpdateFloatProperty(_SP_DoClip, cutval);
            defaultMaterial.UpdateFloatProperty(_SP_Thickness, ThicknessValue);
        }

        public void updateMeshShader()
        {
            float cutval;
            bool plot = MorphoTools.GetPlotIsActive();
            // Boucler sur la liste des 256 materials de Selection de couleur
            if (dataset.CellsByTimePoint != null)
            {
                if (dataset.CellsByTimePoint.ContainsKey(dataset.CurrentTime))
                {
                    foreach (Cell cell in dataset.CellsByTimePoint[dataset.CurrentTime])
                    {
                        for(int i = 0; i < cell.Channels.Count; i++)
                        {
                            CellChannelRenderer firstChannel = cell.GetChannel(i);

                            if (firstChannel != null)
                            {
                                if (firstChannel.Renderer != null)
                                {
                                    Material firstChannelMaterial = firstChannel.Renderer.material;
                                    if (_MeshIsCut /*&& _RawClippingPlaneActivated*/)
                                    {
                                        firstChannelMaterial.UpdateFloatProperty(_SP_ToggleFreeCuttingPlane, 1f);
                                        firstChannelMaterial.UpdateVectorProperty(_SP_FreeCuttingPlaneRepresentation, _MeshFreeCuttingPlaneRepresentation);
                                        if (plot)
                                            firstChannelMaterial.SetVector(_SP_OffsetPlan, Cube.transform.position);
                                        else
                                        {
                                            Vector3 v = Cube.transform.position;
                                            v -= Cube.transform.rotation * (Cube.transform.lossyScale / 2f);
                                            firstChannelMaterial.SetVector(_SP_OffsetPlan, v);
                                        }
                                    }
                                    else
                                    {
                                        firstChannelMaterial.UpdateFloatProperty(_SP_ToggleFreeCuttingPlane, 0f);
                                    }
                                    cutval = 0f;
                                    if (_MeshIsCut /*&& _RawClippingPlaneActivated*/)
                                        cutval = 1;

                                    firstChannelMaterial.UpdateFloatProperty(_SP_DoClip, cutval);
                                    firstChannelMaterial.UpdateIntProperty(_SP_MeshThinMode, _ThinPlanMode == true ? 1 : 0);
                                    firstChannelMaterial.UpdateFloatProperty(_SP_Thickness, ThicknessValue);
                                }
                            }
                        }
                        
                    }
                }
            }

            Material defaultMaterial = SetsManager.instance.Default;

            if (_MeshIsCut)
            {
                defaultMaterial.UpdateFloatProperty(_SP_ToggleFreeCuttingPlane, 1f);
                defaultMaterial.UpdateVectorProperty(_SP_FreeCuttingPlaneRepresentation, _MeshFreeCuttingPlaneRepresentation);
                if (plot)
                    defaultMaterial.SetVector(_SP_OffsetPlan, Cube.transform.position);
                else
                {
                    Vector3 v = Cube.transform.position;
                    v -= Cube.transform.rotation * (Cube.transform.lossyScale / 2f);
                    defaultMaterial.SetVector(_SP_OffsetPlan, v);
                }

            }
            else
            {
                defaultMaterial.UpdateFloatProperty(_SP_ToggleFreeCuttingPlane, 0f);
            }

            cutval = 0f;
            if (MeshIsCut)
                cutval = 1;

            defaultMaterial.UpdateFloatProperty(_SP_DoClip, cutval);
            defaultMaterial.UpdateFloatProperty(_SP_Thickness, ThicknessValue);
        }

        public void updatehideMeshShader()
        {
                // Boucler sur la liste des 256 materials de Selection de couleur
                if (dataset.CellsByTimePoint.ContainsKey(dataset.CurrentTime))
                {
                    foreach (Cell cell in dataset.CellsByTimePoint[dataset.CurrentTime])
                    {
                        for (int i = 0; i < cell.Channels.Count; i++)
                        {
                            CellChannelRenderer channel = cell.GetChannel(i);
                            if (channel != null && channel.Renderer != null)
                            {
                                channel.Renderer.material.UpdateFloatProperty(_SP_DoClip, 0f);
                            }
                        }
                    }
                }

            SetsManager.instance.Default.UpdateFloatProperty(_SP_DoClip, 0f);
        }

        public void setCubePosition()
        {
            _CubePosition.x = RawDim[0] / 2f;
            _CubePosition.y = RawDim[1] / 2f;
            _CubePosition.z = RawDim[2] / 2f;
            if (!MorphoTools.GetPlotIsActive())
                _CubePosition.x = -_CubePosition.x;  //X INVERSE FOR ASSET BUNDLE ( NOT DIRECT PLOT )
            onMovePlan();
            Cube.transform.localPosition = _CubePosition;

            //on no objects mode : different positionning
            if(dataset.mesh_by_time.ContainsKey(dataset.CurrentTime) && dataset.CellsByTimePoint.ContainsKey(dataset.CurrentTime))
            {
                if (dataset.mesh_by_time[dataset.CurrentTime] == null || dataset.CellsByTimePoint[dataset.CurrentTime].Count == 0)
                {
                    CubeScale.transform.localPosition = new Vector3(-Cube.gameObject.transform.localPosition.x * (initScale * rescaleFactor),
                        -Cube.gameObject.transform.localPosition.y * (initScale * rescaleFactor),
                        -Cube.gameObject.transform.localPosition.z * (initScale * rescaleFactor));
                }
            }
            
        }

        public void setCubeSize()
        {
            float xs = RawDim[0];
            if (!MorphoTools.GetPlotIsActive())
                xs = -xs;  //X INVERSE FOR ASSET BUNDLE ( NOT DIRECT PLOT )
            Cube.transform.localScale = new Vector3(xs, RawDim[1], RawDim[2]);
            XPlan.transform.localScale = new Vector3(xs, RawDim[1], RawDim[2]);
            YPlan.transform.localScale = new Vector3(xs, RawDim[1], RawDim[2]);
            ZPlan.transform.localScale = new Vector3(xs, RawDim[1], RawDim[2]);

            UpdateUIFrom(new RawDataSource(this));

        }


        public event Action<bool> OnAxisPlanInteractable;


        

        private void UpdateShader()
        {
            if (MeshIsCut)
                updateMeshShader();
            else
                updatehideMeshShader();
        }

        public void onMovePlan()
        {
            
            float scaledPlaneValueX = 0.5f - 0.5f * VoxelSize[0] + _PlaneValue * VoxelSize[0];
            float scaledPlaneValueY = 0.5f - 0.5f * VoxelSize[1] + _PlaneValue * VoxelSize[1];
            float scaledPlaneValueZ = 0.5f - 0.5f * VoxelSize[2] + _PlaneValue * VoxelSize[2];

            if (!MorphoTools.GetPlotIsActive())
                XPlan.transform.localPosition = new Vector3(-OriginalRawDim[0]* scaledPlaneValueX, 0f, 0f);
            else
                XPlan.transform.localPosition = new Vector3(OriginalRawDim[0] * scaledPlaneValueX, 0f, 0f);
            YPlan.transform.localPosition = new Vector3(0f, OriginalRawDim[1] * scaledPlaneValueY, 0f);
            ZPlan.transform.localPosition = new Vector3(0f,0f, OriginalRawDim[2] * scaledPlaneValueZ);

        }

        public void UpdateMeshFreeCuttingPlane(Plane plane, bool activateMeshCuttingPlane = true, Transform planeTransform = default, bool forceRender = true)
        {
            _MeshIsCut = activateMeshCuttingPlane;
            _MeshFreeCuttingPlaneRepresentation = new Vector4(plane.normal.x, plane.normal.y, plane.normal.z, plane.distance);

            if (_RawClippingPlaneActivated && planeTransform != default)
            {
                Vector3 planNormal = -planeTransform.up.normalized;
                FreeMeshesCutNorm = new Vector4(planNormal.x, planNormal.y, planNormal.z, Vector3.Dot(planNormal, planeTransform.position));
                UpdateRawFreeClippingPlane(FreeMeshesCutNorm);
            }
            if (forceRender)
            {
                if (_MeshIsCut)
                    updateMeshShader();
                else
                    updatehideMeshShader();
            }
            
        }

        public void DeactivateFreeCuttingPlane()
        {
            _MeshIsCut = false;
            updateMeshShader();
        }


        public Texture2D textureFromSprite(Sprite sprite)
        {
            if (sprite.rect.width != sprite.texture.width)
            {
                Texture2D newText = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
                Color[] newColors = sprite.texture.GetPixels((int)sprite.textureRect.x, (int)sprite.textureRect.y, (int)sprite.textureRect.width, (int)sprite.textureRect.height);
                newText.SetPixels(newColors);
                newText.Apply();
                return newText;
            }
            else
                return sprite.texture;
        }

        public void SetNetDatasetIsLoaded()
        {
            _NetDatasetFullyLoaded = true;
            InterfaceManager.instance.containerImages.SetActive(true);
        }


        private void OnDestroy()
        {
            ToggleMeshCut(false);
        }

#region Controller implementation

        public void RegisterUI(IRawDataUI rawDataUI)
        {
            if (!RawDataUIs.Contains(rawDataUI))
                RawDataUIs.Add(rawDataUI);

            rawDataUI.UpdateUIWithoutNotify(new RawDataSource(this));
        }

        public void UnRegisterUI(IRawDataUI rawDataUI) => RawDataUIs.Add(rawDataUI);

        public void UpdateUIFrom(IRawDataUI source)
        {
            foreach (IRawDataUI rawDataUI in RawDataUIs)
            {

                if (rawDataUI == source)
                    continue;

                rawDataUI.UpdateUIWithoutNotify(source);
                //OnAxisPlanInteractable?.Invoke(_AxisPlanInteractable);
            }

            UpdateRawDataParameters(source);
        }



        private void UpdateRawDataParameters(IRawDataUI source)
        {
            UpdateRawDataShaderParameters(source);
            
            OnUIParametersChange?.Invoke(source);
        }

        public void UpdateChannelActive(Toggle t, int c)
        {
            string channel;
            if (c == 0)
                channel = _SP_Channel0Active;
            else if (c==1)
                channel = _SP_Channel1Active;
            else if (c==2)
                channel = _SP_Channel2Active;
            else
                channel = _SP_Channel3Active;

            
            


            //update shader params
            ShaderChannelParams[c].IsActive = t.isOn;
            //reload if we are reactivating
            if (t.isOn)
            {
                bool all_channels_inactive = true;
                for (int i = 0; i < ShaderChannelParams.Length; i++)
                    if (i != c && ShaderChannelParams[i].IsActive)
                        all_channels_inactive = false;
                if (all_channels_inactive)//if we have AT LEAST one checkbox ACTIVATED, setactive cube3Dimage object to TRUE
                {
                    Cube.gameObject.SetActive(true);
                    showRawImages();
                }

                    
            }

            bool all_inactive = true;
            for (int i = 0; i < ShaderChannelParams.Length; i++)
                if (ShaderChannelParams[i].IsActive)
                    all_inactive = false;
            if(all_inactive)
                Cube.gameObject.SetActive(false);


            //if we have all checkboxes deactivated, setactive cube3Dimage object to false

            Cube.material.UpdateFloatProperty(channel, t.isOn?1:0);
        }

        private void UpdateRawDataShaderParameters(IRawDataUI source)
        {
            string threshold, max, transparency, intensity, colorbar, minx, maxx, miny, maxy, minHisto, maxHisto;
            if (_CurrentChannel == 0)
            {
                threshold = _SP_ThresholdC0;
                max = _SP_ThresholdMaxC0;
                transparency = _SP_TransparencyC0;
                intensity = _SP_IntensityC0;
                colorbar = _SP_ColorBarTexC0;
                minx = _SP_AlphaXMinC0;
                maxx = _SP_AlphaXMaxC0;
                miny = _SP_AlphaYMinC0;
                maxy = _SP_AlphaYMaxC0;
                minHisto = _SP_MinScalingC0;
                maxHisto = _SP_MaxScalingC0;
            }
            else if (_CurrentChannel == 1)
            {
                threshold = _SP_ThresholdC1;
                max = _SP_ThresholdMaxC1;
                transparency = _SP_TransparencyC1;
                intensity = _SP_IntensityC1;
                colorbar = _SP_ColorBarTexC1;
                minx = _SP_AlphaXMinC1;
                maxx = _SP_AlphaXMaxC1;
                miny = _SP_AlphaYMinC1;
                maxy = _SP_AlphaYMaxC1;
                minHisto = _SP_MinScalingC1;
                maxHisto = _SP_MaxScalingC1;
            }
            else if(_CurrentChannel==2)
            {
                threshold = _SP_ThresholdC2;
                max = _SP_ThresholdMaxC2;
                transparency = _SP_TransparencyC2;
                intensity = _SP_IntensityC2;
                colorbar = _SP_ColorBarTexC2;
                minx = _SP_AlphaXMinC2;
                maxx = _SP_AlphaXMaxC2;
                miny = _SP_AlphaYMinC2;
                maxy = _SP_AlphaYMaxC2;
                minHisto = _SP_MinScalingC2;
                maxHisto = _SP_MaxScalingC2;
            }
            else
            {
                threshold = _SP_ThresholdC3;
                max = _SP_ThresholdMaxC3;
                transparency = _SP_TransparencyC3;
                intensity = _SP_IntensityC3;
                colorbar = _SP_ColorBarTexC3;
                minx = _SP_AlphaXMinC3;
                maxx = _SP_AlphaXMaxC3;
                miny = _SP_AlphaYMinC3;
                maxy = _SP_AlphaYMaxC3;
                minHisto = _SP_MinScalingC3;
                maxHisto = _SP_MaxScalingC3;
            }



            var newIntensity = source.GetIntensityFieldValue();
            ShaderChannelParams[_CurrentChannel].Intensity = newIntensity;
            Cube.material.UpdateFloatProperty(intensity, newIntensity);


            var newColorBar = source.GetColorBarDropDownValue();
            var newColorMinMax = source.GetColorBarModSliderValue();
            ShaderChannelParams[_CurrentChannel].ColorBarTexture = newColorBar;
            Cube.material.SetTexture(colorbar, textureFromSprite(InterfaceManager.instance.MenuImages.ColorBarSprite(newColorBar)));

            var newMinX = source.GetAlphaMinValue().x;
            var newMaxX = source.GetAlphaMaxValue().x;
            var newMinY = source.GetAlphaMinValue().y;
            var newMaxY = source.GetAlphaMaxValue().y;

            ShaderChannelParams[_CurrentChannel].AlphaMin = new Vector2(newMinX,newMinY);
            Cube.material.UpdateFloatProperty(minx, newMinX);
            Cube.material.UpdateFloatProperty(miny, newMinY);

            ShaderChannelParams[_CurrentChannel].AlphaMax = new Vector2(newMaxX, newMaxY);
            Cube.material.UpdateFloatProperty(maxx, newMaxX);
            Cube.material.UpdateFloatProperty(maxy, newMaxY);

            var newMinMaxHisto = source.GetHistoMinMaxSliderValue();
            ShaderChannelParams[_CurrentChannel].HistoMinMax = newMinMaxHisto;
            
            Cube.material.UpdateFloatProperty(minHisto, newMinMaxHisto.Item1);
            Cube.material.UpdateFloatProperty(maxHisto, newMinMaxHisto.Item2);

            var colorbarmod = source.GetColorBarModSliderValue();
            ShaderChannelParams[_CurrentChannel].ColorBarMod = colorbarmod;


            ActivateRawDataMultiPlanCutting(source);

            //mesh cutting plane
            _MeshIsCut = source.GetCutMeshButtonValue();
            _MeshFreeCuttingPlaneRepresentation = GetActivePlane();
            _MeshFreeCuttingPlaneRepresentation.x *= -1;
            _MeshFreeCuttingPlaneRepresentation.y *= -1;
            _MeshFreeCuttingPlaneRepresentation.z *= -1;
            ToggleMeshCut(_MeshIsCut,true);

            
        }

        private void ActivateRawDataFreePlanCut()
        {
            Cube.material.UpdateIntProperty(_SP_RawClippingPlaneActivated, 1);
            if (FreeMeshesCutNorm == default)
                UpdateRawFreeClippingPlane(XMeshesCutNorm);
            else
                UpdateRawFreeClippingPlane(FreeMeshesCutNorm);
        }

        private void UpdateRawFreeClippingPlane(Vector4 norm) => Cube.material.UpdateVectorProperty(_SP_RawClippingPlane, norm);

        private void ActivateRawDataMultiPlanCutting(IRawDataUI source)
        {
            _RawClippingPlaneActivated = source.GetCuttingPlaneButtonValue();
            _PlaneValue = source.GetPlaneSliderValue();
            _PlaneAxis = source.GetPlaneAxisValue();
            _PlanInverse = source.GetCuttingPlaneInverseValue();
            _ThinPlanMode = source.GetThinModeValue();
            _ThicknessValue = source.GetThicknessSliderValue();

            Cube.material.UpdateIntProperty(_SP_RawClippingPlaneActivated, 0);
            onMovePlan();
            Vector4 plane = GetActivePlane();
            Cube.material.UpdateIntProperty(_SP_RawClippingPlaneActivated, _RawClippingPlaneActivated == true?1:0);
            Cube.material.UpdateIntProperty(_SP_RawClippingThinMode, _ThinPlanMode == true?1:0);
            Cube.material.UpdateFloatProperty(_SP_Thickness, _ThicknessValue);
            Cube.material.UpdateVectorProperty(_SP_RawClippingPlane, plane);
            
        }


        /// <summary>
        /// Template given to concrete UI’s to uptate themselves with this <see cref="RawImages"/> values.
        /// </summary>
        private class RawDataSource : IRawDataUI
        {
            private readonly RawImages _Source;

            public RawDataSource(RawImages source)
            {
                _Source = source;
            }

            public float GetPlaneSliderValue() => _Source.PlaneValue;

            public int GetColorBarDropDownValue() => _Source.ShaderChannelParams[_Source.GetCurrentChannel()].ColorBarTexture;

            public bool GetCutMeshButtonValue() => _Source.MeshIsCut;

            public bool GetCuttingPlaneButtonValue() => _Source._RawClippingPlaneActivated;

            public float GetIntensityFieldValue() => _Source.ShaderChannelParams[_Source.GetCurrentChannel()].Intensity;

            public float GetIntensitySliderValue() => _Source.ShaderChannelParams[_Source.GetCurrentChannel()].Intensity;

            public float GetMaxThresholdFieldValue() => _Source.ShaderChannelParams[_Source.GetCurrentChannel()].ThresholdMax;

            public float GetMinHistoFieldValue() => _Source.ShaderChannelParams[_Source.GetCurrentChannel()].HistoMinMax.Item1;

            public float GetMaxHistoFieldValue() => _Source.ShaderChannelParams[_Source.GetCurrentChannel()].HistoMinMax.Item2;

            public float GetThicknessSliderValue() => _Source.ThicknessValue;

            public float GetThresholdFieldValue() => _Source.ShaderChannelParams[_Source.GetCurrentChannel()].Threshold;

            public float GetTransparencyFieldValue() => _Source.ShaderChannelParams[_Source.GetCurrentChannel()].Transparency;

            public float GetTransparencySliderValue() => _Source.ShaderChannelParams[_Source.GetCurrentChannel()].Transparency;

            public void RawDataUpdate()
            {
                throw new NotImplementedException();
            }

            public void UpdateUIWithoutNotify(IRawDataUI source)
            {
                throw new NotImplementedException();
            }

            public int GetPlaneAxisValue()
            {
                return _Source.PlaneAxis;
            }

            public bool GetCuttingPlaneInverseValue() => _Source.PlanInverse;

            public bool GetThinModeValue() => _Source.ThinPlaneMode;

            public Vector2 GetAlphaMinValue() => _Source.ShaderChannelParams[_Source.GetCurrentChannel()].AlphaMin;

            public Vector2 GetAlphaMaxValue() => _Source.ShaderChannelParams[_Source.GetCurrentChannel()].AlphaMax;

            public (float, float) GetColorBarModSliderValue() => _Source.ShaderChannelParams[_Source.GetCurrentChannel()].ColorBarMod;

            public (float, float) GetHistoMinMaxSliderValue() => _Source.ShaderChannelParams[_Source.GetCurrentChannel()].HistoMinMax;

            public int GetMinValue() => _Source.MinValuesAt[_Source.GetCurrentChannel()][MorphoTools.GetDataset().CurrentTime];

            public int GetMaxValue() => _Source.MaxValuesAt[_Source.GetCurrentChannel()][MorphoTools.GetDataset().CurrentTime];

            public float GetMinCmapFieldValue() => _Source.ShaderChannelParams[_Source.GetCurrentChannel()].ColorBarMod.Item1;

            public float GetMaxCmapFieldValue() => _Source.ShaderChannelParams[_Source.GetCurrentChannel()].ColorBarMod.Item2;
        }

#endregion Controller implementation


#region shader params struct

        public class ChannelShaderParams
        {
            private bool _IsActive;
            private float _Transparency;
            private float _Threshold;
            private float _ThresholdMax;
            private float _Intensity;
            private int _ColorBarTexture;
            private long[] _Histogram;
            private long _HistoMax;
            private Vector2 _AlphaMin;
            private Vector2 _AlphaMax;
            private (float, float) _ColorBarMod;
            private (float, float) _HistoMinMax;

            public ChannelShaderParams()
            {
                _IsActive = true;
                _Transparency = 1;
                _Threshold = 0;
                _ThresholdMax = 1;
                _Intensity = 1;
                _ColorBarTexture = 0;
                _Histogram = new long[256];
                _HistoMax = 0;
                _AlphaMin = Vector2.zero;
                _AlphaMax = new Vector2(1,1);
                _ColorBarMod = (0, 2047);
                _HistoMinMax = (0, 1);
            }

            public bool IsActive { get => _IsActive; set => _IsActive = value; }
            public float Transparency { get => _Transparency; set => _Transparency = value; }
            public float Threshold { get => _Threshold; set => _Threshold = value; }
            public float ThresholdMax { get => _ThresholdMax; set => _ThresholdMax = value; }
            public float Intensity { get => _Intensity; set => _Intensity = value; }
            public int ColorBarTexture { get => _ColorBarTexture; set => _ColorBarTexture = value; }
            public long[] Histogram { get => _Histogram; set => _Histogram = value; }
            public long HistoMax { get => _HistoMax; set => _HistoMax = value; }
            public Vector2 AlphaMin { get => _AlphaMin; set => _AlphaMin = value; }
            public Vector2 AlphaMax { get => _AlphaMax; set => _AlphaMax = value; }

            public (float,float) ColorBarMod { get => _ColorBarMod; set => _ColorBarMod = value; }
            public (float,float) HistoMinMax { get => _HistoMinMax; set => _HistoMinMax = value; }
        }

#endregion
    }
}