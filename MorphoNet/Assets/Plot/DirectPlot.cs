﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Linq;
using System.Globalization;
using System.IO;
using SimpleFileBrowser;

namespace MorphoNet
{
    public struct CellCopyInstance
    {
        public Cell source_object;
        public GameObject copy_target;
        public Vector3 shift;

        public CellCopyInstance(Cell source, GameObject target,Vector3 shift)
        {
            this.source_object = source;
            this.copy_target = target;
            this.shift = shift;
        }
    }
    public class DirectPlot : MonoBehaviour
    {

        private KeyCode ScalingKey = KeyCode.S;
        private KeyCode MoveCopyKey = KeyCode.M;
        private RawImages raws = null;
        private KeyCode CopyCellKey = KeyCode.C;
        private KeyCode DeleteKey = KeyCode.D;
        private KeyCode RotateKey = KeyCode.R;
        // Start is called before the first frame update
        // Start is called before the first frame update
        // Start is called before the first frame update
        public static int upload_infos_id;
        public List<CellCopyInstance> cells_copy;
        public InputField SeedsDistance;
        public InputField RemovePixels;
        public InputField RemoveTimeStep;

        //To Add coordinates
        public GameObject PointerSegmentation;
        public GameObject PointerCopyPaste;

        public GameObject ShowHideBttn;
        public GameObject ShowHideBttnMarker;
        public GameObject ShowHideCopyBttn;
        public GameObject ShowHideCopyBttnMarker;
        public GameObject spherePointer;
        public GameObject sphereCoordinate;
        private Vector3 spherePosition;
        private Vector3 spherePositionCopyPaste;
        private Vector3 prevMousePosition;
        private float speedmoving = 0.01f;
        private Boolean moving;
        public List<GameObject> Coordinates;
        public string type = "";

        //Default Bttn
        public GameObject MenuSegmentation;

        public GameObject MenuSimulation;
        public GameObject defaultBTN;
        public GameObject defaultBTNLPY;
        public GameObject scrollView;
        public GameObject Explanations;
        public GameObject ExplanationsLpy;
        public GameObject Explanation_Active;
        public GameObject ExplanationLpy_Active;
        private bool _isActive;
        public bool IsActive { get { return _isActive; } }

        public GameObject ParentObject;
        public GameObject ParentObjectLpy;
        public Dictionary<string, GameObject> Parents;
        public Dictionary<string, GameObject> ParentsLPY;

        [HideInInspector]
        public int port_send = 9875;

        [HideInInspector]
        public int port_receive = 9876;

        [HideInInspector]
        public string plot_adress = "http://localhost:";

        public Boolean isAvaible = true;

        //To uplaod dataset on the MorphoNet webserver
        public GameObject uploadButton;

        public GameObject uploadButtonLPY;
        public GameObject uploadBox;
        public GameObject uploadBoxLPY;
        public bool previous_point_loaded = true;
        public bool color_load = true;

        private int _info_id=2;

        private float pointer_scale_factor = 1f;
        private float previous_pointer_scale_factor = 1f;
        [SerializeField]
        private Toggle _HighlightModeToggle;

        [SerializeField]
        public Button GoToNextErrorButton;
        public Button GoToPreviousErrorButton;

        public int CurrentErrorTime = -1;
        public int CurrentErrorId;
        public bool DoNotLockErrorUpdate;//tock error update when changing it via the go to next error feature

        public RuntimeMeshEditor RuntimeMeshEditorInstance;

        public List<string> previous_actions;
        public Text last_action_mouse_hover;

        private int _CurrentSegChannel=0;

        private Coroutine _ReceiveCommandCoroutine = null;
        private const int _CHECK_RECEIVE_MAX_TIMES = 10;
        private int _CoroutineFailedCounter = 0;

        private bool _PickerInUse = false;
        public int CurrentVisualizedStep = 0;
        public int MaximumStep = 0;
        public Button NextStepButton;
        public Button PreviousStepButton;
        public Button CancelCurrentStepButton;

        private bool _currently_loading_step = false;
        public Text step_text;
        public GameObject CancelMenu;
        public Text CancelText;
        [SerializeField]
        public List<PluginInterfaceStatus> status_ui_visualization;
        [SerializeField]
        public List<GameObject> UI_plugins_keep_active;

        private DateTime _BeginPlotTime;
        private bool _LoadTimeDisplayed = false;

        public GameObject visualization_text;
        [Serializable]
        public struct PluginInterfaceStatus {
            public string name;
            public bool status;

            public PluginInterfaceStatus(string childName, bool childActiveSelf)
            {
                this.name = childName;
                this.status = childActiveSelf;
            }
        }
        
        public PluginInterfaceStatus? GetStatusByName(GameObject testedObject){
            foreach (PluginInterfaceStatus status in status_ui_visualization)
            {
                if (status.name == testedObject.name)
                {
                    return status;
                }
            }
            return null;
        }
        public void VisualizeStep()
        {
            StartCoroutine(sendPythonCommand("", "step_load_" + CurrentVisualizedStep)); // Send to python
            _currently_loading_step = true; // Prevent click until next step loaded
        }
        public void HideStepsButton()
        {
            step_text.text = "Step " + CurrentVisualizedStep + "/" + MaximumStep;
            NextStepButton.interactable = !(CurrentVisualizedStep >= MaximumStep); // Can click on next if we're not at the end of steps
            PreviousStepButton.interactable = !(CurrentVisualizedStep <= 0); // Can click on previoous if we're not at first step 
            CancelCurrentStepButton.interactable = MaximumStep > 0 && CurrentVisualizedStep != 0; // We can only cancel if there is a step and we're at last step
            ChangePluginsInteraction(CurrentVisualizedStep == MaximumStep); // Activate or Deactivate all plugins play button depending on current step
            visualization_text.SetActive(CurrentVisualizedStep < MaximumStep);
        }

        public void ChangePluginsInteraction(bool status)
        {
            if (status_ui_visualization == null)
            {
                status_ui_visualization = new List<PluginInterfaceStatus>();
            }
            //Debug.Log("Changin plugin interaction");
            InterfaceManager inf = InterfaceManager.instance;
            Transform pluginContainer = inf.PluginsMenu.transform;
            //plugin_container.gameObject.SetActive(status);

            for (int i = 0; i < pluginContainer.childCount; i++) // Old behaviour kept in case people want it back
            {
                GameObject child = pluginContainer.GetChild(i).gameObject;
                if (status){
                    if (UI_plugins_keep_active.Contains(child))
                    {
                        child.SetActive(true);
                    }
                    else
                    {
                        PluginInterfaceStatus? statusFound = GetStatusByName(child);
                        if (child != null && statusFound != null) // If we found a plugin existing
                        {
                            child.SetActive(statusFound.Value.status);
                        }
                    }
                }
                else
                {
                    status_ui_visualization.Add(new PluginInterfaceStatus(child.name,child.activeSelf));
                    child.SetActive(UI_plugins_keep_active.Contains(child));
                }
            }
            
            if (status)
            {
                status_ui_visualization.Clear();
            }



            /*for (int i = 0; i < plugin_container.childCount; i++) // Old behaviour kept in case people want it back 
            {
                Transform child = plugin_container.GetChild(i);
                if (child != null) // If we found a plugin existing
                {
                    //Debug.Log("working on plugin : "+child.name);
                    ExamplePluginOptions plugin_options = child.GetComponent<ExamplePluginOptions>(); // Find the play button
                    if (plugin_options != null) // If it's null , we didn't find a plugin
                    {
                        //Debug.Log("Found plugin options !");
                        plugin_options.PlayButton.interactable = status; // Plugin is inactive if we're not at last step
                    }
                }
            }*/

        }
        public void VisualizeNextStep()
        {
            if (!_currently_loading_step) // Flag to prevent multiple clicks
            {
                int backup_current_step = CurrentVisualizedStep;
                CurrentVisualizedStep = Math.Min(CurrentVisualizedStep + 1, MaximumStep); // Increase if possible
                if (backup_current_step != CurrentVisualizedStep) // If step changed
                {
                    VisualizeStep(); // Send call to python
                    if (CurrentVisualizedStep <= previous_actions.Count())
                    {
                        string next_text = "Cancel : " + previous_actions[Math.Min(CurrentVisualizedStep-1, MaximumStep-1)];
                        last_action_mouse_hover.text = next_text;
                    }
                    
                }
            }

            HideStepsButton(); // refresh UI

        }

        public void VisualizePreviousStep()
        {
            if (!_currently_loading_step){// Flag to prevent multiple clicks
                int backup_current_step = CurrentVisualizedStep;
                CurrentVisualizedStep = Math.Max(CurrentVisualizedStep - 1, 0);// Increase if possible
                if (backup_current_step != CurrentVisualizedStep) // If step changed
                {
                    VisualizeStep(); // Send call to python
                    if (CurrentVisualizedStep <= previous_actions.Count() && CurrentVisualizedStep > 0)
                    {
                        string next_text = "Cancel : " + previous_actions[Math.Max(CurrentVisualizedStep-1,0)];
                        last_action_mouse_hover.text = next_text;
                    }
                    else
                    {
                        string next_text = "Cannot cancel initial step";
                        last_action_mouse_hover.text = next_text;
                    }
                }
            }
            HideStepsButton();// refresh UI
        }
        private IEnumerator Corout_Enable()
        {
            _BeginPlotTime = DateTime.Now;

            yield return new WaitForEndOfFrame();
            if (LoadParameters.instance.id_dataset == 0)
            {
                InterfaceManager.instance.MenuChannel.SetActive(false);
                ExplanationsLpy = MenuSimulation.transform.Find("Explanations").gameObject;
                Explanations = MenuSegmentation.transform.Find("Explanations").gameObject;
                defaultBTN.SetActive(false);
                defaultBTN.transform.Find("IF").gameObject.SetActive(false);
                //defaultBTN.transform.Find("IF").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(100, heightBttn);

                defaultBTN.transform.Find("DD").gameObject.SetActive(false);
                //defaultBTN.transform.Find("DD").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(100, heightBttn);

                defaultBTN.transform.Find("CD").gameObject.SetActive(false);
                defaultBTN.transform.Find("MI").gameObject.SetActive(false);
                //defaultBTN.transform.Find("CD").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(100, heightBttn);

                defaultBTNLPY.SetActive(false);
                defaultBTNLPY.transform.Find("IF").gameObject.SetActive(false);

                defaultBTNLPY.transform.Find("SD").gameObject.SetActive(false);

                defaultBTNLPY.transform.Find("TG").gameObject.SetActive(false);

                scrollView.SetActive(false);

                ParentObject.SetActive(false);
                ParentObjectLpy.SetActive(false);
                uploadBox.SetActive(false);
                uploadBoxLPY.SetActive(false);
                if (LoadParameters.instance.type != "")
                    type = LoadParameters.instance.type;
                if (LoadParameters.instance.morphoplot_port_send == 0)
                    LoadParameters.instance.morphoplot_port_send = 9875;
                if (LoadParameters.instance.morphoplot_port_recieve == 0)
                    LoadParameters.instance.morphoplot_port_recieve = 9876;
                port_send = LoadParameters.instance.morphoplot_port_send;
                port_receive = LoadParameters.instance.morphoplot_port_recieve;
                plot_adress = LoadParameters.instance.morphoplot_adress;
                if (!plot_adress.EndsWith(":"))
                { plot_adress += ":"; }
                Message("Start Interactive Segmentation Mode  ");
                //GameObject restart = new GameObject("restart");
                //StartCoroutine(sendPythonCommand("", restart));
                upload_infos_id = 1;
                _isActive = true;
                InterfaceManager.instance.play_loading.gameObject.SetActive(false);
                InterfaceManager.instance.pause_loading.gameObject.SetActive(false);
                InterfaceManager.stopLoading();

                previous_actions = new List<string>();

                _ReceiveCommandCoroutine = StartCoroutine(receivePythonCommand(port_send));

                //Define sphere to add a new cell
                PointerSegmentation.SetActive(false);
                
                PointerCopyPaste.SetActive(false);

                spherePosition = new Vector3(0, 0, 0);
                spherePointer.transform.localPosition = spherePosition;
                Coordinates = new List<GameObject>();
                sphereCoordinate.SetActive(false);
                Parents = new Dictionary<string, GameObject>();
                ParentsLPY = new Dictionary<string, GameObject>();
                uploadButton.SetActive(LoadParameters.instance.id_user > 0);
                if (LoadParameters.instance.id_user > 0)
                    uploadButtonLPY.SetActive(LoadParameters.instance.privacy != "public");

                _HighlightModeToggle.onValueChanged.RemoveAllListeners();
                _HighlightModeToggle.onValueChanged.AddListener(delegate { MorphoTools.GetPickedManager().ActivateHighlightToggle(); });

                //change the add properties buttons listeners
                InterfaceManager im = InterfaceManager.instance;
                im.RefreshPropertiesButton.gameObject.SetActive(false);
                HideStepsButton();
            }
        }

        private void OnEnable()
        {
            StartCoroutine(Corout_Enable());
        }

        public IEnumerator receivePythonCommand(int portNumber)
        {
                WWWForm form = new WWWForm();
                // MorphoDebug.Log("Listening on :" + plot_adress + portNumber);
                UnityWebRequest www = UnityWebRequests.Post(plot_adress + portNumber, "/get.html", form);
                //yield return new WaitForSeconds(0.1f);
                //MorphoDebug.Log("wait receive");
                //THE FOLLOWING LINE CRASH AFTER RECEIVING 1 TIME STEP
                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                {
                    //MorphoDebug.Log("Failed receiving command, starting again the waiting : "+www.result);
                    www.Dispose();
                    _ReceiveCommandCoroutine = StartCoroutine(receivePythonCommand(portNumber));
                }
                else
                {
                //test fake crash
                    String[] cmds;
                try
                {
                    String commande = Encoding.UTF8.GetString(www.downloadHandler.data);
                    commande = commande.Replace("%20", " ");
                    //MorphoDebug.Log("found commande : " + commande);
                    cmds = commande.Split(';');

                    //MorphoDebug.LogWarning("Recieve  " + cmds[0]);
                    if (cmds[0].StartsWith("MSG"))
                    {
                        if (cmds[1] == "DONE")
                            MorphoDebug.Log("", 1);
                        else
                            MorphoDebug.Log(cmds[1], 1);
                        

                    }
                    if (cmds[0].StartsWith("LOGMSG"))
                    {
                        MorphoDebug.Log($"<color=#F87E00>{cmds[1]}</color>");
                    }
                    else if (cmds[0].StartsWith("STEP"))
                    {
                        if (int.TryParse(cmds[0].Substring(5), NumberStyles.Any, CultureInfo.InvariantCulture,out CurrentVisualizedStep))
                        {
                            MorphoDebug.Log("Parsed Current step");
                        }
                        
                        if (int.TryParse(cmds[1], NumberStyles.Any, CultureInfo.InvariantCulture,out  MaximumStep))
                        {
                            MorphoDebug.Log("Parsed Maximum step");
                        }

                        _currently_loading_step = false;
                        HideStepsButton(); // Update UI depending on received steps
                    }
                    else if (cmds[0].StartsWith("BTN"))
                    {
                        InterfaceManager inf = InterfaceManager.instance;
                        string btnName = cmds[1];
                        string ParentName = cmds[2];
                        //MorphoDebug.Log("Create  " + btnName + " with parent "+ ParentName);
                        GameObject Pgo;
                        if (Parents.ContainsKey(ParentName))
                            Pgo = Parents[ParentName];
                        else
                        {
                            Pgo = Instantiate(inf.ExamplePluginCategory.gameObject, inf.PluginsMenu.transform);
                            ExamplePluginCategory cat = Pgo.GetComponent<ExamplePluginCategory>();
                            if (ParentName == "None")
                            {
                                cat.SetName("");
                                Pgo.name = "";
                            }
                            else
                            {
                                cat.SetName(ParentName);
                                Pgo.name = ParentName;
                            }
                            Pgo.SetActive(true);
                            Parents[ParentName] = Pgo;
                            /*Pgo = (GameObject)Instantiate(ParentObject, MenuSegmentation.transform);
                            if (ParentName == "None")
                                Pgo.name = "";
                            else
                                Pgo.name = ParentName;
                            Pgo.transform.Find("Text").gameObject.GetComponent<Text>().text = ParentName;
                            Pgo.SetActive(true);
                            Parents[ParentName] = Pgo;*/
                        }
                        //Check if the plugin already exist
                        bool already_created = false;
                        for (int ic = 0; ic < Pgo.transform.childCount; ic++)
                        {
                            GameObject co = Pgo.transform.GetChild(ic).gameObject;
                            if (co.name == btnName)
                                already_created = true;
                        }
                        if (already_created)
                            Message("Plugin " + btnName + " already exist");
                        else
                        {
                            Message("Create plugin " + btnName);
                            ExamplePluginCategory cat = Pgo.GetComponent<ExamplePluginCategory>();
                            Button bgo = cat.AddButton(btnName);
                            ExamplePluginButton b = bgo.GetComponent<ExamplePluginButton>();

                            bgo.gameObject.SetActive(true);

                            GameObject plugin = Instantiate(inf.ExamplePluginOptions.gameObject, inf.PluginsMenu.transform);
                            ExamplePluginOptions po = plugin.GetComponent<ExamplePluginOptions>();
                            b.Options = po;

                            plugin.name = btnName;
                            po.SetName(btnName);
                            po.gameObject.SetActive(false);
                            po.SetParentButton(bgo);

                            bgo.onClick.AddListener(delegate { po.ToggleDisplay(); });
                            //add btn listener
                            //addBtnListener(po.PlayButton.gameObject);
                            po.PlayButton.onClick.AddListener(delegate { sendCommand(po.gameObject, true); });

                            GameObject PrevGame = null;
                            if (cmds.Length > 3) //ADDITIONAL FEATURES
                                for (int i = 3; i < cmds.Length; i++)
                                {
                                    if (cmds[i].StartsWith("IF_")) //INPUT FIELD
                                    {
                                        string cmdIF = cmds[i].Substring(cmds[i].IndexOf('_') + 1);
                                        GameObject IF = po.AddInput("IF_" + cmdIF, "IF");

                                        IF.transform.Find("Placeholder").GetComponent<Text>().text = cmdIF;
                                        IF.transform.Find("comment").Find("Text").GetComponent<Text>().text = cmdIF;
                                        IF.SetActive(true);
                                        PrevGame = IF;
                                    }
                                    if (cmds[i].StartsWith("DF_")) //Default Value for Input Field
                                    {
                                        if (PrevGame != null)
                                        {
                                            string defaultValue = cmds[i].Substring(cmds[i].IndexOf('_') + 1);
                                            PrevGame.GetComponent<InputField>().text = defaultValue;
                                        }
                                    }
                                    if (cmds[i].StartsWith("DD_")) //Dropwdown FIELD
                                    {
                                        string[] cmdDDs = cmds[i].Split('_');
                                        GameObject DD = po.AddInput("DD_" + cmdDDs[1], "DD");

                                        for (int j = 2; j < cmdDDs.Length; j++)
                                            if (cmdDDs[j].Trim() != "")
                                                DD.transform.GetComponent<Dropdown>().options.Add(new Dropdown.OptionData() { text = cmdDDs[j] });
                                        DD.transform.Find("comment").Find("Text").GetComponent<Text>().text = cmdDDs[1];
                                        DD.SetActive(true);
                                    }
                                    if (cmds[i].StartsWith("CD_")) //ADD A COORDINATE BUTTON
                                    {
                                        string cmdCD = cmds[i].Substring(cmds[i].IndexOf('_') + 1);
                                        GameObject CD = po.AddInput("CD_" + cmdCD, "CD");

                                        addCoordinateListener(CD.transform.gameObject);
                                    }
                                    if (cmds[i].StartsWith("MI_")) //HIDDEN MESH INPUT
                                    {
                                        string cmdMI = cmds[i].Substring(cmds[i].IndexOf('_') + 1);
                                        GameObject MI = po.AddInput("MI_" + cmdMI, "MI");
                                        //GameObject MI = (GameObject)Instantiate(gobtn.transform.Find("MI").gameObject, gobtn.transform);
                                        MI.name = "MI_" + cmdMI;
                                        MI.transform.Find("Placeholder").GetComponent<Text>().text = cmdMI;
                                        //MI.transform.Find("comment").Find("Text").GetComponent<Text>().text = cmdMI;
                                        MI.transform.Find("Text").GetComponent<Text>().text = cmdMI;
                                        MI.transform.parent.gameObject.SetActive(false);
                                        //MI.SetActive(false);
                                        //PrevGame = MI;
                                    }
                                    if (cmds[i].StartsWith("TI_")) //HIDDEN MESH INPUT
                                    {
                                        string cmdMI = cmds[i].Substring(cmds[i].IndexOf('_') + 1);
                                        GameObject MI = po.AddInput("TI_" + cmdMI, "TI");
                                        //GameObject MI = (GameObject)Instantiate(gobtn.transform.Find("MI").gameObject, gobtn.transform);
                                        MI.name = "TI_" + cmdMI;
                                        MI.transform.Find("Placeholder").GetComponent<Text>().text = cmdMI;
                                        //MI.transform.Find("comment").Find("Text").GetComponent<Text>().text = cmdMI;
                                        MI.transform.Find("Text").GetComponent<Text>().text = cmdMI;
                                        MI.transform.parent.gameObject.SetActive(false);
                                        //MI.SetActive(false);
                                        //PrevGame = MI;
                                    }
                                    if (cmds[i].StartsWith("CB_"))
                                    {
                                        string cmdCB = cmds[i].Substring(cmds[i].IndexOf('_') + 1).Split('_')[0];
                                        string cmdCB_value = cmds[i].Substring(cmds[i].IndexOf('_') + 1).Split('_')[1];
                                        bool.TryParse(cmdCB_value, out bool v);
                                        GameObject MI = po.AddInput("CB_" + cmdCB, "CB");
                                        MI.name = "CB_" + cmdCB;
                                        //MI.transform.Find("Text").GetComponent<Text>().text = cmdMI;
                                        MI.transform.parent.gameObject.SetActive(true);
                                        MI.transform.Find("comment").Find("Text").GetComponent<Text>().text = cmdCB;
                                        MI.GetComponent<Toggle>().isOn = v;
                                    }
                                    if (cmds[i].StartsWith("FP_")) //INPUT FIELD
                                    {
                                        string cmdFP = cmds[i].Substring(cmds[i].IndexOf('_') + 1);
                                        GameObject FP = po.AddInput("FP_" + cmdFP, "FP");
                                        //add listener for button -> filepicker
                                        Button fp = FP.GetComponentInChildren<Button>();
                                        fp.onClick.AddListener(() => FilePickerForPlugin(FP.GetComponent<InputField>()));
                                        FP.transform.Find("Placeholder").GetComponent<Text>().text = cmdFP;
                                        FP.transform.Find("comment").Find("Text").GetComponent<Text>().text = cmdFP;
                                        FP.SetActive(true);
                                        PrevGame = FP;
                                    }
                                    if (cmds[i].StartsWith("FS_")) //INPUT FIELD
                                    {
                                        string cmdFS = cmds[i].Substring(cmds[i].IndexOf('_') + 1);
                                        GameObject FS = po.AddInput("FS_" + cmdFS, "FS");
                                        //add listener for button -> filepicker
                                        Button fs = FS.GetComponentInChildren<Button>();
                                        fs.onClick.AddListener(() => FileSaverForPlugin(FS.GetComponent<InputField>()));

                                        FS.transform.Find("Placeholder").GetComponent<Text>().text = cmdFS;
                                        FS.transform.Find("comment").Find("Text").GetComponent<Text>().text = cmdFS;
                                        FS.SetActive(true);
                                        PrevGame = FS;
                                    }
                                }
                            //reorganizeBttns();
                        }
                    }
                    else if (cmds[0].StartsWith("BTLPY"))
                    {
                        string btnName = cmds[1];
                        string ParentName = cmds[2];

                        GameObject Pgo;
                        if (ParentsLPY.ContainsKey(ParentName))
                            Pgo = ParentsLPY[ParentName];
                        else
                        {
                            Pgo = (GameObject)Instantiate(ParentObjectLpy, MenuSimulation.transform);
                            if (ParentName == "None")
                                Pgo.name = "";
                            else
                                Pgo.name = ParentName;
                            Pgo.transform.Find("Text").gameObject.GetComponent<Text>().text = ParentName;
                            Pgo.SetActive(true);
                            ParentsLPY[ParentName] = Pgo;
                        }
                        //Check if the plugin already exist
                        bool already_created = false;

                        for (int ic = 0; ic < Pgo.transform.childCount; ic++)
                        {
                            GameObject co = Pgo.transform.GetChild(ic).gameObject;
                            if (co.name == btnName)
                                already_created = true;
                        }

                        if (already_created)
                            Message("Plugin " + btnName + " already exist");
                        else
                        {
                            Message("Create plugin " + btnName);
                            GameObject gobtn;
                            GameObject SV = null;

                            if (btnName.Contains("Reload element"))
                            {
                                bool test = false;
                                for (int ic = 0; ic < Pgo.transform.childCount; ic++)
                                {
                                    GameObject co = Pgo.transform.GetChild(ic).gameObject;
                                    if (co.name == "SV")
                                    {
                                        test = true;
                                        SV = co;
                                    }
                                }
                                if (!test)
                                {
                                    SV = (GameObject)Instantiate(scrollView.gameObject, Pgo.transform);
                                    SV.name = "SV";
                                }
                                SV.SetActive(true);
                                gobtn = (GameObject)Instantiate(defaultBTNLPY, SV.transform.Find("ScrollView").Find("Viewport").Find("Content"));
                            }
                            else
                            {
                                gobtn = (GameObject)Instantiate(defaultBTNLPY, Pgo.transform);
                            }
                            gobtn.name = btnName;
                            GameObject btn = gobtn.transform.Find("Btn").gameObject;
                            GameObject btnText = btn.transform.Find("Text").gameObject;
                            gobtn.transform.Find("Explanation").gameObject.SetActive(false);
                            btnText.GetComponent<Text>().text = btnName;
                            RectTransform ct = btn.GetComponent<RectTransform>();
                            addBtnListener(gobtn);

                            gobtn.SetActive(true);
                            CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                            ci.NumberFormat.CurrencyDecimalSeparator = ".";
                            GameObject PrevGame = null;

                            if (cmds.Length > 3) //ADDITIONAL FEATURES
                                for (int i = 3; i < cmds.Length; i++)
                                {
                                    if (cmds[i].StartsWith("IF_")) //INPUT FIELD
                                    {
                                        string cmdIF = cmds[i].Substring(cmds[i].IndexOf('_') + 1);
                                        GameObject IF = (GameObject)Instantiate(gobtn.transform.Find("IF").gameObject, gobtn.transform);
                                        IF.name = "IF_" + cmdIF;
                                        IF.transform.Find("Placeholder").GetComponent<Text>().text = cmdIF;
                                        IF.transform.Find("Text").GetComponent<Text>().text = cmdIF;
                                        IF.transform.Find("parameterName").GetComponent<Text>().text = cmdIF;
                                        IF.transform.Find("comment").Find("Text").GetComponent<Text>().text = cmdIF;
                                        IF.SetActive(true);
                                        PrevGame = IF;
                                    }
                                    if (cmds[i].StartsWith("DF_")) //Default Value for Input Field
                                    {
                                        if (PrevGame != null)
                                        {
                                            string defaultValue = cmds[i].Substring(cmds[i].IndexOf('_') + 1);
                                            PrevGame.GetComponent<InputField>().text = defaultValue;
                                            PrevGame.GetComponent<RectTransform>().sizeDelta = new Vector2(PrevGame.GetComponent<InputField>().preferredWidth + 50, heightBttn);
                                        }
                                    }
                                    if (cmds[i].StartsWith("SD_")) //ADD A SLIDER
                                    {
                                        string[] cmdSDs = cmds[i].Split('_');
                                        GameObject SD = (GameObject)Instantiate(gobtn.transform.Find("SD").gameObject, gobtn.transform);
                                        SD.name = "SD_" + cmdSDs[1];
                                        SD.GetComponent<Slider>().value = float.Parse(cmdSDs[2], NumberStyles.Any, ci);
                                        SD.GetComponent<Slider>().minValue = float.Parse(cmdSDs[3], NumberStyles.Any, ci);
                                        SD.GetComponent<Slider>().maxValue = float.Parse(cmdSDs[4], NumberStyles.Any, ci);
                                        SD.transform.Find("Handle Slide Area").Find("Handle").Find("TextValue").GetComponent<Text>().text = cmdSDs[2];
                                        if (cmdSDs[5] == "1")
                                            SD.GetComponent<Slider>().wholeNumbers = true;
                                        SD.transform.Find("parameterName").GetComponent<Text>().text = cmdSDs[1];
                                        SD.GetComponent<Slider>().onValueChanged.AddListener(delegate
                                        { ValueSlider(SD); });
                                        SD.transform.Find("comment").Find("Text").GetComponent<Text>().text = cmdSDs[1];
                                        SD.SetActive(true);
                                    }
                                    if (cmds[i].StartsWith("TG_")) //ADD A TOGGLE
                                    {
                                        string[] cmdTGs = cmds[i].Split('_');
                                        GameObject TG = (GameObject)Instantiate(gobtn.transform.Find("TG").gameObject, gobtn.transform);
                                        TG.name = "TG_" + cmdTGs[1];
                                        TG.GetComponent<Toggle>().isOn = (cmdTGs[2] == "True");
                                        TG.transform.Find("parameterName").GetComponent<Text>().text = cmdTGs[1];
                                        TG.transform.Find("comment").Find("Text").GetComponent<Text>().text = cmdTGs[1];
                                        TG.SetActive(true);
                                    }
                                }
                            reorganizeBttnsLPY();
                        }
                    }
                    else if (cmds[0].StartsWith("CHANGE"))
                    {
                        string btnName = cmds[1];
                        string ParentName = cmds[2];
                        GameObject Pgo = null;
                        if (!ParentsLPY.ContainsKey(ParentName))
                            Message("Unknown plugin " + btnName);
                        else
                        {
                            Pgo = ParentsLPY[ParentName];
                            GameObject gobtn = null;
                            for (int ic = 0; ic < Pgo.transform.childCount; ic++)
                            {
                                GameObject co = Pgo.transform.GetChild(ic).gameObject;
                                if (co.name == btnName)
                                    gobtn = co;
                            }

                            CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                            ci.NumberFormat.CurrencyDecimalSeparator = ".";

                            GameObject child;
                            string[] cmd;
                            GameObject PrevGame;
                            Message("Actualise plugin " + btnName);
                            if (!btnName.Contains("Reload element"))
                            {
                                for (int i = 3; i < cmds.Length; i++)
                                {
                                    cmd = cmds[i].Split('_');
                                    for (int ic = 0; ic < gobtn.transform.childCount; ic++)
                                    {
                                        child = gobtn.transform.GetChild(ic).gameObject;
                                        if ("IF_" + cmd[1] == child.name)
                                        {
                                            child.GetComponent<InputField>().text = cmd[2];
                                            PrevGame = child;
                                        }
                                        if ("SD_" + cmd[1] == child.name)
                                        {
                                            child.GetComponent<Slider>().value = float.Parse(cmd[2], NumberStyles.Any, ci);
                                            ValueSlider(child);
                                        }
                                        if ("TG_" + cmd[1] == child.name)// Toggle
                                        {
                                            child.GetComponent<Toggle>().isOn = (cmd[2] == "True");
                                        }
                                    }
                                }
                                reorganizeBttnsLPY();
                            }
                        }
                    }
                    else if (cmds[0].StartsWith("EX"))
                    {
                        String[] shapet = cmds[0].Substring(3).Split('_');
                        //for (int x = 0; x < shapet.Length; x++) MorphoDebug.Log(x+ "----->"+shapet[x]);
                        String plugin_name = shapet[3];
                        foreach (GameObject Pgo in Parents.Values) //For Each Parent
                            for (int ic = 0; ic < Pgo.transform.childCount; ic++)
                            {
                                GameObject co = Pgo.transform.GetChild(ic).gameObject;
                                if (co.name == plugin_name)
                                {
                                    MorphoDebug.Log("Load explanation for " + plugin_name);
                                    int sizeX = int.Parse(shapet[0]);
                                    int sizeY = int.Parse(shapet[1]);
                                    int nb_bytes = int.Parse(shapet[2]);
                                    Texture2D tex = new Texture2D(sizeX, sizeY);
                                    byte[] bytes = www.downloadHandler.data;
                                    int header = bytes.Length - nb_bytes;
                                    byte[] bytes_png = new byte[nb_bytes];
                                    for (int x = 0; x < nb_bytes; x++)
                                        bytes_png[x] = bytes[header + x];
                                    tex.LoadImage(bytes_png);
                                    tex.Apply();
                                    GameObject exp = Instantiate(InterfaceManager.instance.ExamplePluginHelp.gameObject, InterfaceManager.instance.PluginsHelpMenu.transform);
                                    exp.SetActive(false);
                                    string url_doc = "https://morphonet.org/helpfiles/API/morphonet.plugins."+Pgo.name.Replace(" ","")+".html#"+plugin_name.Split(':')[0].Replace(" ","").ToLower()+"-plugin";
                                    ExampleHelpWindow help = exp.GetComponent<ExampleHelpWindow>();
                                    help.SetLinkURL(url_doc);
                                    ExamplePluginButton pluginbtn = co.GetComponent<ExamplePluginButton>();
                                    help.SetName(plugin_name);
                                    help.SetImage(tex);
                                    if (shapet.Length >= 5)
                                        help.SetDescription(shapet[4]);
                                    pluginbtn.Options.SetHelpWindow(help);
                                    help.GetImage().sizeDelta = new Vector2(sizeY * 200 / sizeX, 200f);
                                    help.GetImage().sizeDelta *= 1.5f;


                                }
                            }
                    }
                    else if (cmds[0].StartsWith("IC"))
                    {
                        String[] shapet = cmds[0].Substring(3).Split('_');
                        //for (int x = 0; x < shapet.Length; x++) MorphoDebug.Log(x+ "----->"+shapet[x]);
                        String plugin_name = shapet[3];
                        foreach (GameObject Pgo in Parents.Values) //For Each Parent
                            for (int ic = 0; ic < Pgo.transform.childCount; ic++)
                            {
                                GameObject co = Pgo.transform.GetChild(ic).gameObject;
                                if (co.name == plugin_name)
                                {
                                    MorphoDebug.Log("Load icon for " + plugin_name);
                                    int sizeX = int.Parse(shapet[0]);
                                    int sizeY = int.Parse(shapet[1]);
                                    int nb_bytes = int.Parse(shapet[2]);
                                    Texture2D tex = new Texture2D(sizeX, sizeY);
                                    byte[] bytes = www.downloadHandler.data;
                                    int header = bytes.Length - nb_bytes;
                                    byte[] bytes_png = new byte[nb_bytes];
                                    for (int x = 0; x < nb_bytes; x++)
                                        bytes_png[x] = bytes[header + x];
                                    tex.LoadImage(bytes_png);
                                    tex.Apply();
                                    try
                                    {
                                        ExamplePluginButton btn;
                                        if (co.TryGetComponent<ExamplePluginButton>(out btn))
                                        {
                                            btn.SetIcons(tex);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Debug.LogError("could not set image to button for plugin " + plugin_name + ". -> " + e);
                                    }
                                }
                            }
                    }
                    else if (cmds[0].StartsWith("ELPY"))
                    {
                        String[] shapet = cmds[0].Substring(3).Split('_');
                        String plugin_name = shapet[3];
                        foreach (GameObject Pgo in ParentsLPY.Values) //For Each Parent
                            for (int ic = 0; ic < Pgo.transform.childCount; ic++)
                            {
                                GameObject co = Pgo.transform.GetChild(ic).gameObject;
                                if (co.name == plugin_name)
                                {
                                    int sizeX = int.Parse(shapet[0]);
                                    int sizeY = int.Parse(shapet[1]);
                                    int nb_bytes = int.Parse(shapet[2]);
                                    Texture2D tex = new Texture2D(sizeX, sizeY);
                                    byte[] bytes = www.downloadHandler.data;
                                    int header = bytes.Length - nb_bytes;
                                    byte[] bytes_png = new byte[nb_bytes];
                                    for (int x = 0; x < nb_bytes; x++)
                                        bytes_png[x] = bytes[header + x];
                                    tex.LoadImage(bytes_png);
                                    tex.Apply();
                                    GameObject explanationLpy = (GameObject)Instantiate(ExplanationsLpy.transform.Find("image").gameObject, ExplanationsLpy.transform);
                                    explanationLpy.name = plugin_name;
                                    explanationLpy.GetComponent<RawImage>().texture = tex;
                                    float maxSizeX = 500;
                                    explanationLpy.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector3(maxSizeX, sizeX * maxSizeX / sizeY, 0);
                                    co.transform.Find("Explanation").GetComponent<Button>().onClick.RemoveAllListeners();
                                    co.transform.Find("Explanation").GetComponent<Button>().onClick.AddListener(delegate ()
                                    { showExplanationLpy(explanationLpy); });
                                    co.transform.Find("Explanation").gameObject.SetActive(true);
                                }
                            }
                    }
                    else if (cmds[0].StartsWith("LOAD_"))
                    { //LOAD A SPECIFIC TIME STEP
                        bool contained_time = true;
                        int t = int.Parse(cmds[0].Substring(5));
                        int ch = int.Parse(cmds[1]);
                        if (t >= 0)
                        { //WE DO NOT ACCEPT t<0
                          //MorphoDebug.Log(" Plot at " + t+ " linTime:" + MorphoTools.getCurrentDataSet().MinTime + " Max " + MorphoTools.getCurrentDataSet().MaxTime +" iss  "+MorphoTools.getCurrentDataSet().isContainTime(t));
                            if (t >= MorphoTools.GetDataset().MaxTime && InterfaceManager.instance.MenuShortcuts.activeSelf)
                            {
                                InterfaceManager.instance.MenuShortcuts.SetActive(false);
                                contained_time = false;
                            }
                            Message("Load Meshes at " + t+ " for channel "+ch);
                            bool should_update = false;
                            if (!MorphoTools.GetDataset().CellsByTimePoint.ContainsKey(t))
                                MorphoTools.GetDataset().CellsByTimePoint[t] = new List<Cell>();
                            InterfaceManager.instance.MenuChannel.SetActive(true);
                            Dictionary<string, int> ListPreviousSelections = new Dictionary<string, int>();
                            List<string> hidden_IDS = new List<string>();
                            Dictionary<string, List<string>> ListPreviousMothers = new Dictionary<string, List<string>>();
                            Dictionary<string, List<string>> ListPreviousDaughters = new Dictionary<string, List<string>>();

                            if (MorphoTools.GetDataset().MeshAtTimestampExists(t))
                            {
                                //Keep In Mind Previous Selection
                                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
                                {
                                    if (cell.Mothers != null)
                                    {
                                        foreach (Cell m in cell.Mothers)
                                        {
                                            if (!ListPreviousMothers.ContainsKey(cell.getName()))
                                                ListPreviousMothers.Add(cell.getName(), new List<string>());
                                            ListPreviousMothers[cell.getName()].Add(m.getName());
                                        }
                                    }

                                    if (cell.Daughters != null)
                                    {
                                        foreach (Cell da in cell.Daughters)
                                        {
                                            if (!ListPreviousDaughters.ContainsKey(cell.getName()))
                                                ListPreviousDaughters.Add(cell.getName(), new List<string>());
                                            ListPreviousDaughters[cell.getName()].Add(da.getName());
                                        }
                                    }

                                    if (!cell.show)
                                    {
                                        should_update = true;
                                        hidden_IDS.Add(cell.ID);
                                    }
                                    if ((cell != null && cell.getFirstChannel() != null && cell.getFirstChannel().ShouldBeUpdated) || MorphoTools.GetScatter().explodeFactor != 0f)
                                    {
                                        should_update = true;
                                    }
                                    if (cell.selected)
                                    {
                                        ListPreviousSelections[cell.ID] = 0;
                                        MorphoTools.GetPickedManager().RemoveCellSelected(cell);
                                    }
                                    else if (cell.selection != null && cell.selection.Count > 0)
                                        ListPreviousSelections[cell.ID] = cell.selection[0];

                                }

                                MorphoTools.GetDataset().deleteAt(t, ch);
                            }

                            MorphoTools.GetDataset().LoadingTime = t;
                            if (cmds[2].Contains("p"))
                            { DataLoader.instance.createMeshesFromPrimitive(cmds[2], "_DATASET_" + t, t); }
                            else
                            {
                                GameObject goe = DataLoader.instance.createGameFromObj(cmds[2]);
                                if (goe != null)
                                {
                                    List<Cell> old_cells = new List<Cell>(MorphoTools.GetDataset().CellsByTimePoint[t]);
                                    List<Cell> new_cells = new List<Cell>();
                                    DataLoader.instance.LoadMesh(t, goe, false, "_DATASET_" + t, ch.ToString(), false, new_cells);
                                    if (new_cells.Count > 0)
                                    {
                                        foreach (Cell c in old_cells)
                                        {
                                            if (!new_cells.Contains(c) && c.Channels.ContainsKey(ch.ToString()))
                                            {
                                                if (MorphoTools.GetDataset().CellsByTimePoint[t].Contains(c))
                                                {
                                                    MorphoDebug.Log("plot load : removing cell " + c.getName() + ", was deleted on reload");
                                                    MorphoTools.GetDataset().CellsByTimePoint[t].Remove(c);
                                                }

                                            }

                                        }
                                    }
                                    
                                    //DestroyImmediate(goe, true);
                                    InterfaceManager.instance.setTime(t);
                                    if (MorphoTools.GetDataset().CurrentTime == t)
                                        MorphoTools.GetDataset().PreviousTime = t - 1; //TO FORCE RELOAD
                                    foreach (KeyValuePair<int, GameObject> pair in MorphoTools.GetDataset().mesh_by_time)
                                    {
                                        if (pair.Key != t)
                                        {
                                            if (pair.Value != null)
                                                pair.Value.SetActive(false);
                                        }
                                    }
                                }
                            }
                            if (!MorphoTools.GetDataset().TimePointExists(t))
                                MorphoTools.CreateTimePoint(t);//create time point ? or only do it when time point oes not exists. if exists, integrate what we did into the timepoint

                            //test add cells missing
                            MorphoTools.GetDataset().GetTimepointAt(t).AddCellsToTimePoint();

                            foreach (string idcell in hidden_IDS)
                            {
                                Cell bos = MorphoTools.GetDataset().getCell(t, idcell);
                                if (bos != null)
                                {
                                    bos.show = false;
                                }
                            }

                            foreach (var bo in ListPreviousSelections)
                            {
                                Cell bos = MorphoTools.GetDataset().getCell(t, bo.Key);
                                if (bos != null)
                                {
                                    if (should_update)
                                    {
                                        bos.UpdateAllChannel();
                                    }
                                    if (bo.Value == 0)
                                        MorphoTools.GetPickedManager().AddCellSelected(bos);
                                    else
                                        bos.addSelection(bo.Value);
                                }
                            }
                            DataSet d = MorphoTools.GetDataset();
                            foreach (var v in ListPreviousMothers)
                            {
                                Cell c = d.getCell(v.Key, false);
                                if(c != null)
                                {
                                    foreach (var m in v.Value)
                                    {
                                        //c.addMother(d.getCell(m, false));
                                        Cell mother = d.getCell(m, false);
                                        if (mother != null)
                                            mother.addDaughter(c);
                                    }
                                        
                                }
                                
                            }
                            foreach(var v in ListPreviousDaughters)
                            {
                                Cell c = d.getCell(v.Key, false);
                                if (c != null)
                                {
                                    foreach (var da in v.Value)
                                        c.addDaughter(d.getCell(da, false));
                                }
                            }

                            //Put the selection number back



                            if (!contained_time)
                            {
                                MorphoTools.GetTransformationsManager().ApplyTransformationsOnTimeChange();
                            }
#if !UNITY_WEBGL
                            MorphoTools.SendCellsPickedToLineage();
#endif

                            ResetDatasetAppliedModifiers();
                        }
                    }
                    else if (cmds[0].StartsWith("CONTAINSRAW_"))
                    {
                        InterfaceManager.instance.containerImages.SetActive(true);
                        int channel_max = 0;
                        int.TryParse(cmds[1], out channel_max);
                        if (raws == null)
                        {
                            raws = MorphoTools.GetRawImages();
                        }
                        if (channel_max > 0)
                            raws.SetChannels(0, channel_max - 1);

                    }
                    else if (cmds[0].StartsWith("LPY_"))
                    { //LOAD LPY AT A SPECIFIC TIME STEP
                        if (type == "")
                            type = "lpy";
                        int t = int.Parse(cmds[0].Substring(4));
                        if (t >= 0)
                        { //WE DO NOT ACCEPT t<0
                            Message("Load Meshes at " + t);
                            if (!MorphoTools.GetDataset().CellsByTimePoint.ContainsKey(t))
                                MorphoTools.GetDataset().CellsByTimePoint[t] = new List<Cell>();
                            Dictionary<string, int> ListPreviousSelections = new Dictionary<string, int>();
                            if (MorphoTools.GetDataset().MeshAtTimestampExists(t))
                            {
                                MorphoTools.GetPickedManager().ClearSelectedCells();
                                for (int time = MorphoTools.GetDataset().MaxTime; time >= t; time--)
                                {
                                    SetsManager.instance.CleanPrimitives(time);
                                    MorphoTools.GetDataset().deleteAt(time); //DELETE PREVIOUS DATASET
                                }

                                InterfaceManager.instance.ColorMapScale.gameObject.SetActive(false);

                                Correspondence co = DataSetInformations.instance.getCorrespondenceByName("simulation-color");
                                co.parse = true;
                                co.startIndex = 3;
                                co.parseLineLPY(t, MorphoTools.GetDataset().MaxTime);

                                System.GC.Collect();
                            }

                            GameObject goe = DataLoader.instance.createMeshesFromPrimitivesAndObj(cmds[1], "_DATASET_" + t, t);
                            if (goe != null)
                            {
                                DataLoader.instance.LoadMesh(t, goe, false, "_DATASET_" + t, "0", true);
                                DestroyImmediate(goe, true);
                                InterfaceManager.instance.setTime(t);
                                if (MorphoTools.GetDataset().CurrentTime == t)
                                    MorphoTools.GetDataset().PreviousTime = t - 1; //TO FORCE RELOAD
                                foreach (KeyValuePair<int, GameObject> pair in MorphoTools.GetDataset().mesh_by_time)
                                {
                                    if (pair.Key != t)
                                    {
                                        if (pair.Value != null)
                                            pair.Value.SetActive(false);
                                    }
                                }
                                MorphoTools.GetDataset().forceUpdate();
                            }
                            if (t == MorphoTools.GetDataset().MaxTime)
                            {
                                GC.Collect();
                            }
                        }
                    }
                    else if (cmds[0].StartsWith("SELECT")) // Load a List of cells as a selection
                    {
                        Message("Load " + (cmds.Count() - 1) + " objects selected");
                        foreach (string o in cmds)
                        {
                            if (o != "SELECT")
                            {
                                string[] elts = o.Split(',');
                                if (elts.Count() == 4)//4 because t,id,c,LABEL
                                {
                                    Cell bos = MorphoTools.GetDataset().getCell(elts[0] + "," + elts[1] + "," + elts[2], false);
                                    if (bos != null)
                                    {
                                        int selectionNumber = int.Parse(elts[3]);
                                        if (selectionNumber == 0)
                                            MorphoTools.GetPickedManager().AddCellSelected(bos);
                                        else
                                            bos.addSelection(selectionNumber);
                                    }
                                }
                            }
                        }
#if !UNITY_WEBGL
                        MorphoTools.SendCellsPickedToLineage();
#endif
                    }
                    else if (cmds[0].StartsWith("SEEDS"))
                    { //LOAD A SEEDS
                        MorphoDebug.Log("Add " + (cmds.Length - 1) + " centers");
                        for (int i = 1; i < cmds.Length; i++)
                        {
                            string[] ssV = cmds[i].Split(',');
                            float x = float.Parse(ssV[0], NumberStyles.Any, CultureInfo.InvariantCulture);
                            float y = float.Parse(ssV[1], NumberStyles.Any, CultureInfo.InvariantCulture);
                            float z = float.Parse(ssV[2], NumberStyles.Any, CultureInfo.InvariantCulture);
                            addSeed(new Vector3(x,y,z));
                        }
                    }
                    else if (cmds[0].StartsWith("PRIM"))
                    { //LOAD A PRIMITIVE
                        string name = cmds[0].Substring(5);
                        Message("Load Primitive " + name);
                        DataLoader.instance.createPrimitive(name, cmds[1]);
                    }
                    else if (cmds[0].StartsWith("INFO_"))
                    {   //LOAD INFO
                        string infoname = cmds[0].Substring(5);
                        Message("Load info " + infoname);
                        string infoslist = cmds[1];
                        //First whe check if this corrrspondence already exist, it's just a update
                        Correspondence co = MorphoTools.GetDataset().Infos.getCorrespondenceByName(infoname);
                        if (co != null)
                        {
                            //MorphoDebug.Log("Clear Correspondence");
                            //Remove All previosu entry
                            if (co.datatype == "time")
                            { //REMOVE ALL PREVIOUS LINAGE
                                StartCoroutine(MorphoTools.sendPythonCommandPyinstaller("init_lineage", ""));
                                for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
                                    foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
                                        cell.clearRelations();
                            }
                            else
                            {
                                for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
                                    foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
                                        cell.clearInfos(co.id_infos);
                            }
                        }
                        else
                        {
                            // MorphoDebug.Log("Received lineage");
                            string[] lines = infoslist.Split("\n"[0]);
                            string datatype = "";
                            if (lines.Length >= 1)
                            { //OTHER WISE WE DON'T HAVE ENOUGH INFOS..
                                int startIndex = 0;
                                while (startIndex < lines.Length && lines[startIndex].Length > 0 && lines[startIndex][0] == '#')
                                    startIndex++; //We skip comments (TODO We can add the comment when we put the mouse on the button
                                string[] infotypes = lines[startIndex].Split(':');
                                datatype = infotypes[1];
                            }
                            if (datatype == "time") StartCoroutine(MorphoTools.sendPythonCommandPyinstaller("init_lineage", ""));
                            CorrespondenceType ct = DataSetInformations.instance.getCorrespondenceType(datatype);
                            co = new Correspondence(0, ct, SetsManager.instance.IDUser, 2, upload_infos_id, infoname, "", 1, true, datatype, DateTime.Now.ToString(), 0, DataSetInformations.instance);
                            upload_infos_id += 1;
                            ct.addCorrespondence(co);
                            co.active();
                        }

                        if (!co.IsActive)
                            co.active();

                        co.startIndex = 0;

                        //make it so the info is parsed ?
                        //co.parse = true;

                        co.parseTuple(infoslist, co.name);
                        //reset min and max ?
                        co.MinInfos = float.MaxValue;
                        co.MaxInfos = float.MinValue;
                        co.parseLine(10);
                        DataSetInformations.instance.reOrganize();

                    }
                    else if (cmds[0].StartsWith("INFONAME_"))//only have infos that will be downloadable later by clicking on the button
                    {
                        string datatype = cmds[1];
                        string infoname = cmds[0].Replace("INFONAME_", "");
                        CorrespondenceType ct = DataSetInformations.instance.getCorrespondenceType(datatype);
                        Correspondence co = new Correspondence(0, ct, SetsManager.instance.IDUser, 2, upload_infos_id, infoname, "", 1, true, datatype, DateTime.Now.ToString(), 0, DataSetInformations.instance);
                        upload_infos_id++;
                        ct.addCorrespondence(co);
                        //co.active();
                        DataSetInformations.instance.reOrganize();
                    }
                    else if (cmds[0].StartsWith("SKINFONAME_"))//only have infos that will be downloadable later by clicking on the button
                    {
                        string datatype = cmds[1];
                        string infoname = cmds[0].Replace("SKINFONAME_", "");
                        CorrespondenceType ct = DataSetInformations.instance.getCorrespondenceType(datatype);
                        Correspondence co = new Correspondence(0, ct, SetsManager.instance.IDUser, 2, upload_infos_id, infoname, "", 1, true, datatype, DateTime.Now.ToString(), 0, DataSetInformations.instance, true);
                        upload_infos_id++;
                        ct.addCorrespondence(co);
                        co.inf.transform.Find("sklogo").gameObject.SetActive(true);
                        co.inf.transform.Find("deploy").gameObject.SetActive(false);
                        //co.active();
                        DataSetInformations.instance.reOrganize();
                    }
                    else if (cmds[0].StartsWith("INFY_"))
                    {   //LOAD INFO LPY
                        string infoname = cmds[0].Substring(5);
                        string infoslist = cmds[1];
                        int id = -1;
                        Message("Load information parameter " + infoname);

                        //First whe check if this correspondence already exist
                        Correspondence co = DataSetInformations.instance.getCorrespondenceByName(infoname);
                        if (co != null)
                        {
                            co.unActive();
                            id = DataSetInformations.instance.getCorrespondenceByName(infoname).id_infos;
                            for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
                                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
                                {
                                    cell.clearInfos(co.id_infos);
                                }
                            DataSetInformations.instance.deleteCorrespondence(co, false);
                        }
                        string[] lines = infoslist.Split("\n"[0]);
                        string datatype = "";
                        if (lines.Length >= 1)
                        { //OTHER WISE WE DON'T HAVE ENOUGH INFOS..
                            int startIndex = 0;
                            while (lines[startIndex][0] == '#')
                                startIndex++; //We skip comments (TODO We can add the comment when we put the mouse on the button
                            string[] infotypes = lines[startIndex].Split(':');
                            datatype = infotypes[1];
                        }

                        CorrespondenceType ct = DataSetInformations.instance.getCorrespondenceType(datatype);

                        if (id != -1)
                            co = new Correspondence(0, ct, SetsManager.instance.IDUser, _info_id, id, infoname, "", 1, true, datatype, DateTime.Now.ToString(), 0, DataSetInformations.instance);
                        else
                        {
                            co = new Correspondence(0, ct, SetsManager.instance.IDUser, _info_id, upload_infos_id, infoname, "", 1, true, datatype, DateTime.Now.ToString(), 0, DataSetInformations.instance);
                            upload_infos_id += 1;
                        }
                        _info_id++;
                        ct.addCorrespondence(co);

                        co.startIndex = 0;
                        co.parseTuple(infoslist, co.name);
                        co.parseLine();
                        DataSetInformations.instance.reOrganize();
                    }
                    else if (cmds[0].StartsWith("INFCOL_"))
                    {   //LOAD INFO COLOR LPY
                        color_load = false;
                        int id = -1;
                        Message("Load simulation color");
                        string infoname = cmds[0].Substring(7);
                        string infoslist = cmds[1];
                        for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
                        {
                            if (!MorphoTools.GetDataset().CellsByTimePoint.ContainsKey(t))
                                MorphoTools.GetDataset().CellsByTimePoint[t] = new List<Cell>();
                        }

                        //First whe check if this corrrspondence already exist, it's just a update
                        Correspondence co = DataSetInformations.instance.getCorrespondenceByName(infoname);
                        if (co != null)
                        {
                            id = DataSetInformations.instance.getCorrespondenceByName("simulation-color").id_infos;
                            for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
                                foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
                                {
                                    cell.RemoveUploadColorInfo(id);
                                    cell.clearInfos(co.id_infos);
                                }
                            DataSetInformations.instance.deleteCorrespondence(co, false);
                        }
                        string[] lines = infoslist.Split("\n"[0]);
                        string datatype = "";
                        if (lines.Length >= 1)
                        { //OTHER WISE WE DON'T HAVE ENOUGH INFOS..
                            int startIndex = 0;
                            while (lines[startIndex][0] == '#')
                                startIndex++; //We skip comments (TODO We can add the comment when we put the mouse on the button
                            string[] infotypes = lines[startIndex].Split(':');
                            datatype = infotypes[1];
                        }

                        CorrespondenceType ct = DataSetInformations.instance.getCorrespondenceType(datatype);

                        if (id != -1)
                            co = new Correspondence(0, ct, SetsManager.instance.IDUser, 2, id, infoname, "", 1, true, datatype, DateTime.Now.ToString(), 0, DataSetInformations.instance);
                        else
                        {
                            co = new Correspondence(0, ct, SetsManager.instance.IDUser, 2, upload_infos_id, infoname, "", 1, true, datatype, DateTime.Now.ToString(), 0, DataSetInformations.instance);
                            upload_infos_id += 1;
                        }
                        ct.addCorrespondence(co);

                        co.startIndex = 0;
                        co.parseTuple(infoslist, co.name);
                        co.parseLineLPY(MorphoTools.GetDataset().MinTime, MorphoTools.GetDataset().MaxTime);
                        co.activeColorLPY();
                        DataSetInformations.instance.reOrganize();
                        color_load = true;
                    }
                    else if (cmds[0].StartsWith("COLOR_"))
                    {   //RELOAD INFO COLOR LPY
                        color_load = false;
                        Message("Reload simulation color");
                        string infoname = "simulation-color";
                        int time = int.Parse(cmds[0].Substring(6));
                        string infoslist = cmds[1];

                        Correspondence co = DataSetInformations.instance.getCorrespondenceByName(infoname);

                        if (co.inf.transform.Find("show").gameObject.transform.Find("Text").gameObject.transform.GetComponent<Text>().text != "hide")
                            co.activeColorLPY();

                        co.startIndex = 0;
                        co.parseTuple(infoslist, co.name);
                        co.parseLineLPY(MorphoTools.GetDataset().MinTime, MorphoTools.GetDataset().MaxTime);
                        co.activeColorLPY();
                        co.loadColor(true);
                        DataSetInformations.instance.reOrganize();
                    }
                    else if (cmds[0].StartsWith("MAP_"))
                    {   //LOAD SIMULATION COLORMAP
                        Message("Load simulation colormap");
                        string colormap = cmds[1];

                        InterfaceManager.instance.MenuObjects.transform.Find("Selection").gameObject.GetComponent<SelectionManager>().MenuLoadSelection.GetComponent<ColorMapManager>().LoadColormapPlot(colormap);
                    }
                    else if (cmds[0].StartsWith("ACTIONS"))
                    {
                        previous_actions = new List<string>();
                        string actions = cmds[1];
                        foreach (string action in actions.Split(','))
                        {
                            if (action != "]" && action != "[" && action != "ACTIONS")
                            {
                                string real_action = action.Replace("Time:", "Time ");
                                real_action = real_action.Replace("ID:", "ID&");
                                real_action = real_action.Replace("Plugin:", "");
                                real_action = real_action.Replace("\"", "");
                                real_action = real_action.Replace("\'", "");
                                real_action = real_action.Replace("]:", "");
                                real_action = real_action.Replace("]", "");
                                real_action = real_action.Replace("[", "");
                                real_action = real_action.Replace("_", " ");
                                real_action = real_action.Replace(".", ",");
                                //real_action = real_action.Replace(":", ";");
                                real_action = real_action.Replace("ID&", "Objects:");
                                real_action = real_action.Replace(";", "\n");
                                real_action = real_action.Replace("-", "\n");
                                previous_actions.Add(real_action);
                            }

                        }
                        string next_text = "Cancel : " + previous_actions.Last();
                        last_action_mouse_hover.text = next_text;

                    }
                    else if (cmds[0].StartsWith("CUR_"))
                    {
                        string infoname = cmds[0].Substring(4);
                        Message("Load curration for info " + infoname);
                        string infoslist = cmds[1];
                        Correspondence co = MorphoTools.GetDataset().Infos.getCorrespondenceByName(infoname);
                        if (co == null)
                            MorphoDebug.Log("Error info not found ...");
                        else
                        {
                            GameObject gocor = MenuCurrated.instance.getCorrespondence(co);
                            if (gocor == null)
                                MorphoDebug.Log("Error coorespondance for info not found ...");
                            else
                            {
                                string[] lines = infoslist.Split("\n"[0]);
                                int temp_id_people = 0;
                                for (int i = 0; i < lines.Length; i++)
                                    if (lines[i].Length > 1 && lines[i][0] != '#' && !lines[i].Contains("type:"))
                                    {
                                        int p = lines[i].IndexOf(":");
                                        int d = lines[i].IndexOf("#");
                                        string id_object = lines[i].Substring(0, p);
                                        string value = lines[i].Substring(p + 1, d - p - 1);
                                        string date = lines[i].Substring(d + 1);
                                        Cell c = MorphoTools.GetDataset().getCell(id_object, false);
                                        if (c != null)
                                        {
                                            Curation cur = new Curation(gocor, MenuCurrated.instance.directPlotCurId, c, value, temp_id_people, date, MenuCurrated.instance, false, true);
                                            co.addCuration(cur);
                                            MenuCurrated.instance.directPlotCurId++;
                                            MenuCurrated.instance.setLastCuration(co, c, cur);
                                        }
                                    }
                            }

                        }
                    }
                    else if (cmds[0].StartsWith("DEL_"))
                    { //DELETE A SPECIFIC TIME STEP
                        int t = int.Parse(cmds[0].Substring(4));
                        Message("Delete Meshes at  " + t);
                        MorphoTools.GetDataset().deleteAt(t);

                        if (t == MorphoTools.GetDataset().MaxTime)
                        { //READJUST THE TIMES
                            MorphoTools.GetDataset().MaxTime--;
                            if (MorphoTools.GetDataset().MaxTime < 0)
                                MorphoTools.GetDataset().MaxTime = 0;
                            InterfaceManager.instance.initializeTime(MorphoTools.GetDataset().MinTime, MorphoTools.GetDataset().MaxTime);
                            InterfaceManager.instance.UpdateCustomTimePoints();
                        }
                        if (MorphoTools.GetDataset().CurrentTime == t)
                            InterfaceManager.instance.setTime(t - 1);
                    }
                    else if (cmds[0].StartsWith("RAW_"))
                    {
                        String[] shapet = cmds[0].Substring(4).Split('_');
                        int t = int.Parse(shapet[0]);
                        int c = int.Parse(shapet[1]);
                        if (MorphoTools.GetDataset().CurrentTime == t)
                        {
                            if (raws == null)
                            {
                                raws = MorphoTools.GetRawImages();
                            }
                            Message("Read Raw Data at " + t + " (" + c + " channels)");

                            //when receiving a raw image, if it has not yet been activated, update the GUI
                            /*if(!InterfaceManager.instance.MenuImages.RawActive())
                                InterfaceManager.instance.MenuImages.VisibilitySetValue(true);//first change*/

                            //if dataset does not have cells by time point entry for this time
                            if (!MorphoTools.GetDataset().CellsByTimePoint.ContainsKey(t))
                            {
                                MorphoTools.GetDataset().CellsByTimePoint[t] = new List<Cell>();
                            }
                            raws.RawDim[0] = int.Parse(shapet[2]);
                            raws.RawDim[1] = int.Parse(shapet[3]);
                            raws.RawDim[2] = int.Parse(shapet[4]);
                            raws.OriginalRawDim[0] = int.Parse(shapet[2]);
                            raws.OriginalRawDim[1] = int.Parse(shapet[3]);
                            raws.OriginalRawDim[2] = int.Parse(shapet[4]);
                            raws.scaleFactor = int.Parse(shapet[5]);
                            raws.zScaleFactor = int.Parse(shapet[6]);

                            //set min and max values for interface
                            string[] minmaxes = shapet[16].Replace("[", "").Replace("]", "").Split(',');
                            for (int i = 0; i < minmaxes.Length / 2; i++)//each group is a channel
                            {
                                raws.MinValuesAt[i][t] = int.Parse(minmaxes[i * 2].Trim(), CultureInfo.InvariantCulture);
                                raws.MaxValuesAt[i][t] = int.Parse(minmaxes[i * 2 + 1].Trim(), CultureInfo.InvariantCulture);
                            }

                            // Why Emrbyo center is updated by raw ? Raw should adapt to center ..
                            MorphoTools.GetDataset().embryoCenter = new Vector3(int.Parse(shapet[7]), int.Parse(shapet[8]), int.Parse(shapet[9]));

                            raws.setCube(www.downloadHandler.data, c);
                            float vx = float.Parse(shapet[10].Trim(), CultureInfo.InvariantCulture);
                            float vy = float.Parse(shapet[11].Trim(), CultureInfo.InvariantCulture);
                            float vz = float.Parse(shapet[12].Trim(), CultureInfo.InvariantCulture);
                            raws.VoxelSize[0] = vx;
                            raws.VoxelSize[1] = vy;
                            raws.VoxelSize[2] = vz;

                            int rx = int.Parse(shapet[13].Trim(), CultureInfo.InvariantCulture);
                            int ry = int.Parse(shapet[14].Trim(), CultureInfo.InvariantCulture);
                            int rz = int.Parse(shapet[15].Trim(), CultureInfo.InvariantCulture);

                            int srx = rx == -1 ? raws.RawDim[0] : rx;
                            int sry = ry == -1 ? raws.RawDim[1] : ry;
                            int srz = rz == -1 ? raws.RawDim[2] : rz;

                            InterfaceManager.instance.MenuImages.SetImagePropertyLabels(srx, sry, srz, vx, vy, vz, (int)raws.scaleFactor, (int)raws.zScaleFactor);

                            if (shapet.Length > 11)
                            {

                                raws.RawDim[0] = Mathf.RoundToInt(int.Parse(shapet[2].Trim(), CultureInfo.InvariantCulture) * vx);
                                raws.RawDim[1] = Mathf.RoundToInt(int.Parse(shapet[3].Trim(), CultureInfo.InvariantCulture) * vy);
                                raws.RawDim[2] = Mathf.RoundToInt(int.Parse(shapet[4].Trim(), CultureInfo.InvariantCulture) * vz);
                            }

                            raws.setCubeSize();
                            raws.CubePlan.SetActive(true);
                            raws.CubeScale.SetActive(true);
                            Message("Finished reading Raw Data at " + t);
                        }
                    }
                }catch(Exception e)
                {
                    Debug.LogError("coroutine receive crashed : "+e.Message+" \n" + e.StackTrace);
                    _ReceiveCommandCoroutine = null;
                    _ReceiveCommandCoroutine = StartCoroutine(receivePythonCommand(port_send));
                    yield break;
                }
                    if (cmds[0].StartsWith("START"))
                    {
                        String[] Tims = cmds[0].Split('_');
                        int MinTime = int.Parse(Tims[1]);
                        int MaxTime = int.Parse(Tims[2]);
                        yield return new WaitUntil(() => MorphoTools.GetDataset() != null);
                        if (MinTime != MorphoTools.GetDataset().MinTime || MaxTime != MorphoTools.GetDataset().MaxTime)
                        {
                            MorphoTools.GetDataset().MinTime = MinTime;
                            MorphoTools.GetDataset().MaxTime = MaxTime;


                            InterfaceManager.instance.initializeTime(MinTime, MaxTime);
                            InterfaceManager.instance.UpdateCustomTimePoints();
                        }
                        Message("Initialize Morphoplot from " + MinTime + " to " + MaxTime);
                    }
                    else if (cmds[0].StartsWith("STOPLOAD"))
                    {
                        InterfaceManager.stopLoading();

                        if (!_LoadTimeDisplayed)
                        {
                            DateTime finishTime = DateTime.Now;
                            TimeSpan secs = TimeSpan.FromSeconds((finishTime - _BeginPlotTime).TotalSeconds);
                            Message("Dataset loaded in " + secs.Minutes + "m " + secs.Seconds + "s");
                            _LoadTimeDisplayed = true;
                        }
                        
                    }
                    www.Dispose();
                    _ReceiveCommandCoroutine = StartCoroutine(receivePythonCommand(portNumber));
                    InterfaceManager.stopLoading();
                }
            
        }

        // FOR VALUE OF THE SLIDER
        public void ValueSlider(GameObject sd)
        {
            sd.transform.Find("Handle Slide Area").Find("Handle").Find("TextValue").GetComponent<Text>().text = "" + sd.GetComponent<Slider>().value;
        }

        public void showExplanation(GameObject ex)
        {
            if (Explanation_Active != ex) //CHANGE
            {
                if (Explanation_Active != null)
                    Explanation_Active.SetActive(false);
                ex.SetActive(!ex.activeSelf);
                Explanation_Active = ex;
            }
            else //CLOSE EVERYTHING
            {
                Explanation_Active.SetActive(false);
                Explanation_Active = null;
            }
        }

        public void showExplanationLpy(GameObject ex)
        {
            if (ExplanationLpy_Active != ex) //CHANGE
            {
                if (ExplanationLpy_Active != null)
                    ExplanationLpy_Active.SetActive(false);
                ex.SetActive(!ex.activeSelf);
                ExplanationLpy_Active = ex;
            }
            else //CLOSE EVERYTHING
            {
                ExplanationLpy_Active.SetActive(false);
                ExplanationLpy_Active = null;
            }
        }

        public void sendCommand(GameObject btn, bool deepsearch=false)
        {
                var objectsS = new StringBuilder();  //Create the list of
                for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
                { //Selected Cells
                    Cell c = MorphoTools.GetPickedManager().clickedCells[i];
                    string ch = "0";
                    if (c.Channels.Count == 1)
                        ch = c.Channels.Keys.First();
                    else
                        ch = _CurrentSegChannel.ToString();
                    objectsS.Append(c.t + "," + c.ID + ","+ch+";");
                }
                if (objectsS.Length == 0) // Copy Paste only use picked
                {//Nothing were selected, we look in the selection !
                    for (int t = MorphoTools.GetDataset().MinTime; t <= MorphoTools.GetDataset().MaxTime; t++)
                    {
                        if (MorphoTools.GetDataset().CellsByTimePoint.ContainsKey(t))
                        {
                            foreach (Cell cell in MorphoTools.GetDataset().CellsByTimePoint[t])
                            {
                                if (cell.selection != null && cell.selection.Count > 0)
                                {
                                    for (int i = 0; i < cell.selection.Count; i++)
                                    {
                                        string ch = "0";
                                        if (cell.Channels.Count == 1)
                                            ch = cell.Channels.Keys.First();
                                        else
                                            ch = _CurrentSegChannel.ToString();
                                        objectsS.Append(cell.t + "," + cell.ID + "," + ch + "," +  cell.selection[i] + ";");
                                    }
                                }
                            }
                        }
                    }
                }
                string objectsSD = objectsS.ToString();
                if (objectsSD.Length > 0)
                    objectsSD = objectsSD.Substring(0, objectsSD.Length - 1);
                StartCoroutine(sendPythonCommand(objectsSD, btn, deepsearch));

        }

        public IEnumerator sendPythonCommand(string objectsS, GameObject btn, bool deepsearch=false)
        {
            // MorphoDebug.Log("sending command !");
            if (!isAvaible)
                Message("Wait a command is already running ");
            else
            {
                isAvaible = false;


                //Message("Send command " + btn.name);
                WWWForm form = new WWWForm();
                form.AddField("action", btn.name.Replace(" ", "%20"));
                form.AddField("time", MorphoTools.GetDataset().CurrentTime);
                form.AddField("seg_channel", _CurrentSegChannel);
                if (!btn.name.Contains("Copy Paste")) form.AddField("objects", objectsS);
                if (btn.name == "Reload all elements")
                {
                    Transform content = null;
                    foreach (GameObject pgo in ParentsLPY.Values)
                    {
                        if (pgo.name == "Element Parameters")
                        {
                            for (int ic = 0; ic < pgo.transform.childCount; ic++)
                            {
                                GameObject co = pgo.transform.GetChild(ic).gameObject;
                                if (co.name == "SV")
                                {
                                    content = co.transform.Find("ScrollView").Find("Viewport").Find("Content");
                                }
                            }
                        }
                    }
                    foreach (Transform child in content)
                    {
                        foreach (Transform grandchild in child)
                        {
                            if (grandchild.name.StartsWith("IF_") || grandchild.name.StartsWith("FP_") || grandchild.name.StartsWith("FS_"))
                            {//INPUT FIELDS
                                form.AddField(grandchild.name.Substring(3).Replace(" ", "%20"), grandchild.transform.Find("Text").GetComponent<Text>().text.Replace(" ", "%20"));
                            }
                            if (grandchild.name.StartsWith("SD_"))
                            {//SLIDER
                                form.AddField(grandchild.name.Substring(3).Replace(" ", "%20"), "" + grandchild.GetComponent<Slider>().value);
                            }
                            if (grandchild.name.StartsWith("CB_") || grandchild.name.StartsWith("TG_"))
                            {//TOGGLE
                                form.AddField(grandchild.name.Substring(3).Replace(" ", "%20"), "" + grandchild.GetComponent<Toggle>().isOn);
                            }
                        }
                    }
                    MorphoTools.GetPickedManager().ClearSelectedCells();
                }
                else
                {
                    foreach (Transform child in btn.transform)
                    {
                        Transform t = child;
                        if (deepsearch)
                            t = t.Find(child.name);
                        if(t!=null)
                        {
                            if (t.name.StartsWith("IF_"))
                            {//INPUT FIELDS
                                form.AddField(t.name.Substring(3).Replace(" ", "%20"), t.transform.Find("Text").GetComponent<Text>().text.Replace(" ", "%20"));
                            }
                            if (t.name.StartsWith("FP_"))
                            {//INPUT FIELDS
                                form.AddField(t.name.Substring(3).Replace(" ", "%20"), t.GetComponent<InputField>().text.Replace(" ", "%20"));
                            }
                            if (t.name.StartsWith("FS_"))
                            {//INPUT FIELDS
                                form.AddField(t.name.Substring(3).Replace(" ", "%20"), t.GetComponent<InputField>().text.Replace(" ", "%20"));
                            }
                            if (t.name.StartsWith("DD_"))
                            {//DROPDOWN
                                form.AddField(t.name.Substring(3).Replace(" ", "%20"), t.GetComponent<Dropdown>().value);
                            }
                            if (t.name.StartsWith("CD_"))
                            {//COORDINATES
                                var CoordsS = new StringBuilder();  //Create the list of coordinates
                                foreach (GameObject cdho in Coordinates)
                                {
                                    Vector3 pos = cdho.transform.localPosition / InterfaceManager.instance.initCanvasDatasetScale;
                                    CoordsS.Append(pos.ToString() + ";");
                                }
                                string CoordsD = CoordsS.ToString();
                                if (CoordsD.Length > 0)
                                    CoordsD = CoordsD.Substring(0, CoordsD.Length - 1);
                                form.AddField(child.name.Substring(3).Replace(" ", "%20"), CoordsD);
                                //clearAllSpheres();
                            }
                            if (child.name.StartsWith("MI_"))
                            {
                                Cell c = MorphoTools.GetDataset().getCell(objectsS.Split(';')[0], false);
                                if (c != null)
                                {
                                    Mesh m = c.getChannel(MorphoTools.GetDataset().currentChannel).MeshFilter.mesh;
                                    //string mesh = MeshToObjCreator.ConvertMeshToObj(m,c.t+","+c.ID);
                                    string mesh = MeshToObjCreator.ConvertMeshToObjWithoutDuplicateRemoving(m, c.t + "," + c.ID);
                                    form.AddField("mesh_deform", mesh);
                                }

                            }
                            if (child.name.StartsWith("TI_"))
                            {
                                var CoordsS = new StringBuilder();  //Create the list of coordinates
                                Cell c = null;
                                for (int i = 0; i < cells_copy.Count; i++)
                                {
                                    if (c == null ||
                                        cells_copy[i].source_object.getName() ==
                                        c.getName()) // Do this because to prevent any problems with multi cells
                                    {
                                        if (c == null)
                                        {
                                            c = cells_copy[i].source_object;
                                            form.AddField("objects",
                                                cells_copy[i].source_object.t + "," + cells_copy[i].source_object.ID +
                                                "," + cells_copy[i].source_object.getFirstChannel().getName());

                                        }

                                        //CellChannelRenderer c2 = cells_copy[i].source_object.getFirstChannel();
                                        NumberFormatInfo nfi = new NumberFormatInfo();
                                        nfi.NumberDecimalSeparator = ".";
                                        Vector3 translation = (cells_copy[i].copy_target.transform.parent.localPosition - cells_copy[i].shift)/InterfaceManager.instance.initCanvasDatasetScale;
                                        Quaternion rotation = cells_copy[i].copy_target.transform.parent.localRotation;
                                        //float scale = 1f;
                                        float scale = cells_copy[i].copy_target.transform.parent.localScale.x;
                                        string coords = translation.x.ToString(nfi) + "," + translation.y.ToString(nfi) + "," + translation.z.ToString(nfi) +
                                                        "," + rotation.x.ToString(nfi) + "," + rotation.y.ToString(nfi) + "," + rotation.z.ToString(nfi) + "," +
                                                        rotation.w.ToString(nfi) + "," + scale.ToString("F2",nfi) + ";";
                                        //Debug.Log("Sending : "+coords);
                                        CoordsS.Append(coords);
                                    }
                                }

                                ClearAllCopies();
                                if (c != null)
                                {
                                    string CoordsD = CoordsS.ToString();
                                    if (CoordsD.Length > 0)
                                        CoordsD = CoordsD.Substring(0, CoordsD.Length - 1);
                                    form.AddField("copy_objects", CoordsD);
                                }

                            }
                            if (child.name.StartsWith("SD_"))
                            {//SLIDER
                                form.AddField(child.name.Substring(3).Replace(" ", "%20"), "" + child.GetComponent<Slider>().value);
                            }
                            if (child.name.StartsWith("TG_") || (child.name.StartsWith("CB_")))
                            {//TOGGLE
                                form.AddField(child.name.Substring(3).Replace(" ", "%20"), 
                                    "" + child.Find(child.name).GetComponent<Toggle>().isOn);
                            }
                        }
                        
                    }
                    if (btn.name.Contains("Reload element"))
                        MorphoTools.GetPickedManager().ClearSelectedCells();
                   
                }

                InterfaceManager.setLoading();
                UnityWebRequest www = UnityWebRequests.Post(plot_adress + port_receive, "/ send.html", form);
                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                    MorphoDebug.Log("Error sending command ! : " + www.error);
                else
                {
                    MorphoDebug.Log("Successfully sent command "+ btn.name);
                    //String commande = Encoding.UTF8.GetString(www.downloadHandler.data);
                    //if (btn.name != "showraw") UIManager.instance.MenuObjects.transform.Find("Selection").GetComponent<SelectionColor>().clearAllColors();
                    

#if UNITY_WEBGL//lineage update:
                    Lineage.UpdateLineage();

#endif


                }
                
                isAvaible = true;
                Message("");
                www.Dispose();


            }
        }

        public IEnumerator sendPythonCommand(string objectsS, string cmd_name, Correspondence cor=null)
        {
            // MorphoDebug.Log("sending command !");
            if (!isAvaible)
            {
                Message("Wait a command is already running ");
            }
            else
            {
                isAvaible = false;

                //Message("Send command " + cmd_name);
                WWWForm form = new WWWForm();
                form.AddField("action", cmd_name.Replace(" ", "%20"));
                form.AddField("time", MorphoTools.GetDataset().CurrentTime);
                form.AddField("seg_channel", _CurrentSegChannel);
                form.AddField("objects", objectsS);
                
                InterfaceManager.setLoading();
                UnityWebRequest www = UnityWebRequests.Post(plot_adress + port_receive, "/ send.html", form);
                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                    MorphoDebug.Log("Error sending command ! : " + www.error);
                else
                {
                    MorphoDebug.Log("Successfully sent command " + cmd_name.Replace(" ", "%20"));


                }
                
                isAvaible = true;
                Message("");
                www.Dispose();

                if (cor != null)
                    cor.inf.transform.Find("load").gameObject.SetActive(false);
            }
            
        }

        public IEnumerator sendPythonCommand(string objectsS, string cmd_name,string mesh_deform)
        {
            // MorphoDebug.Log("sending command !");
            if (!isAvaible)
                Message("Wait a command is already running ");
            else
            {
                isAvaible = false;

                //Message("Send command " + cmd_name);
                WWWForm form = new WWWForm();
                form.AddField("action", cmd_name.Replace(" ", "%20"));
                form.AddField("time", MorphoTools.GetDataset().CurrentTime);
                form.AddField("seg_channel", _CurrentSegChannel);
                form.AddField("objects", objectsS);
                form.AddField("mesh_deform", mesh_deform);

                InterfaceManager.setLoading();
                UnityWebRequest www = UnityWebRequests.Post(plot_adress + port_receive, "/ send.html", form);
                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                    MorphoDebug.Log("Error sending command ! : " + www.error);
                else
                {
                    MorphoDebug.Log("Successfully sent command " + cmd_name.Replace(" ", "%20"));


                }
                
                isAvaible = true;
                Message("");
                www.Dispose();


            }
        }

        //reset dataset applied modifiers
        public void ResetDatasetAppliedModifiers()
        {
            MorphoTools.GetDataset().TransformationsManager.ApplyTransformationsOnTimeChange();
            if (MorphoTools.GetScatter().scatterReady)
            {
                MorphoTools.GetScatter().UpdateView(MorphoTools.GetDataset().CurrentTime,true);
            }
        }


        //SEND COMMAND UPLOAD DATASET
        public void show_uploadbox()
        { uploadBox.SetActive(true); }

        public void cancel_uploadbox()
        { uploadBox.SetActive(false); }

        public void upload_dataset()
        {
            StartCoroutine(sendPythonCommand(uploadBox.transform.Find("datasetname").transform.Find("Text").GetComponent<Text>().text, MenuSegmentation.transform.Find("upload").gameObject));
            cancel_uploadbox();
        }

        //SEND COMMAND UPLOAD SIMULATION
        public void show_uploadbox_LPY()
        { uploadBoxLPY.SetActive(true); }

        public void cancel_uploadbox_LPY()
        { uploadBoxLPY.SetActive(false); }

        public void upload_dataset_LPY()
        {
            StartCoroutine(SendUploadLPY(uploadBoxLPY.transform.Find("comment").Find("Text").GetComponent<Text>().text));
            cancel_uploadbox_LPY();
        }

        //SEND COMMANDE REDO
        public void redo_LPY()
        {
            StartCoroutine(sendPythonCommand("", MenuSimulation.transform.Find("redo").gameObject));
        }

        //BUTTONS
        public void addBtnListener(GameObject gobtn)
        { gobtn.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(() => onClickSendCommand(gobtn)); }

        public void onClickSendCommand(GameObject gobtn)
        { sendCommand(gobtn); }

        public int heightBttn = 20;
        public int spaceBttn = 5;
        public float maxWidth = 0;

        public void reorganizeBttns()
        {
            //MorphoDebug.Log("Start Reogrganize");
            float gheight = 50;
            foreach (GameObject pgo in Parents.Values) //For Each Parent
            {
                pgo.transform.localPosition = new Vector3(0, -gheight, 0);
                //MorphoDebug.Log("Process ->" + pgo.name);
                float height = 30; //Starting Shift from the parent title
                foreach (Transform child in pgo.transform) //For Each Plugin
                {
                    if (child.name != "Text")
                    {
                        child.transform.localPosition = new Vector3(10, -height, 0);
                        //MorphoDebug.Log(" Add " + child.name);
                        float widthBttn = child.Find("Btn").Find("Text").GetComponent<Text>().preferredWidth + spaceBttn + 40; //Size of the valid Button
                        foreach (Transform grandchild in child.transform) //For Each button
                        {
                            //MorphoDebug.Log("grandchild->" + grandchild.name + " with " + grandchild.gameObject.GetComponent<RectTransform>().sizeDelta.x);
                            if (grandchild.gameObject.activeSelf && grandchild.name != "Btn")
                            {
                                grandchild.gameObject.transform.localPosition = new Vector3(widthBttn, 0, 0);
                                widthBttn += spaceBttn + grandchild.gameObject.GetComponent<RectTransform>().sizeDelta.x;
                                //TODO Du coup on peut aussi mettre son commentaire au bon endroit (à fgauche du menu)
                            }
                        }
                        GameObject explanation = child.Find("Explanation").gameObject;
                        if (explanation.activeSelf)
                        {
                            explanation.transform.localPosition = new Vector3(widthBttn, 0, 0); //Place at the end
                            widthBttn += 20;
                        }

                        height += (20 + spaceBttn); //Normalement c'est + heightBttn mais ca valeur change mysterieusement ....
                        if (widthBttn > maxWidth)
                            maxWidth = widthBttn;
                    }
                }
                height += spaceBttn;
                pgo.GetComponent<RectTransform>().sizeDelta = new Vector3(260, height);
                gheight += height;
            }
            //Fit the maxixmum size widht

            foreach (GameObject pgo in Parents.Values)
                pgo.GetComponent<RectTransform>().sizeDelta = new Vector3(maxWidth + 15, pgo.GetComponent<RectTransform>().sizeDelta.y);

            Explanations.transform.localPosition = new Vector3(maxWidth + 20, -50, 0);
        }

        public float maxWidthLPY = 0;

        public void reorganizeBttnsLPY()
        {
            float gheight = 50;
            float height, widthBttn, heightall, wid, widthText;
            Transform btn, param, placeholder, content;
            foreach (GameObject pgo in ParentsLPY.Values) //For Each Parent
            {
                pgo.transform.localPosition = new Vector3(0, -gheight, 0);
                height = 30; //Starting Shift from the parent title
                foreach (Transform child in pgo.transform) //For Each Plugin
                {
                    if (child.name == "SV")
                    {
                        height = 0;
                        float h = height;
                        float w = 0;
                        content = child.transform.Find("ScrollView").Find("Viewport").Find("Content");
                        float heightContent = 0;
                        foreach (Transform grandchild in content) //For Each Plugin
                        {
                            grandchild.transform.localPosition = new Vector3(10, -10 + heightContent, 0);
                            btn = grandchild.Find("Btn");
                            widthBttn = btn.Find("Text").GetComponent<Text>().text.Length * 8 + spaceBttn + 15 + 10;
                            heightall = 0; //Size of the valid Button
                            wid = 0;
                            widthText = 0;
                            foreach (Transform grandgrandchild in grandchild.transform) //For Each button
                            {
                                widthText = 0;
                                if (grandgrandchild.gameObject.activeSelf && grandgrandchild.name != "Btn")
                                {
                                    if (grandgrandchild.name.Contains("IF"))
                                    {
                                        param = grandgrandchild.Find("parameterName");
                                        placeholder = grandgrandchild.Find("Placeholder");
                                        widthText = param.GetComponent<Text>().preferredWidth + spaceBttn;
                                        grandgrandchild.gameObject.transform.localPosition = new Vector3(10 + widthText, heightall, 0);
                                        placeholder.gameObject.transform.localPosition = new Vector3(0, 0, 0);
                                        param.gameObject.transform.localPosition = new Vector3(-widthText + 10 + spaceBttn + (grandgrandchild.GetComponent<InputField>().text.Length - 1) * 3, -10, 0);
                                        wid = widthText + spaceBttn + placeholder.GetComponent<Text>().preferredWidth;
                                    }
                                    else if (grandgrandchild.name.Contains("SD"))
                                    {
                                        param = grandgrandchild.Find("parameterName");
                                        widthText = param.GetComponent<Text>().preferredWidth + spaceBttn;
                                        grandgrandchild.gameObject.transform.localPosition = new Vector3(widthText / 2, heightall, 0);
                                        param.transform.localPosition = new Vector3(10, -10, 0);
                                        grandgrandchild.Find("Background").gameObject.transform.localPosition = new Vector3(widthText, -10, 0);
                                        grandgrandchild.Find("Fill Area").gameObject.transform.localPosition = new Vector3(widthText, -10, 0);
                                        grandgrandchild.Find("Handle Slide Area").gameObject.transform.localPosition = new Vector3(widthText, -10, 0);
                                        wid = widthText + grandgrandchild.Find("Background").GetComponent<RectTransform>().sizeDelta.x + spaceBttn;
                                        heightall -= heightBttn / 2;
                                    }
                                    else if (grandchild.name.Contains("TG"))
                                    {
                                        grandgrandchild.gameObject.transform.localPosition = new Vector3(10 + spaceBttn, heightall, 0);
                                        param = grandgrandchild.Find("parameterName");
                                        param.transform.localPosition = new Vector3(10, -10, 0);
                                        widthText = param.GetComponent<Text>().preferredWidth + spaceBttn + 10;
                                        grandgrandchild.Find("Background").gameObject.transform.localPosition = new Vector3(widthText, -10, 0);
                                        wid = widthText + grandgrandchild.Find("Background").gameObject.GetComponent<RectTransform>().sizeDelta.x + spaceBttn;
                                    }
                                    else
                                    {
                                        grandgrandchild.gameObject.transform.localPosition = new Vector3(0, heightall, 0);
                                        wid = grandgrandchild.gameObject.GetComponent<RectTransform>().sizeDelta.x + spaceBttn;
                                    }
                                    heightall -= spaceBttn + heightBttn;
                                    if (wid > widthBttn)
                                        widthBttn = wid;
                                }
                            }
                            widthText = btn.Find("Text").GetComponent<Text>().text.Length * 8;
                            btn.gameObject.transform.localPosition = new Vector3(widthText + 10, heightall, 0);
                            btn.Find("Text").gameObject.transform.localPosition = new Vector3(-widthText - 2 * spaceBttn, -10, 0);
                            wid = widthText + 40 + spaceBttn;
                            if (wid > widthBttn)
                                widthBttn = wid;
                            heightall += -spaceBttn - heightBttn;

                            GameObject explanationLpy = grandchild.Find("Explanation").gameObject;
                            if (explanationLpy.activeSelf)
                            {
                                explanationLpy.transform.localPosition = new Vector3(0, heightall, 0); //Place at the end
                                heightall -= 20;
                            }

                            height += (30 + spaceBttn) - heightall;
                            if (widthBttn + 20 > maxWidthLPY)
                                maxWidthLPY = widthBttn + 20;
                            if (widthBttn + 20 > w)
                                w = widthBttn;
                            heightContent += heightall;
                        }
                        child.transform.localPosition = new Vector3(0, -h, 0);
                        content.GetComponent<RectTransform>().sizeDelta = new Vector3(w + 20, height);
                        content.transform.localPosition = new Vector3(0, 0, 0);
                        if (height > 150)
                        {
                            height = 150;
                            child.transform.localPosition = new Vector3(0, -h, 0);
                        }
                        if (content.childCount == 1)
                        {
                            child.transform.localPosition = new Vector3(0, -h + 20, 0);
                        }

                        child.Find("ScrollView").GetComponent<RectTransform>().sizeDelta = new Vector3(w + 45, height);
                        height += 30;
                        if (w + 55 > maxWidthLPY)
                            maxWidthLPY = w + 55;
                    }
                    else if (child.name != "Text")
                    {
                        child.transform.localPosition = new Vector3(10, -height, 0);
                        btn = child.Find("Btn");
                        widthBttn = btn.Find("Text").GetComponent<Text>().preferredWidth + spaceBttn + 80;
                        heightall = 0; //Size of the valid Button
                        wid = 0;
                        widthText = 0;
                        foreach (Transform grandchild in child.transform) //For Each button
                        {
                            if (grandchild.gameObject.activeSelf && grandchild.name != "Btn")
                            {
                                if (grandchild.name.Contains("IF"))
                                {
                                    param = grandchild.Find("parameterName");
                                    placeholder = grandchild.Find("Placeholder");
                                    widthText = param.GetComponent<Text>().preferredWidth + spaceBttn;
                                    grandchild.gameObject.transform.localPosition = new Vector3(10 + widthText, heightall, 0);
                                    placeholder.gameObject.transform.localPosition = new Vector3(0, 0, 0);
                                    param.gameObject.transform.localPosition = new Vector3(-widthText + 10 + spaceBttn + (grandchild.GetComponent<InputField>().text.Length - 1) * 3, -10, 0);
                                    wid = widthText + spaceBttn + placeholder.GetComponent<Text>().preferredWidth;
                                }
                                else if (grandchild.name.Contains("SD"))
                                {
                                    param = grandchild.Find("parameterName");
                                    widthText = param.GetComponent<Text>().preferredWidth + spaceBttn + 10;
                                    grandchild.gameObject.transform.localPosition = new Vector3(widthText / 2, heightall, 0);
                                    param.transform.localPosition = new Vector3(10, -10, 0);
                                    grandchild.Find("Background").gameObject.transform.localPosition = new Vector3(widthText, -10, 0);
                                    grandchild.Find("Fill Area").gameObject.transform.localPosition = new Vector3(widthText, -10, 0);
                                    grandchild.Find("Handle Slide Area").gameObject.transform.localPosition = new Vector3(widthText, -10, 0);
                                    wid = widthText + grandchild.Find("Background").GetComponent<RectTransform>().sizeDelta.x + spaceBttn;
                                    heightall -= heightBttn / 2;
                                }
                                else if (grandchild.name.Contains("TG"))
                                {
                                    grandchild.gameObject.transform.localPosition = new Vector3(10 + spaceBttn, heightall, 0);
                                    param = grandchild.Find("parameterName");
                                    param.transform.localPosition = new Vector3(10, -10, 0);
                                    widthText = param.GetComponent<Text>().preferredWidth + spaceBttn + 10;
                                    grandchild.Find("Background").gameObject.transform.localPosition = new Vector3(widthText, -10, 0);
                                    wid = widthText + grandchild.Find("Background").gameObject.GetComponent<RectTransform>().sizeDelta.x + spaceBttn;
                                }
                                else
                                {
                                    grandchild.gameObject.transform.localPosition = new Vector3(0, heightall, 0);
                                    wid = grandchild.gameObject.GetComponent<RectTransform>().sizeDelta.x + spaceBttn;
                                }
                                heightall -= spaceBttn + heightBttn;
                                if (wid > widthBttn)
                                    widthBttn = wid;
                            }
                        }
                        widthText = btn.Find("Text").GetComponent<Text>().preferredWidth;
                        btn.gameObject.transform.localPosition = new Vector3(widthText + spaceBttn, heightall, 0);
                        btn.Find("Text").gameObject.transform.localPosition = new Vector3(-widthText - spaceBttn, -10, 0);
                        heightall += spaceBttn - heightBttn;

                        GameObject explanationLpy = child.Find("Explanation").gameObject;
                        if (explanationLpy.activeSelf)
                        {
                            explanationLpy.transform.localPosition = new Vector3(0, heightall, 0); //Place at the end
                            heightall -= 20;
                        }

                        height += (20 + spaceBttn) - heightall;
                        if (widthBttn > maxWidthLPY)
                            maxWidthLPY = widthBttn;
                    }
                }
                height += spaceBttn;
                pgo.GetComponent<RectTransform>().sizeDelta = new Vector3(260, height);
                gheight += height;
            }
            //Fit the maxixmum size width
            foreach (GameObject pgo in ParentsLPY.Values)
                pgo.GetComponent<RectTransform>().sizeDelta = new Vector3(maxWidthLPY + 15, pgo.GetComponent<RectTransform>().sizeDelta.y);

            ExplanationsLpy.transform.localPosition = new Vector3(maxWidthLPY + 20, -50, 0);
        }

        public void show_raw()
        { //SHOW RAW DATA
            if (raws == null)
            {
                raws = MorphoTools.GetRawImages();
            }
            if (raws.rawImagesDownloaded != null) 
            { 
                StopCoroutine(raws.rawImagesDownloaded);
                
                isAvaible = true; 
            }
            if (LoadParameters.instance.id_dataset == 0)
            {
                raws.rawImagesDownloaded = StartCoroutine(sendPythonCommand(raws.GetCurrentChannel().ToString(), "showraw"));
            }
                
        }

        public void hide_raw()
        {
            StartCoroutine(sendPythonCommand("", "hideraw"));
        }

        public void recompute_data() //RECOMPUTE MESH AND PROPERTIES 
        {
            StartCoroutine(recompute_request());
        }
        
        public IEnumerator recompute_request()
        {
            if (!isAvaible)
                Message("Wait a command is already running ");
            else
            {
                isAvaible = false;

                Message("Recompute MorphoNet display data");
                WWWForm form = new WWWForm();
                form.AddField("action", "recompute");
                form.AddField("time", MorphoTools.GetDataset().CurrentTime);
                form.AddField("objects", ";");
                UnityWebRequest www = UnityWebRequests.Post(plot_adress + port_receive, "/ send.html", form);
                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                    MorphoDebug.Log("Error : " + www.error);
                www.Dispose();
                Message("");
                
                isAvaible = true;
            }
        }

        public void cancel_last_action() //CANCEL LAST PLUGIN ACTION USING THE BACKUP (KEPT IN CASE OF , BUT DEPRECATED)
        {
            StartCoroutine(cancel_request());
        }

        public void DisplayCancelMenu()
        {
            string cancel_text = "This action will cancel all plugins until step : " + this.CurrentVisualizedStep +
                                 " (step included). This action is irreversible. Are you sure ?";
            CancelText.text = cancel_text;
            CancelMenu.SetActive(true);

        }

        public void HideCancelMenu()
        {
            CancelText.text = "";
            CancelMenu.SetActive(false);
        }

        public void cancel_to_visualization_step() // NEW CANCEL FUNCTION (TO CURRENT STEP)
        {
            StartCoroutine(cancel_request(to_visualization:true));
        }

        public IEnumerator cancel_request(bool to_visualization=false)
        {
            if (!isAvaible)
                Message("Wait a command is already running ");
            else
            {
                isAvaible = false;

                Message("Cancel last action");
                WWWForm form = new WWWForm();
                form.AddField("action", (to_visualization?"cancel_to_visualization":"cancel"));
                form.AddField("time", MorphoTools.GetDataset().CurrentTime);
                form.AddField("objects", ";");
                UnityWebRequest www = UnityWebRequests.Post(plot_adress + port_receive, "/ send.html", form);
                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                    MorphoDebug.Log("Error : " + www.error);
                www.Dispose();
                Message("");
                HideCancelMenu();
                isAvaible = true;
            }
        }

        public void cut_objects() //CUT THE OBJECTS SELECTED
        {
            StartCoroutine(cut_request());
        }

        public IEnumerator cut_request()
        {
            var objectsS = new StringBuilder();  //Create the list of
            for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
            { //Selected Cells
                Cell c = MorphoTools.GetPickedManager().clickedCells[i];
                objectsS.Append(c.t + "," + c.ID + ",0;");
            }
            string objectsSD = objectsS.ToString();
            if (objectsSD.Length > 0)
                objectsSD = objectsSD.Substring(0, objectsSD.Length - 1);
            if (!isAvaible)
                Message("Wait a command is already running ");
            else
            {
                isAvaible = false;

                Message("Cut the objects");
                WWWForm form = new WWWForm();
                form.AddField("action", "cut");
                form.AddField("time", MorphoTools.GetDataset().CurrentTime);
                form.AddField("objects", objectsSD);
                UnityWebRequest www = UnityWebRequests.Post(plot_adress + port_receive, "/ send.html", form);
                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                    MorphoDebug.Log("Error : " + www.error);
                www.Dispose();
                Message("");
                
                isAvaible = true;
                if (objectsS.Length != 0)
                {
                    MorphoTools.GetPickedManager().ClearSelectedCells();
                }
            }
        }

        //////////////// COORDINATES
        public void addCoordinateListener(GameObject cdbtn)
        { cdbtn.GetComponent<Button>().onClick.AddListener(() => addNewCoordinate()); }

        public void addNewCoordinate()
        {
            addSeed(spherePointer.transform.localPosition / InterfaceManager.instance.initCanvasDatasetScale);
        }
        
        public void addSeed(Vector3 s)
        {
            //MorphoDebug.Log("Add " + s.ToString());
            //resetPointer();
            GameObject NewCoordinate = (GameObject)Instantiate(sphereCoordinate, PointerSegmentation.transform);
            NewCoordinate.transform.localPosition = s * InterfaceManager.instance.initCanvasDatasetScale;
            NewCoordinate.name = "CD_" + Coordinates.Count;
            NewCoordinate.SetActive(true);
            Coordinates.Add(NewCoordinate);
            UpdateSeedPosition(NewCoordinate);
            //syncPointer();
            PointerSegmentation.SetActive(true);

        }

        public void resetPointer()
        {
            PointerSegmentation.transform.rotation = new Quaternion(0F, 0F, 0F, 0F);
            PointerSegmentation.transform.position = new Vector3(0F, 0F, 0F);
            PointerSegmentation.transform.localScale = new Vector3(1F,1F,1F);
        }

        public void syncPointer()
        {
            PointerSegmentation.transform.rotation = MorphoTools.GetTransformationsManager().CurrentRotation;
            PointerSegmentation.transform.localScale = MorphoTools.GetTransformationsManager().CurrentScale;
            PointerSegmentation.transform.position = MorphoTools.GetTransformationsManager().CurrentPosition;
        }
        
        public void syncPointerCopyPaste()
        {
            PointerCopyPaste.transform.rotation = MorphoTools.GetTransformationsManager().CurrentRotation;
            PointerCopyPaste.transform.localScale = MorphoTools.GetTransformationsManager().CurrentScale;
            PointerCopyPaste.transform.position = MorphoTools.GetTransformationsManager().CurrentPosition;
        }

        //Show or Hide the Pointer
        public void showHideSpherePointer(bool active)
        {
            moving = false;
            PointerSegmentation.SetActive(active);
        }
        
        
        public void AssignCellToCopyPastPointer(Cell c)
        {
            CellChannelRenderer cellChannelRenderer = c.getFirstChannel();
            GameObject pivot = new GameObject();
            pivot.name = "pivot_" + c.getName();
            pivot.transform.parent = PointerCopyPaste.transform;
            GameObject cellCopy = Instantiate(cellChannelRenderer.AssociatedCellObject, pivot.transform);
            cellCopy.transform.localScale = new Vector3(InterfaceManager.instance.initCanvasDatasetScale,
                InterfaceManager.instance.initCanvasDatasetScale, InterfaceManager.instance.initCanvasDatasetScale);
            //cellCopy.transform.
            Vector3 shiftPos = cellCopy.transform.TransformPoint(GetCurrentGravityCenter(cellCopy));
            pivot.transform.localScale = Vector3.one;
            pivot.transform.localRotation = Quaternion.identity;
            pivot.transform.localPosition = shiftPos;
            //pivot.transform.localPosition += MorphoTools.GetTransformationsManager().CurrentPosition;
            cellCopy.transform.localPosition -= shiftPos;
            StaticWireframeRenderer wireframeRenderer = cellCopy.AddComponent<StaticWireframeRenderer>();
            wireframeRenderer.WireMaterial = SetsManager.instance.HighlightCopyMaterial;
            wireframeRenderer.Color = Color.white;
            cellCopy.GetComponent<Renderer>().material = SetsManager.instance.HighlightCopyMaterial;
            CellCopyInstance cci = new CellCopyInstance(c, cellCopy,shiftPos);
            cells_copy.Add(cci);
        }







        public void ClearAllCopies()
        {
            if (cells_copy.Count > 0)
            {
                for (int i = 0; i < cells_copy.Count; i++)
                {
                    DestroyImmediate(cells_copy[i].copy_target.transform.parent.gameObject);
                }
            }
            cells_copy.Clear();
        }
        
        public void showHideSpherePointerCopyPaste(bool active)
        {
            moving = false;
            if (cells_copy == null )
            {
                cells_copy = new List<CellCopyInstance>();
            }
            
            if (active)
            {
                if (cells_copy.Count > 0)
                {
                    for (int i = 0; i < cells_copy.Count; i++)
                    {
                        cells_copy[i].copy_target.SetActive(true);
                        if (MorphoTools.GetPickedManager().clickedCells == null ||
                            MorphoTools.GetPickedManager().clickedCells.Count  < 1 || MorphoTools.GetPickedManager().clickedCells.Contains(cells_copy[i].source_object))
                        {
                            MorphoTools.GetPickedManager().SelectCell(cells_copy[i].source_object);
                        }
                    }
                }
                PointerCopyPaste.SetActive(true);
               
                
            }
            else
            {
                if (cells_copy.Count > 0)
                {
                    for (int i = 0; i < cells_copy.Count; i++)
                    {
                        cells_copy[i].copy_target.SetActive(false);
                    }
                }
                PointerCopyPaste.SetActive(false);

            }
        }

        public void StartCopy()
        {
            if (MorphoTools.GetPickedManager().clickedCells != null && MorphoTools.GetPickedManager().clickedCells.Count > 0)
                for (int i = 0; i < MorphoTools.GetPickedManager().clickedCells.Count; i++)
                    AssignCellToCopyPastPointer(MorphoTools.GetPickedManager().clickedCells[i]);
        }

        //Clear all spheres
        public void clearAllSpheres()
        {
            while (Coordinates.Count > 0)
            {
                GameObject cdho = Coordinates.Last();
                //MorphoDebug.Log("Remove " + cdho.name);
                Coordinates.Remove(cdho);
                DestroyImmediate(cdho);
            }
            Resources.UnloadUnusedAssets();
            System.GC.Collect();
        }

        public Plane getPlane(Vector4 gop)
        {
            return new Plane(new Vector3(gop.x, gop.y, gop.z), -gop.w);
        }

        public void UpdateSeedPosition(GameObject go)
        {
            go.transform.localScale = new Vector3(sphere_init_scale * pointer_scale_factor,sphere_init_scale * pointer_scale_factor,sphere_init_scale * pointer_scale_factor);

            if (go.transform.localScale.x < sphere_limit)
            {
                pointer_scale_factor = previous_pointer_scale_factor;
                go.transform.localScale = new Vector3(sphere_limit,sphere_limit,sphere_limit);
            }
        }
        public float sphere_init_scale = 0.2f;
        public float sphere_limit = 0.066f;
        public float cross_init_scale = 1f;
        public float cross_limit = 0.3f;
        public void UpdatePointerScale()
        {
            foreach (GameObject go in Coordinates)
            {
                UpdateSeedPosition(go);
            }

            spherePointer.transform.localScale = new Vector3(cross_init_scale * pointer_scale_factor,
                cross_init_scale * pointer_scale_factor, cross_init_scale * pointer_scale_factor);
            if (spherePointer.transform.localScale.x < cross_limit)
            {
                pointer_scale_factor = previous_pointer_scale_factor;
                spherePointer.transform.localScale = new Vector3(cross_limit,cross_limit,cross_limit);
            }
        }

        private bool resizing = false;
        public float scaleLimit = 10f;
        private int current_resized_index = -1;
        public Vector3 CopyInitScale;
        public void UpdateCopiesScale()
        {
            if (cells_copy.Count > 0){
                if (!resizing)
                {
                    RaycastHit hitInfo = new RaycastHit();
                    bool hiting = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
                    if (hiting)
                    {
                        GameObject clicked = hitInfo.transform.gameObject;
                        for (int i = 0; i < cells_copy.Count; i++)
                        {
                            if (clicked == cells_copy[i].copy_target)
                            {
                                current_resized_index = i;
                            }
                        }
                    }

                    if (current_resized_index > -1)
                    {
                        CopyInitScale = cells_copy[current_resized_index].copy_target.transform.parent.localScale;
                        resizing = true;
                    }
                }
                else
                {
                    cells_copy[current_resized_index].copy_target.transform.parent.localScale =
                        CopyInitScale * pointer_scale_factor;
                    //sphere1.transform.position = cells_copy[current_resized_index].copy_target.transform.position ;
                    //Vector3 pivot = cells_copy[current_resized_index].copy_target.transform.position  + cells_copy[current_resized_index].copy_target.transform.TransformPoint(GetCurrentGravityCenter(cells_copy[current_resized_index].copy_target));
                    //ScaleAround(cells_copy[current_resized_index].copy_target,pivot,CopyInitScale * pointer_scale_factor);
                    if (cells_copy[current_resized_index].copy_target.transform.parent.localScale.x < (CopyInitScale.x/scaleLimit))
                    {
                        //ScaleAround(cells_copy[current_resized_index].copy_target,pivot,CopyInitScale/4f);
                        cells_copy[current_resized_index].copy_target.transform.parent.localScale = CopyInitScale/scaleLimit;
                    }
                    else if (cells_copy[current_resized_index].copy_target.transform.parent.localScale.x > (CopyInitScale.x*scaleLimit))
                    {
                        //ScaleAround(cells_copy[current_resized_index].copy_target,pivot,CopyInitScale*4f);
                        cells_copy[current_resized_index].copy_target.transform.parent.localScale = CopyInitScale*scaleLimit;
                    }
                }
            }
        }
        
        public static void ScaleAround(GameObject target, Vector3 pivot, Vector3 newScale)
        {
            // pivot
            Vector3 pivotDelta = target.transform.localPosition - pivot; // diff from object pivot to desired pivot/origin
            Vector3 scaleFactor = new Vector3(
                newScale.x / target.transform.localScale.x,
                newScale.y / target.transform.localScale.y,
                newScale.z / target.transform.localScale.z );
            pivotDelta.Scale(scaleFactor);
            target.transform.localPosition = pivot + pivotDelta;
 
            //scale
            target.transform.localScale = newScale;
        }

        public bool is_rotating = false;
        public int current_rotated_index= -1;
        public bool IsRotating()
        {
            return PointerCopyPaste.activeSelf && Input.GetKey(RotateKey);
        }

        public void RotateCurrentCopy()
        {
        }
        
        
        public bool IsScaling()
        {
            return (PointerSegmentation.activeSelf || PointerCopyPaste.activeSelf) && Input.GetKey(ScalingKey);
        }
        public void UpdateOnScroll(float delta)
        {
            if (Input.GetKey(ScalingKey))
            {
                //Update factor 
                float _ZoomRatio = MorphoTools.GetTransformationsManager()._ZoomRatio;
                if (delta != 0)
                {
                    previous_pointer_scale_factor = pointer_scale_factor;
                    
                    if (PointerCopyPaste.activeSelf)
                    {
                  
                            pointer_scale_factor += delta * (_ZoomRatio/4f);
                            UpdateCopiesScale();
                            

                        
                    }
                    else  if (PointerSegmentation.activeSelf)
                    {
                        
                            pointer_scale_factor += delta * _ZoomRatio / 2f;
                            UpdatePointerScale();
                    }
                    
                }

            }

        }

        public int current_copied_index = -1;
        private Vector3 _DragPosition;
        private Quaternion _DownR;
        private Vector3 StartDrag;
        private Vector3 EndDrag;
        public float _Radius = 50f;
        public bool copied = false;
        //Update Pointer Position if active
        
        public Vector3 GetCurrentGravityCenter(GameObject CellCopy)
        {
            Vector3 gc = new Vector3(0f, 0f, 0f);

            MeshFilter filter = CellCopy.GetComponent<MeshFilter>();

            if (filter != null)
            {
                Vector3[] objectVerts = filter.mesh.vertices;

                for (int i = 0; i < objectVerts.Length; i++)
                for (int j = 0; j < 3; j++)
                    gc[j] += objectVerts[i][j];

                for (int j = 0; j < 3; j++)
                    gc[j] /= objectVerts.Length;
            }
            else
            {
                //Multiples subb Ojbect with different filter are associaed

                for (int c = 0; c < CellCopy.transform.childCount; c++)
                {
                    filter = CellCopy.transform.GetChild(c).GetComponent<MeshFilter>();
                    int nbG = 0;

                    if (filter != null)
                    {
                        Vector3[] objectVerts = filter.mesh.vertices;

                        for (int i = 0; i < objectVerts.Length; i++)
                        for (int j = 0; j < 3; j++)
                            gc[j] += objectVerts[i][j];

                        nbG += objectVerts.Length;
                    }

                    for (int j = 0; j < 3; j++)
                        gc[j] /= nbG;
                }
            }

            gc += CellCopy.transform.localPosition;

            return gc;
        }

        public bool measured;
        public void Update()
        {
            if (PointerCopyPaste.activeSelf)
            {
                syncPointerCopyPaste();
                if (!Input.GetMouseButton(0) && !Input.GetMouseButton(1))
                {
                    if (Input.GetKey(CopyCellKey))
                    {
                        if (!copied)
                        {
                            StartCopy();
                            copied = true;
                        }
                    }
                    else
                    {
                        copied = false;
                    }

                    if (Input.GetKey(RotateKey))
                    {
                        if (cells_copy.Count > 0){
                            if (!is_rotating)
                            {
                                RaycastHit hitInfo = new RaycastHit();
                                bool hiting = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
                                if (hiting)
                                {
                                    GameObject clicked = hitInfo.transform.gameObject;
                                    for (int i = 0; i < cells_copy.Count; i++)
                                    {
                                        if (clicked == cells_copy[i].copy_target)
                                        {
                                            current_rotated_index = i;
                                        }
                                    }
                                }

                                if (current_rotated_index > -1)
                                {
                                    prevMousePosition = Input.mousePosition;
                                    is_rotating = true;
                                    _DragPosition = Input.mousePosition;

                                    //StartDrag = TransformationsManager.MapToSphere(
                                    //    cells_copy[current_rotated_index].copy_target.transform.position,
                                    //    _DragPosition, _Radius);
                                    /*cells_copy[current_rotated_index].copy_target.transform.parent.transform.position =
                                        cells_copy[current_rotated_index].copy_target.transform
                                            .TransformPoint(
                                                GetCurrentGravityCenter(cells_copy[current_rotated_index].copy_target));*/ 
                                    //Vector3 pivot = cells_copy[current_rotated_index].copy_target.transform.TransformPoint(GetCurrentGravityCenter(cells_copy[current_rotated_index].copy_target));
                                    StartDrag = TransformationsManager.MapToSphere(cells_copy[current_rotated_index].copy_target.transform.parent.position, _DragPosition, _Radius);
                                    _DownR = cells_copy[current_rotated_index].copy_target.transform.parent
                                        .rotation;
                                }
                            }
                            else
                            {
                                
                                _DragPosition += Input.mousePosition - prevMousePosition;
                                //Vector3 shifted_pivot = cells_copy[current_rotated_index].copy_target.transform.TransformPoint(GetCurrentGravityCenter(cells_copy[current_rotated_index].copy_target));
                                //sphere2.transform.position = shifted_pivot;
                                //EndDrag = TransformationsManager.MapToSphere(cells_copy[current_rotated_index].copy_target.transform.position, _DragPosition, _Radius);
                                EndDrag = TransformationsManager.MapToSphere(cells_copy[current_rotated_index].copy_target.transform.parent.position, _DragPosition, _Radius);

                                Vector3 axis = Vector3.Cross(StartDrag, EndDrag).normalized;
                                float angle = Vector3.Angle(StartDrag, EndDrag);

                                Quaternion dragR = Quaternion.AngleAxis(angle, axis);

                                Quaternion newRotation = dragR * _DownR;
                                cells_copy[current_rotated_index].copy_target.transform.parent.rotation = newRotation;
                                //cells_copy[current_rotated_index].copy_target.transform.parent.Rotate(axis,-angle);

                                prevMousePosition = Input.mousePosition;
                            }
                        }
                    }
                    else if (Input.GetKey(MoveCopyKey)) //MOVE THE COPIED CELL (Only one for now ..)
                    {
                        current_rotated_index = -1;
                        is_rotating = false;
                        if (cells_copy.Count > 0){
                            if (!moving)
                            {
                                RaycastHit hitInfo = new RaycastHit();
                                bool hiting = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
                                if (hiting)
                                {
                                    GameObject clicked = hitInfo.transform.gameObject;
                                    for (int i = 0; i < cells_copy.Count; i++)
                                    {
                                        if (clicked == cells_copy[i].copy_target)
                                        {
                                            current_copied_index = i;
                                        }
                                    }
                                }

                                if (current_copied_index > -1)
                                {
                                    prevMousePosition = Input.mousePosition;
                                    moving = true;
                                    spherePositionCopyPaste = cells_copy[current_copied_index].copy_target.transform
                                        .parent.position;
                                }
                            }
                            else
                            {
                                spherePositionCopyPaste.x += (Input.mousePosition.x - prevMousePosition.x) * speedmoving;
                                spherePositionCopyPaste.y += (Input.mousePosition.y - prevMousePosition.y) * speedmoving;
                                cells_copy[current_copied_index].copy_target.transform.parent.position = spherePositionCopyPaste;
                                prevMousePosition = Input.mousePosition;
                            }
                        }
                    }
                    else
                    {
                        current_copied_index = -1;
                        current_rotated_index = -1;
                        moving = false;
                        is_rotating = false;
                        if (Input.GetKey(DeleteKey)) //DELETE COPY
                        { //Delete some spheres
                            RaycastHit hitInfo = new RaycastHit();
                            bool hiting = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
                            if (hiting)
                            {
                                GameObject clicked = hitInfo.transform.gameObject;
                                for (int i = 0; i < cells_copy.Count; i++)
                                {
                                    if (clicked == cells_copy[i].copy_target)
                                    {
                                        cells_copy.RemoveAt(i);
                                        DestroyImmediate(hitInfo.transform.gameObject.transform.parent.gameObject);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    current_copied_index = -1;
                    current_rotated_index = -1;
                    moving = false;
                    is_rotating = false;
                }
            }
            else if (PointerSegmentation.activeSelf)
            {
                syncPointer();
                if (!Input.GetMouseButton(0) && !Input.GetMouseButton(1))
                {
                    
                    if (Input.GetKey(KeyCode.T)) //MOVE THE POINTER
                    {
                        if (!moving)
                        {
                            
                            prevMousePosition = Input.mousePosition;
                            moving = true;
                            spherePosition = spherePointer.transform.position;
                        }
                        else
                        {
                            spherePosition.x += (Input.mousePosition.x - prevMousePosition.x) * speedmoving;
                            spherePosition.y += (Input.mousePosition.y - prevMousePosition.y) * speedmoving;
                            spherePointer.transform.position = spherePosition;
                            /*if (MorphoTools.GetRawImages().RawImagesAreVisible) //We project the point to the one the plan
                            {
                                float distMin = float.MaxValue;
                                //Debug.Log("Look for the closest plan for "+ spherePosition.ToString());
                                if (MorphoTools.GetRawImages().XPlanIsCut)
                                {
                                    Plane pl = getPlane(MorphoTools.GetRawImages().XMeshesCutNorm);
                                    //MorphoDebug.Log("PlX=" + pl.ToString());
                                    float d = pl.GetDistanceToPoint(spherePointer.transform.position);
                                    //MorphoDebug.Log("X -->"+d);
                                    if (distMin > d)
                                    {
                                        spherePosition = pl.ClosestPointOnPlane(spherePointer.transform.position);
                                        distMin = d;
                                    }
                                }
                                if (MorphoTools.GetRawImages().YPlanIsCut)
                                {
                                    Plane pl = getPlane(MorphoTools.GetRawImages().YMeshesCutNorm);
                                    float d = pl.GetDistanceToPoint(spherePosition);
                                    //MorphoDebug.Log("Y -->" + d);
                                    if (distMin > d)
                                    {
                                        spherePosition = pl.ClosestPointOnPlane(spherePointer.transform.position);
                                        distMin = d;
                                    }
                                }
                                if (MorphoTools.GetRawImages().ZPlanIsCut)
                                {
                                    Plane pl = getPlane(MorphoTools.GetRawImages().ZMeshesCutNorm);
                                    float d = pl.GetDistanceToPoint(spherePosition);
                                    //MorphoDebug.Log("Z -->" + d);
                                    if (distMin > d)
                                    {
                                        spherePosition = pl.ClosestPointOnPlane(spherePointer.transform.position);
                                        distMin = d;
                                    }
                                }

                                //MorphoDebug.Log(" found --> " + spherePointer.transform.position.ToString());
                            }*/
                            prevMousePosition = Input.mousePosition;
                        }
                    }
                    else
                        moving = false;
                }
                else
                    moving = false;

                if (Input.GetKeyDown(KeyCode.A)) //ADD A SPHERE
                {
                    addNewCoordinate();
                }

                if (Input.GetKeyDown(KeyCode.C)) //CLEAR LAST COORDINATE
                { //Cancel Last Addition of coordinate ....
                    if (Coordinates.Count > 0)
                    {
                        GameObject cdho = Coordinates.Last();
                        //MorphoDebug.Log("Remove " + cdho.name);
                        Coordinates.Remove(cdho);
                        DestroyImmediate(cdho);
                    }
                }

                if (Input.GetKey(DeleteKey)) //DELETE SPHERE
                { //Delete some spheres
                    RaycastHit hitInfo = new RaycastHit();
                    bool hiting = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
                    if (hiting)
                    {
                        string clickedname = hitInfo.transform.gameObject.name;
                        //MorphoDebug.Log("Hit " + clickedname);
                        if (clickedname.StartsWith("CD_"))
                        { //Hit a sphere
                            Coordinates.Remove(hitInfo.transform.gameObject);
                            DestroyImmediate(hitInfo.transform.gameObject);
                        }
                    }
                }
            }
        }

        public void Message(string mssg)
        {
            MorphoDebug.Log(mssg,1);
        }

        //CURATION
        public void CurateInfo(string info_name, string object_name, string value)
        {
            MorphoDebug.Log(info_name + "-> " + object_name + " -> " + value);
            string date = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            StartCoroutine(SendCuration("create_curation", info_name, object_name, value, date));
        }

        public void DeleteCuration(string info_name, string object_name, string value, string date)
        {
            string format_date = date;
            DateTime dt;
            if(DateTime.TryParse(date, out dt))
            {
                format_date = dt.ToString("yyyy-MM-dd HH:mm:ss");
            }

            StartCoroutine(SendCuration("delete_curation", info_name, object_name, value, format_date));
        }

        public void DeleteCurationLineage(string info_name, string object_name, string value, string date)
        {
            string format_date = date;
            DateTime dt;
            if (DateTime.TryParse(date, out dt))
            {
                format_date = dt.ToString("yyyy-MM-dd HH:mm:ss");
            }
            StartCoroutine(SendCuration("delete_curation_value", info_name, object_name, value, format_date));
        }

        public void DeleteInformation(string info_name)
        {
            StartCoroutine(SendDeleteInformation("delete_info_unity", info_name));
        }

        public void DeleteSelectionInInformation(string info_name, int selection_number)
        {
            StartCoroutine(SendDeleteSelection("delete_selection", info_name, selection_number));
        }

        public IEnumerator SendCuration(string action, string info_name, string object_name, string value, string date)
        {
            if (!isAvaible)
                Message("Wait a command is already running ");
            else
            {
                isAvaible = false;

                if (action == "create_curation")
                    Message("Send Curation for " + object_name);
                else
                    Message("Delete Curation for " + object_name + " the " + date);
                WWWForm form = new WWWForm();
                form.AddField("action", action);
                form.AddField("time", MorphoTools.GetDataset().CurrentTime);
                form.AddField("objects", object_name.Replace(" ", "%20"));
                form.AddField("info", info_name.Replace(" ", "%20"));
                form.AddField("value", value.Replace(" ", "%20"));
                form.AddField("date", date.Replace(" ", "%20"));
                UnityWebRequest www = UnityWebRequests.Post(plot_adress + port_receive, "/ send.html", form);

                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                    MorphoDebug.Log("Error : " + www.error);
                www.Dispose();
                Message("");
                
                isAvaible = true;
            }
        }

        public IEnumerator SendDeleteInformation(string action, string info_name)
        {
            if (!isAvaible)
                Message("Wait a command is already running ");
            else
            {
                isAvaible = false;

                if (action == "delete_info_unity")
                    Message("Delete information " + info_name);
                WWWForm form = new WWWForm();
                form.AddField("action", action);
                form.AddField("time", MorphoTools.GetDataset().CurrentTime);
                form.AddField("objects", "");
                form.AddField("info", info_name.Replace(" ", "%20"));
                form.AddField("value", "");
                form.AddField("date", "");
                UnityWebRequest www = UnityWebRequests.Post(plot_adress + port_receive, "/ send.html", form);

                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                {
                    MorphoDebug.Log("Error : " + www.error);
                }
                    
                www.Dispose();
                Message("");
                
                isAvaible = true;

                //MorphoTools.GetDataset().Infos.Refresh();
                MorphoTools.GetDataset().Infos.reOrganize();
            }
        }

        public IEnumerator SendDeleteSelection(string action, string info_name, int selection_number)
        {
            if (!isAvaible)
                Message("Wait a command is already running");
            else
            {
                isAvaible = false;

                if (action == "delete_selection")
                    Message("Delete selection " + selection_number + " for information " + info_name);
                WWWForm form = new WWWForm();
                form.AddField("action", action);
                form.AddField("time", MorphoTools.GetDataset().CurrentTime);
                form.AddField("objects", "");
                form.AddField("info", info_name.Replace(" ", "%20"));
                form.AddField("selection", selection_number.ToString());
                form.AddField("date", "");
                UnityWebRequest www = UnityWebRequests.Post(plot_adress + port_receive, "/ send.html", form);

                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                    MorphoDebug.Log("Error : " + www.error);
                www.Dispose();
                Message("");
                
                isAvaible = true;
            }
        }

        public void ReloadInfos()
        {
            StartCoroutine(SendReloadInfoRequest());
        }

        public IEnumerator SendReloadInfoRequest()
        {
            if (!isAvaible)
                Message("Wait a command is already running ");
            else
            {
                isAvaible = false;

                Message("Reload informations from files");
                WWWForm form = new WWWForm();
                form.AddField("action", "reload_infos");
                form.AddField("time", MorphoTools.GetDataset().CurrentTime);
                form.AddField("objects", ";");
                UnityWebRequest www = UnityWebRequests.Post(plot_adress + port_send, "/ send.html", form);

                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                    MorphoDebug.Log("Error : " + www.error);
                www.Dispose();
                Message("");
                
                isAvaible = true;
            }
        }

        public void CreateInfo(string morpho, string name, string datatype)
        {
            StartCoroutine(CreateInfoRequest(morpho, name, datatype));
        }

        public IEnumerator CreateInfoRequest(string morpho, string name, string datatype, string file = "", bool reload = false)
        {
            if (!isAvaible)
                Message("Wait a command is already running ");
            else
            {
                isAvaible = false;

                Message("Create informations");
                WWWForm form = new WWWForm();
                form.AddField("action", "create_info_unity");
                form.AddField("time", MorphoTools.GetDataset().CurrentTime);
                form.AddField("objects", ";");
                form.AddField("name", name);
                form.AddField("datatype", datatype);
                form.AddField("infos", morpho);
                UnityWebRequest www = UnityWebRequests.Post(plot_adress + port_receive, "/ send.html", form);

                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                    MorphoDebug.Log("Error : " + www.error);
                www.Dispose();
                Message("");
                
                isAvaible = true;
            }
        }

        public void AddPropertyFromFile(string filepath)
        {

            string filename = filepath.Split(Path.DirectorySeparatorChar)[filepath.Split(Path.DirectorySeparatorChar).Length - 1].Split('.')[0];

            List<string> textToLoad = new List<string>();
            StringBuilder infocontent = new StringBuilder();
            int startIndex = 0;
            using (StreamReader sr = new StreamReader(filepath))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line[0] == '#')
                        startIndex++;
                    if(startIndex<10)//arbitrary limit of 10 lines of comment
                    textToLoad.Add(line);
                    infocontent.AppendLine(line);
                }
            }

            string[] infotypes = textToLoad[startIndex].Split(':');
            if (infotypes.Count() != 2)
                MorphoDebug.Log("ERROR UPLOAD uncorrect format type specification " + textToLoad[startIndex]);
            else
            {
                string datatype = infotypes[1];
                StartCoroutine(CreateInfoRequest(infocontent.ToString(), filename, datatype, filepath));
            }

        }

        public IEnumerator SendRestartRequest()
        {
            if (!isAvaible) Message("Wait a command is already running");
            else
            {
                MorphoDebug.Log("SEND RESTART REQUEST");
                isAvaible = false;
            
                Message("Create informations");
                WWWForm form = new WWWForm();
                form.AddField("action", "leave");
                form.AddField("time", MorphoTools.GetDataset().CurrentTime);
                form.AddField("objects", ";");
                UnityWebRequest www = UnityWebRequests.Post(plot_adress + port_receive, "/ send.html", form);

                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError) MorphoDebug.Log("Error : " + www.error);
                www.Dispose();
                Message("");
            
                isAvaible = true;
            }
        }


        // Plugin LPY
        public IEnumerator SendCreatePlugLPY(int t, string ID)
        {
            while (!isAvaible)
                yield return null;
            if (!isAvaible)
            { Message("Wait a command is already running"); MorphoDebug.LogError("not avaible for create"); }
            else
            {
                isAvaible = false;
                
                WWWForm form = new WWWForm();
                form.AddField("action", "create_plugin");
                form.AddField("time", MorphoTools.GetDataset().CurrentTime);
                form.AddField("objects", ";");
                form.AddField("step", "" + t);
                form.AddField("id", ID);
                UnityWebRequest www = UnityWebRequests.Post(plot_adress + port_receive, "/ send.html", form);

                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                    MorphoDebug.Log("Error : " + www.error);
                www.Dispose();
                Message("");
                
                isAvaible = true;
            }
        }

        public IEnumerator SendDeletePlugLPY(int t, string ID)
        {
            while (!isAvaible)
                yield return null;
            if (!isAvaible)
                Message("Wait a command is already running");
            else
            {
                GameObject btnall = null;
                foreach (GameObject pgo in ParentsLPY.Values)
                {
                    if (pgo.name == "Element Parameters")
                    {
                        for (int ic = 0; ic < pgo.transform.childCount; ic++)
                        {
                            GameObject co = pgo.transform.GetChild(ic).gameObject;
                            if (co.name == "Reload all elements")
                            {
                                btnall = co;
                            }
                        }
                    }
                }
                foreach (GameObject pgo in ParentsLPY.Values)
                {
                    if (pgo.name == "Element Parameters")
                    {
                        for (int ic = 0; ic < pgo.transform.childCount; ic++)
                        {
                            GameObject co = pgo.transform.GetChild(ic).gameObject;
                            if (co.name == "SV")
                            {
                                Transform content = co.transform.Find("ScrollView").Find("Viewport").Find("Content");
                                for (int k = 0; k < content.childCount; k++)
                                {
                                    GameObject c = content.GetChild(k).gameObject;
                                    if (c.name == "Reload element " + t + "," + ID)
                                    {
                                        c.transform.SetParent(null);
                                        DestroyImmediate(c, true);
                                    }
                                }
                                if (content.childCount == 0)
                                {
                                    co.SetActive(false);
                                    co.transform.SetParent(null);
                                    DestroyImmediate(co, true);
                                    scrollView.SetActive(false);
                                    if (btnall != null)
                                    {
                                        btnall.transform.SetParent(null);
                                        DestroyImmediate(btnall, true);
                                    }
                                }
                            }
                        }
                    }
                }
                reorganizeBttnsLPY();
                isAvaible = false;
                
                WWWForm form = new WWWForm();
                form.AddField("action", "delete_plugin");
                form.AddField("time", MorphoTools.GetDataset().CurrentTime);
                form.AddField("objects", ";");
                form.AddField("plug", "Reload element " + t + "," + ID);
                UnityWebRequest www = UnityWebRequests.Post(plot_adress + port_receive, "/ send.html", form);

                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                    MorphoDebug.Log("Error : " + www.error);
                www.Dispose();
                Message("");
                
                isAvaible = true;
            }
        }

        public IEnumerator SendUploadLPY(string comment)
        {
            if (!isAvaible)
                Message("Wait a command is already running");
            else
            {
                isAvaible = false;
                
                WWWForm form = new WWWForm();
                form.AddField("action", "upload");
                form.AddField("time", MorphoTools.GetDataset().CurrentTime);
                form.AddField("objects", ";");
                form.AddField("comment", comment);
                UnityWebRequest www = UnityWebRequests.Post(plot_adress + port_receive, "/ send.html", form);

                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
                    MorphoDebug.Log("Error : " + www.error);
                www.Dispose();
                Message("");
                
                isAvaible = true;
            }
        }

        public int GenerateBoundariesAtT(string direction,int time)
        {
            DataSet dataset = MorphoTools.GetDataset();
            int boundary = -1;
        
            if (dataset.CellsByTimePoint.ContainsKey(time)){
                List<Cell> cells_at_time = new List<Cell>(dataset.CellsByTimePoint[time]);
                cells_at_time.Sort(SortById);
                if (direction != "forward")
                {
                    cells_at_time.Reverse();
                }
                for (int i = 0; i < cells_at_time.Count; i++)
                {
                    Cell c = cells_at_time[i];
                    int cell_id;
                    if (int.TryParse(c.ID, out cell_id))
                    {
                        if ((direction == "forward" && cell_id > boundary) ||
                            (direction != "forward" && cell_id < boundary))
                        {
                            boundary = cell_id;
                        }
                    }
                }
            }
            return boundary;
        }

        static int SortById(Cell p1, Cell p2)
        {
            return int.Parse(p1.ID).CompareTo(int.Parse(p2.ID));
        }

        
        public Cell GetNextIdError(string direction)
        {
            DataSet dataset = MorphoTools.GetDataset();
            int next_id = -1;
            List<Cell> cells_at_time = new List<Cell>(dataset.CellsByTimePoint[CurrentErrorTime]);
            cells_at_time.Sort(SortById);
            Cell found = null;
            for (int i = 0; i < cells_at_time.Count; i++)
            {
                Cell c = cells_at_time[i];
                if (c.show){
                    int cell_id = -1;
                    if (int.TryParse(c.ID, out cell_id))
                    {
                        if ((direction == "forward" && cell_id > CurrentErrorId) ||
                            (direction != "forward" && cell_id < CurrentErrorId))
                        {
                            if ( c != null && ((c.Mothers == null || c.Mothers.Count == 0) && CurrentErrorTime != dataset.MinTime) ||
                                 ((c.Daughters == null || c.Daughters.Count == 0) && CurrentErrorTime != dataset.MaxTime) ||
                                 (c.Daughters != null && c.Daughters.Count > 1)) //if we find an error (birth != tmin, death != tmax, or division)
                            {
                                if (direction != "forward" || found == null){
                                    found= c;
                                }
                            }
                        }
                    }
                }
            }

            return found;
        }

        /// <summary>
        /// function that goes to the next potential error in curation : division, cell death, cell birth
        /// </summary>
        public void GoToNextDetectedErrorInCuration(string direction)
        {
            DataSet dataset = MorphoTools.GetDataset();

            if (CurrentErrorTime == -1)
            {
                CurrentErrorTime = dataset.CurrentTime;
            }
            if (CurrentErrorTime < dataset.MinTime) // Safety to start at correct time point
            {
                CurrentErrorTime = dataset.MinTime;
            }

            if (CurrentErrorTime > dataset.MaxTime) // Safety to start at correct time point
            {
                CurrentErrorTime = dataset.MaxTime;
            }

            if (CurrentErrorId == -1)
            {
                if (direction == "forward")
                {
                    CurrentErrorId = 0;
                }
                else
                {
                    CurrentErrorId = int.MaxValue;
                }
            }
            
                int boundary = GenerateBoundariesAtT(direction, CurrentErrorTime);
                if ((direction == "forward" && CurrentErrorId >= boundary))           // If we didn't find a possible next cell, we need to go to next (or previous) time point
                {
                    if (CurrentErrorTime < dataset.MaxTime)
                    {
                        CurrentErrorTime +=  1; // Go to next time
                        CurrentErrorId = 0;
                        GoToNextDetectedErrorInCuration(direction);
                    }
                    
                    return;

                }
                if ((direction == "backward" && CurrentErrorId <= boundary))
                {
                    if (CurrentErrorTime > dataset.MinTime)
                    {
                        CurrentErrorTime -=  1; // Go to next time
                        CurrentErrorId = int.MaxValue;
                        GoToNextDetectedErrorInCuration(direction);
                    }

                    return;
                }
                Cell next_cell_error = GetNextIdError(direction);

            if (next_cell_error != null)
            {
                CurrentErrorId = int.Parse(next_cell_error.ID);
                PickedManager pm = MorphoTools.GetPickedManager();
                pm.SelectCell(next_cell_error, true); 
                MorphoTools.GetInformations().updateSelectionOnSelectedCells();
            }
            else
            {
                if (direction == "forward")           // If we didn't find a possible next cell, we need to go to next (or previous) time point
                {
                    if (CurrentErrorTime < dataset.MaxTime)
                    {
                        CurrentErrorTime +=  1; // Go to next time
                        CurrentErrorId = 0;
                        GoToNextDetectedErrorInCuration(direction);
                    }
                    
                    return;

                }
                else
                {
                    if (CurrentErrorTime > dataset.MinTime)
                    {
                        CurrentErrorTime -=  1; // Go to next time
                        CurrentErrorId = int.MaxValue;
                        GoToNextDetectedErrorInCuration(direction);
                    }

                    return;
                }
            }
        }

        /// <summary>
        /// function to manage file picker usage for plugins (wih inputfield)
        /// </summary>
        /// <param name="f">inputfield linked</param>
        public void FilePickerForPlugin(InputField f)
        {
            if (!_PickerInUse)
            {
                _PickerInUse = true;
                FileBrowser.SetFilters(true);
                StartCoroutine(WaitForFilePicker(f));
            }
        }

        /// <summary>
        /// Coroutine to select a file
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        public IEnumerator WaitForFilePicker(InputField f)
        {
            yield return FileBrowser.WaitForLoadDialog(FileBrowser.PickMode.Files, false, null, null, "Load file", "Select");

            if (FileBrowser.Success)
            {

                if (File.Exists(FileBrowser.Result[0]))//if simple file
                {
                    f.text = FileBrowser.Result[0];
                }
                else
                {
                    MorphoDebug.LogWarning("WARNING : incorrect path input : " + (int)File.GetAttributes(FileBrowser.Result[0]) + ", " + FileBrowser.Result[0]);
                }
            }
            _PickerInUse = false;
        }

        /// <summary>
        /// function to manage file saver usage for plugins (wih inputfield)
        /// </summary>
        /// <param name="f">inputfield linked</param>
        public void FileSaverForPlugin(InputField f)
        {
            if (!_PickerInUse)
            {
                _PickerInUse = true;
                FileBrowser.SetFilters(true);
                StartCoroutine(WaitForFileSaver(f));
            }
        }

        /// <summary>
        /// Coroutine to select a file (filesaver mode)
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        public IEnumerator WaitForFileSaver(InputField f)
        {
            yield return FileBrowser.WaitForSaveDialog(FileBrowser.PickMode.Files, false, null, null, "Load file", "Select");

            if (FileBrowser.Success)
            {
                f.text = FileBrowser.Result[0];
            }
            _PickerInUse = false;
        }


        public void SetMeshEditorStatus(bool status)
        {
            RuntimeMeshEditorInstance.gameObject.SetActive(status);
            InterfaceManager.instance.MorphingSliderPanel.SetActive(status);

            if (MorphoTools.GetPickedManager().clickedCells.Count == 0)
            {
                MorphoDebug.Log("Please select one object to morph", 1);
            }
        }

        public void ToggleMeshEditorStatus(bool active)
        {
            RuntimeMeshEditorInstance.gameObject.SetActive(active);
            InterfaceManager.instance.MorphingSliderPanel.SetActive(active);
            if (active)
            {
                if (MorphoTools.GetPickedManager().clickedCells.Count == 0)
                {
                    MorphoDebug.Log("Please select one object to morph", 1);
                }
            }

        }


    }
}