using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ExamplePluginButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public ExamplePluginOptions Options;

    public void OnPointerEnter(PointerEventData eventData)
    {
        Options.SetHelpWindowVisibility(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Options.SetHelpWindowVisibility(false);
    }

    public void SetIcons(Texture2D tex)
    {
        try
        {
            Image img;
            if (TryGetComponent<Image>(out img))
            {
                img.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
            }
            Options.SetImages(tex);
        }
        catch (Exception e)
        {
            Debug.LogError("could not set image to button for plugin " + name + ". -> " + e);
        }
    }
}
