using MorphoNet;
using UnityEngine;
using UnityEngine.UI;


//this behavior can allow 

public class RuntimeMeshEditor : MonoBehaviour
{
    /// <summary>
    /// Represent a state of a mesh
    /// </summary>
    public class MeshState
    {
        public MeshCollider MeshCollider = null;
        public Mesh Mesh;
        public Vector3[] Vertices;

        public MeshState(Mesh mesh, Vector3[] vertices)
        {
            Mesh = mesh;
            Vertices = vertices;
        }

        public MeshState(Mesh mesh, Vector3[] vertices, MeshCollider meshCollider)
        {
            Mesh = mesh;
            Vertices = vertices;
            MeshCollider = meshCollider;
        }

        /// <summary>
        /// Restore the state of a mesh (does not check if mesh exists)
        /// </summary>
        public void Restore()
        {
            Mesh.vertices = Vertices;
            Mesh.RecalculateNormals();
            if (MeshCollider != null)
            {
                MeshCollider.sharedMesh = null;
                MeshCollider.sharedMesh = Mesh;
            }
        }
    }

    /// <summary>
    /// Represent a chain of MeshStates
    /// </summary>
    public class MeshStateChainNode
    {
        public MeshState state;
        public MeshStateChainNode previous = null;
        public MeshStateChainNode next = null;

        public MeshStateChainNode(MeshState state)
        {
            this.state = state;
        }
    }

    #region params

    public Slider rangeSlider;
    public AbstractManipulator[] Manipulators;

    private int _CurrentManipulatorIndex = 0;
    public int CurrentManipulatorIndex
    {
        get { return _CurrentManipulatorIndex; }
        set { SetCurrentManipulatorIndex(value); }
    }

    private MeshStateChainNode _CurrentMeshState = null;
    private uint _NumberOfStates = 0;

    #endregion

    #region singleton

    public static RuntimeMeshEditor instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    #endregion

    private void Start()
    {
        foreach (AbstractManipulator manipulator in Manipulators)
        {
            manipulator.enabled = false;
        }
        Manipulators[_CurrentManipulatorIndex].enabled = true;
        SetRadiusFromSlider(rangeSlider.value);
    }

    private void Update()
    {
        // Detect if user is mooving, rotating or sclaing the mesh
        // Something with InteractionMediator
        //bool update = MorphoTools.GetDataset().update;
        //bool update = false;
        ////Debug.Log("Update : " + update);
        //if (update && Manipulators[_CurrentManipulatorIndex].enabled)
        //{
        //    Manipulators[_CurrentManipulatorIndex].enabled = false;
        //}
        //else if (!update && !Manipulators[_CurrentManipulatorIndex].enabled)
        //{
        //    Manipulators[_CurrentManipulatorIndex].enabled = true;
        //}

        // Detect when we send a command, clear history and disable editing
        if (!SetsManager.instance.directPlot.isAvaible && _CurrentMeshState != null)
        {
            _CurrentMeshState = null;
            _NumberOfStates = 0;
        }
    }

    public void Undo()
    {
        MeshStateChainNode previous = _CurrentMeshState.previous;
        if (previous != null)
        {
            _CurrentMeshState = previous;
            _CurrentMeshState.state.Restore();
            _NumberOfStates--;
        }
    }

    public void Redo()
    {
        MeshStateChainNode next = _CurrentMeshState.next;
        if (next != null)
        {
            _CurrentMeshState = next;
            _CurrentMeshState.state.Restore();
            _NumberOfStates++;
        }
    }

    public void SaveState(MeshState state)
    {
        _NumberOfStates++; // TODO erase oldest state if _NumberOfStates is too big
        MeshStateChainNode newState = new MeshStateChainNode(state);
        if (_CurrentMeshState == null)
        {
            _CurrentMeshState = newState;
        }
        else
        {
            newState.previous = _CurrentMeshState;
            _CurrentMeshState.next = newState; // erase next states (garbage collected)
            _CurrentMeshState = _CurrentMeshState.next;
        }
    }

    public void ReplaceState(MeshState state)
    {
        if (_CurrentMeshState == null)
        {
            _CurrentMeshState = new MeshStateChainNode(state);
        }
        else
        {
            _CurrentMeshState.state = state;
        }
    }



    public void SetCurrentManipulatorIndex(int index)
    {
        Manipulators[_CurrentManipulatorIndex].enabled = false;
        _CurrentManipulatorIndex = index;
        Manipulators[_CurrentManipulatorIndex].enabled = true;
    }

    public void SetRadiusFromSlider(float size)
    {
        Manipulators.ForEach(man => man.SetRadiusFromSlider(size));
        //Manipulators[_CurrentManipulatorIndex].SetRadiusFromSlider(size);
    }
}
