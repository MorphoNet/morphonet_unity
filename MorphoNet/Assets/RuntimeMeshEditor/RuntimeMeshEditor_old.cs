using UnityEngine;
using Cursor = UnityEngine.Cursor;


//this behavior can allow 

public class RuntimeMeshEditor_old : MonoBehaviour
{
    public enum ToolMode
    {
        Kelvinlets_Dual,
        Smooth
    }
    //public enum MouseMode
    //{
    //    FrontPlane,
    //    PushPull
    //}

    //public enum SmoothMode
    //{
    //    Inflate,
    //    Deflate
    //}


    //private const string OUTPUT_PATH = @"Assets/sphere2.obj";

    #region params
    public bool Activated = false;

    private bool _Selected = false;
    private bool _TranslatePressed = false;

    private GameObject _CurrentGameObject;
    private Mesh _CurrentMesh;
    //private List<List<int>> _CurrentMeshOneRing;
    //private Vector3 _CurrentVertex;
    //private int _CurrentVertexIndex;

    private Vector3[] DisplacedVertices;
    private Vector3[] OriginalVertices;

    private Vector3 _startPos_meshCoordinate;
    private Vector3 _endPos_meshCoordinate;

    private Vector3 _PreviousMousePos;
    //private Vector3 _StartMousePos;

    private GameObject _TranslatingGizmo = null;

    public float TransformRadius;

    public float ElasticShearModulus = 1.0f;
    public float PoissonRatio = 0.4f;
    public float PressureFactor = 1f;

    public GameObject GizmoStart;
    public GameObject GizmoEnd;
    public GameObject GizmoArrow;

    public GameObject RadiusSphere;

    public GameObject Line;

    public ToolMode _ToolMode = ToolMode.Smooth;
    //public SmoothMode _SmoothMode = SmoothMode.Deflate;

    //public MouseMode _PlaneMode = MouseMode.FrontPlane;

    public BrushManager BrushManager;
    public float MinValue = 0.002f;

    #endregion

    public static RuntimeMeshEditor_old instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    private void Start()
    {
        //_CurrentMeshOneRing = new List<List<int>>();
        if (BrushManager == null)
        {
            bool got = TryGetComponent<BrushManager>(out BrushManager);
            if (!got)
            {
                BrushManager = gameObject.AddComponent<BrushManager>();
            }
        }
        OnValueChanged();
    }

    // Pre-computes values and update visuals only when values are changed
    public void OnValueChanged()
    {
        BrushManager.InitComputation(ElasticShearModulus, PoissonRatio, PressureFactor, TransformRadius);
        //radiusSphere.transform.localScale = new Vector3(transformRadius * 2.0f, transformRadius * 2.0f, 0.1f);
        if (_CurrentGameObject != null)
        {
            if (_ToolMode == ToolMode.Kelvinlets_Dual)
            {
                RadiusSphere.transform.localScale = Vector3.Scale(_CurrentGameObject.transform.lossyScale, BrushManager.ComputeRange(MinValue, Input.mousePosition - _PreviousMousePos));
                UpdateTranslatingVertices();
            }
            else if (_ToolMode == ToolMode.Smooth)
            {
                RadiusSphere.transform.localScale = _CurrentGameObject.transform.lossyScale * 2.0f * TransformRadius;
            }
        }
    }

    public void SetSphereSize(float size)
    {
        TransformRadius = size;
        OnValueChanged();
    }

    public void UpdatePositionGizmo()
    {
        Vector3 worldStartPos = _CurrentGameObject.transform.TransformPoint(_startPos_meshCoordinate);
        Vector3 worldEndPos = _CurrentGameObject.transform.TransformPoint(_endPos_meshCoordinate);
        GizmoStart.transform.position = worldStartPos;
        GizmoEnd.transform.position = worldEndPos;
        Line.transform.LookAt(worldEndPos);
        Line.transform.localScale = new Vector3(Line.transform.localScale.x, Line.transform.localScale.y, Vector3.Distance(worldStartPos, worldEndPos));
        GizmoArrow.transform.position = 0.5f * (worldStartPos + worldEndPos);
        GizmoArrow.transform.LookAt(worldEndPos, Camera.main.transform.position - GizmoArrow.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        Vector3 dir = (mousePos - _PreviousMousePos) * 0.02f;
        //if (_PlaneMode == MouseMode.PushPull)
        //{
        //    dir.z = dir.y;
        //    dir.x = 0;
        //    dir.y = 0;
        //}
        if (_CurrentGameObject != null)
        {
            dir = _CurrentGameObject.transform.InverseTransformVector(dir);
            //_endPos += dir;
            //Gizmo.transform.position = _CurrentGameObject.transform.TransformPoint(_startPos);
            //Line.transform.position = _CurrentGameObject.transform.TransformPoint(_startPos);
            //Line.transform.LookAt(_CurrentGameObject.transform.TransformPoint(_endPos));
            //dir = Quaternion.Inverse(_currentGameObject.transform.rotation) * dir;
        }
        else if (_Selected)
        {
            // Something has gone wrong, reset
            OnDisable();
        }

        //if (Input.GetMouseButtonDown(0) && !_Selected) // if left click since activation
        //{
        //    GetEditableMeshPoint();
        //}

        if (Input.GetMouseButtonDown(0) && _TranslatingGizmo == null) // if left click
        {
            GetEditableGizmo();
            if (_TranslatingGizmo == null)
            {
                GetEditableMeshPoint();
            }
        }

        if (Input.GetMouseButton(0) && _TranslatingGizmo != null) // if holding left click (Translate)
        {
            if (!_TranslatePressed)
            {
                _TranslatePressed = true;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.None;
            }
        }

        if (!Input.GetMouseButton(0)) // if releasing left click
        {
            if (_CurrentGameObject != null && _TranslatePressed)
            {
                _CurrentGameObject.AddComponent<MeshCollider>();
            }
            _TranslatingGizmo = null;
            _TranslatePressed = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        //if(Input.GetKey(KeyCode.T) && _Selected)//if press T (translate)
        //{
        //    if (!_TranslatePressed)
        //    {
        //        _TranslatePressed = true;
        //        Cursor.visible = false;
        //        Cursor.lockState = CursorLockMode.None;
        //    }
        //}
        //if (!Input.GetKey(KeyCode.T))
        //{
        //    if (_CurrentGameObject != null && _TranslatePressed) {
        //        _CurrentGameObject.AddComponent<MeshCollider>();
        //    }
        //    _TranslatePressed = false;
        //    Cursor.visible = true;
        //    Cursor.lockState = CursorLockMode.None;
        //}

        if (_Selected && _TranslatePressed)
        {
            //if (_ToolMode == ToolMode.Kelvinlets)
            {
                if (_TranslatingGizmo == GizmoEnd)
                {
                    _endPos_meshCoordinate += dir;
                }
                else
                {
                    _startPos_meshCoordinate += dir;
                }
                //UpdatePositionGizmo();
                UpdateTranslatingVertices();
            }
            //else if (_ToolMode == ToolMode.Smooth)
            //{
            //    float factor = _SmoothMode == SmoothMode.Deflate ? 1.0f : -1.0f;
            //    if (_SmoothMode == SmoothMode.Deflate)
            //    {
            //        SmoothInSphere(0.3f);
            //        SmoothInSphere(-0.31f);
            //    }
            //    else if (_SmoothMode == SmoothMode.Inflate)
            //    {
            //        float d;
            //        for (int i = 0; i < _CurrentMesh.vertexCount; i++)
            //        {
            //            d = Vector3.Distance(_CurrentVertex, DisplacedVertices[i]);
            //            if (d < TransformRadius)//only smooth those in radius
            //            {
            //                Vector3 dis = (DisplacedVertices[i] - _CurrentVertex).normalized * 0.1f;
            //                DisplacedVertices[i] += dis;
            //            }
            //        }
            //    }
            //    ApplySmoothing();
            //}
        }


        bool moving = (Input.GetMouseButton(0) || Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.Space));
#if UNITY_STANDALONE_OSX
        moving = (Input.GetMouseButton(0) || Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.LeftControl));
#endif

        if (_CurrentGameObject != null && (moving || Input.mouseScrollDelta.y != 0f))
        {
            UpdatePositionGizmo();
        }
        //Debug.Log("translate_pressed is : " + translate_pressed);

        //temp exporter
        /*if (Input.GetKeyDown(KeyCode.I))
        {
            Export();
        }*/

        _PreviousMousePos = Input.mousePosition;
    }

    /// <summary>
    /// Select a point in a mesh for editing with a raycast
    /// </summary>
    private void GetEditableMeshPoint()
    {
        //_Selected = false;
        //cast a ray from camera to mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        GameObject previousGameObject = _CurrentGameObject;
        if (Physics.Raycast(ray, out hitInfo, 1000.0f))
        {
            MeshFilter mf;
            if ((mf = hitInfo.collider.gameObject.GetComponent<MeshFilter>()) && (hitInfo.collider.gameObject.name != "Background"))//if the item has a mesh (and a collider):
            {
                _Selected = true;
                _CurrentMesh = mf.mesh;
                //if (mf.gameObject != null && _ToolMode == ToolMode.Smooth && _CurrentGameObject != mf.gameObject)
                //{
                //    CollectOneRingFromCurrentMesh();
                //}

                _CurrentGameObject = mf.gameObject;
                //Destroy(_currentGameObject.GetComponent<MeshCollider>());//we remove the mesh collider so the user can click on the axis (may change to just x,y and z buttons)
                Vector3 closest = _CurrentMesh.vertices[0];
                Vector3 pt = hitInfo.point;
                Vector3 _CurrentVertex = Vector3.zero;
                for (int v_id = 0; v_id < _CurrentMesh.vertices.Length; v_id++)//get the vertex closest to the ray intersection
                {
                    Vector3 p = _CurrentMesh.vertices[v_id];
                    Vector3 tp = _CurrentGameObject.transform.TransformPoint(p);
                    if (Vector3.Distance(pt, tp) < Vector3.Distance(pt, closest))
                    {
                        closest = tp;
                        _CurrentVertex = p;
                        //CurrentVertexIndex = v_id;
                        //_StartMousePos = Input.mousePosition;
                    }
                }
                //_currentVertex = new Vector3(closest.x,closest.y,closest.z);
                DisplacedVertices = _CurrentMesh.vertices;
                OriginalVertices = _CurrentMesh.vertices;
                _startPos_meshCoordinate = _CurrentVertex;
                _endPos_meshCoordinate = _CurrentVertex;
                GizmoStart.transform.position = closest;
                GizmoStart.SetActive(true);
                GizmoEnd.transform.position = closest;
                GizmoEnd.SetActive(true);
                GizmoArrow.SetActive(true);
                _TranslatingGizmo = GizmoEnd;
                UpdatePositionGizmo();
            }
        }
        // Do not reset when cliqued elsewhere
        //if (!_Selected && Gizmo.activeSelf && (hitInfo.collider.gameObject.name != "Background") && (hitInfo.collider.gameObject != Gizmo_x) && (hitInfo.collider.gameObject != Gizmo_y) &&
        //        (hitInfo.collider.gameObject != Gizmo_z) && (hitInfo.collider.gameObject != RadiusSphere))
        //{
        //    Gizmo.SetActive(false);
        //    if (_CurrentGameObject)
        //    {
        //        //_currentGameObject.AddComponent<MeshCollider>();
        //        _CurrentGameObject = null;
        //        _CurrentMesh = null;
        //    }
        //}
        if (previousGameObject)
        {
            previousGameObject.gameObject.AddComponent<MeshCollider>();
        }
        /*        Vector3 e = Gizmo.transform.position;
                e.x += transformRadius;*/
    }

    /// <summary>
    /// Select the Gizmo to translate with a raycast
    /// </summary>
    public void GetEditableGizmo()
    {
        int layerMask = (1 << 16); // raycast only in layer 16 (DeformationGizmo)
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        GameObject previousGameObject = _CurrentGameObject;
        if (Physics.Raycast(ray, out hitInfo, 1000.0f, layerMask))
        {
            _TranslatingGizmo = hitInfo.collider.gameObject;
        }
    }

    public void OnDisable()
    {
        GizmoStart.SetActive(false);
        GizmoEnd.SetActive(false);
        GizmoArrow.SetActive(false);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        if (_CurrentGameObject)
        {
            //_currentGameObject.AddComponent<MeshCollider>();
            _CurrentGameObject = null;
            _CurrentMesh = null;
            _Selected = false;
            _TranslatingGizmo = null;
            _TranslatePressed = false;
        }
    }

    private void OnEnable()
    {
        //GizmoStart.SetActive(true);
        //GizmoEnd.SetActive(true);
        //GizmoArrow.SetActive(true);
    }


    /// <summary>
    /// update the mesh when translating a point, with a transform radius for a uniform effect
    /// </summary>
    public void UpdateTranslatingVertices()
    {

        //float d

        for (int i = 0; i < DisplacedVertices.Length; i++)
        {
            DisplacedVertices[i] = OriginalVertices[i];
        }

        Vector3 pos = _startPos_meshCoordinate;
        Vector3 f = (_endPos_meshCoordinate - pos) * 2.0f;
        int nbvertex = _CurrentMesh.vertexCount;
        for (int iter = 0; iter < 50; iter++)
        {
            //pos = _startPos + iter / 100f * f;
            //transform all points
            for (int i = 0; i < nbvertex; i++)
            {
                /*
                d = Math.Min(Vector3.Distance(_currentVertex, displacedVertices[i]), transformRadius) * _currentGameObject.transform.localScale.x;
                if (d < transformRadius)//only transform those in radius
                {
                    displacedVertices[i] += (dir / (100.0f * _currentGameObject.transform.localScale.x)) * (1.0f - (d / transformRadius));
                }
                */

                //Vector3 dis = BrushManager.ComputeDisplacement(DisplacedVertices[i], _CurrentVertex, f);
                //Vector3 dis = BrushManager.ComputeDisplacement(DisplacedVertices[i], pos, f);
                Vector3 dis = BrushManager.ComputeDisplacement(DisplacedVertices[i], _startPos_meshCoordinate, f);
                DisplacedVertices[i] += dis;
    /*            if (dis.magnitude > minValue)
                {
                }*/
            }

            //pos += BrushManager.ComputeDisplacement(pos, pos, f);
        }


        //then update the position of reference point and gizmo
        //Vector3 curentDis = BrushManager.ComputeDisplacement(_startPos, _startPos, f);
        //_CurrentVertex += curentDis;
        //_currentVertex += dir / (100.0f * _currentGameObject.transform.localScale.x);
        //Gizmo.transform.position = _CurrentGameObject.transform.TransformPoint(_CurrentVertex);

        _CurrentMesh.vertices = DisplacedVertices;
        _CurrentMesh.RecalculateNormals();
        //destroy the old mesh collider, anoter one will be automatically created to match the new mesh
        Destroy(_CurrentGameObject.GetComponent<MeshCollider>());

    }


    //public void ApplySmoothing()
    //{
    //    //then update the position of reference point and gizmo
    //    _CurrentVertex = DisplacedVertices[_CurrentVertexIndex];
    //    GizmoStart.transform.position = _CurrentGameObject.transform.TransformPoint(_CurrentVertex);
    //    GizmoEnd.transform.position = GizmoStart.transform.position;

    //    _CurrentMesh.vertices = DisplacedVertices;
    //    _CurrentMesh.RecalculateNormals();
    //    //destroy the old mesh collider, anoter one will be automatically created to match the new mesh
    //    Destroy(_CurrentGameObject.GetComponent<MeshCollider>());
    //}

    ///// <summary>
    ///// update the mesh when smoothing from a point
    ///// </summary>
    //public void SmoothInSphere(float strength)
    //{
    //    float d;
    //    for (int i = 0; i < _CurrentMesh.vertexCount; i++)
    //    {
    //        d = Vector3.Distance(_CurrentVertex, DisplacedVertices[i]);
    //        if (d < TransformRadius)//only smooth those in radius
    //        {
    //            Vector3 avgPoint = Vector3.zero;
    //            int nbNeigh = 0;
    //            foreach (int neigh in _CurrentMeshOneRing[i])
    //            {
    //                avgPoint += DisplacedVertices[neigh];
    //                nbNeigh++;
    //            }
    //            avgPoint /= nbNeigh;
    //            float scalingFactor = Mathf.Exp(- d*d / (TransformRadius*TransformRadius));
    //            Vector3 dis = (avgPoint - DisplacedVertices[i]) * strength * scalingFactor;
    //            DisplacedVertices[i] += dis;
    //        }
    //    }
    //}

    //private void CollectOneRingFromCurrentMesh()
    //{
    //    // Colect one ring

    //    for (int v_id = _CurrentMeshOneRing.Count; v_id < _CurrentMesh.vertexCount; v_id++)
    //    {
    //        _CurrentMeshOneRing.Add(new List<int>(12)); // Initialize with a capacity of 12
    //    }
    //    for (int v_id = 0; v_id < _CurrentMesh.vertexCount; v_id++)
    //    {
    //        _CurrentMeshOneRing[v_id].Clear();
    //    }
    //    for (int t_id = 0; t_id < _CurrentMesh.triangles.Length; t_id += 3)
    //    {
    //        int t0 = _CurrentMesh.triangles[t_id];
    //        int t1 = _CurrentMesh.triangles[t_id + 1];
    //        int t2 = _CurrentMesh.triangles[t_id + 2];
    //        _CurrentMeshOneRing[t0].Add(t1);
    //        _CurrentMeshOneRing[t0].Add(t2);

    //        _CurrentMeshOneRing[t1].Add(t0);
    //        _CurrentMeshOneRing[t1].Add(t2);

    //        _CurrentMeshOneRing[t2].Add(t0);
    //        _CurrentMeshOneRing[t2].Add(t1);
    //    }
    //}

    /*public void Export()
    {
        
        MeshToObjCreator.SaveMeshToObj(_CurrentMesh, OUTPUT_PATH);
        
    }*/


}
