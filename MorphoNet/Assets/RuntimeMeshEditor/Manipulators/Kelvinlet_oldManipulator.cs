using UnityEngine;
using Cursor = UnityEngine.Cursor;


public class Kelvinlet_oldManipulator : AbstractManipulator
{
    private bool _Selected = false;
    private bool _TranslatePressed = false;

    private GameObject _CurrentGameObject;
    private Mesh _CurrentMesh;

    private Vector3[] DisplacedVertices;
    private Vector3[] OriginalVertices;

    private Vector3 _startPos_meshCoordinate;
    private Vector3 _endPos_meshCoordinate;

    private Vector3 _PreviousMousePos;

    private GameObject _TranslatingGizmo = null;

    public float TransformRadius;

    public float ElasticShearModulus = 1.0f;
    public float PoissonRatio = 0.4f;
    public float PressureFactor = 1f;

    public GameObject GizmoStart;
    public GameObject GizmoEnd;
    public GameObject GizmoArrow;

    public GameObject RadiusSphere;

    public GameObject Line;

    public BrushManager BrushManager;
    public float MinValue = 0.002f;

    private void Start()
    {
        if (BrushManager == null)
        {
            bool got = TryGetComponent(out BrushManager);
            if (!got)
            {
                BrushManager = gameObject.AddComponent<BrushManager>();
            }
        }
        OnValueChanged();
    }

    // Pre-computes values and update visuals only when values are changed
    public void OnValueChanged()
    {
        BrushManager.InitComputation(ElasticShearModulus, PoissonRatio, PressureFactor, TransformRadius);
        if (_CurrentGameObject != null)
        {
            RadiusSphere.transform.localScale = Vector3.Scale(_CurrentGameObject.transform.lossyScale, BrushManager.ComputeRange(MinValue, Input.mousePosition - _PreviousMousePos));
            UpdateTranslatingVertices();
        }
    }

    public override void SetRadiusFromSlider(float size)
    {
        size = ConvertSliderValue(size);
        TransformRadius = size;
        OnValueChanged();
    }

    public void UpdatePositionGizmo()
    {
        Vector3 worldStartPos = _CurrentGameObject.transform.TransformPoint(_startPos_meshCoordinate);
        Vector3 worldEndPos = _CurrentGameObject.transform.TransformPoint(_endPos_meshCoordinate);
        GizmoStart.transform.position = worldStartPos;
        GizmoEnd.transform.position = worldEndPos;
        Line.transform.LookAt(worldEndPos);
        Line.transform.localScale = new Vector3(Line.transform.localScale.x, Line.transform.localScale.y, Vector3.Distance(worldStartPos, worldEndPos));
        GizmoArrow.transform.position = 0.5f * (worldStartPos + worldEndPos);
        GizmoArrow.transform.LookAt(worldEndPos, Camera.main.transform.position - GizmoArrow.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        Vector3 dir = (mousePos - _PreviousMousePos) * 0.02f;
        if (_CurrentGameObject != null)
        {
            dir = _CurrentGameObject.transform.InverseTransformVector(dir);
        }
        else if (_Selected)
        {
            // Something has gone wrong, reset
            OnDisable();
        }

        if (Input.GetMouseButtonDown(0) && _TranslatingGizmo == null) // if left click
        {
            GetEditableGizmo();
            if (_TranslatingGizmo == null)
            {
                GetEditableMeshPoint();
            }
        }

        if (Input.GetMouseButton(0) && _TranslatingGizmo != null) // if holding left click (Translate)
        {
            if (!_TranslatePressed)
            {
                _TranslatePressed = true;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.None;
            }
        }

        if (!Input.GetMouseButton(0)) // if releasing left click
        {
            if (_CurrentGameObject != null && _TranslatePressed)
            {
                _CurrentGameObject.AddComponent<MeshCollider>();
            }
            _TranslatingGizmo = null;
            _TranslatePressed = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        if (_Selected && _TranslatePressed)
        {
            if (_TranslatingGizmo == GizmoEnd)
            {
                _endPos_meshCoordinate += dir;
            }
            else
            {
                _startPos_meshCoordinate += dir;
            }
            UpdateTranslatingVertices();
        }

        bool moving = (Input.GetMouseButton(0) || Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.Space));
#if UNITY_STANDALONE_OSX
            moving = (Input.GetMouseButton(0) || Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.LeftControl));
#endif

        if (_CurrentGameObject != null && (moving || Input.mouseScrollDelta.y != 0f))
        {
            UpdatePositionGizmo();
        }

        _PreviousMousePos = Input.mousePosition;
    }

    /// <summary>
    /// Select a point in a mesh for editing with a raycast
    /// </summary>
    private void GetEditableMeshPoint()
    {
        //cast a ray from camera to mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        GameObject previousGameObject = _CurrentGameObject;
        if (Physics.Raycast(ray, out hitInfo, 1000.0f))
        {
            MeshFilter mf;
            if ((mf = hitInfo.collider.gameObject.GetComponent<MeshFilter>()) && (hitInfo.collider.gameObject.name != "Background"))//if the item has a mesh (and a collider):
            {
                _Selected = true;
                _CurrentMesh = mf.mesh;

                _CurrentGameObject = mf.gameObject;
                Vector3 closest = _CurrentMesh.vertices[0];
                Vector3 pt = hitInfo.point;
                Vector3 _CurrentVertex = Vector3.zero;
                for (int v_id = 0; v_id < _CurrentMesh.vertices.Length; v_id++)//get the vertex closest to the ray intersection
                {
                    Vector3 p = _CurrentMesh.vertices[v_id];
                    Vector3 tp = _CurrentGameObject.transform.TransformPoint(p);
                    if (Vector3.Distance(pt, tp) < Vector3.Distance(pt, closest))
                    {
                        closest = tp;
                        _CurrentVertex = p;
                    }
                }
                DisplacedVertices = _CurrentMesh.vertices;
                OriginalVertices = _CurrentMesh.vertices;
                _startPos_meshCoordinate = _CurrentVertex;
                _endPos_meshCoordinate = _CurrentVertex;
                GizmoStart.transform.position = closest;
                GizmoStart.SetActive(true);
                GizmoEnd.transform.position = closest;
                GizmoEnd.SetActive(true);
                GizmoArrow.SetActive(true);
                _TranslatingGizmo = GizmoEnd;
                UpdatePositionGizmo();
            }
        }
        if (previousGameObject)
        {
            previousGameObject.gameObject.AddComponent<MeshCollider>();
        }
    }

    /// <summary>
    /// Select the Gizmo to translate with a raycast
    /// </summary>
    public void GetEditableGizmo()
    {
        int layerMask = LayerMask.GetMask("DeformationGizmo"); // raycast only in layer DeformationGizmo
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        GameObject previousGameObject = _CurrentGameObject;
        if (Physics.Raycast(ray, out hitInfo, 1000.0f, layerMask))
        {
            _TranslatingGizmo = hitInfo.collider.gameObject;
        }
    }

    public void OnDisable()
    {
        GizmoStart.SetActive(false);
        GizmoEnd.SetActive(false);
        GizmoArrow.SetActive(false);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        if (_CurrentGameObject)
        {
            _CurrentGameObject = null;
            _CurrentMesh = null;
            _Selected = false;
            _TranslatingGizmo = null;
            _TranslatePressed = false;
        }
    }

    private void OnEnable()
    {

    }


    /// <summary>
    /// update the mesh when translating a point, with a transform radius for a uniform effect
    /// </summary>
    public void UpdateTranslatingVertices()
    {

        for (int i = 0; i < DisplacedVertices.Length; i++)
        {
            DisplacedVertices[i] = OriginalVertices[i];
        }

        Vector3 pos = _startPos_meshCoordinate;
        Vector3 f = (_endPos_meshCoordinate - pos) * 2.0f;
        int nbvertex = _CurrentMesh.vertexCount;
        for (int iter = 0; iter < 50; iter++)
        {
            for (int i = 0; i < nbvertex; i++)
            {
                Vector3 dis = BrushManager.ComputeDisplacement(DisplacedVertices[i], _startPos_meshCoordinate, f);
                DisplacedVertices[i] += dis;
            }
        }

        _CurrentMesh.vertices = DisplacedVertices;
        _CurrentMesh.RecalculateNormals();
        //destroy the old mesh collider, anoter one will be automatically created to match the new mesh
        Destroy(_CurrentGameObject.GetComponent<MeshCollider>());

    }
}
