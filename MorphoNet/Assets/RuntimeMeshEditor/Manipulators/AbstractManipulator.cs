using MorphoNet;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class AbstractManipulator : MonoBehaviour
{
    public float diag = 10.0f;
    public float percentRadiusMin = 0.1f;
    public float percentRadiusMax = 0.5f;

    public float sliderValue;

    public virtual void SetRadiusFromSlider(float size)
    {
        sliderValue = size;
    }

    protected float ConvertSliderValue(float value)
    {
        float radiusMin = percentRadiusMin * diag;
        float radiusMax = percentRadiusMax * diag;
        return Mathf.Lerp(radiusMin, radiusMax, value);
    }

    protected bool RaycastFromScreenPosition(Vector3 position, out RaycastHit hitInfo)
    {
        int layerMask = Physics.DefaultRaycastLayers & ~(LayerMask.GetMask("DeformationGizmo")); // Raycast without layer DeformationGizmo
        Ray ray = Camera.main.ScreenPointToRay(position);
        return Physics.Raycast(ray, out hitInfo, 1000.0f, layerMask);
    }

    protected bool IsPointerGameObjectAvaiableToDeform(GameObject go, bool checkUI = true)
    {
        bool cursorPosIsAvailableToDeform = false;
        // Check if raycast point is available to deform : 
        //      First check if the object has a MeshFilter, isn't background and there isn't UI under cursor
        MeshFilter mf = go.GetComponent<MeshFilter>();
        if (mf != null && go != MorphoTools.GetBackground() && (!checkUI || !EventSystem.current.IsPointerOverGameObject()))
        {
            //  And check that the cell is selected
            //  a cell gameObject name has format : [TIME],[ID],[?]
            string name = go.name;
            string[] names = name.Split(',');
            if (names.Length > 2)
            {
                try
                {
                    int cursorCellTime = int.Parse(names[0]);
                    string cursorCellId = names[1];
                    List<Cell> clickedCells = MorphoTools.GetPickedManager().clickedCells;
                    cursorPosIsAvailableToDeform = clickedCells.Exists(
                            cell => cell.ID == cursorCellId && cell.t == cursorCellTime
                        );
                }
                catch (Exception)
                {

                }
            }
        }

        return cursorPosIsAvailableToDeform;
    }
}
