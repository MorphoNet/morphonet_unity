using MorphoNet;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class SmoothManipulator : AbstractManipulator
{
    private bool _Selected = false;
    private bool _ClickedBackground = false;

    private MeshFilter _MouseOverMeshFilter;
    private GameObject _CurrentGameObject;
    private MeshCollider _CurrentMeshCollider;
    private Mesh _CurrentMesh;
    private List<List<int>> _CurrentMeshOneRing;
    private Vector3[] _DisplacedVertices;
    private Vector3 _CurrentVertex_meshCoordinate;
    private Vector3 _CurrentVisualScaling = new Vector3(0.1f, 0.1f, 0.1f);

    public float SmoothingRadius;

    public float SmoothingStrength = 0.3f;

    public Gizmo Gizmo;
    public GameObject RadiusSphere;
    public EventSystem EventSystem;

    private void Awake()
    {
        _CurrentMeshOneRing = new List<List<int>>();
    }

    public override void SetRadiusFromSlider(float size)
    {
        base.SetRadiusFromSlider(size);
        SmoothingRadius = ConvertSliderValue(sliderValue);
        if (enabled) 
        {
            UpdateRadiusGizmo();
        }
    }

    private void UpdatePositionGizmo()
    {
        Vector3 worldCurrentVertex = _CurrentGameObject.transform.TransformPoint(_CurrentVertex_meshCoordinate);
        Gizmo.transform.position = worldCurrentVertex;
    }

    private void UpdateRadiusGizmo()
    {
        RadiusSphere.transform.localScale = _CurrentVisualScaling * 2.0f * SmoothingRadius;
    }

    private void Update()
    {
        if (!_Selected)
        {
            RaycastHit hitInfo;
            if (RaycastFromScreenPosition(Input.mousePosition, out hitInfo))
            {
                MeshFilter mf = hitInfo.collider.gameObject.GetComponent<MeshFilter>();
                if (!_ClickedBackground && IsPointerGameObjectAvaiableToDeform(mf.gameObject))
                {
                    _CurrentVisualScaling = mf.gameObject.transform.lossyScale;
                    UpdateRadiusGizmo();
                    Gizmo.AvailablePosition = Gizmo.GizmoStatus.Available;
                    _MouseOverMeshFilter = mf;
                }
                else
                {
                    Gizmo.AvailablePosition = Gizmo.GizmoStatus.NonAvailable;
                    _MouseOverMeshFilter = null;
                }

                Gizmo.transform.position = hitInfo.point;
            }
        }

        if (Input.GetMouseButtonDown(0)) // if pressed left click
        {
            if (!SetsManager.instance.directPlot.isAvaible)
            {
                _ClickedBackground = true;
                string errorMessage = "Wait, a command is running";
                MorphoDebug.Log(errorMessage, 1);
                return;
            }
            InitSmoothMesh(); // Raycast to get mesh and compute OneRing if needed
        }

        if (Input.GetMouseButton(0) && _Selected) // if holding left click
        {
            UpdateSmoothPosition(); // Raycast and update smooth origin
            Smooth();
        }

        if (Input.GetMouseButtonUp(0)) // if released left click
        {
            _ClickedBackground = false;
            if (_Selected)
            {
                _Selected = false;
                RuntimeMeshEditor.instance.SaveState(
                        new RuntimeMeshEditor.MeshState(_CurrentMesh, (Vector3[])_DisplacedVertices.Clone(), _CurrentMeshCollider)
                    );
            }
        }
    }

    public void OnDisable()
    {
        Gizmo.gameObject.SetActive(false);
        _Selected = false;
        _ClickedBackground = false;
        _CurrentGameObject = null;
        _CurrentMesh = null;
    }

    private void OnEnable()
    {
        Gizmo.gameObject.SetActive(true);
        // Get first selected cell's GameObject to display the correct width
        List<Cell> cells = MorphoTools.GetPickedManager().clickedCells;
        if (cells.Count > 0 && cells[0].Channels.Count > 0)
        {
            GameObject cellObj = cells[0].Channels.First().Value.AssociatedCellObject;
            diag = cellObj.GetComponent<MeshFilter>().mesh.bounds.size.magnitude;
            _CurrentVisualScaling = cellObj.transform.lossyScale;
            SetRadiusFromSlider(sliderValue);
        }
    }

    private void Smooth()
    {
        SmoothInSphere(SmoothingStrength);
        SmoothInSphere(-SmoothingStrength+0.1f);
        ApplySmoothing();
    }


    /// <summary>
    /// Select a GameObject for editing
    /// </summary>
    private void InitSmoothMesh()
    {
        MeshFilter mf = _MouseOverMeshFilter;
        if (mf != null)
        {
            _Selected = true;
            _CurrentMesh = mf.mesh;
            _CurrentMeshCollider = mf.gameObject.GetComponent<MeshCollider>();
            _DisplacedVertices = _CurrentMesh.vertices;
            Gizmo.AvailablePosition = Gizmo.GizmoStatus.Selected;
            if (_CurrentGameObject != mf.gameObject)
            {
                _CurrentGameObject = mf.gameObject;
                // Todo : Dictionary of (n number of) oneRing
                CollectOneRingFromCurrentMesh();
                //UpdateRadiusGizmo();
                RuntimeMeshEditor.instance.ReplaceState(
                        new RuntimeMeshEditor.MeshState(_CurrentMesh, (Vector3[])_CurrentMesh.vertices.Clone(), _CurrentMeshCollider)
                    );
            }
        }
        else
        {
            _ClickedBackground = true;
        }
    }

    /// <summary>
    /// Update smooth's editing point on mesh
    /// </summary>
    private void UpdateSmoothPosition()
    {
        int layerMask = Physics.DefaultRaycastLayers & ~(LayerMask.GetMask("DeformationGizmo")); // Raycast without layer DeformationGizmo
        //cast a ray from camera to mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, 1000.0f, layerMask))
        {
            if (hitInfo.collider.gameObject == _CurrentGameObject)
            {
                _CurrentVertex_meshCoordinate = _CurrentGameObject.transform.InverseTransformPoint(hitInfo.point);
                UpdatePositionGizmo();
            }
        }
    }

    /// <summary>
    /// apply the DisplacedVertices to the mesh
    /// </summary>
    public void ApplySmoothing()
    {
        _CurrentMesh.vertices = _DisplacedVertices;
        _CurrentMesh.RecalculateNormals();
        // if the object uses a mesh collider, we need to update it
        if (_CurrentMeshCollider != null)
        {
            _CurrentMeshCollider.sharedMesh = null;
            _CurrentMeshCollider.sharedMesh = _CurrentMesh;
        }
    }

    /// <summary>
    /// update the DisplacedVertices array when smoothing from a point
    /// </summary>
    public void SmoothInSphere(float strength)
    {
        // can be optimised by using a kdTree of vertices to find vertices inside of the radius
        float inverseSmoothingRadiusSquared = 1.0f / (SmoothingRadius * SmoothingRadius);
        for (int i = 0; i < _CurrentMesh.vertexCount; i++)
        {
            float d = Vector3.Distance(_CurrentVertex_meshCoordinate, _DisplacedVertices[i]);
            if (d < SmoothingRadius) // only smooth those in radius
            {
                Vector3 avgPoint = Vector3.zero;
                int nbNeigh = 0;
                foreach (int neigh in _CurrentMeshOneRing[i])
                {
                    avgPoint += _DisplacedVertices[neigh];
                    nbNeigh++;
                }
                avgPoint /= nbNeigh;
                float scalingFactor = Mathf.Exp(-d * d * inverseSmoothingRadiusSquared);
                Vector3 dis = (avgPoint - _DisplacedVertices[i]) * strength * scalingFactor;
                _DisplacedVertices[i] += dis;
            }
        }
    }

    private void CollectOneRingFromCurrentMesh()
    {
        // If current mesh has more vertices than the list, append the list
        for (int v_id = _CurrentMeshOneRing.Count; v_id < _CurrentMesh.vertexCount; v_id++)
        {
            _CurrentMeshOneRing.Add(new List<int>(12)); // Initialize with a capacity of 12
        }
        // Clear each lists
        for (int v_id = 0; v_id < _CurrentMesh.vertexCount; v_id++)
        {
            _CurrentMeshOneRing[v_id].Clear();
        }
        // Colect one ring
        for (int t_id = 0; t_id < _CurrentMesh.triangles.Length; t_id += 3)
        {
            int t0 = _CurrentMesh.triangles[t_id];
            int t1 = _CurrentMesh.triangles[t_id + 1];
            int t2 = _CurrentMesh.triangles[t_id + 2];
            _CurrentMeshOneRing[t0].Add(t1);
            _CurrentMeshOneRing[t0].Add(t2);

            _CurrentMeshOneRing[t1].Add(t0);
            _CurrentMeshOneRing[t1].Add(t2);

            _CurrentMeshOneRing[t2].Add(t0);
            _CurrentMeshOneRing[t2].Add(t1);
        }
    }

}
