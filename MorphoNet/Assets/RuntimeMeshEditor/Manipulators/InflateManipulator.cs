using MorphoNet;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class InflateManipulator : AbstractManipulator
{
    private bool _Selected = false;
    private bool _ClickedBackground = false;

    private MeshFilter _MouseOverMeshFilter;
    private GameObject _CurrentGameObject;
    private MeshCollider _CurrentMeshCollider;
    private Mesh _CurrentMesh;
    private Vector3 _CurrentVertex_meshCoordinate;
    private Vector3 _CurrentVisualScaling = new Vector3(0.1f, 0.1f, 0.1f);

    public float Radius;

    public float Strength = 2f;

    public Gizmo Gizmo;
    public GameObject RadiusSphere;
    public EventSystem EventSystem;

    private void Awake()
    {
    }

    public override void SetRadiusFromSlider(float size)
    {
        base.SetRadiusFromSlider(size);
        Radius = ConvertSliderValue(sliderValue);
        if (enabled) 
        {
            UpdateRadiusGizmo();
        }
    }

    private void UpdatePositionGizmo()
    {
        Vector3 worldCurrentVertex = _CurrentGameObject.transform.TransformPoint(_CurrentVertex_meshCoordinate);
        Gizmo.transform.position = worldCurrentVertex;
    }

    private void UpdateRadiusGizmo()
    {
        RadiusSphere.transform.localScale = _CurrentVisualScaling * 2.0f * Radius;
    }

    private void Update()
    {
        if (!_Selected)
        {
            RaycastHit hitInfo;
            if (RaycastFromScreenPosition(Input.mousePosition, out hitInfo))
            {
                MeshFilter mf = hitInfo.collider.gameObject.GetComponent<MeshFilter>();
                if (!_ClickedBackground && IsPointerGameObjectAvaiableToDeform(mf.gameObject))
                {
                    _CurrentVisualScaling = mf.gameObject.transform.lossyScale;
                    UpdateRadiusGizmo();
                    Gizmo.AvailablePosition = Gizmo.GizmoStatus.Available;
                    _MouseOverMeshFilter = mf;
                }
                else
                {
                    Gizmo.AvailablePosition = Gizmo.GizmoStatus.NonAvailable;
                    _MouseOverMeshFilter = null;
                }

                Gizmo.transform.position = hitInfo.point;
            }
        }

        if (Input.GetMouseButtonDown(0)) // if pressed left click
        {
            if (!SetsManager.instance.directPlot.isAvaible)
            {
                _ClickedBackground = true;
                string errorMessage = "Wait, a command is running";
                MorphoDebug.Log(errorMessage, 1);
                return;
            }
            InitMesh(); // Raycast to get mesh and compute OneRing if needed
        }

        if (Input.GetMouseButton(0) && _Selected) // if holding left click
        {
            UpdatePosition(); // Raycast and update smooth origin
            Inflate();
        }

        if (Input.GetMouseButtonUp(0)) // if released left click
        {
            _ClickedBackground = false;
            if (_Selected)
            {
                _Selected = false;
                RuntimeMeshEditor.instance.SaveState(
                        new RuntimeMeshEditor.MeshState(_CurrentMesh, (Vector3[])_CurrentMesh.vertices.Clone(), _CurrentMeshCollider)
                    );
            }
        }
    }

    public void OnDisable()
    {
        Gizmo.gameObject.SetActive(false);
        _Selected = false;
        _ClickedBackground = false;
        _CurrentGameObject = null;
        _CurrentMesh = null;
    }

    private void OnEnable()
    {
        Gizmo.gameObject.SetActive(true);
        // Get first selected cell's GameObject to display the correct width
        List<Cell> cells = MorphoTools.GetPickedManager().clickedCells;
        if (cells.Count > 0 && cells[0].Channels.Count > 0)
        {
            GameObject cellObj = cells[0].Channels.First().Value.AssociatedCellObject;
            diag = cellObj.GetComponent<MeshFilter>().mesh.bounds.size.magnitude;
            _CurrentVisualScaling = cellObj.transform.lossyScale;
            SetRadiusFromSlider(sliderValue);
        }
    }

    private Vector3 GetClosestNormal(Vector3 position, Mesh mesh)
    {
        // can be optimised by using a kdTree of vertices to find nearest neighbour's normal
        int closestId = 0;
        float closestMagn = Vector3.SqrMagnitude(position - mesh.vertices[closestId]);
        for (int v_id = 0; v_id < mesh.vertices.Length; v_id++)//get the vertex closest to the ray intersection
        {
            Vector3 p = mesh.vertices[v_id];
            float curMagn = Vector3.SqrMagnitude(position - p);
            if (curMagn < closestMagn)
            {
                closestId = v_id;
                closestMagn = curMagn;
            }
        }
        return mesh.normals[closestId];
    }

    private void Inflate()
    {
        // can be optimised by using a kdTree of vertices to find vertices inside of the radius
        float scalingFactor = Radius * Strength * Time.deltaTime;
        float squareRadius = Radius * Radius;
        Vector3 closestNormal = GetClosestNormal(_CurrentVertex_meshCoordinate, _CurrentMesh);
        Vector3 pos = _CurrentVertex_meshCoordinate - Radius * closestNormal;
        Vector3[] DisplacedVertices = _CurrentMesh.vertices;
        for (int i = 0; i < _CurrentMesh.vertexCount; i++)
        {
            if (Vector3.SqrMagnitude(DisplacedVertices[i] - _CurrentVertex_meshCoordinate) < squareRadius)
            {
                DisplacedVertices[i] += Vector3.Normalize(DisplacedVertices[i] - pos) * scalingFactor;
            }
        }
        _CurrentMesh.vertices = DisplacedVertices;
        _CurrentMesh.RecalculateNormals();
        // if the object uses a mesh collider, we need to update it
        if (_CurrentMeshCollider != null)
        {
            _CurrentMeshCollider.sharedMesh = null;
            _CurrentMeshCollider.sharedMesh = _CurrentMesh;
        }


        //Vector3 dir = _CurrentGameObject.transform.InverseTransformDirection(- Camera.main.ScreenPointToRay(Input.mousePosition).direction);
        //float inverseRadiusSquared = 4.0f / (Radius * Radius);
        //Vector3[] DisplacedVertices = _CurrentMesh.vertices;
        //for (int i = 0; i < _CurrentMesh.vertexCount; i++)
        //{
        //    Vector3 diff = DisplacedVertices[i] - _CurrentVertex_meshCoordinate;
        //    float d = diff.magnitude;
        //    if (d < Radius)
        //    {
        //        float scalingFactor = Mathf.Exp(-d * d * inverseRadiusSquared);
        //        Vector3 dis = (0.75f*dir + 0.25f*diff.normalized) * Strength * scalingFactor;
        //        DisplacedVertices[i] += dis;
        //    }
        //}
        //_CurrentMesh.vertices = DisplacedVertices;
        //_CurrentMesh.RecalculateNormals();
        //// if the object uses a mesh collider, we need to update it
        //if (_CurrentMeshCollider != null)
        //{
        //    _CurrentMeshCollider.sharedMesh = null;
        //    _CurrentMeshCollider.sharedMesh = _CurrentMesh;
        //}
    }


    /// <summary>
    /// Select a GameObject for editing
    /// </summary>
    private void InitMesh()
    {
        MeshFilter mf = _MouseOverMeshFilter;
        if (mf != null)
        {
            _Selected = true;
            _CurrentMesh = mf.mesh;
            _CurrentMeshCollider = mf.gameObject.GetComponent<MeshCollider>();
            Gizmo.AvailablePosition = Gizmo.GizmoStatus.Selected;
            if (_CurrentGameObject != mf.gameObject)
            {
                _CurrentGameObject = mf.gameObject;
                RuntimeMeshEditor.instance.ReplaceState(
                        new RuntimeMeshEditor.MeshState(_CurrentMesh, (Vector3[])_CurrentMesh.vertices.Clone(), _CurrentMeshCollider)
                    );
            }
        }
        else
        {
            _ClickedBackground = true;
        }
    }

    /// <summary>
    /// Update editing point on mesh
    /// </summary>
    private void UpdatePosition()
    {
        int layerMask = Physics.DefaultRaycastLayers & ~(LayerMask.GetMask("DeformationGizmo")); // Raycast without layer DeformationGizmo
        //cast a ray from camera to mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, 1000.0f, layerMask))
        {
            if (hitInfo.collider.gameObject == _CurrentGameObject)
            {
                _CurrentVertex_meshCoordinate = _CurrentGameObject.transform.InverseTransformPoint(hitInfo.point);
                UpdatePositionGizmo();
            }
        }
    }
}
