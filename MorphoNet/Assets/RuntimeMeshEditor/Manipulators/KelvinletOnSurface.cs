using MorphoNet;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class KelvinletOnSurface : AbstractManipulator
{
    private bool _Selected = false;
    private bool _ClickedBackground = false;

    private MeshFilter _MouseOverMeshFilter;
    private GameObject _CurrentGameObject;
    private MeshCollider _CurrentMeshCollider;
    private Mesh _CurrentMesh;

    private Vector3 _CurrentVertex_meshCoordinate;
    private Vector3 _CurrentNormal = Vector3.up;
    private Vector3 _CurrentVisualScaling = new Vector3(0.1f, 0.1f, 0.1f);

    public float mode = -1.0f;

    public float TransformRadius;

    public float ElasticShearModulus = 1.0f;
    public float PoissonRatio = 0.4f;
    public float PressureFactor = 1f;

    public Gizmo Gizmo;
    public GameObject RadiusSphere;

    public BrushManager BrushManager;
    public float MinValue = 0.002f;


    private void Awake()
    {
        if (BrushManager == null)
        {
            bool got = TryGetComponent(out BrushManager);
            if (!got)
            {
                BrushManager = gameObject.AddComponent<BrushManager>();
            }
        }
        BrushManager.InitBrushes();
    }

    public override void SetRadiusFromSlider(float size)
    {
        base.SetRadiusFromSlider(size);
        TransformRadius = ConvertSliderValue(sliderValue);
        if (enabled)
        {
            UpdateRadiusGizmo();
        }
    }

    private void UpdateRadiusGizmo()
    {
        BrushManager.InitComputation(ElasticShearModulus, PoissonRatio, PressureFactor, TransformRadius);
        Vector3 scale = Vector3.Scale(_CurrentVisualScaling, BrushManager.ComputeRange(MinValue, _CurrentNormal));
        RadiusSphere.transform.localScale = new Vector3(scale.x, scale.y, 0.1f);
    }

    private void UpdatePositionGizmo()
    {
        Vector3 worldCurrentVertex = _CurrentGameObject.transform.TransformPoint(_CurrentVertex_meshCoordinate);
        Gizmo.transform.position = worldCurrentVertex;
        RadiusSphere.transform.forward = _CurrentGameObject.transform.TransformDirection(_CurrentNormal);
    }

    private void Update()
    {
        if (!_Selected)
        {
            RaycastHit hitInfo;
            if (RaycastFromScreenPosition(Input.mousePosition, out hitInfo))
            {
                MeshFilter mf = hitInfo.collider.gameObject.GetComponent<MeshFilter>();

                Gizmo.transform.position = hitInfo.point;
                _CurrentNormal = GetClosestNormal(mf.transform.InverseTransformPoint(hitInfo.point), mf.mesh);
                RadiusSphere.transform.forward = mf.transform.TransformDirection(_CurrentNormal);

                if (!_ClickedBackground && IsPointerGameObjectAvaiableToDeform(mf.gameObject))
                {
                    Gizmo.AvailablePosition = Gizmo.GizmoStatus.Available;
                    if (_MouseOverMeshFilter != mf)
                    {
                        _MouseOverMeshFilter = mf;
                        _CurrentVisualScaling = mf.gameObject.transform.lossyScale;
                        UpdateRadiusGizmo();
                        RuntimeMeshEditor.instance.ReplaceState(
                                new RuntimeMeshEditor.MeshState(mf.mesh, (Vector3[])mf.mesh.vertices.Clone(), mf.gameObject.GetComponent<MeshCollider>())
                            );
                    }
                }
                else
                {
                    Gizmo.AvailablePosition = Gizmo.GizmoStatus.NonAvailable;
                    _MouseOverMeshFilter = null;
                }
            }
        }

        if (Input.GetMouseButtonDown(0)) // if pressed left click
        {
            if (!SetsManager.instance.directPlot.isAvaible)
            {
                _ClickedBackground = true;
                string errorMessage = "Wait a command is running";
                MorphoDebug.Log(errorMessage, 1);
                return;
            }
            InitMesh();
        }
        

        if (Input.GetMouseButton(0) && _Selected) // if holding left click
        {
            bool isPositionAvailableToDeform = UpdatePosition(); // Raycast and update origin
            if (isPositionAvailableToDeform)
            {
                UpdateTranslatingVertices();
            }
        }

        if (Input.GetMouseButtonUp(0)) // if released left click
        {
            _ClickedBackground = false;
            if (_Selected)
            {
                _Selected = false;
                RuntimeMeshEditor.instance.SaveState(
                        new RuntimeMeshEditor.MeshState(_CurrentMesh, (Vector3[])_CurrentMesh.vertices.Clone(), _CurrentMeshCollider)
                    );
            }
        }
    }

    public void OnDisable()
    {
        Gizmo.gameObject.SetActive(false);
        _Selected = false;
        _ClickedBackground = false;
        _CurrentGameObject = null;
        _CurrentMesh = null;
        RadiusSphere.transform.forward = Vector3.forward;
    }

    private void OnEnable()
    {
        BrushManager.currentBrush = 1;
        Gizmo.gameObject.SetActive(true);
        // Get cell gameobject to display the correct width
        List<Cell> cells = MorphoTools.GetPickedManager().clickedCells;
        if (cells.Count > 0 && cells[0].Channels.Count > 0)
        {
            GameObject cellObj = cells[0].Channels.First().Value.AssociatedCellObject;
            diag = cellObj.GetComponent<MeshFilter>().mesh.bounds.size.magnitude;
            _CurrentVisualScaling = cellObj.transform.lossyScale;
        }
        SetRadiusFromSlider(sliderValue);
    }

    /// <summary>
    /// Select a GameObject for editing
    /// </summary>
    private void InitMesh()
    {
        MeshFilter mf = _MouseOverMeshFilter;
        if (mf != null)
        {
            _Selected = true;
            _CurrentMesh = mf.mesh;
            _CurrentMeshCollider = mf.gameObject.GetComponent<MeshCollider>();
            Gizmo.AvailablePosition = Gizmo.GizmoStatus.Selected;
            if (_CurrentGameObject != mf.gameObject)
            {
                _CurrentGameObject = mf.gameObject;
                // to optimise GetClosestNormal, construct kdTree of vertices here
                RuntimeMeshEditor.instance.ReplaceState(
                        new RuntimeMeshEditor.MeshState(_CurrentMesh, (Vector3[])_CurrentMesh.vertices.Clone(), _CurrentMeshCollider)
                    );
            }
        }
        else
        {
            _ClickedBackground = true;
        }
    }

    private Vector3 GetClosestNormal(Vector3 position, Mesh mesh)
    {
        // can be optimised by using a kdTree of vertices to find nearest neighbour's normal
        int closestId = 0;
        float closestMagn = Vector3.SqrMagnitude(position - mesh.vertices[closestId]);
        for (int v_id = 0; v_id < mesh.vertices.Length; v_id++)//get the vertex closest to the ray intersection
        {
            Vector3 p = mesh.vertices[v_id];
            float curMagn = Vector3.SqrMagnitude(position - p);
            if (curMagn < closestMagn)
            {
                closestId = v_id;
                closestMagn = curMagn;
            }
        }
        return mesh.normals[closestId];
    }

    /// <summary>
    /// Update smooth's editing point on mesh
    /// </summary>
    private bool UpdatePosition()
    {
        RaycastHit hitInfo;
        if (RaycastFromScreenPosition(Input.mousePosition, out hitInfo))
        {
            MeshFilter mf = hitInfo.collider.gameObject.GetComponent<MeshFilter>();
            if (IsPointerGameObjectAvaiableToDeform(mf.gameObject, checkUI:false))
            {
                GameObject hitObj = mf.gameObject;
                _CurrentMesh = mf.mesh;
                _CurrentMeshCollider = hitObj.GetComponent<MeshCollider>();
                _CurrentVertex_meshCoordinate = hitObj.transform.InverseTransformPoint(hitInfo.point);
                Vector3 closestNormal = GetClosestNormal(_CurrentVertex_meshCoordinate, _CurrentMesh);
                if (Vector3.Dot(closestNormal, _CurrentNormal) < 0.5f)
                {
                    _CurrentNormal = closestNormal;
                }
                UpdatePositionGizmo();

                if (_CurrentGameObject != hitObj)
                {
                    _CurrentGameObject = hitObj;
                    _CurrentVisualScaling = _CurrentGameObject.transform.lossyScale;
                    UpdateRadiusGizmo();
                    RuntimeMeshEditor.instance.SaveState(
                        new RuntimeMeshEditor.MeshState(_CurrentMesh, (Vector3[])_CurrentMesh.vertices.Clone(), _CurrentMeshCollider)
                    );
                }

                return true;
            }
        }
        return false;
    }


    /// <summary>
    /// update the mesh when translating a point, with a transform radius for a uniform effect
    /// </summary>
    public void UpdateTranslatingVertices()
    {
        Vector3[] DisplacedVertices = _CurrentMesh.vertices;

        Vector3 pos = _CurrentVertex_meshCoordinate;
        Vector3 f = _CurrentNormal * mode;
        int nbvertex = _CurrentMesh.vertexCount;
        for (int iter = 0; iter < 1; iter++)
        {
            for (int i = 0; i < nbvertex; i++)
            {
                Vector3 dis = BrushManager.ComputeDisplacement(DisplacedVertices[i], pos, f);
                DisplacedVertices[i] += dis;
            }
        }

        _CurrentMesh.vertices = DisplacedVertices;
        _CurrentMesh.RecalculateNormals();
        // if the object uses a mesh collider, we need to update it
        if (_CurrentMeshCollider != null)
        {
            _CurrentMeshCollider.sharedMesh = null;
            _CurrentMeshCollider.sharedMesh = _CurrentMesh;
        }
    }
}
