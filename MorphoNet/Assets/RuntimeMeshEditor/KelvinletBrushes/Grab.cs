using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grab : AbstractBrush
{
    public override Vector3 ComputeDisplacement(Vector3 pos, Vector3 origin, Vector3 f, float pressure, float a, float b, float c, float radiusScale_1, float radiusScale_2, float radiusScale_3)
    {
        Vector3 vr = pos - origin;
        float r = vr.magnitude;
        float re = Mathf.Sqrt(r * r + radiusScale_1 * radiusScale_1);

        // GRAB

        float u = (a - b) / re + a * radiusScale_1 * radiusScale_1 / (2 * re * re * re) + b * r * r / (re * re * re);
        u *= c;
        Vector3 dis = f * u * pressure;

        return dis;
    }
}
