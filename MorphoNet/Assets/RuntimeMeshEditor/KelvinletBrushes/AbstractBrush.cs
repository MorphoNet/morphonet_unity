using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractBrush
{
    public abstract Vector3 ComputeDisplacement(Vector3 pos, Vector3 origin, Vector3 f, float pressure, float a, float b, float c, float radiusScale_1, float radiusScale_2, float radiusScale_3);

    public Vector3 ComputeRange(float minValue, Vector3 mouseDir, float pressure, float a, float b, float c, float radiusScale_1, float radiusScale_2, float radiusScale_3)
    {
        float curRange = minValue;
        float prevRange = 0.0f;
        int maxIteration = 100;

        minValue *= minValue;

        int curIt = 0;

        Vector3[] points =
        {
            new Vector3( 1,  0,  0),
            new Vector3(-1,  0,  0),
            new Vector3( 0,  1,  0),
            new Vector3( 0, -1,  0),
            new Vector3( 0,  0,  1),
            new Vector3( 0,  0, -1)
        };
        bool wasRangeGood = false;
        while (curIt < maxIteration)
        {
            bool rangeGood = true;
            for (int i = 0; i < points.Length; i++)
            {
                Vector3 dis = ComputeDisplacement(points[i] * curRange, Vector3.zero, mouseDir, pressure, a, b, c, radiusScale_1, radiusScale_2, radiusScale_3);
                if (dis.sqrMagnitude > minValue)
                {
                    rangeGood = false;
                    break;
                }
            }
            if (wasRangeGood != rangeGood)
            {
                float tmp = curRange;
                curRange = 0.5f * (prevRange + curRange);
                prevRange = tmp;
            }
            else
            {
                curRange = curRange + (curRange - prevRange);
            }

            if (Mathf.Abs(curRange - prevRange) < minValue)
            {
                return Vector3.one * curRange * 2.0f;
            }

            wasRangeGood = rangeGood;
            curIt++;
        }

        return Vector3.one * curRange;
    }
}
