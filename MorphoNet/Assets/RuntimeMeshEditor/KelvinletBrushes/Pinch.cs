using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pinch : AbstractBrush
{
    public override Vector3 ComputeDisplacement(Vector3 pos, Vector3 origin, Vector3 f, float pressure, float a, float b, float c, float radiusScale_1, float radiusScale_2, float radiusScale_3)
    {
        Vector3 vr = pos - origin;
        float r = vr.magnitude;
        float re = Mathf.Sqrt(r * r + radiusScale_1 * radiusScale_1);

        // PINCH

        Matrix4x4 pinchF = Matrix4x4.zero;
        pinchF[0, 1] = f.x;
        pinchF[1, 0] = f.y;

        Vector3 v1 = pinchF * vr;

        Matrix4x4 vr_t = Matrix4x4.zero;
        vr_t[0, 0] = vr.x;
        vr_t[0, 1] = vr.x;
        vr_t[0, 2] = vr.x;

        Matrix4x4 ttt = (vr_t * pinchF).transpose;
        Matrix4x4 ttt1 = ttt * vr_t;
        Vector3 v2 = ttt1 * vr;
        Vector3 dis = v1 * (2 * b - a) / (re * re * re) - v2 * 3 * b / (re * re * re * re * re) - v1 * 3 * a * radiusScale_1 * radiusScale_1 / (2 * re * re * re * re * re);
        dis = -dis * pressure;
        dis *= c;

        return dis;
    }
}