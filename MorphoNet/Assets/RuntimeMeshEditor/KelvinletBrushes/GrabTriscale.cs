using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabTriscale : AbstractBrush
{
    public override Vector3 ComputeDisplacement(Vector3 pos, Vector3 origin, Vector3 f, float pressure, float a, float b, float c, float radiusScale_1, float radiusScale_2, float radiusScale_3)
    {
        Vector3 vr = pos - origin;
        float r = vr.magnitude;
        float re = Mathf.Sqrt(r * r + radiusScale_1 * radiusScale_1);

        // GRAB BISCALE

        float re_1 = re;
        float re_2 = Mathf.Sqrt(r * r + radiusScale_2 * radiusScale_2);
        float re_3 = Mathf.Sqrt(r * r + radiusScale_3 * radiusScale_3);
        float u_1 = (a - b) / re_1 + a * radiusScale_1 * radiusScale_1 / (2f * re_1 * re_1 * re_1) + b * r * r / (re_1 * re_1 * re_1);
        float u_2 = (a - b) / re_2 + a * radiusScale_2 * radiusScale_2 / (2f * re_2 * re_2 * re_2) + b * r * r / (re_2 * re_2 * re_2);
        float u_3 = (a - b) / re_3 + a * radiusScale_3 * radiusScale_3 / (2f * re_3 * re_3 * re_3) + b * r * r / (re_3 * re_3 * re_3);
        float w1 = 1f;
        float w2 = -(radiusScale_3 * radiusScale_3 - radiusScale_1 * radiusScale_1 + 0.0f) / (radiusScale_3 * radiusScale_3 - radiusScale_2 * radiusScale_2);
        float w3 =  (radiusScale_2 * radiusScale_2 - radiusScale_1 * radiusScale_1 + 0.0f) / (radiusScale_3 * radiusScale_3 - radiusScale_2 * radiusScale_2);
        float u = u_1 * w1 + u_2 * w2 + u_3 * w3;
        u *= (c / (w1 / radiusScale_1 + w2 / radiusScale_2 + w3 / radiusScale_3));
        Vector3 dis = f * u * pressure;

        return dis;
    }
}
