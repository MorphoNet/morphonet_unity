using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabBiscale : AbstractBrush
{
    public override Vector3 ComputeDisplacement(Vector3 pos, Vector3 origin, Vector3 f, float pressure, float a, float b, float c, float radiusScale_1, float radiusScale_2, float radiusScale_3)
    {
        Vector3 vr = pos - origin;
        float r = vr.magnitude;
        float re = Mathf.Sqrt(r * r + radiusScale_1 * radiusScale_1);

        // GRAB BISCALE

        float re_1 = re;
        float re_2 = Mathf.Sqrt(r * r + radiusScale_2 * radiusScale_2);
        float u_1 = (a - b) / re_1 + a * radiusScale_1 * radiusScale_1 / (2f * re_1 * re_1 * re_1) + b * r * r / (re_1 * re_1 * re_1);
        float u_2 = (a - b) / re_2 + a * radiusScale_2 * radiusScale_2 / (2f * re_2 * re_2 * re_2) + b * r * r / (re_2 * re_2 * re_2);
        float u = u_1 - u_2;
        u *= (c / (1.0f / radiusScale_1 - 1f / radiusScale_2));
        Vector3 dis = f * u * pressure;

        return dis;
    }
}
