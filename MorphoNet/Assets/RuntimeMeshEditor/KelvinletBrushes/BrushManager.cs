using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushManager : MonoBehaviour
{
    public AbstractBrush[] brushes;
    public int currentBrush = 2;

    private float a;
    private float b;
    private float c;

    private float pressure;

    private float radiusScale_1;
    private float radiusScale_2;
    private float radiusScale_3;

    private float _pressureFactor;

    public void InitBrushes()
    {
        brushes = new AbstractBrush[6];
        brushes[0] = new Grab();
        brushes[1] = new GrabBiscale();
        brushes[2] = new GrabTriscale();
        brushes[3] = new Twist();
        brushes[4] = new Scale();
        brushes[5] = new Pinch();
    }

    public void InitComputation(float elasticShearModulus, float poissonRatio, float pressureFactor, float transformRadius)
    {
        a = 1f / (4f * Mathf.PI * elasticShearModulus);
        b = a / (4f * (1 - poissonRatio));
        c = 2f / (3f * a - 2 * b);

        radiusScale_1 = transformRadius * 0.1f;
        radiusScale_2 = radiusScale_1 * 2;
        radiusScale_3 = radiusScale_2 * 2;

        pressure = .001f * pressureFactor * radiusScale_3;

        _pressureFactor = pressureFactor;
    }

    public Vector3 ComputeDisplacement(Vector3 pos, Vector3 origin, Vector3 f)
    {
        return brushes[currentBrush].ComputeDisplacement(pos, origin, f, pressure, a, b, c, radiusScale_1, radiusScale_2, radiusScale_3);
    }

    public Vector3 ComputeRange(float minValue, Vector3 mouseDir)
    {
        if (mouseDir.sqrMagnitude==0)
        {
            mouseDir = Vector3.one;
        }
        return brushes[currentBrush].ComputeRange(minValue, mouseDir.normalized / _pressureFactor, pressure, a, b, c, radiusScale_1, radiusScale_2, radiusScale_3);
    }
}
