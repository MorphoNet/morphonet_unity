using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Twist : AbstractBrush
{
    public override Vector3 ComputeDisplacement(Vector3 pos, Vector3 origin, Vector3 f, float pressure, float a, float b, float c, float radiusScale_1, float radiusScale_2, float radiusScale_3)
    {
        Vector3 vr = pos - origin;
        float r = vr.magnitude;
        float re = Mathf.Sqrt(r * r + radiusScale_1 * radiusScale_1);

        // TWIST

        float u = -a * (1 / (re * re * re) + 3 * radiusScale_1 * radiusScale_1 / (2 * re * re * re * re * re));

        u *= c;
        Vector3 rot = new Vector3(-f.y, f.x, f.z);
        Vector3 qr = Vector3.Cross(rot, vr);
        Vector3 dis = qr * u * pressure;

        return dis;
    }
}