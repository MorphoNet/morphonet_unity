using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;


/// <summary>
/// simple converter from single mesh (no submeshes) to simple obj file (only faces and triangles).
/// </summary>
public class MeshToObjCreator
{
	public static string ConvertMeshToObj(Mesh m, string name = "")
	{
		//first off, we want to remove point doubles, and make the "old" points ids point towards the non-doubled lists for the faces array, to try and reconstruct the original mesh before unity operations
		//remove duplicates in vertices
		Vector3[] uniquePoints = m.vertices.Distinct().ToArray();

		Debug.Log("number of points without doubles : " + uniquePoints.Length);

		//then map for each "old id" the new id of the unique list
		Dictionary<int, int> oldToNewVertexId = new Dictionary<int, int>();

		int oldId = 0;
		foreach (Vector3 v in m.vertices)
		{
			for (int newId = 0; newId < uniquePoints.Length; newId++)
			{
				if (uniquePoints[newId] == v)
				{
					oldToNewVertexId.Add(oldId, newId);
				}
			}
			oldId++;
		}

		//finally, we can write the obj file, with the new points for the vertex part of the file, and the corresponding new ids for the triangles part of the file

		StringWriter sr = new StringWriter();
		sr.WriteLine("g " + name);

		//write new points
		foreach (Vector3 vv in uniquePoints)
		{
			sr.WriteLine("v {0} {1} {2}", vv.x.ToString("n8", CultureInfo.InvariantCulture), vv.y.ToString("n8", CultureInfo.InvariantCulture), vv.z.ToString("n8", CultureInfo.InvariantCulture));
		}

		//write triangles with the new corresponding ids using the dictionary
		for (int material = 0; material < m.subMeshCount; material++)
		{
			int[] triangles = m.GetTriangles(material);
			for (int i = 0; i < triangles.Length; i += 3)
			{
				sr.WriteLine(string.Format("f {0} {1} {2}", oldToNewVertexId[triangles[i]] + 1, oldToNewVertexId[triangles[i + 1]] + 1, oldToNewVertexId[triangles[i + 2]] + 1));
			}
		}

		return sr.ToString();
	}

    public static string ConvertMeshToObjWithoutDuplicateRemoving(Mesh m, string name = "")
    {
        StringWriter sr = new StringWriter();
        sr.WriteLine("g " + name);

        //write new points
        foreach (Vector3 vv in m.vertices)
        {
            sr.WriteLine("v {0} {1} {2}", vv.x.ToString("n8", CultureInfo.InvariantCulture), vv.y.ToString("n8", CultureInfo.InvariantCulture), vv.z.ToString("n8", CultureInfo.InvariantCulture));
        }

        //write triangles with the new corresponding ids using the dictionary
        for (int material = 0; material < m.subMeshCount; material++)
        {
            int[] triangles = m.GetTriangles(material);
            for (int i = 0; i < triangles.Length; i += 3)
            {
                sr.WriteLine(string.Format("f {0} {1} {2}", triangles[i] + 1, triangles[i + 1] + 1, triangles[i + 2] + 1));
            }
        }

        return sr.ToString();
    }


    /*public static void SaveMeshToObj(Mesh m ,string path, string name="")
    {
		//first off, we want to remove point doubles, and make the "old" points ids point towards the non-doubled lists for the faces array, to try and reconstruct the original mesh before unity operations
		//remove duplicates in vertices
		Vector3[] uniquePoints = m.vertices.Distinct().ToArray();

		Debug.Log("number of points without doubles : " + uniquePoints.Length);

		//then map for each "old id" the new id of the unique list
		Dictionary<int, int> oldToNewVertexId = new Dictionary<int, int>();

		int oldId = 0;
		foreach(Vector3 v in m.vertices)
        {
			for (int newId = 0; newId < uniquePoints.Length; newId++)
			{
				if (uniquePoints[newId] == v)
				{
					oldToNewVertexId.Add(oldId, newId);
				}
			}
			oldId++;
        }

		//finally, we can write the obj file, with the new points for the vertex part of the file, and the corresponding new ids for the triangles part of the file

		var Stream = new FileStream(path, FileMode.Create);
		StreamWriter lLineStreamWriter = new StreamWriter(Stream);
		lLineStreamWriter.WriteLine("g "+name);

		//write new points
		foreach (Vector3 vv in uniquePoints)
		{
			lLineStreamWriter.WriteLine("v {0} {1} {2}", vv.x.ToString("n8",CultureInfo.InvariantCulture), vv.y.ToString("n8", CultureInfo.InvariantCulture), vv.z.ToString("n8", CultureInfo.InvariantCulture));
		}

		//write triangles with the new corresponding ids using the dictionary
		for (int material = 0; material < m.subMeshCount; material++)
		{
			int[] triangles = m.GetTriangles(material);
			for (int i = 0; i < triangles.Length; i += 3)
			{
				lLineStreamWriter.WriteLine(string.Format("f {0} {1} {2}", oldToNewVertexId[triangles[i]] + 1, oldToNewVertexId[triangles[i + 1]] + 1, oldToNewVertexId[triangles[i + 2]] + 1));
			}
		}

		lLineStreamWriter.Flush();
	}*/



}
