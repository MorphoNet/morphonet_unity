using UnityEngine;

public class Gizmo : MonoBehaviour
{
    public enum GizmoStatus
    {
        NonAvailable,   // mouse over an invalid location (e.g. background)
        Available,      // mouse over an valid location (e.g. a cell)
        Selected        // mouse drag
    }
    private GizmoStatus _AvailablePosition = GizmoStatus.NonAvailable;

    public GizmoStatus AvailablePosition { get => _AvailablePosition; set => SetAvailablePosition(value); }

    public MeshRenderer[] MaterialsToChange;
    public Material materialNonAvailable;
    public Material materialAvailable;
    public Material materialSelected;

    private void SetAvailablePosition(GizmoStatus value)
    {
        if (_AvailablePosition != value)
        {
            _AvailablePosition = value;
            Material materialToSet;
            switch (value)
            {
                case GizmoStatus.NonAvailable:
                    materialToSet = materialNonAvailable;
                    break;
                case GizmoStatus.Available:
                    materialToSet = materialAvailable;
                    break;
                case GizmoStatus.Selected:
                    materialToSet = materialSelected;
                    break;
                default:
                    materialToSet = materialNonAvailable;
                    break;
            }
            MaterialsToChange.ForEach(mr => mr.material = materialToSet);
        }
    }

}
