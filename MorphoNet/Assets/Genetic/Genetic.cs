﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using System.Collections.Generic;
using AssemblyCSharp;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using System;

/*
public class Gene : MonoBehaviour{

	public GameObject go;
	public string name;
	public int selection;
	public  Dictionary<Cell,float> Cells; //For  each Cell only one expression for each Gene
	public bool isActive;
	public bool isSelected;
	public int nbCells;
	public Gene (string name)
	{
		this.name = name;
		this.Cells = null;
		this.nbCells = -1;
		this.isActive = false;

		this.go = (GameObject)Instantiate (Genetic.GeneTemplate,Genetic.GeneTemplate.transform.parent);
		this.go.name = this.name;
		this.setName ();
		//this.go.transform.Translate(0, -shift*22f*Instantiation.canvasScale, 0);
		int shift=Genetic.Genes.Count();
		this.go.transform.position=new Vector3(this.go.transform.position.x, Genetic.initialY-shift*Genetic.spaceY*Instantiation.canvasScale, this.go.transform.position.z);
		if(shift<=Genetic.maxNbDraw) this.go.SetActive (true);
		else this.go.SetActive (false);

		this.selection = shift + 1;
		while (this.selection > 254) this.selection -= 254;//Mathf.RoundToInt(Mathf.Repeat(shift+1, 254))+1;;
		this.go.transform.Find("SelectionValue").gameObject.GetComponent<InputField> ().text=this.selection.ToString();
		Genetic.activeListen(this.go.transform.Find ("apply").gameObject.GetComponent<Toggle> (),this);
		this.go.transform.Find ("SelectionValue").gameObject.SetActive (true);
		this.go.transform.Find ("apply").gameObject.SetActive (true);
		Genetic.editSelectionListen (this.go.transform.Find ("SelectionValue").gameObject.GetComponent<InputField> (), this);
	}

	public void setName(){
		string n = this.name;
		if (this.nbCells >= 0) n = "(" + this.nbCells + ")" + n;
		this.go.transform.Find ("GeneName").gameObject.GetComponent<Text> ().text= n;
	}

	public void shift(int shiftV,int decals){
		if (shiftV >= decals && shiftV <= decals + Genetic.maxNbDraw) {
			this.go.SetActive (true);
			this.go.transform.position = new Vector3 (this.go.transform.position.x, Genetic.initialY- (shiftV-decals) * Genetic.spaceY*Instantiation.canvasScale, this.go.transform.position.z);
		}
		else this.go.SetActive (false);
	}

	public void addExpression(Cell c,float expression){
		if (this.Cells == null) this.Cells = new Dictionary<Cell,float> ();
		if (this.Cells.ContainsKey (c)) MorphoDebug.Log ("This Cell " + c.ID + " already has an expression profile for " + this.name);
		this.Cells [c] = expression;
		this.nbCells = this.Cells.Count ();
		this.setName();
	}

	public void editSelection(){
		int previousSelection = this.selection;
		string s = this.go.transform.Find ("SelectionValue").gameObject.GetComponent<InputField> ().text;
		MorphoDebug.Log ("Edit " + s);
		int ns;
		if (int.TryParse (s,out ns)) {
			if (ns > 254) ns = 254;
			this.selection = ns;
		}
		this.go.transform.Find ("SelectionValue").gameObject.GetComponent<InputField> ().text=this.selection.ToString();
		if (this.isActive && previousSelection!=this.selection) {
			foreach (var cv in this.Cells) {
				Cell c = cv.Key;
				c.removeSelection (previousSelection);
				c.addSelection (this.selection);
			}
		}
	}

	public void changeActive(){
		if (!this.isActive) this.active ();
		else this.inActive();
	}
	public void active(){
		foreach (var cv in this.Cells) {
			Cell c = cv.Key;
			c.addSelection (this.selection);
		}
		this.isActive = true;
	}


	public void inActive(){
		foreach (var cv in this.Cells) {
			Cell c = cv.Key;
			c.removeSelection (this.selection);
		}
		this.isActive = false;
	}
}

public class Genetic : MonoBehaviour {

	public static GameObject scrollBar;
	public static Dictionary<string, Gene> Genes ; //List of Genes 


	public static GameObject MenuGenetic;
	public static GameObject GeneTemplate;
	public static GameObject GeneList;


	public static void init () {
		//MorphoDebug.Log("Start Genetic");
		if (scrollBar == null) {
			//Activat Button Genetic
			Button BoutonGenetic = Instantiation.canvas.transform.Find ("BoutonAniseed").gameObject.GetComponent<Button> ();
			BoutonGenetic.onClick.RemoveAllListeners ();
			BoutonGenetic.onClick.AddListener (() => Instantiation.canvas.GetComponent<Menus> ().MenuGeneticActive ());
			Instantiation.canvas.GetComponent<Menus> ().buttonGenetic.SetActive(true);
			Instantiation.canvas.GetComponent<Menus> ().reOrganizeButtons (); //RESCALE BUTTONS AND MENU
			//Initialise Variables 
			MenuGenetic=Instantiation.canvas.transform.Find ("MenuGenetic").gameObject;
			GeneList = MenuGenetic.transform.Find ("GeneList").gameObject;
			GeneTemplate = GeneList.transform.Find ("GeneTemplate").gameObject;
			GeneTemplate.SetActive (false);
			initialY = GeneTemplate.transform.position.y;
			scrollBar = MenuGenetic.transform.Find ("Scrollbar").gameObject;
			scrollBar.SetActive (false);
		}
	}

	public static Gene getGene(string genename){
		if (Genes == null) Genes = new Dictionary<string, Gene> ();
		if (Genes.ContainsKey (genename)) return Genes [genename];
		//Create a new one
		Gene gg=new Gene(genename);
		Genes [genename] = gg;
		return gg;
	}

	public static void addCellGene(Cell c, string genename, float expression){
		Gene g = getGene (genename);
		if(expression>0) g.addExpression (c, expression);
	}

	public static void updateGenesNbCells(){
		foreach (var gv in Genes) {
			Gene g = gv.Value;
			if(g.Cells!=null) g.nbCells = g.Cells.Count ();
			MorphoDebug.Log (" updateGenesNbCells for " + g.name + " -> " + g.nbCells);
			g.setName ();
		}

	}

	//ACTIVE GENE
	public static void activeListen(Toggle b,Gene g){ b.onValueChanged.AddListener (delegate { Genetic.Active (g); });}
	public static void Active(Gene g){   g.changeActive ();  }
	//EDIT SELECTION
	public static void editSelectionListen(InputField inf,Gene g){inf.onEndEdit.AddListener (delegate { Genetic.editSelection (g); });}
	public static void editSelection(Gene g){ 	g.editSelection (); }




	//FOR SCROLL BAR
	public static float spaceY = 20;
	public static float initialY=0;
	public static int maxNbDraw=29;
	public static float sizeBar = 20f;
	public static void updateScrol(int nbTotalScroll){
		scrollBar.GetComponent<Scrollbar> ().value = 1;
		scrollBar.GetComponent<Scrollbar> ().numberOfSteps = 1 + nbTotalScroll - maxNbDraw;
		scrollBar.GetComponent<Scrollbar> ().size = 1f / (float)(1 + nbTotalScroll - maxNbDraw);
		if (nbTotalScroll < Genetic.maxNbDraw)
			scrollBar.SetActive (false);
		else 
			scrollBar.SetActive (true);

		Scroll (1); //Each Time we update the scroll bar (search ,selected cells ..) we reset the list from scratch

	}
	public void onSroll(Single value){
		Scroll (value);
	}
	public static void Scroll(Single value){
		int nbSteps = scrollBar.GetComponent<Scrollbar> ().numberOfSteps;
		int decals = (int)Mathf.Round(nbSteps-value * (nbSteps - 1) -1);
		int nbView = 0;
		foreach (var gv in Genes) {
			Gene g = gv.Value;
			if (g.isSelected) {
				g.shift (nbView, decals);
				nbView += 1;
			}
		}
	}


}
*/