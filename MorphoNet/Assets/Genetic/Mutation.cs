﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Networking;
using System;

namespace MorphoNet
{
    //Mutant
    public class Mutant
    {
        public int id;
        public GameObject go;
        public int biomaterial_id;
        public int mutant_id;
        public int nbderegulations;
        public Dictionary<Cell, float> CellsMutant;
        public Mutation parent;

        public Mutant(int id, Mutation source)
        { this.id = id; parent = source; }

        public void active()
        {
            //MorphoDebug.Log("ACTIVE");
            if (CellsMutant != null)
            {
                foreach (var cn in CellsMutant)
                {
                    Cell c = cn.Key;
                    c.addSelection(parent.geneMutate.selection);
                }
            }
        }
    }

    //Main mutation classes
    public class Mutation : MonoBehaviour
    {
        public GameObject MenuMutants;
        public GameObject MenuDeregulations;
        public GameObject scrollBar;
        public Mutant MutantActif;

        //List all Mutants avaible
        public List<Mutant> Mutants;

        public Gene geneMutate;
        public DataSet AttachedDataset;

        public void AttachDataset(DataSet source)
        {
            AttachedDataset = source;
        }

        public void Start()
        {
        }

        public void init()
        {
            MenuMutants = InterfaceManager.instance.MenuMutant;
            MenuDeregulations = InterfaceManager.instance.MenuDeregulations;
            scrollBar = InterfaceManager.instance.scrollbar_menumutant;
            scrollBar.SetActive(false);
            MutantActif = null;
        }

        //Is this gene have mutant ?
        public void havemutant(Gene g)
        { StartCoroutine(ismutant(g)); }

        //Check if we have any mutant for this gene
        public IEnumerator ismutant(Gene g)
        {
            g.SubGene.transform.Find("mutant").gameObject.SetActive(false);
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("havemutant", g.id);
            //MorphoDebug.Log("havemutant ?. Gene id" + g.id);
            UnityWebRequest www = UnityWebRequests.Post(SetsManager.instance.urlAniseed, "", form);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                if (www.downloadHandler.text == "")
                    g.SubGene.transform.Find("mutant").gameObject.SetActive(false); //No mutant ID
                else
                {  //MUTANT ID
                   //MorphoDebug.Log("Mutant is Desactivate for all genes "+g.id);
                   //g.SubGene.transform.Find("mutant").gameObject.SetActive(true); //DESACTIVATE FOR NOW
                }
            }
            www.Dispose();
        }

        //DEREGULATIONs
        public void listMutantsListen(Button b, Gene g)
        { b.onClick.AddListener(() => listMutants(g)); }

        public void listMutants(Gene g)
        { StartCoroutine(queryMutants(g)); }

        public IEnumerator queryMutants(Gene g)
        {
            MutantActif = null;
            geneMutate = g;
            //MorphoDebug.Log("geneMutate==" + geneMutate.name);
            MenuMutants.SetActive(false);
            MenuDeregulations.SetActive(false);

            MenuMutants.transform.Find("MutationFor").gameObject.GetComponent<Text>().text = g.name;

            g.SubGene.transform.Find("mutant").gameObject.SetActive(false);
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("listmutants", g.id);
            UnityWebRequest www = UnityWebRequests.Post(SetsManager.instance.urlAniseed, "", form);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                //MorphoDebug.Log("www.downloadHandler.text="+www.downloadHandler.text);
                if (www.downloadHandler.text == "")
                    g.SubGene.transform.Find("mutant").gameObject.SetActive(false); //No mutant ID
                else
                { //MUTANT ID
                    g.SubGene.transform.Find("mutant").gameObject.SetActive(true);
                    MenuMutants.SetActive(true);
                    var N = JSONNode.Parse(www.downloadHandler.text);
                    if (N != null && N.Count > 0)
                    {
                        //Delete Previous Deregulations
                        if (Mutants != null)
                        {
                            foreach (Mutant mut in Mutants)
                                Destroy(mut.go);
                            Mutants.Clear();
                        }
                        else
                            Mutants = new List<Mutant>();
                        GameObject MutantsList = MenuMutants.transform.Find("Mutants").gameObject;
                        GameObject template = MutantsList.transform.Find("MutantTemplate").gameObject;
                        template.SetActive(false);
                        initialY = template.transform.position.y;
                        int nbTotalDeregulation = 0;
                        foreach (var key in N.Keys)
                        {
                            int id = int.Parse(key.ToString().Trim());
                            Mutant mutant = new Mutant(id, this);
                            mutant.go = (GameObject)Instantiate(template, MutantsList.transform);
                            mutant.go.name = "MUT_" + id.ToString();
                            mutant.biomaterial_id = int.Parse(N[key]["biomaterial_id"].ToString().Replace('"', ' ').Trim());
                            JSONNode NJ = N[key]["names"];
                            String names = "";
                            //MorphoDebug.Log("Muta,nt "+id+ "with "+NJ.Count);
                            for (int i = 0; i < NJ.Count; i++)
                            {
                                if (names != "")
                                    names += "\n";
                                names += NJ[i].ToString().Replace('"', ' ').Trim();
                            }
                            //MorphoDebug.Log(names);
                            mutant.nbderegulations = NJ.Count;
                            GameObject mutant_name = mutant.go.transform.Find("mutant_name").gameObject;
                            mutant_name.GetComponent<Text>().text = names;
                            //Resize the menu
                            RectTransform mutant_rect = mutant.go.transform.GetComponent<RectTransform>();
                            mutant_rect.sizeDelta = new Vector2(mutant_rect.rect.width, mutant.nbderegulations * spaceY);

                            RectTransform mutant_name_rect = mutant_name.transform.GetComponent<RectTransform>();
                            mutant_name_rect.sizeDelta = new Vector2(mutant_name_rect.rect.width, mutant.nbderegulations * spaceY);

                            mutant.go.SetActive(true);
                            mutant.go.transform.position = new Vector3(mutant.go.transform.position.x, initialY - nbTotalDeregulation * spaceY * InterfaceManager.instance.canvasScale, mutant.go.transform.position.z);
                            nbTotalDeregulation += mutant.nbderegulations;

                            openDeregulationListen(mutant_name.GetComponent<Button>(), mutant);

                            mutant.go.transform.Find("selection").transform.Find("Background").gameObject.GetComponent<Image>().sprite = null;
                            mutant.go.transform.Find("selection").transform.Find("Background").gameObject.GetComponent<Image>().color = geneMutate.go.transform.Find("selection").transform.Find("Background").gameObject.GetComponent<Image>().color;

                            activeMutantListen("selection", mutant.go.transform.Find("selection").gameObject.GetComponent<Toggle>(), mutant);
                            activeMutantListen("selectionWT", mutant.go.transform.Find("selectionWT").gameObject.GetComponent<Toggle>(), mutant);

                            activeAniseedCardListen(mutant.go.transform.Find("genecard").gameObject.GetComponent<Button>(), mutant);
                            Mutants.Add(mutant);
                        }
                        //Resize the menu
                        RectTransform dere_rect = MenuMutants.transform.GetComponent<RectTransform>();
                        if (nbTotalDeregulation > maxNbDraw)
                            dere_rect.sizeDelta = new Vector2(dere_rect.rect.width, 65f + (maxNbDraw + 1) * spaceY);
                        else
                            dere_rect.sizeDelta = new Vector2(dere_rect.rect.width, 65f + nbTotalDeregulation * spaceY);
                    }
                }
            }
            www.Dispose();
            updateScrol();
            StartCoroutine(queryNbCellsbyMutant("selection", g));
            StartCoroutine(queryNbCellsbyMutant("selectionWT", g));
        }

        //ANISEED CARD
        public void activeAniseedCardListen(Button b, Mutant mn)
        { b.onClick.AddListener(() => activeAniseedCard(mn)); }

        public void activeAniseedCard(Mutant mn)
        {  //Change show_gene by show_expression to get expression
            Application.ExternalEval("window.open(\"https://www.aniseed.cnrs.fr/aniseed/experiment/show_insitu_by_biomaterial?biomaterial_id=" + mn.biomaterial_id + "&mutant_id=" + mn.id + "\")");
        }

        //DEREGULATIONs
        public void openDeregulationListen(Button b, Mutant mn)
        { b.onClick.AddListener(() => openDeregulation(mn)); }

        public void openDeregulation(Mutant mn)
        { StartCoroutine(Deregulate.queryDeregulations(mn)); }

        //FOR SCROLL BAR
        public int maxNbDraw = 27;  //Commpris

        public float initialY = 0;
        public float spaceY = 20f;

        public int getNbScrol()
        {
            int nb = 0;
            if (Mutants != null)
                foreach (Mutant mn in Mutants)
                    nb += mn.nbderegulations;
            return nb;
        }

        public void updateScrol()
        {
            int nbTotalScroll = getNbScrol();
            if (nbTotalScroll < maxNbDraw)
            {
                scrollBar.SetActive(false);
                int nbView = 0;
                foreach (Mutant mn in Mutants)
                {
                    mn.go.SetActive(true);
                    mn.go.transform.position = new Vector3(mn.go.transform.position.x, initialY - nbView * spaceY * InterfaceManager.instance.canvasScale, mn.go.transform.position.z);
                    nbView += mn.nbderegulations;
                }
            }
            else
            {
                if (!scrollBar.activeSelf)
                    scrollBar.GetComponent<Scrollbar>().value = 1;
                scrollBar.GetComponent<Scrollbar>().numberOfSteps = 1 + nbTotalScroll - maxNbDraw;
                scrollBar.GetComponent<Scrollbar>().size = 1f / (float)(1 + nbTotalScroll - maxNbDraw);
                scrollBar.SetActive(true);
                Scroll(); //Each Time we update the scroll bar (search ,selected cells ..) we reset the list from scratc
            }
        }

        public void onSroll(Single value)
        { Scroll(); }

        public void Scroll()
        {
            Single value = scrollBar.GetComponent<Scrollbar>().value;
            int nbSteps = scrollBar.GetComponent<Scrollbar>().numberOfSteps;
            int decals = (int)Mathf.Round(nbSteps - value * (nbSteps - 1) - 1);
            int nbView = 0;
            foreach (Mutant mn in Mutants)
            {
                if (nbView >= decals && nbView <= decals + maxNbDraw - mn.nbderegulations + 1)
                {
                    mn.go.SetActive(true);
                    mn.go.transform.position = new Vector3(mn.go.transform.position.x, initialY - (nbView - decals) * spaceY * InterfaceManager.instance.canvasScale, mn.go.transform.position.z);
                }
                else
                    mn.go.SetActive(false);
                nbView += mn.nbderegulations;
            }
        }

        public IEnumerator queryNbCellsbyMutant(String which, Gene g)
        {
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("nbcellsmutants", g.id.ToString());
            if (which == "selectionWT")
                form.AddField("wildtype", "ok");
            form.AddField("organism_id", AttachedDataset.GeneticManager.organism_id.ToString());
            form.AddField("start_stage", AttachedDataset.start_stage);
            form.AddField("end_stage", AttachedDataset.end_stage);
            UnityWebRequest www = UnityWebRequests.Post(SetsManager.instance.urlAniseed, "", form);
            //MorphoDebug.Log (Instantiation.urlAniseed + "?nbcellsmutants="+g.id.ToString()+"&organism_id=" + organism_id.ToString () + "&start_stage=" + Instantiation.start_stage + "&end_stage=" + Instantiation.end_stage);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                MorphoDebug.Log("Error : " + www.error);
            }
            else
            {
                //MorphoDebug.Log ("queryNbCellsbyMutant="+www.downloadHandler.text);
                var N = JSONNode.Parse(www.downloadHandler.text);
                if (N != null)
                {
                    Dictionary<int, int> CellsForMutant = new Dictionary<int, int>();
                    for (int i = 0; i < N.Count; i++)
                    {
                        int id_mutant = int.Parse(N[i][0].ToString().Replace('"', ' ').Trim());
                        int nbcells = int.Parse(N[i][1].ToString().Replace('"', ' ').Trim());
                        CellsForMutant[id_mutant] = nbcells;
                        //MorphoDebug.Log("Keep " + id_mutant + " with " + nbcells + " cells");
                    }
                    if (which == "selection")
                    {
                        foreach (Mutant mn in Mutants)
                        {
                            if (CellsForMutant.ContainsKey(mn.id))
                                mn.go.transform.Find(which).gameObject.SetActive(true);
                            else
                                mn.go.transform.Find(which).gameObject.SetActive(false);
                        }
                    }
                    else
                    {
                        foreach (Mutant mn in Mutants)
                        {
                            if (CellsForMutant.ContainsKey(mn.biomaterial_id))
                                mn.go.transform.Find(which).gameObject.SetActive(true);
                            else
                                mn.go.transform.Find(which).gameObject.SetActive(false);
                        }
                    }
                }
            }
            www.Dispose();
        }

        //ACTIVE MUTANT
        public void activeMutantListen(String which, Toggle b, Mutant mn)
        { b.onValueChanged.AddListener(delegate { activeMutant(which, mn); }); }

        /*public static void activeMutant(String which,Mutant mutant) {
            String waswhich="";
            if(MutantActif!=null){
                Toggle ton = MutantActif.go.transform.Find("selection").gameObject.GetComponent<Toggle>();
                if (ton.isOn)
                {
                    waswhich="selection";
                    ton.onValueChanged.RemoveAllListeners();
                    ton.isOn = false;
                    activeMutantListen("selection",ton, MutantActif);
                }
                Toggle tonWT = MutantActif.go.transform.Find("selectionWT").gameObject.GetComponent<Toggle>();
                if (tonWT.isOn)
                {
                    waswhich = "selectionWT";
                    tonWT.onValueChanged.RemoveAllListeners();
                    tonWT.isOn = false;
                    activeMutantListen("selectionWT",tonWT, MutantActif);
                }
            }
            //if (MutantActif != mutant || (MutantActif == mutant && which!=waswhich)) {
                Instantiation.canvas.GetComponent<Instantiation>().StartCoroutine(Mutation.queryCells(which,mutant));
                Toggle to = mutant.go.transform.Find(which).gameObject.GetComponent<Toggle>();
                to.onValueChanged.RemoveAllListeners();
                to.isOn = true;
                activeMutantListen(which,to, mutant);
                MutantActif = mutant;
            //}
        }*/

        public void activeMutant(String which, Mutant mutant)
        {
            Toggle to = mutant.go.transform.Find(which).gameObject.GetComponent<Toggle>();
            if (to.isOn)
            {
                if (mutant.CellsMutant == null)
                    StartCoroutine(queryCells(which, mutant));
                else
                    mutant.active();
                to.onValueChanged.RemoveAllListeners();
                to.isOn = true;
                activeMutantListen(which, to, mutant);
                MutantActif = mutant;
            }
        }

        public IEnumerator queryCells(String which, Mutant mn)
        {
            mn.go.transform.Find(which).gameObject.SetActive(false);
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("mutantcells", mn.id.ToString());
            if (which == "selectionWT")
                form.AddField("wildtype", mn.biomaterial_id.ToString());
            form.AddField("organism_id", AttachedDataset.GeneticManager.organism_id.ToString());
            form.AddField("start_stage", AttachedDataset.start_stage);
            form.AddField("end_stage", AttachedDataset.end_stage);
            UnityWebRequest www = UnityWebRequests.Post(SetsManager.instance.urlAniseed, "", form);
            //MorphoDebug.Log (Instantiation.urlAniseed + "?mutantcells="+mn.id.ToString()+"&organism_id=" + Aniseed.organism_id.ToString () + "&start_stage=" + Instantiation.start_stage + "&end_stage=" + Instantiation.end_stage);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                MorphoDebug.Log("Error : " + www.error);
            }
            else
            {
                //MorphoDebug.Log(which +" -> queryCells=" + www.downloadHandler.text);
                if (www.downloadHandler.text != "[]")
                {
                    mn.CellsMutant = new Dictionary<Cell, float>();
                    var N = JSONNode.Parse(www.downloadHandler.text);
                    //MorphoDebug.Log ("Found " + N.Count + " Stages");
                    if (N.Count > 0)
                    {
                        int i = 0;

                        foreach (var key in N.Keys)
                        {
                            string stage = key.ToString().Trim();
                            //MorphoDebug.Log ("stage=" + stage);
                            string[] cellnamess = N[i].ToString().Replace('[', ' ').Replace(']', ' ').Replace('"', ' ').Trim().Split(',');

                            Vector2 hpfb = AttachedDataset.GeneticManager.getHPFBoundaries(stage);
                            //MorphoDebug.Log ("Found hpfb.x=" + hpfb.x + " hpfb.y=" + hpfb.y);
                            int minTime = (int)Mathf.Floor((hpfb.x * 3600.0f - AttachedDataset.spf) / AttachedDataset.dt);
                            int maxTime = (int)(Mathf.Floor((hpfb.y * 3600.0f - AttachedDataset.spf) / AttachedDataset.dt) + 1);
                            //MorphoDebug.Log (stage+"-> found MinTime=" + minTime + " MaxTime=" + maxTime);
                            if (minTime < AttachedDataset.MinTime)
                                minTime = AttachedDataset.MinTime;
                            if (maxTime > AttachedDataset.MaxTime)
                                maxTime = AttachedDataset.MaxTime;
                            //MorphoDebug.Log (stage+"-> found MinTime=" + minTime + " MaxTime=" + maxTime);

                            List<string> cellnamesConverted = new List<string>();
                            foreach (string cn in cellnamess)
                            {
                                string cnc = AttachedDataset.GeneticManager.convertCellName(cn);
                                if (cnc != "" && !cellnamess.Contains(cnc))
                                {
                                    cellnamesConverted.Add(cnc);
                                }
                            }

                            for (int t = minTime; t <= maxTime; t++)
                            {
                                foreach (Cell cell in AttachedDataset.CellsByTimePoint[t])
                                {
                                    string cn = cell.getInfos(AttachedDataset.GeneticManager.idxInfoxName);
                                    if (cellnamesConverted.Contains(cn))
                                    {
                                        if (!mn.CellsMutant.ContainsKey(cell))
                                        {
                                            mn.CellsMutant[cell] = 1f;
                                        }
                                    }
                                }
                            }
                            i += 1;
                        }
                        mn.active();
                    }
                }
            }
            www.Dispose();
            mn.go.transform.Find(which).gameObject.SetActive(true);
        }

        public void resetExpression()
        {
            //Desactive all
            foreach (Mutant mn in Mutants)
            {
                Toggle ton = mn.go.transform.Find("selection").gameObject.GetComponent<Toggle>();
                if (ton.isOn)
                {
                    ton.onValueChanged.RemoveAllListeners();
                    ton.isOn = false;
                    activeMutantListen("selection", ton, mn);
                }
                Toggle tonWT = mn.go.transform.Find("selectionWT").gameObject.GetComponent<Toggle>();
                if (tonWT.isOn)
                {
                    tonWT.onValueChanged.RemoveAllListeners();
                    tonWT.isOn = false;
                    activeMutantListen("selectionWT", tonWT, mn);
                }
            }

            for (int t = AttachedDataset.MinTime; t <= AttachedDataset.MaxTime; t++)
            {
                foreach (Cell cell in AttachedDataset.CellsByTimePoint[t])
                {
                    cell.removeSelection(geneMutate.selection);
                }
            }
        }
    }
}