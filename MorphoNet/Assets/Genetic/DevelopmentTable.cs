﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using System.Globalization;

namespace MorphoNet
{
    public class DevelopmentTable : MonoBehaviour
    {
        public DataSet AttachedDataset;

        public void AttachDataSet(DataSet source)
        {
            AttachedDataset = source;
        }

        //Download Developmental Table
        public Dictionary<int, List<Dvpt>> Dvpts = new Dictionary<int, List<Dvpt>>();

        public class Dvpt
        {
            public string period;
            public string stage;
            public string developmentaltstage;
            public string description;
            public string hpf;
            public string hatch;

            public Dvpt(string period, string stage, string developmentaltstage, string description, string hpf, string hatch) //Constructeur
            {
                this.period = period;
                this.stage = stage;
                this.developmentaltstage = developmentaltstage;
                this.description = description;
                this.hpf = hpf;
                this.hatch = hatch;
            }
        }

        public string getStage(int t)
        {
            if (AttachedDataset.dt > 0 && AttachedDataset.spf > 0)
            {
                //MorphoDebug.Log("getStage : if");
                return convertHPFtoStage(convertTimetoHPF(t));
            }
            else
            {
                //MorphoDebug.Log("getStage : else");
                if (AttachedDataset.mesh_by_time[t] != null && AttachedDataset.mesh_by_time[t].name.Contains("Stage"))
                    return AttachedDataset.mesh_by_time[t].name.ToLower().Replace("stage", "").Trim();
            }
            return "";
        }

        public int convertHPFtoTime(float HPF)
        {
            return (int)Mathf.Round((HPF * 3600.0f - AttachedDataset.spf) / AttachedDataset.dt);
        }

        public float convertTimetoHPF(int t)
        {
            return (float)(AttachedDataset.spf + t * AttachedDataset.dt) / 3600f;
        }

        public string convertHPFtoStage(float HPF)
        {
            //MorphoDebug.Log("ConvertHPFtoStage : " + HPF);
            string prevStage = "";
            if (Dvpts.ContainsKey(AttachedDataset.id_type))
            {
                foreach (Dvpt dvpt in Dvpts[AttachedDataset.id_type])
                { //Dvpt are orderer in time !
                    CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    ci.NumberFormat.CurrencyDecimalSeparator = ".";
                    float dvptHPF;
                    float.TryParse(dvpt.hpf, NumberStyles.Any, ci, out dvptHPF);
                    if (HPF == dvptHPF)
                        return dvpt.stage;
                    else if (HPF < dvptHPF)
                        return prevStage;
                    prevStage = dvpt.stage;
                }
            }
            //  MorphoDebug.Log("Return empty");
            return "";
        }

        public float convertHPFToHatch(float HPF)
        {
            //Lookfor min max
            float MaxHatch = 0;
            float MaxHPF = 0;
            CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            ci.NumberFormat.CurrencyDecimalSeparator = ".";

            foreach (Dvpt dvpt in Dvpts[AttachedDataset.id_type])
            {
                float dvpthatch = 0;
                if (float.TryParse(dvpt.hatch, NumberStyles.Any, ci, out dvpthatch))
                    if (dvpthatch > MaxHatch)
                    {
                        float.TryParse(dvpt.hatch, NumberStyles.Any, ci, out MaxHatch);
                        float.TryParse(dvpt.hpf, NumberStyles.Any, ci, out MaxHPF);
                    }
            }
            //MorphoDebug.Log ("HPF=" + HPF + " *MaxHatch=" + MaxHatch + " / MaxHPF=" + MaxHPF);
            return HPF * MaxHatch / MaxHPF;
        }

        public float HatchLength = 9f;

        public void updateDatasetStages()
        {
            //MorphoDebug.Log("updateDatasetStages -> Dataset id type: " + AttachedDataset.id_type +" -> with dt=" + AttachedDataset.dt + " and spf=" + AttachedDataset.spf);
            if (AttachedDataset.dt > 0 && AttachedDataset.spf > 0)
            {
                AttachedDataset.start_stage = "Stage " + getStage(AttachedDataset.MinTime);
                AttachedDataset.end_stage = "Stage " + getStage(AttachedDataset.MaxTime);
            }
            else
            {   //Embryo Split by Stage (for ANISEED) where gameobject embryo name correspond to stage
                if (AttachedDataset.mesh_by_time[AttachedDataset.MinTime] != null && AttachedDataset.mesh_by_time[AttachedDataset.MinTime].name.Contains("Stage"))
                    AttachedDataset.start_stage = AttachedDataset.mesh_by_time[AttachedDataset.MinTime].name;
                else
                    AttachedDataset.start_stage = "Stage 5a"; //Default Value if meshes not yet loaded
                if (AttachedDataset.mesh_by_time[AttachedDataset.MaxTime] != null && AttachedDataset.mesh_by_time[AttachedDataset.MaxTime].name.Contains("Stage"))
                    AttachedDataset.end_stage = AttachedDataset.mesh_by_time[AttachedDataset.MaxTime].name;
                else
                    AttachedDataset.end_stage = "Stage 17"; //Default Value if meshes not yet loaded
            }
        }

        public IEnumerator downloadCellFromTerritory(string territory, string search_stage)
        {
            //MorphoDebug.Log("downloadCellFromTerritory " + territory + " at " + search_stage);
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("territory", territory);
            if (search_stage == null || search_stage.Trim() != "")
            {
                form.AddField("stage", search_stage);
            }
            else
            {
                if (AttachedDataset.start_stage == null)
                    updateDatasetStages();
                form.AddField("start_stage", AttachedDataset.start_stage);
                form.AddField("end_stage", AttachedDataset.end_stage);
            }
            UnityWebRequest www = UnityWebRequests.Get(MorphoTools.GetServer(), "api/partialdevlineage/?hash=" + SetsManager.instance.hash + "&territory=" + territory + (search_stage == null || search_stage.Trim() != "" ? "&stage=" + search_stage : "&start_stage=" + AttachedDataset.start_stage + "&end_stage=" + AttachedDataset.end_stage));
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                MorphoDebug.Log("Error : " + www.error + " with " + www.responseCode);
            }
            else
            {
                if (www.downloadHandler.text != "")
                {
                    int idxInfoxName = AttachedDataset.Infos.getIdInfosForName();
                    var N = JSONNode.Parse(www.downloadHandler.text);
                    for (int i = 0; i < N.Count; i++)
                    {
                        string stage = UtilsManager.convertJSON(N[i]["stage"]);
                        string[] cells = UtilsManager.convertJSON(N[i]["cells"]).Split(',');
                        // MorphoDebug.Log("at " + stage + "->" +cells);

                        int minTime = -1;
                        int maxTime = -1;
                        if (AttachedDataset.dt != -1 && AttachedDataset.spf != -1)
                        {
                            //MorphoDebug.Log (" -> stage=" + stage);
                            Vector2 hpfb = AttachedDataset.GeneticManager.getHPFBoundaries(stage);
                            //MorphoDebug.Log ("Found hpfb.x=" + hpfb.x + " hpfb.y=" + hpfb.y);
                            minTime = (int)Mathf.Floor((hpfb.x * 3600.0f - AttachedDataset.spf) / AttachedDataset.dt);
                            maxTime = (int)(Mathf.Floor((hpfb.y * 3600.0f - AttachedDataset.spf) / AttachedDataset.dt) + 1);
                            //MorphoDebug.Log (stage+"-> found MinTime=" + minTime + " MaxTime=" + maxTime);
                        }
                        else
                        { //Embryo Split by Stage (for ANISEED) where gameobject embryo name correspond to stage
                            for (int t = AttachedDataset.MinTime; t <= AttachedDataset.MaxTime; t++)
                                if (AttachedDataset.mesh_by_time[t] != null && AttachedDataset.mesh_by_time[t].name == stage)
                                { minTime = t; maxTime = t; }
                        }

                        if (minTime != -1 && maxTime != -1)
                        {
                            if (minTime < AttachedDataset.MinTime)
                                minTime = AttachedDataset.MinTime;
                            if (maxTime > AttachedDataset.MaxTime)
                                maxTime = AttachedDataset.MaxTime;
                            //MorphoDebug.Log(stage + "-> found MinTime=" + minTime + " MaxTime=" + maxTime);

                            List<string> cellnamesConverted = new List<string>();
                            foreach (string cell_name in cells)
                            {
                                string cnc = AttachedDataset.GeneticManager.convertCellName(cell_name); //Convert the cell name format
                                if (!cellnamesConverted.Contains(cnc))
                                    cellnamesConverted.Add(cnc);
                                //MorphoDebug.Log(cell_name + " -> " + cnc);
                            }

                            for (int t = minTime; t <= maxTime; t++)
                            {
                                if (t != AttachedDataset.CurrentTime)
                                    InterfaceManager.instance.setTime(t);

                                foreach (Cell cell in AttachedDataset.CellsByTimePoint[t])
                                {
                                    string infos = cell.getInfos(idxInfoxName); //Get the cell name
                                    if (cellnamesConverted.Contains(infos))
                                    {
                                        AttachedDataset.PickedManager.AddCellSelected(cell);
                                    }
                                }
                            }
                        }
                    }
#if !UNITY_WEBGL
                    MorphoTools.SendCellsPickedToLineage();
#endif
                }
            }
            www.Dispose();
        }
    }
}