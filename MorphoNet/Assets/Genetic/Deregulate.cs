﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using System.Collections.Generic;
using AssemblyCSharp;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using System;

namespace MorphoNet
{
    //Main mutation classes
    public class Deregulate : MonoBehaviour
    {
        public static GameObject dere;

        public void Start()
        {
        }

        public static List<GameObject> Deregulations;

        public static IEnumerator queryDeregulations(Mutant mn)
        {
            MorphoTools.GetMutations().MenuDeregulations.SetActive(false);
            // Mutation.MenuDeregulations.transform.Find("DeregulationsFor").gameObject.GetComponent<Text>().text = g.name;
            WWWForm form = new WWWForm();
            form.AddField("hash", SetsManager.instance.hash);
            form.AddField("listderegulations", mn.id);
            UnityWebRequest www = UnityWebRequests.Post(SetsManager.instance.urlAniseed, "", form);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                //MorphoDebug.Log("www.downloadHandler.text="+www.downloadHandler.text);
                if (www.downloadHandler.text != "") //DEREGULATIONs ID
                {
                    MorphoTools.GetMutations().MenuDeregulations.SetActive(true);
                    var N = JSONNode.Parse(www.downloadHandler.text);
                    if (N != null && N.Count > 0)
                    {
                        //Delete Previous Deregulations
                        if (Deregulations != null)
                        {
                            foreach (GameObject dere in Deregulations)
                                Destroy(dere);
                            Deregulations.Clear();
                        }
                        else
                            Deregulations = new List<GameObject>();
                        GameObject DeregulationsList = MorphoTools.GetMutations().MenuDeregulations.transform.Find("Deregulations").gameObject;
                        GameObject template = DeregulationsList.transform.Find("DeregulationTemplate").gameObject;
                        template.SetActive(false);
                        for (int i = 0; i < N.Count; i++)
                        {
                            JSONNode NJ = N[i];
                            int id_dere = int.Parse(NJ["id"].ToString().Replace('"', ' ').Trim());
                            string moltool_name = NJ["moltool_name"].ToString().Replace('"', ' ').Trim();
                            string regulation_type = NJ["regulation_type"].ToString().ToLower().Replace('"', ' ').Trim();
                            string target_feature = NJ["target_feature"].ToString().Replace('"', ' ').Trim();

                            GameObject dere = (GameObject)Instantiate(template, DeregulationsList.transform);
                            dere.name = "DERE_" + id_dere.ToString();
                            dere.transform.Find("moltool_name").gameObject.GetComponent<Text>().text = moltool_name;
                            dere.transform.Find("target_feature").gameObject.GetComponent<Text>().text = target_feature;
                            if (regulation_type.IndexOf("loss") >= 0)
                                dere.transform.Find("loss").gameObject.SetActive(true);
                            else if (regulation_type.IndexOf("gain") >= 0)
                                dere.transform.Find("gain").gameObject.SetActive(true);
                            else
                                MorphoDebug.Log("Unknow regulation_type " + regulation_type);
                            dere.transform.position = new Vector3(dere.transform.position.x, template.transform.position.y - Deregulations.Count() * MorphoTools.GetMutations().spaceY * InterfaceManager.instance.canvasScale, dere.transform.position.z);
                            dere.SetActive(true);
                            Deregulations.Add(dere);
                        }
                        //Resize the menu
                        RectTransform dere_rect = MorphoTools.GetMutations().MenuDeregulations.transform.GetComponent<RectTransform>();
                        dere_rect.sizeDelta = new Vector2(dere_rect.rect.width, 50f + Deregulations.Count * MorphoTools.GetMutations().spaceY);
                        MorphoTools.GetMutations().MenuDeregulations.transform.position = new Vector3(MorphoTools.GetMutations().MenuDeregulations.transform.position.x, mn.go.transform.position.y, MorphoTools.GetMutations().MenuDeregulations.transform.position.z);
                    }
                }
            }
            www.Dispose();
            // Mutation.updateScrol();
            //Instantiation.canvas.GetComponent<Instantiation>().StartCoroutine(Mutation.queryNbCellsbyMutant(g));
        }
    }
}