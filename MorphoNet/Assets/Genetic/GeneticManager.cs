﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using System.Collections.Generic;
using AssemblyCSharp;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using System;
using System.Globalization;
using System.Diagnostics;

namespace MorphoNet
{
    public class Gene
    {
        public GeneticManager parent;
        public GameObject go;
        public GameObject SubGene;
        public int id;
        public string name;
        public string unique_id;
        public int selection;
        public Dictionary<Cell, float> Cells;
        public bool isActive;
        public bool isDownload;
        public bool isSelected;
        public int nbCells;
        public bool isQuantitative;
        public Correspondence cor; //For genetic file, the associated correspondence

        public Gene(int id, string name, string unique_id, GeneticManager source)
        {
            this.Cells = null;
            this.id = id;
            this.name = name;
            this.unique_id = unique_id;
            this.isActive = false;
            this.isDownload = false;
            this.isSelected = true;
            this.nbCells = -1;
            this.isQuantitative = false;
            this.cor = null;
            this.parent = source;
        }

        #region Constants

        private const string MAIN_COLOR = "Main Color";

        private const string HALO_COLOR = "Halo Color";
        private const string SECOND_COLOR = "Second Color";
        private const string METALLIC = "Metallic";
        private const string SMOOTHNESS = "Smoothness";
        private const string TRANSPARENCY = "Transparency";

        #endregion Constants

        public void init(int shiftV)
        {
            this.go = (GameObject)GameObject.Instantiate(parent.GeneTemplate, parent.GeneTemplate.transform.parent);
            //this.name=this.name;
            this.go.name = this.name;
            this.setName();
            this.selection = shiftV + 1;
            while (this.selection > 254)
                this.selection -= 254;//Mathf.RoundToInt(Mathf.Repeat(shift+1, 254))+1;;

            this.SubGene = this.go.transform.Find("SubGene").gameObject;
            SubGene.SetActive(false);

            this.SubGene.transform.Find("SelectionValue").gameObject.GetComponent<InputField>().text = this.selection.ToString();
            //this.go.transform.Find ("selection").gameObject.SetActive (false);
            setSelectionRendering();

            this.go.transform.Find("annotation").gameObject.SetActive(false); //FOR MASSIVE CURRATION

            parent.activeListen(this.go.transform.Find("selection").gameObject.GetComponent<Toggle>(), this);
            parent.sliderListen(this.SubGene.transform.Find("Slider").gameObject.GetComponent<Slider>(), this);

            if (id != -1)
            {
                this.SubGene.transform.Find("genecard").gameObject.SetActive(true);
                parent.openLinkListen(this.SubGene.transform.Find("genecard").gameObject.GetComponent<Button>(), this);
                parent.AttachedDataset.Mutations.listMutantsListen(this.SubGene.transform.Find("mutant").gameObject.GetComponent<Button>(), this);
            }
            else
            {
                this.SubGene.transform.Find("genecard").gameObject.SetActive(false);
                this.SubGene.transform.Find("mutant").gameObject.SetActive(false);
            }
            parent.showSubGeneListen(this.go.transform.Find("deploy").gameObject.GetComponent<Button>(), this);
            parent.editSelectionListen(this.SubGene.transform.Find("SelectionValue").gameObject.GetComponent<InputField>(), this);

            parent.changeColorBarListen(this.SubGene.transform.Find("Quantitative").gameObject.GetComponent<Button>(), this);
            parent.changeColorMapListen(this.SubGene.transform.Find("Quantitative").gameObject.transform.Find("execute").gameObject.GetComponent<Button>(), this);
            //add remove colormap listener
            SubGene.transform.Find("removeMap").gameObject.GetComponent<Button>().onClick.AddListener(() => RevertToSelectionID());
        }

        public void showSubGene()
        {
            if (this.SubGene.activeSelf)
                this.SubGene.SetActive(false);
            else
                this.SubGene.SetActive(true);
            parent.reOrganize();
        }

        public void addExpression(Cell c, float expression)
        {
            if (this.Cells == null)
                this.Cells = new Dictionary<Cell, float>();
            this.Cells[c] = expression;
            this.nbCells = this.Cells.Count();
            this.setName();
        }

        public void removeExpression(Cell c)
        {
            if (this.Cells != null && this.Cells.ContainsKey(c))
                this.Cells.Remove(c);
        }

        //SELECTION
        public void editSelection()
        {
            int previousSelection = this.selection;
            string s = this.SubGene.transform.Find("SelectionValue").gameObject.GetComponent<InputField>().text;
            //MorphoDebug.Log ("Edit " + s);
            int ns;
            if (int.TryParse(s, out ns))
            {
                if (ns > 254)
                    ns = 254;
                this.selection = ns;
            }
            this.SubGene.transform.Find("SelectionValue").gameObject.GetComponent<InputField>().text = this.selection.ToString();
            if (this.isActive && this.isQuantitative)
                this.inActiveQuantitative();

            if (this.isActive && previousSelection != this.selection)
            {
                foreach (var ce in this.Cells)
                {
                    Cell c = ce.Key;
                    c.removeSelection(previousSelection);
                    c.addSelection(this.selection);
                }
            }
            this.SubGene.transform.Find("Slider").gameObject.GetComponent<Slider>().value = this.selection;
        }

        public void slider()
        {
            if (this.isActive && this.isQuantitative)
                this.inActiveQuantitative();
            int previousSelection = this.selection;
            this.selection = Mathf.RoundToInt(this.SubGene.transform.Find("Slider").gameObject.GetComponent<Slider>().value);
            this.SubGene.transform.Find("SelectionValue").gameObject.GetComponent<InputField>().text = this.selection.ToString();
            this.setSelectionRendering();
            if (this.isActive && previousSelection != this.selection)
            {
                foreach (var ce in this.Cells)
                {
                    Cell c = ce.Key;
                    c.removeSelection(previousSelection);
                    c.addSelection(this.selection);
                }
            }
        }

        public void setSelectionRendering()
        {
            this.go.transform.Find("selection").transform.Find("Background").gameObject.GetComponent<Image>().sprite = null;
            this.go.transform.Find("selection").transform.Find("Background").gameObject.GetComponent<Image>().color = SelectionManager.getSelectedMaterial(this.selection).color;
        }

        //QUANTITATIVE
        public void inActiveQuantitative()
        {
            if (this.isActive && this.isQuantitative)
            {
                this.removeQuantitative();
                GameObject buttonToColorize = this.SubGene.transform.Find("Quantitative").gameObject;
                buttonToColorize.GetComponentInChildren<Text>().text = "None";
                buttonToColorize.GetComponent<Image>().sprite = null;
            }
            this.isQuantitative = false;
        }

        public void removeQuantitative()
        {
            foreach (var cn in this.Cells)
            {
                Cell c = cn.Key;
                c.UploadedColor = false;
                c.updateShader();
            }
        }

        //When we click on the button
        public void changeColorBar()
        {
            if (InterfaceManager.instance.MenuColorBar.activeSelf)
            {
                MenuColorBar.instance.cancel();
            }

            //MorphoDebug.Log("changeColorBar "+ parent.AttachedDataset.infos.MenuBarColor.activeSelf);
            MenuColorBar.instance.buttonToColorize = this.SubGene.transform.Find("Quantitative").gameObject;
            int type = int.Parse(MenuColorBar.instance.buttonToColorize.transform.Find("type").gameObject.GetComponentInChildren<Text>().text);
            float MinV = 0f;
            float MaxV = 1f;

            parent.AttachedDataset.Infos.MenuBarColor.SetActive(true);
            parent.AttachedDataset.Infos.MenuBarColor.transform.SetParent(InterfaceManager.instance.MenuGenetic.transform, false);
            MenuColorBar.instance.AssignValues(type, int.Parse(MenuColorBar.instance.buttonToColorize.transform.Find("min").gameObject.GetComponentInChildren<Text>().text), int.Parse(MenuColorBar.instance.buttonToColorize.transform.Find("max").gameObject.GetComponentInChildren<Text>().text), MinV, MaxV, this.name);
            MenuColorBar.instance.SetUpGenetic();
        }

        public void changeColorMap()
        {
            GameObject buttonToColorize = go.transform.Find("SubGene").Find("Quantitative").gameObject;
            string parameter = buttonToColorize.transform.Find("parameter").gameObject.GetComponentInChildren<Text>().text;
            string lastParameter = buttonToColorize.transform.Find("lastParameter").gameObject.GetComponentInChildren<Text>().text;
            int typecmap = int.Parse(buttonToColorize.transform.Find("type").gameObject.GetComponentInChildren<Text>().text);
            int minC = int.Parse(buttonToColorize.transform.Find("min").gameObject.GetComponentInChildren<Text>().text);
            int maxC = int.Parse(buttonToColorize.transform.Find("max").gameObject.GetComponentInChildren<Text>().text);
            float minV = MenuColorBar.instance.GetMinVal();
            float maxV = MenuColorBar.instance.GetMaxVal();
            float minVOther = MenuColorBar.instance.GetMinVOther();
            float maxVOther = MenuColorBar.instance.GetMaxVOther();

            if (parameter != lastParameter && !string.IsNullOrEmpty(lastParameter))
            {
                switch (lastParameter)
                {
                    case MAIN_COLOR:
                        CancelMainCmap();
                        break;

                    case HALO_COLOR:
                        CancelHaloCmap();
                        break;

                    case SECOND_COLOR:
                        CancelSecondCmap();
                        break;

                    case METALLIC:
                        CancelMetallicMap();
                        break;

                    case SMOOTHNESS:
                        CancelSmoothnessMap();
                        break;

                    case TRANSPARENCY:
                        CancelTransparencyMap();
                        break;

                    default:
                        break;
                }
            }

            switch (parameter)
            {
                case MAIN_COLOR:
                    if (typecmap > 0)
                    {
                        CancelSecondCmap();
                        ApplyMainCmap(typecmap, minC, maxC, minV, maxV);
                    }
                    else
                        CancelMainCmap();
                    break;

                case HALO_COLOR:
                    if (typecmap > 0)
                    {
                        CancelSecondCmap();
                        ApplyHaloCmap(typecmap, minC, maxC, minV, maxV);
                    }
                    else
                        CancelHaloCmap();
                    break;

                case SECOND_COLOR:
                    if (typecmap > 0)
                    {
                        ApplySecondCmap(typecmap, minC, maxC, minV, maxV);
                    }
                    else
                        CancelSecondCmap();
                    break;

                case METALLIC:
                    CancelSecondCmap();
                    ApplyMetallicMap(minV, maxV, minVOther, maxVOther);
                    break;

                case SMOOTHNESS:
                    CancelSecondCmap();
                    ApplySmoothnessMap(minV, maxV, minVOther, maxVOther);
                    break;

                case TRANSPARENCY:
                    CancelSecondCmap();
                    ApplyTransparencyMap(minV, maxV, minVOther, maxVOther);
                    break;

                default:
                    break;
            }
        }

        //Apply the changement
        public void applyCmap(int typecmap, int minC, int maxC, float minV, float maxV)
        {
            if (this.isActive && !this.isQuantitative)
            { //REMOVE OLD SELECTION
                foreach (var ce in this.Cells)
                {
                    Cell c = ce.Key;
                    c.removeSelection(this.selection);
                }
            }
            this.isQuantitative = true;
            if (this.isActive)
            {
                bool thresh = MenuColorBar.instance.ThresholdToggle.isOn;
                foreach (var cn in this.Cells)
                {
                    Cell c = cn.Key;
                    float v = cn.Value;
                    c.UploadMainColor(MenuColorBar.instance.GetColor(typecmap, v, minC, maxC, minV, maxV, thresh));
                }
            }
        }

        public void RevertToSelectionID()
        {
            this.isQuantitative = false;
            parent.resetAndClear();
            InteractionManager.instance.ClearAllColorsInfo();
        }

        public void ApplyMainCmap(int typecmap, int minC, int maxC, float minV, float maxV)
        {
            if (this.isActive && !this.isQuantitative)
            { //REMOVE OLD SELECTION
                foreach (var ce in this.Cells)
                {
                    Cell c = ce.Key;
                    c.removeSelection(this.selection);
                }
            }
            this.isQuantitative = true;
            if (this.isActive)
            {
                bool thresh = MenuColorBar.instance.ThresholdToggle.isOn;
                foreach (var cn in this.Cells)
                {
                    Cell c = cn.Key;
                    float v = cn.Value;
                    c.UploadMainColor(MenuColorBar.instance.GetColor(typecmap, v, minC, maxC, minV, maxV, thresh));
                }
            }
        }

        public void ApplyHaloCmap(int typecmap, int minC, int maxC, float minV, float maxV)
        {
            if (this.isActive && !this.isQuantitative)
            { //REMOVE OLD SELECTION
                foreach (var ce in this.Cells)
                {
                    Cell c = ce.Key;
                    c.removeSelection(this.selection);
                }
            }
            this.isQuantitative = true;
            if (this.isActive)
            {
                bool thresh = MenuColorBar.instance.ThresholdToggle.isOn;
                foreach (var cn in this.Cells)
                {
                    Cell c = cn.Key;
                    float v = cn.Value;
                    c.UploadHaloColor(MenuColorBar.instance.GetColor(typecmap, v, minC, maxC, minV, maxV, thresh));
                }
            }
        }

        public void ApplySecondCmap(int typecmap, int minC, int maxC, float minV, float maxV)
        {
            if (this.isActive && !this.isQuantitative)
            { //REMOVE OLD SELECTION
                foreach (var ce in this.Cells)
                {
                    Cell c = ce.Key;
                    c.removeSelection(this.selection);
                }
            }
            this.isQuantitative = true;
            if (this.isActive)
            {
                bool thresh = MenuColorBar.instance.ThresholdToggle.isOn;
                foreach (var cn in this.Cells)
                {
                    Cell c = cn.Key;
                    float v = cn.Value;
                    c.UploadSecondColor(MenuColorBar.instance.GetColor(typecmap, v, minC, maxC, minV, maxV, thresh));
                }
            }
        }

        public void ApplyMetallicMap(float minV, float maxV, float minVOther, float maxVOther)
        {
            if (this.isActive && !this.isQuantitative)
            { //REMOVE OLD SELECTION
                foreach (var ce in this.Cells)
                {
                    Cell c = ce.Key;
                    c.removeSelection(this.selection);
                }
            }
            this.isQuantitative = true;
            if (this.isActive)
            {
                bool thresh = MenuColorBar.instance.ThresholdToggle.isOn;
                foreach (var cn in this.Cells)
                {
                    Cell c = cn.Key;
                    float v = cn.Value;
                    float val = minVOther + (maxVOther - minVOther) * (v - minV) / (maxV - minV);
                    if (thresh)
                        if (val > maxV || val < minV)
                            val = 0f;
                    c.UploadMetallic(val);
                }
            }
        }

        public void ApplySmoothnessMap(float minV, float maxV, float minVOther, float maxVOther)
        {
            if (this.isActive && !this.isQuantitative)
            { //REMOVE OLD SELECTION
                foreach (var ce in this.Cells)
                {
                    Cell c = ce.Key;
                    c.removeSelection(this.selection);
                }
            }
            this.isQuantitative = true;
            if (this.isActive)
            {
                bool thresh = MenuColorBar.instance.ThresholdToggle.isOn;
                foreach (var cn in this.Cells)
                {
                    Cell c = cn.Key;
                    float v = cn.Value;
                    float val = minVOther + (maxVOther - minVOther) * (v - minV) / (maxV - minV);
                    if (thresh)
                        if (val > maxV || val < minV)
                            val = 0f;
                    c.UploadSmoothness(val);
                }
            }
        }

        public void ApplyTransparencyMap(float minV, float maxV, float minVOther, float maxVOther)
        {
            if (this.isActive && !this.isQuantitative)
            { //REMOVE OLD SELECTION
                foreach (var ce in this.Cells)
                {
                    Cell c = ce.Key;
                    c.removeSelection(this.selection);
                }
            }
            this.isQuantitative = true;
            if (this.isActive)
            {
                bool thresh = MenuColorBar.instance.ThresholdToggle.isOn;
                foreach (var cn in this.Cells)
                {
                    Cell c = cn.Key;
                    float v = cn.Value;
                    float val = minVOther + (maxVOther - minVOther) * (v - minV) / (maxV - minV);
                    c.UploadTransparency(val);
                }
            }
        }

        //Reset Upload Color
        public void cancelCmap()
        {
            this.isQuantitative = false;
            if (this.isActive)
                removeQuantitative();
            setSelectionRendering();
        }

        //for many colormap options :
        //Reset Upload Main Color
        public void CancelMainCmap()
        {
            for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
            {
                foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                {
                    cell.UploadMainColor(Color.white);
                    cell.updateShader();
                }
            }
        }

        //Reset Upload Halo Color
        public void CancelHaloCmap()
        {
            for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
            {
                foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                {
                    cell.UploadedHaloColor = false;
                    cell.updateShader();
                }
            }
        }

        //Reset Upload Second Color
        public void CancelSecondCmap()
        {
            for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
            {
                foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                {
                    cell.UploadedSecondColor = false;
                    cell.updateShader();
                }
            }
        }

        //Reset Upload Metallic
        public void CancelMetallicMap()
        {
            for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
            {
                foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                {
                    cell.UploadedMetallicParameter = false;
                    cell.updateShader();
                }
            }
        }

        //Reset Upload Smoothness
        public void CancelSmoothnessMap()
        {
            for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
            {
                foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                {
                    cell.UploadedSmoothnessParameter = false;
                    cell.updateShader();
                }
            }
        }

        //Reset Upload Transparency
        public void CancelTransparencyMap()
        {
            for (int t = parent.AttachedDataset.MinTime; t <= parent.AttachedDataset.MaxTime; t++)
            {
                foreach (Cell cell in parent.AttachedDataset.CellsByTimePoint[t])
                {
                    cell.UploadedTransparencyParameter = false;
                    cell.updateShader();
                }
            }
        }

        public void setNbCells(int nb)
        {
            this.nbCells = nb;
            this.setName();
        }

        public void setName()
        {
            string n = this.name;
            if (this.nbCells >= 0)
                n = "(" + this.nbCells + ")" + n;
            this.go.transform.Find("GeneName").gameObject.GetComponent<Text>().text = n;
        }

        public void download()
        {
            //if(this.Cells!=null) this.setName("(" + this.Cells.Count + ") "+ this.name);
            //else this.setName("(0) "+ this.name);
            this.go.transform.Find("selection").gameObject.SetActive(true);
            this.isDownload = true;
        }

        public void changeActive()
        {
            if (!this.isActive)
                this.active();
            else
                this.inActive();
        }

        public void active()
        {
            if (!this.isDownload)
                parent.StartCoroutine(DataLoader.instance.queryGeneAnissed(this, parent.AttachedDataset));
            else
            {
                this.isActive = true;
                if (parent.isUI == 0)
                {
                    if (this.Cells != null)
                    {
                        if (!this.isQuantitative)
                        { //SELECTION
                            foreach (var cn in this.Cells)
                            {
                                Cell c = cn.Key;

                                c.addSelection(this.selection);
                                float v = cn.Value;
                                if (v >= 0 && v < 1)
                                    c.UploadColor = new Color(0, 0, 0, v); //TRANSPARENCY
                                else if (c.UploadColor.a != 1f)
                                    c.UploadColor = new Color(c.UploadColor.r, c.UploadColor.g, c.UploadColor.b, 1); //TO RESET
                            }
                        }
                        else
                        { //QUANTITATIVE
                            this.changeColorMap();
                        }
                    }
                }
                else
                {
                    parent.removeUI();
                    parent.computeCellsUI();
                }
            }
        }

        public void inActive()
        {
            this.isActive = false;
            if (parent.isUI == 0)
            {
                if (!this.isQuantitative)
                { //SELECTION
                    if (this.Cells != null)
                        foreach (var ce in this.Cells)
                        {
                            Cell c = ce.Key;
                            c.removeSelection(this.selection);
                            float v = ce.Value;
                            if (v >= 0 && v < 1)
                                c.UploadColor = new Color(c.UploadColor.r, c.UploadColor.g, c.UploadColor.b, 1); //TO RESET
                        }
                }
                else
                    this.removeQuantitative();
            }
            else
            {
                parent.removeUI();
                parent.computeCellsUI();
            }
        }

        public void select(int shiftV)
        {
            this.shift(shiftV, 0);
            this.isSelected = true;
        }

        public void shift(int shiftV, int decals)
        {
            if (shiftV >= decals && shiftV <= decals + parent.maxNbDraw)
            {
                this.go.SetActive(true);
                this.go.transform.localPosition = new Vector3(this.go.transform.localPosition.x, parent.initialY - (shiftV - decals) * parent.spaceY, this.go.transform.localPosition.z);
            }
            else
                this.go.SetActive(false);
        }

        public void unSelect()
        {
            this.go.SetActive(false);
            this.isSelected = false;
        }

        public void openLink()
        {
            //MorphoDebug.Log ("Open " + this.unique_id);
            //Change show_gene by show_expression to get expression
            Application.ExternalEval("window.open(\"https://www.aniseed.cnrs.fr/aniseed/gene/show_gene?unique_id=" + this.unique_id + "\")");
        }

        public void annotate()
        {
            if (this.Cells == null)
                this.Cells = new Dictionary<Cell, float>();
            GameObject annotation = this.go.transform.Find("annotation").gameObject;
            bool v = annotation.transform.Find("annotate").gameObject.GetComponent<Toggle>().isOn;
            if (v)
            { //Activate Cell
                foreach (Cell cc in parent.AttachedDataset.PickedManager.clickedCells)
                    if (!this.Cells.ContainsKey(cc))
                    {
                        this.Cells[cc] = 1f;
                        parent.StartCoroutine(parent.AttachedDataset.Curations.addCuration(this.cor, cc, "1"));
                    }
                annotation.transform.Find("nbTrue").gameObject.GetComponent<Text>().text = "" + parent.AttachedDataset.PickedManager.clickedCells.Count.ToString() + "/" + parent.AttachedDataset.PickedManager.clickedCells.Count.ToString();
            }
            else
            { //Inactive Cell
              //Dictionary<Cell,float> TempCells = new Dictionary<Cell,float>  ();
                foreach (Cell cc in parent.AttachedDataset.PickedManager.clickedCells)
                    if (this.Cells.ContainsKey(cc))
                    {
                        this.Cells.Remove(cc);
                        parent.StartCoroutine(parent.AttachedDataset.Curations.addCuration(this.cor, cc, "0"));
                    }

                /*foreach (var cf in this.Cells) {
                    Cell cc = cf.Key;
                    if (!SelectionCell.clickedCells.Contains (cc)) {
                        TempCells [cc] = cf.Value;
                    }else
                        Instantiation.canvas.GetComponent<Instantiation> ().StartCoroutine(MenuCurrated.addCuration (this.cor,cc,"0"));
                }
                this.Cells=TempCells;*/
                annotation.transform.Find("nbTrue").gameObject.GetComponent<Text>().text = "0/" + parent.AttachedDataset.PickedManager.clickedCells.Count.ToString();
            }
        }
    }

    public class GeneticManager : MonoBehaviour
    {
        //GENE CARD https://www.aniseed.cnrs.fr/aniseed/gene/show_gene?unique_id=Cirobu.g00006940
        //WASHU BROWSER https://www.aniseed.cnrs.fr/browser/?genome=Cirobu_KH&coordinate=KhC4:4306889-4323528&defaultContent=on
        //GENOME BROWSER https://www.aniseed.cnrs.fr/fgb2/gbrowse/ciona_intestinalis?name=KhC4:4306889..4323528

        public GameObject scrollBar;
        public int organism_id = 112;//Ciona intestinalis (see http://dev.aniseed.cnrs.fr/api/)

        public Dictionary<string, Gene> Genes; //List of Genes

        public GameObject MenuGenetic;

        public GameObject GeneTemplate;
        public GameObject GeneList;

        public Text currenttextloaded; //TEMP Text draw for informations loading
        public float initialY = 0;
        public float spaceY = 20;
        public int idxInfoxName;
        public InputField GeneName; //For Search

        //UNION INTRSECTION (For Genes Rendering)
        public GameObject GenesUnion;

        public GameObject GenesIntersection;
        public GameObject GenesDifferent;
        public GameObject GenesButtonActive;

        private bool alreadyRequested = false;
        public DataSet AttachedDataset;

        public void AttachDataset(DataSet source)
        {
            AttachedDataset = source;
        }

        public void onClickMenuGenetic()
        { //When we click on Menu Genetic  the first time
          //MorphoDebug.Log("START GENETIC for "+ AttachedDataset.id_type);
            if (!alreadyRequested)
            {
                if (AttachedDataset.id_type == 2 || AttachedDataset.id_type == 3)
                {
                    organism_id = 112;
                    idxInfoxName = AttachedDataset.Infos.getAllIdInfosForName();
                    //MorphoDebug.Log("Start Anisseed " + idxInfoxName);
                    init();
                    StartCoroutine(DataLoader.instance.requestAllGenes(AttachedDataset));
                    alreadyRequested = true;
                }
            }
        }

        //IList faut rajouter dans 'lapi menu close et reset selection ou reset coolro ...

        public void init()
        {
            if (scrollBar == null)
            {
                //MorphoDebug.Log("INIT ANISEED");
                if (Genes == null)
                    Genes = new Dictionary<string, Gene>();
                //MorphoDebug.Log("After gene");
                MenuGenetic = InterfaceManager.instance.MenuGenetic;
                InterfaceManager.setImageColors(MenuGenetic.transform.Find("resetAllColors").GetComponent<Button>());
                //MorphoDebug.Log("after set color");
                byGenes = MenuGenetic.transform.Find("byGenes").gameObject;
                //MorphoDebug.Log("after by genes");
                byCells = MenuGenetic.transform.Find("byCells").gameObject;
                //MorphoDebug.Log("after bycells");
                byStages = MenuGenetic.transform.Find("byStages").gameObject;
                //MorphoDebug.Log("after gy stage");
                ButtonByGenes = MenuGenetic.transform.Find("showByGenes").gameObject;
                //	MorphoDebug.Log("after show genes");
                ButtonByCells = MenuGenetic.transform.Find("showByCells").gameObject;
                //MorphoDebug.Log("after show cells");
                ButtonByStages = MenuGenetic.transform.Find("showByStages").gameObject;
                //	MorphoDebug.Log("after show stages");
                if (AttachedDataset.id_type != 2 && AttachedDataset.id_type != 3)
                    ButtonByStages.transform.Find("Text").GetComponent<Text>().text = "Times";
                //	MorphoDebug.Log("after times");
                InterfaceManager.setButtonColors(ButtonByGenes.GetComponent<Button>());
                //	MorphoDebug.Log("after set color 2");
                InterfaceManager.setButtonColors(ButtonByCells.GetComponent<Button>());
                //	MorphoDebug.Log("after set color 3");
                InterfaceManager.setButtonColors(ButtonByStages.GetComponent<Button>());
                //	MorphoDebug.Log("after set color 4");
                setGeneCellButtonActive(ButtonByGenes);
                //	MorphoDebug.Log("after set get");
                InterfaceManager.setImageColors(byCells.transform.Find("PickedCellDifferent").GetComponent<Button>());
                //	MorphoDebug.Log("after set color 5");
                InterfaceManager.setImageColors(byCells.transform.Find("PickedCellIntersection").GetComponent<Button>());
                //	MorphoDebug.Log("after set color 6");
                InterfaceManager.setImageColors(byCells.transform.Find("PickedCellUnion").GetComponent<Button>());
                //	MorphoDebug.Log("after set color 7");
                setPickedCellsActive(byCells.transform.Find("PickedCellUnion").gameObject);
                //	MorphoDebug.Log("after set picked ");
                GenesUnion = byGenes.transform.Find("GenesUnion").gameObject;
                GenesIntersection = byGenes.transform.Find("GenesIntersection").gameObject;
                GenesDifferent = byGenes.transform.Find("GenesDifferent").gameObject;
                //	MorphoDebug.Log("after union,intersection");
                InterfaceManager.setImageColors(GenesUnion.GetComponent<Button>());
                InterfaceManager.setImageColors(GenesIntersection.GetComponent<Button>());
                InterfaceManager.setImageColors(GenesDifferent.GetComponent<Button>());
                //	MorphoDebug.Log("after set color 10");
                setGenesActive(GenesDifferent);
                //	MorphoDebug.Log("after set genes active");
                onGenesIntersectionListen();
                onGenesDifferentListen();
                onGenesUnionListen();
                //	MorphoDebug.Log("after on op listen");

                currenttextloaded = InterfaceManager.instance.loading_text.GetComponent<Text>();

                //MorphoDebug.Log("after currenttextloaded");
                GeneName = InterfaceManager.instance.GeneName;
                scrollBar = InterfaceManager.instance.scrollbar_genetic;
                scrollBar.SetActive(false);
                //	MorphoDebug.Log("after inactive scroll bar");
                InterfaceManager.instance.BoutonAniseed.SetActive(true);
                InterfaceManager.instance.BoutonAniseed.transform.parent.gameObject.SetActive(true);
                //	MorphoDebug.Log("after active button");
                //	MorphoDebug.Log("after reorganize");
                GeneList = MenuGenetic.transform.Find("GeneList").gameObject;
                //	MorphoDebug.Log("after genelist");
                GeneTemplate = GeneList.transform.Find("GeneTemplate").gameObject;
                //	MorphoDebug.Log("after genetemplate");
                GeneTemplate.SetActive(false);
                GeneList = MenuGenetic.transform.Find("GeneList").gameObject;
                //	MorphoDebug.Log("after genelist");
                GeneTemplate.SetActive(false);
                initialY = GeneTemplate.transform.position.y;
                activeUI(false);
                //	MorphoDebug.Log("after activeUI");

                if (SetsManager.instance.IDUser < 0)
                    MenuGenetic.transform.Find("annotation").gameObject.SetActive(false); //For pubilc Access we unactive curration
                                                                                          //	MorphoDebug.Log("after annotation");
                                                                                          //StartCoroutine(requestAllGenes());
                MorphoTools.GetMutations().init();
                //	MorphoDebug.Log("after mutations");
            }
        }

        public void hideGenesBar(bool activated)
        {
            if (MenuGenetic == null)
                MenuGenetic = InterfaceManager.instance.MenuGenetic;
            //MenuGenetic.transform.Find("backgroundImage").gameObject.SetActive(activated);
            MenuGenetic.transform.Find("annotation").gameObject.SetActive(activated);
            //MenuGenetic.transform.Find("SelectedCell").gameObject.SetActive(activated);
            MenuGenetic.transform.Find("applyall").gameObject.SetActive(activated);
            MenuGenetic.transform.Find("byGenes").gameObject.SetActive(activated);
            MenuGenetic.transform.Find("byCells").gameObject.SetActive(activated);
            MenuGenetic.transform.Find("showByGenes").gameObject.SetActive(activated);
            MenuGenetic.transform.Find("showByCells").gameObject.SetActive(activated);
        }

        public GameObject sliderUI;
        public GameObject selectionUI;
        public int selectionUIV = 1;
        public List<Cell> UICells;
        public int isUI = 0; //0 : Different, 1: Union, 2 : Intersection,

        public void activeUI(bool v)
        {
            if (sliderUI == null)
            {
                sliderUI = byGenes.transform.Find("SliderUI").gameObject;
                sliderUI.GetComponent<Slider>().onValueChanged.AddListener(delegate
                { onScrollUI(); });
            }
            if (selectionUI == null)
                selectionUI = byGenes.transform.Find("selectionUI").gameObject;
            sliderUI.SetActive(v);
            selectionUI.SetActive(v);
            if (v)
            {
                if (GenesButtonActive == GenesDifferent)
                { //We change only from different
                    selectionUI.GetComponent<Image>().color = SelectionManager.getSelectedMaterial(selectionUIV).color;
                    foreach (var gn in Genes)
                    {
                        Gene g = gn.Value;
                        g.SubGene.SetActive(false);
                    }
                    reOrganize();
                }
            }
            else
            {//NORMAL STATE
                if (GenesButtonActive != GenesDifferent)
                    reOrganize();
            }
        }

        public void onScrollUI()
        {
            int previousSelection = selectionUIV;
            selectionUIV = Mathf.RoundToInt(sliderUI.GetComponent<Slider>().value);
            if (UICells != null)
                foreach (Cell c in UICells)
                {
                    c.removeSelection(previousSelection);
                    c.addSelection(selectionUIV);
                }
            selectionUI.GetComponent<Image>().color = SelectionManager.getSelectedMaterial(selectionUIV).color;
        }

        public void removeUI()
        {
            if (UICells != null)
                foreach (Cell c in UICells)
                    c.removeSelection(selectionUIV);
        }

        public void removeDifferent()
        {
            foreach (var gn in Genes)
            {
                Gene g = gn.Value;
                if (g.isActive && g.Cells != null)
                {
                    foreach (var cn in g.Cells)
                    {
                        Cell c = cn.Key;
                        c.removeSelection(g.selection);
                    }
                }
            }
        }

        //Compute all cells for union or intersecction and apply its selection value
        public void computeCellsUI()
        {
            UICells = new List<Cell>();
            if (GenesButtonActive == GenesUnion)
            { //UNION
                foreach (var gn in Genes)
                {
                    Gene g = gn.Value;
                    if (g.isActive && g.Cells != null)
                    {
                        //MorphoDebug.Log ("ad gene " + g.name);
                        foreach (var cn in g.Cells)
                        {
                            Cell c = cn.Key;
                            if (!UICells.Contains(c))
                                UICells.Add(c);
                        }
                    }
                }
            }
            else
            if (GenesButtonActive == GenesIntersection)
            { //INTERSECTION
                List<Cell> AllCells = new List<Cell>(); //First We List All Cells in all Active Genes
                foreach (var gn in Genes)
                {
                    Gene g = gn.Value;
                    if (g.isActive && g.Cells != null)
                    {
                        //MorphoDebug.Log ("ad gene " + g.name);
                        foreach (var cn in g.Cells)
                        {
                            Cell c = cn.Key;
                            if (!AllCells.Contains(c))
                                AllCells.Add(c);
                        }
                    }
                }
                //Now we check for all cell if they are inside all Genes
                foreach (Cell ca in AllCells)
                {
                    bool isAll = true;
                    foreach (var gn in Genes)
                    {
                        Gene g = gn.Value;
                        if (isAll && g.isActive && g.Cells != null)
                            if (!g.Cells.ContainsKey(ca))
                                isAll = false;
                    }
                    if (isAll && !UICells.Contains(ca))
                        UICells.Add(ca);
                }
            }
            foreach (Cell c in UICells)
                c.addSelection(selectionUIV); //Apply the selection value
        }

        //Active Union
        public void setGenesActive(GameObject activeGeneButton)
        {
            GenesButtonActive = activeGeneButton;
            GenesUnion.GetComponent<Button>().interactable = GenesUnion != activeGeneButton;
            GenesIntersection.GetComponent<Button>().interactable = GenesIntersection != activeGeneButton;
            GenesDifferent.GetComponent<Button>().interactable = GenesDifferent != activeGeneButton;
            if (GenesUnion == activeGeneButton)
                isUI = 1;
            if (GenesIntersection == activeGeneButton)
                isUI = 2;
            if (GenesDifferent == activeGeneButton)
                isUI = 0;
        }

        /*onGenesIntersectionListen();
        onGenesDifferentListen();
        onGenesUnionListen();*/

        public void onGenesUnionListen()
        { GenesUnion.GetComponent<Button>().onClick.AddListener(delegate { onGenesChange(GenesUnion); }); }

        public void onGenesIntersectionListen()
        { GenesIntersection.GetComponent<Button>().onClick.AddListener(delegate { onGenesChange(GenesIntersection); }); }

        public void onGenesDifferentListen()
        { GenesDifferent.GetComponent<Button>().onClick.AddListener(delegate { onGenesChange(GenesDifferent); }); }

        public void onGenesChange(GameObject activeGeneButton)
        {
            //Previous Activatation
            if (GenesDifferent == GenesButtonActive)
                removeDifferent(); //We Remove all individuals Selection
            else
                removeUI(); ////We Remove Intersection or Union Selection
                            //Current Activation
            setGenesActive(activeGeneButton);
            if (GenesDifferent == GenesButtonActive)
            {
                activeUI(false);
                //Reattribute all cell specification
                foreach (var gn in Genes)
                {
                    Gene g = gn.Value;
                    if (g.isActive && g.Cells != null)
                    {
                        foreach (var cn in g.Cells)
                        {
                            Cell c = cn.Key;
                            c.addSelection(g.selection);
                        }
                    }
                }
            }
            else
            {
                activeUI(true);
                computeCellsUI();
            }
        }

        public void desactivateToggle(Toggle b)
        {
            b.onValueChanged.RemoveAllListeners();
            b.isOn = false;
        }

        public Gene getGene(string genename)
        {
            if (Genes == null)
                Genes = new Dictionary<string, Gene>();
            if (Genes.ContainsKey(genename))
                return Genes[genename];

            //Create a new one
            //MorphoDebug.Log (" Create Gene for " + genename + " -> ");
            Gene gg = new Gene(Genes.Count + 1, genename, "", this);
            gg.init(Genes.Count);
            gg.shift(Genes.Count, 0);
            gg.download();
            Genes[genename] = gg;
            return gg;
        }

        public void addCellGene(Cell c, string genename, float expression, Correspondence cor)
        {
            Gene g = getGene(genename);
            g.cor = cor;
            if (expression > 0)
                g.addExpression(c, expression);
            else
                g.removeExpression(c);
        }

        public void updateGenesNbCells()
        {
            if (Genes != null && Genes.Count > 0)
            {
                foreach (var gn in Genes)
                {
                    Gene g = gn.Value;
                    if (g.Cells != null)
                        g.nbCells = g.Cells.Count();
                    //MorphoDebug.Log (" updateGenesNbCells for " + g.name + " -> " + g.nbCells);
                    g.setName();
                }
            }
        }

        //When we select a gene name
        public void search(string name)
        {
            name = name.ToLower().Trim();
            int nbView = 0;
            if (name == "")
            {
                foreach (var gn in Genes)
                {
                    Gene g = gn.Value;
                    g.select(nbView);
                    nbView += 1;
                    if (g.SubGene.activeSelf)
                        nbView += 1;
                }
            }
            else
            {
                foreach (var gn in Genes)
                {
                    Gene g = gn.Value;
                    if (g.name.ToLower().Contains(name) || g.unique_id.ToLower().Contains(name))
                    {
                        g.select(nbView);
                        nbView += 1;
                        if (g.SubGene.activeSelf)
                            nbView += 1;
                    }
                    else
                        g.unSelect();
                }
            }
            updateScrol(nbView);
        }

        public void cancel()
        {
            if (GeneName.text != "")
                GeneName.text = ""; //THIS directrly invoke search ("");
            else
                search("");
        }

        //Reset all active genes and clear all colors
        public void resetAndClear()
        {
            if (Genes != null)
            {
                foreach (var gn in Genes)
                {
                    Gene g = gn.Value;
                    if (g.isSelected)
                        g.go.transform.Find("selection").gameObject.GetComponent<Toggle>().isOn = false;//g.inActive ();
                }
                InterfaceManager.instance.MenuObjects.transform.Find("Selection").GetComponent<SelectionManager>().Start_clearAllColors();
            }
        }

        //Show Sub Gene Infos
        public void showSubGeneListen(Button b, Gene g)
        { b.onClick.AddListener(delegate { showSubGene(g); }); }

        public void showSubGene(Gene g)
        {
            if (isUI == 0)
            {
                g.showSubGene();
                AttachedDataset.Mutations.havemutant(g);
            }
        }

        //ACTIVE GENE
        public void activeListen(Toggle b, Gene g)
        { b.onValueChanged.AddListener(delegate { Active(g); }); }

        public void Active(Gene g)
        { g.changeActive(); }

        //EDIT SELECTION
        public void editSelectionListen(InputField inf, Gene g)
        { inf.onEndEdit.AddListener(delegate { editSelection(g); }); }

        public void editSelection(Gene g)
        { g.editSelection(); }

        //SLIDER FOR SELECTION
        public void sliderListen(Slider s, Gene g)
        { s.onValueChanged.AddListener(delegate { slider(g); }); }

        public void slider(Gene g)
        { g.slider(); }

        //COLOR MAP FOR QUANTITATIVE EXPRESSION
        public void changeColorBarListen(Button b, Gene g)
        { b.onClick.AddListener(() => changeColorBar(g)); }

        public void changeColorBar(Gene g)
        { g.changeColorBar(); }

        //Change Colormap
        public void changeColorMapListen(Button b, Gene g)
        { b.onClick.AddListener(() => changeColorMap(g)); }

        public void changeColorMap(Gene g)
        { g.changeColorMap(); }

        //For Massive Gene Annotation
        public void annotateListen(Toggle b, Gene g)
        { b.onValueChanged.AddListener(delegate { annotate(g); }); }

        public void annotate(Gene g)
        { g.annotate(); }

        public void onActiveAll(bool v)
        {
            foreach (var gn in Genes)
            {
                Gene g = gn.Value;
                if (g.isSelected)
                {
                    //MorphoDebug.Log ("Active Gene " + g.name);
                    if (v)
                        g.go.transform.Find("selection").gameObject.GetComponent<Toggle>().isOn = true;//g.active ();
                    else
                        g.go.transform.Find("selection").gameObject.GetComponent<Toggle>().isOn = false;//g.inActive ();
                }
            }
        }

        //FOR SCROLL BAR
        public int maxNbDraw = 27;

        public float sizeBar = 20f;

        public void reOrganize()
        {
            int nbView = 0;
            foreach (var gn in Genes)
            {
                Gene g = gn.Value;
                if (g.isSelected)
                {
                    nbView += 1;
                    if (g.SubGene.activeSelf)
                        nbView += 1;
                }
            }
            updateScrol(nbView);
        }

        public void updateScrol(int nbTotalScroll)
        {
            if (nbTotalScroll < maxNbDraw)
                scrollBar.SetActive(false);
            else
            {
                if (!scrollBar.activeSelf)
                    scrollBar.GetComponent<Scrollbar>().value = 1;
                scrollBar.GetComponent<Scrollbar>().numberOfSteps = 1 + nbTotalScroll - maxNbDraw;
                scrollBar.GetComponent<Scrollbar>().size = 1f / (float)(1 + nbTotalScroll - maxNbDraw);
                scrollBar.SetActive(true);
            }
            Scroll(); //Each Time we update the scroll bar (search ,selected cells ..) we reset the list from scratch
        }

        public void onSroll(Single value)
        { Scroll(); }

        public void Scroll()
        {
            Single value = scrollBar.GetComponent<Scrollbar>().value;
            int nbSteps = scrollBar.GetComponent<Scrollbar>().numberOfSteps;
            int decals = (int)Mathf.Round(nbSteps - value * (nbSteps - 1) - 1);
            int nbView = 0;
            foreach (var gn in Genes)
            {
                Gene g = gn.Value;
                if (g.isSelected)
                {
                    g.shift(nbView, decals);
                    nbView += 1;
                    if (g.SubGene.activeSelf)
                        nbView += 1;
                }
            }
        }

        //ANISEED DB SPECIFIC REQUEST
        //Query to look for a gene namev

        //Query the lislt of genes for a specific gene

        public void assignTo(bool PastFut, Gene g, int t, Dictionary<string, float> cellnamesConverted)
        {
            if (t >= AttachedDataset.MinTime && t <= AttachedDataset.MaxTime)
            {
                foreach (Cell cell in AttachedDataset.CellsByTimePoint[t])
                {
                    string cn = cell.getInfos(idxInfoxName); //Get the cell name
                    if (cellnamesConverted.ContainsKey(cn))
                    {
                        //MorphoDebug.Log(" Add Cell " + cn + " at " + t + " with value "+cellnamesConverted[cn]);
                        List<Cell> Benef;
                        if (PastFut)
                            Benef = cell.Mothers;
                        else
                            Benef = cell.Daughters;
                        foreach (Cell itM in Benef)
                        {
                            g.Cells[itM] = cellnamesConverted[cn];
                        }
                    }
                }
            }
        }

        public Vector2 getHPFBoundaries(string stage)
        {
            InterfaceManager.setLoading();
            CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            ci.NumberFormat.CurrencyDecimalSeparator = ".";
            stage = stage.Replace("Stage ", "");
            if (!AttachedDataset.DevTable.Dvpts.ContainsKey(2))
                return new Vector2(0f, 20f);
            List<DevelopmentTable.Dvpt> dvptL = AttachedDataset.DevTable.Dvpts[2];
            int i = 0;
            foreach (DevelopmentTable.Dvpt dvpt in dvptL)
            {
                //MorphoDebug.Log ("Compare  :" + stage+"=="+dvpt.stage+":");
                if (dvpt.stage == stage)
                {
                    //MorphoDebug.Log ("Found  :" +dvpt.stage);
                    float start_HPF = 0;
                    float.TryParse(dvpt.hpf, NumberStyles.Any, ci, out start_HPF);
                    float end_HPF = start_HPF + 10f;
                    if (i < dvptL.Count)
                    {
                        DevelopmentTable.Dvpt dvptAfter = dvptL[i + 1];
                        //MorphoDebug.Log ("dvptAfter  :" +dvptAfter.stage+ " at "+dvptAfter.hpf);
                        float.TryParse(dvptAfter.hpf, NumberStyles.Any, ci, out end_HPF);
                    }
                    //MorphoDebug.Log("start : " + start_HPF + " end : " + end_HPF);
                    return new Vector2(start_HPF, end_HPF);
                }
                i += 1;
            }
            //MorphoDebug.Log ("Didn't find this stage " + stage);
            return new Vector2(0f, 20f);
        }

        //Convert A10.34* to A10.00034*
        public string convertCellName(string cell_name)
        {
            string[] celspl = cell_name.ToLower().Trim().Split('.');
            if (celspl.Count() != 2)
                return "";
            string ab = celspl[0];
            string nb = celspl[1];
            if (ab[0] != 'a' && ab[0] != 'b')
                return "";
            bool start = false; // Is there a star at the end (for left cells)
            if (nb[nb.Length - 1] == '*')
            { nb = nb.Substring(0, nb.Length - 1); start = true; }
            int nbv = 0;
            if (!int.TryParse(nb, out nbv))
                return "";
            cell_name = ab + '.' + nbv.ToString("0000"); //Get the last par of the cell name
            if (start)
                return cell_name + '*';
            return cell_name + '_';
        }

        //Invser convert A10.00034*  to A10.34*
        public string invConvertCellName(string cell_name)
        {
            string[] celspl = cell_name.ToLower().Trim().Split('.');
            if (celspl.Count() != 2)
                return "";
            string ab = celspl[0];
            string nb = celspl[1];
            if (ab[0] != 'a' && ab[0] != 'b')
                return "";
            bool start = false; // Is there a star at the end (for left cells)
            while (nb[0] == '0')
                nb = nb.Substring(1); //Remove the 0
            if (nb[nb.Length - 1] == '*')
            { nb = nb.Substring(0, nb.Length - 1); start = true; }
            if (nb[nb.Length - 1] == '_')
            { nb = nb.Substring(0, nb.Length - 1); }
            cell_name = ab + '.' + nb; //Get the last par of the cell name
            if (start)
                return cell_name + '*';
            return cell_name;
        }

        //OPEN GENE CARD
        public void openLinkListen(Button b, Gene g)
        { b.onClick.AddListener(() => openLink(g)); }

        public void openLink(Gene g)
        { g.openLink(); }

        //FILTER GENES ON SELECTED CELL

        public void onshowSelectedCells()
        {
            activeShowSelectedCells();
        }

        //To See in live Genes in Select Cell
        //UNION INTRSECTION (For Genes Rendering)
        public string ByWhat = "Genes";

        public GameObject byGenes;
        public GameObject byCells;
        public GameObject byStages;
        public GameObject ButtonByGenes;
        public GameObject ButtonByCells;
        public GameObject ButtonByStages;

        public GameObject PickedCellUnion;
        public GameObject PickedCellDifferent;
        public GameObject PickedCellIntersection;
        public GameObject activePickedCell;
        public Coroutine queryGeneList = null;
        public UnityWebRequest GeneListRequest = null;

        public void setGeneCellButtonActive(GameObject ActiveButton)
        {
            //MorphoDebug.Log("ActiveButton=" + ActiveButton.name);
            if (ActiveButton == ButtonByGenes)
                showPickedOptions(false);

            byGenes.SetActive(ActiveButton == ButtonByGenes);
            byCells.SetActive(ActiveButton == ButtonByCells);
            byStages.SetActive(ActiveButton == ButtonByStages);

            ButtonByGenes.GetComponent<Button>().interactable = ButtonByGenes != ActiveButton;
            ButtonByCells.GetComponent<Button>().interactable = ButtonByCells != ActiveButton;
            ButtonByStages.GetComponent<Button>().interactable = ButtonByStages != ActiveButton;
        }

        public void changeSubMenuBy(string what) //CALL BY showBy(s) Button
        {
            ByWhat = what;
            byStages.transform.Find("Stage").GetComponent<Text>().text = "";
            switch (ByWhat)
            {
                case "Genes":
                    setGeneCellButtonActive(ButtonByGenes);
                    break;

                case "Cells":
                    setGeneCellButtonActive(ButtonByCells);
                    break;

                case "Stages":
                    setGeneCellButtonActive(ButtonByStages);
                    break;
            }
            activeShowSelectedCells();
        }

        public void setPickedCellsActive(GameObject ActiveButton)
        {
            PickedCellUnion.GetComponent<Button>().interactable = PickedCellUnion != ActiveButton;
            PickedCellIntersection.GetComponent<Button>().interactable = PickedCellIntersection != ActiveButton;
            PickedCellDifferent.GetComponent<Button>().interactable = PickedCellDifferent != ActiveButton;
        }

        public void showPickedOptions(Boolean showide)
        {
            if (PickedCellUnion == null)
            {
                PickedCellUnion = byCells.transform.Find("PickedCellUnion").gameObject;
                PickedCellUnion.GetComponent<Button>().onClick.AddListener(delegate
                { changePickedAction(PickedCellUnion); ; });
                activePickedCell = PickedCellUnion;
            }
            if (PickedCellIntersection == null)
            {
                PickedCellIntersection = byCells.transform.Find("PickedCellIntersection").gameObject;
                PickedCellIntersection.GetComponent<Button>().onClick.AddListener(delegate
                { changePickedAction(PickedCellIntersection); ; });
            }
            if (PickedCellDifferent == null)
            {
                PickedCellDifferent = byCells.transform.Find("PickedCellDifferent").gameObject;
                PickedCellDifferent.GetComponent<Button>().onClick.AddListener(delegate
                { changePickedAction(PickedCellDifferent); ; });
            }

            PickedCellUnion.SetActive(showide);
            PickedCellIntersection.SetActive(showide);
            PickedCellDifferent.SetActive(showide);
        }

        public void setPickedText(string text)
        {
            byCells.transform.Find("Cell").GetComponent<Text>().text = text;
        }

        public void changePickedAction(GameObject go)
        {
            activePickedCell = go;
            setPickedCellsActive(activePickedCell);
            activeShowSelectedCells();
        }

        public bool isCellAt(Dictionary<Cell, float> cells, int t)
        {
            foreach (var cc in cells)
                if (cc.Key.t == t)
                    return true;
            return false;
        }

        public bool isOneCellIn(Dictionary<Cell, float> cells, List<Cell> listcells) //UNION
        {
            foreach (Cell cc in listcells)
                if (cells.ContainsKey(cc))
                    return true;
            return false;
        }

        public bool isAllCellIn(Dictionary<Cell, float> cells, List<Cell> listcells) //INTERSECTION
        {
            foreach (Cell cc in listcells)
                if (!cells.ContainsKey(cc))
                    return false;
            return true;
        }

        public bool isUniqueCellIn(Dictionary<Cell, float> cells, List<Cell> listcells) //DIFFERENCE
        {
            int nbIn = 0;
            int nbNot = 0;
            foreach (Cell cc in listcells)
                if (!cells.ContainsKey(cc))
                    nbNot += 1;
                else
                    nbIn += 1;
            if (nbNot > 0 && nbIn > 0)
                return true;
            return false;
        }

        //Show all Genes
        public void showAllGenes()
        {
            cancel();
        }

        //Show only Genes involve at this stage (or times)
        public void showStagesGenes()
        {
            string currentStage = byStages.transform.Find("Stage").GetComponent<Text>().text;
            if (AttachedDataset.id_type == 2 || AttachedDataset.id_type == 3) //ANISEED DATABASE
            {
                string newStage = "Stage " + AttachedDataset.DevTable.getStage(AttachedDataset.CurrentTime);
                if (currentStage != newStage)
                {
                    byStages.transform.Find("Stage").GetComponent<Text>().text = newStage;
                    if (GeneListRequest != null)
                    { GeneListRequest.Abort(); }
                    if (queryGeneList != null)
                        StopCoroutine(queryGeneList);
                    queryGeneList = StartCoroutine(DataLoader.instance.queryStage("Stage " + AttachedDataset.DevTable.getStage(AttachedDataset.CurrentTime), this));
                }
            }
            else  //PERSONAL DATABASE
            {
                string newStage = "Time " + AttachedDataset.CurrentTime;
                if (currentStage != newStage)
                {
                    byStages.transform.Find("Stage").GetComponent<Text>().text = newStage;
                    ShowGenes = new Dictionary<int, int>();
                    foreach (var gn in Genes)
                    {
                        Gene g = gn.Value;
                        if (isCellAt(g.Cells, AttachedDataset.CurrentTime))
                            ShowGenes[g.id] = 1;
                    }
                    UpdateGenesView();
                }
            }
        }

        //Show only Genes invovle in the picked Cells
        public void showCellsGenes()
        {
            string textShow = "";
            ShowGenes = new Dictionary<int, int>();
            if (AttachedDataset.PickedManager.clickedCells == null || AttachedDataset.PickedManager.clickedCells.Count() == 0)
            { //NO CELLS SELECVTED
                textShow = "0 cell";
                UpdateGenesView();
                showPickedOptions(false);
            }
            else
            { //SOME CELLS
                if (AttachedDataset.PickedManager.clickedCells.Count() == 1)
                    showPickedOptions(false);
                else
                    showPickedOptions(true);
                if (AttachedDataset.id_type == 2 || AttachedDataset.id_type == 3) //ANISEED DATABASE
                {
                    if (GeneListRequest != null)
                    { GeneListRequest.Abort(); }
                    if (queryGeneList != null)
                        StopCoroutine(queryGeneList);

                    if (AttachedDataset.PickedManager.clickedCells.Count() == 1 && AttachedDataset.PickedManager.selectedCell != null)
                    {
                        textShow = invConvertCellName(AttachedDataset.PickedManager.selectedCell.getInfos(idxInfoxName)) + " at stage " + AttachedDataset.DevTable.getStage(AttachedDataset.PickedManager.selectedCell.t);
                        queryGeneList = StartCoroutine(DataLoader.instance.querySelectedCell(AttachedDataset.PickedManager.selectedCell, AttachedDataset));
                    }
                    else
                    {
                        textShow = AttachedDataset.PickedManager.clickedCells.Count().ToString() + " cells";
                        queryGeneList = StartCoroutine(DataLoader.instance.querySelectedCells(AttachedDataset.PickedManager.clickedCells, AttachedDataset));
                    }
                }
                else //PERSONAL DATABASE
                { //We Check for all genes if they contains the cells
                    if (AttachedDataset.PickedManager.clickedCells.Count() == 1 && AttachedDataset.PickedManager.selectedCell != null)
                        textShow = AttachedDataset.PickedManager.selectedCell.getName();
                    else
                        textShow = AttachedDataset.PickedManager.clickedCells.Count() + " cells";
                    if (isAnnotate)
                    { //ANNOTATION MODE
                        foreach (var gn in Genes)
                            ShowGenes[gn.Value.id] = 1;
                        UpdateGenesView();
                        checkValues();
                    }
                    else
                    {  //NORMALS MODE
                        if (AttachedDataset.PickedManager.clickedCells.Count() == 1) //ONE CELLS
                        {
                            foreach (var gn in Genes)
                            {
                                Gene g = gn.Value;
                                if (g.Cells != null && isOneCellIn(g.Cells, AttachedDataset.PickedManager.clickedCells))
                                    ShowGenes[g.id] = 1;
                            }
                        }
                        else //MULTIPLE CELLS
                            foreach (var gn in Genes)
                            {
                                Gene g = gn.Value;
                                if (g.Cells != null)
                                {
                                    if (PickedCellUnion == activePickedCell && isOneCellIn(g.Cells, AttachedDataset.PickedManager.clickedCells))
                                        ShowGenes[g.id] = 1;
                                    if (PickedCellIntersection == activePickedCell && isAllCellIn(g.Cells, AttachedDataset.PickedManager.clickedCells))
                                        ShowGenes[g.id] = 1;
                                    if (PickedCellDifferent == activePickedCell && isUniqueCellIn(g.Cells, AttachedDataset.PickedManager.clickedCells))
                                        ShowGenes[g.id] = 1;
                                }
                            }

                        UpdateGenesView();
                    }
                }
            }
            setPickedText(textShow);
        }

        public void activeShowSelectedCells()
        {
            //MorphoDebug.Log("activeShowSelectedCells for " + ByWhat);
            switch (ByWhat)
            {
                case "Genes":
                    showAllGenes();
                    break;

                case "Stages":
                    showStagesGenes();
                    break;

                case "Cells":
                    showCellsGenes();
                    break;
            }
        }

        public bool isAnnotate = false;

        public void massiveCurration()
        {
            isAnnotate = !isAnnotate;
            if (isAnnotate)
            {//In Curration Mode
                int nbView = 0;
                foreach (var gn in Genes)
                {
                    Gene g = gn.Value;
                    g.go.transform.Find("deploy").gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
                    g.go.transform.Find("selection").gameObject.SetActive(false);
                    g.go.transform.Find("annotation").gameObject.SetActive(true);
                    g.select(nbView);
                    nbView += 1;
                    g.SubGene.SetActive(false);
                }
                updateScrol(nbView);
                checkValues();
            }
            else
            {
                int nbView = 0;
                foreach (var gn in Genes)
                {
                    Gene g = gn.Value;
                    showSubGeneListen(g.go.transform.Find("deploy").gameObject.GetComponent<Button>(), g);
                    g.go.transform.Find("annotation").gameObject.SetActive(false);
                    g.go.transform.Find("selection").gameObject.SetActive(true);
                    if (g.isSelected)
                        nbView += 1;
                }
                updateScrol(nbView);
            }
        }

        //Check the last values on all genes (for select cells)
        public void checkValues()
        {
            if (AttachedDataset.PickedManager.clickedCells == null || AttachedDataset.PickedManager.clickedCells.Count == 0)
            { //NO CELLS SELECTED
                foreach (var gn in Genes)
                {
                    Gene g = gn.Value;
                    GameObject annotation = g.go.transform.Find("annotation").gameObject;
                    annotation.transform.Find("nbTrue").gameObject.GetComponent<Text>().text = "0";
                    annotation.transform.Find("annotate").gameObject.SetActive(false);
                }
            }
            else
            {
                foreach (var gn in Genes)
                {
                    Gene g = gn.Value;
                    GameObject annotation = g.go.transform.Find("annotation").gameObject;
                    int nbOn = 0;
                    int nbTot = AttachedDataset.PickedManager.clickedCells.Count;
                    foreach (Cell cc in AttachedDataset.PickedManager.clickedCells)
                        if (g.Cells != null && g.Cells.ContainsKey(cc))
                            nbOn++;
                    annotation.transform.Find("nbTrue").gameObject.GetComponent<Text>().text = "" + nbOn.ToString() + "/" + nbTot.ToString();
                    GameObject annotate = annotation.transform.Find("annotate").gameObject;
                    annotate.SetActive(true);
                    Toggle annotateT = annotate.GetComponent<Toggle>();
                    annotateT.onValueChanged.RemoveAllListeners();
                    if (nbOn == nbTot)
                        annotateT.isOn = true;
                    else
                        annotateT.isOn = false;
                    annotateListen(annotateT, g);
                }
            }
        }

        public Dictionary<int, int> ShowGenes;

        public void UpdateGenesView()
        {
            int nbView = 0;
            foreach (var gn in Genes)
            {
                Gene g = gn.Value;
                if (ShowGenes.ContainsKey(g.id))
                {
                    g.select(nbView);
                    nbView += 1;
                    if (g.SubGene.activeSelf)
                        nbView += 1;
                }
                else
                    g.unSelect();
            }
            updateScrol(nbView);
        }
    }
}