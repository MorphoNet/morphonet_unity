using System;

using UnityEngine;

namespace MorphoNet
{
    /// <summary>
    /// Lazy singleton creation.
    /// If the instance does not exist when called, create it automaticaly at runtime.
    /// </summary>
    /// <typeparam name="T">The singleton type.</typeparam>
    public abstract class UnitySingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static readonly Lazy<T> LazyInstance = new Lazy<T>(CreateSingleton);

        public static T Instance => LazyInstance.Value;

        /// <summary>
        /// Create a new singleton instance, create a new <see cref="GameObject"/>,
        /// attach the instance to it and prevent the GameObject from destruction.
        /// </summary>
        private static T CreateSingleton()
        {
            var ownerObject = new GameObject($"{typeof(T).Name} (singleton)");
            var instance = ownerObject.AddComponent<T>();
            DontDestroyOnLoad(ownerObject);
            return instance;
        }
    }
}