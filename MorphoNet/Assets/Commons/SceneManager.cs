using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.Management;

using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;

namespace MorphoNet
{
    /// <summary>
    /// This class is an interface for the <see cref="UnityEngine.SceneManagement.SceneManager"/> specific to the Morphonet Application.
    /// It handle Morphonet <see cref="ScenesNames"/> and their relative <see cref="XrParameters"/>.
    /// </summary>
    public class SceneManager : UnitySingleton<SceneManager>
    {
        #region Scenes parameters

        /// <summary>
        /// Scenes names are saved as the path from the "Assets" folder to the scene file, without extension.
        /// Ex: "Assets/Scenes/SceneName.unity" --> "Scenes/SceneName".
        /// </summary>
        private static class ScenesNames
        {
            public const string Morphonet = "SceneEmbryo";
            public const string MorphonetVR = "MorphonetVR";
            public const string ParametersLoading = "LoadParameters";
        }

        #endregion Scenes parameters

        private static int VR_SETUP_ATTEMPTS=5;

        public void LoadParameterScene() => UnitySceneManager.LoadScene(ScenesNames.ParametersLoading);
        public void LoadEmbryoScene() => UnitySceneManager.LoadScene(ScenesNames.Morphonet);

        public bool IsSceneMorphoNet() => UnitySceneManager.GetActiveScene().name == ScenesNames.Morphonet;
        
        public bool IsSceneParametersLoading() => UnitySceneManager.GetActiveScene().name == ScenesNames.ParametersLoading;

        public void EnableVr() => StartCoroutine(LoadVrScene());

        public void DisableVr() => StartCoroutine(UnloadVrScene());

        /// <summary>
        /// Coroutine loading the VR scene asynchronously.
        /// </summary>
        /// <returns></returns>
        private IEnumerator LoadVrScene()
        {
            for (int i = 0; i < UnitySceneManager.sceneCount; i++)
            {
                if (UnitySceneManager.GetSceneAt(i).name == ScenesNames.MorphonetVR)
                {
                    MorphoDebug.LogWarning("Vr already launched, cannot start another.");
                    yield break;
                }
            }
            int tries = 0;

            while (!XRGeneralSettings.Instance.Manager.isInitializationComplete && tries < VR_SETUP_ATTEMPTS)
            {
                tries++;
                yield return InitializeVR();
            }
            if (tries < VR_SETUP_ATTEMPTS)
            {
                XRGeneralSettings.Instance.Manager.StartSubsystems();

                if (XRGeneralSettings.Instance.Manager.isInitializationComplete)
                    UnitySceneManager.LoadSceneAsync(ScenesNames.MorphonetVR, LoadSceneMode.Additive);
                else
                    MorphoDebug.LogWarning("Tried to launch VR, when system was not initialized.");

                InterfaceManager.instance.ExpandVRViewButton.gameObject.SetActive(true);
                InterfaceManager.instance.VRViewTexture.gameObject.SetActive(true);
            }   

            
        }

        public void CheckAndInitVR()
        {
            if(RightManager.Instance.canAccessVR)StartCoroutine(InitializeVR());
        }
        

        /// <summary>
        /// check VR initialization, and manage if HMD is plugged in or not
        /// </summary>
        /// <returns></returns>
        private IEnumerator InitializeVR()
        {
            //StopCoroutine("InitianizeVR");
            if (!XRGeneralSettings.Instance.Manager.isInitializationComplete)
            {
                int tries = 0;
                while (!XRGeneralSettings.Instance.Manager.isInitializationComplete && tries < VR_SETUP_ATTEMPTS)
                {
                    tries++;
                    yield return XRGeneralSettings.Instance.Manager.InitializeLoader();
                }
                    

                if (UnityEngine.XR.OpenXR.OpenXRRuntime.name != "")//if no HMD started
                    InterfaceManager.instance.ButtonVR.interactable = true;

                
            }
            else
            {
                InterfaceManager.instance.ButtonVR.interactable = true;
                InterfaceManager.instance.ExpandVRViewButton.gameObject.SetActive(true);
                InterfaceManager.instance.VRViewTexture.gameObject.SetActive(true);
            }
            yield return null;
        }


        
        private IEnumerator UnloadVrScene()
        {
# if !UNITY_WEBGL
            StopVr();
#endif            

            yield return new WaitUntil(() => MorphoTools.GetDataset());
        
            MorphoTools.GetDataset().DataSetDisplayStrategy = SetsManager.instance._DataSetDisplayStrategy;
            MorphoTools.GetDataset().ApplyDisplayStrategyAndUpdateAll();
            bool unload = false;
            for (int i = 0; i < UnitySceneManager.sceneCount; i++)
            {
                if (UnitySceneManager.GetSceneAt(i).name == ScenesNames.MorphonetVR)
                {
                    unload = true;
                }
            }
            if (unload)
            {
                UnitySceneManager.UnloadSceneAsync(ScenesNames.MorphonetVR);
                InterfaceManager.instance.ExpandVRViewButton.gameObject.SetActive(false);
                InterfaceManager.instance.VRViewTexture.gameObject.SetActive(false);
            }
                
        }

        private void OnApplicationQuit()
        {
            StopVr();
        }

        private static void StopVr()
        {
            if (XRGeneralSettings.Instance.Manager.isInitializationComplete)
            {
                XRGeneralSettings.Instance.Manager.StopSubsystems();
                XRGeneralSettings.Instance.Manager.DeinitializeLoader();
            }
        }
    }
}