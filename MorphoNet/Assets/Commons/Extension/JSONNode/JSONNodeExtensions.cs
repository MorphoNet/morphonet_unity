using JsonNode = SimpleJSON.JSONNode;

namespace MorphoNet.Extensions.JSONNode
{
    public static class JSONNodeExtensions
    {
        public static bool IsFieldEmptyOrNull(this JsonNode node, string fieldToCheck)
        {
            return
                node[fieldToCheck] is null
                || string.IsNullOrWhiteSpace(node[fieldToCheck].Value.Trim())
                || node[fieldToCheck].Value.Trim().ToLower() == "null";
        }
    }
}