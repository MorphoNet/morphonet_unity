using System;
using UnityEngine;

namespace MorphoNet.Extensions.Unity
{
    public static class RendererExtensions
    {
        public static void UpdateIntProperty(this Renderer renderer, string propertyName, int value, int materialIndex = 0)
        {
            materialIndex = materialIndex < 0 ? 0 : materialIndex;
            renderer.materials[materialIndex].UpdateIntProperty(propertyName, value);
        }

        public static void UpdateFloatProperty(this Renderer renderer, string propertyName, float value, int materialIndex = 0)
        {
            materialIndex = materialIndex < 0 ? 0 : materialIndex;
            renderer.materials[materialIndex].UpdateFloatProperty(propertyName, value);
        }

        public static void UpdateVectorProperty(this Renderer renderer, string propertyName, Vector4 value, int materialIndex = 0)
        {
            materialIndex = materialIndex < 0 ? 0 : materialIndex;
            renderer.materials[materialIndex].UpdateVectorProperty(propertyName, value);
        }

        public static void UpdateColorProperty(this Renderer renderer, string propertyName, Color color, int materialIndex = 0)
        {
            materialIndex = materialIndex < 0 ? 0 : materialIndex;
            renderer.materials[materialIndex].UpdateColorProperty(propertyName, color);
        }

        public static void UpdateIntProperty(this Material material, string propertyName, int value)
        {
            if (material.HasProperty(propertyName))
                if (material.GetInt(propertyName) != value)
                {
                    material.SetInt(propertyName, value);
                }
        }

        public static void UpdateFloatProperty(this Material material, string propertyName, float value)
        {
            if (material.HasProperty(propertyName)){
                if (Math.Abs(material.GetFloat(propertyName) - value) > 0.00001f)
                {
                    material.SetFloat(propertyName, value);
                }
            }
        }

        public static void UpdateVectorProperty(this Material material, string propertyName, Vector4 value)
        {
            if (material.HasProperty(propertyName)){
                if (material.GetVector(propertyName) != value)
                {
                    material.SetVector(propertyName, value);
                }
            }
        }

        public static void UpdateColorProperty(this Material material, string propertyName, Color color)
        {
            if (material.HasProperty(propertyName))
            {
                Color prop = material.GetColor(propertyName);
                if (Math.Abs(prop.r - color.r) > 0.001f || Math.Abs(prop.g - color.g) > 0.001f || Math.Abs(prop.b - color.b) > 0.001f || Math.Abs(prop.a - color.a) > 0.001f )
                {
                    material.SetColor(propertyName, color);
                }
            }
        }
    }
}