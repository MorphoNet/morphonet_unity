using UnityEngine;

namespace MorphoNet.Extensions.Unity
{
    public static class Vector3Extensions
    {
        public static bool Approximately(this Vector3 self, Vector3 target)
            => Mathf.Approximately(self.x, target.x)
                && Mathf.Approximately(self.y, target.y)
                && Mathf.Approximately(self.z, target.z);
    }
}