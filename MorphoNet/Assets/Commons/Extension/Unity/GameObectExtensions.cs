using UnityEngine;

namespace MorphoNet.Extensions.Unity
{
    public static class GameObectExtensions
    {
        /// <summary>
        /// Fetch and return the first Component of type <typeparamref name="T"/> found
        /// on the given <paramref name="gameObject"/>. If none was found, add one to the
        /// <paramref name="gameObject"/> and return it.
        /// </summary>
        /// <typeparam name="T">The type of component to found/create.</typeparam>
        public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
        {
            T component = gameObject.GetComponent<T>();

            if (component == null)
                component = gameObject.AddComponent<T>();

            return component;
        }

        /// <summary>
        /// Check if the given <paramref name="gameObject"/> has
        /// a component of type <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gameObject"></param>
        /// <returns></returns>
        public static bool HasComponent<T>(this GameObject gameObject) where T : MonoBehaviour
            => gameObject.GetComponent<T>() != null;

        /// <summary>
        /// Switch the <see cref="GameObject.activeSelf"/> of <paramref name="gameObject"/> to <c>true</c>.
        /// </summary>
        /// <param name="gameObject"></param>
        public static void Activate(this GameObject gameObject)
                => gameObject.SetActive(true);

        /// <summary>
        /// Switch the <see cref="GameObject.activeSelf"/> of <paramref name="gameObject"/> to <c>true</c>.
        /// </summary>
        public static void Deactivate(this GameObject gameObject)
                => gameObject.SetActive(false);

        /// <summary>
        /// Switch the active state of the given <see cref="GameObject"/>.
        /// </summary>
        public static void SwitchActiveState(this GameObject gameObject)
                => gameObject.SetActive(!gameObject.activeSelf);
    }
}