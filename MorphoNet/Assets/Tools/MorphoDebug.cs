using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace MorphoNet
{
    public static class MorphoDebug
    {

        private const string LOG_FILE = "LogFile.txt";
        private const string LOG_FILE_PREV = "LogFile-prev.txt";

        
        public static void Log(string message, int verbose = 2)
        {
            Debug.Log(message);
            VerboseMessage(message, verbose);
        }


        public static void LogWarning(string message, int verbose = 2)
        {
            Debug.LogWarning(message);
            VerboseMessage($"<color=#FFC50B>{message}</color>", verbose);
        }

        public static void LogError(string message, int verbose = 2)
        {
            Debug.LogError(message);
            VerboseMessage($"<color=#E21300>{message}</color>", verbose);
        }

        private static void VerboseMessage(string message, int verbose = 2)
        {

            if (verbose <= 1)//message in main scene message label
            {
                SendMainSceneMessage(message);
            }
            if (verbose <= 2)//message in main scene console
            {
                SendConsoleMessage(message);
            }
            //message in log file : do it all the time ONLY in standalone
            WriteLogFileMessage(message);
#if !UNITY_EDITOR && UNITY_STANDALONE
            
            WriteLogFileMessage(message);
#endif
        }

        private static void SendMainSceneMessage(string message)
        {
            if (InterfaceManager.instance != null)
            {
                InterfaceManager.instance.setComment(message);
            }
        }


        private static void SendConsoleMessage(string message)
        {
            if (DebugConsoleManager.instance != null)
            {
                DebugConsoleManager.instance.SendConsoleMessage(message);
            }
        }

        private static void WriteLogFileMessage(string message)
        {
            try
            {
                StreamWriter writer = new StreamWriter(Path.Combine(Application.persistentDataPath, LOG_FILE), true);

                writer.Write(message + Environment.NewLine);

                writer.Close();
            }catch(Exception e)
            {
                Debug.LogError("WARNING: could not write to LogFile : "+e.StackTrace);
            }
            
        }
        
        public static void SetupLogOnLaunch()
        {
            string logpath = Path.Combine(Application.persistentDataPath, LOG_FILE);
            string prevlogpath = Path.Combine(Application.persistentDataPath, LOG_FILE_PREV);
            MorphoDebug.Log(logpath);
            MorphoDebug.Log(prevlogpath);
            //if prev exists, remove its contents
            if (File.Exists(prevlogpath))
            {
                FileStream fileStream = File.Open(prevlogpath, FileMode.Truncate);
                fileStream.SetLength(0);
                fileStream.Close();
            }

            if (File.Exists(logpath)){
                //put contents of logfile in prev
                using (StreamReader reader = new StreamReader(logpath))
                {
                    using (StreamWriter writer = new StreamWriter(prevlogpath))
                    {
                        while (!reader.EndOfStream)
                        {
                            writer.WriteLine(reader.ReadLine());
                        }
                    }
                    
                }
                //then truncate log file
                FileStream fileStream = File.Open(logpath, FileMode.Truncate);
                fileStream.SetLength(0);
                fileStream.Close();
            }
            Log("Initianizing MorphoNet Standalone : " + DateTime.Now);
        }



    }
}