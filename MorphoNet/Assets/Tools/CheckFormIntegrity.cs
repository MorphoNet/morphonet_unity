using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckFormIntegrity : MonoBehaviour
{
    [SerializeField]
    public List<InputField> StringInputs = new List<InputField>();
    [SerializeField]
    public List<InputField> IntInputs = new List<InputField>();
    [SerializeField]
    public List<InputField> FloatInputs = new List<InputField>();

    [SerializeField]
    public List<InputField> MinimumRequired = new List<InputField>();//list of all inputfields that need at least one valid entry

    [SerializeField]
    public Button ConfirmFormButton;

    [SerializeField]
    public bool InitStateOfButton = false;

    private Color _WarningRed = new Color(0.9716f, 0.7756f, 0.7756f,1f);


    // Start is called before the first frame update
    void Start()
    {
        foreach(var s in StringInputs)
        {
            s.onValueChanged.AddListener(delegate { CheckFieldValueForForm(); });
        }

        foreach (var s in IntInputs)
        {
            s.onValueChanged.AddListener(delegate { CheckFieldValueForForm(); });
        }

        foreach (var s in FloatInputs)
        {
            s.onValueChanged.AddListener(delegate { CheckFieldValueForForm(); });
        }

        foreach (var s in MinimumRequired)
        {
            s.onValueChanged.AddListener(delegate { CheckFieldValueForForm(); });
        }

        ConfirmFormButton.interactable = InitStateOfButton;
    }


    public void CheckFieldValueForForm()
    {
        bool ok = true;
        foreach(var s in StringInputs)
        {
            if (s.text == "")
            {
                s.gameObject.GetComponent<Image>().color = _WarningRed;
                ok = false;
            }
            else
            {
                s.gameObject.GetComponent<Image>().color = Color.white;
            }
                
        }

        foreach (var i in IntInputs)
        {
            int res;
            if (int.TryParse(i.text,out res))
            {
                i.gameObject.GetComponent<Image>().color = Color.white;
                
            }
            else
            {
                i.gameObject.GetComponent<Image>().color = _WarningRed;
                ok = false;
            }

        }

        foreach (var f in FloatInputs)
        {
            float res;
            if (float.TryParse(f.text,System.Globalization.NumberStyles.Any,System.Globalization.CultureInfo.InvariantCulture, out res))
            {
                f.gameObject.GetComponent<Image>().color = Color.white;
            }
            else
            {
                f.gameObject.GetComponent<Image>().color = _WarningRed;
                ok = false;
            }

        }

        bool allempty = true;
        if (MinimumRequired.Count > 0)
        {
            foreach (var m in MinimumRequired)
            {
                if(m.text != "")
                {
                    allempty = false;
                }
            }
            if (allempty)
                ok = false;
        }
            

        ConfirmFormButton.interactable = ok;
    }
    

    
}
