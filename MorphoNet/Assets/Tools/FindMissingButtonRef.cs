using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using MorphoNet;

#if UNITY_EDITOR
public class FindMissingButtonRef : EditorWindow
{
    static int go_count = 0;
    static int components_count = 0;

   [MenuItem("Window/FindMissingButtonRef")]
   public static void ShowWindow()
   {
       EditorWindow.GetWindow(typeof(FindMissingButtonRef));
   }

   public void OnGUI()
   {
       if (GUILayout.Button("Find Missing Scripts in selected GameObjects"))
       {
           FindInSelected();
       }
   }
   private static void FindInSelected()
   {
       GameObject[] go = Selection.gameObjects;
       go_count = 0;
       components_count = 0;
       foreach (GameObject g in go)
       {
           FindInGO(g);
       }
   }


   private static void FindInGO(GameObject g)
   {
       go_count++;
       Button[] components = g.GetComponents<Button>();
       for (int i = 0; i < components.Length; i++)
       {
           components_count++;
            if (components[i].onClick.GetPersistentEventCount() > 0)
            {
                for (int j = 0; j< components[i].onClick.GetPersistentEventCount(); j++)
                {
                    if (string.IsNullOrEmpty( components[i].onClick.GetPersistentMethodName(j)))
                    {
                        string message = " has a button with missing method on click at index : ";
                        PrintReferenceError(g, i,message);
                    }

                    if (components[i].onClick.GetPersistentTarget(j) == null)
                    {
                        string message = " has a button with missing reference object on click at index : ";
                        PrintReferenceError(g,i,message);
                    }
                }
            }
       }
       // Now recurse through each child GO (if there are any):
       foreach (Transform childT in g.transform)
       {
           //MorphoDebug.Log("Searching " + childT.name  + " " );
           FindInGO(childT.gameObject);
       }
   }

    private static void PrintReferenceError(GameObject g, int i,string message)
    {
        string s = g.name;
        Transform t = g.transform;
        while (t.parent != null)
        {
            s = t.parent.name + "/" + s;
            t = t.parent;
        }
        Debug.Log(s + message + i, g);
    }
}
#endif
