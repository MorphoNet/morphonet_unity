﻿using ICSharpCode.SharpZipLib.BZip2;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System.Globalization;

namespace MorphoNet
{
    public class MorphoPostWebRequest
    {
        private string url;
        private string hash;
        private int download_id;
        private int id_dataset;
        private int id_people;
        private int id_infos;
        private int quality;
        private byte[] actions;
        private byte[] file_upload;
        private byte[] field;
        private int id_colormap;
        private byte[] colormaptxt;
        private string colormap;
        private string colormap_name;
        private int id_comment;

        //bundles
        private int t;

        private int id;
        private int priority;

        //Scenario
        private int scenario;

        private int id_scenario;
        private int movie;
        private int id_user;
        private int framerate;
        private string outputname;

        //screenshot
        private string name;

        //aniseed
        private string allgene;

        private int organism_id;
        private string start_stage;
        private string end_stage;
        private int gene_id;
        private string numberofcell;
        private string getGenes;
        private string stage;

        public string text_result;
        public byte[] data_result;
        public bool id_done;

        public MorphoPostWebRequest(string url, string hash = "", int download_id = -1, int id_dataset = -1, int id_people = -1, int id_infos = -1, int quality = -1, byte[] actions = null, byte[] fileupload = null, byte[] field = null, byte[] colormaptxt = null, string colormap = "", string colormap_name = "", int t = -1, int id = -1, int scenario = -1, int id_scenario = -1, int movie = -1, int id_user = -1, int framerate = -1, string outputname = "", string name = "", int id_comment = -1, int priority = -1, string allgene = "", int organism_id = -1, string start_stage = "", string end_stage = "", int gene_id = -1, string numberofcell = "", string getGenes = "", string stage = "", int id_colormap = -1)
        {
            this.url = url;
            this.hash = hash;
            this.download_id = download_id;
            this.id_dataset = id_dataset;
            this.id_infos = id_infos;
            this.id_people = id_people;
            this.quality = quality;
            this.actions = actions;
            file_upload = fileupload;
            this.field = field;
            this.colormaptxt = colormaptxt;
            this.colormap = colormap;
            this.colormap_name = colormap_name;
            this.t = t;
            this.id = id;
            this.scenario = scenario;
            this.id_scenario = id_scenario;
            this.movie = movie;
            this.id_user = id_user;
            this.framerate = framerate;
            this.outputname = outputname;
            this.name = name;
            this.id_comment = id_comment;
            this.priority = priority;
            this.allgene = allgene;
            this.organism_id = organism_id;
            this.start_stage = start_stage;
            this.end_stage = end_stage;
            this.gene_id = gene_id;
            this.numberofcell = numberofcell;
            this.getGenes = getGenes;
            this.stage = stage;
            //...
            id_done = false;
            this.id_colormap = id_colormap;

            SetsManager.instance.StartCoroutine(PostRequest());
        }

        public IEnumerator PostRequest()
        {
            WWWForm form = new WWWForm();

            if (hash != "")
                form.AddField("hash", hash);
            if (download_id != -1)
                form.AddField("download", download_id);
            if (id_dataset != -1)
                form.AddField("id_dataset", id_dataset);
            if (id_infos != -1)
                form.AddField("id_infos", id_infos);
            if (colormap != "")
                form.AddField("colormap", colormap);
            if (id_people != -1)
                form.AddField("id_people", id_people);
            if (colormap_name != "")
                form.AddField("name", colormap_name);
            if (colormaptxt != null)
                form.AddBinaryData("colormap.txt", colormaptxt);
            if (quality != -1)
                form.AddField("quality", quality);
            if (actions != null)
                form.AddBinaryData("actions", actions);
            if (file_upload != null)
                form.AddBinaryData("file_upload", file_upload);
            if (field != null)
                form.AddBinaryData("field", field);
            if (t != -1)
                form.AddField("t", t);
            if (id != -1)
                form.AddField("id", id);
            if (scenario != -1)
                form.AddField("scenario", scenario);
            if (id_scenario != -1)
                form.AddField("id_scenario", id_scenario);
            if (movie != -1)
                form.AddField("movie", movie);
            if (id_user != -1)
                form.AddField("id_people", id_user);
            if (framerate != -1)
                form.AddField("framerate", framerate);
            if (outputname != "")
                form.AddField("outputname", outputname);
            if (name != "")
                form.AddField("name", name);
            if (id_comment != -1)
                form.AddField("id_comment", id_comment);
            if (priority != -1)
                form.AddField("priority", priority);
            if (allgene != "")
                form.AddField("allgene", allgene);
            if (organism_id != -1)
                form.AddField("organism_id", organism_id);
            if (start_stage != "")
                form.AddField("start_stage", start_stage);
            if (end_stage != "")
                form.AddField("end_stage", end_stage);
            if (gene_id != -1)
                form.AddField("gene_id", gene_id);
            if (numberofcell != "")
                form.AddField("numberofcell", numberofcell);
            if (getGenes != "")
                form.AddField("getGenes", getGenes);
            if (stage != "")
                form.AddField("stage", stage);
            if (id_colormap != -1)
                form.AddField("id_colormap", id_colormap);

            UnityWebRequest www = UnityWebRequests.Post(url, "", form);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                MorphoDebug.Log("ERROR :" + www.error);
                text_result = "";
                data_result = null;
                id_done = true;
                yield return null;
            }
            else
            {
                if (www.downloadHandler.text != "")
                    MorphoDebug.Log("Requete Post faite  " + www.downloadHandler.text);
                text_result = www.downloadHandler.text;
                data_result = www.downloadHandler.data;
                id_done = true;
            }
            www.Dispose();
        }
    }

    public class MorphoGetWebRequest
    {
        private string url;
        public string text_result;
        public byte[] data_result;
        public bool is_done;

        public MorphoGetWebRequest(string url)
        {
            this.url = url;
            is_done = false;

            SetsManager.instance.StartCoroutine(GetRequest());
        }

        public IEnumerator GetRequest()
        {
            UnityWebRequest www = UnityWebRequests.Get(url, "");
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                MorphoDebug.Log("ERROR :" + www.error);
                text_result = "";
                data_result = null;
                is_done = true;
                yield return null;
            }
            text_result = www.downloadHandler.text;
            data_result = www.downloadHandler.data;
            is_done = true;
        }
    }

    public static class UtilsManager
    {
        public static byte[] Unzip(byte[] data)
        {
            MemoryStream inStream = new MemoryStream(data, 0, data.Length);
            MemoryStream outStream = new MemoryStream();
            BZip2.Decompress(inStream, outStream, true);
            byte[] result = outStream.ToArray();
            inStream.Close();
            outStream.Close();
            return result;
        }

        public static byte[] Zip(byte[] data)
        {
            MemoryStream inStream = new MemoryStream(data, 0, data.Length);
            MemoryStream outStream = new MemoryStream();
            BZip2.Compress(inStream, outStream, true, 9);
            byte[] result = outStream.ToArray();
            inStream.Close();
            outStream.Close();
            return result;
        }

        public static int convertJSONToInt(string jsonfield)
        {
            int i = 0;
            if (jsonfield != null)
                int.TryParse(jsonfield.ToString().Replace('"', ' ').Trim(), out i);
            return i;
        }

        public static float convertJSONToFloat(string jsonfield)
        {
            CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            ci.NumberFormat.CurrencyDecimalSeparator = ".";
            float i = 0;

            float.TryParse(jsonfield.ToString().Replace('"', ' ').Trim(), NumberStyles.Any, ci, out i);
            return i;
        }

        public static string convertJSON(string jsonfield)
        {
            return jsonfield.ToString().Replace('"', ' ').Trim();
        }

        public static string getIdxFormat(int idx)
        {
            int i = 0;
            foreach (KeyValuePair<string, string> mf in SetsManager.instance.MorphoFormat)
            {
                if (i == idx)
                    return mf.Key;
                i += 1;
            }
            return "";
        }
    }

    public class UnityWebRequests
    {
        public static string UrlCombine(string uri1, string uri2)
        {
            if (uri1 == "")
            {
                return uri2;
            }

            if (uri2 == "")
            {
                return uri1;
            }

            uri1 = uri1.TrimEnd('/');
            uri2 = uri2.TrimStart('/');
            return string.Format("{0}/{1}", uri1, uri2);
        }

        public UnityWebRequests()
        {
        }

        public static UnityWebRequest Get(string urlServer, string uri)
        {
            //MorphoDebug.Log(" Get Request : " + UrlCombine(urlServer, uri));
            UnityWebRequest www = UnityWebRequest.Get(UrlCombine(urlServer, uri));
#if !UNITY_WEBGL || UNITY_EDITOR
            if (urlServer.StartsWith("https"))
                www.certificateHandler = new AcceptAllCertificatesSignedWithASpecificKeyPublicKey();
#endif
            return www;
        }

        public static UnityWebRequest Post(string urlServer, string uri, WWWForm form)
        {
            UnityWebRequest www = UnityWebRequest.Post(UrlCombine(urlServer, uri), form);
#if !UNITY_WEBGL || UNITY_EDITOR
            if (urlServer.StartsWith("https"))
                www.certificateHandler = new AcceptAllCertificatesSignedWithASpecificKeyPublicKey();
#endif
            return www;
        }

        public static UnityWebRequest Delete(string urlServer, string uri)
        {
            UnityWebRequest www = UnityWebRequest.Delete(UrlCombine(urlServer, uri));
#if !UNITY_WEBGL || UNITY_EDITOR
            if (urlServer.StartsWith("https"))
                www.certificateHandler = new AcceptAllCertificatesSignedWithASpecificKeyPublicKey();
#endif
            return www;
        }
    }

    public class UnityWebRequestAssetBundles
    {
        public static string UrlCombine(string uri1, string uri2)
        {
            if (uri1 == "")
            {
                return uri2;
            }

            if (uri2 == "")
            {
                return uri1;
            }

            uri1 = uri1.TrimEnd('/');
            uri2 = uri2.TrimStart('/');
            return string.Format("{0}/{1}", uri1, uri2);
        }

        public UnityWebRequestAssetBundles()
        {
        }

        public static UnityWebRequest GetAssetBundle(string urlServer, string uri, uint version, uint crc)
        {
            UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(UrlCombine(urlServer, uri), version, crc);

#if !UNITY_WEBGL || UNITY_EDITOR
            if (urlServer.StartsWith("https"))
                www.certificateHandler = new AcceptAllCertificatesSignedWithASpecificKeyPublicKey();
#endif
            return www;
        }
    }

    public class UnityWebRequestTextures
    {
        public static string UrlCombine(string uri1, string uri2)
        {
            if (uri1 == "")
            {
                return uri2;
            }

            if (uri2 == "")
            {
                return uri1;
            }

            uri1 = uri1.TrimEnd('/');
            uri2 = uri2.TrimStart('/');
            return string.Format("{0}/{1}", uri1, uri2);
        }

        public UnityWebRequestTextures()
        {
        }

        public static UnityWebRequest GetTexture(string urlServer, string uri)
        {
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(UrlCombine(urlServer, uri));
#if !UNITY_WEBGL || UNITY_EDITOR
            if (urlServer.StartsWith("https"))
                www.certificateHandler = new AcceptAllCertificatesSignedWithASpecificKeyPublicKey();
#endif
            return www;
        }
    }
}