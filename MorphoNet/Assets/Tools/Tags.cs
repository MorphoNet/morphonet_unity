namespace MorphoNet.Tools
{
    public static class Tags
    {
        public const string Scrollbar = "Scrobar";
        public const string SliderCutPosX = "SliderCutPosX";
        public const string SliderCutPosY = "SliderCutPosY";
        public const string SliderCutRotX = "SliderCutRotX";
        public const string SliderCutRotY = "SliderCutRotY";
        public const string Embryon = "Embryon";
        public const string create = "create";
        public const string UIManager = "UIManager";
    }
}