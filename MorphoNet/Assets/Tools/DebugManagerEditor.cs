﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
[CustomEditor(typeof(DebugManager))]
public class DebugManagerEditor : Editor
{
    private int choice;
    public string[] options = new string[]
    {
         "localhost", "morphonet.org", "morphonet.org"+"/Dev", "morpho.lirmm.fr","ns244.evxonline.net"
    };


    //choices loaded in the editor dropdown, to choose wich case will be loaded in "DebugManager" script
    public string[] dataset_options = new string[]
    {

      "humanKnee","Connectome","Arabido_DR5-YFP","Phallusia Mammillata gastrulation","ANISEED-EMBRYOS","SpheresAsPrimitives","SpheresAsParticles","EmilieCalibration","CubeRawPos","Astec - Pm1","Astec - Pm2","Astec - Pm3","Astec - Pm4","CellWall_t001_t008","Voxel Man Quality 0","140317-Patrick-St8-EFuser","140317-Patrick-St8-Otheruser","BodyParts3D","160707-Ralph-St8","160708-Aquila-St8","BioEmergences 070604a","BioEmergences 070418a","VirtualPlant-Mango-id57","Virtual Worm","Brain","170830-RalphRevision","Body 3D Parts","160708-Aquila-St8-200times","171028-Jude-St8","Ascidian Division Pattern Quiver","Epithelial Vertex Model 1 Divisions","VirtualPlant-Mango-id103","Jacques","TEST","C Elegans Membranes","Vertex-140317-Patrick-St8","Floral Meristem Registered","Drosophilia Tracking","Drosophila_Confocal","160708-Aquila-St8-200timePoints","Mango Tree","TEST-Dataset 627","Termites ( ae aze aze ae aze aze aeza)","Ants-241TimePoints","YR01 Floral Meristem User 19","Jude","Example_Phallusia","BioEmergences-081213h","NeoCortex","FlowerBud","ascidiela - 225","ascidiela - 279","Zebrafish Confocal","Pokemon","ASTEC PM 10","WIZARD TEST","140317-Patrick-St8 id 1152","WIZARD TEST time 1","Human Chir","Direct Plot","FULL DROSO 2D","argyris_data","argyris_tensor_data","Ritika_tensor_data","190919-EmilieProp_postR-19","Phallusia mammillata embryo (Wild type, live SPIM imaging, stages 8-17)","CellWall","testUploadMesh","PM_Stages","LiveMode","160708-Aquila-St8_2-64-10-4","myExternalData","uploadMorphoNetOrg","Floral Meristem Full backup","astecpm2","TestTextures","Drosophila melanogaster medulla connectome","Phallusia mammillata embryo (Wild type, live SPIM imaging, stages 8-17) 180","TEST2CHANNELBEN","TEST2CHANNELBEN2","Oozooid","Danio rerio brain","GIL","Platynereis","Gil-v2","p194","Halocynthia Roretzi"
    };
    public int data_set_choice;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        var prevChoice = (target as DebugManager).url;
        var indexFromPrev = System.Array.IndexOf(options,prevChoice);
        choice = EditorGUILayout.Popup("Servers", indexFromPrev, options);
        var prevDChoice = (target as DebugManager).dataset;
        var indexDFromPrev = System.Array.IndexOf(dataset_options, prevDChoice);
        data_set_choice = EditorGUILayout.Popup("Datasets", indexDFromPrev, dataset_options);
        // Save the changes back to the object
        var aClass = target as DebugManager;
        aClass.url = options[choice];
        aClass.dataset = dataset_options[data_set_choice];
        EditorUtility.SetDirty(target);

    }
}
#endif
