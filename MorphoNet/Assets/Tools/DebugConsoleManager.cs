using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace MorphoNet
{
    public class DebugConsoleManager : MonoBehaviour
    {
        private static int SB_MAX_LEN = 65000;
        public static DebugConsoleManager instance;

        private GameObject _Console;

        private Text _ConsoleContents;

        private StringBuilder sb;

        public void Awake()
        {
            if (instance == null)
            {
                instance = this;
                sb = new StringBuilder(65536);
                DontDestroyOnLoad(this.gameObject);
            }
            else
                Destroy(this.gameObject);
        }

        public void SendConsoleMessage(string message)
        {
            //sometimes flush stringbuilder if it is getting really too big
            if (sb.Length > SB_MAX_LEN)
            {
                sb.Clear();
            }

            bool check = true;
            if (_ConsoleContents == null || _Console == null)
            {
                check = CheckObjectReferences();
            }
            if (check)
            {
                if (!message.Contains(Environment.NewLine))
                    message += Environment.NewLine;
                sb.Append(message);
                _ConsoleContents.text = sb.ToString();
            }
            
        }

        public void ToggleConsole()
        {
            bool check = true;
            if (_ConsoleContents == null || _Console == null)
            {
                check = CheckObjectReferences();
            }
            if (check)
            {
                if (_Console.activeSelf)
                {
                    _Console.SetActive(false);
                }
                else
                {
                    _Console.SetActive(true);
                }
            }
        }

        private bool CheckObjectReferences()
        {
            if (StandaloneMainMenuInteractions.instance != null)
            {
                _Console = StandaloneMainMenuInteractions.instance.Console;
                _ConsoleContents = StandaloneMainMenuInteractions.instance.ConsoleContents;
                return true;
            }
            else if (InterfaceManager.instance != null)
            {
                _Console = InterfaceManager.instance.Console;
                _ConsoleContents = InterfaceManager.instance.ConsoleContents;
                return true;
            }
            else
            {
                MorphoDebug.LogError("Could not find console ref to write message!", 3);
            }
            return false;
        }

    }

}
