using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using MorphoNet;


public class DebugManager : MonoBehaviour
{
    //THIS IS DESTROYED IF EXECUTED IN WEBGL = NO MEMORY COST
    public bool is_active;
    public static DebugManager instance = null;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    [HideInInspector]public string url;
    [HideInInspector]public string dataset;

    public bool loaded_data = false;

    int min_time = -1;
    int is_bundle = -1;
    int max_time = -1;
    int spf = -1;
    int dt = -1;
    int id_dataset = -1;
    int id_type = -1;
    int id_owner = -1;
    int id_user = -1;
    int quality = -1;
    string embryo_name = "";
    string name_user = "";
    string surname_user = "";
    string obj = "";
    bool liveMode = false;
    int user_right = -1;
    string distant_server_adress = "";
    string distant_server_path = "";
    string token = "";
    //This will load selected data (on the editor sript dropdown) inside the viewer
    public void LoadTestParameters()
    {
        //if (LoadParameters.instance.id_dataset == 0)
        //{
            LoadParameters.instance.urlSERVER = "https://" + url + "/";
            if(url=="ns244.evxonline.net") LoadParameters.instance.urlSERVER = "http://" + url + "/";
        MorphoDebug.Log("URL loaded : " + LoadParameters.instance.urlSERVER);

            if (obj != "") { liveMode = true; }
            if (name_user != "") LoadParameters.instance.name_user = name_user;
            if (surname_user != "") LoadParameters.instance.surname_user = surname_user;
            if (embryo_name != "") LoadParameters.instance.embryo_name = embryo_name;
            if (quality != -1) LoadParameters.instance.quality = quality;
            if (id_user != -1) LoadParameters.instance.id_user = id_user;
            if (id_owner != -1) LoadParameters.instance.id_owner = id_owner;
            if (id_type != -1) LoadParameters.instance.id_type = id_type;
            if (id_dataset != -1) LoadParameters.instance.id_dataset = id_dataset;
            if (spf != -1) LoadParameters.instance.spf = spf;
            if (dt != -1) LoadParameters.instance.dt = dt;
            if (min_time != -1) {LoadParameters.instance.min_time = min_time;}
            if (max_time != -1) LoadParameters.instance.max_time = max_time;
            if (is_bundle != -1) LoadParameters.instance.is_bundle = is_bundle;
            if (obj != "") LoadParameters.instance.OBJ = obj;
            if (liveMode != false) LoadParameters.instance.liveMode = liveMode;
            if (user_right != -1) LoadParameters.instance.user_right = user_right;
            if (distant_server_adress != "") LoadParameters.instance.server_adress = distant_server_adress;
            if (distant_server_path != "") LoadParameters.instance.server_path = distant_server_path;
            if (token != "") LoadParameters.instance.user_token = token;

        loaded_data = true;
           MorphoDebug.Log("Parameters loaded from DebugManager !");
        //}
    }
    // Start is called before the first frame update
    void Start()
    {
        if(is_active)
        {

#if UNITY_EDITOR

            //MorphoDebug.Log("bundleType=" + LoadParameters.instance.bundle_type);
            id_owner = 1;

            //right
            name_user = "Faure";
            surname_user = "Emmanuel";
            id_user = 1;
            quality = 0;
            id_type = 0;

            //Each case here overrides parameters inside unity editor to start with a set of parameters, if you want to add a parameter, add a case here, and use the case name inside the array of DebugManagerEditor script
            switch (dataset)
            {
                //Patrick for debug
                case "140317-Patrick-St8-EFuser":
                    spf = 13963; id_type = 2; min_time = 1; max_time = 10; dt = 89; embryo_name = "140317-Patrick-St8"; id_dataset = 1; quality = 0; break;

                case "140317-Patrick-St8-Otheruser": spf = 13963; id_type = 2; min_time = 1; max_time = 10; dt = 89; embryo_name = "140317-Patrick-St8"; id_dataset = 1; id_user = 4; surname_user = "Laussu"; break;


                case "BodyParts3D":
                    embryo_name = "BodyParts3D"; id_dataset = 55; min_time = 0; max_time = 0; break;
                //RALPH
                case "160707-Ralph-St8":
                    id_type = 2; id_dataset = 31; min_time = 4; max_time = 95; spf = 13995; dt = 125; embryo_name = "160707-Ralph-St8"; quality = 4; break;
                //AQUILA
                case "160708-Aquila-St8":
                    id_type = 2; id_dataset = 42; min_time = 1; max_time = 3; embryo_name = "160708-Aquila-St8"; break;
                //BioEmergences Sea Urchin 070604a
                case "BioEmergences 070604a":
                    id_dataset = 50; min_time = 1; max_time = 86; embryo_name = "BioEmergences 070604a"; break;
                //BioEmergences Zebbrafish 070418a
                case "BioEmergences 070418a":
                    id_dataset = 56; min_time = 0; max_time = 10; is_bundle = 2; embryo_name = "BioEmergences 070418a"; break;
                case "VirtualPlant-Mango-id57":
                    id_dataset = 57; min_time = 0; max_time = 0; embryo_name = "VirtualPlant-Mango"; break;
                case "Virtual Worm":
                    id_dataset = 58; min_time = 0; max_time = 0; embryo_name = "Virtual Worm"; break;
                case "Brain":
                    id_dataset = 60; min_time = 0; max_time = 0; embryo_name = "Brain"; break;
                //Raph Revision
                //HUMAN 3D
                case "Body 3D Parts":
                    min_time = 0; max_time = 0; embryo_name = "Body 3D Parts"; quality = 4; id_dataset = 101; break;
                //Aquila 200 times points
                case "160708-Aquila-St8-200times":
                    id_type = 2; min_time = 1; max_time = 200; embryo_name = "160708-Aquila-St8"; id_dataset = 42; break;
                //Jude
                case "171028-Jude-St8":
                    id_type = 2; min_time = 1; max_time = 10; spf = 13963; dt = 89; embryo_name = "171028-Jude-St8"; id_dataset = 126; quality = 4; id_user = 4; break;
                //Quiover Devu
                case "Ascidian Division Pattern Quiver":
                    id_type = 2; min_time = 0; max_time = 4; embryo_name = "Ascidian Division Pattern Quiver"; id_dataset = 737; quality = 0; id_user = 1; break;
                //Bruno Model
                case "Epithelial Vertex Model 1 Divisions":
                    name_user = "Bruno"; surname_user = "Leggio"; id_user = 3; min_time = 1; max_time = 200; embryo_name = "Epithelial Vertex Model 1 Divisions"; id_dataset = 86; break;
                //Mango Virtual Plant
                case "VirtualPlant-Mango-id103":
                    id_dataset = 103; min_time = 0; max_time = 0; embryo_name = "VirtualPlant-Mango"; break;
                //Jacques
                case "Jacques":
                    id_dataset = 105; min_time = 0; max_time = 0; embryo_name = "Jacques"; break;
                case "TEST":
                    id_dataset = 107; min_time = 0; max_time = 0; embryo_name = "TEST"; id_user = -1; break;
                //Simulated Ants
                case "Ants":
                    id_dataset = 123; min_time = 0; max_time = 120; embryo_name = "Ants"; id_user = 21; break;
                //C Elegans Membranes
                case "C Elegans Membranes":
                    id_type = 16; min_time = 1; max_time = 4; embryo_name = "C Elegans Membranes"; id_dataset = 130; break;
                case "Vertex-140317-Patrick-St8":
                    id_type = 5; min_time = 1; max_time = 80; spf = 13963; dt = 89; embryo_name = "Vertex-140317-Patrick-St8"; id_dataset = 132; break;
                case "YR01 Floral Meristem":
                    id_type = 61; min_time = 0; max_time = 17; embryo_name = "YR01 Floral Meristem"; id_dataset = 124; id_user = 1; break;
                //Drosophil tracking
                case "Drosophilia Tracking":
                    id_type = 61; min_time = 0; max_time = 100; embryo_name = "Drosophilia Tracking"; id_dataset = 142; break;
                //STEGMAIER
                //Drosophila_Confocal
                case "Drosophila_Confocal":
                    id_type = 61; min_time = 0; max_time = 0; embryo_name = "Drosophila_Confocal"; id_dataset = 556; quality = 0; break;
                //MANGO TREES
                case "Mango Tree":
                    id_type = 0; min_time = 1; max_time = 16; embryo_name = "Mango Tree"; id_dataset = 380; id_user = 1; quality = 0; break;
                //Aquila
                case "160708-Aquila-St8-200timePoints":
                    id_type = 2; min_time = 1; max_time = 200; spf = 13963; dt = 89; embryo_name = "160708-Aquila-St8"; id_dataset = 42; quality = 4; break;
                case "TEST-Dataset 627":
                    id_dataset = 627; min_time = 0; max_time = 0; embryo_name = "TEST"; id_user = -1; break;
                case "Termites ( ae aze aze ae aze aze aeza)":
                    id_dataset = 143; min_time = 0; max_time = 0; id_user = 1; embryo_name = "Termites ( ae aze aze ae aze aze aeza)"; id_user = -1; break;
                case "Ants-241TimePoints":
                    id_type = 62; min_time = 0; max_time = 241; embryo_name = "Ants"; id_dataset = 123; break;
                case "Floral Meristem Registered":
                    id_type = 61; min_time = 0; max_time = 7; embryo_name = "Floral Meristem Registered"; id_dataset = 1152; id_user = 3; break;
                case "Jude":
                    id_type = 2; min_time = 0; max_time = 100; embryo_name = "Jude"; id_dataset = 126; id_user = 4; quality = 4; break;
                case "Example_Phallusia":
                    id_type = 1; min_time = 2; max_time = 3; embryo_name = "Example_Phallusia"; id_dataset = 128; id_user = 3; quality = 0; break;
                case "BioEmergences-081213h":
                    id_type = 1; min_time = 0; max_time = 83; embryo_name = "BioEmergences-081213h"; id_dataset = 133; id_user = 1; quality = 0; break;
                case "NeoCortex":
                    id_type = 1; min_time = 0; max_time = 0; embryo_name = "NeoCortex"; id_dataset = 134; id_user = 1; quality = 0; break;
                case "FlowerBud":
                    id_type = 1; min_time = 1; max_time = 30; embryo_name = "FlowerBud"; id_dataset = 204; id_user = 1; quality = 0; break;
                case "ascidiela - 225":
                    id_type = 1; min_time = 1; max_time = 41; embryo_name = "ascidiela"; id_dataset = 225; id_user = 1; quality = 0; break;
                case "ascidiela - 279":
                    id_type = 2; min_time = 1; max_time = 41; embryo_name = "ascidiela"; id_dataset = 279; id_user = 4; quality = 0; break;
                case "Zebrafish Confocal":
                    id_type = 2; min_time = 0; max_time = 0; embryo_name = "Zebrafish Confocal"; id_dataset = 745; id_user = 1; quality = 0; break;
                case "Pokemon":
                    id_type = 2; min_time = 0; max_time = 0; embryo_name = "Pokemon"; id_dataset = 951; id_user = 1; quality = 0; user_right = 2; break;
                case "ASTEC PM 10":
                    id_type = 2; min_time = 0; max_time = 10; spf = -1; dt = 117; embryo_name = "ASTEC PM 10"; id_dataset = 1049; quality = 2; id_user = 4; break;
                case "WIZARD TEST time 0":
                    id_type = 2; min_time = 0; max_time = 0; spf = -1; dt = 117; embryo_name = "WIZARD TEST"; id_dataset = 1; quality = 0; id_user = 1;break;
                case "140317-Patrick-St8 id 1152":
                    id_type = 0; min_time = 0; max_time = 17; embryo_name = "140317-Patrick-St8"; id_dataset = 1152; quality = 0;break;
                case "WIZARD TEST time 1":
                    id_type = 2; min_time = 1; max_time = 1; spf = -1; dt = 117; embryo_name = "WIZARD TEST"; id_dataset = 1; quality = 0; id_user = 1; break;
                case "Human Chir":
                    id_type = 0; min_time = 1; max_time = 8; embryo_name = "Human Chir"; id_dataset = 1180; quality = 0; break;
                case "Direct Plot":
                    id_type = 0; min_time = 0; max_time = 0; embryo_name = "Morpho Plot"; id_dataset = 0; quality = 0; break;
                case "FULL DROSO 2D":
                    id_type = 0; min_time = 9; max_time = 11; embryo_name = "FULL DROSO 2D"; id_dataset = 1249; quality = 0;break;
                case "argyris_data":
                    id_dataset = 1292; embryo_name = "argyris_data";min_time = 0;max_time = 0;id_type = 1; break;
                case "argyris_tensor_data":
                    id_dataset = 1320; embryo_name = "Argyris tensor data";min_time = 0; max_time = 0;id_type = 1;break;
                case "Ritika_tensor_data":
                    id_dataset = 1309;embryo_name = "Ritika_tensor_data";min_time = 0; max_time = 0; id_type = 0; quality = 0; break;
                case "190919-EmilieProp_postR-19":
                    id_dataset = 1300;embryo_name = "190919-EmilieProp_postR-19";min_time = 1; max_time = 15;id_type = 0; quality = 0;break;
                case "Phallusia mammillata embryo (Wild type, live SPIM imaging, stages 8-17)":
                    id_dataset = 1; embryo_name = "Phallusia mammillata embryo(Wild type, live SPIM imaging, stages 8 - 17)"; min_time = 1; max_time =80; id_type = 2; spf = 13963; dt = 89;/*id_user = 0; user_right= 2;*/ break;
                case "CellWall":
                    id_dataset = 1387; embryo_name = "Cell Wall"; min_time = 0; max_time = 0; break;
                case "Voxel Man Quality 0":
                    id_dataset = 738; embryo_name = "Voxel Man"; min_time = 0; max_time = 0; quality = 0; break;
                case "CellWall_t001_t008":
                    id_dataset = 1422; embryo_name = "CellWall_t001_t008"; min_time = 1; max_time = 8; quality = 0; break;
                case "Astec - Pm4":
                    id_dataset = 1048; embryo_name = "Astec - Pm4"; min_time = 1; max_time = 20; quality = 0; break;
                case "Astec - Pm3":
                    id_dataset = 1043; embryo_name = "Astec - Pm3"; min_time = 1; max_time = 20; quality = 0; break;
                case "Astec - Pm2":
                    id_dataset = 1040; embryo_name = "Astec - Pm2"; min_time = 1; max_time = 2; quality = 0; break;
                case "Astec - Pm1":
                    id_dataset = 1095; embryo_name = "Astec - Pm1"; min_time = 1; max_time = 25; quality = 0; id_type = 2; spf = 14400;dt = 89;break;
                case "CubeRawPos":
                    id_dataset = 1452; embryo_name = "CubeRawPos"; min_time = 0; max_time = 1; quality = 0; break;
                case "testUploadMesh":
                    id_dataset = 1619; embryo_name = " "; min_time = 0; max_time = 1; quality = 0;break;
                case "PM_Stages":
                    id_dataset = 1062; embryo_name = "ASTEC PM_STAGES"; min_time = 0; max_time = 15;quality = 0; break;
                case "EmilieCalibration":
                    id_dataset = 1455; embryo_name = "EmilieCalibration"; min_time = 0; max_time = 10; quality = 0; break;
                case "SpheresAsParticles":
                    id_dataset = 1473; embryo_name = "SpheresAsParticles"; min_time = 0; max_time = 70; quality = 0; break;
                case "SpheresAsPrimitives":
                    id_dataset = 1474; embryo_name = "SpheresAsPrimitives"; min_time = 0; max_time = 100; quality = 0; break;

                case "ANISEED-EMBRYOS":
                    id_dataset = 1062; embryo_name = "Astec-Pm-Stages (Mosaic of wild type Phallusia mammillata embryos, live SPIM imaging, stages 5a-17)";  id_type = 2; min_time = 1; max_time = 15; quality = 0; dt = -1; spf = -1; break;
                case "Phallusia Mammillata gastrulation":
                    id_dataset = 1518; embryo_name = "Phallusia Mammillata gastrulation"; min_time = 1; max_time = 95; quality = 0; break;

                case "TESTUPLOADMULTIPLEMESHES":
                    id_dataset = 1536; embryo_name = "TESTUPLOADMULTIPLEMESHES"; min_time = 0; max_time = 10; quality = 0; break;
                case "LiveMode":
                    id_dataset = -2; embryo_name = "LiveMode";min_time = 0;max_time = 0; id_owner = 1; obj = "g 0 1 0\n v 0.000000E+00 0.000000E+00 78.0000\nv 45.0000 45.0000 0.000000E+00\nv 45.0000 - 45.0000 0.000000E+00\nv - 45.0000 - 45.0000 0.000000E+00\nv - 45.0000 45.0000 0.000000E+00\nv 0.000000E+00 0.000000E+00 - 78.0000\nf     1 2 3\nf     1 3 4\nf     1 4 5\nf     1 5 2\nf     6 5 4\nf     6 4 3\nf     6 3 2\nf     6 2 1\nf     6 1 5";  break;
                case "160708-Aquila-St8_2-64-10-4":
                    id_dataset = 274; embryo_name = "160708-Aquila-St8_2-64-10-4";min_time = 0;max_time = 33;quality = 0;
                    break;
                case "myExternalData":
                    id_dataset = 1552; distant_server_adress = "macmanu.freeboxos.fr"; distant_server_path = "/Users/morphonet/Desktop/DATA_MORPHONET"; min_time = 0; max_time = 0; quality = 0; id_owner = 1; embryo_name = "MyExternalData"; break;
                case "uploadMorphoNetOrg":
                    id_dataset = 1554; distant_server_adress = "morphonet.org"; distant_server_path = "/var/www/html/DATA_MN2"; min_time = 0; max_time = 0; quality = 0; id_owner = 1; embryo_name = "TestMorphoNetORG"; break;
                case "Floral Meristem Full backup":
                    id_dataset = 1420; min_time = 0; max_time = 13; quality = 0;id_owner = 1; embryo_name = "Floral meristem full backup"; break;
                case "astecpm2":
                    id_dataset = 1040;min_time = 0;max_time = 13;quality = 0; id_owner = 1;embryo_name = "Astec PM2";break;
                case "Arabido_DR5-YFP":
                    id_dataset = 183; min_time = 0; max_time = 0; quality = 0; id_owner = 1; embryo_name = "Arabido_DR5-YFP"; break;
                case "TestTextures":
                    id_dataset = 1561; min_time = 0; max_time = 0; quality = 0; id_owner = 1; embryo_name = "TestTextures"; break;
                case "Drosophila melanogaster medulla connectome":
                    id_dataset = 301; min_time = 0; max_time = 0; quality = 0; id_owner = 1; embryo_name = "Drosophila melanogaster medulla connectome"; break;
                case "Phallusia mammillata embryo (Wild type, live SPIM imaging, stages 8-17) 180":
                    id_dataset = 1; embryo_name = "Phallusia mammillata embryo(Wild type, live SPIM imaging, stages 8 - 17)"; min_time = 1; max_time = 180; id_type = 2; spf = 13963; dt = 89; break;
                case "TEST2CHANNELBEN":
                    id_dataset = 1618; embryo_name = "TESTUPLOADMULTIPLECHANNELSBEN";id_owner = 1; id_type = 1 ; break;
                case "TEST2CHANNELBEN2":
                    id_dataset = 1619; embryo_name = "TESTUPLOADMULTIPLECHANNELSBEN2"; id_owner = 1; id_type = 1; break;
                case "Oozooid":
                    id_dataset = 1187; embryo_name = "OOZOID"; id_owner = 1; id_type = 1; break;
                case "Danio rerio brain":
                    id_dataset = 179; embryo_name = "Danio rerio brain"; id_owner = 1; id_type = 1; break;
                case "GIL":
                    id_dataset = 1645; embryo_name = "GIL"; id_owner = 1; id_type = 1; break;
                case "Platynereis":
                    id_dataset = 1660; embryo_name = "Platynereis"; id_owner = 1; min_time = 1; max_time = 20; id_type = 1;break;
                case "Gil-v2":
                    id_dataset = 1655; embryo_name = "200901-Gil ( CORR_INT_POST_V2 )"; id_owner = 1; min_time = 0;max_time = 25;id_type = 1;  break;
                case "p194":
                    id_dataset = 1725; embryo_name = "p194"; id_owner = 1; min_time = 0; max_time = 5; id_type = 1; break;
                case "Halocynthia Roretzi":
                    id_dataset = 1730; embryo_name = "Halocynthia Roretzi"; id_owner = 1; min_time = 0; max_time = 0; break;
                case "patrick-test":
                  id_dataset = 1809; embryo_name = "ConvertPatrick"; id_owner = 85; min_time = 0; max_time = 2; break;
                case "Connectome":
                  id_dataset = 1770; embryo_name = "Connectome"; id_owner = 1; min_time = 0; max_time = 0; break;
                case "humanKnee":
                    id_dataset = 108; embryo_name = "humanKnee"; id_owner = 1; min_time = 0; max_time = 0; break;

            }

            /// Patrick in API MODE
            /*  if (id_dataset == 0) {
                  id_user = -1;//PUBLIC ACCESS
                  API_Menu = "Genetic";
                  API_Time = 9;
                  API_Genes = new List<string>();
                  API_Genes.Add("Cirobu.g00006940");
                  API_Genes.Add("Cirobu.g00011105");*/

            //API_Territory = "Epidermis";
            // API_Fate = "Epidermis";
            // ArcBall.InitialRotation = new Quaternion(0.6f, 0.2f, 0.1f, 0.8f);
            //id_type = 2; min_time = 1; max_time = 15; spf = -1; dt = -1; embryo_name = "ANISEED GOLD"; id_dataset = 1062; quality = 0;
            // }

            LoadTestParameters();

#endif
        }
    }

}
