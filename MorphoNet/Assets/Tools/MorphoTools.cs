using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using UnityEngine.Networking;
using System;
using System.Linq;
using AssemblyCSharp;
using UnityEngine.UI;

namespace MorphoNet
{
    public class MorphoTools : MonoBehaviour
    {
        //global accessor to current selected dataset (modify it when the soft will be able to handle 2+ datasets)
        //called with MorphoTools.get_dataset() anywhere
        //an idea will be to have a function to check which dataset has focus and return it

        public static void CreateTimePoint(int time)
        {
            if (GetDataset() != null)
                if(GetDataset().mesh_by_time.ContainsKey(time))
                    DataLoader.instance.CreateTimePoint(GetDataset().embryo_container.transform, GetDataset().mesh_by_time[time].transform,time);
        }





        /// <summary>
        /// Proxy to send an information to the lineage , USE THIS INSTEAD OF DIRECT CALL TO THE LINEAGE VIEWER FUNCTION
        /// </summary>
        /// <param name="id_info">id of the info to get values and that will be sent</param>
        public static void SendInfoToLineage(int id_info)
        {
            if (InterfaceManager.instance.lineage_viewer != null)
                InterfaceManager.instance.lineage_viewer.SetIsDisplayed(true, id_info);

            if (InterfaceManager.instance.lineage_viewer != null && InterfaceManager.instance.lineage_viewer.lineage_shown)
            {
                InterfaceManager.instance.lineage_viewer.SendInfo(id_info);
            }
        }

        /// <summary>
        /// Remove the info in cells in the lineage for standalone , USE THIS INSTEAD OF DIRECT CALL TO THE LINEAGE VIEWER FUNCTION
        /// </summary>
        /// <param name="id_info">id of the info to remove</param>
        public static void ClearInfoInLineage(int id_info)
        {
            if (InterfaceManager.instance.lineage_viewer != null)
                InterfaceManager.instance.lineage_viewer.SetIsDisplayed(false, id_info);

            if (InterfaceManager.instance.lineage_viewer != null && InterfaceManager.instance.lineage_viewer.lineage_shown)
            {
                InterfaceManager.instance.lineage_viewer.ClearInfo(id_info);
            }
        }
        /// <summary>
        /// Proxy to send an visibility status to the lineage , USE THIS INSTEAD OF DIRECT CALL TO THE LINEAGE VIEWER FUNCTION
        /// </summary>
        /// 
        public static void SendCellsVisibilityToLineage()
        {
            if (InterfaceManager.instance.lineage_viewer != null && InterfaceManager.instance.lineage_viewer.lineage_shown)
            {
                InterfaceManager.instance.lineage_viewer.SendCellsHidden();
            }
        }

        /// <summary>
        /// Proxy to send an picked status to the lineage , USE THIS INSTEAD OF DIRECT CALL TO THE LINEAGE VIEWER FUNCTION
        /// </summary>
        /// 
        public static void SendCellsPickedToLineage(bool zoom = true)
        {
            if (InterfaceManager.instance.lineage_viewer != null && InterfaceManager.instance.lineage_viewer.lineage_shown)
            {
                InterfaceManager.instance.lineage_viewer.SendCellsPicked(zoom);
            }
        }

        /// <summary>
        /// Proxy to send an picked status to the lineage , USE THIS INSTEAD OF DIRECT CALL TO THE LINEAGE VIEWER FUNCTION
        /// </summary>
        /// 
        public static void SendClearPickedToLineage(List<Cell> cells)
        {
            if (InterfaceManager.instance.lineage_viewer != null && InterfaceManager.instance.lineage_viewer.lineage_shown)
            {
                InterfaceManager.instance.lineage_viewer.SendClearPicked(cells);
            }
        }

        /// <summary>
        /// Proxy to send an color status to the lineage , USE THIS INSTEAD OF DIRECT CALL TO THE LINEAGE VIEWER FUNCTION
        /// </summary>
        /// 
        public static void SendCellsColorToLineage()
        {
            if (InterfaceManager.instance.lineage_viewer != null && InterfaceManager.instance.lineage_viewer.lineage_shown)
            {
                InterfaceManager.instance.lineage_viewer.SendCellsColor();
            }
        }
       
        public static IEnumerator sendPythonCommandPyinstaller(string action, string data)
        {
# if !UNITY_WEBGL
            WWWForm form = new WWWForm();
            form.AddField("action", action);
            form.AddField("time", MorphoTools.GetDataset().CurrentTime);
            form.AddField("data", data);

            UnityWebRequest www = UnityWebRequests.Post("localhost:" + 9874, "/ send.html", form);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                MorphoDebug.LogError("Error sending command ! "+action+" : " + www.error);
                MorphoDebug.LogError("Need to restart whole standalone");
            }
               
            else
            {
                MorphoDebug.Log("Successfully sent command "+action);
                //String commande = Encoding.UTF8.GetString(www.downloadHandler.data);
                //if (btn.name != "showraw") UIManager.instance.MenuObjects.transform.Find("Selection").GetComponent<SelectionColor>().clearAllColors();
            }

            www.Dispose();
#else
            yield return null;
#endif
        }
        public static string lstrip(string s, char c)
        {
            string result = s;
            while (result[0] == c && result.Length > 0)
            {
                result = result.Substring(1, result.Length - 1);
            }
            return result;
        }

        public static List<string> ComputeChildrenNamesKonklin(string name)
        {
            List<string> result = new List<string>();
            if (name == null || name == "" || !Regex.IsMatch(name, @"^[aAbB]\d{1,2}[.][0-9999]"))
            {
                result.Add("None");
                return result;
            }

            char abvalue = name.Split('.')[0][0];
            string stage = name.Split('.')[0].Substring(1, name.Split('.')[0].Length - 1);

            string p = name.Split('.')[1].Substring(0, 4);

            char lrvalue = name.Split('.')[1][4];
            int i_stage = 0;
            if (!int.TryParse(stage, out i_stage))
            {
                result.Add("None");
                return result;
            }

            int i_p = 0;
            if (!int.TryParse(p, out i_p))
            {
                result.Add("None");
                return result;
            }

            int new_stage = i_stage + 1;

            string d1 = abvalue.ToString() + new_stage + ".";
            string d2 = abvalue.ToString() + new_stage + ".";

            d1 += String.Format("{0,4}", (2 * i_p - 1).ToString("D4"));
            d2 += String.Format("{0,4}", (2 * i_p).ToString("D4"));

            d1 += lrvalue;
            d2 += lrvalue;

            result.Add(d1);
            result.Add(d2);

            return result;
        }

        public static string ComputeParentNameKonklin(string name)
        {
            if (name == null || name == "" || !Regex.IsMatch(name, @"^[aAbB]\d{1,2}[.][0-9999]"))
            {
                return "None";
            }

            char abvalue = name.Split('.')[0][0];
            string stage = name.Split('.')[0].Substring(1, name.Split('.')[0].Length - 1);

            string p = name.Split('.')[1].Substring(0, 4);

            char lrvalue = name.Split('.')[1][4];

            int i_stage = 0;
            if (!int.TryParse(stage, out i_stage))
            {
                return "None";
            }
            string parent = abvalue.ToString() + (i_stage - 1) + ".";
            int i_p = 0;
            if (!int.TryParse(p, out i_p))
            {
                return "None";
            }

            if (i_p % 2 == 1)
            {
                parent += String.Format("{0,4}", ((i_p + 1) / 2).ToString("D4"));
            }
            else
            {
                parent += String.Format("{0,4}", ((i_p) / 2).ToString("D4"));
                //parent += String.Format("{:0{width}{0}}", ((i_p) / 2), 4);
            }

            parent += lrvalue;
            return parent;
        }

        public static int GetGenInName(string name)
        {
            if (name == null || name == "" || !Regex.IsMatch(name, @"^[aAbB]\d{1,2}[.][0-9999]"))
            {
                return -1;
            }

            char abvalue = name.Split('.')[0][0];

            string stage = name.Split('.')[0].Substring(1, name.Split('.')[0].Length - 1);

            int i_stage;

            if (!int.TryParse(stage, out i_stage))
            {
                return -1;
            }
            return i_stage;
        }

        public static string FindParentName(string cell_name)
        {
            string cell_to_treat = cell_name;
            while (cell_to_treat != null && cell_to_treat != "None" && cell_to_treat != "" && GetGenInName(cell_to_treat) >= GetGenInName(cell_name))
            {
                cell_to_treat = ComputeParentNameKonklin(cell_to_treat);
            }

            return cell_to_treat;
        }

        public static List<string> FindSisterNames(string cell_name)
        {
            List<string> sisters_names = new List<string>();
            string parent = FindParentName(cell_name);
            sisters_names = ComputeChildrenNamesKonklin(parent);
            sisters_names.Remove(cell_name);
            return sisters_names;
        }

        public static bool isAnterior(string cell_name)
        {
            string cell_to_treat = cell_name;
            while (cell_to_treat != null && cell_to_treat != "None" && cell_to_treat != "" && GetGenInName(cell_to_treat) > 4)
            {
                cell_to_treat = ComputeParentNameKonklin(cell_to_treat);
            }

            if (cell_to_treat.StartsWith("a4.0001") || cell_to_treat.StartsWith("b4.0001"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsObjectInFrontOfPlane(Vector3 test_object, Transform plan)
        {
            var heading = test_object - plan.position;
            float dot = Vector3.Dot(heading, plan.forward);
            return (dot > 0);
        }

        public static Dictionary<TKey, TValue> AddKeyToDict<TKey, TValue>(Dictionary<TKey, TValue> dict, TKey key)
        {
            if (dict == null)
            {
                dict = new Dictionary<TKey, TValue>();
            }

            if (!dict.ContainsKey(key))
            {
                dict.Add(key, default(TValue));
            }

            return dict;
        }

        public static List<Cell> FindSisterCells(Cell cell)
        {
            List<Cell> sisters = new List<Cell>();
            //	MorphoDebug.Log("Start search for : " + temp_clickedCells[i].getName());
            sisters = FindSisterCellsLineage(cell);

            if (sisters == null || sisters.Count == 0)
            {
                sisters = FindSisterCellsUsingName(cell);
            }
            //MorphoDebug.Log ("CEll " + temp_clickedCells [i].ID);
            return sisters;
        }

        public static List<Cell> FindSisterCellsLineage(Cell cell)
        {
            List<Cell> sisters = new List<Cell>();
            //	MorphoDebug.Log("Start search for : " + temp_clickedCells[i].getName());
            sisters = StartSearchInMothers(cell, cell);
            //MorphoDebug.Log ("CEll " + temp_clickedCells [i].ID);
            return sisters;
        }

        public static List<Cell> FindSisterCellsUsingName(Cell cell)
        {
            List<Cell> sisters = new List<Cell>();
            string cell_name = "";

            if (cell != null && cell.Infos != null)
            {
                foreach (KeyValuePair<int, string> pair in cell.Infos)
                {
                    Correspondence c = GetDataset().Infos.Correspondences.Find(corr => { return corr.id_infos == pair.Key; });
                    if (c.name.ToLower().Contains("name"))
                    {
                        cell_name = pair.Value;
                    }
                }

                string parent = FindParentName(cell_name);
                List<string> sister_names = ComputeChildrenNamesKonklin(parent);

                foreach (Cell c in GetDataset().CellsByTimePoint[cell.t])
                {
                    foreach (KeyValuePair<int, string> pair3 in c.Infos)
                    {
                        Correspondence correspondence = GetDataset().Infos.Correspondences.Find(corr => { return corr.id_infos == pair3.Key; });

                        if (correspondence.name.ToLower().Contains("name")
                            && sister_names.Contains(pair3.Value)
                            && !sisters.Contains(c))
                        {
                            sisters.Add(c);
                        }
                    }
                }
            }
            return sisters;
        }

        public static List<Cell> StartSearchInMothers(Cell source, Cell current)
        {
            List<Cell> result = null;

            if (current == null)
            { return null; }
            if (current.t == GetDataset().MinTime)
            {
                return null;
            }

            if (current.Mothers != null)
            {
                foreach (Cell cell in current.Mothers)
                {
                    //MorphoDebug.Log("Start search in mothers for " + cell.getName());
                    List<Cell> new_result = new List<Cell>();
                    new_result = FindSisterInDaughters(source, cell);
                    if (new_result != null && new_result.Count > 0)
                    {
                        //	MorphoDebug.Log("Found "+ new_result.Count +" sisters");
                        if (result == null)
                        {
                            result = new List<Cell>();
                        }
                        foreach (Cell b in new_result)
                        {
                            if (!result.Contains(b) && b != source)
                            {
                                //MorphoDebug.Log("found a sister in mothers for " + cell.getName());
                                result.Add(b);
                            }
                        }
                    }
                }
            }

            if (result == null || result.Count == 0)
            {
                if (current.Mothers != null)
                {
                    foreach (Cell m in current.Mothers)
                    {
                        List<Cell> next_mother = null;
                        next_mother = StartSearchInMothers(source, m);
                        if (next_mother != null && next_mother.Count > 0)
                        {
                            if (result == null)
                            {
                                result = new List<Cell>();
                            }
                            foreach (Cell b in next_mother)
                            {
                                if (!result.Contains(b) && b != source)
                                {
                                    result.Add(b);
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }

        public static List<Cell> FindSisterInDaughters(Cell source, Cell current)
        {
            List<Cell> sisters = null;

            if (current == null)
            {
                return null;
            }

            if (current.t == source.t)
            {
                sisters = new List<Cell>();
                sisters.Add(current);
                return sisters;
            }

            if (current.Daughters != null)
            {
                foreach (Cell cell in current.Daughters)
                {
                    //MorphoDebug.Log("Search in daughter : " + cell.getName());
                    List<Cell> result = new List<Cell>();
                    result = FindSisterInDaughters(source, cell);
                    if (result != null && result.Count > 0)
                    {
                        if (sisters == null)
                        {
                            sisters = new List<Cell>();
                        }
                        foreach (Cell b in result)
                        {
                            if (b != source)
                            {
                                //MorphoDebug.Log("found a sister in daughter : " + cell.getName());
                                sisters.Add(b);
                            }
                        }
                    }
                }
            }
            return sisters;
        }

        /*
        public static string ShortCutCellName(string c_name)
        {
            string result = c_name;
            string[] splitted = c_name.Split('.');
            result = splitted[0] + "." + lstrip(splitted[1], '0');
            return result;
        }*/

        public static string ShortCutCellName(string c_name)
        {
            string result = c_name;
            if (Regex.IsMatch(c_name, @"^[aAbB]\d{1,2}[.][0-9999]"))
            {
                string[] splitted = c_name.Split('.');
                result = splitted[0] + "." + lstrip(splitted[1], '0');
            }
            return result;
        }

        public static DataSet GetDataset()
        {
            return SetsManager.instance.DataSet;
        }

        //global accessor to current selected dataset genetic information manager(modify it when the soft will be able to handle 2+ datasets)
        //called with MorphoTools.get_genetic_manager() anywhere
        public static GeneticManager GetGeneticManager()
        {
            return GetDataset().GeneticManager;
        }

        //Tool function,
        public static string ConvertTimeToHPF(int t)
        {
            return (Math.Round(100.0 * (t * GetDataset().dt + GetDataset().spf) / 3600.0) / 100.0).ToString() + "hpf";
        }

        //global accessor to current selected dataset picked cell manager(modify it when the soft will be able to handle 2+ datasets)
        //called with MorphoTools.get_picked_manager() anywhere
        public static PickedManager GetPickedManager()
        {
            return GetDataset().PickedManager;
        }

        public static Recording GetRecorder()
        {
            return GetDataset().Recorder;
        }

        public static TransformationsManager GetTransformationsManager()
        {
            return GetDataset().TransformationsManager;
        }

        public static DataSetInformations GetInformations()
        {
            return GetDataset().Infos;
        }

        public static Mutation GetMutations()
        {
            return GetDataset().Mutations;
        }

        public static string GetServer()
        {
            return SetsManager.instance.urlSERVER;
        }

        public static RawImages GetRawImages()
        {
            return GetDataset().RawImages;
        }

        public static ScatterView GetScatter()
        {
            return GetDataset().Scatter;
        }

        public static DevelopmentTable GetDevelopmentalTable()
        {
            return GetDataset().DevTable;
        }

        public static FigureManager GetFigureManager()
        {
            return GetDataset().FigureManager;
        }

        public static string Rot39(string input)
        {
            // This string contains 78 different characters in random order.
            var mix = "QDXkW<_(V?cqK.lJ>-*y&zv9prf8biYCFeMxBm6ZnG3H4OuS1UaI5TwtoA#Rs!,7d2@L^gNhj)EP$0";
            var result = (input ?? "").ToCharArray();
            for (int i = 0; i < result.Length; ++i)
            {
                int j = mix.IndexOf(result[i]);
                result[i] = (j < 0) ? result[i] : mix[(j + 39) % 78];
            }
            return new string(result);
        }

        public static bool GetPlotIsActive()
        {
            if (SetsManager.instance.directPlot != null)
            {
                return SetsManager.instance.directPlot.IsActive;

            }
            return false;
        }
        
        public static DirectPlot GetPlot()
        {
            if (SetsManager.instance.directPlot != null)
            {
                return SetsManager.instance.directPlot;

            }
            return null;
        }


        public static GameObject GetBackground()
        {
            return SetsManager.instance.Background;
        }

        public static bool FormatReturn(int i)
        {
            if (i == 1)
            {
                return true;
            }

            return false;

        }
        public static bool NaturalCompare(string x, string y)
        {
            Regex _re = new Regex(@"(?<=\D)(?=\d)|(?<=\d)(?=\D)", RegexOptions.Compiled);
            x = x.ToLower();
            y = y.ToLower();
            if(string.Compare(x, 0, y, 0, Math.Min(x.Length, y.Length)) == 0)
            {
                if(x.Length == y.Length) return false;
                return x.Length < y.Length ? false : true;
            }
            var a = _re.Split(x);
            var b = _re.Split(y);
            int i = 0;
            while(true)
            {
                int r = PartCompare(a[i], b[i]);
                if(r != 0) return FormatReturn(r);
                ++i;
            }
        }

        
        private static int PartCompare(string x, string y)
        {
            int a, b;
            if(int.TryParse(x, out a) && int.TryParse(y, out b))
                return a.CompareTo(b);
            return x.CompareTo(y);
        }

        public static string GetVersionNumber()
        {
            return LoadParameters.instance.VersionNumberAsset.text;
        }

        public static string GetFullVersionNumber()
        {
            return "Version "+GetVersionNumber()+" - "+LoadParameters.instance.GitVersionNumberAsset.text;
        }
    }
}