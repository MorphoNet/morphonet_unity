using MorphoNet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEngine;

public class InitStandalone : MonoBehaviour
{
#if UNITY_STANDALONE
#if UNITY_STANDALONE_OSX
    private static string _STANDALONE_APP = Path.Combine(Application.dataPath, "STANDALONE", "MorphoNet_Standalone.app", "Contents", "MacOS", "MorphoNet_Standalone");
    
#elif UNITY_STANDALONE_WIN
    private static string _STANDALONE_APP = Path.Combine(Directory.GetParent(Application.dataPath).FullName, "libs", "STANDALONE", "MorphoNet_Standalone", "MorphoNet_Standalone.exe");
#elif UNITY_STANDALONE_LINUX
    private static string _STANDALONE_APP = Path.Combine(Directory.GetParent(Application.dataPath).FullName,"libs", "STANDALONE", "MorphoNet_Standalone", "MorphoNet_Standalone");
#endif

    /// <summary>
    /// linux execute command function
    /// </summary>
    /// <param name="command"></param>

    public static void ExecuteCommand(string command)
    {
        Process proc = new System.Diagnostics.Process();
        proc.StartInfo.FileName = "gnome-terminal"; // File to execute
        //proc.StartInfo.Arguments = "-- " + command;
        proc.StartInfo.Arguments = "-- sh -c \"bash -c '" + command + "; exec bash'\"";
        proc.StartInfo.UseShellExecute = false;
        proc.StartInfo.RedirectStandardOutput = true;
        proc.Start();

    }



    [RuntimeInitializeOnLoadMethod]
    static void OnRuntimeMethodLoad()
    {
        //Application.targetFrameRate = -1;
        Application.targetFrameRate = Screen.currentResolution.refreshRate;
        //also clear log files if you are not in webGL!
#if !UNITY_WEBGL
        MorphoDebug.SetupLogOnLaunch();
#endif

        if (!_STANDALONE_APP.ToLower().Contains("http"))
        {
            try
            {

                MorphoDebug.Log("opening MN standalone app @ " + _STANDALONE_APP);


#if UNITY_STANDALONE_OSX
                try
                {
                    System.Diagnostics.Process process = new System.Diagnostics.Process();
                    process.StartInfo.FileName = "open";
                    if (_STANDALONE_APP.StartsWith(Path.DirectorySeparatorChar.ToString()))
                        process.StartInfo.Arguments = _STANDALONE_APP;
                    else
                        process.StartInfo.Arguments = Path.DirectorySeparatorChar + _STANDALONE_APP;
                    process.Start();
                }
                catch(Exception e)
                {
                    MorphoDebug.LogError("Could not open MN app in terminal mode, opening in background");
                    System.Diagnostics.Process.Start(_STANDALONE_APP);
                }
                
#else
#if UNITY_STANDALONE_WIN
                Application.OpenURL(_STANDALONE_APP);

#elif UNITY_STANDALONE_LINUX
                ExecuteCommand(_STANDALONE_APP);
#endif
#endif
            }
            catch (Exception e)
            {
                MorphoDebug.LogError("COULD NOT OPEN MN STANDALONE APP : " + e);
            }
        }


    }
#endif

}