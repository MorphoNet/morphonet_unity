namespace MorphoNet.Tools
{
    public static class Layers
    {
        public const string Default = "Default";
        public const string TransparentFX = "TransparentFX";
        public const string IgnoreRaycast = "Ignore Raycast";
        public const string Water = "Water";
        public const string UI = "UI";
        public const string TeleportationLayer = "TeleportationLayer";
        public const string LineageButton = "LineageButton";
        public const string LineageViewer = "LineageViewer";
        public const string GridLine = "GridLine";
        public const string LastHit = "LastHit";
        public const string VROnly = "VROnly";
        public const string CentralGaze = "CentralGaze";
    }
}