using UnityEngine;

namespace MorphoNet
{
    /// <summary>
    /// This class compute the rights for features of MorphoNet Application depending on the user, the operating system ,and the current module (plot , distant plot , net)
    /// scene LoadParameters : call on LoadScene, Login and Logout
    ///scene Embryo : call on LoadScene
    ///Rights list : https://docs.google.com/spreadsheets/d/1bI_H_rJYMcE520m1J_hvJu4KjpsiDOJn3cEYCfnsh0E/edit#gid=1029803500
    /// </summary>
    public class RightManager : UnitySingleton<RightManager>
    {
        #region Right Properties

        public bool canDisplayPlotStandalone => !IsMobile && !IsWebGL;

        // TODO : integrate this test when the new TAO standalone interface will be merged
        public bool canDisplayDistantPlotStandalone => !IsMobile && !IsWebGL && IsConnected;

        public bool canResetTransformation => true;

        public bool canSetTransformationServer => IsOwnerOrMore && !IsMobile && IsNet;

        public bool canDownloadMeshes => IsNet;

        //Infos menu

        public bool canUploadInfo => IsNet && IsConnected;
        public bool canSaveInfo => IsConnected;

        public bool canAccessSubInfo => IsConnected;

        public bool canSuspendInfo => true;

        public bool canCurateInfo => IsOwnerOrMore;

        public bool canUpdateSelection => IsOwnerOrMore;
        public bool canShareInfo => IsOwnerOrMore && IsNet;

        public bool canDeleteInfo => IsOwnerOrMore;

        public bool getDownloadInfo => IsConnected && IsNet;

        //Object menu

        public bool canLoadColormap => IsReaderOrMore;

        public bool canSaveColormap => IsReaderOrMore;

        //Tools menu

        public bool canAccessRecorder => IsReaderOrMore && !IsMobile;

        // Lineage Menu

        public bool canAccessLineage => !IsMobile;

        //Curate menu
        public bool canUploadDBFromPlot => !IsMobile && IsPlot;

        public bool canAccessCuration => !IsMobile && IsPlot;

        // VR

        public bool canAccessVR => IsWindows;

        //AR

        public bool canAccessAR => IsMobile;

        #endregion Right Properties

        #region Application state properties

        private bool IsMobile => Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer;

        private bool IsWebGL => Application.platform == RuntimePlatform.WebGLPlayer;

        private bool IsWindows => Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor;

        private bool IsPlot => LoadParameters.instance.id_dataset == 0;

        private bool IsNet => LoadParameters.instance.id_dataset > 0;
        private bool IsOwnerOrMore => LoadParameters.instance.user_right > 1;

        private bool IsReaderOrMore => LoadParameters.instance.user_right > 0;

        private bool IsConnected => LoadParameters.instance.is_connected || LoadParameters.instance.id_user > 0;

        #endregion Application state properties
    }
}