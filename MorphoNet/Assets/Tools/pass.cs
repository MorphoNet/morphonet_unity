﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Net;
using System.Net.Sockets;
using UnityEngine.Networking;
using SimpleJSON;
using System.Security;
using System.IO;
using System.Security.Cryptography;
using System;
using System.Text;
using TMPro;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MorphoNet.User
{
    

    public class pass : MonoBehaviour {

        [SerializeField]
        private InputField _Password;
        [SerializeField]
        private InputField _Login;
        [SerializeField]
        private Button _ConnectBtn;
        [SerializeField]
        private Text _ErrorLabel;
        [SerializeField]
        private Button _LogOffButton;
        [SerializeField]
        private Text _UsernameLabel;
        [SerializeField]
        private Text _Version;

        private string _UserName;

        [SerializeField]
        private string _TokenFile = "aejefnljd.log";

        [SerializeField]
        private GameObject _LogPanel;
        [SerializeField]
        private Button _LogInBtn;


        private Coroutine DatasetLoadRoutine;
        
        private Vector2 final_pos = Vector2.zero;
        private Vector2 init_pos = new Vector2(0, -158);
 
        public IEnumerator CheckNewVersion()
        {
            string currentVersionNumber = MorphoTools.GetVersionNumber();
            string current_version_text = MorphoTools.GetFullVersionNumber();
            string current_with_update = current_version_text +
                                         "\nA new version is available <color=#0000EE><link=\"https://morphonet.org/downloads\">here</link>";
            StandaloneMainMenuInteractions.instance._MenuNewVersion.GetComponentInChildren<TMPro.TMP_Text>()
                    .text = current_version_text;
            //Current version : 2.1.10. A new version is available by clicking here
            StandaloneMainMenuInteractions.instance._TextVersionLoading.text = current_version_text;
            //Text t = AssetDatabase.LoadAssetAtPath("Assets/Textures/texture.jpg", typeof(Texture2D));
            
            /*axios.get('/api/standalonelistpublic/')
                .then(res => {
                    this.setState({builds:res.data['builds']});
                });*/
            UnityWebRequest www = UnityWebRequests.Get(LoadParameters.instance.urlSERVER, "/api/standalonelistpublic");
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                MorphoDebug.Log("Error :" + www.error);
                yield return null;
            }
            else
            {
                var N = JSONNode.Parse(www.downloadHandler.text);
                string max_version_found = currentVersionNumber;
                for (int i = 0; i < N["builds"].Count; i++)
                {
                    string version_found = N["builds"][i]["version"];
                    if (N["builds"][i]["build"].Value.Replace("\"","") == "stable"){
                        if (MorphoTools.NaturalCompare(version_found,max_version_found))
                        {
                            max_version_found = version_found;
                        }
                    }
                }
                
                if (max_version_found != currentVersionNumber && MorphoTools.NaturalCompare(max_version_found,currentVersionNumber))
                {
                    StandaloneMainMenuInteractions.instance._TextVersionLoading.text = current_with_update;

                    StandaloneMainMenuInteractions.instance._MenuNewVersion.GetComponentInChildren<TMPro.TMP_Text>().text = current_with_update;
                }
            }
            yield return null;
        }
        public void Start()
        {
            #if !UNITY_WEBGL
            StartCoroutine(CheckNewVersion());
            #endif
            if (GetToken() != "")
            {
                string[] data = GetToken().Split(':');
                LoadParameters.instance.user_token = data[0];
                LoadParameters.instance.id_user = int.Parse(data[1]);
                UserConnected();
                StartCoroutine(GetUserNameById());
                if (LoadParameters.instance.SetsDisplay == null)
                    LoadParameters.instance.ClearSetsFav();
                //DatasetLoadRoutine = StartCoroutine(LoadParameters.instance.GetAllForUser());

                //testing
                //DatasetLoadRoutine = StartCoroutine(LoadParameters.instance.GetDatasetsForUser(StandaloneMainMenuInteractions.instance.CurrentOrderingType, StandaloneMainMenuInteractions.DATASET_CHUNK_SIZE));
                DatasetLoadRoutine = StartCoroutine(LoadParameters.instance.GetDatasetsForUser());
                StartCoroutine(CheckIfAdmin());
            }
            else
            {
                StandaloneMainMenuInteractions.instance.SetFilterRights(false);
                //DatasetLoadRoutine = StartCoroutine(LoadParameters.instance.GetPublicWithSearch());
                //DatasetLoadRoutine = StartCoroutine(LoadParameters.instance.GetDatasetsForPublic(StandaloneMainMenuInteractions.instance.CurrentOrderingType, StandaloneMainMenuInteractions.DATASET_CHUNK_SIZE));
                DatasetLoadRoutine = StartCoroutine(LoadParameters.instance.GetDatasetsForPublic());
                LoadParameters.instance.SetAdmin(false);

            }
#if !UNITY_EDITOR
            if(LoadParameters.instance.InitDone)
                LoadParameters.instance.LoadLocalDatasets();
#endif

#if UNITY_EDITOR
            LoadParameters.instance.LoadLocalDatasets();
#endif
        }

        public void UserConnected()
        {
            _LogOffButton.gameObject.SetActive(true);
            _UsernameLabel.text = _UserName;
            _Password.gameObject.SetActive(false);
            _Login.gameObject.SetActive(false);
            _ConnectBtn.gameObject.SetActive(false);
            _Version.gameObject.SetActive(false);
            _LogInBtn.gameObject.SetActive(false);
            _LogPanel.gameObject.SetActive(false);

        }

        public void UserDisconnected()
        {
            _LogOffButton.gameObject.SetActive(false);
            _UsernameLabel.text = "Log in";
            _Password.gameObject.SetActive(true);
            _Login.gameObject.SetActive(true);
            _ConnectBtn.gameObject.SetActive(true);
            _Version.gameObject.SetActive(true);
            _LogInBtn.gameObject.SetActive(true);

        }

        public void LogoutUser()
        {
            StandaloneMainMenuInteractions.instance.SetAddDatasetPanelActive(false);
            StandaloneMainMenuInteractions.instance.DisplaySideSourceMenu();
            LoadParameters.instance.id_user = 0;
            LoadParameters.instance.user_token = "";
            LoadParameters.instance.is_connected = false;
            LoadParameters.instance.ClearSetsFav();
            try
            {
                File.Delete(Path.Combine(Application.streamingAssetsPath, _TokenFile));
            }
            catch (Exception e)
            {
                MorphoDebug.Log("Unable to delete old token file");
            }
            _UserName = "";
            UserDisconnected();
            if(DatasetLoadRoutine!=null)
            {
                StopCoroutine(DatasetLoadRoutine);
                StandaloneMainMenuInteractions.instance.LoadingImage.gameObject.SetActive(false);
            }
            LoadParameters.instance.SetAdmin(false);

            StartCoroutine(StandaloneMainMenuInteractions.instance.SendLogoutToPyinstaller());

            LoadParameters.instance.NCBIInAvailableSets.Clear();
            DatasetLoadRoutine = StartCoroutine(LoadParameters.instance.GetDatasetsForPublic());
            StandaloneMainMenuInteractions.instance.InitializeGroupFilterMenu();
            StandaloneMainMenuInteractions.instance.InitializeTagFilterMenu();
            //StandaloneMainMenuInteractions.instance.InitializeSpeciesFilterMenu();
            StandaloneMainMenuInteractions.instance.ClearAllFilters();
            StandaloneMainMenuInteractions.instance.SetFilterRights(false);
            
            LoadParameters.instance.LoadLocalDatasets();
        }

        public string UrlCombine(string uri1, string uri2)
        {
            if (uri1 == "")
            {
                return uri2;
            }

            if (uri2 == "")
            {
                return uri1;
            }

            uri1 = uri1.TrimEnd('/');
            uri2 = uri2.TrimStart('/');
            return string.Format("{0}/{1}", uri1, uri2);
        }
        public IEnumerator Log()
        {
            StandaloneMainMenuInteractions.instance.SetAddDatasetPanelActive(false);
            StandaloneMainMenuInteractions.instance.DisplaySideSourceMenu();

            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("username", _Login.text);
            data.Add("password", _Password.text);
            string bodyJson = JsonUtility.ToJson(data);
            WWWForm form = new WWWForm();
            form.AddField("username", _Login.text);
            form.AddField("password", _Password.text);
            UnityWebRequest www = UnityWebRequests.Post(LoadParameters.instance.urlSERVER, "/rest-auth/login/", form);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                _ErrorLabel.text = "Unable to login, please verify your credentials"; //if there is an error, tell
            }
            else
            {
                if (DatasetLoadRoutine != null)
                {
                    StopCoroutine(DatasetLoadRoutine);
                    StandaloneMainMenuInteractions.instance.LoadingImage.gameObject.SetActive(false);
                }

                StandaloneMainMenuInteractions.instance.ClearSpeciesFilterElements();
                StandaloneMainMenuInteractions.instance.ClearAllFilters();

                _ErrorLabel.text = ""; //if there is an error, tell
                var N = JSONNode.Parse(www.downloadHandler.text);
                int u_id = int.Parse(N["user"].ToString().Replace('\'', ' ').Replace('"', ' ').Trim());
                string token = N["key"].ToString().Replace('\'', ' ').Replace('"', ' ').Trim();
                LoadParameters.instance.id_user = u_id;
                LoadParameters.instance.is_connected = true;
                LoadParameters.instance.user_token = token;
                WriteToken(MorphoTools.Rot39(token + ":" + u_id));
                UserConnected();
                DatasetLoadRoutine = StartCoroutine(LoadParameters.instance.GetDatasetsForUser());

                StandaloneMainMenuInteractions.instance.InitializeGroupFilterMenu();
                StandaloneMainMenuInteractions.instance.InitializeTagFilterMenu();
                //StandaloneMainMenuInteractions.instance.InitializeSpeciesFilterMenu();
                StandaloneMainMenuInteractions.instance.SetFilterRights(true);
                LoadParameters.instance.LoadLocalDatasets();
                StartCoroutine(GetUserNameById());

                StartCoroutine(StandaloneMainMenuInteractions.instance.SendLoginToPyinstaller());
                StartCoroutine(CheckIfAdmin());
            }
            www.Dispose();
        }

        private IEnumerator CheckIfAdmin()
        {
            UnityWebRequest www = UnityWebRequests.Get(LoadParameters.instance.urlSERVER, "/api/people-admin");
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }
            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ProtocolError || www.result == UnityWebRequest.Result.ConnectionError)
            {
                Debug.LogError("error during request to check user privileges");
            }
            else
            {
                JSONNode N = JSONNode.Parse(www.downloadHandler.text);
                if (N["staff"] != null)
                    LoadParameters.instance.SetAdmin(N["staff"].AsBool);
            }
            www.Dispose();
        }

        static byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            // Create an Rijndael object
            // with the specified key and IV.
            using (Aes rijAlg = Aes.Create())
            {
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                // Create an encryptor to perform the stream transform.
                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream.
            return encrypted;
        }

        static string DecryptStringFromBytes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an Rijndael object
            // with the specified key and IV.
            using (Aes rijAlg = Aes.Create())
            {
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                // Create a decryptor to perform the stream transform.
                ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;
        }


        public string GetToken()
        {
            string result = "";
            string token_path = Path.Combine(Application.streamingAssetsPath, _TokenFile);

            if (File.Exists(token_path))
            {
                try
                {
                    result = File.ReadAllText(token_path);
                }
                catch (Exception e)
                {
                    MorphoDebug.Log("Unable to read the token");
                }
            }
            return MorphoTools.Rot39(result);
        }

        public void WriteToken(string data)
        {
            string token_path = Path.Combine(Application.streamingAssetsPath, _TokenFile);

            try
            {
                if (File.Exists(token_path))
                {
                    File.Delete(token_path);
                }
            }
            catch (Exception e)
            {
                MorphoDebug.Log("Unable to delete the previous token");
            }

            try
            {
                File.WriteAllText(token_path, data);
            }
            catch (Exception e)
            {
                MorphoDebug.Log("Unable to write token");
            }
        }
        public IEnumerator GetUserNameById()
        {
            UnityWebRequest www = UnityWebRequests.Get(LoadParameters.instance.urlSERVER, "/api/getusernamebyidunity?id_user=" + LoadParameters.instance.id_user);
            if (LoadParameters.instance.user_token != "" && LoadParameters.instance.user_token != null)
            {
                www.SetRequestHeader("Authorization", "TOKEN " + LoadParameters.instance.user_token);
            }
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                MorphoDebug.Log("Error :" + www.error);
            }
            else
            {
                var N = JSONNode.Parse(www.downloadHandler.text);
                string u_id =N["result"]["surname"].ToString().Replace('\'', ' ').Replace('"', ' ').Trim();
                string token = N["result"]["name"].ToString().Replace('\'', ' ').Replace('"', ' ').Trim();
                _UserName = u_id + " " + token;

                _UsernameLabel.text = _UserName;
                _UsernameLabel.gameObject.SetActive(true);
            }
        }

        public void TryLog()
        {
            StartCoroutine(Log());
        }

        public void SetLogPanelActive(bool value)
        {
            StandaloneMainMenuInteractions.instance.SetAddDatasetPanelActive(false);
            _LogPanel.SetActive(value);
        }


        

    }

}
