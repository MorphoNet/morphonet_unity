﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using SimpleJSON;
using UnityEngine.Networking;
using System.Globalization;

namespace MorphoNet
{
    public class EmbryosList : MonoBehaviour
    {
        public GameObject Parent;
        public GameObject NewDataset;
        public static Material Default;
        public Material DefaultMat;
        public GameObject canvas;
        public Camera cam;
        public LoadParameters loadP;

        public Dictionary<int, dataset> DataSets;

        private int IDUser = 1;
        private string nameUser = "Faure";
        private string surnameUser = "Emmanuel";
        private string urlSERVER = "http://morphonet.org/";

        //int width = 0;
        // Use this for initialization
        private void Start()
        {
            loadP = GameObject.Find("LoadParameters").GetComponent<LoadParameters>();
            scrollbar.SetActive(false);
            Default = DefaultMat;
            NewDataset.SetActive(false);
            StartCoroutine(listDataSet());
        }

        //Canvas Scale
        public static float canvasScale = 1F;

        public static float initcanvasScale = 0.01392758F;//0.01302083F; //Standard Scale for the Canvas
        public static float canvasHeight = 718;
        public static float canvasWidth = 1007;//Default Exprort Value

        //SCROLLBAR
        public GameObject scrollbar;

        public static int marging = 14;
        public static int MaxHeight = 700;
        public static int nbSCroll = 0;

        public void onScroll()
        {
            int nbSteps = scrollbar.GetComponent<Scrollbar>().numberOfSteps;
            float value = scrollbar.GetComponent<Scrollbar>().value;
            nbSCroll = (int)Mathf.Round(nbSteps - value * (nbSteps - 1) - 1);
            reorganiseDatasetType();
        }

        public float diffMvt = 0;
        public float prevX = 0;
        public bool moving = false;

        private int heightShiftMenu = 40;
        private int HeightMenu = 0;
        //private int widthShiftMenu = 25;

        public void reorganiseDatasetType()
        {
            HeightMenu = 0;

            HeightMenu += heightShiftMenu;
            foreach (KeyValuePair<int, dataset> itemD in DataSets)
            {
                dataset dt = itemD.Value;
                dt.dtgo.transform.position = new Vector2(70, 5 - HeightMenu + nbSCroll * heightShiftMenu);
                HeightMenu += heightShiftMenu;
            }

            //MorphoDebug.Log("HeightMenu=" + HeightMenu);
            if (HeightMenu > MaxHeight)
            {
                if (!scrollbar.activeSelf)
                {
                    scrollbar.SetActive(true);
                    scrollbar.GetComponent<Scrollbar>().numberOfSteps = 1 + (HeightMenu - MaxHeight) / heightShiftMenu; //DEFINE SCROLLBAR LENGTH
                    scrollbar.GetComponent<Scrollbar>().size = 1f / (float)(1 + (HeightMenu - MaxHeight) / heightShiftMenu);
                    scrollbar.GetComponent<Scrollbar>().value = scrollbar.GetComponent<Scrollbar>().numberOfSteps;
                }
            }
            else
                scrollbar.SetActive(false);
        }

        /// <summary>
        /// ////////////////////////////////////// DATASET
        /// </summary>
        ///

        //DATASET
        public class dataset
        {
            public int id_dataset;
            public string name;
            public string date;
            public int minTime;
            public int maxTime;
            public int id_owner;
            public int isBundle;
            public int id_type;
            public Quaternion rotation;
            public Vector3 translation;
            public Vector3 scale;
            public int spf;
            public int dt;
            public int quality;
            public GameObject dtgo;
            public GameObject dtgoSmall;

            public dataset()
            {
                dtgoSmall = null;
            }
        }

        //Check the uploaded data in the DB
        public IEnumerator listDataSet()
        {
            DataSets = new Dictionary<int, dataset>();
            UnityWebRequest www = UnityWebRequests.Get(urlSERVER, "api/embryolistsets/?id_people=" + IDUser.ToString());
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
                MorphoDebug.Log("Error : " + www.error);
            else
            {
                var N = JSONNode.Parse(www.downloadHandler.text);
                //MorphoDebug.Log("Found " + N.Count+" dataset");
                if (N != null && N.Count > 0)
                {
                    for (int i = 0; i < N.Count; i++)
                    {   //Number of Uploaded Data
                        //MorphoDebug.Log(N[i]);

                        dataset dst = new dataset();
                        dst.id_dataset = int.Parse(N[i]["id"].ToString().Replace('"', ' ').Trim());
                        dst.name = N[i]["name"].ToString().Replace('"', ' ').Trim();
                        dst.date = N[i]["date"].ToString().Replace('"', ' ').Trim();
                        dst.minTime = int.Parse(N[i]["mintime"].ToString().Replace('"', ' ').Trim());
                        dst.maxTime = int.Parse(N[i]["maxtime"].ToString().Replace('"', ' ').Trim());
                        dst.id_owner = int.Parse(N[i]["id_people"].ToString().Replace('"', ' ').Trim());
                        dst.isBundle = int.Parse(N[i]["bundle"].ToString().Replace('"', ' ').Trim());
                        dst.id_type = int.Parse(N[i]["id_dataset_type"].ToString().Replace('"', ' ').Trim());
                        string[] rot = N[i]["rotation"].ToString().Replace('"', ' ').Replace('(', ' ').Replace(')', ' ').Trim().Split(',');

                        CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                        ci.NumberFormat.CurrencyDecimalSeparator = ".";

                        dst.rotation = Quaternion.Euler(new Vector3(0.0f, 1.0f, 0.0f));//Quaternion.identity;
                        if (rot.Count() == 4)
                            dst.rotation = new Quaternion(float.Parse(rot[0], NumberStyles.Any, ci), float.Parse(rot[1], NumberStyles.Any, ci), float.Parse(rot[2], NumberStyles.Any, ci), float.Parse(rot[3], NumberStyles.Any, ci));
                        //MorphoDebug.Log ("rotation=" + dst.rotation.ToString ());

                        dst.translation = Vector3.zero;
                        string[] trans = N[i]["translation"].ToString().Replace('"', ' ').Replace('(', ' ').Replace(')', ' ').Trim().Split(',');
                        if (trans.Count() == 3)
                            dst.translation = new Vector3(float.Parse(trans[0], NumberStyles.Any, ci), float.Parse(trans[1], NumberStyles.Any, ci), float.Parse(trans[2], NumberStyles.Any, ci));
                        //MorphoDebug.Log ("translation=" + dst.translation.ToString ());

                        dst.scale = Vector3.zero;
                        string[] sca = N[i]["scale"].ToString().Replace('"', ' ').Replace('(', ' ').Replace(')', ' ').Trim().Split(',');
                        if (sca.Count() == 3)
                            dst.scale = new Vector3(float.Parse(sca[0], NumberStyles.Any, ci), float.Parse(sca[1], NumberStyles.Any, ci), float.Parse(sca[2], NumberStyles.Any, ci));
                        //MorphoDebug.Log ("scale=" + scale.ToString ());

                        dst.spf = int.Parse(N[i]["spf"].ToString().Replace('"', ' ').Trim());
                        dst.dt = int.Parse(N[i]["dt"].ToString().Replace('"', ' ').Trim());
                        dst.quality = 0;
                        int.TryParse(N[i]["quality"].ToString().Replace('"', ' ').Trim(), out dst.quality);
                        DataSets[dst.id_dataset] = dst;

                        //Create a Gameobcet for referenc
                        dst.dtgo = (GameObject)Instantiate(NewDataset);
                        dst.dtgo.transform.SetParent(canvas.transform, false);
                        dst.dtgo.gameObject.name = "DATASET_" + dst.id_dataset;

                        dst.dtgo.transform.Find("date").gameObject.GetComponent<Text>().text = "(" + dst.id_dataset + ") " + dst.date;
                        if (dst.minTime != dst.maxTime)
                            dst.dtgo.transform.Find("label").gameObject.GetComponent<Text>().text = dst.name + " (" + dst.minTime + "-" + dst.maxTime + ") ";
                        else
                            dst.dtgo.transform.Find("label").gameObject.GetComponent<Text>().text = dst.name;
                        Button loadButton = dst.dtgo.transform.Find("load").gameObject.GetComponent<Button>().GetComponent<Button>();
                        EmbryonSelectedListen(loadButton, dst.dtgo, dst.id_type, dst.id_dataset, dst.minTime, dst.maxTime, dst.name, dst.isBundle, dst.rotation, dst.translation, dst.scale, dst.id_owner, dst.dt, dst.spf, dst.quality);

                        dst.dtgo.SetActive(true);
                    }
                }
            }
            www.Dispose();
            reorganiseDatasetType();
        }

        //VERY WEIRED BUT NECESSARY OTHER WISE IT DOES NOT TAKE IN ACCOUNT THE VARIABLE CHANGEMENT
        private void EmbryonSelectedListen(Button b, GameObject emb, int dataset_type, int id_dataset, int minTime, int maxTime, string name, int isBundle, Quaternion rotation, Vector3 translation, Vector3 scale, int id_owner, int dt, int spf, int quality)
        {
            b.onClick.AddListener(() => EmbryonSelected(emb, dataset_type, id_dataset, minTime, maxTime, name, isBundle, rotation, translation, scale, id_owner, dt, spf, quality));
        }

        //nom:EmbronSelected
        //sémantique:fonction qui s'éxécute lorsque l'on clique sur un bouton généré par le DataFile
        //précond: url fonctionne
        public void EmbryonSelected(GameObject emb, int dataset_type, int id_dataset, int minTime, int maxTime, string name, int isBundle, Quaternion rotation, Vector3 translation, Vector3 scale, int id_owner, int dt, int spf, int quality)
        {
            string parameters = "{\"id\":\"" + id_dataset + "\"," +
                "\"name\":\"" + name + "\"," +
                "\"date\":\"2016-10-14 18:59:06\"," +
                "\"mintime\":\"" + minTime + "\"," +
                "\"maxtime\":\"" + maxTime + "\"," +
                "\"id_people\":\"" + id_owner + "\"," +
                "\"bundle\":\"" + isBundle + "\"," +
                "\"id_dataset_type\":\"0\"," +
                "\"rotation\":\"" + rotation + "\"," +
                "\"translation\":\"" + translation + "\"," +
                "\"scale\":\"" + scale + "\"," +
                "\"spf\":\"" + spf + "\"," +
                "\"dt\":\"+" + dt + "\"," +
                "\"quality\":\"" + quality + "\"," +
                "\"u_ID\":\"" + IDUser + "\"," +
                "\"u_name\":\"" + nameUser + "\"," +
                "\"u_surname\":\"" + surnameUser + "\"" +
                "}";

            loadP.parseParameters(parameters);
        }
    }
}