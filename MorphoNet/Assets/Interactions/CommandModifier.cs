﻿using System;

namespace MorphoNet.Interaction
{
    /// <summary>
    /// Command modifiers that may change the commands and or effects of inputs when active.
    /// </summary>
    [Flags]
    public enum CommandModifier
    {
        None = 1 << 0,

        #region Data set transform

        RotationActivated = 1 << 1,
        TranslationActivated = 1 << 2,
        ScatteringActivated = 1 << 3,
        ScalingActivated = 1 << 4,
        MeshCuttingActivated = 1 << 5,

        #endregion Data set transform

        #region Object selection

        XRCollisionSelection = 1 << 6,
        XRRaySelection = 1 << 7,
        SelectionMultiple = 1 << 8,
        SelectionSquare = 1 << 9,
        SelectionOver = 1 << 10,

        #endregion Object selection

        /// <summary>
        /// Left click,
        /// </summary>
        PrimaryButtonDown = 1 << 11,

        /// <summary>
        /// Right click,
        /// </summary>
        SecondaryButtonDown = 1 << 12,

        #region Figure transform

        FigureRotationActivated = 1 << 13,
        FigureTranslationActivated = 1 << 14,

        #endregion Figure transform

        TimeLineUpdate = 1 << 15,
        CuttingPlaneControl = 1 << 16,
        CuttingPlaneOnMainHand = 1 << 17,
        XRRotationActivated = 1 << 18,
    }
}