using MorphoNet.Extensions.Unity;
using MorphoNet.Interaction.Input;
using MorphoNet.Interaction.Physic;
using MorphoNet.UI;
using MorphoNet.XR;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR;

namespace MorphoNet.Interaction
{
    /// <summary>
    /// Handle User interactions on the application.
    /// It listen to user input then dispatch command to the appropriate
    /// part of the program (mostly to the <see cref="DataSetManager"/> or to the <see cref="UIMediator"/>).
    /// </summary>
    public class InteractionMediator
    {
        #region Input listener

        /// <summary>
        /// Translate Mouse user's interactions to event listen by this <see cref="InteractionMediator"/>.
        /// </summary>
        private readonly MouseInputListener _MouseListener;

        /// <summary>
        /// Translate Keyboard user's interactions to event listen by this <see cref="InteractionMediator"/>.
        /// </summary>
        private readonly KeyboardInputListener _KeyboardListener;

        /// <summary>
        /// Translate XR user's interactions to event listen by this <see cref="InteractionMediator"/>.
        /// </summary>
        private readonly XRInputListener _XRListener;

        #endregion Input listener

        #region External references

        /// <summary>
        /// Reference to the last selected <see cref="Cell"/>.
        /// This reference is updated when a new cell is picked (<see cref="SwitchCellSelectionState(Cell)"/>).
        /// </summary>
        public Cell LastSelectedCell { get; private set; } = null;

        /// <summary>
        /// Reference to the <see cref="SetsManager"/>, to send message to it.
        /// </summary>
        private readonly SetsManager _SetsManager;

        /// <summary>
        /// Reference to the <see cref="UIMediator"/>, to send message to the UI.
        /// </summary>
        private UIMediator _UIMediator;

        #endregion External references

        private CommandModifier _CommandModifier = CommandModifier.None;

        /// <summary>
        /// Temporary value used for mouse dragging.
        /// <para>
        /// It stores the position of the mouse when the mouse left
        /// button is pressed down (<see cref="OnLeftClickDown(Vector2)"/>).
        /// </para>
        /// <para>
        /// It's value is value is reset when the mouse left button
        /// is released (<see cref="OnLeftClickUp(Vector2)"/>).
        /// </para>
        /// </summary>
        private Vector2 _MousePositionOnDragStart = Vector2.zero;

        private Pose _SecondaryXRControllerPose;

        private Pose _MainXRControllerPose;

        public Pose GetMainXRControllerWorldPose()
        {
            Pose pose;

            pose.position = _MainController.transform.position;
            pose.rotation = _MainController.transform.rotation;

            return pose;
        }

        public Pose GetSecondaryXRControllerWorldPose()
        {
            Pose pose;

            pose.position = _SecondaryController.transform.position;
            pose.rotation = _SecondaryController.transform.rotation;

            return pose;
        }

        public Transform GetMainXRControllerCuttingPlaneTransform()
        {
            return _MainController.CuttingPlane.transform;
        }

        /// <summary>
        /// Vector from the left controller position to the right controller position.
        /// Its is used to calculate rotation of the <see cref="_MainController"/>
        /// controller around the <see cref="_SecondaryController"/> controllerPosition.
        /// This rotation is then apply to the active dataset.
        /// </summary>
        private Vector3 _InterXRControllersVector;

        private LayerMask _IgnoredLayer;

        private GameObject _HeadController;
        private XRController _MainController;
        private XRController _SecondaryController;

        private string _CurrentFeedbackDisplayed = "";

        #region Construtor and initialisation

        public InteractionMediator(
            MouseInputListener mouseListener,
            KeyboardInputListener keyboardListener,
            XRInputListener xrListener,
            SetsManager datasetManager,
            GameObject cuttingPlanePrefab)
        {
            _MouseListener = mouseListener;
            _KeyboardListener = keyboardListener;
            _XRListener = xrListener;

            _SetsManager = datasetManager;

            _IgnoredLayer = InterfaceManager.instance.IgnoredLayer;

            InitMouseListener();
            InitKeyboardListener();
            InitXRListener();

            InitCuttingPlane(cuttingPlanePrefab);
        }

        /// <summary>
        /// Set this <see cref="InteractionMediator"/>'s <see cref="UIMediator"/>.
        /// </summary>
        /// <param name="mediator">The <see cref="UIMediator"/> to use.</param>
        /// <seealso cref="UIMediator"/>
        public void SetUIMediator(UIMediator mediator)
            => _UIMediator = mediator;

        public void SetControllers(
            GameObject headController,
            XRController mainController,
            XRController secondaryController)
        {
            _HeadController = headController;
            _MainController = mainController;
            _SecondaryController = secondaryController;

            _MainController.Role = XRController.ControllerRole.Main;
            _SecondaryController.Role = XRController.ControllerRole.Secondary;

            UpdateControllersColliders();
        }

        /// <summary>
        /// Define on which controller collision this object has to listen to trigger controller interactions.
        /// </summary>
        public void UpdateControllersColliders()
        {
            _XRListener.SetRightControllerCollider(_MainController.PhysicCollisionListener);
            _XRListener.SetLeftControllerCollider(_SecondaryController.PhysicCollisionListener);
        }

        /// <summary>
        /// Initialize the callbacks for mouse input.
        /// </summary>
        private void InitMouseListener()
        {
            _MouseListener.OnLeftButtonDown += OnLeftClickDown;
            _MouseListener.OnLeftButtonUp += OnLeftClickUp;

            _MouseListener.OnLeftButtonDown += _ => AddModifier(CommandModifier.PrimaryButtonDown);
            _MouseListener.OnLeftButtonUp += _ => RemoveModifier(CommandModifier.PrimaryButtonDown);

            _MouseListener.OnDrag += OnMouseDrag;

            _MouseListener.OnPositionUpdate += OnMousePositionUpdate;
            _MouseListener.OnScroll += OnMouseWhellScroll;
        }

        /// <summary>
        /// Initialize the callbacks for keyboard input.
        /// </summary>
        private void InitKeyboardListener()
        {
            _KeyboardListener.OnCPressed += () => AddModifier(CommandModifier.SelectionSquare);
            _KeyboardListener.OnCReleased += () => RemoveModifier(CommandModifier.SelectionSquare);

            _KeyboardListener.OnXPressed += () => AddModifier(CommandModifier.SelectionOver);
            _KeyboardListener.OnXReleased += () => RemoveModifier(CommandModifier.SelectionOver);

            _KeyboardListener.OnKPressed += () => AddModifier(CommandModifier.FigureTranslationActivated);
            _KeyboardListener.OnKReleased += () => RemoveModifier(CommandModifier.FigureTranslationActivated);

            _KeyboardListener.OnLPressed += () => AddModifier(CommandModifier.FigureRotationActivated);
            _KeyboardListener.OnLReleased += () => RemoveModifier(CommandModifier.FigureRotationActivated);

            _KeyboardListener.OnLeftCtrlPressed += () => AddModifier(CommandModifier.RotationActivated);
            _KeyboardListener.OnLeftCtrlReleased += () => RemoveModifier(CommandModifier.RotationActivated);

            _KeyboardListener.OnLeftShiftPressed += () => AddModifier(CommandModifier.SelectionMultiple);
            _KeyboardListener.OnLeftShiftReleased += () => RemoveModifier(CommandModifier.SelectionMultiple);

            if (SystemInfo.operatingSystem.Contains("Windows"))
            {
                _SetsManager.TranslationButton = KeyCode.Space;
                _KeyboardListener.OnSpacePressed += () => AddModifier(CommandModifier.TranslationActivated);
                _KeyboardListener.OnSpaceReleased += () => RemoveModifier(CommandModifier.TranslationActivated);
            }
            else
            {
                _SetsManager.TranslationButton = KeyCode.LeftAlt;
                _KeyboardListener.OnLeftAltPressed += () => AddModifier(CommandModifier.TranslationActivated);
                _KeyboardListener.OnLeftAltReleased += () => RemoveModifier(CommandModifier.TranslationActivated);
            }
        }

        /// <summary>
        /// Register to <see cref="_XRListener"/> events.
        /// </summary>
        private void InitXRListener()
        {
            _XRListener.OnRightControllerCollisionStart += OnRightControllerTriggerCollision;

            _XRListener.OnLeftControllerPositionUpdate += UpdateLeftControllerPosition;
            _XRListener.OnRightControllerPositionUpdate += UpdateRightControllerPosition;
            _XRListener.OnRightTriggerFirstPress += OnRightTriggerNewPress;
            _XRListener.OnRightTriggerPressed += OnRightTriggerDown;
            _XRListener.OnRightTriggerReleased += OnRightTriggerUp;
        }

        #endregion Construtor and initialisation

        #region Mouse Handling

        /// <summary>
        /// Action to trigger when the left mouse button is pressed down.
        /// </summary>
        /// <param name="mousePosition">The position of the mouse on the screen when the button is pressed.</param>
        private void OnLeftClickDown(Vector2 mousePosition)
        {
            // If the mouse is over the UI, this will be handled by the UI.
            if (EventSystem.current.IsPointerOverGameObject() || _SetsManager.DataSet == null)
            {
                return;
            }
                

            DataSet dataset = _SetsManager.DataSet;

            if (Physics.Raycast(Camera.main.ScreenPointToRay(mousePosition), out RaycastHit hit, 1000f, ~_IgnoredLayer)
                && _UIMediator.CanInteractWithViewer())
            {
                string hitObjectName = hit.transform.name;

                if (hit.transform.gameObject == MorphoTools.GetBackground())
                {
                    dataset.TransformationsManager.ClickBackground(true);
                    //AddModifier(CommandModifier.RotationActivated);
                }
                else
                {
                    if (hitObjectName.Contains("_MeshPart"))
                        hitObjectName = hit.transform.parent.name;

                    if (dataset.TryParseCellName(hitObjectName, out string cellId, out int cellTimeStamp))
                        SwitchCellSelectionState(dataset.getCell(cellTimeStamp, cellId));
                }
            }

            _MousePositionOnDragStart
                = _CommandModifier.HasFlag(CommandModifier.SelectionMultiple)
                ? Vector2.zero
                : _MouseListener.CurrentMousePosition();

            if (dataset.Infos != null)
                dataset.Infos.describe(); //Update the description menu ! VERY COSTLY IN MEMORY
        }

        

        /// <summary>
        /// Action to trigger when the left mouse button is released (up).
        /// </summary>
        /// <param name="mousePosition">The position of the mouse on the screen when the button is released.</param>
        private void OnLeftClickUp(Vector2 mousePosition)
        {
            DataSet dataset = _SetsManager.DataSet;

            if (!_CommandModifier.HasFlag(CommandModifier.SelectionSquare) && _MousePositionOnDragStart != Vector2.zero)
                dataset.PickedManager.StopSquareSelection();

            if (dataset != null && dataset.TransformationsManager != null)
                dataset.TransformationsManager.ClickBackground(false);

            RemoveModifier(CommandModifier.RotationActivated);

            _MousePositionOnDragStart = Vector2.zero;
        }

        /// <summary>
        /// Action to trigger when the left mouse button is pressed while the mouse is mouving.
        /// </summary>
        /// <param name="mousePosition">The mouse position on the screen.</param>
        /// <param name="mouseDelta">The mouse delta since last frame (Difference since the last frame).</param>
        private void OnMouseDrag(Vector2 mousePosition, Vector2 mouseDelta)
        {
            DataSet dataset = _SetsManager.DataSet;

            if (_CommandModifier.HasFlag(CommandModifier.SelectionSquare)
                && !_CommandModifier.HasFlag(CommandModifier.SelectionMultiple))
            {
                if (_MousePositionOnDragStart != Vector2.zero)
                {
                    dataset
                        .PickedManager
                        .StartOrContinueSquareSelection(_MousePositionOnDragStart, mousePosition);
                }
            }
        }

        /// <summary>
        /// Call everytime the mouse position is updated (should be on fixed update)
        /// </summary>
        /// <param name="mousePosition">The mouse position on the screen.</param>
        private void OnMousePositionUpdate(Vector2 mousePosition)
        {
            if (EventSystem.current.IsPointerOverGameObject())
                return;

            DataSet dataSet = _SetsManager.DataSet;

            if (dataSet is null)
                return;

            HandleMouseOverCell(mousePosition, dataSet.PickedManager);

            HandleTransformations(mousePosition, dataSet.TransformationsManager);
        }

        /// <summary>
        /// Update the text to display next to the mouse if it is on a Cell.
        /// If <see cref="CommandModifier.SelectionOver"/> is active, add the cell under pointer to the selection.
        /// </summary>
        /// <param name="mousePosition">The current position of the mouse on the screen.</param>
        /// <param name="pickedManager">The instance handling the cell selection.</param>
        private void HandleMouseOverCell(Vector2 mousePosition, PickedManager pickedManager)
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(mousePosition), out RaycastHit hit, 1000f, ~_IgnoredLayer)
                            && _UIMediator.CanInteractWithViewer())
            {
                if (pickedManager.IsGameObjectACell(hit.transform.gameObject, out Cell cell))
                {
                    pickedManager.UpdateCellShowString(cell, mousePosition);

                    if (_CommandModifier.HasFlag(CommandModifier.SelectionOver))
                        pickedManager.AddCellToSelection(cell);
                }
                else
                {
                    pickedManager.UpdateCellShowString(null, mousePosition);
                }
            }
        }

        /// <summary>
        /// Dispatch differents transformations operations to specific sub-routines.
        /// </summary>
        /// <param name="mousePosition">The current position of the mouse on the screen.</param>
        /// <param name="transformationManager">The instance handling the transformation operations.</param>
        private void HandleTransformations(Vector2 mousePosition, TransformationsManager transformationManager)
        {
            HandleDataSetTransformation(mousePosition, transformationManager);

            HandleFigureTranslation(mousePosition, transformationManager);
        }

        /// <summary>
        /// Activate or deactivate the rotation of the dataset if the corrects modifiers are activated.
        /// </summary>
        /// <param name="mousePosition">The current position of the mouse on the screen.</param>
        /// <param name="transformationManager">The instance handling the transformation operations.</param>
        private void HandleDataSetTransformation(Vector2 mousePosition, TransformationsManager transformationManager)
        {
            if (!(MorphoTools.GetPlotIsActive() && MorphoTools.GetPlot().IsRotating())){
                if (_CommandModifier.HasFlag(CommandModifier.RotationActivated))
                    transformationManager.DataSetRotation(mousePosition);
                else
                    transformationManager.CancelDataSetRotation();
            }

            if (_CommandModifier.HasFlag(CommandModifier.TranslationActivated))
                transformationManager.DataSetTranslation(mousePosition);
            else
                transformationManager.CancelDataSetTranslation();
        }

        /// <summary>
        /// Activate or deactivate the rotation of the figures if the corrects modifiers are activated.
        /// </summary>
        /// <param name="mousePosition">The current position of the mouse on the screen.</param>
        /// <param name="transformationManager">The instance handling the transformation operations.</param>
        private void HandleFigureTranslation(Vector2 mousePosition, TransformationsManager transformationManager)
        {
            if (!transformationManager.CanTransformFigureManager())
                return;

            if (_CommandModifier.HasFlag(CommandModifier.FigureTranslationActivated))
                transformationManager.FigureTranslation(mousePosition);
            else
                transformationManager.CancelFigureTranslation();

            if (_CommandModifier.HasFlag(CommandModifier.FigureRotationActivated))
                transformationManager.FigureRotation(mousePosition);
            else
                transformationManager.CancelFigureRotation();
        }

        private void OnMouseWhellScroll(float scrollDelta)
        {
            if (_UIMediator.CanInteractWithViewer() && _SetsManager.DataSet != null)
            {
                if (!(MorphoTools.GetPlotIsActive() && MorphoTools.GetPlot().IsScaling())){
                    DataSet dataSet = _SetsManager.DataSet;
                    var delta = scrollDelta > 0 ? 1 : -1;
                    dataSet.TransformationsManager.UpdateDatasetScaling(delta);
                }
                if (MorphoTools.GetPlotIsActive())
                {
                    MorphoTools.GetPlot().UpdateOnScroll(scrollDelta);
                }
            }
        }

        #endregion Mouse Handling

        #region Command modifiers

        /// <summary>
        /// Base modifiers are always active, no matter what other modifiers are.
        /// One exeptions is when <see cref="DeactivateAllModifiers"/> is called, in this case, nothing is active.
        /// </summary>
        public const CommandModifier BaseModifiers
            = CommandModifier.XRRotationActivated
            | CommandModifier.ScalingActivated
            | CommandModifier.TimeLineUpdate;

        public CommandModifier DefaultModifiers
            => CommandModifier.SelectionMultiple
            | BaseModifiers
            | SelectionMethod;

        public CommandModifier SelectionMethod
            = CommandModifier.XRCollisionSelection;

        /// <summary>
        /// Add a <see cref="CommandModifier"/> flag.
        /// Nothing happends if this flag is already set.
        /// </summary>
        /// <param name="modifier">The flag to add.</param>
        public void AddModifier(CommandModifier modifier)
        {
            _CommandModifier |= modifier | BaseModifiers;

            // Assure that None is not active
            RemoveModifier(CommandModifier.None);
        }

        /// <summary>
        /// Remove a <see cref="CommandModifier"/> flag.
        /// Nothing happends if this flag is not set.
        /// </summary>
        /// <param name="modifier">The flag to remove.</param>
        public void RemoveModifier(CommandModifier modifier) => _CommandModifier &= ~modifier;

        public void DeactivateAllModifiers() => _CommandModifier = CommandModifier.None;

        public void ResetXRModifiersToDefault()
            => _CommandModifier = DefaultModifiers;

        #endregion Command modifiers

        #region XR Interactions

        private const float _MinimumVelocity = 0.10f;
        private const int _VelocitySteps = 10;
        private const float _VelocityStepSize = 0.1f;
        private const float _MaximumVelocity = _MinimumVelocity + _VelocitySteps * _VelocityStepSize;
        private const float _Speed = 10;

        private const float _HandRotationFactor = 1.5f;

        private Vector3 _InterControllersVectorAtFirstRightPress = Vector3.zero;
        private Vector3 _ScaleAtFirstRightPress = Vector3.one;

        private bool LeftTriggerPressedAlone
            => _XRListener.PressedButtons.HasFlag(XRInputListener.XRControllerButton.LeftTrigger)
            && !_XRListener.PressedButtons.HasFlag(XRInputListener.XRControllerButton.RightTrigger);

        private bool RightTriggerPressedAlone
            => _XRListener.PressedButtons.HasFlag(XRInputListener.XRControllerButton.RightTrigger)
            && !_XRListener.PressedButtons.HasFlag(XRInputListener.XRControllerButton.LeftTrigger);

        private bool RightAndLeftTriggersPressed
            => _XRListener.PressedButtons.HasFlag(XRInputListener.XRControllerButton.RightTrigger)
            && _XRListener.PressedButtons.HasFlag(XRInputListener.XRControllerButton.LeftTrigger);

        /// <summary>
        /// Action to trigger when the left controller position changes.
        /// </summary>
        /// <param name="position">The new left controller position.</param>
        private void UpdateLeftControllerPosition(Vector3 position, Vector3 velocity)
        {
            _SecondaryXRControllerPose = new Pose(_SecondaryController.transform.position, _SecondaryController.transform.rotation);

            _InterXRControllersVector = _MainXRControllerPose.position - _SecondaryXRControllerPose.position;

            bool shouldUpdateTimeline = _CommandModifier.HasFlag(CommandModifier.TimeLineUpdate);

            if (LeftTriggerPressedAlone && shouldUpdateTimeline)
            {
                UpdateDataSetTimestamp(velocity);
                _CurrentFeedbackDisplayed = "Timeline Navigation";
            }
            else
            {
                _CurrentFeedbackDisplayed = "";
            }
        }

        /// <summary>
        /// Action to trigger when the right controller position changes.
        /// </summary>
        /// <param name="position">The new right controller position.</param>
        private void UpdateRightControllerPosition(Vector3 position, Vector3 velocity)
        {
            Pose previousRightXRControllerPose = _MainXRControllerPose;
            _MainXRControllerPose = new Pose(_MainController.transform.position, _MainController.transform.rotation);

            Vector3 previousInterControllesDirection = _InterXRControllersVector;
            _InterXRControllersVector = _MainXRControllerPose.position - _SecondaryXRControllerPose.position;

            bool shouldScatter = _CommandModifier.HasFlag(CommandModifier.ScatteringActivated);
            bool shouldRotate = _CommandModifier.HasFlag(CommandModifier.XRRotationActivated);
            bool shouldScale = _CommandModifier.HasFlag(CommandModifier.ScalingActivated);
            bool shouldMoveCuttingPlane = _CommandModifier.HasFlag(CommandModifier.CuttingPlaneControl);

            bool meshCuttingActivated = _CommandModifier.HasFlag(CommandModifier.MeshCuttingActivated);

            bool shouldCastXRRay = _CommandModifier.HasFlag(CommandModifier.XRRaySelection); // Add future command flag using raycast later

            if (shouldCastXRRay)
            {
                XRRayCast(_MainController);
                ShowRay();
            }
            else
            {
                HideRay();
            }

            if (RightAndLeftTriggersPressed)
            {
                if (shouldRotate)
                {
                    UpdateDataSetRotation(previousRightXRControllerPose, previousInterControllesDirection, _InterXRControllersVector);
                }

                if (shouldScale)
                {
                    UpdateDatasetScale();
                }

                if (meshCuttingActivated)
                {
                    if (_CuttingPlane.activeSelf)
                    {
                        ActivateOrUpdateClippingPlane();
                    }
                    else
                    {
                        //_SetsManager.DataSet.RawImagesGetActivePlane();
                    }
                    _SetsManager.DataSet.RawImages.updateMeshShader();
                }

                _CurrentFeedbackDisplayed = "Rotation and Scaling";
            }
            else if (RightTriggerPressedAlone)
            {
                if (shouldScatter)
                {
                    Scatter(velocity);
                    _CurrentFeedbackDisplayed = "Scattering";
                }
                else if (shouldMoveCuttingPlane)
                {
                    UpdateClippingPlanePose(previousRightXRControllerPose);
                    _CurrentFeedbackDisplayed = "Cutting plane manipulation";
                }
                else if (_UIMediator.GetCurrentSelectionMethod() == CommandModifier.XRCollisionSelection)
                {
                    _CurrentFeedbackDisplayed = "Cancelling selection";
                }
            }
            else
            {
                _CurrentFeedbackDisplayed = "";
            }

            _UIMediator.ChangeFeedbackText(_CurrentFeedbackDisplayed);
        }

        private RaycastHit _LaserHit;
        private Vector3? _LastLaserHitPoint = null;

        private void XRRayCast(XRController controller)
        {
            const float maxDistance = 50f;

            Vector3 rayStartPosition = controller.GetTriggerPosition();
            Vector3 rayDirection = controller.GetControllerForwardDirection();

            bool hit = Physics.Raycast(
                rayStartPosition,
                rayDirection,
                out _LaserHit,
                maxDistance,
                ~_IgnoredLayer);

            hit &= _LaserHit.collider != null && _LaserHit.collider.name != "MazeBg";

            if (hit && _LaserHit.collider.gameObject.layer == LayerMask.NameToLayer(Tools.Layers.VROnly))
                _LastLaserHitPoint = rayStartPosition + rayDirection;
            else if (hit)
                _LastLaserHitPoint = _LaserHit.point;
            else
                _LastLaserHitPoint = rayStartPosition + (Vector3.Normalize(rayDirection) * maxDistance);
        }

        private void ShowRay()
        {
            _MainController.RaycastLaser.Draw(_LastLaserHitPoint);
        }

        private void HideRay()
        {
            _MainController.RaycastLaser.Undraw();
        }

        private void SelectFromRayCast()
        {
            HandleCellSelection(new CollisionResult(_LaserHit.collider));
        }

        private float GetXAxisVelocity(Vector3 referenceControllerPosition, Vector3 handControllerVelocity)
        {
            Vector3 modelPosition = _SetsManager.DataSet.TransformationsManager.CurrentPosition;
            Vector3 direction = modelPosition - referenceControllerPosition;
            Vector3 midPoint = referenceControllerPosition + (direction / 2f);
            Vector3 normal = Vector3.Cross(Vector3.up, midPoint);

            handControllerVelocity = Vector3.Project(handControllerVelocity, normal);

            float xAxisVelocity = Mathf.Min(handControllerVelocity.x, _MaximumVelocity);
            xAxisVelocity = Mathf.Max(xAxisVelocity, -_MaximumVelocity);

            return xAxisVelocity;
        }

        private void Scatter(Vector3 velocity)
        {
            float time = Time.deltaTime;
            var xVelocity = GetXAxisVelocity(_HeadController.transform.position, velocity);

            if (Mathf.Abs(xVelocity) > _MinimumVelocity)
            {
                float value = time * xVelocity * _Speed;
                MorphoTools.GetScatter().UpdateExplodeFactor(value);
            }
        }

        private void UpdateDataSetTimestamp(Vector3 velocity)
        {
            float time = Time.deltaTime;
            var xAxisVelocity = GetXAxisVelocity(_HeadController.transform.position, velocity);

            if (Mathf.Abs(xAxisVelocity) > _MinimumVelocity)
            {
                int steps = Mathf.Abs(Mathf.RoundToInt(time * xAxisVelocity * _VelocitySteps * _Speed));

                float velocitySign = Mathf.Sign(xAxisVelocity);

                if (velocitySign > 0)
                    for (int i = 0; i < steps; i++)
                        _UIMediator.GoToNextTimestamp();
                else
                    for (int i = 0; i < steps; i++)
                        _UIMediator.GoToPreviousTimestamp();
            }
        }

        private void UpdateDatasetScale()
        {
            var x = _InterControllersVectorAtFirstRightPress.magnitude;

            if (x == 0)
                x = 1;

            float factor = _InterXRControllersVector.magnitude / x;
            _SetsManager.DataSet.TransformationsManager.rescale(_ScaleAtFirstRightPress * factor);
        }

        /// <summary>
        /// Upadate the current Data Set rotation relatively to the angle difference between
        /// the previous and current <see cref="_InterXRControllersVector"/> angle difference.
        /// </summary>
        /// <param name="previousInterControllersVector">The <see cref="_InterXRControllersVector"/> before the position update.</param>
        /// <param name="interControllersVector">The <see cref="_InterXRControllersVector"/> after the position update.</param>
        private void UpdateDataSetRotation(
            Pose previousReferencePose,
            Vector3 previousInterControllersVector,
            Vector3 interControllersVector)
        {
            DataSet dataset = _SetsManager.DataSet;

            if (OptionController.Instance.GetModelRotationWithOneHandOption().Value)
            {
                var updatedRotation =
                    _MainXRControllerPose.rotation
                    * Quaternion.Inverse(previousReferencePose.rotation);

                dataset.TransformationsManager.RotateSlerpUnclamped(updatedRotation, _HandRotationFactor);
            }
            else
            {
                dataset
                    .TransformationsManager
                    .RotateInverse(
                        Vector3.Cross(previousInterControllersVector, interControllersVector),
                        Vector3.Angle(interControllersVector, previousInterControllersVector) * 2.5f);
            }
        }

        /// <summary>
        /// Method to call when the Right Trigger is down.
        /// </summary>
        private void OnRightTriggerDown()
        {
            AddModifier(CommandModifier.PrimaryButtonDown);
        }

        private void OnRightTriggerNewPress()
        {
            AddModifier(CommandModifier.PrimaryButtonDown);
            _InterControllersVectorAtFirstRightPress = _InterXRControllersVector;
            _ScaleAtFirstRightPress = _SetsManager.DataSet.TransformationsManager.CurrentScale;

            if (_CommandModifier.HasFlag(CommandModifier.XRRaySelection))
                SelectFromRayCast();
        }

        /// <summary>
        /// Method to call when the Right Trigger is up.
        /// </summary>
        private void OnRightTriggerUp()
        {
            RemoveModifier(CommandModifier.PrimaryButtonDown);
            _InterControllersVectorAtFirstRightPress = Vector3.zero;
        }

        /// <summary>
        /// Callback executed whenever the right controller collide with something.
        /// </summary>
        /// <params name="result">The result of the collision between this controller and another colliding object.</params>
        private void OnRightControllerTriggerCollision(CollisionResult result)
        {
            HandleCellSelection(result);
        }

        /// <summary>
        /// Check if the given <paramref name="other"/> is a cell. If so, handle cell selection.
        /// </summary>
        /// <params name="other">The <see cref="Collider"/> to check.</params>
        private void HandleCellSelection(CollisionResult result)
        {
            DataSet dataset = _SetsManager.DataSet;
            if (result != null)
            {
                if (result.Other != null)
                {
                    string hitObjectName = result.Other.name.Contains("_MeshPart")
                ? result.Other.transform.parent.name
                : result.Other.name;

                    bool collisionSelection = _CommandModifier.HasFlag(CommandModifier.XRCollisionSelection);

                    bool raySelection = _CommandModifier.HasFlag(CommandModifier.XRRaySelection);

                    if ((collisionSelection || raySelection)
                        && result.IsExternalCollision
                        && dataset.TryParseCellName(hitObjectName, out string cellId, out int cellTimeStamp))
                    {
                        var cell = dataset.getCell(cellTimeStamp, cellId);

                        if (raySelection)
                            SwitchCellSelectionState(cell);
                        else if (_XRListener.PressedButtons.HasFlag(XRInputListener.XRControllerButton.RightTrigger))
                            RemoveCellFromSelection(cell);
                        else
                            AddCellToSelection(cell);
                    }
                }
            }
            
        }

        public void ResetSelectionModifiers()
        {
            RemoveModifier(CommandModifier.SelectionOver);
            RemoveModifier(CommandModifier.SelectionMultiple);
            RemoveModifier(CommandModifier.SelectionSquare);
        }

        /// <summary>
        /// Ask the <see cref="PickedManager"/> to select the given cell (or unselect if it was already).
        /// </summary>
        /// <params name="cell">The cell to select/unselect.</params>
        private void SwitchCellSelectionState(Cell cell)
        {
            if (cell is null)
                return;
            //if mesh morphing is active, do not toggle selection
            if (MorphoTools.GetPlotIsActive() && InterfaceManager.instance.DirectPlot.RuntimeMeshEditorInstance.isActiveAndEnabled)
                return;

            PickedManager picker = GetPickerForSelection(cell);

            picker.SwitchCellSelectionState(cell);

            SetLastSelectedCell(picker);
        }

        private void AddCellToSelection(Cell cell)
        {
            if (cell is null)
                return;

            PickedManager picker = GetPickerForSelection();
            picker.AddCellToSelection(cell);

            SetLastSelectedCell(picker);
        }

        private void RemoveCellFromSelection(Cell cell)
        {
            if (cell is null)
                return;

            PickedManager picker = GetPickerForSelection();

            picker.RemoveCellSelected(cell);

/*#if !UNITY_WEBGL
            if (InterfaceManager.instance.lineage_viewer != null && InterfaceManager.instance.lineage_viewer.lineage_shown)
            {
                List<Cell> cells = new List<Cell>();
                cells.Add(cell);
                InterfaceManager.instance.lineage_viewer.SendClearPicked(cells);
            }
#endif*/

            SetLastSelectedCell(picker);
        }

        private PickedManager GetPickerForSelection(Cell exception = null)
        {
            DataSet dataset = _SetsManager.DataSet;
            PickedManager picker = dataset.PickedManager;

            if (!_CommandModifier.HasFlag(CommandModifier.SelectionMultiple))
                picker.ClearSelectedCells(exception);
            return picker;
        }

        private void SetLastSelectedCell(PickedManager picker)
        {
            if (picker.clickedCells.Count == 0)
            {
                LastSelectedCell = null;
            }
            else
            {
                LastSelectedCell = picker.clickedCells[picker.clickedCells.Count - 1];
            }
        }

        #endregion XR Interactions

        #region Cutting Plane Control

        private GameObject _CuttingPlane;

        private void InitCuttingPlane(GameObject cuttingPlanePrefab)
        {
            _CuttingPlane = GameObject.Instantiate(cuttingPlanePrefab);
            _CuttingPlane.transform.position = _SetsManager.embryosBase.transform.position;
            _CuttingPlane.transform.rotation = _SetsManager.embryosBase.transform.rotation;
            DeactivateClippingPlane();
        }

        internal void ActivateOrUpdateClippingPlane()
        {
            DataSet dataset = _SetsManager.DataSet;
            _CuttingPlane.Activate();

            if (dataset != null)
            {
                var up = _CuttingPlane.transform.up;//dataset.GetTimepointAt(dataset.CurrentTime).TimePointRoot.InverseTransformDirection(_CuttingPlane.transform.up);
                var pos = _CuttingPlane.transform.position;//dataset.GetTimepointAt(dataset.CurrentTime).TimePointRoot.InverseTransformPoint(_CuttingPlane.transform.position);
                dataset.RawImages.UpdateMeshFreeCuttingPlane(new Plane(up, pos), _CommandModifier.HasFlag(CommandModifier.MeshCuttingActivated), _CuttingPlane.transform);
            }
        }

        internal void DeactivateClippingPlane()
        {
            DataSet dataset = _SetsManager.DataSet;
            _CuttingPlane.Deactivate();
            if (dataset != null)
                dataset.RawImages.DeactivateFreeCuttingPlane();
        }

        internal void ResetClippingPlanePose(bool updateClip = false)
        {
            DataSet dataset = _SetsManager.DataSet;
            if (dataset != null)
            {
                _CuttingPlane.transform.position = dataset.transform.position;
                _CuttingPlane.transform.rotation = dataset.transform.rotation;
                dataset.RawImages.UpdateMeshFreeCuttingPlane(new Plane(_CuttingPlane.transform.up, _CuttingPlane.transform.position), updateClip, _CuttingPlane.transform);
            }
        }

        private void UpdateClippingPlanePose(Pose previousReferencePose)
        {
            bool cuttingPlaneOnHand = _CommandModifier.HasFlag(CommandModifier.CuttingPlaneOnMainHand);

            if (cuttingPlaneOnHand)
            {
                _CuttingPlane.transform.position = _MainController.CuttingPlane.transform.position;
                _CuttingPlane.transform.rotation = _MainController.CuttingPlane.transform.rotation;
            }
            else
            {
                var planeDirection = _MainXRControllerPose.position - previousReferencePose.position;
                var updatedRotation = _MainXRControllerPose.rotation * Quaternion.Inverse(previousReferencePose.rotation);

                _CuttingPlane.transform.position += planeDirection;

                _CuttingPlane.transform.rotation =
                    Quaternion.SlerpUnclamped(
                        _CuttingPlane.transform.rotation,
                        updatedRotation * _CuttingPlane.transform.rotation,
                        _HandRotationFactor);
            }

            ActivateOrUpdateClippingPlane();
        }

        #endregion Cutting Plane Control
    }
}