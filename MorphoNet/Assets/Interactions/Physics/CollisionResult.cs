using UnityEngine;

namespace MorphoNet.Interaction.Physic
{
    public class CollisionResult
    {
        public Collider Other { get; private set; }
        public bool IsExternalCollision { get; private set; }

        public CollisionResult(Collider other, bool isExternal = true)
            => (Other, IsExternalCollision) = (other, isExternal);
    }
}
