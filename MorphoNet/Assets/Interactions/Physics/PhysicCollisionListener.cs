using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MorphoNet.Interaction.Physic
{
    [RequireComponent(typeof(Collider))]
    public class PhysicCollisionListener : MonoBehaviour
    {
        public Collider GetCollider => GetComponent<Collider>();

        public event Action<CollisionResult> OnCollisionStarted;

        public event Action<CollisionResult> OnCollisionStopped;

        private readonly HashSet<Collider> _SurroundingColliders = new HashSet<Collider>();

        private static LayerMask _DefaultLayer;
        private static LayerMask _LastHitLayer;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        private void Awake()
        {
            _DefaultLayer = LayerMask.NameToLayer(Tools.Layers.Default);
            _LastHitLayer = LayerMask.NameToLayer(Tools.Layers.LastHit);
        }

        /// <summary>
        /// OnTriggerEnter is called when the Collider other enters the trigger.
        /// </summary>
        /// <param name="other">The other Collider involved in this collision.</param>
        private void OnTriggerEnter(Collider other)
        {
            bool externalCollision = !_SurroundingColliders.Contains(other);

            if (other.gameObject.layer == _DefaultLayer)
                other.gameObject.layer = _LastHitLayer;

            OnCollisionStarted?.Invoke(new CollisionResult(other, externalCollision));
        }

        /// <summary>
        /// OnTriggerExit is called when the Collider other has stopped touching the trigger.
        /// </summary>
        /// <param name="other">The other Collider involved in this collision.</param>
        private void OnTriggerExit(Collider other)
        {
            RaycastHit[] hits;
            Vector3 rayOrigin = transform.position;
            float rayLength = 10f;
            int layerMask = 1 << _LastHitLayer;

            int otherRaycastHitCount = 0;

            Func<RaycastHit[], bool> anyHitOnOther = hits => hits.Any(hit => hit.collider == other);

            hits = Physics.RaycastAll(rayOrigin, Vector3.forward, rayLength, layerMask);
            otherRaycastHitCount += anyHitOnOther(hits) ? 1 : 0;

            hits = Physics.RaycastAll(rayOrigin, Vector3.back, rayLength, layerMask);
            otherRaycastHitCount += anyHitOnOther(hits) ? 1 : 0;

            hits = Physics.RaycastAll(rayOrigin, Vector3.left, rayLength, layerMask);
            otherRaycastHitCount += anyHitOnOther(hits) ? 1 : 0;

            hits = Physics.RaycastAll(rayOrigin, Vector3.right, rayLength, layerMask);
            otherRaycastHitCount += anyHitOnOther(hits) ? 1 : 0;

            hits = Physics.RaycastAll(rayOrigin, Vector3.up, rayLength, layerMask);
            otherRaycastHitCount += anyHitOnOther(hits) ? 1 : 0;

            hits = Physics.RaycastAll(rayOrigin, Vector3.down, rayLength, layerMask);
            otherRaycastHitCount += anyHitOnOther(hits) ? 1 : 0;

            bool insideOther = otherRaycastHitCount >= 4;

            if (insideOther)
            {
                _SurroundingColliders.Add(other);
            }
            else
            {
                if (other.gameObject.layer == _LastHitLayer)
                    other.gameObject.layer = _DefaultLayer;
                _SurroundingColliders.Remove(other);
            }

            OnCollisionStopped?.Invoke(new CollisionResult(other, !insideOther));
        }
    }
}