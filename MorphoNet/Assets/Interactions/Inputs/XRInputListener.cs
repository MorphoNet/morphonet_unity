using System;
using MorphoNet.Interaction.Physic;
using UnityEngine;
using UnityEngine.InputSystem;

using XRPose = UnityEngine.XR.OpenXR.Input.Pose;

namespace MorphoNet.Interaction.Input
{

    /// <summary>
    /// Handle <see cref="InputAction"/>s triggered by the XR devices and controllers collisions triggers outside the UI.
    /// Trigger c# events based on thoose.
    /// </summary>
    public class XRInputListener : MonoBehaviour
    {
        #region XR buttons

        [Flags]
        public enum XRControllerButton
        {
            None = 0,
            LeftTrigger = 1,
            RightTrigger = 2,
            LeftPad = 4,
            RightPad = 8,
            LeftGrip = 16,
            RightGrip = 32,
        }

        private void Press(XRControllerButton button)
        {
            PressedButtons |= button;

            switch (button)
            {
                case XRControllerButton.LeftTrigger:
                    if (!_LeftControllerTriggerPressedLastFrame)
                    {
                        _LeftControllerTriggerPressedLastFrame = true;
                        OnLeftTriggerFirstPress?.Invoke();
                    }
                    OnLeftTriggerPressed?.Invoke();
                    break;

                case XRControllerButton.RightTrigger:
                    if (!_RightControllerTriggerPressedLastFrame)
                    {
                        _RightControllerTriggerPressedLastFrame = true;
                        OnRightTriggerFirstPress?.Invoke();
                    }
                    OnRightTriggerPressed?.Invoke();
                    break;
                default:
                    break;
            }
        }

        private void UnPress(XRControllerButton button)
        {
            PressedButtons &= ~button;

            switch (button)
            {
                case XRControllerButton.LeftTrigger:
                    _LeftControllerTriggerPressedLastFrame = false;
                    OnLeftTriggerReleased?.Invoke();
                    break;
                case XRControllerButton.RightTrigger:
                    _RightControllerTriggerPressedLastFrame = false;
                    OnRightTriggerReleased?.Invoke();
                    break;
                default:
                    break;
            }
        }

        #endregion XR buttons

        #region Fields

        public XRControllerButton PressedButtons;
        public Vector3 LeftControllerPosition;
        public Vector3 RightControllerPosition;
        public Vector3 LeftControllerVelocity;
        public Vector3 RightControllerVelocity;

        /// <summary>
        /// Threshold above which a XR trigger is considerated fully pressed.
        /// </summary>
        private const float _TriggerFullPressThreshold = 0.95f;


        /// <summary>
        /// Collision listener attached to the part of the left controller that interact with other physic elements.
        /// </summary>
        private PhysicCollisionListener _LeftControllerCollider;

        /// <summary>
        /// Collision listener attached to the part of the right controller that interact with other physic elements.
        /// </summary>
        private PhysicCollisionListener _RightControllerCollider;

        #endregion Fields

        #region Input Actions
        /// <summary>
        /// Reference to the <see cref="InputAction"/> handling left trigger press.
        /// </summary>
        /// <seealso cref="LeftTriggerPressed(InputAction.CallbackContext)"/>
        [SerializeField]
        private InputActionReference _LeftTrigger;

        /// <summary>
        /// Reference to the <see cref="InputAction"/> handling right trigger press.
        /// </summary>
        /// <seealso cref="RightTriggerPressed(InputAction.CallbackContext)"/>
        [SerializeField]
        private InputActionReference _RightTrigger;

        /// <summary>
        /// Reference to the <see cref="InputAction"/> handling left controller pose (position and rotation) update.
        /// </summary>
        /// <seealso cref="LeftControllerPoseUpdate(InputAction.CallbackContext)"/>
        [SerializeField]
        private InputActionReference _LeftControllerPoseUpdate;

        /// <summary>
        /// Reference to the <see cref="InputAction"/> handling right controller pose (position and rotation) update.
        /// </summary>
        /// <seealso cref="RightControllerPoseUpdate(InputAction.CallbackContext)"/>
        [SerializeField]
        private InputActionReference _RightControllerPoseUpdate;

        #endregion Input Actions


        #region Events

        /// <summary>
        /// Event triggering when the left <see cref="PhysicCollisionListener"/> collide with another <see cref="Collider"/>.
        /// </summary>
        public event Action<CollisionResult> OnLeftControllerCollisionStart;

        /// <summary>
        /// Event triggering when the right <see cref="PhysicCollisionListener"/> collide with another <see cref="Collider"/>.
        /// </summary>
        public event Action<CollisionResult> OnRightControllerCollisionStop;


        /// <summary>
        /// Event triggering when the left <see cref="PhysicCollisionListener"/> stop colliding with another <see cref="Collider"/>.
        /// </summary>
        public event Action<CollisionResult> OnLeftControllerCollisionStop;

        /// <summary>
        /// Event triggering when the right <see cref="PhysicCollisionListener"/> stop colliding with another <see cref="Collider"/>.
        /// </summary>
        public event Action<CollisionResult> OnRightControllerCollisionStart;

        /// <summary>
        /// Allow external classes to trigger actions when the left trigger is Pressed.
        /// </summary>
        public event Action OnLeftTriggerPressed;

        /// <summary>
        /// Allow external classes to trigger actions when the left trigger is fully Pressed for the first time since last release.
        /// </summary>
        public event Action OnLeftTriggerFirstPress;

        /// <summary>
        /// Allow external classes to trigger actions when the left trigger is release.
        /// </summary>
        public event Action OnLeftTriggerReleased;
        private bool _LeftControllerTriggerPressedLastFrame = false;

        /// <summary>
        /// Allow external classes to trigger actions when the right trigger is pressed.
        /// </summary>
        public event Action OnRightTriggerPressed;

        /// <summary>
        /// Allow external classes to trigger actions when the right trigger is fully Pressed for the first time since last release.
        /// </summary>
        public event Action OnRightTriggerFirstPress;

        /// <summary>
        /// Allow external classes to trigger actions when the right trigger is released.
        /// </summary>
        public event Action OnRightTriggerReleased;
        private bool _RightControllerTriggerPressedLastFrame = false;

        /// <summary>
        /// Allow external classes to trigger actions when the right controller is moved.
        /// Action parameter is the controller position.
        /// </summary>
        public event Action<Vector3, Vector3> OnLeftControllerPositionUpdate;
        private bool _LeftControllerPoseHasBeenChangedThisFrame = true;

        /// <summary>
        /// Allow external classes to trigger actions when the left controller is moved.
        /// Action parameters are the controller position and velocity.
        /// </summary>
        public event Action<Vector3, Vector3> OnRightControllerPositionUpdate;
        private bool _RightControllerPoseHasBeenChangedThisFrame = true;


        #endregion Events

        #region Unity methods

        private void Start()
        {
            _LeftTrigger.action.performed += LeftTriggerPressed;

            _RightTrigger.action.performed += RightTriggerPressed;

            _LeftControllerPoseUpdate.action.performed += LeftControllerPoseUpdate;
            _RightControllerPoseUpdate.action.performed += RightControllerPoseUpdate;
        }

        /// <summary>
        /// LateUpdate is called every frame, if the Behaviour is enabled.
        /// It is called after all Update functions have been called.
        /// </summary>
        void LateUpdate()
        {
            _RightControllerPoseHasBeenChangedThisFrame = false;
            _LeftControllerPoseHasBeenChangedThisFrame = false;
        }


        #endregion Unity methods


        #region Callback registration methods

        private void TriggerLeftControllerCollision(CollisionResult result) => OnLeftControllerCollisionStart?.Invoke(result);
        private void TriggerRightControllerCollision(CollisionResult result) => OnRightControllerCollisionStart?.Invoke(result);
        private void TriggerLeftControllerCollisionStop(CollisionResult result) => OnLeftControllerCollisionStop?.Invoke(result);
        private void TriggerRightControllerCollisionStop(CollisionResult result) => OnRightControllerCollisionStop?.Invoke(result);


        /// <summary>
        /// Method to trigger whenever the left trigger press value change.
        /// </summary>
        /// <param name="context">Input action context from the Input system.</param>
        private void LeftTriggerPressed(InputAction.CallbackContext obj)
        {
            HandleTriggerPress(XRControllerButton.LeftTrigger, obj.ReadValue<float>());
        }

        /// <summary>
        /// Method to trigger whenever the right trigger press value change.
        /// </summary>
        /// <param name="context">Input action context from the Input system.</param>
        private void RightTriggerPressed(InputAction.CallbackContext obj)
        {
            HandleTriggerPress(XRControllerButton.RightTrigger, obj.ReadValue<float>());
        }

        /// <summary>
        /// Method to trigger when the left controller pose changes.
        /// </summary>
        /// <param name="context">Input action context from the Input system.</param>
        private void LeftControllerPoseUpdate(InputAction.CallbackContext obj)
        {
            if (!_LeftControllerPoseHasBeenChangedThisFrame)
            {
                var pose = obj.ReadValue<XRPose>();

                LeftControllerPosition = pose.position;
                LeftControllerVelocity = pose.velocity;
                OnLeftControllerPositionUpdate?.Invoke(LeftControllerPosition, LeftControllerVelocity);


                _LeftControllerPoseHasBeenChangedThisFrame = true;
            }
        }

        /// <summary>
        /// Method to trigger when the right controller pose changes.
        /// </summary>
        /// <param name="context">Input action context from the Input system.</param>
        private void RightControllerPoseUpdate(InputAction.CallbackContext obj)
        {
            if (!_RightControllerPoseHasBeenChangedThisFrame)
            {
                var pose = obj.ReadValue<XRPose>();

                RightControllerPosition = pose.position;
                RightControllerVelocity = pose.velocity;
                OnRightControllerPositionUpdate?.Invoke(RightControllerPosition, RightControllerVelocity);

                _RightControllerPoseHasBeenChangedThisFrame = true;
            }
        }


        #endregion Callback registration methods

        #region Setters

        /// <summary>
        /// Register on the new <see ref="PhysicCollisionListener"/> and unregister from the previous one, if any.
        /// </summary>
        /// <param name="colliderListener">The new <see ref="PhysicCollisionListener"/> to refer to.</param>
        public void SetLeftControllerCollider(PhysicCollisionListener colliderListener)
        {
            // Unregister from previous listener, if any.
            if (_LeftControllerCollider != null)
            {
                _LeftControllerCollider.OnCollisionStarted -= TriggerLeftControllerCollision;
                _LeftControllerCollider.OnCollisionStopped -= TriggerLeftControllerCollisionStop;
            }

            _LeftControllerCollider = colliderListener;
            _LeftControllerCollider.OnCollisionStarted += TriggerLeftControllerCollision;
            _LeftControllerCollider.OnCollisionStopped += TriggerLeftControllerCollisionStop;
        }

        /// <summary>
        /// Register on the new <see ref="PhysicCollisionListener"/> and unregister from the previous one, if any.
        /// </summary>
        /// <param name="colliderListener">The new <see ref="PhysicCollisionListener"/> to refer to.</param>
        public void SetRightControllerCollider(PhysicCollisionListener colliderListener)
        {
            // Unregister from previous listener, if any.
            if (_RightControllerCollider != null)
            {
                _RightControllerCollider.OnCollisionStarted -= TriggerRightControllerCollision;
                _RightControllerCollider.OnCollisionStopped -= TriggerRightControllerCollisionStop;
            }

            _RightControllerCollider = colliderListener;
            _RightControllerCollider.OnCollisionStarted += TriggerRightControllerCollision;
            _RightControllerCollider.OnCollisionStopped += TriggerRightControllerCollisionStop;
        }

        #endregion


        /// <summary>
        /// Handle weither the given trigger is fully pressed or released from a full press.
        /// Full press is determine by comparing <see cref="_TriggerFullPressThreshold"/> with <paramref name="pressValue"/>.
        /// </summary>
        /// <param name="trigger">The trigger to turn on/off.</param>
        /// <param name="pressValue">The pression intensity of the trigger (from 0 to 1).</param>
        private void HandleTriggerPress(XRControllerButton trigger, float pressValue)
        {
            if (!PressedButtons.HasFlag(trigger) && pressValue >= _TriggerFullPressThreshold)
            {
                Press(trigger);
            }
            else if (PressedButtons.HasFlag(trigger) && pressValue <= _TriggerFullPressThreshold)
            {
                UnPress(trigger);
            }
        }

    }
}