using System;

using UnityEngine;
using UnityEngine.InputSystem;

namespace MorphoNet.Interaction.Input
{
    /// <summary>
    /// Handle <see cref="InputAction"/>s triggered by the mouse.
    /// Trigger c# events based on thoose.
    /// </summary>
    public class MouseInputListener : MonoBehaviour
    {
        #region Input Actions

        /// <summary>
        /// Reference to the <see cref="InputAction"/> handling left mouse clicks.
        /// </summary>
        /// <seealso cref="ClickStarted(InputAction.CallbackContext)"/>
        /// <seealso cref="ClickCanceled(InputAction.CallbackContext)"/>
        [SerializeField]
        private InputActionReference _Click;

        /// <summary>
        /// Reference to the <see cref="InputAction"/> triggering on mouse movement.
        /// No callbacks is bound to this <see cref="InputAction"/> unless the left mouse button
        /// is pressed. This callback is unregistered when the button is released.
        /// </summary>
        /// <seealso cref="ClickStarted(InputAction.CallbackContext)"/>
        /// <seealso cref="ClickCanceled(InputAction.CallbackContext)"/>
        /// <seealso cref="MouseDragged(InputAction.CallbackContext)"/>
        [SerializeField]
        private InputActionReference _Drag;

        [SerializeField]
        private InputActionReference _ScrollWheel;

        #endregion Input Actions

        #region Events

        public event Action<Vector2> OnLeftButtonDown;

        public event Action<Vector2> OnLeftButtonUp;

        /// <summary>
        /// Allow external classes to trigger action when mouse move with the left button pressed.
        /// Action parameters are the mouse position and the mouse delta.
        /// </summary>
        public event Action<Vector2, Vector2> OnDrag;

        public event Action<Vector2> OnPositionUpdate;

        public event Action<float> OnScroll;

        #endregion Events

        #region Unity methods

        private void Start()
        {
            _Click.action.started += ClickStarted;
            _Click.action.canceled += ClickCanceled;

            _ScrollWheel.action.performed += ScrollPerformed;
        }

        private void FixedUpdate()
        {
            #if !UNITY_IOS && !UNITY_ANDROID
            OnPositionUpdate?.Invoke(CurrentMousePosition());
            #endif
        }

        #endregion Unity methods

        #region Input Actions callbacks

        /// <summary>
        /// Action to trigger when the mouse left button is pressed down.
        /// </summary>
        private void ClickStarted(InputAction.CallbackContext _)
        {
            OnLeftButtonDown?.Invoke(Mouse.current.position.ReadValue());
            _Drag.action.performed += MouseDragged;
        }

        /// <summary>
        /// Action to trigger when the click action is canceled (button up).
        /// </summary>
        private void ClickCanceled(InputAction.CallbackContext _)
        {
            OnLeftButtonUp?.Invoke(Mouse.current.position.ReadValue());
            // Unregister the action unless it wasn�t. In this case, does nothing.
            _Drag.action.performed -= MouseDragged;
        }

        /// <summary>
        /// Action to trigger when the mouse is moving with the left button down.
        /// </summary>
        /// <param name="context">Input action context from the Input system.</param>
        private void MouseDragged(InputAction.CallbackContext context)
        {
            OnDrag?.Invoke(CurrentMousePosition(), context.ReadValue<Vector2>());
        }

        private void ScrollPerformed(InputAction.CallbackContext context)
        {
            // Scroll get back a Vector2 in case of mouse supporting horizontal scrolling.
            // We do not use such scroll and mouse supporting it aren't common.
            OnScroll?.Invoke(context.ReadValue<Vector2>().y);
        }

        #endregion Input Actions callbacks

        /// <summary>
        /// Return the currently used mouse's position on screen, not in the window.
        /// </summary>
        public Vector2 CurrentMousePosition()
            => Mouse.current.position.ReadValue();
    }
}