using System;

using UnityEngine;
using UnityEngine.InputSystem;

namespace MorphoNet.Interaction.Input
{
    /// <summary>
    /// Handle <see cref="InputAction"/>s triggered by the keyboard.
    /// Trigger c# events based on thoose.
    /// </summary>
    public class KeyboardInputListener : MonoBehaviour
    {
        #region Input Actions

        [SerializeField]
        private InputActionReference _CButton;

        [SerializeField]
        private InputActionReference _XButton;

        [SerializeField]
        private InputActionReference _KButton;

        [SerializeField]
        private InputActionReference _LButton;

        [SerializeField]
        private InputActionReference _LeftShiftButton;

        [SerializeField]
        private InputActionReference _LeftCtrlButton;

        [SerializeField]
        private InputActionReference _SpaceButton;

        [SerializeField]
        private InputActionReference _LeftAltButton;

        #endregion Input Actions

        #region Events

        public event Action OnCPressed;

        public event Action OnCReleased;

        public event Action OnXPressed;

        public event Action OnXReleased;

        public event Action OnKPressed;

        public event Action OnKReleased;

        public event Action OnLPressed;

        public event Action OnLReleased;

        public event Action OnLeftCtrlPressed;

        public event Action OnLeftCtrlReleased;

        public event Action OnLeftShiftPressed;

        public event Action OnLeftShiftReleased;

        public event Action OnSpacePressed;

        public event Action OnSpaceReleased;

        public event Action OnLeftAltPressed;

        public event Action OnLeftAltReleased;

        #endregion Events

        #region Unity methods

        private void Start()
        {
            _CButton.action.performed += CButtonPressed;
            _CButton.action.canceled += CButtonReleased;

            _XButton.action.performed += XButtonPressed;
            _XButton.action.canceled += XButtonReleased;

            _KButton.action.performed += KButtonPressed;
            _KButton.action.canceled += KButtonReleased;

            _LButton.action.performed += LButtonPressed;
            _LButton.action.canceled += LButtonReleased;

            _LeftCtrlButton.action.performed += LeftCtrlButtonPressed;
            _LeftCtrlButton.action.canceled += LeftCtrlButtonReleased;

            _LeftShiftButton.action.performed += LeftShiftButtonPressed;
            _LeftShiftButton.action.canceled += LeftShiftButtonReleased;

            _SpaceButton.action.performed += SpaceButtonPressed;
            _SpaceButton.action.canceled += SpaceButtonReleased;

            _LeftAltButton.action.performed += LeftAltButtonPressed;
            _LeftAltButton.action.canceled += LeftAltButtonReleased;
        }

        #endregion Unity methods

        #region Input Actions callbacks

        /// <summary>
        /// Perform the appropriate calls when the C key is pressed.
        /// </summary>
        private void CButtonPressed(InputAction.CallbackContext _) => OnCPressed?.Invoke();

        /// <summary>
        /// Perform the appropriate calls when the C key is released.
        /// </summary>
        private void CButtonReleased(InputAction.CallbackContext _) => OnCReleased?.Invoke();

        /// <summary>
        /// Perform the appropriate calls when the X key is pressed.
        /// </summary>
        private void XButtonPressed(InputAction.CallbackContext _) => OnXPressed?.Invoke();

        /// <summary>
        /// Perform the appropriate calls when the X key is released.
        /// </summary>
        private void XButtonReleased(InputAction.CallbackContext _) => OnXReleased?.Invoke();

        /// <summary>
        /// Perform the appropriate calls when the K key is pressed.
        /// </summary>
        private void KButtonPressed(InputAction.CallbackContext _) => OnKPressed?.Invoke();

        /// <summary>
        /// Perform the appropriate calls when the K key is released.
        /// </summary>
        private void KButtonReleased(InputAction.CallbackContext _) => OnKReleased?.Invoke();

        /// <summary>
        /// Perform the appropriate calls when the L key is pressed.
        /// </summary>
        private void LButtonPressed(InputAction.CallbackContext _) => OnLPressed?.Invoke();

        /// <summary>
        /// Perform the appropriate calls when the L key is released.
        /// </summary>
        private void LButtonReleased(InputAction.CallbackContext _) => OnLReleased?.Invoke();

        /// <summary>
        /// Perform the appropriate calls when the Left Ctrl key is pressed.
        /// </summary>
        private void LeftCtrlButtonPressed(InputAction.CallbackContext _) => OnLeftCtrlPressed?.Invoke();

        /// <summary>
        /// Perform the appropriate calls when the Left Ctrl key is released.
        /// </summary>
        private void LeftCtrlButtonReleased(InputAction.CallbackContext _) => OnLeftCtrlReleased?.Invoke();

        /// <summary>
        /// Perform the appropriate calls when the Left Shift key is pressed.
        /// </summary>
        private void LeftShiftButtonPressed(InputAction.CallbackContext _) => OnLeftShiftPressed?.Invoke();

        /// <summary>
        /// Perform the appropriate calls when the Left Shift key is released.
        /// </summary>
        private void LeftShiftButtonReleased(InputAction.CallbackContext _) => OnLeftShiftReleased?.Invoke();

        /// <summary>
        /// Perform the appropriate calls when the Space key is pressed.
        /// </summary>
        private void SpaceButtonPressed(InputAction.CallbackContext _) => OnSpacePressed?.Invoke();

        /// <summary>
        /// Perform the appropriate calls when the Space key is released.
        /// </summary>
        private void SpaceButtonReleased(InputAction.CallbackContext _) => OnSpaceReleased?.Invoke();

        /// <summary>
        /// Perform the appropriate calls when the Left Alt key is pressed.
        /// </summary>
        private void LeftAltButtonPressed(InputAction.CallbackContext _) => OnLeftAltPressed?.Invoke();

        /// <summary>
        /// Perform the appropriate calls when the Left Alt key is released.
        /// </summary>
        private void LeftAltButtonReleased(InputAction.CallbackContext _) => OnLeftAltReleased?.Invoke();

        #endregion Input Actions callbacks
    }
}