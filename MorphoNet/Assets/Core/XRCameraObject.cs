using UnityEngine;

namespace MorphoNet
{
    public class XRCameraObject : MonoBehaviour
    {
        public static XRCameraObject Instance { get; private set; } = null;

        private void Awake()
        {
            Instance = this;
            //init depth in case
            Camera c = GetComponent<Camera>();
            if (c != null)
            {
                if (c.depthTextureMode != DepthTextureMode.Depth)
                    c.depthTextureMode = DepthTextureMode.Depth;
            }
            
        }
    }
}