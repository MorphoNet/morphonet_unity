using MorphoNet.Core;
using MorphoNet.Interaction.Physic;
using MorphoNet.UI.XR;
using System.Collections.Generic;
using UnityEngine;

namespace MorphoNet.XR
{
    public class LoadParametersXRRigConfig : MonoBehaviour
    {
        [SerializeField]
        private GameObject _HeadController;

        [SerializeField]
        private XRController _MainController;

        [SerializeField]
        private XRController _SecondaryController;

        [SerializeField]
        private XRDataSetMenu _DataSetMenu;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        private void Awake()
        {
            _DataSetMenu.SetButtonsColliders(new List<Collider>() { _MainController.PhysicCollisionListener.GetCollider });

            var device = XRController.ControllerDevice.Quest2Controller;

            _MainController.Device = device;
            _SecondaryController.Device = device;
        }
    }
}