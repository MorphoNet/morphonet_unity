using MorphoNet.Core;
using MorphoNet.UI.XR;
using System.Collections.Generic;
using UnityEngine;

namespace MorphoNet.XR
{
    public class XRRigConfig : MonoBehaviour
    {
        [SerializeField]
        private XRPanel _XRPanelPrefab;

        private XRPanel _XRPanel;

        [SerializeField]
        private Transform _PinnedMenuContainer;

        [SerializeField]
        private GameObject _HeadController;

        [SerializeField]
        private XRController _MainController;

        [SerializeField]
        private XRController _SecondaryController;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        private void Awake()
        {
            Init();
        }

        private void Init()
        {
            var startup = FindObjectOfType<StartUp>();
            var xrMediator = startup.GetXRMediator();
            var interacitonMediator = startup.GetInteractionMediator();

            _XRPanel = Instantiate(_XRPanelPrefab, _SecondaryController.MenuContainer);
            _XRPanel.Init(
                xrMediator,
                _PinnedMenuContainer,
                new List<Collider>() { _MainController.PhysicCollisionListener.GetCollider });

            var device = XRController.ControllerDevice.Quest2Controller;

            _MainController.Device = device;
            _SecondaryController.Device = device;

            interacitonMediator.SetControllers(_HeadController, _MainController, _SecondaryController);
        }
    }
}