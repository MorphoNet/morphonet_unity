using MorphoNet.Interaction;
using MorphoNet.Interaction.Input;
using MorphoNet.UI;
using MorphoNet.UI.Desktop;
using MorphoNet.UI.XR;

using System.Collections.Generic;

using UnityEngine;

namespace MorphoNet.Core
{
    /// <summary>
    /// This class is used as an entry point for the program.
    /// It initialize several classes of the and link them together.
    /// </summary>
    public class StartUp : MonoBehaviour
    {
        #region Inputs and interactions

        /// <summary>
        /// The <see cref="MouseInputListener"/> used in the instance of the program.
        /// </summary>
        [Header("Inputs and interaction")]
        [SerializeField]
        private MouseInputListener _MouseInputListener;

        /// <summary>
        /// The <see cref="KeyboardInputListener"/> used in the instance of the program.
        /// </summary>
        [SerializeField]
        private KeyboardInputListener _KeyboardInputListener;

        /// <summary>
        /// The <see cref="XRInputListener"/> used in the instance of the program.
        /// </summary>
        [SerializeField]
        private XRInputListener _XRInputListener;

        /// <summary>
        /// The <see cref="InteractionMediator"/> used in the instance of the program.
        /// </summary>
        private InteractionMediator _InteractionMediator;

        [SerializeField]
        private GameObject _CuttingPlanePrefab;

        #endregion Inputs and interactions

        #region UI

        /// <summary>
        /// The Mediator handling Desktop UI in this instance of the program.
        /// </summary>
        [Header("UI")]
        [SerializeField]
        private DesktopMediator _DesktopUIMediator;

        /// <summary>
        /// The Mediator handling XR UI in this instance of the program.
        /// </summary>
        [SerializeField]
        private XRMediator _XRMediator;

        /// <summary>
        /// The Mediator handling other UI (sub)mediators in this instance of the program.
        /// </summary>
        private UIMediator _UIMediator;

        #endregion UI

        #region Data Set

        /// <summary>
        /// Current <see cref="SetsManager"/> of the application.
        /// TODO: Should be replace by another class later (or at least be refactored).
        /// </summary>
        [Header("DataSetManager")]
        [SerializeField]
        private SetsManager _DataSetManager;

        #endregion Data Set

        private void Start()
        {
            _DataSetManager = SetsManager.instance;

            _InteractionMediator = new InteractionMediator(
                _MouseInputListener,
                _KeyboardInputListener,
                _XRInputListener,
                _DataSetManager,
                _CuttingPlanePrefab);

            _UIMediator = new UIMediator(
                new List<ISubMediator>() { _DesktopUIMediator, _XRMediator },
                _DataSetManager);

            _UIMediator.SetInteractionMediator(_InteractionMediator);
            _InteractionMediator.SetUIMediator(_UIMediator);

            SceneManager.Instance.CheckAndInitVR();
        }

        public XRMediator GetXRMediator() => _XRMediator;

        public InteractionMediator GetInteractionMediator() => _InteractionMediator;
    }
}