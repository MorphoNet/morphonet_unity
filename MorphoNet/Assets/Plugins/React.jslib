mergeInto(LibraryManager.library, {
	RetrieveParameters: function(){
		ReactUnityWebGL.RetrieveParameters();
	},
	loadingReady: function(){
		ReactUnityWebGL.LoadingReady();
	},
	selectFile: function(){
		ReactUnityWebGL.selectFile();
	},
	selectColormap: function(){
		ReactUnityWebGL.selectColormap();
	},
	DatasetLoadFinishedJS: function(){
		ReactUnityWebGL.DatasetLoadFinished();
	},
	OpenLineage: function(i){
		ReactUnityWebGL.OpenLineage(i);
	},
	CloseLineage: function(){
		ReactUnityWebGL.CloseLineage();
	},
	ClearAll: function(){
		ReactUnityWebGL.ClearAll();
	},
	Hide: function(tid){
		ReactUnityWebGL.Hide(Pointer_stringify(tid));
	},
	Show: function(tid){

		ReactUnityWebGL.Show(Pointer_stringify(tid));
	},
	HideCellsAllTime: function(vlistcells){
		ReactUnityWebGL.HideCellsAllTime(Pointer_stringify(vlistcells));
	},
	Colorize: function(tidrgb){
		ReactUnityWebGL.Colorize(Pointer_stringify(tidrgb));
	},
	Uncolorize: function(tid){
		ReactUnityWebGL.Uncolorize(Pointer_stringify(tid));
	},
	ApplySelection: function(rgblistcells){
		ReactUnityWebGL.ApplySelection(Pointer_stringify(rgblistcells));
	},
	ApplySelectionMap: function(rgblistcells){
		ReactUnityWebGL.ApplySelectionMap(Pointer_stringify(rgblistcells));
	},
	ResetSelectionOnSelectedCells: function(listcells){
		ReactUnityWebGL.ResetSelectionOnSelectedCells(Pointer_stringify(listcells));
	},
	HideCells: function(vlistcells){
		ReactUnityWebGL.HideCells(Pointer_stringify(vlistcells));
	},
	PropagateThis: function(vrgblistcells){
		ReactUnityWebGL.PropagateThis(Pointer_stringify(vrgblistcells));
	},
	SendLineageMorphoPlot: function(lineage){
		ReactUnityWebGL.SendLineageMorphoPlot(Pointer_stringify(lineage));
	},
	ReceiveMinTime : function(min_time){
		ReactUnityWebGL.ReceiveMinTime(min_time);
	},
	ReceiveMaxTime : function(max_time){
		ReactUnityWebGL.ReceiveMaxTime(max_time);
	},
	SendInfoToLineage : function(infostring){
		ReactUnityWebGL.SendInfoToLineage(Pointer_stringify(infostring));
	},
	UncolorizeMultiple : function(listcells){
		ReactUnityWebGL.UncolorizeMultiple(Pointer_stringify(listcells));
	},
	ColorizeMultiple : function(clistcells){
		ReactUnityWebGL.ColorizeMultiple(Pointer_stringify(clistcells));
	},
	LeaveFullscreen : function(){
		ReactUnityWebGL.LeaveFullscreen();
	},
	updateInfoDeployed : function(info_id){
		ReactUnityWebGL.updateInfoDeployed(info_id);
	},
	multipleSelection : function(listcellscolor){
		ReactUnityWebGL.multipleSelection(Pointer_stringify(listcellscolor));
	},
	resetLineage : function(){
		ReactUnityWebGL.resetLineage();
	},
	ReceiveColorInfos : function(iddata){
		ReactUnityWebGL.ReceiveColorInfos(Pointer_stringify(iddata));
	},
	SendColors : function(listcellscolor){
		ReactUnityWebGL.SendColors(Pointer_stringify(listcellscolor));
	},
	SendShowCells : function(celllist){
		ReactUnityWebGL.SendShowCells(Pointer_stringify(celllist));
	},
	SendHideCells : function(celllist){
		ReactUnityWebGL.SendHideCells(Pointer_stringify(celllist));
	},
	SendPropagateTime : function(celllist){
		ReactUnityWebGL.SendPropagateTime(Pointer_stringify(celllist));
	},
	clearCells : function(celllist){
		ReactUnityWebGL.clearCells(Pointer_stringify(celllist));
	},
	SendBackgroundColor : function(color){
		ReactUnityWebGL.SendBackgroundColor(Pointer_stringify(color))
	},
	updateSelectionColor : function(parames){
		ReactUnityWebGL.updateSelectionColor(Pointer_stringify(parames))
	},
	cancelPropagationListCells : function(parames){
		ReactUnityWebGL.cancelPropagationListCells(Pointer_stringify(parames))
	},
	pickCells : function(parames){
		ReactUnityWebGL.pickCells(Pointer_stringify(parames))
	},


});
