using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.IO;

[Serializable]
public class Object3DImage : MonoBehaviour, IPointerDownHandler, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    private string _Name;
    public string Name { get => _Name; set => _Name = value; }

    private int _OriginalTime;
    public int OriginalTime { get => _OriginalTime; set => _OriginalTime = value; }

    private int _Time;
    public int Time { get => _Time; set => _Time = value; }

    private int _Channel;
    public int Channel { get => _Channel; set => _Channel = value; }

    private int _OriginalChannel;
    public int OriginalChannel { get => _OriginalChannel; set => _OriginalChannel = value; }

    private string _Path;
    public string Path { get => _Path; set => _Path = value; }

    private Vector3 _VoxelSize;
    public Vector3 VoxelSize { get => _VoxelSize; set => _VoxelSize = value; }

    private Vector3 _Dimensions;
    public Vector3 Dimensions { get => _Dimensions; set => _Dimensions = value; }

    private int[] _Encoding;
    public int[] Encoding { get => _Encoding; set => _Encoding = value; }

    [SerializeField]
    private Text _NameText;

    [SerializeField]
    private Text _TimeText;

    [SerializeField]
    private Text _ChannelText;

    [SerializeField]
    private Image _Background;

    private RectTransform _DragCopy;

    private Color _GhostColor = new Color(.5f, .5f, .5f, .5f);
    private Color _SelectColor = new Color(.66f, 1f, .66f, 1f);
    private Color _ReplaceColor = new Color(.66f, .86f, 1f, 1f);

    private Canvas _ParentCanvas;

    public void SetFromData(Image3D source)
    {
        _Name = source.Name;
        _OriginalTime = source.OriginalTime;
        _OriginalChannel = source.OriginalChannel;
        _Time = source.Time;
        _Channel = source.Channel;
        _Path = source.Path;
        _VoxelSize = new Vector3(source.VoxelSize[0], source.VoxelSize[1], source.VoxelSize[2]);
        _Dimensions = new Vector3(source.Size[0], source.Size[1], source.Size[2]);
    }

    public void Start()
    {
        Canvas[] c = GetComponentsInParent<Canvas>();
        _ParentCanvas = c[c.Length - 1];
    }

    public void SetName(string name)
    {
        string nameWithoutExt = name.Split('.')[0];
        _NameText.text = nameWithoutExt;
    }

    public void SetTime(string time)
    {
        _TimeText.text = time;
    }

    public void SetChannel(string channel)
    {
        _ChannelText.text = channel;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        CreateDatasetMenuManager.Instance.HideAllOptionMenus();
        CreateDatasetMenuManager.Instance.ResetAllImgColors();
        _Background.color = _SelectColor;
        //int filenamelen = System.IO.Path.GetFileName(_Path).Split('.')[0].Length;
        //string nameWithExt = _Name + System.IO.Path.GetFileName(_Path).Substring(filenamelen);
        //Debug.LogError(nameWithExt);
        CreateDatasetMenuManager.Instance.SetImageMenuOptions(_Name,_Time,_Channel,_VoxelSize,_Dimensions,_Path,this, _OriginalTime, _OriginalChannel);
    }

    public void ResetColor()
    {
        _Background.color = Color.white;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        _Background.color = _GhostColor;
        _DragCopy = Instantiate(this, _ParentCanvas.transform).GetComponent<RectTransform>();
        _DragCopy.gameObject.GetComponent<Object3DImage>()._Background.color = Color.white;
        Destroy(_DragCopy.gameObject.GetComponent<Object3DImage>());
        CreateDatasetMenuManager.Instance.IsDragging = true;
        CreateDatasetMenuManager.Instance.DraggingObject = this;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (_DragCopy != null)
            _DragCopy.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _Background.color = Color.white;
        CreateDatasetMenuManager.Instance.IsDragging = false;
        Destroy(_DragCopy.gameObject);
    }

    public void OnPointerEnter()
    {
        if (CreateDatasetMenuManager.Instance.IsDragging && CreateDatasetMenuManager.Instance.DraggingObject != this)
            _Background.color = _ReplaceColor;
    }

    public void OnPointerExit()
    {
        _Background.color = Color.white;
    }
}
