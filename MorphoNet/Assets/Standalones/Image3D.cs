using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class Image3D
{
    public string Name;
    public int OriginalTime;
    public int OriginalChannel;
    public int Time;
    public int Channel;
    public string Path;
    public float[] VoxelSize = new float[3];
    public int[] Size = new int[3];
    public int[] Encoding;

    public Image3D()
    {
        Name = "";
        OriginalTime = 0;
        OriginalChannel = 0;
        Time = 0;
        Channel = 0;
        Path = "";
        VoxelSize = new float[] { 0, 0, 0 };
        Size = new int[] { 0, 0, 0 };
        Encoding = new int[] {0,1,2,3,4 };
    }

    public Image3D(string name,int original_t,int original_c,int t, int c, string path, float[] vs, int[] size, int[] encoding) 
    {
        Name = name;
        OriginalTime = original_t;
        OriginalChannel = original_c;
        Time = t;
        Channel = c;
        Path = path;
        VoxelSize = vs;
        Size = size;
        Encoding = encoding;
    }

    public Image3D(Object3DImage source)
    {
        Name = source.Name;
        OriginalTime = source.OriginalTime;
        OriginalChannel = source.OriginalChannel;
        Time = source.Time;
        Channel = source.Channel;
        Path = source.Path;
        VoxelSize = new float[] {source.VoxelSize[0], source.VoxelSize[1] , source.VoxelSize[2] };
        Size = new int[] {(int)source.Dimensions[0], (int)source.Dimensions[1] , (int)source.Dimensions[2] };
        Encoding = source.Encoding;
    }

    public bool IsEqual(Image3D iobj)
    {
        if (Name != iobj.Name) return false;
        if (OriginalTime != iobj.OriginalTime) return false;
        if (OriginalChannel != iobj.OriginalChannel) return false;
        if (Channel != iobj.Channel) return false;
        if (Path != iobj.Path) return false;
        if (!VoxelSize.SequenceEqual(iobj.VoxelSize)) return false;
        if (!Size.SequenceEqual(iobj.Size)) return false;
        if (!Encoding.SequenceEqual(iobj.Encoding)) return false;
        if (Time != iobj.Time) return false;
        return true;
    }
}
