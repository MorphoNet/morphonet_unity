using MorphoNet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static UnityEngine.UI.Dropdown;

public class CreateDatasetMenuManager : MonoBehaviour
{

    public static CreateDatasetMenuManager Instance;

    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
            Destroy(this.gameObject);
    }

    [SerializeField]
    private GameObject EmptyImagePanel;

    [SerializeField]
    private Object3DImage GenericImageObject;

    [SerializeField]
    private GameObject SegmentedPanel;

    [SerializeField]
    private GameObject IntensityPanel;

    [SerializeField]
    private GameObject PropertiesPanel;

    [SerializeField]
    private GameObject MetadataPanel;

    [SerializeField]
    private GameObject MetadataAwaitPanel;

    [SerializeField]
    private Toggle SegToggle;

    [SerializeField]
    private Toggle IntensityToggle;

    [SerializeField]
    private Toggle PropertiesToggle;

    [SerializeField]
    private InputField DatasetNameField;

    [SerializeField]
    private Text DatasetSegTimes;

    [SerializeField]
    private Text DatasetIntensityTimes;

    [Header("Metadata menu options")]
    [SerializeField]
    private Text MetadataPath;

    [SerializeField]
    private Text MetadataInputType;

    [SerializeField]
    private Text MetadataEncoding;

    [SerializeField]
    private InputField MetadataVoxelX;
    [SerializeField]
    private InputField MetadataVoxelY;
    [SerializeField]
    private InputField MetadataVoxelZ;

    [SerializeField]
    private Dropdown Metadata0;
    [SerializeField]
    private Dropdown Metadata1;
    [SerializeField]
    private Dropdown Metadata2;
    [SerializeField]
    private Dropdown Metadata3;
    [SerializeField]
    private Dropdown Metadata4;

    private Dropdown[] _DimDropdowns = new Dropdown[5];
    private Text[] _DimTexts = new Text[5];

    [SerializeField]
    private Text Metadata0Value;
    [SerializeField]
    private Text Metadata1Value;
    [SerializeField]
    private Text Metadata2Value;
    [SerializeField]
    private Text Metadata3Value;
    [SerializeField]
    private Text Metadata4Value;

    [SerializeField]
    private InputField MetadataTimeOffset;

    [SerializeField]
    public Button ConfirmMetadata;

    [Header("segmented menu options")]
    [SerializeField]
    private GameObject SegmentedImageOptions;
    [SerializeField]
    private Text SegImageName;
    [SerializeField]
    private InputField SegImageTime;
    [SerializeField]
    private InputField SegImageChannel;
    [SerializeField]
    private Text SegImageVoxelSize;
    [SerializeField]
    private Text SegImageSize;
    [SerializeField]
    private Text SegImagePath;
    [SerializeField]
    private Text SegImageOriginalTime;
    [SerializeField]
    private Text SegImageOriginalChannel;
    [SerializeField]
    private Button SegImageRemove;
    [SerializeField]
    private Button AddSegImageButton;

    [SerializeField]
    private RectTransform SegChannel0Panel;
    [SerializeField]
    private RectTransform SegChannel1Panel;
    [SerializeField]
    private RectTransform SegChannel2Panel;
    [SerializeField]
    private RectTransform SegChannel3Panel;

    [SerializeField]
    private InputField DownscaleSegXYField;
    [SerializeField]
    private InputField DownscaleSegZField;

    [SerializeField]
    private Slider DownscaleSegXYSlider;
    [SerializeField]
    private Slider DownscaleSegZSlider;

    [SerializeField]
    private InputField BackgroundField;

    [SerializeField]
    private ScrollRect SegScrollView;

    [SerializeField]
    private GameObject DisableSegPanel;

    [SerializeField]
    private Button ClearSegButton;

    [Header("advanced mesh menu options")]
    [SerializeField]
    private GameObject AdvancedMeshMenu;

    [SerializeField]
    public Toggle Smoothing;
    [SerializeField]
    public InputField SmoothingPassband;
    [SerializeField]
    public InputField SmoothingIterations;

    [SerializeField]
    public Toggle QuadricClustering;
    [SerializeField]
    public InputField QCDivisions;

    [SerializeField]
    public Toggle Decimation;
    [SerializeField]
    public InputField DecimateReduction;
    [SerializeField]
    public InputField DecimateThreshold;

    [Header("intensity menu options")]
    [SerializeField]
    private GameObject IntensityImageOptions;
    [SerializeField]
    private Text IntensityImageName;
    [SerializeField]
    private InputField IntensityImageTime;
    [SerializeField]
    private InputField IntensityImageChannel;
    [SerializeField]
    private Text IntensityImageVoxelSize;
    [SerializeField]
    private Text IntensityImageSize;
    [SerializeField]
    private Text IntensityImagePath;
    [SerializeField]
    private Button IntensityImageRemove;
    [SerializeField]
    private Text IntensityImageOriginalTime;
    [SerializeField]
    private Text IntensityImageOriginalChannel;

    [SerializeField]
    private RectTransform IntensityChannel0Panel;
    [SerializeField]
    private RectTransform IntensityChannel1Panel;
    [SerializeField]
    private RectTransform IntensityChannel2Panel;
    [SerializeField]
    private RectTransform IntensityChannel3Panel;

    [SerializeField]
    private InputField DownscaleRawXYField;
    [SerializeField]
    private InputField DownscaleRawZField;

    [SerializeField]
    private Slider DownscaleRawXYSlider;
    [SerializeField]
    private Slider DownscaleRawZSlider;

    [SerializeField]
    private ScrollRect RawScrollView;

    [Header("properties menu options")]
    [SerializeField]
    private InputField PropertiesXMLFile;
    [SerializeField]
    private Button PropertiesXMLAddButton;

    [Header("Edit mode options")]
    [SerializeField]
    public Button EditButton;
    [SerializeField]
    private Button LaunchButton;
    [SerializeField]
    public Button DeleteDatasetButton;
    [SerializeField]
    public Button PrepareUploadDatasetButton;
    [SerializeField]
    public Button UploadDatasetButton;
    [SerializeField]
    private GameObject UploadMenu;
    [SerializeField]
    private InputField UploadDatasetName;

    [SerializeField]
    private GameObject ExportSubMenu;
    [SerializeField]
    private GameObject ExportWindow;
    [SerializeField]
    private Dropdown ImageExportFormat;
    [SerializeField]
    private Toggle CompressExport;
    [SerializeField]
    private Dropdown PropertyExportFormat;
    [SerializeField]
    private Dropdown SKPropertyExportFormat;


    #region generic UI references for switching
    private GameObject _CurrentImageOptions;
    private Text _CurrentImageName;
    private InputField _CurrentImageTime;
    private InputField _CurrentImageChannel;
    private Text _CurrentImageVoxelSize;
    private Text _CurrentImagelSize;
    private Text _CurrentImagePath;
    private Text _CurrentImageOriginalT;
    private Text _CurrentImageOriginalC;
    private Button _CurrentImageRemove;

    private RectTransform _CurrentChannel0Panel;
    private RectTransform _CurrentChannel1Panel;
    private RectTransform _CurrentChannel2Panel;
    private RectTransform _CurrentChannel3Panel;

    private ScrollRect _CurrentScrollView;
    #endregion


    private bool _IsDragging = false;
    public bool IsDragging { get => _IsDragging; set => _IsDragging = value; }

    private Object3DImage _DraggingObject;
    public Object3DImage DraggingObject { get => _DraggingObject; set => _DraggingObject = value; }

    private GameObject _HoverObject;

    private bool _OptionsMode = false;

    private bool _DisabledSegEdit = false;


    const int CHANNEL_MAX = 4;
    const int EMPTY_TIMES = 5;

    private int[] _InitMetadataOrder;

    //internal parameters
    private bool _SegMenuActive = true;
    private Dictionary<int, Dictionary<int, Object3DImage>> Objects3DImages = new Dictionary<int, Dictionary<int, Object3DImage>>();
    private Dictionary<int, Dictionary<int, Object3DImage>> SegObjects3DImages = new Dictionary<int, Dictionary<int, Object3DImage>>();
    private Dictionary<int, Dictionary<int, Object3DImage>> IntensityObjects3DImages = new Dictionary<int, Dictionary<int, Object3DImage>>();

    private Color _WarningRed = new Color(0.9716f, 0.7756f, 0.7756f, 1f);

    //public static string[] OUT_FILE_FORMATS = {".czi",".dv",".mha",".nii",".nd2",".tiff", ".tif",".ometiff",".inr" };
    public static string[] OUT_FILE_FORMATS = {".mha",".nii",".tiff", ".tif",".ometiff",".inr" };


    public void Start()
    {
        Metadata0.onValueChanged.AddListener(delegate { CheckForNoDoublesOnDropDowns(Metadata0); });
        Metadata1.onValueChanged.AddListener(delegate { CheckForNoDoublesOnDropDowns(Metadata1); });
        Metadata2.onValueChanged.AddListener(delegate { CheckForNoDoublesOnDropDowns(Metadata2); });
        Metadata3.onValueChanged.AddListener(delegate { CheckForNoDoublesOnDropDowns(Metadata3); });
        Metadata4.onValueChanged.AddListener(delegate { CheckForNoDoublesOnDropDowns(Metadata4); });

        //init arrays
        _DimTexts = new Text[5];
        _DimTexts[0] = Metadata0Value;
        _DimTexts[1] = Metadata1Value;
        _DimTexts[2] = Metadata2Value;
        _DimTexts[3] = Metadata3Value;
        _DimTexts[4] = Metadata4Value;
        //also set proper values for XYZCT if specific metadata
        _DimDropdowns = new Dropdown[5];
        _DimDropdowns[0] = Metadata0;
        _DimDropdowns[1] = Metadata1;
        _DimDropdowns[2] = Metadata2;
        _DimDropdowns[3] = Metadata3;
        _DimDropdowns[4] = Metadata4;

        //InitFillGrids();

        SegToggle.onValueChanged.AddListener(delegate { SetMenuVisible(SegmentedPanel); });
        IntensityToggle.onValueChanged.AddListener(delegate { SetMenuVisible(IntensityPanel); });
        PropertiesToggle.onValueChanged.AddListener(delegate { SetMenuVisible(PropertiesPanel); });
        
        //set seg mode by default
        SetSegMode(true);

        //fill formats
        foreach(var f in OUT_FILE_FORMATS)
        {
            ImageExportFormat.options.Add(new OptionData(f));
        }
        ImageExportFormat.value = 2;//default at .tiff
    }

    public void ToggleAdvancedMeshMenuDisplay()
    {
        AdvancedMeshMenu.SetActive(!AdvancedMeshMenu.activeSelf);
    }

    public void Update()
    {
        if(!(_DisabledSegEdit && _SegMenuActive))//only available if not in frozen mode
        {
            if (IsDragging)//manage preview for drag/drop swap
            {
                PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
                eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                List<RaycastResult> results = new List<RaycastResult>();
                EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
                Empty3DObject ERayT = null;
                Object3DImage RayT = null;
                bool hover_leave = true;
                foreach (var r in results)
                {
                    if (r.gameObject.TryGetComponent(out ERayT) || r.gameObject.TryGetComponent(out RayT))
                    {
                        GameObject target = null;
                        if (RayT != null && RayT == DraggingObject && _HoverObject != null)//if we are on the base object, same as leaving
                        {
                            Object3DImage temp = null;
                            if (_HoverObject.TryGetComponent(out temp))
                                temp.OnPointerExit();
                            else
                                _HoverObject.GetComponent<Empty3DObject>().OnPointerExit();
                            _HoverObject = null;
                        }
                        if (RayT != null && RayT != DraggingObject)
                        {
                            target = RayT.gameObject;
                        }
                        else if (ERayT != null)
                        {
                            target = ERayT.gameObject;
                        }

                        if (_HoverObject == null && target != null)//if no hover object, add hover & enter functions call
                        {
                            _HoverObject = target;
                            Object3DImage temp = null;
                            if (_HoverObject.TryGetComponent(out temp))
                                temp.OnPointerEnter();
                            else
                                _HoverObject.GetComponent<Empty3DObject>().OnPointerEnter();
                        }

                        if (_HoverObject != null && target != null && _HoverObject != target)//if new object was not the one on which we hovered previously (basically changing hover)
                        {
                            Object3DImage temp = null;
                            if (_HoverObject.TryGetComponent(out temp))
                                temp.OnPointerExit();
                            else
                                _HoverObject.GetComponent<Empty3DObject>().OnPointerExit();

                            _HoverObject = target;
                            if (_HoverObject.TryGetComponent(out temp))
                                temp.OnPointerEnter();
                            else
                                _HoverObject.GetComponent<Empty3DObject>().OnPointerEnter();
                        }

                        if (_HoverObject != null)//means we have an hover object
                        {
                            hover_leave = false;
                        }
                    }
                }
                if (hover_leave && _HoverObject != null)//at the end, if we found nothing, exit
                {
                    Object3DImage temp = null;
                    if (_HoverObject.TryGetComponent(out temp))
                        temp.OnPointerExit();
                    else
                        _HoverObject.GetComponent<Empty3DObject>().OnPointerExit();
                    _HoverObject = null;
                }
            }
            else if (_HoverObject)
            {
                //in that case we are dropping an object. replacing
                Swap3DImages(DraggingObject, _HoverObject);

                Object3DImage temp = null;
                if (_HoverObject.TryGetComponent(out temp))
                    temp.OnPointerExit();
                else
                    _HoverObject.GetComponent<Empty3DObject>().OnPointerExit();
                _HoverObject = null;
            }
        }
        
    }

    #region AccessProperties

    public Dictionary<int, Dictionary<int, Object3DImage>> GetSegData()
    {
        return SegObjects3DImages;
    }

    public Dictionary<int, Dictionary<int, Image3D>> GetConvertedSegData()
    {
        Dictionary<int, Dictionary<int, Image3D>> dict = new Dictionary<int, Dictionary<int, Image3D>>();
        foreach (var t in SegObjects3DImages)
        {
            dict.Add(t.Key, new Dictionary<int, Image3D>());
            foreach (var c in t.Value)
            {
                dict[t.Key].Add(c.Key, new Image3D(SegObjects3DImages[t.Key][c.Key]));
            }
        }
        return dict;
    }

    public Dictionary<int, Dictionary<int, Object3DImage>> GetIntensityData()
    {
        return IntensityObjects3DImages;
    }

    public Dictionary<int, Dictionary<int, Image3D>> GetConvertedRawData()
    {
        Dictionary<int, Dictionary<int, Image3D>> dict = new Dictionary<int, Dictionary<int, Image3D>>();
        foreach (var t in IntensityObjects3DImages)
        {
            dict.Add(t.Key, new Dictionary<int, Image3D>());
            foreach (var c in t.Value)
            {
                dict[t.Key].Add(c.Key, new Image3D(IntensityObjects3DImages[t.Key][c.Key]));
            }
        }
        return dict;
    }

    public int GetSegMinTime()
    {
        //get min time from raw and seg, and return the smallest
        foreach(var seg in SegObjects3DImages.OrderBy(im =>im.Key))//starting from lowest time
        {
            if (seg.Value.Count > 0)//if we have any data in the time point
                return seg.Key;//return the time
        }
        return int.MaxValue;
    }

    public int GetSegMaxTime()
    {
        //get max time from raw and seg and return the highest
        foreach (var seg in SegObjects3DImages.OrderByDescending(im => im.Key))//starting from highest time
        {
            if (seg.Value.Count > 0)//if we have any data in the time point
                return seg.Key;//return the time
        }
        return int.MinValue;
    }

    public int GetRawMinTime()
    {
        //get min time from raw and seg, and return the smallest
        foreach (var seg in IntensityObjects3DImages.OrderBy(im => im.Key))//starting from lowest time
        {
            if (seg.Value.Count > 0)//if we have any data in the time point
                return seg.Key;//return the time
        }
        return int.MaxValue;
    }

    public int GetRawMaxTime()
    {
        //get max time from raw and seg and return the highest
        foreach (var seg in IntensityObjects3DImages.OrderByDescending(im => im.Key))//starting from highest time
        {
            if (seg.Value.Count > 0)//if we have any data in the time point
                return seg.Key;//return the time
        }
        return int.MinValue;
    }

    public int GetMinTime()
    {
        return Mathf.Min(GetSegMinTime(), GetRawMinTime());
    }

    public int GetMaxTime()
    {
        return Mathf.Max(GetSegMaxTime(), GetRawMaxTime());
    }

    public string GetDatasetName()
    {
        return DatasetNameField.text;
    }

    public int GetSegDownscale()
    {
        return (int)DownscaleSegXYSlider.value;
    }

    public int GetSegZDownscale()
    {
        return (int)DownscaleSegZSlider.value;
    }

    public int GetRawDownscale()
    {
        return (int)DownscaleRawXYSlider.value;
    }

    public int GetRawZDownscale()
    {
        return (int)DownscaleRawZSlider.value;
    }

    public string GetBackgroundValue()
    {
        return BackgroundField.text;
    }

    public string GetXMLFile()
    {
        return PropertiesXMLFile.text;
    }

    public string GetExportImageFormat()
    {
        return ImageExportFormat.options[ImageExportFormat.value].text;
    }

    public int GetExportPropertyFormat()
    {
        return PropertyExportFormat.value;
    }

    public int GetSKExportPropertyFormat()
    {
        return PropertyExportFormat.value;
    }

    public bool GetExportCompressed()
    {
        return CompressExport.isOn;
    }
    #endregion

    public void Swap3DImages(Object3DImage image, GameObject target)
    {
        //use scene info to avoid reference issues
        int swap_c = int.Parse(target.transform.parent.name.Replace("Channel", "").Replace("Panel", ""));
        int swap_t = target.transform.GetSiblingIndex();
        int og_c = int.Parse(image.transform.parent.name.Replace("Channel", "").Replace("Panel", ""));
        int og_t = image.transform.GetSiblingIndex();
        int c = 0;
        bool fix_time = false;

        //if we are gonna create a "hole" by moving this image, we need to move the images "on top" afterwards.
        if(Objects3DImages[og_t].Count > 0)
        {
            Object3DImage img = null;
            if (Objects3DImages[og_t].Keys.Max() > og_c && !target.TryGetComponent(out img))
            {
                fix_time = true;
            }
        }

        if (!Objects3DImages.ContainsKey(swap_t))
        {
            Objects3DImages.Add(swap_t, new Dictionary<int, Object3DImage>());
            c = 0;
        }
        else
        {
            //HERE WAS -1?
            if (Objects3DImages[swap_t].Count<=swap_c)//if you are leaving a hole in channels basically
            {
                c = Objects3DImages[swap_t].Count;
                if (Objects3DImages[swap_t].LastOrDefault().Value == DraggingObject)//if the object itself was here, do one less since it counts itself otherwise
                    c--;
                    
            }
            else//otherwise, just swap normally !
            {
                c = swap_c;
            }
        }
        //generic swapping procedure
        target = GetChannelPanel(c).GetChild(swap_t).gameObject;
        target.transform.SetParent(GetChannelPanel(og_c));
        target.transform.SetSiblingIndex(og_t);

        Object3DImage swappedObject = null;

        //then do the rest
        if (!Objects3DImages[swap_t].ContainsKey(c))
            Objects3DImages[swap_t].Add(c, image);
        else
        {
            //case in which you replace : keep a copy
            swappedObject = Objects3DImages[swap_t][c];
            Objects3DImages[swap_t][c] = image;
        }
            
        Objects3DImages[og_t].Remove(og_c);
        

        //then swap the gameobjects
        image.transform.SetParent(GetChannelPanel(c));
        image.transform.SetSiblingIndex(swap_t);
        image.Channel = c;
        image.Time = swap_t;

        //if swapped object is an image, change its time and channel values !
        //Object3DImage temp = null;
        //if (_HoverObject.TryGetComponent(out temp))
        if (swappedObject!=null)
        {
            swappedObject.Channel = og_c;
            swappedObject.Time = og_t;
            Objects3DImages[og_t].Add(og_c, swappedObject);
        }
        else//if we remove everything in original time, clear the time from dict (we do not want empty times)
        {
            if (Objects3DImages[og_t].Count == 0)
                Objects3DImages.Remove(og_t);
        }

        if (fix_time)//fix original time point by replacing holes created by swapping
        {
            
            FixHoles(og_t);
        }

        AutoFillGrid();
        DraggingObject = null;
        AutoSetMinMaxTimes();
    }

    /// <summary>
    /// automatically fix "holes" in segmented grid
    /// </summary>
    /// <param name="t">time point on which to fix</param>
    public void FixHoles(int t)
    {
        if (Objects3DImages.ContainsKey(t))
        {
            //from the bottom to up : 
            for (int i = 1; i < CHANNEL_MAX; i++)
            {
                if (Objects3DImages[t].ContainsKey(i) && !Objects3DImages[t].ContainsKey(i-1))//if cell below is empty, swap the two cells
                {
                    //if(GetChannelPanel(i - 1).GetChild(t).gameObject.TryGetComponent(out cell))
                    //{
                        Object3DImage tile = Objects3DImages[t][i];
                        GameObject cell = GetChannelPanel(i - 1).GetChild(t).gameObject;
                        cell.transform.SetParent(GetChannelPanel(i));
                        cell.transform.SetSiblingIndex(t);
                        tile.transform.SetParent(GetChannelPanel(i - 1));
                        tile.transform.SetSiblingIndex(t);
                        tile.Channel--;
                        if (Objects3DImages[t].ContainsKey(i - 1))
                            Objects3DImages[t][i - 1] = tile;
                        else
                            Objects3DImages[t].Add(i - 1, tile);

                        Objects3DImages[t].Remove(i);
                    //}
                }
            }
        }
    }

    /// <summary>
    /// fille both data grid at initialisation
    /// </summary>
    /// <param name="empty_cells">number of empty cells to add</param>
    public void InitFillGrids(int empty_cells=EMPTY_TIMES)
    {
        for(int i = 0; i < CHANNEL_MAX; i++)
        {
            for(int j = 0; j < empty_cells; j++)
            {
                Instantiate(EmptyImagePanel, GetSegChannelPanel(i));
                Instantiate(EmptyImagePanel, GetIntensityChannelPanel(i));
            }
        }
    }

    /// <summary>
    /// Fill current grid with empty spaces. To be used with offset option
    /// </summary>
    /// <param name="empty_cells">number of empty cells to add</param>
    public void FillGridWithEmptySpaces(int empty_cells)
    {
        for (int i = 0; i < CHANNEL_MAX; i++)
        {
            for (int j = 0; j < empty_cells; j++)
            {
                Instantiate(EmptyImagePanel, GetChannelPanel(i));
            }
        }
    }

    public void ResetAllImgColors()
    {
        foreach(var t in Objects3DImages)
        {
            foreach(var c in t.Value)
            {
                c.Value.ResetColor();
            }
        }
    }


    public Transform GetChannelPanel(int channel)
    {
        switch (channel)
        {
            case 0:
                return _CurrentChannel0Panel;
            case 1:
                return _CurrentChannel1Panel;
            case 2:
                return _CurrentChannel2Panel;
            case 3:
                return _CurrentChannel3Panel;
            default:
                MorphoDebug.LogError("ERROR : tried to get unsupported seg channel value " + channel);
                return null;
        }
    }

    public Transform GetSegChannelPanel(int channel)
    {
        switch (channel)
        {
            case 0:
                return SegChannel0Panel;
            case 1:
                return SegChannel1Panel;
            case 2:
                return SegChannel2Panel;
            case 3:
                return SegChannel3Panel;
            default:
                MorphoDebug.LogError("ERROR : tried to get unsupported seg channel value " + channel);
                return null;
        }
    }

    public Transform GetIntensityChannelPanel(int channel)
    {
        switch (channel)
        {
            case 0:
                return IntensityChannel0Panel;
            case 1:
                return IntensityChannel1Panel;
            case 2:
                return IntensityChannel2Panel;
            case 3:
                return IntensityChannel3Panel;
            default:
                MorphoDebug.LogError("ERROR : tried to get unsupported intensity channel value " + channel);
                return null;
        }
    }

    public void SetMenuVisible(GameObject item)
    {
        SegmentedPanel.SetActive(false);
        IntensityPanel.SetActive(false);
        PropertiesPanel.SetActive(false);
        AdvancedMeshMenu.SetActive(false);
        item.SetActive(true);

        if(SegmentedPanel.activeSelf && !_SegMenuActive)// change to seg mode
        {
            SetSegMode(true);
}
        if (IntensityPanel.activeSelf && _SegMenuActive)// change to seg mode
        {
            SetSegMode(false);
        }
    }

    public void SetDatasetTimes(int min, int max)
    {
        DatasetSegTimes.text = $"{min} - {max}";
        DatasetIntensityTimes.text = $"{min} - {max}";
    }

    public void AutoSetMinMaxTimes()
    {
        //"regular" case
        int min = Math.Min(GetSegMinTime(), GetRawMinTime());
        int max = Math.Max(GetSegMaxTime(), GetRawMaxTime());
        if (GetSegMinTime() == int.MaxValue && GetSegMaxTime() == int.MinValue) //case with no seg: do not consider seg
        {
            min = GetRawMinTime() == int.MaxValue ? 0 : GetRawMinTime();
            max = GetRawMaxTime() == int.MinValue ? 0 : GetRawMaxTime();
        }
        if (GetRawMinTime() == int.MaxValue && GetRawMaxTime() == int.MinValue) //case with no seg: do not consider seg
        {
            min = GetSegMinTime() == int.MaxValue ? 0 : GetSegMinTime();
            max = GetSegMaxTime() == int.MinValue ? 0 : GetSegMaxTime();
        }


        SetDatasetTimes(min, max);
    }

    public void SetSegMode(bool value)
    {
        _SegMenuActive = value;
        if (_SegMenuActive)
        {
            _CurrentChannel0Panel = SegChannel0Panel;
            _CurrentChannel1Panel = SegChannel1Panel;
            _CurrentChannel2Panel = SegChannel2Panel;
            _CurrentChannel3Panel = SegChannel3Panel;
            _CurrentImageOptions = SegmentedImageOptions;
            _CurrentImageName = SegImageName;
            _CurrentImageTime = SegImageTime;
            _CurrentImageChannel = SegImageChannel;
            _CurrentImageVoxelSize = SegImageVoxelSize;
            _CurrentImagelSize = SegImageSize;
            _CurrentImagePath = SegImagePath;
            _CurrentImageRemove = SegImageRemove;
            _CurrentScrollView = SegScrollView;
            _CurrentImageOriginalC = SegImageOriginalChannel;
            _CurrentImageOriginalT = SegImageOriginalTime;

            //before switch, set dict value to intensity image dict (to save changes)
            IntensityObjects3DImages = new Dictionary<int, Dictionary<int, Object3DImage>>(Objects3DImages);

            //then set dict as seg
            Objects3DImages = SegObjects3DImages;
        }
        else
        {
            _CurrentChannel0Panel = IntensityChannel0Panel;
            _CurrentChannel1Panel = IntensityChannel1Panel;
            _CurrentChannel2Panel = IntensityChannel2Panel;
            _CurrentChannel3Panel = IntensityChannel3Panel;
            _CurrentImageOptions = IntensityImageOptions;
            _CurrentImageName = IntensityImageName;
            _CurrentImageTime = IntensityImageTime;
            _CurrentImageChannel = IntensityImageChannel;
            _CurrentImageVoxelSize = IntensityImageVoxelSize;
            _CurrentImagelSize = IntensityImageSize;
            _CurrentImagePath = IntensityImagePath;
            _CurrentImageRemove = IntensityImageRemove;
            _CurrentScrollView = RawScrollView;
            _CurrentImageOriginalC = IntensityImageOriginalChannel;
            _CurrentImageOriginalT = IntensityImageOriginalTime;

            //before switch, set dict value to intensity image dict (to save changes)
            SegObjects3DImages = new Dictionary<int, Dictionary<int, Object3DImage>>(Objects3DImages);

            //then set dict as Intensity
            Objects3DImages = IntensityObjects3DImages;
        }
        ResetAllImgColors();
        HideAllOptionMenus();
    }

    public void SetMetadataMenuVisible(bool value)
    {
        MetadataPanel.SetActive(value);
    }

    public void SetMetadataLoadVisible(bool value)
    {
        MetadataAwaitPanel.SetActive(value);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="encoding">string describing the image encoding</param>
    /// <param name="vx">voxel size x</param>
    /// <param name="vy">voxel size y</param>
    /// <param name="vz">voxel size z</param>
    /// <param name="shapes">shapes (XYZCT) of the image IN ORDER</param>
    /// <param name="shape_order">values corresponding to shapes->read order</param>
    /// <param name="path">optional path of the image(s)</param>
    /// <param name="type">optional type of the image(s) : folder, 3D/4D/5D image</param>
    public void SetMetadata(string encoding, float vx, float vy, float vz, int[] shapes, int[] shape_order, string path=null, string type=null)//xyzct missing
    {
        if(path!=null)
            MetadataPath.text = path;
        if (type != null)
            MetadataInputType.text = type;
        MetadataEncoding.text = encoding;
        MetadataVoxelX.text = vx < 0 ? "" : vx.ToString();
        MetadataVoxelY.text = vy < 0 ? "" : vy.ToString();
        MetadataVoxelZ.text = vz < 0 ? "" : vz.ToString();

        Metadata0Value.text = shapes[0].ToString();
        Metadata1Value.text = shapes[1].ToString();
        Metadata2Value.text = shapes[2].ToString();
        Metadata3Value.text = shapes[3].ToString();
        Metadata4Value.text = shapes[4].ToString();

        _InitMetadataOrder = new int[shape_order.Length];
        for(int i = 0;i < shape_order.Length;i++)
            _InitMetadataOrder[i] = shape_order[i];


        for (int j = 0; j < _DimDropdowns.Length; j++)
        {
            _DimDropdowns[j].value = shape_order[j];
            _DimDropdowns[j].RefreshShownValue();
        }
        CheckProperMetadataDims();

        MetadataTimeOffset.text = "0";
    }

    public void SetMetadadataPath(string path)
    {
        MetadataPath.text = path;
    }

    public void SetMetadadataPathType(string type)
    {
        MetadataInputType.text = type;
    }

    public void CheckForNoDoublesOnDropDowns(Dropdown self)
    {
        int[] values = { 0, 1, 2, 3, 4 };
        int[] m_values = { Metadata0.value, Metadata1.value, Metadata2.value, Metadata3.value, Metadata4.value };
        if (values.Except(m_values).Count() > 0)
        {
            int empty_val = values.Except(m_values).First();
            int val = self.value;
            if (Metadata0 != self && Metadata0.value == val)
                Metadata0.SetValueWithoutNotify(empty_val);
            if (Metadata1 != self && Metadata1.value == val)
                Metadata1.SetValueWithoutNotify(empty_val);
            if (Metadata2 != self && Metadata2.value == val)
                Metadata2.SetValueWithoutNotify(empty_val);
            if (Metadata3 != self && Metadata3.value == val)
                Metadata3.SetValueWithoutNotify(empty_val);
            if (Metadata4 != self && Metadata4.value == val)
                Metadata4.SetValueWithoutNotify(empty_val);
        }
        CheckProperMetadataDims();

    }

    public void CheckProperMetadataDims()
    {
        //check if c < 4 or x,y,z == 1 (2d/incorrect image)
        bool button_interactable = true;
        if ((Metadata0.options[Metadata0.value].text == "C" && int.Parse(Metadata0Value.text) > 4) || 
            (Metadata0.options[Metadata0.value].text == "X" || Metadata0.options[Metadata0.value].text == "Y" || Metadata0.options[Metadata0.value].text == "Z") && int.Parse(Metadata0Value.text) == 1)
        {
            Metadata0.gameObject.GetComponent<Image>().color = _WarningRed;
            ConfirmMetadata.interactable = false;
            button_interactable = false;
        }
        else
        {
            Metadata0.gameObject.GetComponent<Image>().color = Color.white;
        }


        if (Metadata1.options[Metadata1.value].text == "C" && int.Parse(Metadata1Value.text) > 4|| 
            (Metadata1.options[Metadata1.value].text == "X" || Metadata1.options[Metadata1.value].text == "Y" || Metadata1.options[Metadata1.value].text == "Z") && int.Parse(Metadata1Value.text) == 1)
        {
            Metadata1.gameObject.GetComponent<Image>().color = _WarningRed;
            ConfirmMetadata.interactable = false;
            button_interactable = false;
        }
        else
        {
            Metadata1.gameObject.GetComponent<Image>().color = Color.white;
        }

        if (Metadata2.options[Metadata2.value].text == "C" && int.Parse(Metadata2Value.text) > 4 ||
            (Metadata2.options[Metadata2.value].text == "X" || Metadata2.options[Metadata2.value].text == "Y" || Metadata2.options[Metadata2.value].text == "Z") && int.Parse(Metadata2Value.text) == 1)
        {
            Metadata2.gameObject.GetComponent<Image>().color = _WarningRed;
            ConfirmMetadata.interactable = false;
            button_interactable = false;
        }
        else
        {
            Metadata2.gameObject.GetComponent<Image>().color = Color.white;
        }

        if (Metadata3.options[Metadata3.value].text == "C" && int.Parse(Metadata3Value.text) > 4 ||
            (Metadata3.options[Metadata3.value].text == "X" || Metadata3.options[Metadata3.value].text == "Y" || Metadata3.options[Metadata3.value].text == "Z") && int.Parse(Metadata3Value.text) == 1)
        {
            Metadata3.gameObject.GetComponent<Image>().color = _WarningRed;
            ConfirmMetadata.interactable = false;
            button_interactable = false;
        }
        else
        {
            Metadata3.gameObject.GetComponent<Image>().color = Color.white;
        }

        if (Metadata4.options[Metadata4.value].text == "C" && int.Parse(Metadata4Value.text) > 4 ||
            (Metadata4.options[Metadata4.value].text == "X" || Metadata4.options[Metadata4.value].text == "Y" || Metadata4.options[Metadata4.value].text == "Z") && int.Parse(Metadata4Value.text) == 1)
        {
            Metadata4.gameObject.GetComponent<Image>().color = _WarningRed;
            ConfirmMetadata.interactable = false;
            button_interactable = false;
        }
        else
        {
            Metadata4.gameObject.GetComponent<Image>().color = Color.white;
        }

        if (button_interactable)
        {
            ConfirmMetadata.interactable = true;
        }
    }
    

    private Object3DImage Add3DImageObject(int c, int t, int original_c, int original_t, string path)
    {
        Object3DImage img = null;
        if (c <= CHANNEL_MAX)
        {
            img = Instantiate(GenericImageObject, GetChannelPanel(c));
            img.transform.SetSiblingIndex(t);//put sibling index at max time
            img.SetChannel("c"+original_c);
            img.SetTime("t"+original_t);
            img.OriginalChannel = original_c;
            img.OriginalTime = original_t;
            string justfilename = Path.GetFileName(path);
            img.SetName(justfilename);

            float vx = float.Parse(MetadataVoxelX.text.Replace(",", "."), NumberStyles.Any, CultureInfo.InvariantCulture);
            float vy = float.Parse(MetadataVoxelY.text.Replace(",", "."), NumberStyles.Any, CultureInfo.InvariantCulture);
            float vz = float.Parse(MetadataVoxelZ.text.Replace(",", "."), NumberStyles.Any, CultureInfo.InvariantCulture);
            img.VoxelSize = new Vector3(vx, vy, vz);
            img.Name = justfilename;
            img.Path = path;
            img.Channel = c;
            img.Time = t;
            float x = GetMetadataDimension(Dimension.X);
            float y = GetMetadataDimension(Dimension.Y);
            float z = GetMetadataDimension(Dimension.Z);
            img.Dimensions = new Vector3(x, y, z);
            img.Encoding = GetEncoding();

            if(DatasetNameField.text == "")
            {
                DatasetNameField.text = justfilename.Split('.')[0];
            }
        }
        else
        {
            MorphoDebug.LogError("error : tried to place image3dobject at channel "+c);
        }
        return img;
    }

    private Object3DImage Add3DImageObject(Image3D item)
    {
        Object3DImage img = null;
        if (item.Channel <= CHANNEL_MAX)
        {
            img = Instantiate(GenericImageObject, GetChannelPanel(item.Channel));
            img.transform.SetSiblingIndex(item.Time);//put sibling index at max time
            img.SetChannel("c" + item.OriginalChannel);
            img.SetTime("t" + item.OriginalTime);
            img.OriginalChannel = item.OriginalChannel;
            img.OriginalTime = item.OriginalTime;
            string justfilename = Path.GetFileName(item.Path);
            img.SetName(justfilename);

            float vx = item.VoxelSize[0];
            float vy = item.VoxelSize[1];
            float vz = item.VoxelSize[2];
            img.VoxelSize = new Vector3(vx, vy, vz);
            img.Name = justfilename;
            img.Path = item.Path;
            img.Channel = item.Channel;
            img.Time = item.Time;
            float x = item.Size[0];
            float y = item.Size[1];
            float z = item.Size[2];
            img.Dimensions = new Vector3(x, y, z);
            img.Encoding = item.Encoding;

            if (DatasetNameField.text == "")
            {
                DatasetNameField.text = justfilename.Split('.')[0];
            }
        }
        else
        {
            MorphoDebug.LogError("error : tried to place image3dobject at channel " + item.Channel);
        }
        return img;
    }

    /// <summary>
    /// return array of dimension swaps for image encoding. Not swapping dimensions from imread will return {0,1,2,3,4}.
    /// </summary>
    /// <returns>int[] of size 5 containing dimension swap info</returns>
    private int[] GetEncoding()
    {
        Dictionary<int, int> corresp = new Dictionary<int, int>();
        for(int i=0; i < _InitMetadataOrder.Length; i++)// make dict that corresponds init dim order with 0,1,2,3,4
        {
            corresp.Add(_InitMetadataOrder[i], i);
        }
        return new int[] { corresp[Metadata0.value], corresp[Metadata1.value], corresp[Metadata2.value], corresp[Metadata3.value], corresp[Metadata4.value] };
    }

    private int GetObject3DImagesCount()
    {
        if (Objects3DImages.Count > 0)
        {
            int val = Objects3DImages.Keys.Max();
            foreach (var v in Objects3DImages.Keys.OrderByDescending(im => im))
            {
                if (Objects3DImages[v].Count != 0)
                    return val;
                val--;
            }
            return val;
        }
        return 0;
    }

    private bool IsObject3DImagesOccupied(int t)
    {
        if (Objects3DImages.ContainsKey(t))
            return (Objects3DImages[t].Count > 0);
        return false;
    }


    public void AddImageFromFile()
    {
        int t_offset = 0;
        if (MetadataTimeOffset.text != "0")
        {
            t_offset = int.Parse(MetadataTimeOffset.text, NumberStyles.None,CultureInfo.InvariantCulture);
            if (t_offset > 0)
            {
                FillGridWithEmptySpaces(t_offset);
            }
        }

        if(GetMetadataDimension(Dimension.C) == 1 && GetMetadataDimension(Dimension.T) == 1)//3D image
        {
            if (Objects3DImages.Count == 0)
            {
                Object3DImage img = Add3DImageObject(0, t_offset, 0, 0, MetadataPath.text);
                Objects3DImages.Add(t_offset, new Dictionary<int, Object3DImage>());
                Objects3DImages[t_offset].Add(0, img);
            }
            else
            {
                int t=0, c=0;
                if (Objects3DImages[GetObject3DImagesCount()].Count >= CHANNEL_MAX)//if channels at last time are filled: go to next time first channel
                {
                    t = GetObject3DImagesCount() + 1 + t_offset;
                    Object3DImage img = Add3DImageObject(0, t, 0, 0, MetadataPath.text);
                    if (!Objects3DImages.ContainsKey(t))
                        Objects3DImages.Add(t, new Dictionary<int, Object3DImage>());
                    Objects3DImages[t].Add(0, img);
                }
                else//otherwise just last time next channel
                {
                    t = GetObject3DImagesCount() + t_offset;
                    c = Objects3DImages[t].Count;
                    Object3DImage img = Add3DImageObject(c, t, 0, 0, MetadataPath.text);
                    Objects3DImages[t].Add(c, img);
                }
            }
        }
        else
        {
            if (GetMetadataDimension(Dimension.C) == 1 || GetMetadataDimension(Dimension.T) == 1){//4D image
                
                int start_t = GetObject3DImagesCount() + t_offset;
                if (IsObject3DImagesOccupied(start_t))
                    start_t += 1;
                if (GetMetadataDimension(Dimension.C) > 1 && GetMetadataDimension(Dimension.C) <= CHANNEL_MAX)//multichannel and has max 4 channels
                {
                    if (!Objects3DImages.ContainsKey(start_t))
                        Objects3DImages.Add(start_t, new Dictionary<int, Object3DImage>());// always start by adding a new time
                    for (int i=0;i< GetMetadataDimension(Dimension.C); i++)
                    {
                        int c = Objects3DImages[start_t].Count;
                        Object3DImage img = Add3DImageObject(c,start_t, i, 0, MetadataPath.text);
                        Objects3DImages[start_t].Add(c, img);
                    }
                }
                else// either 4D with time or channel that has > 4 channels
                {
                    for (int i = 0; i < Mathf.Max(GetMetadataDimension(Dimension.C), GetMetadataDimension(Dimension.T)); i++)
                    {
                        int t = start_t+i;
                        if (IsObject3DImagesOccupied(t))
                            t += 1;
                        if (!Objects3DImages.ContainsKey(t))
                            Objects3DImages.Add(t, new Dictionary<int, Object3DImage>());
                        Object3DImage img = Add3DImageObject(0, t, 0, i, MetadataPath.text);
                        Objects3DImages[t].Add(0, img);
                    }
                }
            }
            else if(GetMetadataDimension(Dimension.C) <= CHANNEL_MAX)//5D image
            {
                int start_t = GetObject3DImagesCount() + t_offset;
                if (IsObject3DImagesOccupied(start_t))
                    start_t += 1;
                for (int i = 0; i < GetMetadataDimension(Dimension.C); i++)//for each channel
                {
                    for (int j = 0; j < GetMetadataDimension(Dimension.T); j++)//for each time
                    {
                        int t = start_t+j;
                        if(!Objects3DImages.ContainsKey(t))
                            Objects3DImages.Add(t, new Dictionary<int, Object3DImage>());
                        Object3DImage img = Add3DImageObject(i, t, i, j, MetadataPath.text);
                        Objects3DImages[t].Add(i, img);
                    }
                }
            }
            else
            {
                Debug.LogError("Unsupported 5D image format. maximum of 4 channels authorized");
            }
        }
        AutoFillGrid();
        AutoSetMinMaxTimes();
        //then hide metadata menu
        SetMetadataMenuVisible(false);
    }



    public void AddImageFromFolder(List<string> images)
    {
        int t_offset = 0;
        if (MetadataTimeOffset.text != "0")
        {
            t_offset = int.Parse(MetadataTimeOffset.text, NumberStyles.None, CultureInfo.InvariantCulture);
            if (t_offset > 0)
            {
                FillGridWithEmptySpaces(t_offset);
            }
        }

        if (GetMetadataDimension(Dimension.C) == 1 && GetMetadataDimension(Dimension.T) == 1)//collection of 3D images
        {
            int start_t = GetObject3DImagesCount() + t_offset;
            if (IsObject3DImagesOccupied(start_t))
                start_t += 1;
            if (images.Count <= CHANNEL_MAX)//max 4 images : put in next available time and fill channels
            {
                if (!Objects3DImages.ContainsKey(start_t))
                    Objects3DImages.Add(start_t, new Dictionary<int, Object3DImage>());
                for(int i=0; i < images.Count; i++)
                {
                    int c = Objects3DImages[start_t].Count;
                    Object3DImage img = Add3DImageObject(c, start_t, 0, 0, images[i]);
                    Objects3DImages[start_t].Add(c, img);
                }
            }
            else//otherwise fill on next times
            {
                for (int i = 0; i < images.Count; i++)
                {
                    int t = start_t+i;
                    if (IsObject3DImagesOccupied(t))
                        t += 1;
                    if(!Objects3DImages.ContainsKey(t))
                        Objects3DImages.Add(t, new Dictionary<int, Object3DImage>());
                    Object3DImage img = Add3DImageObject(0, t, 0, 0, images[i]);
                    Objects3DImages[t].Add(0, img);
                }
            }
            
        }
        else
        {
            if (GetMetadataDimension(Dimension.C) == 1 || GetMetadataDimension(Dimension.T) == 1)//collection of 4D images
            {
                if (GetMetadataDimension(Dimension.C) > 1 && GetMetadataDimension(Dimension.C) <= CHANNEL_MAX)//multichannel and has max 4 channels
                {
                    int start_t = GetObject3DImagesCount() + t_offset;
                    for (int j = 0; j < images.Count; j++)
                    {
                        int t = start_t+j;
                        if (IsObject3DImagesOccupied(t))
                            t += 1;
                        if (!Objects3DImages.ContainsKey(t))
                            Objects3DImages.Add(t, new Dictionary<int, Object3DImage>());// always start by adding a new time
                        for (int i = 0; i < GetMetadataDimension(Dimension.C); i++)
                        {
                            int c = Objects3DImages[t].Count;
                            Object3DImage img = Add3DImageObject(c, t, i, 0, images[j]);
                            Objects3DImages[t].Add(c, img);
                        }
                    }
                    
                }
                else// collection of 4D temporal : works only if 4 images max (and fill channels)
                {
                    if(images.Count < CHANNEL_MAX)
                    {
                        int start_t = GetObject3DImagesCount() + t_offset;
                        for (int j = 0; j < images.Count; j++)
                        {
                            for (int i = 0; i < Mathf.Max(GetMetadataDimension(Dimension.C), GetMetadataDimension(Dimension.T)); i++)
                            {
                                int t = start_t+i;
                                if (IsObject3DImagesOccupied(t))
                                    t += 1;
                                if (!Objects3DImages.ContainsKey(t))
                                    Objects3DImages.Add(t, new Dictionary<int, Object3DImage>());
                                Object3DImage img = Add3DImageObject(j, t, 0, i, images[j]);
                                Objects3DImages[t].Add(j, img);
                            }
                        }
                    }
                    else
                    {
                        Debug.LogError("Unsupported 4D image format. maximum of 4 temporal images supported");
                    }                    
                }
            }
            else //collection of 5D images
            {
                Debug.LogError("Unsupported 5D image format. maximum of 4 channels authorized");
            }
        }
        AutoFillGrid();
        AutoSetMinMaxTimes();
        SetMetadataMenuVisible(false);
    }

    public void HideAllOptionMenus()
    {
        SegmentedImageOptions.SetActive(false);
        IntensityImageOptions.SetActive(false);
    }

    public void SetImageMenuOptions(string name, int time, int channel, Vector3 vs, Vector3 dims, string path, Object3DImage image, int original_time, int original_channel)
    {
        _CurrentImageOptions.SetActive(true);
        _CurrentImageName.text = name;
        _CurrentImageTime.text = time.ToString();
        _CurrentImageChannel.text = channel.ToString();
        _CurrentImageVoxelSize.text = $"{vs[0]}, {vs[1]}, {vs[2]}";
        _CurrentImagelSize.text = $"{dims[0]}, {dims[1]}, {dims[2]}";
        _CurrentImagePath.text = path;
        _CurrentImageOriginalC.text = original_time.ToString();
        _CurrentImageOriginalT.text = original_channel.ToString();

        _CurrentImageRemove.onClick.RemoveAllListeners();
        _CurrentImageRemove.onClick.AddListener(delegate { RemoveImageObject(image); });
        
    }

    private void RemoveImageObject(Object3DImage img)
    {
        int c = img.Channel;
        int t = img.Time;
        int position = img.transform.GetSiblingIndex();
        try
        {
            Objects3DImages[t].Remove(c);
            if (Objects3DImages[t].Count == 0)//clear time if gets empty. otherwise it will send an empty dictionary
                Objects3DImages.Remove(t);
        }
        catch(Exception e)
        {
            Debug.LogError("ERROR: could not remove objectimage at channel " + c + ", and time " + t);
            Debug.LogError(e.StackTrace);
        }
        //place empty cell in place of deleted image
        GameObject cell = Instantiate(EmptyImagePanel, GetChannelPanel(c));
        cell.transform.SetSiblingIndex(position);

        Destroy(img.gameObject);

        HideAllOptionMenus();

        foreach(var v in Objects3DImages.Keys.OrderBy(im => im))
        {
            FixHoles(v);
        }

        

        //AutoFillSegGrid();
    }

    /// <summary>
    /// clear currently displayed image grid, in UI and internal data
    /// </summary>
    public void ClearCurrentGrid(bool refill=true)
    {
        //clear all gameobjects in this grid
        for (int i = 0; i < CHANNEL_MAX; i++)
        {
            for (int j = 0; j < GetChannelPanel(i).childCount; j++)
            {
                Destroy(GetChannelPanel(i).GetChild(j).gameObject);
            }
        }
        //clear data
        Objects3DImages.Clear();
        if (_SegMenuActive)
            SegObjects3DImages.Clear();
        else
            IntensityObjects3DImages.Clear();


        if (refill)
        {
            //reset default
            for (int i = 0; i < CHANNEL_MAX; i++)
            {
                for (int j = 0; j < EMPTY_TIMES; j++)
                {
                    Instantiate(EmptyImagePanel, GetChannelPanel(i));
                }
            }
        }
        

        AutoSetMinMaxTimes();
        HideAllOptionMenus();
    }

    public void SetAdvancedMenuValues(bool smooth, float smooth_passband, int smooth_iter, bool quadric_clustering, int nb_divisions, bool decimation, float reduction, int auto_threshold)
    {
        Smoothing.isOn = smooth;
        SmoothingPassband.text = smooth_passband.ToString().Replace(',', '.');
        SmoothingIterations.text = smooth_iter.ToString();
        QuadricClustering.isOn = quadric_clustering;
        QCDivisions.text = nb_divisions.ToString();
        Decimation.isOn = decimation;
        DecimateReduction.text = reduction.ToString().Replace(',','.');
        DecimateThreshold.text = auto_threshold.ToString();
    }


    /// <summary>
    /// reset default values for dataset page
    /// </summary>
    public void ResetDatasetPage()
    {
        _OptionsMode = false;
        _DisabledSegEdit = false;

        RawScrollView.horizontalScrollbar.value = 0;
        SegScrollView.horizontalScrollbar.value = 0;
        
        SetSegMode(true);
        ClearCurrentGrid();
        SetSegMode(false);
        ClearCurrentGrid();

        DatasetNameField.text = "";
        SetAdvancedMenuValues(true, 0.01f, 25, true, 1, true, 0.8f, 30);//reset default values
        DownscaleRawXYSlider.value = 4;
        DownscaleRawZSlider.value = 4;
        DownscaleSegXYSlider.value = 4;
        DownscaleSegZSlider.value = 4;
        BackgroundField.text = "0";
        PropertiesXMLFile.text = "";

        SetSegMode(true);
        SetSegMode(true);
        SegToggle.isOn = true;
        SetMenuVisible(SegmentedPanel);
        AutoSetMinMaxTimes();

        //reset buttons/authorizations:
        EditButton.gameObject.SetActive(false);
        LaunchButton.gameObject.SetActive(true);
        
        ExportSubMenu.gameObject.SetActive(false);
        DatasetNameField.interactable = true;
        DisableSegPanel.SetActive(false);
        ClearSegButton.interactable = true;

        AddSegImageButton.interactable = true;
        PropertiesXMLFile.interactable = true;
        PropertiesXMLAddButton.interactable = true;
    }


    public IEnumerator SetDatasetPageFromLocalDataset(LocalDatasetItem item)
    {
        _OptionsMode = true;
        //start by clearing just in case
        SetSegMode(true);
        ClearCurrentGrid(false);
        SetSegMode(false);
        ClearCurrentGrid(false);

        yield return new WaitForEndOfFrame();

        InitFillGrids(item.MinTime + EMPTY_TIMES);

        DatasetNameField.text = item.Name;
        SetAdvancedMenuValues(item.Smoothing, item.SmoothPassband, item.SmoothIterations, item.QuadricClustering, item.QCDivisions,
            item.Decimation, item.DecimateReduction, item.AutoDecimateThreshold);
        DownscaleRawXYSlider.value = item.RawDownScale;
        DownscaleRawZSlider.value = item.ZRawDownScale;
        DownscaleSegXYSlider.value = item.DownScale;
        DownscaleSegZSlider.value = item.ZDownScale;
        BackgroundField.text = item.Background.ToString();
        PropertiesXMLFile.text = item.XMLFile;

        //set seg
        SetSegMode(true);
        SegObjects3DImages = new Dictionary<int, Dictionary<int, Object3DImage>>();
        Objects3DImages = new Dictionary<int, Dictionary<int, Object3DImage>>();
        foreach(var t in item.SegmentedData)
        {
            Objects3DImages.Add(t.Key, new Dictionary<int, Object3DImage>());
            foreach (var c in t.Value)
            {
                Image3D i = c.Value;
                Object3DImage img = Add3DImageObject(i);
                img.SetFromData(c.Value);
                Objects3DImages[t.Key].Add(c.Key, img);
            }
        }
        AutoFillGrid();

        yield return new WaitForEndOfFrame();

        //set raw
        SetSegMode(false);
        IntensityObjects3DImages = new Dictionary<int, Dictionary<int, Object3DImage>>();
        Objects3DImages = new Dictionary<int, Dictionary<int, Object3DImage>>();
        foreach (var t in item.IntensityData)
        {
            Objects3DImages.Add(t.Key, new Dictionary<int, Object3DImage>());
            foreach (var c in t.Value)
            {
                Image3D i = c.Value;
                Object3DImage img = Add3DImageObject(i);
                img.SetFromData(c.Value);
                Objects3DImages[t.Key].Add(c.Key, img);
            }
        }
        AutoFillGrid();
        //finally set mode to segmode

        yield return new WaitForEndOfFrame();

        SetSegMode(true);
        SegToggle.isOn = true;
        SetMenuVisible(SegmentedPanel);
        AutoSetMinMaxTimes();

        //set authorizations
        EditButton.gameObject.SetActive(true);
        LaunchButton.gameObject.SetActive(false);
        ExportSubMenu.gameObject.SetActive(true);
        DatasetNameField.interactable = false;
        PropertiesXMLFile.interactable = false;
        PropertiesXMLAddButton.interactable = false;

        if (CheckIfDatasetCurated(item.FullPath))
        {
            _DisabledSegEdit = true;
            DisableSegPanel.SetActive(true);
            ClearSegButton.interactable = false;
            AddSegImageButton.interactable = false;
        }
        else
        {
            _DisabledSegEdit = false;
            DisableSegPanel.SetActive(false);
            ClearSegButton.interactable = true;
            AddSegImageButton.interactable = true;
        }

    }

    /// <summary>
    /// automatically fills the segmented images grid with empty cases to have each channel equal
    /// </summary>
    public void AutoFillGrid()
    {
        //Debug.LogError("autofillgrid");
        int[] childcount = new int[CHANNEL_MAX];
        for (int i = 0; i < CHANNEL_MAX; i++)
            childcount[i] = GetChannelPanel(i).childCount;

        int max = Mathf.Max(childcount);
        //first check if last cell is an image. if it is, increase max by 1 so last cell is always empty
        for (int i = 0; i < CHANNEL_MAX; i++)
        {
            Object3DImage temp = null;
            if (GetChannelPanel(i).GetChild(childcount[i] - 1).TryGetComponent(out temp))
            {
                max += 1;
                break;
            }
        }

        for (int i = 0; i < CHANNEL_MAX; i++)
        {
            for(int j = 0; j < max - childcount[i]; j++)
            {
                Instantiate(EmptyImagePanel, GetChannelPanel(i));
            }
        }
        
    }

    public void AutoPlaceScrollBar()
    {
        _CurrentScrollView.horizontalScrollbar.value = 0.99f;
    }

    private int GetMetadataDimension(Dimension d)
    {
        for(int i = 0; i < _DimDropdowns.Length; i++)
        {
            if(_DimDropdowns[i].value == (int)d)
            {
                return int.Parse(_DimTexts[i].text);
            }
        }
        MorphoDebug.LogError("ERROR : could not find dimension " + d + " from metadata");
        return 0;
    }

    public void SyncDownsampleRawToSlider(bool z)
    {
        if (z)
            DownscaleRawZField.SetTextWithoutNotify(DownscaleRawZSlider.value.ToString());
        else
            DownscaleRawXYField.SetTextWithoutNotify(DownscaleRawXYSlider.value.ToString());
    }

    public void SyncDownsampleSegToSlider(bool z)
    {
        if (z)
            DownscaleSegZField.SetTextWithoutNotify(DownscaleSegZSlider.value.ToString());
        else
            DownscaleSegXYField.SetTextWithoutNotify(DownscaleSegXYSlider.value.ToString());
    }

    private bool CheckIfDatasetCurated(string path)
    {
        if (Directory.Exists(Path.Combine(path, "1")))
            return true;
        return false;
    }

    public void SetUploadName(string name)
    {
        UploadDatasetName.text = name;
    }

    public string GetUploadName()
    {
        return UploadDatasetName.text;
    }

    public void SetUploadMenuVisible(bool value)
    {
        UploadMenu.gameObject.SetActive(value);
    }

    public void SetExportMenuVisible(bool value)
    {
        ExportWindow.SetActive(value);
    }

    private enum Dimension
    {
        X=0,
        Y=1,
        Z=2,
        C=3,
        T=4
    }

    public bool EqualImageDict(Dictionary<int, Dictionary<int, Image3D>> d1, Dictionary<int, Dictionary<int, Image3D>> d2)
    {

        //check d1 to d2
        foreach(var t in d1)
        {
            if (!d2.ContainsKey(t.Key))
                return false;
            foreach(var c in t.Value)
            {
                if (!d2[t.Key].ContainsKey(c.Key))
                    return false;
                if (!d2[t.Key][c.Key].IsEqual(c.Value))
                    return false;
            }
        }
        //check other way
        foreach (var t in d2)
        {
            if (!d1.ContainsKey(t.Key))
                return false;
            foreach (var c in t.Value)
            {
                if (!d1[t.Key].ContainsKey(c.Key))
                    return false;
                if (!d1[t.Key][c.Key].IsEqual(c.Value))
                    return false;
            }
        }

        return true;
    }

}
