using MorphoNet.UI;
using Newtonsoft.Json;
using Shibuya24.Utility;
using SimpleFileBrowser;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace MorphoNet
{

    public enum DatasetOrderingType
    {
        Latest,
        Oldest,
        Alphabetical,
        ReverseAlphabetical
    }

    public class StandaloneMainMenuInteractions : MonoBehaviour
    {

        public static StandaloneMainMenuInteractions instance;

        #region consts

        public const string LOCALHOST = "http://localhost";
        public static string PREV_LOCAL_DATASET_PATH = "/local_datasets.json";
        public static string LOCAL_DATASET_PATH = "/local_datasets-2-1-17.json";
        public static int PYINSTALLER_PORT = 9874;
        public static int PYINSTALLER_PORT_RECEIVE = 9873;

        public static int DATASET_CHUNK_SIZE = 50;

#if UNITY_STANDALONE_OSX
        public static int NUMBER_INIT_TRIES = 120;
#elif UNITY_STANDALONE_LINUX
        public static int NUMBER_INIT_TRIES = 240;
#else
        public static int NUMBER_INIT_TRIES = 60;
#endif

        #endregion consts


        #region parameters

        public GameObject delete_panel;

        [SerializeField]
        public GameObject Console;
        [SerializeField]
        public Text ConsoleContents;
        [SerializeField]
        public Button ConsoleOpenButton;

        [SerializeField]
        private GameObject _StandaloneMenu;

        [SerializeField]
        private Text _AwaitInitText;
        [SerializeField]
        private GameObject _InitPanel;

        [Header("Plot menu parameters")]

        [SerializeField]
        private UISkin _PickerSkin;

        [SerializeField]
        private string morphoplot_adress = LOCALHOST;

        private bool _PickerInUse = false;

        private string _UploadDatasetName="";//need the base name of the dataset we upload, in case it is changed
        private string _UploadDatasetFullPath="";//need the base name of the dataset we upload, in case it is changed

        private bool logged = false;

        private bool _recompute_raw = false;
        private bool _recompute_seg = false;
        private bool _recompute_background = false;
        private bool _recompute_meshgen = false;
        private string _exportFullPath = "";

        [Header("Net menu parameters")]

        [SerializeField]
        public InputField DatasetSearchField;

        [SerializeField]
        public RawImage SearchIcon;

        [SerializeField]
        public Sprite SearchIconBase;

        [SerializeField]
        public Sprite SearchIconLoading;


        [SerializeField]
        private RectTransform _NetDatasetListTransform;

        [SerializeField]
        private RectTransform _NetDatasetListItemTransform;


        public DatasetParameterItem UISetExample;

        public GameObject NetStandaloneDatasetList;//UI_standalone_parent;

        public Toggle OrderDateLatest;
        public Toggle OrderDateOldest;
        public Toggle OrderAlphaAZ;
        public Toggle OrderAlphaZA;

        public DatasetOrderingType CurrentOrderingType = DatasetOrderingType.Latest;

        public GameObject LoadingImage;


        [Header("Alt menu parameters")]

        [SerializeField]
        private ToggleFilterElement PrefabToggleFilterElement;

        [SerializeField]
        private Button AddDatasetButton;

        [SerializeField]
        private GameObject AddDatasetPanel;

        [SerializeField]
        public GameObject SideSourceMenu;
        [SerializeField]
        public GameObject SideGroupsMenu;
        [SerializeField]
        public GameObject SideTagsMenu;
        [SerializeField]
        public GameObject SideSpeciesMenu;
        [SerializeField]
        public GameObject SideFavMenu;

        //contents of scrollview lists for easier access
        private Transform _GroupFilterListScrollTransform;
        private Transform _TagFilterListScrollTransform;
        private Transform _SpeciesFilterListScrollTransform;


        //lists for generated filter elements
        private Dictionary<int, ToggleFilterElement> _FilterGroupElements = new Dictionary<int, ToggleFilterElement>();
        private Dictionary<int, ToggleFilterElement> _FilterSpeciesElements = new Dictionary<int, ToggleFilterElement>();
        private Dictionary<int, ToggleFilterElement> _FilterTagElements = new Dictionary<int, ToggleFilterElement>();

        private List<ToggleFilterElement> _ActiveFilterSourceElements = new List<ToggleFilterElement>();
        private List<ToggleFilterElement> _ActiveFilterGroupElements = new List<ToggleFilterElement>();
        private List<ToggleFilterElement> _ActiveFilterTagElements = new List<ToggleFilterElement>();
        private List<ToggleFilterElement> _ActiveFilterSpeciesElements = new List<ToggleFilterElement>();
        private bool _ActiveFilterFav = false;

        public Toggle GroupFilterButton;
        public Toggle FavFilterButton;
        public Toggle TagFilterButton;
        public Toggle SourceFilterButton;
        public Toggle NCBIFilterButton;

        [Header("Sprites")]
        public Sprite MorphoNetSourceSprite;
        public Sprite MorphoPlotSourceSprite;
        public Sprite DistantPlotSourceSprite;


        private Coroutine SpeciesFilterCoroutine;

        private LocalDatasetItem _CurrentLocalDataset;

        [Header("Others")]
        public GameObject AwaitFunctionPanel;
        [SerializeField]
        private Text AwaitFunctionText;

        [SerializeField]
        public GameObject ConfirmPanel;
        [SerializeField]
        private Text _ConfirmPanelDeleteText;
        [SerializeField]
        private Text _ConfirmPanelUpdateText;

        [SerializeField]
        public Button ConfirmApplyButton;

        [SerializeField]
        private GameObject OptionsPanel;

        [SerializeField]
        private CanvasScaler MainCanvasScaler;

        [SerializeField]
        private Slider CanvasScaleSlider;

        [SerializeField]
        private Toggle _VerboseMaxToggle;
        
        [SerializeField]
        public GameObject _MenuNewVersion;
        
        [SerializeField]
        public TMP_Text _TextVersionLoading;

        private bool _HeadersFolderMode = false;
        private List<string> _FolderModeFiles = new List<string>();

        private (int,int) _screenResolution = (0, 0);
        private const float _TARGET_RES_HEIGHT = 1440;

        public static string[] IN_FILE_FORMATS = { ".czi", ".dv", ".h5", ".lif", ".mha", ".nii", ".nd2", ".tiff", ".tif", ".ometiff", ".inr",
                                                            ".czi.gz", ".dv.gz", ".h5.gz", ".lif.gz", ".mha.gz", ".nii.gz", ".nd2.gz", ".tiff.gz", ".tif.gz", ".ometiff.gz", ".inr.gz"};
        [Header("dev menu parameters")]
        [SerializeField]
        private GameObject _DevPanel;

        [SerializeField]
        private InputField _PortSend;

        [SerializeField]
        private InputField _PortReceive;


        #endregion parameters



        public void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
                Destroy(this.gameObject);
        }

#if UNITY_STANDALONE_OSX
        private void Update()
        {
            (int, int) newRes = (Screen.currentResolution.width, Screen.currentResolution.height);
            if (newRes != _screenResolution)
            {
                _screenResolution = newRes;
                MainCanvasScaler.scaleFactor = _screenResolution.Item2 / _TARGET_RES_HEIGHT;
            }
        }
#endif

        void Start()
        {
            
#if UNITY_STANDALONE_OSX
            _screenResolution = (Screen.currentResolution.width, Screen.currentResolution.height);
            MainCanvasScaler.scaleFactor = _screenResolution.Item2 / _TARGET_RES_HEIGHT;
#endif




#if UNITY_EDITOR
            StandaloneInitialized.instance.IsReady = true;
#endif
#if UNITY_WEBGL
            _StandaloneMenu.SetActive(false);

#else
            StartCoroutine(WaitForMorphoStandaloneInit(0));

            _GroupFilterListScrollTransform = SideGroupsMenu.transform.Find("ScrollView").Find("Viewport").Find("Content");
            if (_GroupFilterListScrollTransform == null)
               MorphoDebug.LogError("ERROR : group filter scrollview has not been found. It will cause issues.");

            _TagFilterListScrollTransform = SideTagsMenu.transform.Find("ScrollView").Find("Viewport").Find("Content");
            if (_GroupFilterListScrollTransform == null)
                MorphoDebug.LogError("ERROR : tag filter scrollview has not been found. It will cause issues.");

            _SpeciesFilterListScrollTransform = SideSpeciesMenu.transform.Find("ScrollView").Find("Viewport").Find("Content");
            if (_GroupFilterListScrollTransform == null)
                MorphoDebug.LogError("ERROR : species filter scrollview has not been found. It will cause issues.");

            InitializeGroupFilterMenu();
            InitializeTagFilterMenu();
            //InitializeSpeciesFilterMenu();

            //listeners for order buttons
            OrderAlphaAZ.onValueChanged.AddListener(delegate { ToggleAlphaAZOrder(); });
            OrderAlphaZA.onValueChanged.AddListener(delegate { ToggleAlphaZAOrder(); });
            OrderDateLatest.onValueChanged.AddListener(delegate { ToggleDateLatestOrder(); });
            OrderDateOldest.onValueChanged.AddListener(delegate { ToggleDateOlderOrder(); });

            OrderDateLatest.isOn = true;

            FileBrowser.Skin = _PickerSkin;

            ConsoleOpenButton.onClick.AddListener(() => { DebugConsoleManager.instance.ToggleConsole(); });
#endif


            AwaitFunctionText = AwaitFunctionPanel.transform.Find("Panel").Find("Text").GetComponent<Text>();
        }

        public void DisplayAwaitFunction(string text)
        {
            AwaitFunctionPanel.SetActive(true);
            AwaitFunctionText.text = text;
        }

        public void HideAwaitFunction()
        {
            AwaitFunctionPanel.SetActive(false);
        }


        public void UpdateStatePlot(bool state)
        {
            LoadParameters.instance.UpdateStatePlot(state);
            StartCoroutine(LoadParameters.instance.PlotLoading());
        }


        public void SetFilterRights(bool rights)//attention, might need to manage in detail what was the selected button to avoid odd behavior ?
        {
            if (rights)
            {
                GroupFilterButton.gameObject.SetActive(true);
                FavFilterButton.gameObject.SetActive(true);
            }
            else
            {
                GroupFilterButton.gameObject.SetActive(false);
                SideGroupsMenu.SetActive(false);
                FavFilterButton.gameObject.SetActive(false);
                SideFavMenu.SetActive(false);
            }
        }


        public void SendSetDatasetRequest()
        {
            LoadParameters.instance.morphoplot_adress = LOCALHOST;
            //LoadParameters.instance.SetPortReceive(_PortRecieveField.text);
            //LoadParameters.instance.SetPortSend(_PortSendField.text);

            StartCoroutine(PlotInit());
        }

        public IEnumerator PlotInit()
        {
            CreateDatasetMenuManager cm = CreateDatasetMenuManager.Instance;

            string persistent = Application.persistentDataPath;
#if UNITY_STANDALONE_WIN
            persistent = persistent.Replace("/", "\\");
#endif
            string prefix = Path.Combine(persistent, ".TEMP");
            string mainpath = cm.GetDatasetName();
            string date = DateTime.Now.ToString("MM-dd-yyyy-HH-mm-ss");

            string fullpath = Path.Combine(prefix, mainpath +"-"+date);
            fullpath = fullpath.Replace("{:", "").Replace("}", "");

            string jsonpath = Application.persistentDataPath + LOCAL_DATASET_PATH;

            //start by saving the local dataset. we'll delete the entry later if the request failed
            SaveLocalDataset(date, fullpath);

            int minTime = CreateDatasetMenuManager.Instance.GetMinTime();
            int maxTime = CreateDatasetMenuManager.Instance.GetMaxTime();
            LoadParameters.instance.max_time = maxTime;
            LoadParameters.instance.min_time = minTime;

            WWWForm form = new WWWForm();
            form.AddField("action", "set_dataset");
            form.AddField("time", 0);
            //lighter request : just sent fullpath + json path. all you need to find the dataset in saved json file
            form.AddField("objects", fullpath + ";" + jsonpath + ";" + (_VerboseMaxToggle.isOn ? "3" : "1"));


            UnityWebRequest www2 = UnityWebRequests.Post(morphoplot_adress + ":" + PYINSTALLER_PORT, "/ send.html", form);
            www2.SetRequestHeader("Access-Control-Allow-Origin", "*");
            www2.SetRequestHeader("Content-Security-Policy", "upgrade-insecure-requests");
            www2.timeout = 2;
            yield return www2.SendWebRequest();
            if (www2.result == UnityWebRequest.Result.ConnectionError)
            {
                MorphoDebug.Log("ERROR");
            }
            else
            {
                MorphoDebug.Log("standalone plot successful init request!");

                

                //LoadParameters.instance.UpdateStatePlot(true);
                LoadParameters.instance.start_plot = true;
                LoadParameters.instance.use_plot = true;
                StartCoroutine(LoadParameters.instance.PlotLoading(mainpath));

            }

            yield return new WaitForSeconds(4.0f); //WAS 5 SECONDS
        }

        public void SendPlotExport()
        {
            FileBrowser.SetFilters(false, new FileBrowser.Filter("Images", IN_FILE_FORMATS));
            FileBrowser.SetDefaultFilter(".tif");
            StartCoroutine(WaitForDirectoryPicker());

            
        }

        public IEnumerator RequestHeaders(string imagepath)
        {
            if(imagepath!=null && imagepath != "")
            {
                //error on request if this func is run later ?
                StartCoroutine(ReceivePythonCommandFromPyinstaller());
                CreateDatasetMenuManager.Instance.SetMetadataLoadVisible(true);
                CreateDatasetMenuManager.Instance.SetMetadataMenuVisible(true);

                WWWForm form = new WWWForm();
                form.AddField("action", "parse_headers");
                form.AddField("time", 0);
                form.AddField("objects", imagepath);

                UnityWebRequest www2 = UnityWebRequests.Post(morphoplot_adress + ":" + PYINSTALLER_PORT, "/ send.html", form);
                www2.SetRequestHeader("Access-Control-Allow-Origin", "*");
                www2.SetRequestHeader("Content-Security-Policy", "upgrade-insecure-requests");
                www2.timeout = 120;

                yield return www2.SendWebRequest();
                if (www2.result == UnityWebRequest.Result.ConnectionError)
                {
                    MorphoDebug.Log("ERROR");
                    CreateDatasetMenuManager.Instance.SetMetadataLoadVisible(false);
                    CreateDatasetMenuManager.Instance.SetMetadataMenuVisible(false);
                }
                else
                {
                    //StartCoroutine(ReceivePythonCommandFromPyinstaller());
                    CreateDatasetMenuManager.Instance.SetMetadataLoadVisible(false);
                }
            }
            else
            {
                MorphoDebug.Log("ERROR, cannot parse nothing");
                CreateDatasetMenuManager.Instance.SetMetadataLoadVisible(false);
                CreateDatasetMenuManager.Instance.SetMetadataMenuVisible(false);
            }
            
        }

        public void SetExportFullPath(string fullpath)
        {
            _exportFullPath = fullpath;
        }


        public IEnumerator PlotExport(string exportpath)
        {
            if (_CurrentLocalDataset != null)
            {
                string jsonpath = Application.persistentDataPath + LOCAL_DATASET_PATH;
                StartCoroutine(ReceivePythonCommandFromPyinstaller());
                DisplayAwaitFunction("Exporting dataset...");
                string exportformat = CreateDatasetMenuManager.Instance.GetExportImageFormat();
                if (CreateDatasetMenuManager.Instance.GetExportCompressed())
                    exportformat += ".gz";
                int propformat = CreateDatasetMenuManager.Instance.GetExportPropertyFormat();
                int sk_propformat = CreateDatasetMenuManager.Instance.GetSKExportPropertyFormat();

                WWWForm form = new WWWForm();
                form.AddField("action", "export_dataset");
                form.AddField("time", 0);
                form.AddField("objects", _exportFullPath + ";" + jsonpath + ";" + (_VerboseMaxToggle.isOn ? "3" : "1") + ";" + exportpath + ";" + exportformat + ";" + propformat + ";" + sk_propformat);


                UnityWebRequest www2 = UnityWebRequests.Post(morphoplot_adress + ":" + PYINSTALLER_PORT, "/ send.html", form);
                www2.SetRequestHeader("Access-Control-Allow-Origin", "*");
                www2.SetRequestHeader("Content-Security-Policy", "upgrade-insecure-requests");
                www2.timeout = 2;
                yield return www2.SendWebRequest();
                if (www2.result == UnityWebRequest.Result.ConnectionError)
                {
                    MorphoDebug.Log("ERROR");
                }
                else
                {
                    MorphoDebug.Log("standalone export dataset successful request!");
                    AddDatasetPanel.SetActive(false);
                }
            }
            else
            {
                MorphoDebug.LogError("Error, could not find dataset to export...");
            }

           
        }
        public IEnumerator PlotRecomputeData(LocalDatasetItem item)
        {
            if (item != null)
            {
                string jsonpath = Application.persistentDataPath + LOCAL_DATASET_PATH;
                StartCoroutine(ReceivePythonCommandFromPyinstaller());
                DisplayAwaitFunction("Recompute dataset...");
                WWWForm form = new WWWForm();
                form.AddField("action", "recompute_data");
                form.AddField("time", 0);
                form.AddField("objects", item.FullPath + ";" + jsonpath + ";" + (_VerboseMaxToggle.isOn ? "3" : "1") + ";" + _recompute_background + ";"
                    + _recompute_meshgen + ";" + _recompute_raw + ";" + _recompute_seg);

                UnityWebRequest www2 = UnityWebRequests.Post(morphoplot_adress + ":" + PYINSTALLER_PORT, "/ send.html", form);
                www2.SetRequestHeader("Access-Control-Allow-Origin", "*");
                www2.SetRequestHeader("Content-Security-Policy", "upgrade-insecure-requests");
                www2.timeout = 2;
                yield return www2.SendWebRequest();
                if (www2.result == UnityWebRequest.Result.ConnectionError)
                {
                    MorphoDebug.Log("ERROR");
                }
            }
            else
            {
                MorphoDebug.LogError("Error, could not find dataset to refresh...");
            }

           
        }


        public IEnumerator PlotRestart()
        {
            WWWForm form = new WWWForm();
            form.AddField("action", "restart");
            form.AddField("time", 0);


            UnityWebRequest www2 = UnityWebRequests.Post(morphoplot_adress + ":" + PYINSTALLER_PORT, "/ send.html", form);
            www2.SetRequestHeader("Access-Control-Allow-Origin", "*");
            www2.SetRequestHeader("Content-Security-Policy", "upgrade-insecure-requests");
            www2.timeout = 2;
            yield return www2.SendWebRequest();
            if (www2.result == UnityWebRequest.Result.ConnectionError)
            {
                MorphoDebug.Log("ERROR : wanted to restart plot by querying local server, but could not");
            }
            else
            {
                MorphoDebug.Log("Managed to restart plot using local server");
                //LoadParameters.instance.UpdateStatePlot(true);
                /*LoadParameters.instance.start_plot = true;
                LoadParameters.instance.use_plot = true;
                StartCoroutine(LoadParameters.instance.PlotLoading());*/

            }

            yield return new WaitForSeconds(4.0f); //WAS 5 SECONDS
        }

        public IEnumerator SendUpdateLocalDatasets()
        {
            
            string jsonpath = Application.persistentDataPath + PREV_LOCAL_DATASET_PATH;
            WWWForm form = new WWWForm();
            form.AddField("action", "update_local_sets");
            form.AddField("objects", jsonpath);


            UnityWebRequest www2 = UnityWebRequests.Post(morphoplot_adress + ":" + PYINSTALLER_PORT, "/ send.html", form);
            www2.SetRequestHeader("Access-Control-Allow-Origin", "*");
            www2.SetRequestHeader("Content-Security-Policy", "upgrade-insecure-requests");
            www2.timeout = 2;
            yield return www2.SendWebRequest();
            if (www2.result == UnityWebRequest.Result.ConnectionError)
            {
                MorphoDebug.Log("warning : wanted to reset local datasets json file, but could not send request");
                
            }
            else
            {
                MorphoDebug.Log("sent request to update local sets json file");

            }
            StartCoroutine(ReceivePythonCommandFromPyinstaller());
        }

        public IEnumerator RemoveLocalTempData(string path)
        {
            yield return StartCoroutine(ReceivePythonCommandFromPyinstaller());
            WWWForm form = new WWWForm();
            
            form.AddField("action", "remove_temp_data");
            form.AddField("time", 0);
            form.AddField("objects", path);

            UnityWebRequest www2 = UnityWebRequests.Post(morphoplot_adress + ":" + PYINSTALLER_PORT, "/ send.html", form);
            www2.SetRequestHeader("Access-Control-Allow-Origin", "*");
            www2.SetRequestHeader("Content-Security-Policy", "upgrade-insecure-requests");
            //www2.timeout = 2;
            yield return www2.SendWebRequest();
            
        }

        public bool ignore_next_start = false;
        public void SetPlotDeleting(bool status)
        {
            if (status)
            {
                DisplayAwaitFunction("Delete dataset...");
            }
            else
            {
                HideAwaitFunction();
            }

            LoadParameters.instance.is_plot_deleting = status;
        }

        private bool _alreadyClickedFlag = false;
        public void LoadLocalDataset(LocalDatasetItem item, int minTime, int maxTime)
        {
            if (!_alreadyClickedFlag) // Prevent user double click to break loading
            {
                _alreadyClickedFlag = true; // Prevent user double click to break loading
                LoadParameters.instance.morphoplot_adress = LOCALHOST;
                LoadParameters.instance.SetPortReceive(item.PortRecieve.ToString());
                LoadParameters.instance.SetPortSend(item.PortSend.ToString());

                StartCoroutine(PlotInitWithLocalSet(item, minTime, maxTime));
            }
        }

        public IEnumerator PlotInitWithLocalSet(LocalDatasetItem item, int minTime, int maxTime)
        {
            string jsonpath = Application.persistentDataPath + LOCAL_DATASET_PATH;
            WWWForm form = new WWWForm();
            form.AddField("action", "set_dataset");
            form.AddField("time", 0);
            form.AddField("objects", item.FullPath + ";" + jsonpath + ";" + (_VerboseMaxToggle.isOn ? "3" : "1") + ";" + minTime + ";" + maxTime);

            LoadParameters.instance.max_time = maxTime;
            LoadParameters.instance.min_time = minTime;

            UnityWebRequest www2 = UnityWebRequests.Post(morphoplot_adress + ":" + StandaloneMainMenuInteractions.PYINSTALLER_PORT, "/ send.html", form);
            www2.SetRequestHeader("Access-Control-Allow-Origin", "*");
            www2.SetRequestHeader("Content-Security-Policy", "upgrade-insecure-requests");
            www2.timeout = 2;
            yield return www2.SendWebRequest();
            if (www2.result == UnityWebRequest.Result.ConnectionError)
            {
                MorphoDebug.Log("ERROR");
                _alreadyClickedFlag = false; // Flag is reset if we fail to load this dataset 
            }
            else
            {
                MorphoDebug.Log("standalone plot successful init request!");

                //LoadParameters.instance.UpdateStatePlot(true);
                LoadParameters.instance.start_plot = true;
                LoadParameters.instance.use_plot = true;
                StartCoroutine(LoadParameters.instance.PlotLoading(item.Name));

            }

            yield return new WaitForSeconds(4.0f); //WAS 5 SECONDS
        }

        /// <summary>
        /// Upload a dataset to MorphoNet from information in upload form
        /// </summary>
        /// <returns></returns>
        public IEnumerator UploadDatasetToMorphoNet()
        {
            Debug.Log("sending upload request..."+_UploadDatasetName);
            string jsonpath = Application.persistentDataPath + LOCAL_DATASET_PATH;
            WWWForm form = new WWWForm();
            form.AddField("action", "upload_dataset");
            form.AddField("time", 0);
            form.AddField("objects", _UploadDatasetFullPath + ";" + jsonpath + ";" + (_VerboseMaxToggle.isOn ? "3" : "1") + ";" + _UploadDatasetName);
            /*form.AddField("objects", _UploadDatasetBaseName + ";" + _UploadDatasetFullPath + ";" + _UploadNameField.text + ";" + _UploadSegmentedField.text + ";" + _UploadRawField.text + ";" + 
                (_UploadFactorField.text == "None" ? "1" : _UploadFactorField.text) + ";" +_UploadTimeBeginField.text + ";" + _UploadTimeEndField.text + ";" + _UploadBackGroundField.text + ";" + 
                (_ZDownScale.text == "None" ? "1" : _ZDownScale.text) + ";" + _VoxelSizeOverride.isOn + ";" + _VoxelSizeXField.text + ";" + _VoxelSizeYField.text + ";" + _VoxelSizeZField.text + ";" +
                _Smoothing.isOn + ";" + _SmoothingPassband.text + ";" + _SmoothingIterations.text + ";" + _QuadricClustering.isOn + ";" + _QCDivisions.text + ";" + _Decimation.isOn + ";" +
                _DecimateReduction.text + ";" + _DecimateThreshold.text + ";"+ (_VerboseMaxToggle.isOn ? "3" : "1") + ";" + _DownScaleRawSlider.value + ";" + _ZDownScaleRawSlider.value);*/

            UnityWebRequest www2 = UnityWebRequests.Post(morphoplot_adress + ":" + PYINSTALLER_PORT, "/ send.html", form);
            www2.SetRequestHeader("Access-Control-Allow-Origin", "*");
            www2.SetRequestHeader("Content-Security-Policy", "upgrade-insecure-requests");
            //www2.timeout = 300;
            yield return www2.SendWebRequest();
            if (www2.result == UnityWebRequest.Result.ConnectionError)
            {
                MorphoDebug.Log("ERROR during upload");
            }
            else
            {
                MorphoDebug.Log("standalone plot successful upload request!");
                
            }

            yield return new WaitForSeconds(4.0f); //WAS 5 SECONDS
        }

        public void LaunchDataSetUpload()
        {
            StartCoroutine(LoginAndUpload());
        }

        public IEnumerator LoginAndUpload()
        {
            _UploadDatasetName = CreateDatasetMenuManager.Instance.GetUploadName();
            yield return StartCoroutine(SendLoginToPyinstaller());
            DisplayAwaitFunction("Please wait during the upload...");
            yield return new WaitForSeconds(2);
            StartCoroutine(ReceivePythonCommandFromPyinstaller());
            StartCoroutine(UploadDatasetToMorphoNet());
        }

        public void SetUploadDatasetFullPath(string path)
        {
            _UploadDatasetFullPath = path;
        }

        public void FilePickerForImage()
        {
            if (!_PickerInUse)
            {
                _PickerInUse = true;
                FileBrowser.SetFilters(false, new FileBrowser.Filter("Images", IN_FILE_FORMATS));
                FileBrowser.SetDefaultFilter(".tif");
                StartCoroutine(WaitForFilePicker());
            }
        }

        public void FilePickerForFieldXML(InputField f)
        {
            if (!_PickerInUse)
            {
                _PickerInUse = true;
                FileBrowser.SetFilters(true, new FileBrowser.Filter("Properties file", ".xml", ".txt"));
                FileBrowser.SetDefaultFilter(".xml");
                StartCoroutine(WaitForFilePickerFileMode(f));
            }
        }

        public void FilePickerForFieldZip(InputField f)
        {
            if (!_PickerInUse)
            {
                _PickerInUse = true;
                FileBrowser.SetFilters(true, new FileBrowser.Filter("MorphoNet dataset archive", ".zip"));
                FileBrowser.SetDefaultFilter(".zip");
                StartCoroutine(WaitForFilePickerFileMode(f));
            }
        }

        public IEnumerator WaitForFilePicker()
        {
            yield return FileBrowser.WaitForLoadDialog(FileBrowser.PickMode.FilesAndFolders, false, null, null, "Load image(s)", "Select");

            if (FileBrowser.Success)
            {

                //if (File.GetAttributes(FileBrowser.Result[0]) == FileAttributes.Directory)//if directory
                if (File.GetAttributes(FileBrowser.Result[0]).HasFlag(FileAttributes.Directory))//if directory
                {
                    /*var files = Directory.EnumerateFiles(FileBrowser.Result[0], "*.*", SearchOption.TopDirectoryOnly)
                        .Where(s => s.EndsWith(".tiff") || s.EndsWith(".tif") || s.EndsWith(".mha") || s.EndsWith(".mha.gz") || s.EndsWith(".nii") || s.EndsWith(".tiff.gz") || s.EndsWith(".tif.gz") ||
                        s.EndsWith(".nii.gz") || s.EndsWith(".inr") || s.EndsWith(".inr.gz"));*/

                    var allFiles = Directory.EnumerateFiles(FileBrowser.Result[0], "*.*", SearchOption.TopDirectoryOnly);
                    List<string> files = new List<string>();
                    foreach(var filter in IN_FILE_FORMATS)
                    {
                        foreach(var f in allFiles.Where(s => s.ToLower().EndsWith(filter)))
                        {
                            if (!files.Contains(f))// no doubles
                                files.Add(f);
                        }
                    }

                    CreateDatasetMenuManager.Instance.SetMetadadataPathType("Folder ("+ files.Count()+ " files)");
                    _HeadersFolderMode = true;
                    _FolderModeFiles = new List<string>(files);
                    //get first image in folder with similar name then get headers 4 metadata verification
                    CreateDatasetMenuManager.Instance.SetMetadadataPath(FileBrowser.Result[0]);
                    StartCoroutine(RequestHeaders(files.FirstOrDefault()));
                }
                else if (File.Exists(FileBrowser.Result[0]))//if simple file
                {
                    //change fill input field with checking for metadata
                    CreateDatasetMenuManager.Instance.SetMetadadataPath(FileBrowser.Result[0]);
                    StartCoroutine(RequestHeaders(FileBrowser.Result[0]));

                }
                else
                {
                    MorphoDebug.LogWarning("WARNING : incorrect path input : "+ (int)File.GetAttributes(FileBrowser.Result[0])+", "+FileBrowser.Result[0]);
                }


            }
            _PickerInUse = false;
        }

        public IEnumerator WaitForFilePickerFileMode(InputField f)
        {
            yield return FileBrowser.WaitForLoadDialog(FileBrowser.PickMode.Files, false, null, null, "Load file", "Select");

            if (FileBrowser.Success)
            {
                if (File.Exists(FileBrowser.Result[0]))//if simple file
                {
                    MorphoDebug.Log("single file picked");
                    f.text = (FileBrowser.Result[0]);
                }
                else
                {
                    MorphoDebug.LogWarning("WARNING : incorrect path input");
                }


            }
            _PickerInUse = false;
        }

        public IEnumerator WaitForDirectoryPicker()
        {
            yield return FileBrowser.WaitForLoadDialog(FileBrowser.PickMode.Folders, false, null, null, "Select export destination", "Select");

            if (FileBrowser.Success)
            {

                if (Directory.Exists(FileBrowser.Result[0]))
                {
                    CreateDatasetMenuManager.Instance.SetExportMenuVisible(false);
                    StartCoroutine(PlotExport(FileBrowser.Result[0]));
                }
                else
                {
                    MorphoDebug.LogError("ERROR: export directory does not exist");
                }

                
            }
        }

        //finds the best file to base parsing from list
        public string FindBestFileForParsing(List<string> files)
        {
            Dictionary<string, int> prefixes = new Dictionary<string, int>();
            foreach (string file in files)
            {
                string pref = ExtractPrefixFromStringWithDigits(file);
                if (!prefixes.ContainsKey(pref))
                    prefixes[pref] = 1;
                else
                    prefixes[pref] = prefixes[pref] + 1;
            }

            string best = prefixes.Aggregate((x, y) => x.Value > y.Value ? x : y).Key;
            files.Sort();
            foreach (string f in files)
            {
                if (f.Substring(0, best.Length) == best)
                    return f;
            }
            return files[0];

        }

        public string ExtractPrefixFromStringWithDigits(string s)
        {
            int last_digits_id = -1;
            int index = 0;
            bool digits_found = false;
            foreach (char c in s)
            {
                if (Char.IsDigit(c) && !digits_found)//first digit of a new "region"
                {
                    digits_found = true;
                    last_digits_id = index;
                }
                else if (!Char.IsDigit(c) && digits_found)//end of digit region
                {
                    digits_found = false;
                }
                index++;
            }
            if (last_digits_id != -1)
                return s.Substring(0, last_digits_id);
            else
                return s;
        }




        public void SetAddDatasetPanelActive(bool v)
        {
            AddDatasetPanel.SetActive(v);
            //DisplayLocalPlot();
        }

        public void DisplaySideMenuFilter(GameObject menu)
        {
            SideFavMenu.SetActive(false);
            SideGroupsMenu.SetActive(false);
            SideSourceMenu.SetActive(false);
            SideSpeciesMenu.SetActive(false);
            SideTagsMenu.SetActive(false);

            menu.SetActive(true);
        }

        public void DisplaySideSourceMenu()
        {
            DisplaySideMenuFilter(SideSourceMenu);
            SourceFilterButton.isOn = true;
        }

        //fill filter menu with the filtering options!

        public void InitializeGroupFilterMenu()
        {
            ClearGroupFilterElements();
            StartCoroutine(LoadParameters.instance.LoadFiltersForGroupMenu());
        }

        public void GenerateGroupFilterElement(string filterName, int id)
        {
            ToggleFilterElement item = Instantiate(PrefabToggleFilterElement, _GroupFilterListScrollTransform);
            item.SetLabel(filterName);
            item.Id = id;
            item.Type = FilterType.Group;
            _FilterGroupElements.Add(id, item);
            //add listener for filtering!
            item.Toggle.onValueChanged.AddListener(delegate { ToggleFilter(item); });
        }

        public void ClearGroupFilterElements()
        {
            foreach (var t in _FilterGroupElements)
            {
                if (t.Value != null)
                    Destroy(t.Value.gameObject);
            }
            _FilterGroupElements.Clear();
        }

        public void InitializeTagFilterMenu()
        {
            ClearTagFilterElements();
            StartCoroutine(LoadParameters.instance.LoadFiltersForTagMenu());
        }

        public void GenerateTagFilterElement(string filterName, int id)
        {
            ToggleFilterElement item = Instantiate(PrefabToggleFilterElement, _TagFilterListScrollTransform);
            item.SetLabel(filterName);
            item.Id = id;
            item.Type = FilterType.Tag;
            _FilterTagElements.Add(id, item);
            item.Toggle.onValueChanged.AddListener(delegate { ToggleFilter(item); });
        }

        public void InitializeSpeciesFilterMenu()
        {
            ClearSpeciesFilterElements();
            if (SpeciesFilterCoroutine != null)
                StopCoroutine(SpeciesFilterCoroutine);
            SpeciesFilterCoroutine = StartCoroutine(LoadParameters.instance.LoadFiltersForSpeciesMenu());
        }

        public void ClearTagFilterElements()
        {
            foreach (var t in _FilterTagElements)
            {
                if (t.Value != null)
                    Destroy(t.Value.gameObject);
            }
            _FilterTagElements.Clear();
        }

        public void GenerateSpeciesFilterElement(string filterName, int id)
        {
            ToggleFilterElement item = Instantiate(PrefabToggleFilterElement, _SpeciesFilterListScrollTransform);
            item.SetLabel(filterName);
            item.Id = id;
            item.Type = FilterType.Species;
            _FilterSpeciesElements.Add(id, item);
            item.Toggle.onValueChanged.AddListener(delegate { ToggleFilter(item); });
        }

        public void ClearSpeciesFilterElements()
        {
            foreach (var t in _FilterSpeciesElements)
            {
                if (t.Value != null)
                    Destroy(t.Value.gameObject);
            }
            _FilterSpeciesElements.Clear();
        }

        public void ApplyCurrentOrderingMethod()
        {
            switch (CurrentOrderingType)
            {
                case DatasetOrderingType.Latest:
                    ToggleDateLatestOrder();
                    break;
                case DatasetOrderingType.Oldest:
                    ToggleDateOlderOrder();
                    break;
                case DatasetOrderingType.Alphabetical:
                    ToggleAlphaAZOrder();
                    break;
                case DatasetOrderingType.ReverseAlphabetical:
                    ToggleAlphaZAOrder();
                    break;
            }

        }


        public void ToggleDateLatestOrder()
        {
            if (OrderDateLatest.isOn)
            {
                CurrentOrderingType = DatasetOrderingType.Latest;
                LoadParameters.instance.OrderByLatest();
            }
            else
            {
                CurrentOrderingType = DatasetOrderingType.Latest;
                LoadParameters.instance.ResetDefaultOrder();
            }

        }

        public void ToggleDateOlderOrder()
        {
            if (OrderDateOldest.isOn)
            {
                CurrentOrderingType = DatasetOrderingType.Oldest;
                LoadParameters.instance.OrderByOldest();
            }
            else
            {
                CurrentOrderingType = DatasetOrderingType.Latest;
                LoadParameters.instance.ResetDefaultOrder();
            }
        }

        public void ToggleAlphaAZOrder()
        {
            if (OrderAlphaAZ.isOn)
            {
                CurrentOrderingType = DatasetOrderingType.Alphabetical;
                LoadParameters.instance.OrderByAlphaAZ();
            }
            else
            {
                CurrentOrderingType = DatasetOrderingType.Latest;
                LoadParameters.instance.ResetDefaultOrder();
            }
        }

        public void ToggleAlphaZAOrder()
        {
            if (OrderAlphaZA.isOn)
            {
                CurrentOrderingType = DatasetOrderingType.ReverseAlphabetical;
                LoadParameters.instance.OrderByAlphaZA();
            }
            else
            {
                CurrentOrderingType = DatasetOrderingType.Latest;
                LoadParameters.instance.ResetDefaultOrder();
            }
        }

        public void ToggleFilter(ToggleFilterElement t)
        {
            if (t.Toggle.isOn)
            {
                AddFilter(t);
            }
            else
            {
                RemoveFilter(t);
            }
            ApplyAllFilters();
        }

        //add/remove a filter to the list
        public void AddFilter(ToggleFilterElement elem)
        {
            switch (elem.Type)
            {
                case FilterType.Source:
                    if (!_ActiveFilterSourceElements.Contains(elem))
                        _ActiveFilterSourceElements.Add(elem);
                    break;
                case FilterType.Group:
                    if (!_ActiveFilterGroupElements.Contains(elem))
                        _ActiveFilterGroupElements.Add(elem);
                    break;
                case FilterType.Tag:
                    if (!_ActiveFilterTagElements.Contains(elem))
                        _ActiveFilterTagElements.Add(elem);
                    break;

                case FilterType.Species:
                    if (!_ActiveFilterSpeciesElements.Contains(elem))
                        _ActiveFilterSpeciesElements.Add(elem);
                    break;
                case FilterType.Favorite:
                    _ActiveFilterFav = true;
                    break;
            }

        }

        public void RemoveFilter(ToggleFilterElement elem)
        {
            switch (elem.Type)
            {
                case FilterType.Source:
                    if (_ActiveFilterSourceElements.Contains(elem))
                    {
                        _ActiveFilterSourceElements.Remove(elem);
                    }
                    break;
                case FilterType.Group:
                    if (_ActiveFilterGroupElements.Contains(elem))
                    {
                        _ActiveFilterGroupElements.Remove(elem);
                    }
                    break;
                case FilterType.Tag:
                    if (_ActiveFilterTagElements.Contains(elem))
                    {
                        _ActiveFilterTagElements.Remove(elem);
                    }
                    break;

                case FilterType.Species:
                    if (_ActiveFilterSpeciesElements.Contains(elem))
                    {
                        _ActiveFilterSpeciesElements.Remove(elem);
                    }
                    break;
                case FilterType.Favorite:
                    _ActiveFilterFav = false;
                    break;
            }
        }

        public void ClearAllFilters()
        {
            _ActiveFilterSourceElements.Clear();
            _ActiveFilterGroupElements.Clear();
            _ActiveFilterTagElements.Clear();
            _ActiveFilterSpeciesElements.Clear();
            //LoadParameters.instance.FilteredSetsDisplay = new List<DatasetParameterItem>(LoadParameters.instance.SetsDisplay);
            foreach (DatasetParameterItem p in LoadParameters.instance.SetsDisplay.Values)
            {
                p.SetActive(true);
            }
        }

        public void ApplyAllFilters()
        {
            StartCoroutine(LoadParameters.instance.RoutineApplyAllFilters());
        }



        public void ApplySourceFilters(DatasetParameterItem p)
        {
            if (_ActiveFilterSourceElements.Count > 0)
            {
                bool contains = false;
                foreach (ToggleFilterElement f in _ActiveFilterSourceElements)
                {
                    if (p.DataSource == f.Source)
                        contains = true;
                }
                if (!contains)
                {
                    p.gameObject.SetActive(false);
                }
                else
                {
                    if (!LoadParameters.instance.FilteredSetsDisplay.ContainsValue(p))
                        LoadParameters.instance.FilteredSetsDisplay.Add(p.ID, p);
                }
            }
        }

        public void ApplyTagFilters(DatasetParameterItem p)
        {
            if (_ActiveFilterTagElements.Count > 0)
            {
                bool contains = false;
                foreach (ToggleFilterElement f in _ActiveFilterTagElements)
                {
                    if (p.Tags.Contains(f.Id))
                        contains = true;
                }
                if (!contains)
                {
                    p.gameObject.SetActive(false);
                }
                else
                {
                    if (!LoadParameters.instance.FilteredSetsDisplay.ContainsValue(p))
                        LoadParameters.instance.FilteredSetsDisplay.Add(p.ID, p);
                }
            }
        }

        public void ApplyGroupFilters(DatasetParameterItem p)
        {
            if (_ActiveFilterGroupElements.Count > 0)
            {
                bool contains = false;
                foreach (ToggleFilterElement f in _ActiveFilterGroupElements)
                {

                    if (p.Groups.Contains(f.Id))
                        contains = true;

                }
                if (!contains)
                {
                    p.gameObject.SetActive(false);
                }
                else
                {
                    if (!LoadParameters.instance.FilteredSetsDisplay.ContainsValue(p))
                        LoadParameters.instance.FilteredSetsDisplay.Add(p.ID,p);
                }
            }

        }

        public void ApplySpeciesFilters(DatasetParameterItem p)
        {
            if (_ActiveFilterSpeciesElements.Count > 0)
            {
                bool contains = false;
                foreach (ToggleFilterElement f in _ActiveFilterSpeciesElements)
                {
                    if (p.Species == f.Id)
                        contains = true;
                }
                if (!contains)
                {
                    p.gameObject.SetActive(false);
                }
                else
                {
                    if (!LoadParameters.instance.FilteredSetsDisplay.ContainsValue(p))
                        LoadParameters.instance.FilteredSetsDisplay.Add(p.ID, p);
                }
            }
        }

        public void ApplyFavFilters(DatasetParameterItem p)
        {
            if (_ActiveFilterFav)
            {
                if (!p.Favorite)
                {
                    p.gameObject.SetActive(false);
                }
                else
                {
                    if (!LoadParameters.instance.FilteredSetsDisplay.ContainsValue(p))
                        LoadParameters.instance.FilteredSetsDisplay.Add(p.ID, p);
                }
            }

        }
        

        public IEnumerator ReceivePythonCommandFromPyinstaller()
        {
            MorphoDebug.Log("waiting for response from pyinstaller");
            WWWForm form = new WWWForm();
            UnityWebRequest www = UnityWebRequests.Post(LOCALHOST + ":" + PYINSTALLER_PORT_RECEIVE, "/get.html", form);
            //www.timeout = 1;
            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ProtocolError || www.result == UnityWebRequest.Result.ConnectionError)
            {
                MorphoDebug.LogWarning("Failed receiving command, starting again : ");
                //yield return new WaitForSeconds(0.2f);
                www.Dispose();
                StartCoroutine(ReceivePythonCommandFromPyinstaller());
            }
            else
            {
                
                String commande = Encoding.UTF8.GetString(www.downloadHandler.data);
                commande = commande.Replace("%20", " ");
                String[] cmds = commande.Split(';');
                MorphoDebug.Log("recieved command " + cmds[0]);
                if (cmds[0].StartsWith("STOP_"))
                {
                    if (cmds[0].Contains("UPLOAD"))
                    {
                        HideAwaitFunction();
                        CreateDatasetMenuManager.Instance.SetUploadMenuVisible(false);
                    }

                    if (cmds[0].Contains("EXPORT"))
                    {
                        HideAwaitFunction();
                    }
                    
                    if (cmds[0].Contains("RECOMPUTE"))
                    {
                        HideAwaitFunction();
                        SetAddDatasetPanelActive(false);
                    }
                    if (cmds[0].Contains("HEADERS_"))
                    {
                        HideAwaitFunction();
                        string headers = cmds[0].Replace("STOP_HEADERS_", "");
                        ParseFileHeaders(headers,setType:!_HeadersFolderMode,directory: _HeadersFolderMode);
                        _HeadersFolderMode = false;
                    }
                    if (cmds[0].Contains("UPDATE_LOCAL_JSON_"))
                    {
                        HideAwaitFunction();
                        LoadParameters.instance.LoadLocalDatasets();
                    }

                }
                if (cmds[0].StartsWith("DELETE_DONE"))
                {
                    //ignore_next_start = true;
                    SetPlotDeleting(false);   
                }
                if (cmds[0].StartsWith("ERROR"))
                {
                    MorphoDebug.LogError("ERROR : ");
                }
            }
        }

        public IEnumerator WaitForMorphoStandaloneInit(int iter)
        {
            float wait = 1f;
#if UNITY_STANDALONE_OSX
            wait=2f;
#endif
            if(!StandaloneInitialized.instance.IsReady)
            {
                if (iter < NUMBER_INIT_TRIES)
                {
                    WWWForm form = new WWWForm();
                    UnityWebRequest www = UnityWebRequests.Post(LOCALHOST + ":" + PYINSTALLER_PORT_RECEIVE, "/get.html", form);
                    
                    yield return www.SendWebRequest();
                    if (www.isHttpError || www.isNetworkError)
                    {
                        MorphoDebug.LogWarning("Failed receiving command that Standalone has started, start again : " + iter);
                        yield return new WaitForSeconds(wait);
                        www.Dispose();
                        StartCoroutine(WaitForMorphoStandaloneInit(iter += 1));
                    }
                    else
                    {
                        MorphoDebug.Log("recieved init command");
                        String commande = Encoding.UTF8.GetString(www.downloadHandler.data);
                        commande = commande.Replace("%20", " ");
                        String[] cmds = commande.Split(';');
                        if (cmds[0].StartsWith("INIT"))
                        {
                            _InitPanel.SetActive(false);

                            StandaloneInitialized.instance.IsReady = true;
                            DontDestroyOnLoad(StandaloneInitialized.instance.gameObject);
                            LoadParameters.instance.InitDone = true;
                            //here you can launch local dataset load
#if !UNITY_EDITOR
                            LoadParameters.instance.LoadLocalDatasets();
#endif
                        }
                    }
                }
                else
                {
                    _AwaitInitText.text = "ERROR : Standalone could not start properly";
                }
            }
            else
            {
                _InitPanel.SetActive(false);
            }

        }


        public void DisplayConfirmPanel(bool v)
        {
            ConfirmPanel.SetActive(v);
        }

        public void DisplayOptionsPanel(bool v)
        {
            OptionsPanel.SetActive(v);
        }

        public void onCanvasScaleValueChanged(float v)
        {
            MainCanvasScaler.scaleFactor = v;
        }

        public void ResetCanvasScaler()
        {
            MainCanvasScaler.scaleFactor=1f;
            CanvasScaleSlider.SetValueWithoutNotify(1f);
        }



        public void SetConfirmPanelTextUpdateDataset()
        {
            _ConfirmPanelDeleteText.gameObject.SetActive(false);
            _ConfirmPanelUpdateText.gameObject.SetActive(true);
        }

        public void SetConfirmPanelTextDeleteDataset()
        {
            _ConfirmPanelDeleteText.gameObject.SetActive(true);
            _ConfirmPanelUpdateText.gameObject.SetActive(false);
        }

        public void SetUpdateFactorField(float value)
        {
            /*_OptionsFactorField.SetTextWithoutNotify(value.ToString());
            _ZDownScale.SetTextWithoutNotify(value.ToString());
            _ZDownScaleSlider.SetValueWithoutNotify(value);*/
        }


#region StandaloneDatasetsInteractions

        public void SaveLocalDataset(string time="",string fullpath="")
        {

            CreateDatasetMenuManager cm = CreateDatasetMenuManager.Instance;


            int background = int.Parse(cm.GetBackgroundValue(), NumberStyles.Any, CultureInfo.InvariantCulture);
            bool smoothing = cm.Smoothing.isOn;
            float smoothpassband = float.Parse(cm.SmoothingPassband.text.Replace(",", "."), NumberStyles.Any, CultureInfo.InvariantCulture);
            int smoothiter = int.Parse(cm.SmoothingIterations.text, NumberStyles.Any, CultureInfo.InvariantCulture);
            bool QC = cm.QuadricClustering.isOn;
            int QCD = int.Parse(cm.QCDivisions.text, NumberStyles.Any, CultureInfo.InvariantCulture);
            bool decim = cm.Decimation.isOn;
            float decim_reduc = float.Parse(cm.DecimateReduction.text.Replace(",", "."), NumberStyles.Any, CultureInfo.InvariantCulture);
            int decim_thresh = int.Parse(cm.DecimateThreshold.text, NumberStyles.Any, CultureInfo.InvariantCulture);

            string name = cm.GetDatasetName();
            LocalDatasetItem local_set = new LocalDatasetItem(name, cm.GetSegData(), cm.GetIntensityData(), cm.GetMinTime(), cm.GetMaxTime(), cm.GetSegDownscale(), background,
                cm.GetXMLFile(), 9875, 9876, time, fullpath, cm.GetSegZDownscale(), smoothing, smoothpassband, smoothiter, QC, QCD, decim, decim_reduc, decim_thresh, 
                cm.GetRawDownscale(), cm.GetRawZDownscale());

            string path = Application.persistentDataPath + LOCAL_DATASET_PATH;

            if (!File.Exists(path))
                File.WriteAllText(path, "");
            string rawjson = File.ReadAllText(path);

            LocalDatasetList json = JsonConvert.DeserializeObject<LocalDatasetList>(rawjson);
            List<LocalDatasetItem> localsets = new List<LocalDatasetItem>();

            if (json == null || json.LocalDatasetItems.Length == 0 || json.LocalDatasetItems == null)
            {
                json = new LocalDatasetList();
                json.LocalDatasetItems = new LocalDatasetItem[0];
            }
            else
            {
                localsets = new List<LocalDatasetItem>(json.LocalDatasetItems);
            }
            //don't add the set if it already exists in the list
            bool add = true;
            foreach (LocalDatasetItem l in localsets)
            {
                if (l.RawDownScale == 0 || l.RawDownScale == 0)//if voxel size is 0,0,0, must be a bug: reset as default
                    l.SetRawScalingDefault();

                if (l.Equals(local_set))
                    add = false;
            }

            if (add)
                localsets.Add(local_set);

            json.LocalDatasetItems = localsets.ToArray();

            string outjson = JsonConvert.SerializeObject(json, Formatting.Indented);
            File.WriteAllText(path, outjson);

        }


        public void RemoveLocalDataset(LocalDatasetItem item)
        {

            string path = Application.persistentDataPath + LOCAL_DATASET_PATH;

            if (File.Exists(path))
            {
                string rawjson = File.ReadAllText(path);


                //LocalDatasetList json = JsonUtility.FromJson<LocalDatasetList>(rawjson);
                LocalDatasetList json = JsonConvert.DeserializeObject<LocalDatasetList>(rawjson);
                List<LocalDatasetItem> localsets = new List<LocalDatasetItem>();

                if (json != null && json.LocalDatasetItems.Length != 0 && json.LocalDatasetItems != null)
                {
                    localsets = new List<LocalDatasetItem>(json.LocalDatasetItems);
                    foreach (LocalDatasetItem l in new List<LocalDatasetItem>(localsets))
                    {
                        if (l.RawDownScale == 0 || l.RawDownScale == 0)//if voxel size is 0,0,0, must be a bug: reset as default
                            l.SetRawScalingDefault();

                        if (l.Equals(item))
                        {
                            //string lpath = l.GetMainPath();
                            SetPlotDeleting(true);
                            localsets.Remove(l);
                            StartCoroutine(RemoveLocalTempData(l.FullPath));
                        }

                    }
                }


                json.LocalDatasetItems = localsets.ToArray();

                string outjson = JsonConvert.SerializeObject(json, Formatting.Indented);
                File.WriteAllText(path, outjson);
            }



        }
        

        public LocalDatasetItem UpdateLocalDataset(LocalDatasetItem item)
        {
            LocalDatasetItem ret = null;
            string path = Application.persistentDataPath + LOCAL_DATASET_PATH;

            if (File.Exists(path))
            {
                string rawjson = File.ReadAllText(path);


                //LocalDatasetList json = JsonUtility.FromJson<LocalDatasetList>(rawjson);
                LocalDatasetList json = JsonConvert.DeserializeObject<LocalDatasetList>(rawjson);
                List<LocalDatasetItem> localsets = new List<LocalDatasetItem>();

                if (json != null && json.LocalDatasetItems.Length != 0 && json.LocalDatasetItems != null)
                {
                    localsets = new List<LocalDatasetItem>(json.LocalDatasetItems);
                    foreach (LocalDatasetItem l in new List<LocalDatasetItem>(localsets))
                    {
                        if (l.RawDownScale == 0 || l.RawDownScale == 0)
                            l.SetRawScalingDefault();

                        if (l.Equals(item))//if we have the dataset:
                        {
                            ret = l;
                            CreateDatasetMenuManager cm = CreateDatasetMenuManager.Instance;
                            int background = int.Parse(cm.GetBackgroundValue(), NumberStyles.Any, CultureInfo.InvariantCulture);
                            bool smoothing = cm.Smoothing.isOn;
                            float smoothpassband = float.Parse(cm.SmoothingPassband.text.Replace(",", "."), NumberStyles.Any, CultureInfo.InvariantCulture);
                            int smoothiter = int.Parse(cm.SmoothingIterations.text, NumberStyles.Any, CultureInfo.InvariantCulture);
                            bool QC = cm.QuadricClustering.isOn;
                            int QCD = int.Parse(cm.QCDivisions.text, NumberStyles.Any, CultureInfo.InvariantCulture);
                            bool decim = cm.Decimation.isOn;
                            float decim_reduc = float.Parse(cm.DecimateReduction.text.Replace(",", "."), NumberStyles.Any, CultureInfo.InvariantCulture);
                            int decim_thresh = int.Parse(cm.DecimateThreshold.text, NumberStyles.Any, CultureInfo.InvariantCulture);

                            _recompute_background = (l.Background != background);
                            _recompute_meshgen = (l.Smoothing != smoothing || l.SmoothPassband != smoothpassband || l.SmoothIterations != smoothiter ||
                                l.QuadricClustering != QC || l.QCDivisions != QCD || l.Decimation != decim || l.DecimateReduction != decim_reduc ||
                                l.AutoDecimateThreshold != decim_thresh || l.DownScale != cm.GetSegDownscale() || l.ZDownScale != cm.GetSegZDownscale());


                            _recompute_raw = (l.RawDownScale != cm.GetRawDownscale() || l.ZRawDownScale != cm.GetRawZDownscale()) ||
                                !cm.EqualImageDict(l.IntensityData, cm.GetConvertedRawData());

                            _recompute_seg = !cm.EqualImageDict(l.SegmentedData,cm.GetConvertedSegData());

                            if (!_recompute_background && !_recompute_meshgen && !_recompute_raw && !_recompute_seg)//if this happens we did not actually change anything, just end it.
                                return ret;

                            l.Smoothing = smoothing;
                            l.SmoothPassband = smoothpassband;
                            l.SmoothIterations = smoothiter;
                            l.QuadricClustering = QC;
                            l.QCDivisions = QCD;
                            l.Decimation = decim;
                            l.DecimateReduction = decim_reduc;
                            l.AutoDecimateThreshold = decim_thresh;

                            l.UpdateData(cm.GetSegData(), cm.GetIntensityData());

                            l.DownScale = cm.GetSegDownscale();
                            l.ZDownScale = cm.GetSegZDownscale();
                            l.RawDownScale = cm.GetRawDownscale();
                            l.ZRawDownScale = cm.GetRawZDownscale();

                            l.Background = background;

                            l.MinTime = cm.GetMinTime();
                            l.MaxTime = cm.GetMaxTime();


                        }

                    }
                }


                json.LocalDatasetItems = localsets.ToArray();

                string outjson = JsonConvert.SerializeObject(json, Formatting.Indented);
                File.WriteAllText(path, outjson);
            }

            return ret;

        }


        public IEnumerator SendLoginToPyinstaller()
        {
            if (!logged)
            {
                WWWForm form = new WWWForm();
                form.AddField("action", "login_morphonet");
                form.AddField("time", 0);
                form.AddField("objects", LoadParameters.instance.user_token + ";" + LoadParameters.instance.id_user);


                UnityWebRequest www2 = UnityWebRequests.Post(LOCALHOST + ":" + PYINSTALLER_PORT, "/ send.html", form);
                www2.SetRequestHeader("Access-Control-Allow-Origin", "*");
                www2.SetRequestHeader("Content-Security-Policy", "upgrade-insecure-requests");
                www2.timeout = 2;
                yield return www2.SendWebRequest();
                if (www2.result == UnityWebRequest.Result.ConnectionError)
                {
                    MorphoDebug.LogError("ERROR : Could not send login info properly");
                }
                else
                {
                    MorphoDebug.Log("Login sent to MorphoNet API");
                    logged = true;

                }
            }

        }

        public IEnumerator SendLogoutToPyinstaller()
        {

            WWWForm form = new WWWForm();
            form.AddField("action", "logout_morphonet");
            form.AddField("time", 0);
            form.AddField("objects", "");


            UnityWebRequest www2 = UnityWebRequests.Post(LOCALHOST + ":" + PYINSTALLER_PORT, "/ send.html", form);
            www2.SetRequestHeader("Access-Control-Allow-Origin", "*");
            www2.SetRequestHeader("Content-Security-Policy", "upgrade-insecure-requests");
            www2.timeout = 2;
            yield return www2.SendWebRequest();
            if (www2.result == UnityWebRequest.Result.ConnectionError)
            {
                MorphoDebug.Log("ERROR : Could not send logout info properly");
            }
            else
            {
                MorphoDebug.Log("Logout sent to MorphoNet API");
                logged = false;

            }


        }

        public void SetCurrentLocalDatasetForExport(LocalDatasetItem item)
        {
            _CurrentLocalDataset = item;
        }

        public void ParseFileHeaders(string headers, bool setType = true, bool directory=false)
        {
            //managing header values
            string[] values = headers.Split('/');
            //shape
            string s = values[0].Replace("[", "").Replace("]", "").Replace("(", "").Replace(")", "");
            string[] s_values = s.Split(',');
            //int[] shape = new int[s_values.Length];

            int[] shape = new int[5]; //XYZCT
            for(int i = 0; i < shape.Length; i++)
            {
                if(i< s_values.Length)
                {
                    shape[i] = int.Parse(s_values[i].Replace("\'", "").Replace(" ", ""));
                }
                else
                {
                    shape[i] = 1; //auto fill "missing" shapes to 1 if any
                }
                
            }

            //voxel size
            float[] voxel_size = new float[3];
            string[] f_values = values[1].Replace("(", "").Replace(")", "").Replace("\'", "").Split(',');
            for (int i = 0; i < voxel_size.Length; i++)
            {
                if (f_values[i].ToLower().Contains("none"))
                {
                    voxel_size[i] = -1.0f;
                }
                else
                {
                    voxel_size[i] = float.Parse(f_values[i].Replace(" ", ""), NumberStyles.Any, CultureInfo.InvariantCulture);
                }

            }

            //read order XYZCT
            int[] dd_order = new int[] { 0,1,2,3,4};
            //actually parse this
            string o = values[3].Replace("[", "").Replace("]", "").Replace("(", "").Replace(")", "").Replace("\'","").Replace(" ", "");
            string[] o_values = o.Split(',');
            if(o_values.Length >= 3) // only if we have at least 3 values. Else, we default as XYZCT
            {
                for (int i = 0; i < 5; i++)
                {
                    if (i < o_values.Length)
                    {
                        int val = SetXYZCTId(o_values[i].ToUpper()[0]);
                        dd_order[i] = (val == -1 ? SmallestRemainingOrderValue(dd_order, i) : val); // only add value if input was correct. otherwise auto-calculate (None)
                    }
                    else//if needed fill with the lowest remaining number
                    {
                        dd_order[i] = SmallestRemainingOrderValue(dd_order, i);
                    }
                }   
            }
            

            //encoding
            string encoding = values[2].Replace("<class", "").Replace(">", "").Replace("\'", "").Replace("numpy.", "").Replace(" ", "");
            
            CreateDatasetMenuManager.Instance.SetMetadata(encoding, voxel_size[0], voxel_size[1], voxel_size[2], shape, dd_order);

            if (setType)
            {
                string inType = "3D image";
                if (NumberOfDimensions(shape) == 4)
                    inType = "4D image";
                if (NumberOfDimensions(shape) == 5)
                    inType = "5D image";
                CreateDatasetMenuManager.Instance.SetMetadadataPathType(inType);
            }

            //add listener for image(s) add after metadata validate
            CreateDatasetMenuManager.Instance.ConfirmMetadata.onClick.RemoveAllListeners();
            if (!directory)
                CreateDatasetMenuManager.Instance.ConfirmMetadata.onClick.AddListener(() => CreateDatasetMenuManager.Instance.AddImageFromFile());
            else
                CreateDatasetMenuManager.Instance.ConfirmMetadata.onClick.AddListener(() => CreateDatasetMenuManager.Instance.AddImageFromFolder(_FolderModeFiles));
        }


        private int SetXYZCTId(char value)
        {
            int ret = -1;
            switch (value)
            {
                case 'X':
                    ret = 0;
                    break;
                case 'Y':
                    ret = 1;
                    break;
                case 'Z':
                    ret = 2;
                    break;
                case 'C':
                    ret = 3;
                    break;
                case 'T':
                    ret = 4;
                    break;
                default:
                    Debug.Log("ERROR: detected wrong character instead of XYZCT : "+value);
                    break;
            }
            return ret;
        }

        private int SmallestRemainingOrderValue(int[] array, int index)
        {
            int[] values = { 0, 1, 2, 3, 4 };
            int[] subArr = new int[index];
            for (int i = 0; i < index; i++)
                subArr[i] = array[i];
            IEnumerable<int> remain = values.Except(subArr);
            return remain.Min();
        }

        /// <summary>
        /// returns number of dimensions from dimension array of image. 3D = 2 dimensions at 1, 4D = 1 dimension at 1, 5D = all dimensions > 1
        /// </summary>
        /// <returns>3,4 or 5</returns>
        private int NumberOfDimensions(int[] dimensions)
        {
            int ones = 0;
            foreach(int val in dimensions)
            {
                if (val == 1)
                    ones++;
            }
            return 5 - ones;
        }


        public void ToggleDevPanelVisibility()
        {
            _DevPanel.SetActive(!_DevPanel.activeSelf);
        }

        public void StartDevPlot()
        {
            LoadParameters.instance.morphoplot_port_send = int.Parse(_PortSend.text, NumberStyles.Any, CultureInfo.InvariantCulture);
            LoadParameters.instance.morphoplot_port_recieve = int.Parse(_PortReceive.text, NumberStyles.Any, CultureInfo.InvariantCulture);
            LoadParameters.instance.start_plot = true;
            LoadParameters.instance.use_plot = true;
            StartCoroutine(LoadParameters.instance.PlotLoading());
        }

#endregion StandaloneDatasetsInteractions

        [Serializable]
        public class LocalDatasetList
        {
            public LocalDatasetItem[] LocalDatasetItems;
        }

        public void SearchDataset()
        {
            LoadParameters.instance.DatasetSetSearchWithFilter();
        }

    }
}



