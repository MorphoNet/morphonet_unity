using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandaloneInitialized : MonoBehaviour
{
    public static StandaloneInitialized instance;

    public bool IsReady = false;

    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
            Destroy(this.gameObject);
    }
}
