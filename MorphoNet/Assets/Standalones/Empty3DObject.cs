using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Empty3DObject : MonoBehaviour
{
    [SerializeField]
    private Image _Background;

    private Color _BaseColor;
    private Color _ReplaceColor = new Color(.66f, .86f, 1f, 1f);

    void Start()
    {
        _BaseColor = _Background.color;
    }

    public void OnPointerEnter()
    {
        if (CreateDatasetMenuManager.Instance.IsDragging)
            _Background.color = _ReplaceColor;
    }

    public void OnPointerExit()
    {
        _Background.color = _BaseColor;
    }
}
