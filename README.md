# MorphoNet_Unity

## MorphoNet"
MorphoNet is a novel concept of web-based morphodynamic browser to visualise and interact with complex datasets, with applications in research and teaching.
MorphoNet offers a comprehensive palette of interactions to explore the structure, dynamics and variability of biological shapes and its connection to genetic expressions.
By handling a broad range of natural or simulated morphological data, it fills a gap which has until now limited the quantitative understanding of morphodynamics and its genetic underpinnings by contributing to the creation of ever-growing morphological atlases.

This git contains the Unity Project of the 3D viewer of MorphoNet

## Built With
In order to open the unity project please use  : Unity 2020.3.10f1 - The game engine used

## Get Started

Some documentation to help you get started is referenced in the [morphonet_documentation](https://gitlab.inria.fr/MorphoNet/morphonet_documentation) repository.

## Authors

Emmanuel Faure  [1,2]

Kévin Fournier [3]

Benjamin Gallean [1,2]

Tao Laurent [2]


with theses affiliations

  - [1] Centre de Recherche de Biologie cellulaire de Montpellier, CRBM, CNRS, Université de Montpellier, France.
  - [2] Laboratoire d'Informatique, de Robotique et de Microélectronique de Montpellier (LIRMM), LIRMM, Univ Montpellier, CNRS, Montpellier, France.
  - [3] Centre INRIA de L'université de Rennes, INRIA, Université de Rennes, France

*Correspondence should be addressed to Emmanuel Faure (emmanuel.faure@lirmm.fr)

## License
This project is licensed under the CeCILL License - see the [LICENSE.md](LICENSE.md) file for details
