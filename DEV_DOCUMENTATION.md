# Introduction

This section is geared towards developers interested on working on MorphoNet. This documentation will first guide you through how to work on MorphoNet, then will explain some of the architecture of the viewer part of the platform.

# Setup

## Overview

MorphoNet is a peculiar platform as it is actually composed of 3 different parts. There is the 3D viewer (which is contained in this repository), developed in Unity3D. \
The second part is the Python backend (which can be found here: https://gitlab.inria.fr/MorphoNet/morphonet_api). Without this "part" of the platform, as a developer you will only be able to use the online part of the web viewer, and will not be able to use the local dataset and curation module.
Finally, there is the lineage window (which is another project you can find here: https://gitlab.inria.fr/MorphoNet/morphonet_lineage). Without this module you will not be able to

## Getting started

### Unity setup

First of all, download the Unity Hub (https://unity.com/fr/download). Then, using the Hub, install the Unity3D 2020.3.35f1 version (you may have to search in the installation archive.). This version is required for the MorphoNet and MorphoNet lineage projects.

you can then clone both repositories and add the local projects to your unity Hub (the MorphoNet project is in the root directory of the repository and the MorphoNet lineage is in the MorphoNetLineage directory)

### python setup

You will need a >= 3.7 working python environment. On it, you will need to install the morphonet package

```
pip install morphonet
```

If you want to be able to fully work on MorphoNet, we recommend that you clone the API repository (which can be found here: https://gitlab.inria.fr/MorphoNet/morphonet_api), and install the package in editable mode.

```
#in the API repository directory:
pip install -e .
```

This way, you will be able to develop in the pyton backend of the MorphoNet standalone, that handles the local dataset and curation module of the standalone application.

## How to work on MorphoNet as a developer

Working on the main viewer is pretty straightforward. You just need to open the project, and select the scene "LoadParmeters.unity" in the Scenes directory. But by using only this part of the platform you will be limited to be able to work with the online viewer only.

To work with the python backend, open a terminal window on the python environment which you setup earlier, go to the /build-standalone directory in the API repository, and launch the script MorphoNet_Standalone.py.

**NOTE**: Without a running MorphoNet process, this script (which is essentially the whole python backend of the standalone application) stops itself. This is so the python process stops itself if the main application window is closed in the built standalone application. To fix this issue, we reccommend that you build MorphoNet_Unity by clicking on File > Build Settings on the MorphoNet_Unity Unity3D window, and by clicking on the Build button in the bottom right, making sure the platform is "PC, Mac & Linux Standalone". After the project is built, just run in once, minimize it and you will be able to run the above script without issue.

Finally, if you want to work on the lineage window, open the lineage project on Unity3D. you simply need to the project to be running before you click on the lineage button with a lineage-compatible online or offline dataset and the windows will connect to each other.

by following these instructions, you will simulate the way the standalone application works.

## Using MorphoNet VR

MorphoNet is compatible with most VR headsets. Using MorphoNet in VR is relatively straightforward. At the moment, the VR mode only works on Windows computers.

### Using an Oculus/Meta compatible headset

You will need to install on your computer the Meta quest app (https://www.meta.com/fr-fr/help/quest/articles/headsets-and-accessories/oculus-rift-s/install-app-for-link/). Open the applicaton, and connect your VR device using the cable or Air link, and complete the setup so your device is recognized in the application. The app needs to be running anytime you want to use the VR module. Once your VR device is properly setup. you need to activate the link mode on the device, which should send you to a white room. Once this is done, just click on the VR button on the top-right in the main viewer application to activate the VR mode.

### Using a SteamVR compatible device

Install Steam (https://store.steampowered.com/), and using the application install SteamVR (https://store.steampowered.com/). Connect your VR device and complete the setup on SteamVR.
Once this is done, just click on the VR button on the top-right in the main viewer application to activate the VR mode.


## Building your own Standalone application.

Here we will show you how to build locally the standalone application, since it is made of several parts. You will need to individually build all the parts and assemble them together.

### Building the MorphoNet_Unity application

To build the MorphoNet_Unity main application, open the project on Unity and simply click on the Build > Build X, X being the OS on which you are operating (on Windows, please select WINDOWS (with VR)). Then Get the build from the Unity_Build folder.

### Building the python backend application

The python backend is build using [pyinstaller](https://pyinstaller.org/en/stable/). It builds the singluar backend script into a launchable application.
Once your python environment is properly setup, in the API repository, go to the build_standalone with a terminal, and do the following command:

```
pyinstaller MorphoNet_Standalone_X.spec #X being the OS for which you want to build
```

Once the command has finished running, the finished build folder can be found in the *dist* folder.

### Building the MorphoNet_Lineage application

To build the MorphoNet_lineage application, open the project on Unity and simply click on the Build > Build X, X being the OS on which you are operating. Then Get the build from the Unity_Build folder.

## Assembling into the standalone

To assemble the full lineage app, you need to basically backage the result of the python backend resulting application folder and the lineage app build in a inside the MorphoNet_Unity build, inside folders called LINEAGE and STANDALONE respectively. \
The final application should be packaged as following:

**Windows:**
```
.
|-MorphoNet
  |-UnityPlayer.dll
  |-UnityCrashHandler64.exe
  |-MorphoNet.exe
  |-MorphoNet_Data
  |-MonoBleedingEdge
  |-libs
    |-LINEAGE
      |-MorphoNetLineage
        |-UnityPlayer.dll
        |-UnityCrashHandler64.exe
        |-MorphoNetLineage.exe
        |-MorphoNetLineage_Data
        |-MonoBleedingEdge
    |-STANDALONE
      |-MorphoNet_Standalone
        |-_internal
          |- ...
        |-MorphoNet_Standalone.exe
```

**MacOS:**
```
.
|-MorphoNet.app
  |-Contents
    |-...
    |-LINEAGE
      |-MorphoNetLineage.app
        |-...
    |-STANDALONE
      |-MorphoNet_Standalone.app
        |-...
```

**Linux:**
```
.
|-MorphoNet
  |-MorphoNet (executable)
  |-UnityPlayer.so
  |-...
  |-libs
    |-LINEAGE
      |-MorphoNetLineage
        |-MorphoNetLineage (executable)
        |-UnityPlayer.so
        |-...
    |-STANDALONE
      |-MorphoNet_Standalone
        |-MorphoNet_Standalone (executable)
        |-_internal
          |- ...

```



# MorphoNet Unity (different documentation file ?)

In the following section, you will learn how the MorphoNet Unity Project is organized.

For this whole section, we start from the principle that you are somewhat familiar with Unity3D. If you are just discovering how to use Unity3D, you can find [tutorials here](https://unity.com/fr/learn/get-started) and the [overall documentation here](https://docs.unity3d.com/2020.3/Documentation/Manual/)

## The scenes

Being a Unity3D project, MorphoNet is divided into scenes. The project is divided into 2 main scenes, and another that is streamed in real-time on demand. These scenes can be found in the Scenes folder.

- **LoadParameters**: This scene is the main menu of the standalone application. It it, you can login or logout of your MorphoNet account, browse the online datasets, as well as local datasets if you have any. This is also the page on which you can create local datasets for visualization and/or curation by using the python backend.

- **SceneEmbryo**: This scene is the main MorphoNet viewer. Whether you are viewing an online dataset or doing curation on a local dataset, everything happens on this scene.

- **MorphoNetVR**: This scene is never actuall accessed: its contents are added dynamically on the *SceneEmbryo* when the VR module is used. Its contents are subsequently unloaded if the VR module is deactivated. If you are modifying anything on the VR scene hierarchy, you will need to do it in this scene.

## Working with prefabs

When working on MorphoNet, one important aspect is working with [prefabs](https://docs.unity3d.com/Manual/Prefabs.html). Prefabs are a Unity3D specific concept, that is used to instantiate multiple instances of a specific object or object group. It is used for the same purposes in morphonet, whether it is for scene elements or UI elements, but it is also used for an important aspect of MorphoNet : UI elements organization and collaborative work.

Using prefabs for UI elements allows to put scenes in compartments. If no prefabs are being used for the UI of a scene, the restult is a very large file for the corresponding scene. Some scenes (mostly the SceneEmbryo in our case) can get very large and complex, with a lot of interconnected parts. When working with several other people, this can cause large and complexe scene conflics when merging changes, which are complicated to work with. To avoid this, when reworking any part of the UI or adding a new menu/element, make it into a prefab. This way several people can work on different parts of the same scene without conflicts.

For instance, If looking at the SceneEmbryo scene, you may notice that the whole curation menu is a prefab. if you need to work on this menu, you need to edit the prefab, not the scene itself (as much as possible, for some object references that are outside of the prefab you may need to add the reference in the scene).

## Singletons

For organization and acces purposes, you will notice that a lot of important scripts in morphonet are Singletons. This way, it facilitates "distant" components in the scene hierarchy to interact together easily.

# Scene organizations

In this section we will be seeing the main points in the 3 main scenes composing the main project. We will not detail every script nor show you where they are located in the scene hierarchy, but mostly point you to the main components of the scenes, to hopefully give you a good overview of how each scene functions.

## LoadParameters scene

The organization of the scene is relatively straightforward. all of the GUI is in the Canvas root object, and there are 3 main scripts to know.

### LoadParameters.cs

A [monobehavior](https://docs.unity3d.com/ScriptReference/MonoBehaviour.html) in the root *LoadParmeters* GameObject, it is a primordial component of this scene. It is a persistant components (it stays the same between scene transitions), and is used to send the main parameters for dataset instantiation to the next scene (SceneEmbryo). It is a singleton. It is also used for some communication functions with the pyton bakend during dataset creation.

### StandaloneMainMenuInteractions.cs

A [monobehavior](https://docs.unity3d.com/ScriptReference/MonoBehaviour.html) in the root *LocalInteractionManager* GameObject, it is a primordial component to manage the UI logic of the scene. It is a singleton. It manages the loading of datasets (online and local), the filtering of datasets, and most of the interaction between the UI elements of the scene (such as the filepicker to dataset creation).

### CreateDatasetMenuManager.cs

Placed in the prefab ADD_DATASET_PANEL_V2, it is a MonoBehavior. Its place in the scene hierarchy is as follows:

```
...
-Canvas
  -container
    -ADD_DATASET_PANEL_V2 #Prefab
      -Window
        -MAIN_DATASET_MENU #CreateDatasetMenuManager.cs here
        ...
      ...
    ...
  ...
...
```
This script handles all the interactions for the *Add dataset* menu, that allows to create datasets from local data with the help of the python backend, in order to do data curation.

### Other notable scripts

The other notable scripts in this scene are:

- **StandaloneInitialized.cs**: Found int the root object of the same name, it is important for anything that happens at the launch of the application.

## SceneEmbryo scene

This scene is the main 3D+t viewer. it is very complex and a lot of different scripts operate it. We will mostly explain the main components of the viewer and where you can find them in the scene hierarchy.

What is important to note is that a lot of GameObjects and Monobehaviors are instantiated after the scene starts. As a result, a lot will be only seen on a runnig scene (as opposed to the scene in edit mode, that is missing most of the main components)

### Overview

We can divide this scene in several parts:

- **dataset management:** The first group of scripts manages everything around the *dataset* Object. This is essentially all the data and interactions surrounding the dataset you are viewing (The meshes, intensity images, dataset properties, etc.). We will see in detail later what are the main scripts and how they are organized.

- **UI management:** The second group contains all the scipts that manage the overall interface of the scene. All the interaction of the menus, and the UI logic is contained in these scripts.

- ***DirectPlot:*** This oddly-named part is the part that manages all the specific interactions around the curation module, exclusive to local datasets, that require the runnig python backend. It bears this name as the curation module was called *MorphoPlot* during it early development.

 - **Lineage viewer:** This part is composed of all the systems that interact with the lineage viewer, whether the dataset is online or offline. Note that the lineage viewer works differently in the standalone compared to the WebGL version. in WebGL, the lineage window is another web page, whereas in the standalone it is another Unity application.

### Dataset management

The dataset management is centered around the **Dataset.cs** script. This monobehavior is attached to the Embryos>embryo0 GameObject, which is instantiated when a dataset is loaded.

#### Dataset initialization

All the systems to load the data of this dataset are handled by 3 different scripts :

 - **LoadParameters.cs** : This script (Which is permanent from the previous scene) handles passing data to the initialization systems described in this section. It is located on a root object of the same name (in the sub category *DontDestroyOnLoad*)

 - **SetsManager.cs:** A monobehavior on the root GameObject of the same name, this script handles a lot of functions surrounding the instantiation of the *dataset* object.

- **DataLoader.cs:** A monobehavior on the root GameObject of the same name, this script handles all the functions that are called to load all the data (whether online or offline) of the Dataset : meshes, intensity images, properties, etc.

#### Interacting with the dataset

All the scripts around the *dataset* object are as follow (they are all Monobehaviors attached to the dataset):

- **TransformationsManager.cs:** This script manages the position, rotation and scaling of the dataset. All functions that changes these values go through this manager.

- **DataSetInformations.cs:** This script manages the **Properties** (previously called informations) of a dataset. All functions that interact with these properties use this script. It also handles UI logic of the Properties menu.

- **GroupManager.cs:** This script manages element groups of the dataset. All functions concerning element or cell groups are on this script.

- **TransformationsManager.cs:** This script manages the position, rotation and scaling of the dataset. All functions that changes these values go through this manager.

- **RawImages.cs:** This script manages the **Intensity images** (previously named raw images) of a dataset. All functions regarding loading images, handling the intensity image shader, and overall intensity image management are in this script.

- **PickedManager.cs:** This script contains the functions around the **selection** (previously called picking) of cells or elements in a dataset.

- **ScatterView.cs:** This script manages the scatter function, that allow to have an exploded view of the elements in a dataset.

- **FigureManager.cs:** This script manages the functions used in the **Tools menu** (we will see about it in a later section) to quickly make figures.

- **GeneticManager.cs:** This script manages the linking of a genetic database to a dataset (for instance with Aniseed).

- **Recording.cs:** This script manages the functions used to record movies using the **Movies** menu in the **Tools** menu.

## UI management

The UI management is mostly done through the **InterfaceManager.cs** script. Placed in the root of the scene on a GameObject with the same name, it is a singleton that aggregates references to almost all the UI elements of the scene, so it can be used either in functions in the script, or in other scripts. It also contains a lot of the UI logic of the overall scene.

For the main menus (Dataset, Images, Properties, Labels, groups, genetic, and tools), there are other scripts that contain the corresponding logic:

- **DataSetMenu.cs:** Placed on the Canvas>button layout group>Dataset GameObject. It handles some of the logic of the dataset menu.

- **ImagesMenu.cs:** Placed on the Canvas>button layout group>Images>MenuImages GameObject. It handles all th UI logic of the Images menu.

- **SelectionManager.cs:** Placed on the Canvas>button layout group>Labels>MenuLabels>Selection GameObject. It handles some of the logic of the Labels menu, that handles labeing elements of a dataset.

## Curation Module - *Direct Plot*

The curation module, sometimes called MorphoPlot or DirectPlot in scripts, is the compilation of the systems that are unique to the local datasets / curation mode of MorphoNet.

MorphoNet_Unity communicates with the python backend using a socked server created in python. it then communicates using simple html requests, which are sent in [Coroutines](https://docs.unity3d.com/Manual/Coroutines.html). To recuperate information from the python backend it also runs requests, sometimes in a loop until data is sent.

Essentially all of the code from this module is in the **DirectPlot.cs** script. it handles anything from the UI of the Curation menu, to the python-to-unity communication.

Some functionalities unique to the offline curation mode are in the following scripts:

- **RuntimeMeshEditor.cs:** This script handles the logic for the *Mesh morphing plugin* tools. it is found in the Canvas>button layout group>Segmentation>MenuSegmentation>RuntimeMeshEditor
